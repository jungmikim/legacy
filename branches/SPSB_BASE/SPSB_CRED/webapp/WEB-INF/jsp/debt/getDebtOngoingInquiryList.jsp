<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>

<%-- 정렬관련 공통코드 --%>
<c:set var="orderTypeList" value="${result.orderTypeList}"/>

<%-- 목록갯수 공통코드 --%>
<c:set var="rowSizeTypeList" value="${result.rowSizeTypeList}"/>

<%-- 작성일자 검색조건 공통코드 --%>
<c:set var="registPeriodConditionTypeList" value="${result.registPeriodConditionTypeList}"/>

<%-- 정렬조건 공통코드 --%>
<c:set var="debtOngoingListOrderConditionTypeList" value="${result.debtOngoingListOrderConditionTypeList}"/>

<%-- 채무불이행 등록 담당자 --%>
<c:set var="debtMbrList" value="${result.debtMbrList}"/>

<%-- 채무불이행 신청자 --%>
<c:set var="debtRegistMbrList" value="${result.debtRegistMbrList}"/>

<%-- 채무불이행 신청서 리스트 --%>
<c:set var="debtOngoingList" value="${result.debtOngoingList}"/>

<%-- 채무불이행 신청서 페이징 --%>
<c:set var="pcPage" value="${result.pcPage}"/>

<%-- 채무불이행 검색 총 갯수 --%>
<c:set var="total" value="${result.total}"/>



<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>채무불이행등록 &gt; 진행 중 접수문서 | 스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtInquiryList.css" />

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	
	// 채무금액정정 select show/hide 유무
	if($(".chkmodType:checked:not(#condition01)").length>0 || $(".processStatusType:checked").length==0 ){
		$("#mod_stat_type").show();
		
	}else{
		$("#mod_stat_type").hide();
	}
	
	/*
	* <pre>
	* 거래처 검색조건 체인지 이벤트 함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014. 05. 22
	*/
	$('.custSearchType').change(function() {
		$('#cust_kwd').val(''); // 거래처 inputbox 초기화
		if($(this).val() == 'business_all') {
			$('#cust_kwd').attr("disabled", "disabled");
		}else{
			$('#cust_kwd').removeAttr("disabled");
		}
	});
	
	/**
	* <pre>
	*   진행중 접수문서 CHECKBOX 전체 선택 토글 함수
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	**/
	$('#btn_select_all').toggle(
		function(){
			$('.chk_remove_debt').attr('checked', true);
		},
		function(){
			$('.chk_remove_debt').attr('checked', false);
		}
	);
	
	/**
	* <pre>
	*   검색 버튼 클릭 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.search').click(function(){
		if(!$.fn.validate()) return false;
			$('#num').val(1);
			$('form').attr({'action':'${HOME}/debt/getDebtOngoingList.do', 'method':'POST'}).submit();
	});
	
	/**
	* <pre>
	*	정렬 조건을 변경 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.listOrderOption').change(function(){
		if(!$.fn.validate()) return false;
		$('#num').val(1);
		$('form').attr({'action':'${HOME}/debt/getDebtOngoingList.do', 'method':'POST'}).submit();
	});
	
	/**
	* <pre>
	*	진행상태 CHECKBOX 전체선택 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.processStatusType').change(function(){        	
       	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $(this).parent().find(':checked').not('[name=processStatusALL]').length;
       	
		if($(this).val() == 'ALL' && checkedLen == 11){
       		$(this).attr('checked',true);
       		$("#process_status_type").val("");
		}else if($(this).val() == 'ALL' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       		$("#process_status_type").val("");
       	}else if($(this).val() != 'ALL' && checkedLen != 11){
       		$(this).siblings().eq(0).attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 11){
       		$(this).siblings().eq(0).attr('checked',true);
       		$("#process_status_type").val("");
       	}
	});
        
	/**
	* <pre>
	*	등록담당자 전체/개별선택 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.debtMbrType').change(function(){
		if($(this).val() == 'ALL'){
			$("#nopayment_mbr_select").attr('disabled','disabled');
			$("#mbr_spanList > span").remove();
			if($(this).attr("id")=='person_all'){
				$("#mbr_list").val("");
				$("#mbr_name_list").val("");
			}			
		}else{
       		$("#nopayment_mbr_select").removeAttr('disabled');
       	}
	});
	
	/**
	* <pre>
	*	신청자 전체/개별선택 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.noPaymentRegistMbrType').change(function(){
		if($(this).val() == 'ALL'){
			$("#nopayment_regist_mbr_select").attr('disabled','disabled');
			$("#mbr_regist_spanList > span").remove();
			
			if($(this).attr("id")=='registPerson_all'){
				$("#mbr_regist_list").val("");
				$("#mbr_regist_name_list").val("");
			}			
			
		}else{
       		$("#nopayment_regist_mbr_select").removeAttr('disabled');
       	}
	});
	
	/**
	* <pre>
	*	등록 담당자 선택 시 발생되는 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('#nopayment_mbr_select').change(function(){
     	   
		// [1] 첫번째 option 값이 설정되었을 경우 검증
		if($(this).find('option').is(':first:selected')){
			return false;
		}
     	   
		// [2] 이미 선택된 담당자인지 체크함
		$mbrUsrId = $(this).find('option:selected').attr('mbrUsrId');
		$checkBTF = true;
		$("#mbr_spanList > span").each(function(e){
			if($(this).attr('mbrUsrId') == $mbrUsrId){
				alert("이미 선택된 담당자 입니다.");
				$checkBTF = false;
			}
		});
		if(!$checkBTF){
			return false;
		}
     	   
		// [3] 10명이상 담당자가 늘어난 경우 담당자 추가 제한함
		if($("#mbr_spanList").children().length + 1 > 10){
			alert("채무불이행 등록 담당자 검색은 10명 이상 할 수 없습니다.\n다수의 담당자를 검색하시고 싶은 고객은 [전체]선택을 통해 검색해주세요.");
			return false;
		}
     	   
		// [4] 검증 통과 후 , 담당자 명 설정 표기함
		$data = '<span class="mbr_name" mbrUsrId="' + $(this).find('option:selected').attr('mbrUsrId') + '">' + $(this).val() + '&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>';
		$('#mbr_spanList').append($data);
     	   
		// [5] HIDDEN TYPE 의 INPUT BOX에 데이터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			$("#mbr_spanList > span").each(function(index){
				$mbr_list += $.trim($(this).attr('mbrUsrId'));
				$mbr_name_list += $.trim($(this).text());
				if((index+1) != $("#mbr_spanList").children().length){
					$mbr_list += ',';
					$mbr_name_list += ',';
				}
			});
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
     	   
		// [6] 담당자 선택 SELECT TAG 초기화 실시함
		$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
	});
	
	/**
	* <pre>
	*	신청자 선택 시 발생되는 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('#nopayment_regist_mbr_select').change(function(){
     	   
		// [1] 첫번째 option 값이 설정되었을 경우 검증
		if($(this).find('option').is(':first:selected')){
			return false;
		}
     	   
		// [2] 이미 선택된 담당자인지 체크함
		$mbrUsrId = $(this).find('option:selected').attr('mbrRegistUsrId');
		$checkBTF = true;
		$("#mbr_regist_spanList > span").each(function(e){
			if($(this).attr('mbrRegistUsrId') == $mbrUsrId){
				alert("이미 선택된 담당자 입니다.");
				$checkBTF = false;
			}
		});
		if(!$checkBTF){
			return false;
		}
     	   
		// [3] 10명이상 담당자가 늘어난 경우 담당자 추가 제한함
		if($("#mbr_regist_spanList").children().length + 1 > 10){
			alert("채무불이행 신청자 검색은 10명 이상 할 수 없습니다.\n다수의 담당자를 검색하시고 싶은 고객은 [전체]선택을 통해 검색해주세요.");
			return false;
		}
     	   
		// [4] 검증 통과 후 , 담당자 명 설정 표기함
		$data = '<span class="mbr_regist_name" mbrRegistUsrId="' + $(this).find('option:selected').attr('mbrRegistUsrId') + '">' + $(this).val() + '&nbsp;<a href="#none"><img class="btn_remove_mbr_regist_name" src="${IMG}/common/btn_s_X.gif"></a></span>';
		$('#mbr_regist_spanList').append($data);
     	   
		// [5] HIDDEN TYPE 의 INPUT BOX에 데이터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_regist_spanList").children().length > 0){
			$("#mbr_regist_spanList > span").each(function(index){
				$mbr_list += $.trim($(this).attr('mbrRegistUsrId'));
				$mbr_name_list += $.trim($(this).text());
				if((index+1) != $("#mbr_regist_spanList").children().length){
					$mbr_list += ',';
					$mbr_name_list += ',';
				}
			});
		}
		$("#mbr_regist_list").val($mbr_list);
		$("#mbr_regist_name_list").val($mbr_name_list);
     	   
		// [6] 담당자 선택 SELECT TAG 초기화 실시함
		$("#nopayment_regist_mbr_select option:eq(0)").attr("selected", "selected");
	});
         
	/*
	* <pre>
	*   나열되어있는 등록담당자 삭제 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$(document).on('click','.btn_remove_mbr_name',function(e){
		// [1] 해당 등록담당자 ELEMENT를 삭제한다.
		$(this).parent().parent().remove();
		
		// [2] 모든 담당자 삭제시 자동 [전체] 메뉴가 선택된다.
		if($("#mbr_spanList").children().length == 0){
			$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
			$('#nopayment_mbr_select').attr('disabled', true);
			$("#person_name").attr('checked',false);
			$("#person_all").attr('checked','checked');
			
		}
		
		//[3] FORM ACTION 파라미터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			   $("#mbr_spanList > span").each(function(index){
				   $mbr_list += $.trim($(this).attr('mbrUsrId'));
				   $mbr_name_list += $.trim($(this).text());
				   if((index+1) != $("#mbr_spanList").children().length){
					   $mbr_list += ',';
					   $mbr_name_list += ',';
				   }
			   });
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
	});
	
	/*
	* <pre>
	*   나열되어있는 신청자 삭제 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$(document).on('click','.btn_remove_mbr_regist_name',function(e){
		// [1] 해당 등록담당자 ELEMENT를 삭제한다.
		$(this).parent().parent().remove();
		
		// [2] 모든 담당자 삭제시 자동 [전체] 메뉴가 선택된다.
		if($("#mbr_regist_spanList").children().length == 0){
			$("#nopayment_regist_mbr_select option:eq(0)").attr("selected", "selected");
			$('#nopayment_regist_mbr_select').attr('disabled', true);
			$("#registPerson_name").attr('checked',false);
			$("#registPerson_all").attr('checked','checked');
			
		}
		
		//[3] FORM ACTION 파라미터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_regist_spanList").children().length > 0){
			   $("#mbr_regist_spanList > span").each(function(index){
				   $mbr_list += $.trim($(this).attr('mbrRegistUsrId'));
				   $mbr_name_list += $.trim($(this).text());
				   if((index+1) != $("#mbr_regist_spanList").children().length){
					   $mbr_list += ',';
					   $mbr_name_list += ',';
				   }
			   });
		}
		$("#mbr_regist_list").val($mbr_list);
		$("#mbr_regist_name_list").val($mbr_name_list);
	});
	
	/**
	*<pre>
	*	작성기간 전체가 아닐 경우 날짜표시 function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.07
	*/
    $('#period_condition').change(function(){
   	 if('ALL_DT' == $(this).val()){
   		 $(this).parent().siblings().attr('disabled','disabled').hide(); 
   	 } else {
   		 $(this).parent().siblings().removeAttr('disabled').show();
   	 }
    });
	
    /**
     * <pre>
     *	사업자등록번호 붙여넣기 할 때 '-' 삭제하기
     * </pre>
     * @author 백원태
     * @since 2014. 12. 15
     */
     $('#cust_kwd').change(function(){
    	 if(($('#business_no').attr('checked')) || ($('#company_corp_no').attr('checked'))){
    	 	$('#cust_kwd').attr('value', $('#cust_kwd').attr('value').replace(/-/g, '')); 
    	 }
     });
     
     
	/**
    * <pre>
    *	진행 중 접수문서 조회 정보 Excel 다운로드 함수
    * </pre>
	*@author HWAJUNG SON
	*@since 2014.05.08
    */
	$('#btn_save_excel').click(function(){
		if(!$.fn.validate()) return false;
		$('form').attr({'action':'${HOME}/debt/getDebtOngoingListForExcel.do', 'method':'POST'}).submit();
	});
	
	/**
	*<pre>
	*	진행상태 클릭시  function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.07
	*/
	$(".processStatusType").click(function(){
		
		$processList = '';
		$parent = $(this).parent();
		

		if($parent.find('input:checked').length > 0){
			$parent.find('input:checked').each(function(idx){
				$processList += $(this).val();
				if(idx+1 != $parent.find('input:checked').length){
					$processList += ',';
				}
			});
		}
		$("#process_status_type").val($processList);
		
		// 전체를 클릭하였을 경우 
		if($(this).attr("id")=='condition01'){
			if($(this).is(":checked")){
				$('.processStatusType').prop('checked', true);	
				$("#process_status_type").val("");
				
			}else{
				$('.processStatusType').prop('checked', false);
			}
		}
		
		// 진행상태가 전체, 통보서 준비, 통보서 발송, 민원발생, 민원발생(추가증빙), 등록완료 
		// 체크 되어있을 경우 채무금액 정정 검색가능 
		if($(".chkmodType:checked:not(#condition01)").length>0 || $(".processStatusType:checked").length==0 ){
			$("#mod_stat_type").show();
			
		}else{
			$("#mod_stat_type").hide();
			$(".modStatType").prop("checked",false);
		}
			
	});
	
	/**
	*<pre>
	*	접수코드 클릭시 상세정보 이동 function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.09
	*
	*/
	$(".linkDebt").click(function(){
		$paramObj = {
				debtApplId : $(this).attr('data-debtApplId')
   	    };
       	location.href ="${HOME}/debt/getDebtOngoingInquiry.do?" + $.param($paramObj);
	});
	
	
});

/**
 * <pre>
 *   검색 & 정렬 전 검증 function
 * </pre>
 * @author HWAJUNG SON
 * @since 2014. 05. 7
 */
 $.fn.validate = function() {
	
	 $bTF = true; // 검증용 Flag 변수

	 // 신청서 진행상태는 반드시 선택되어야 함
	 if ($('.processStatusType:checked').length == 0) {
		 alert("진행상태를 선택해주세요.");
		 return false;
	 }
	 
	 // 거래처 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	 if(!$("#business_all").is(':checked') && $.trim($("#cust_kwd").val()) == ''){
		 alert("[" + $(".custSearchType:checked").next().text() + "] 검색어를 입력해주세요");
		 $("#cust_kwd").focus();
		 return false;
	 }
	 
	// 거래처 사업자등록번호 선택할 경우 숫자를 검증
	if($('.custSearchType:checked').attr('id') == 'business_no' || $('.custSearchType:checked').attr('id') == 'company_corp_no') {
		 if(!typeCheck('numCheck',$.trim($('#cust_kwd').val()))){
			 alert($(".custSearchType:checked").attr('title') + '는 숫자만 입력가능합니다.');
			 $('#cust_kwd').focus();
			return false;
		 }
	}
	 return $bTF;
 };
 
</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<%-- Top Area End --%>

<%-- contentArea --%>
<div class="contentArea sub01"><%-- menu별 이미지 class sub00 --%>
	<%-- content --%>
	<section id="content">
		<h2 class="tit_me03_0102">진행 중 접수문서 조회</h2>
		<p class="tit_me03_0102_under">신청 완료 후, 접수 진행 중인 문서를 조회하고 확인합니다.</p>
		
		<form name="debtOngoinInquiryListFrm" id="debtOngoinInquiryListFrm">
		
		<%-- FORM HIDDEN AREA START --%>
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/debt/getDebtOngoingList.do">
		
		<%-- 채무불이행 등록담당자 --%>
		<input type="hidden" name="mbrList" id="mbr_list" value="${sBox.mbrList}" />
		<input type="hidden" name="mbrNameList" id="mbr_name_list" value="${sBox.mbrNameList}" />
		
		<%-- 채무불이행 신청자 --%>
		<input type="hidden" name="registMbrList" id="mbr_regist_list" value="${sBox.registMbrList}" />
		<input type="hidden" name="mbrRegistNameList" id="mbr_regist_name_list" value="${sBox.mbrRegistNameList}" />

		<%-- 채무불이행 진행상태 대상 리스트(구분자 ,) --%>
		<input type="hidden" name="processStatusType" id="process_status_type" value="${sBox.processStatusType}"/>
	
		<%-- FORM HIDDEN AREA END --%>
		
		<fieldset class="searchBox">
			<legend>진행 중 접수문서 조회</legend>
			<table summary="채무불이행 접수 진행 중 리스트 테이블로, 검색조건인 작성일자,거래처,진행상태,채무금액정정, 등록담당자, 신청자로 구성되어있습니다.">
			<caption>진행 중 접수문서 조회 </caption>
				<colgroup>
					<col width="20%" />
					<col width="80%" />
				</colgroup>
				<tr>
					<th scope="row">작성기간</th>
					<td class="block">
						<span class="first">
							<select title="작성기간" name="periodConditionType" id="period_condition">
								<c:forEach var="registPeriodConditionTypeList" items="${registPeriodConditionTypeList}">
									<option value="${registPeriodConditionTypeList.KEY}" <c:if test="${sBox.periodConditionType eq registPeriodConditionTypeList.KEY}">selected</c:if>>${registPeriodConditionTypeList.VALUE}</option>	
								</c:forEach>
							</select>
						</span>
						<input type="text" name="periodStDt" id="period_st_dt" class="firstDate periodDate" title="작성기간 시작조건" value="${sBox.periodStDt}" readonly="readonly" <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>/><span <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>> ~ </span>
						<input type="text" name="periodEdDt" id="period_ed_dt" class="firstDate periodDate" title="작성기간 종료조건" value="${sBox.periodEdDt}" readonly="readonly" <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>/>
					</td>
				</tr>
				<tr>
					<th scope="row">거래처</th>
					<td class="block">
						<input type="radio" class="custSearchType" name="custSearchType" value="business_all" id="business_all" <c:if test="${sBox.custSearchType eq 'business_all' }">checked</c:if>/> <label for="business_all">전체</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="business_no" id="business_no" title="사업자등록번호" <c:if test="${sBox.custSearchType eq 'business_no' }">checked</c:if>/> <label for="business_no">사업자등록번호</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_name" id="company_name" <c:if test="${sBox.custSearchType eq 'company_name' }">checked</c:if>/> <label for="company_name">거래처명</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_owner_name" id="company_owner_name" <c:if test="${sBox.custSearchType eq 'company_owner_name' }">checked</c:if>/> <label for="company_owner_name">대표자(채무자)</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_corp_no" id="company_corp_no" title="법인등록번호" <c:if test="${sBox.custSearchType eq 'company_corp_no' }">checked</c:if>/> <label for="company_corp_no">법인등록번호</label> 
						<input type="text" id="cust_kwd" name="custKwd" class="txtInput" <c:if test="${sBox.custSearchType eq 'business_all'}">disabled="disabled"</c:if> style="width:110px" value="${sBox.custKwd}" maxlength="70"/>
					</td>
				</tr>
				<tr>
					<th scope="row">진행상태</th>
					<td class="inlineBlock">
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusALL" value="" id="condition01" <c:if test="${sBox.processStatusALL eq ''}">checked</c:if> /> <label for="condition01">전체</label>
						<input type="checkbox" class="processStatusType" name="processStatusAW" value="AW" id="condition02" <c:if test="${sBox.processStatusAW eq 'AW'}">checked</c:if> /> <label for="condition02">심사대기</label>
						<input type="checkbox" class="processStatusType" name="processStatusEV" value="EV" id="condition03" <c:if test="${sBox.processStatusEV eq 'EV'}">checked</c:if>  /> <label for="condition03">심사중</label>
						<input type="checkbox" class="processStatusType" name="processStatusAA" value="AA" id="condition04" <c:if test="${sBox.processStatusAA eq 'AA'}">checked</c:if>  /> <label for="condition04">결제대기</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusPY" value="PY" id="condition05" <c:if test="${sBox.processStatusPY eq 'PY'}">checked</c:if>  /> <label for="condition05">통보서 준비</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusNT" value="NT" id="condition06" <c:if test="${sBox.processStatusNT eq 'NT'}">checked</c:if>  /> <label for="condition06">통보서 발송</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusCA" value="CA" id="condition07" <c:if test="${sBox.processStatusCA eq 'CA'}">checked</c:if>  /> <label for="condition07">민원발생</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusFA" value="FA" id="condition08" <c:if test="${sBox.processStatusFA eq 'FA'}">checked</c:if>  /> <label for="condition08">추가증빙제출</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusRC" value="RC" id="condition09" <c:if test="${sBox.processStatusRC eq 'RC'}">checked</c:if>  /> <label for="condition09">등록완료</label>
						<input type="checkbox" class="processStatusType" name="processStatusRR" value="RR" id="condition10" <c:if test="${sBox.processStatusRR eq 'RR'}">checked</c:if>  /> <label for="condition10">해제요청</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusCB" value="CB" id="condition11" <c:if test="${sBox.processStatusCB eq 'CB'}">checked</c:if>  /> <label for="condition11">민원발생</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusFB" value="FB" id="condition12" <c:if test="${sBox.processStatusFB eq 'FB'}">checked</c:if>  /> <label for="condition12">추가증빙제출</label>
					</td>
				</tr>
				
				<tr id="mod_stat_type">
					<th scope="row">채무금액정정</th>
					<td>
						<input type="checkbox" class="modStatType" name="modStatR" value="R" id="mod_stat_r" <c:if test="${sBox.modStatR eq 'R'}">checked</c:if>  /> <label for="mod_stat_r">채무금액 정정요청</label>
						<input type="checkbox" class="modStatType" name="modStatC" value="C" id="mod_stat_c" <c:if test="${sBox.modStatC eq 'C'}">checked</c:if>  /> <label for="mod_stat_c">채무금액 정정완료</label>
						
					</td>
				</tr>
		
				<%-- 채무불이행 신청권한이 있는 유저만이 등록담당자 조건으로 검색가능함 --%>
				<c:if test="${sBox.isSessionDebtApplGrn}">
					<tr>
						<th scope="row">등록담당자</th>
						<td>
							<input type="radio" name="debtMbrType" class="debtMbrType" id="person_all" value="ALL" <c:if test="${sBox.debtMbrType eq 'ALL'}">checked</c:if> /> <label for="person_all">전체</label>
							<input type="radio" name="debtMbrType" class="debtMbrType" id="person_name" value="name" <c:if test="${sBox.debtMbrType eq 'name'}" >checked</c:if>/> <label for="person_name" class="hidden-accessible">등록 담당자</label>
							<select id="nopayment_mbr_select" <c:if test="${sBox.debtMbrType ne 'name'}">disabled="disabled"</c:if> title="등록 담당자" >
								<option>--선택--</option>
								<c:forEach var="i" items="${debtMbrList}">
									<option value="${i.USR_NM }" <c:if test="${i.MBR_TYPE eq 'N'}">class="mem_type_N"</c:if>  mbrUsrId="${i.MBR_USR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
								</c:forEach>
							</select>
							
							<%-- 등록 담당자 리스트 출력 부분 --%>
							<span id="mbr_spanList">
								<c:set var="mbrNameListArray" value="${fn:split(sBox.mbrNameList,',')}"/> 
								<c:forEach var="i" items="${sBox.mbrList}" varStatus="idx">
									<span class="mbr_name" mbrUsrId="${i}">${mbrNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>
								</c:forEach>
							</span>
						</td>
					</tr>
				</c:if>
				<%-- 채무불이행 신청권한이 있는 유저만이 신청자 조건으로 검색가능함 --%>
				<c:if test="${sBox.isSessionDebtApplGrn}">
					<tr>
						<th scope="row">신청자</th>
						<td>
							<input type="radio" name="registMbrType" class="noPaymentRegistMbrType" id="registPerson_all" value="ALL" <c:if test="${sBox.registMbrType eq 'ALL'}">checked</c:if> /> <label for="registPerson_all">전체</label>
							<input type="radio" name="registMbrType" class="noPaymentRegistMbrType" id="registPerson_name" value="name" <c:if test="${sBox.registMbrType eq 'name'}" >checked</c:if>/> <label for="registPerson_name" class="hidden-accessible">신청자</label>
							<select id="nopayment_regist_mbr_select" <c:if test="${sBox.registMbrType ne 'name'}">disabled="disabled"</c:if> title="신청자" >
								<option>--선택--</option>
								<c:forEach var="i" items="${debtRegistMbrList}">
									<option value="${i.USR_NM }" <c:if test="${i.MBR_TYPE eq 'N'}">class="mem_type_N"</c:if>  mbrRegistUsrId="${i.MBR_USR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
								</c:forEach>
							</select>
							
							<%-- 신청자 리스트 출력 부분 --%>
							<span id="mbr_regist_spanList">
								<c:set var="mbrRegistNameListArray" value="${fn:split(sBox.mbrRegistNameList,',')}"/> 
								<c:forEach var="i" items="${sBox.registMbrList}" varStatus="idx">
									<span class="mbr_regist_name" mbrRegistUsrId="${i}">${mbrRegistNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_mbr_regist_name" src="${IMG}/common/btn_s_X.gif"></a></span>
								</c:forEach>
							</span>
						</td>
					</tr>
				</c:if>
			</table>
			<div class="search_btn">
				<div>
					<a class="search" href="#">검색</a>
				</div>
			</div>
		</fieldset>
		
		<div class="tableTop" style="margin-bottom:10px;">
			<p class="totalCnt">검색결과 [${total}개]</p>
			<div class="rightTop">
				정렬
				<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자별로 정렬">
					<c:forEach var="i" items="${debtOngoingListOrderConditionTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderCondition eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
				</select>
				<select  id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
					<c:forEach var="i" items="${orderTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderType eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
				</select>
				<span class="noList">
					<label for="listNo">목록갯수</label>
					<select name="rowSize" id="listNo" class="listOrderOption">
						<c:forEach var="i" items="${rowSizeTypeList}">
							<option value="${i.KEY}" <c:if test="${sBox.rowSize eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
						</c:forEach>
					</select>
				</span>
			</div>
		</div>
		</form>
		
		<div class="tableUlWrap">
			<ul>
				<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
				<c:if test="${(fn:length(debtOngoingList) eq 0) or (debtOngoingList eq null) }">
					<div class="noSearch" align="center" style="padding-top: 5px; padding-bottom: 5px;">검색 결과가 없습니다.</div>
				</c:if>
				<c:forEach var="i" items="${debtOngoingList}">
					<li>
						<dl>
							<dt>
							<c:if test="${i.DOC_CD ne null}">

								신청코드 : <a href="#none" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"  data-custId="${i.CUST_ID}"><strong>${i.DOC_CD}</strong></a> <em>|</em>

							</c:if>
							<c:if test="${i.ACPT_ID ne null}">

								접수번호 : <a href="#none" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"  data-custId="${i.CUST_ID}"><strong>${i.ACPT_ID}</strong></a> <em>|</em>

							</c:if>
							<c:if test="${i.APPL_DT_TIME ne null}">	

								신청일 : ${i.APPL_DT_TIME} <em>|</em>

							</c:if>
							<c:if test="${i.CUST_NM ne null}">

								거래처명 : ${i.CUST_NM} <em>|</em>

							</c:if>
							<c:if test="${i.CUST_NO ne null}">

								사업자등록번호 : ${fn:substring(i.CUST_NO,0,3)}-${fn:substring(i.CUST_NO,3,5)}-${fn:substring(i.CUST_NO,5,10)} <em>|</em>

							</c:if>
							<br />							
							<c:if test="${i.CUST_OWN_NM ne null}">

								대표자(채무자) : ${i.CUST_OWN_NM} <em>|</em>

							</c:if>
							<c:if test="${fn:trim(i.CUST_CORP_NO) ne ''}">

								법인등록번호 : ${i.CUST_CORP_NO} <em>|</em>

							</c:if>
							<c:if test="${i.ST_DEBT_AMT ne null}">

								채무금액(원) : ${i.ST_DEBT_AMT}원 <em>|</em>

							</c:if>
							<c:if test="${i.OVER_ST_DT ne null}">

								연체개시일 : ${i.OVER_ST_DT_TIME} <em>|</em>

							</c:if>

							    진행상태 : <span <c:if test="${i.STAT_CODE eq 'AA'}">class="red"</c:if> >${i.STAT}</span> <em>|</em>
							<br />
							<c:if test="${i.CH_STAT_DT ne null}">

								진행상태변경일 : ${i.CH_STAT_DT_TIME} <em>|</em>

							</c:if>
							<c:if test="${i.REG_DUE_DT ne null}">

								등록예정일 : ${i.REG_DUE_DT_TIME} <em>|</em>

							</c:if>
							<c:if test="${i.APPL_USR_NM ne null}">	

								신청자 : ${i.APPL_USR_NM} 

							</c:if>
							
							<c:if test="${i.PAY_AMT ne null}">

							    <em>|</em>  결제금액(원) : ${i.PAY_AMT}원 
	
							</c:if>
							</dt>
						</dl>
						<span class="date">${i.REG_USR_ID} : ${i.UPT_DT}</span>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div class="btmBtnWrap">
			<div class="paging">
				<c:out value="${pcPage}" escapeXml="false" />
			</div>
			<p class="rightBtn"><a id="btn_save_excel" href="#"><img src="${IMG}/common/btn_save.gif" alt="저장" /></a></p>
		</div>
	</section>
	<%-- //content --%>
</div>
<%-- //contentArea --%>

<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>