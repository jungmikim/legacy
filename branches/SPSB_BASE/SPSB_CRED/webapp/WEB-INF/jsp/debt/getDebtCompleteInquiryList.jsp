<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>

<%-- 정렬관련 공통코드 --%>
<c:set var="orderTypeList" value="${result.orderTypeList}"/>

<%-- 목록갯수 공통코드 --%>
<c:set var="rowSizeTypeList" value="${result.rowSizeTypeList}"/>

<%-- 정렬 공통코드 --%>
<c:set var="debtCompleteListOrderConditionTypeList" value="${result.debtCompleteListOrderConditionTypeList}"/>

<%-- 채무불이행 등록 담당자 --%>
<c:set var="debtMbrList" value="${result.debtMbrList}"/>

<%-- 채무불이행 신청 담당자 --%>
<c:set var="debtRegistMbrList" value="${result.debtRegistMbrList}"/>

<%-- 채무불이행 신청서 리스트 --%>
<c:set var="debtCompleteList" value="${result.debtCompleteList}"/>

<%-- 채무불이행 신청서 페이징 --%>
<c:set var="pcPage" value="${result.pcPage}"/>

<%-- 채무불이행 검색 총 갯수 --%>
<c:set var="total" value="${result.total}"/>

<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%><html class="no-js" lang="ko"> <%--<![endif]--%>
<head>
<title>채무불이행등록 &gt; 진행완료 접수문서 | 스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtCompleteInquiryList.css" />

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	  
	/*
	* <pre>
	* 거래처 검색조건 체인지 이벤트 함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014. 05. 22
	*/
	$('.custSearchType').change(function() {
		$('#cust_kwd').val(''); // 거래처 inputbox 초기화
		if($(this).val() == 'business_all') {
			$('#cust_kwd').attr("disabled", "disabled");
		}else{
			$('#cust_kwd').removeAttr("disabled");
		}
	});
	
	/**
	* <pre>
	* 채무불이행 검색 버튼 클릭 이벤트 함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.07
	*/
	$('.search').click(function(){
		if(!$.fn.validate()) return false;
			$('#num').val(1);
			$('form').attr({'action':'${HOME}/debt/getDebtCompleteInquiryList.do', 'method':'POST'}).submit();
	});

	/**
    * <pre>
    *	채무불이행 신청서 정보 Excel Download 함수
    * </pre>
    * @author sungrangkong
    * @since 2014. 04. 30
    */
	$('#btn_save_excel').click(function(){
		if(!$.fn.validate()) return false;
		$('form').attr({'action':'${HOME}/debt/getDebtCompleteInquiryListForExcel.do', 'method':'POST'}).submit();
	});
      
	/**
	* <pre>
	* 채무불이행 진행완료 접수문서 상세 정보 페이지 조회
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.07
	*/
	$('.linkDebt').click(function(e){
		$params = $(this).attr('data-debtApplId');
       	location.href ="${HOME}/debt/getDebtCompleteInquiry.do?debtApplId="+$params;
	});
       
	/**
	* <pre>
	* 채무불이행 정렬 조건을 변경시키면 form을 전송
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.07
	*/
	$('.listOrderOption').change(function(){
		if(!$.fn.validate()) return false;
		$('#num').val(1);
		$('form').attr({'action':'${HOME}/debt/getDebtCompleteInquiryList.do', 'method':'POST'}).submit();
	});

	/**
	* <pre>
	* 채무불이행 진행 상태 에 따른 checkbox CHANGE 이벤트
	* </pre>
	* @author YouKyung Hong
	* @since 2014. 05. 07
	*/
	$('.processStatusType').on('change', function(){        	
       	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $(this).parent().find(':checked').not('[name=processStatusALL]').length;
		
		if($(this).val() == 'ALL' && checkedLen == 5){
			$(this).siblings().attr('checked',false);
		}else if($(this).val() == 'ALL' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       	}else if($(this).val() != 'ALL' && checkedLen != 5){
       		$(this).siblings().eq(0).attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 5){
       		$(this).siblings().eq(0).attr('checked',true);
       	}
	});
        
	/**
	* <pre>
	* 채무불이행 등록담당자 RADIO 박스 변경에 따른 CHANGE 이벤트
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 29
	*/
	$('.debtMbrType').change(function(){
		if($(this).val() == 'ALL'){
			$("#nopayment_mbr_select").attr('disabled','disabled');
			$("#mbr_spanList > span").remove();
			$("#mbr_list").val('');
		}else{
       		$("#nopayment_mbr_select").removeAttr('disabled');
       	}
	});
	
	/**
	* <pre>
	* 채무불이행 신청자 RADIO 박스 변경에 따른 CHANGE 이벤트
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.07
	*/
	$('.registMbrType').change(function(){
		if($(this).val() == 'ALL'){
			$("#nopayment_reg_mbr_select").attr('disabled','disabled');
			$("#reg_mbr_spanList > span").remove();
			$("#reg_mbr_list").val('');
		}else{
       		$("#nopayment_reg_mbr_select").removeAttr('disabled');
       	}
	});
        
    /**
     * <pre>
     *	사업자등록번호 붙여넣기 할 때 '-' 삭제하기
     * </pre>
     * @author 백원태
     * @since 2014. 12. 15
     */
     $('#cust_kwd').change(function(){
    	 if(($('#business_no').attr('checked')) || ($('#company_corp_no').attr('checked'))){
    	 	$('#cust_kwd').attr('value', $('#cust_kwd').attr('value').replace(/-/g, '')); 
    	 }
     });
     
     
	/**
	* <pre>
	*	채무불이행 등록 담당자 선택 시 이벤트 함수
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	$('#nopayment_mbr_select').change(function(){

		// [1] 첫번째 option 값이 설정되었을 경우 검증
		if($(this).find('option').is(':first:selected')){
			return false;
		}
     	   
		// [2] 이미 선택된 담당자인지 체크함
		$mbrUsrId = $(this).find('option:selected').attr('data-mbrUsrId');
		$checkBTF = true;
		$("#mbr_spanList > span").each(function(e){
			if($(this).attr('data-mbrUsrId') == $mbrUsrId){
				alert("이미 선택된 담당자 입니다.");
				$checkBTF = false;
			}
		});
		if(!$checkBTF){
			return false;
		}
     	   
		// [3] 10명이상 담당자가 늘어난 경우 담당자 추가 제한함
		if($("#mbr_spanList").children().length + 1 > 10){
			alert("채무불이행 등록 담당자 검색은 10명 이상 할 수 없습니다.\n다수의 담당자를 검색하시고 싶은 고객은 [전체]선택을 통해 검색해주세요.");
			return false;
		}
     	   
		// [4] 검증 통과 후 , 담당자 명 설정 표기함
		$data = '<span class="mbr_name" data-mbrUsrId="' + $(this).find('option:selected').attr('data-mbrUsrId') + '">' + $(this).val() + '&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>';
		$('#mbr_spanList').append($data);
     	   
		// [5] HIDDEN TYPE 의 INPUT BOX에 데이터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			$("#mbr_spanList > span").each(function(index){
				$mbr_list += $.trim($(this).attr('data-mbrUsrId'));
				$mbr_name_list += $.trim($(this).text());
				if((index+1) != $("#mbr_spanList").children().length){
					$mbr_list += ',';
					$mbr_name_list += ',';
				}
			});
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
     	   
		// [6] 담당자 선택 SELECT TAG 초기화 실시함
		$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
	});
         
	/*
	* <pre>
	*   채무불이행 등록 담당자 X 표시 클릭 이벤트 함수
	* </pre>
	* @author sungrangkong 
	* @since 2014. 04. 30
	*/
	$(document).on('click','.btn_remove_mbr_name',function(e){
		// [1] 해당 등록담당자 ELEMENT를 삭제한다.
		$(this).parent().parent().remove();
		
		// [2] 모든 담당자 삭제시 자동 [전체] 메뉴가 선택된다.
		if($("#mbr_spanList").children().length == 0){
			$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
			$('#nopayment_mbr_select').attr('disabled', true);
			$("#person_name").attr('checked',false);
			$("#person_all").attr('checked','checked');
			$("#mbr_list").val('');
		}
		
		//[3] FORM ACTION 파라미터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			   $("#mbr_spanList > span").each(function(index){
				   $mbr_list += $.trim($(this).attr('data-mbrUsrId'));
				   $mbr_name_list += $.trim($(this).text());
				   if((index+1) != $("#mbr_spanList").children().length){
					   $mbr_list += ',';
					   $mbr_name_list += ',';
				   }
			   });
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
	});
	
	/**
	* <pre>
	* 채무불이행 등록 담당자 선택 시 이벤트 함수
	* </pre>
	* @author YouKyung Hong 
	* @since 2014.05.08
	*/
	$('#nopayment_reg_mbr_select').change(function(){

		// [1] 첫번째 option 값이 설정되었을 경우 검증
		if($(this).find('option').is(':first:selected')){
			return false;
		}
     	   
		// [2] 이미 선택된 담당자인지 체크함
		$mbrUsrId = $(this).find('option:selected').attr('data-regMbrId');
		$checkBTF = true;
		$("#reg_mbr_spanList > span").each(function(e){
			if($(this).attr('data-regMbrId') == $mbrUsrId){
				alert("이미 선택된 담당자 입니다.");
				$checkBTF = false;
			}
		});
		if(!$checkBTF){
			return false;
		}
     	   
		// [3] 10명이상 담당자가 늘어난 경우 담당자 추가 제한함
		if($("#reg_mbr_spanList").children().length + 1 > 10){
			alert("채무불이행 신청자 검색은 10명 이상 할 수 없습니다.\n다수의 신청자를 검색하시고 싶은 고객은 [전체]선택을 통해 검색해주세요.");
			return false;
		}
     	   
		// [4] 검증 통과 후 , 담당자 명 설정 표기함
		$data = '<span class="regMbrName" data-regMbrId="' + $(this).find('option:selected').attr('data-regMbrId') + '">' + $(this).val() + '&nbsp;<a href="#none"><img class="btn_remove_reg_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>';
		$('#reg_mbr_spanList').append($data);
     	   
		// [5] HIDDEN TYPE 의 INPUT BOX에 데이터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#reg_mbr_spanList").children().length > 0){
			$("#reg_mbr_spanList > span").each(function(index){
				$mbr_list += $.trim($(this).attr('data-regMbrId'));
				$mbr_name_list += $.trim($(this).text());
				if((index+1) != $("#mbr_spanList").children().length){
					$mbr_list += ',';
					$mbr_name_list += ',';
				}
			});
		}
		$("#reg_mbr_list").val($mbr_list);
		$("#reg_mbr_name_list").val($mbr_name_list);
     	   
		// [6] 담당자 선택 SELECT TAG 초기화 실시함
		$("#nopayment_reg_mbr_select option:eq(0)").attr("selected", "selected");
	});
         
	/*
	* <pre>
	* 채무불이행 등록 신청자 X 표시 클릭 이벤트 함수
	* </pre>
	* @author YouKyung Hong 
	* @since 2014.05.08
	*/
	$(document).on('click','.btn_remove_reg_mbr_name',function(e){
		// [1] 해당 등록담당자 ELEMENT를 삭제한다.
		$(this).parent().parent().remove();
		
		// [2] 모든 담당자 삭제시 자동 [전체] 메뉴가 선택된다.
		if($("#reg_mbr_spanList").children().length == 0){
			$("#nopayment_reg_mbr_select option:eq(0)").attr("selected", "selected");
			$('#nopayment_reg_mbr_select').attr('disabled', true);
			$("#reg_person_name").attr('checked',false);
			$("#reg_person_all").attr('checked','checked');
			$("#reg_mbr_list").val('');
		}
		
		//[3] FORM ACTION 파라미터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#reg_mbr_spanList").children().length > 0){
			   $("#reg_mbr_spanList > span").each(function(index){
				   $mbr_list += $.trim($(this).attr('data-regMbrId'));
				   $mbr_name_list += $.trim($(this).text());
				   if((index+1) != $("#reg_mbr_spanList").children().length){
					   $mbr_list += ',';
					   $mbr_name_list += ',';
				   }
			   });
		}
		$("#reg_mbr_list").val($mbr_list);
		$("#reg_mbr_name_list").val($mbr_name_list);
	});

});

/**
 * <pre>
 *   FORM 전송 직전 검증 함수
 * </pre>
 * @author sungrangkong
 * @since 2014. 04. 30 
 */
 $.fn.validate = function() {
	
	$bTF = true; // 검증용 Flag 변수
	 
	// 거래처 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	if(!$("#business_all").is(':checked') && $.trim($("#cust_kwd").val()) == ''){
		alert("[" + $(".custSearchType:checked").next().text() + "] 검색어를 입력해주세요");
		$("#cust_kwd").focus();
		return false;
	}
	
	// 거래처 사업자등록번호 선택할 경우 숫자를 검증
	if($('.custSearchType:checked').attr('id') == 'business_no' || $('.custSearchType:checked').attr('id') == 'company_corp_no') {
		 if(!typeCheck('numCheck',$.trim($('#cust_kwd').val()))){
			alert($(".custSearchType:checked").attr('title') + '는 숫자만 입력가능합니다.');
			$('#cust_kwd').focus();
			return false;
		 }
	}
	
	// 진행상태는 반드시 선택되어야 함
	if($('.processStatusType:checked').length==0) {
		alert("진행상태를 선택해주세요.");
		return false;
	}
	
	return $bTF;
 };
</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<%-- Top Area End --%>

<%-- contentArea --%>
<div class="contentArea sub01"><%-- menu별 이미지 class sub00 --%>
	<%-- content --%>
	<section id="content">
		<h2 class="tit_me03_0103">진행완료 접수문서</h2>
		<p class="tit_me03_0103_under">신청 및 진행이 모두 완료된 문서의 상세내역을 확인합니다.</p>

		<form name="debtInquiryListFrm" id="debtInquiryListFrm" autocomplete="off" >
		
		<%-- FORM HIDDEN AREA START --%>
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/debt/getDebtCompleteInquiryList.do">
		
		<%-- 채무불이행 등록담당자 --%>
		<input type="hidden" name="mbrList" id="mbr_list" value="${sBox.mbrList}" />
		<input type="hidden" name="mbrNameList" id="mbr_name_list" value="${sBox.mbrNameList}" />
		<%-- 채무불이행 신청자 --%>
		<input type="hidden" name="registMbrList" id="reg_mbr_list" value="${sBox.registMbrList}" />
		<input type="hidden" name="registMbrNameList" id="reg_mbr_name_list" value="${sBox.registMbrNameList}" />
		<%-- FORM HIDDEN AREA END --%>
		
		<fieldset class="searchBox">
			<legend>신청서 조회</legend>
			<table summary="채무불이행 진행완료 접수문서 조회 테이블로, 검색조건인 조회기간,거래처,진행상태,등록담당자,신청자로 구성되어있습니다.">
			<caption>신청서 조회 </caption>
				<colgroup>
					<col width="20%" />
					<col width="80%" />
				</colgroup>
				<tr>
					<th scope="row">조회기간</th>
					<td>
						<select name="periodConditionType" style="height: 18px; margin-bottom: 2px;">
							<option value="APPL_DT">신청일</option>
						</select>
						<input type="text" name="periodStDt" id="period_st_dt" class="firstDate periodDate" title="신청일 시작조건" value="${sBox.periodStDt}" readonly="readonly" /><span> ~ </span>
						<input type="text" name="periodEdDt" id="period_ed_dt" class="lastDate periodDate" title="신청일 종료조건" value="${sBox.periodEdDt}" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<th scope="row">거래처</th>
					<td class="block">
						<input type="radio" class="custSearchType" name="custSearchType" value="business_all" id="business_all" <c:if test="${sBox.custSearchType eq 'business_all' }">checked</c:if>/> <label for="business_all">전체</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="business_no" id="business_no" title="사업자등록번호"<c:if test="${sBox.custSearchType eq 'business_no' }">checked</c:if>/> <label for="business_no">사업자등록번호</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_name" id="company_name" <c:if test="${sBox.custSearchType eq 'company_name' }">checked</c:if>/> <label for="company_name">거래처명</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_owner_name" id="company_owner_name" <c:if test="${sBox.custSearchType eq 'company_owner_name' }">checked</c:if>/> <label for="company_owner_name">대표자(채무자)</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_corp_no" id="company_corp_no" title="법인등록번호" <c:if test="${sBox.custSearchType eq 'company_corp_no' }">checked</c:if>/> <label for="company_corp_no">법인등록번호</label> 
						<input type="text" id="cust_kwd" name="custKwd" class="txtInput" <c:if test="${sBox.custSearchType eq 'business_all'}">disabled="disabled"</c:if> style="width:110px" value="${sBox.custKwd}" maxlength="70"/>
					</td>
				</tr>
				<tr>
					<th scope="row">진행상태</th>
					<td class="inlineBlock">
						<input type="checkbox" class="processStatusType" name="processStatusALL" value="ALL" id="condition01" <c:if test="${sBox.processStatusALL eq 'ALL'}">checked</c:if> /> <label for="condition01">전체</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AU" id="condition02" <c:if test="${sBox.processStatusAU eq 'AU'}">checked</c:if> /> <label for="condition02">신청불가</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AC" id="condition03" <c:if test="${sBox.processStatusAC eq 'AC'}">checked</c:if> /> <label for="condition03">신청취소</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="CR" id="condition04" <c:if test="${sBox.processStatusCR eq 'CR'}">checked</c:if> /> <label for="condition04">해제완료</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="RU" id="condition05" <c:if test="${sBox.processStatusRU eq 'RU'}">checked</c:if> /> <label for="condition05">등록불가</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="KF" id="condition06" <c:if test="${sBox.processStatusKF eq 'KF'}">checked</c:if> /> <label for="condition06">심사과실발생</label>
					</td>
				</tr>
				
				<%-- 채무불이행 신청권한이 있는 유저만이 등록담당자 조건으로 검색가능함 --%>
				<c:choose>
				<c:when test="${sBox.isSessionDebtApplGrn}">
					<tr>
						<th scope="row">등록담당자</th>
						<td>
							<input type="radio" name="debtMbrType" class="debtMbrType" id="person_all" value="ALL" <c:if test="${sBox.debtMbrType eq 'ALL'}">checked</c:if> /> <label for="person_all">전체</label>
							<input type="radio" name="debtMbrType" class="debtMbrType" id="person_name" value="name" <c:if test="${sBox.debtMbrType eq 'name'}" >checked</c:if>/> <label for="person_name" class="hidden-accessible">등록 담당자</label>
							<select id="nopayment_mbr_select" <c:if test="${sBox.debtMbrType ne 'name'}">disabled="disabled"</c:if> title="등록 담당자" >
								<option>--선택--</option>
								<c:forEach var="i" items="${debtMbrList}">
									<option value="${i.USR_NM}" <c:if test="${i.MBR_TYPE eq 'N'}">class="mem_type_N"</c:if> data-mbrUsrId="${i.MBR_USR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
								</c:forEach>
							</select>
							
							<%-- 등록 담당자 리스트 출력 부분 --%>
							<span id="mbr_spanList">
								<c:set var="mbrNameListArray" value="${fn:split(sBox.mbrNameList,',')}"/> 
								<c:forEach var="i" items="${sBox.mbrList}" varStatus="idx">
									<span class="mbr_name" data-mbrUsrId="${i}">${mbrNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>
								</c:forEach>
							</span>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<input type="hidden" name="debtMbrType" value="none" />
				</c:otherwise>
				</c:choose>
				<tr>
					<th scope="row">신청자</th>
					<td>
						<input type="radio" name="registMbrType" class="registMbrType" id="reg_person_all" value="ALL" <c:if test="${sBox.registMbrType eq 'ALL'}">checked</c:if> /> <label for="reg_person_all">전체</label>
						<input type="radio" name="registMbrType" class="registMbrType" id="reg_person_name" value="name" <c:if test="${sBox.registMbrType eq 'name'}" >checked</c:if>/> <label for="reg_person_name" class="hidden-accessible">등록 담당자</label>
						<select id="nopayment_reg_mbr_select" <c:if test="${sBox.registMbrType ne 'name'}">disabled="disabled"</c:if> title="신청자" >
							<option>--선택--</option>
							<c:forEach var="i" items="${debtRegistMbrList}">
								<option value="${i.USR_NM}" <c:if test="${i.MBR_TYPE eq 'N'}">class="mem_type_N"</c:if> data-regMbrId="${i.MBR_USR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
							</c:forEach>
						</select>
						
						<%-- 신청자 리스트 출력 부분 --%>
						<span id="reg_mbr_spanList">
							<c:set var="regMbrNameListArray" value="${fn:split(sBox.registMbrNameList,',')}"/> 
							<c:forEach var="i" items="${sBox.registMbrList}" varStatus="idx">
								<span class="regMbrName" data-regMbrId="${i}">${regMbrNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_reg_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>
							</c:forEach>
						</span>
					</td>
				</tr>
			</table>
			<div class="search_btn">
				<div>
					<a class="search" href="#">검색</a>
				</div>
			</div>
		</fieldset>
		
		<div class="tableTop" style="margin-bottom: 10px;">
			<p class="totalCnt">검색결과 [${total}개]</p>
			<div class="rightTop">
				정렬
				<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자별로 정렬">
					<c:forEach var="i" items="${debtCompleteListOrderConditionTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderCondition eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
				</select>
				<select  id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
					<c:forEach var="i" items="${orderTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderType eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
				</select>
				<span class="noList">
					<label for="listNo">목록갯수</label>
					<select name="rowSize" id="listNo" class="listOrderOption">
						<c:forEach var="i" items="${rowSizeTypeList}">
							<option value="${i.KEY}" <c:if test="${sBox.rowSize eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
						</c:forEach>
					</select>
				</span>
			</div>
		</div>
		</form>

		<div class="tableUlWrap">
			<ul>
				<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
				<c:if test="${(fn:length(debtCompleteList) eq 0) or (debtCompleteList eq null) }">
					<div class="noSearch" align="center" style="padding-top: 5px; padding-bottom: 5px;">검색 결과가 없습니다.</div>
				</c:if>
				<c:forEach var="i" items="${debtCompleteList}">
					<li>
						<dl>
							<dt>
								신청코드 : <a href="#none" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"><strong>${i.DOC_CD}</strong></a> <em>|</em>
								접수번호 : <a href="#none" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"><strong>${i.ACPT_ID}</strong></a> <em>|</em>
								
							<c:if test="${i.REG_DT ne null}">

								신청일 : ${i.REG_DT} <em>|</em>

							</c:if>
							<c:if test="${i.CUST_NM ne null}">

								거래처명 : ${i.CUST_NM} <em>|</em>

							</c:if>
							<c:if test="${i.CUST_NO ne null}">

								사업자등록번호 : ${fn:substring(i.CUST_NO,0,3)}-${fn:substring(i.CUST_NO,3,5)}-${fn:substring(i.CUST_NO,5,10)} <em>|</em>

							</c:if>
							<br />
							<c:if test="${i.CUST_OWN_NM ne null}">

								대표자(채무자) : ${i.CUST_OWN_NM} <em>|</em>

							</c:if>
							<c:if test="${fn:trim(i.CUST_CORP_NO) ne ''}">

								법인등록번호 : ${i.CUST_CORP_NO} <em>|</em>

							</c:if>
							<c:if test="${i.ST_DEBT_AMT ne null}">

								채무금액(원) : ${i.ST_DEBT_AMT}원 <em>|</em>

							</c:if>
							<c:if test="${i.STAT ne null}">

							     진행상태 : ${i.STAT} <em>|</em>
	
							</c:if>
							<br />
							<c:if test="${i.CH_STAT_DT_TIME ne null}">

								 진행상태변경일 : ${i.CH_STAT_DT_TIME} <em>|</em>

							</c:if>
							<c:if test="${i.APPL_USR_NM ne null}">

							     신청자 : ${i.APPL_USR_NM}<em>|</em>

							</c:if>
							
							<c:if test="${i.PAY_AMT ne null}">

							     결제금액(원) : ${i.PAY_AMT}원 
	
							</c:if>
							
							</dt>
						</dl>
						<span class="date">${i.REG_USR_ID} : ${i.UPT_DT}</span>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div class="btmBtnWrap">
			<p class="leftBtn btnWrap"></p>
			<div class="paging">
				<c:out value="${pcPage}" escapeXml="false" />
			</div>
			<p class="rightBtn"><a id="btn_save_excel" href="#"><img src="${IMG}/common/btn_save.gif" alt="저장" /></a></p>
		</div>
	</section>
	<%-- //content --%>
</div>
<%-- //contentArea --%>

<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>