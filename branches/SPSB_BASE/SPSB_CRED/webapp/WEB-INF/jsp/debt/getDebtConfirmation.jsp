<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="result" value="${result}" />
<c:set var="debtBox" value="${result.debtBox}" />
<c:set var="debentureList" value="${result.debentureList}" />
<c:set var="prfFileList" value="${result.prfFileList}" />

<!DOCTYPE html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>채무불이행등록 &gt; 채무불이행 신청 | 스마트채권 - 채권관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtConfirmation.css" />
<link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css">

<style type="text/css">
li.Apply_slide span.slideSpan a {
	text-decoration: none;
}
li.Apply_slide a {
	text-decoration: none;
}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/common/certX.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitConfig.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitObject.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$.datepicker.setDefaults({
		    dateFormat: 'yy-mm-dd'
		    ,monthNames: ['년 1월','년 2월','년 3월','년 4월','년 5월','년 6월','년 7월','년 8월','년 9월','년 10월','년 11월','년 12월']
			,monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
		    ,dayNamesMin: ['일', '월', '화', '수', '목', '금', '토']
			,showMonthAfterYear:true
			,changeYear:true
			,changeMonth:true
			,maxDate: "-60D"
		});
		
		$('.applyDate').datepicker();

		
		// STEP03 선택채권 건수 입력
		$('#selected_bond > p').text($('.debnList').find('tr').length + "건");

		// STEP03 채권 없을 시 띄울 화면
		if ($('.debnList').find('tr').length == 0) {
			$('.debnList').find('tbody').append('<tr><td colspan="5">검색결과가 존재하지 않습니다.</td></tr>');
		}

		// 채권이 열 개 이상일 시 height 조정 auto 풀기
		if ($('.debnList').find('tr').length >= 10) {
			$('.debnList').removeAttr('style');
		}
		
		// STEP04 맨 마지막 증빙자료 다음에는 콤마 삭제
		$('#prfArea').find('.default_note').each(function() {
			$(this).find('.comma').last().text('');
		});
		
		/*
		 * <pre>
		 * STEP TITLE CLICK EVENT
		 * </pre>
		 * @author 비즈온 디자인팀
		 */
		$('#menu_slide > li.Apply_slide > .slideSpan > a').click(function() {
			$checkElement = $(this).parent().next();
			if (($checkElement.is('ul')) && ($checkElement.is(':visible'))) {
				$checkElement.slideUp(300);
				$(this).attr("class", "down");
				return false;
			}
			if (($checkElement.is('ul')) && (!$checkElement.is(':visible'))) {
				$checkElement.slideDown(300);
				$(this).attr("class", "up");
				return false;
			}
		});

		/*
		 * <pre>
		 * 모두펼치기 버튼 클릭 EVENT
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.05.10
		 */
		$('#open_all').click(function() {
			$('#menu_slide > li.Apply_slide > ul').slideDown(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class", "up");
			
			$('#open_all').hide();
			$('#close_all').attr('style','font-size:11px');
		});
		 
		/*
		* <pre>
		* 모두닫기 버튼 CLICK EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.14
		*/
		$('#close_all').click(function() {
			$('#menu_slide > li.Apply_slide > ul').slideUp(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class", "down");
			
			$('#close_all').hide();
			$('#open_all').attr('style','font-size:11px');
		});
		
		/*
		* <pre>
		* 타이틀 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		8 @since 2014.05.14
		*/
		$('#menu_slide > li.Apply_slide > span > a').click(function() {
			
			if($(this).hasClass('down')) {
				$('#close_all').hide();
				$('#open_all').removeAttr('style');
			}else {
				if($('.down').length == 0){
					$('#open_all').hide();
					$('#close_all').removeAttr('style');
				}
			}
			
		}); 
		
		/*
		* <pre>
		* 등록사유발생일자
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.10
		*/
		$(document).on('change', '.applyDate', function() {
			$time = new Date($(this).val());
			$time.setDate($time.getDate()+90);
			
			$year = $time.getFullYear();
			$month = ((eval($time.getMonth()+1) < 10) ? "0" : "") + eval($time.getMonth()+1);
			$day = (($time.getDate() < 10 ? "0" : "")) + $time.getDate();
			
			$('#reason_occur_date').text($year+"-"+$month+"-"+$day);
		});
		
		/*
		* <pre>
		* 파일 다운로드 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.10
		*/
		$('.browsing').click(function() {
			
			$('#file_name').val($(this).text());
			$('#file_path').val($(this).attr('data-path'));
			
			$('form').attr({'action':'${HOME}/debt/getDebtPrfFileDownload.do', 'method':'POST'}).submit();
		});
		 
		/*
		* <pre>
		* 서명(공인인증) 버튼 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.10
		*/
		$('#btn_signature').click(function() {
			
			if ($('#overdue_open_date').val() == '') {
				alert("연체개시일을 먼저 설정해주세요.");
				
				if (!$('#btn_date_title').parent().parent().find('ul').is(':visible')) {
					$('#btn_date_title').trigger('click');	
				}
				return false;
			}
			var debtDtResult = true;
			$('.debtBillDt').each(function(e){
				if (Date.parse($('#overdue_open_date').val()) < Date.parse($(this).val())) {
					alert("연체개시일자는 세금계산서 작성일자와 같거나 이후여야 합니다.");
					debtDtResult=false;
					return false;
				}
			});
			
			if(!debtDtResult){
				return false;
			}
			
			
			if (!$('#agree').is(':checked')) {
				alert("약관에 먼저 동의해주세요.");
				
				if (!$('#btn_agree_title').parent().parent().find('ul').is(':visible')) {
					$('#btn_agree_title').trigger('click');
				}
				return false;
			}
			
			if (confirm("공인인증서로 서명하시면 신청이 완료되며, [진행 중 접수문서 조회]에서 확인할 수 있습니다. 심사는 최대 3일 소요됩니다. 등록하시겠습니까?")) {
				// 인증서 출력 호출
				CheckIDN('${sBox.sessionUsrNo}');
			}

		});
		
		
		/*
		* <pre>
		* 공인인증서 신청 버튼 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.10
		*/
		$('#btn_apply_authentication').click(function() {
			if(confirm('공인인증서 신청페이지로 이동하시겠습니까?')){
				location.href ="${smartBillUrl}"+"xMain/cert/app/app.aspx";
			 }else{
			 }
			
		});
		
		/*
		* <pre>
		* 신청서 수정 버튼 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.07.18
		*/
		$('#btn_modifyDebtApply').click(function() {
	       	$paramObj = {
	       		 debtApplId : "${debtBox.DEBT_APPL_ID}",
	   			 custId : "${debtBox.CUST_ID}"
	   	    };
	       	location.href ="${HOME}/debt/getDebtInquiry.do?" + $.param($paramObj);
		});
		
		
		/**
		* <pre>
		* 인증서 등록 성공 CallBack 함수
		* </pre>
		* @author sungrangkong
		* @since 2014. 05. 10.
		*/
		$('#afterSucessCert').click(function() {
			// 인증서 정보 비교
			/* if ($.trim($('#cert_info').val()) != $.trim(certInfo)) {
				alert("인증서가 동일하지 않아 신청이 불가능합니다.");
				return false;
			}else{ */
				//alert("신청하였습니다.");
			//}
			
			$param = {
				overStDt : $('#overdue_open_date').val(),
				debtApplId : "${debtBox.DEBT_APPL_ID}",
				npmCustId : "${debtBox.CUST_ID}"
			};
			
			$.ajax({
				type : "POST",
				url : "${HOME}/debt/insertDebtApplication.do",
				dataType : "json",
				data : $.param($param),
				async : false,
				success : function(msg) {
					$result = msg.model.result;
					
					if($result.REPL_CD == '00000') {
						location.href = '${HOME}/debt/getDebtOngoingInquiry.do?debtApplId=${debtBox.DEBT_APPL_ID}';
					} else {
						alert($result.REPL_MSG);
					}
				},
				error : function(xmlHttpRequest, textStatus, errorThrown) {
					alert("채무불이행 신청 도중 에러 발생 [" + textStatus + "]");
				}
			});
		});
		
	});
	
</script>
 
</head>
<body>
	<%--  Top Area Start --%>
	<jsp:include page="../common/top.jsp" flush="true">
		<jsp:param name="gnb" value="0201" />
	</jsp:include>
	<%-- Top Area End --%>

	<%-- HIDDEN AREA START --%>
	<div style="display: none;">
	<form>
		<input type="text" id="file_name" name="fileNm" />
		<input type="text" id="file_path" name="filePath" />
	</form>
	</div>
	<input type="hidden" id="cert_info" value="${result.CERT_INFO}" />
	<%-- HIDDEN AREA END --%>

	<%-- contentArea --%>
	<div class="contentArea sub07">
		<!-- menu별 이미지 class sub00 -->
		<!-- content -->
		<section id="content">

			<div class="titbox">
				<h2 class="tit_me03_0201">채무불이행 신청</h2>
				<p class="tit_me03_0201_under">채무불이행 등록을 위해 작성한 신청서를 접수할 수 있습니다.</p>
			</div>
			<%-- 채무불이행 신청서 테이블 시작 --%>
			<div class="allView">
				<a href="#none" id="open_all" class="ApplyBtn02" style="font-size: 11px;">모두펼치기</a>
				<a id="close_all" href="#none" class="ApplyBtn02" style="display: none; font-size: 11px;">모두닫기</a>
			</div>
			<div id="defaultApplyWrap">
				<div class="default_Apply">
					<ul id="menu_slide">
						<li class="Apply_slide"><span class="slideSpan">
							<a id="btn_date_title" href="#none" class="up" style="font-family: nanumgothic, dotum, areal; font-size: 11px;">STEP 01. 연체개시일을 입력하세요</a></span>
							<ul>

								<%-- STEP01 시작 --%>
								<li class="Apply_sub">
									<div class="ApplysubWrap">
										<table class="defaultTbl01_01" summary="정보등록 담장자">
											<caption>정보등록 담당자</caption>
											<colgroup>
												<col width="18%">
												<col width="32%">
												<col width="18%">
												<col width="32%">
											</colgroup>
											<tbody>
												<tr>
													<td class="title">연체개시일</td>
													<td class="default_note"><input id="overdue_open_date" type="text" class="applyDate" readonly="readonly" /></td>
													<td class="title">등록사유발생일</td>
													<td id="reason_occur_date" class="default_note"></td>
												</tr>
												<%-- <tr>
													<td class="title">등록사유</td>
													<td class="default_note" colspan="3">${debtBox.DEBT_TYPE_STR}</td>
												</tr> --%>
											</tbody>
										</table>

										<div class="ApplyBtn">
											<em>*</em> 최초 연체가 시작된 연체개시일로부터 60일 지나야 채무불이행 신청을 하실 수 있습니다.
											연체개시일을 입력하시면, 등록사유발생일이 연체개시일 90일 이후로 자동으로 산출됩니다.<br/>
											<em>*</em> 연체개시일자는 세금계산서 작성일자와 같거나 이후여야 합니다.
										</div>
									</div>
								</li>
								<%-- STEP01 끝 --%>

							</ul></li>
						<p class="line"></p>
						<li class="Apply_slide"><span class="slideSpan">
							<a href="#none" class="down" style="font-family: nanumgothic, dotum, areal; font-size: 11px;">STEP 02. 채무불이행 신청서의 내용을 확인하세요.</a></span> 
							<ul style="display: none;">

								<%-- STEP02 시작 --%>
								<li class="Apply_sub">
									<div class="ApplysubWrap">

									<h4 class="normh4">▶ 신청인 정보 </h4>								
									<table class="defaultTbl01_01" summary="신청인 정보" style="margin-bottom: 20px;">
									<caption>회사정보</caption>
										<colgroup>
											<col width="9%">
											<col width="9%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title" rowspan="5">회사 정보</td>
<!-- 
												<td class="title">구분</td>
												<td class="default_note" colspan="3">${debtBox.COMP_TYPE_STR}</td>
											</tr>
											<tr>
 -->
												<td class="title">회사명</td>
												<td>
													${debtBox.COMP_NM} 
												</td>
												<td class="title">사업자등록번호</td>
												<td>
													<c:set var="USR_NO" value="${fn:substring(debtBox.COMP_NO, 0, 3)}-${fn:substring(debtBox.COMP_NO, 3, 5)}-${fn:substring(debtBox.COMP_NO, 5, 13)}" />
													${USR_NO}
												</td>
											</tr>
											<c:choose>
												<c:when test="${debtBox.COMP_TYPE eq 'C'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">법인등록번호</td>
														<td>${debtBox.CORP_NO}</td>
													</tr>
													<tr>
														<td class="title">업종</td>
														<td class="default_note" colspan="3">${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
												<c:when test="${debtBox.COMP_TYPE eq 'I'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">업종</td>
														<td>${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
											</c:choose>
											<tr>
												<td class="title">주소</td>
												<td class="default_note" colspan="3">
												(${debtBox.POST_NO}) ${debtBox.ADDR_1} ${debtBox.ADDR_2}
												</td>
											</tr>											
										</tbody>
									</table>
																
									<table class="defaultTbl01_01" summary="정보등록 담장자" style="margin-bottom: 10px;">
									<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="9%">
											<col width="9%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title" rowspan="4">정보 등록<br/>담당자</td>
												<td class="title">성명</td>
												<td>${debtBox.USR_NM}</td>
												<td class="title">부서</td>
												<td>${debtBox.DEPT_NM}</td>
											</tr>
											<tr>
												<td class="title">직위</td>
												<td class="default_note">${debtBox.JOB_TL_NM}</td>
												<td class="title">전화</td>
												<td class="default_note">
													 ${debtBox.TEL_NO}
												</td>
											</tr>
											<tr>
												<td class="title">휴대폰</td>
												<td class="default_note">
													${debtBox.MB_NO}
												</td>												
												<td class="title">팩스</td>
												<td class="default_note">
													${debtBox.FAX_NO}
												</td>
											</tr>
											<tr>
												<td class="title">이메일</td>
												<td class="default_note" colspan="3">
													${debtBox.EMAIL}
												</td>
											</tr>										
										</tbody>
									</table>
										<br> <br>
										<h4 class="normh4">▶ 채무자 정보</h4>
										<table class="defaultTbl01_01" summary="회사정보 입력테이블">
											<caption>회사정보</caption>
											<colgroup>
												<col width="18%">
												<col width="32%">
												<col width="18%">
												<col width="32%">
											</colgroup>
											<tbody>
<!-- 											
												<tr>
													<td class="title">구분</td>
													<td class="default_note" colspan="3">${debtBox.CUST_TYPE_STR}</td>
												</tr>
 -->
												<tr>
													<td class="title">회사명</td>
													<td class="default_note">
														${debtBox.CUST_NM} 
													</td>
													<td class="title">사업자등록번호</td>
													<td>
														<c:set var="CUST_USR_NO" value="${fn:substring(debtBox.CUST_NO, 0, 3)}-${fn:substring(debtBox.CUST_NO, 3, 5)}-${fn:substring(debtBox.CUST_NO, 5, 13)}" />
														${CUST_USR_NO}
													</td>
												</tr>
												<tr>
													<td class="title">대표자명</td>
													<td <c:if test="${debtBox.CUST_TYPE eq 'I'}">colspan="3"</c:if>>${debtBox.CUST_OWN_NM}</td>
													<c:if test="${debtBox.CUST_TYPE eq 'C'}">
														<td class="title">법인등록번호</td>
														<td>${debtBox.CUST_CORP_NO}</td>
													</c:if>
												</tr>
												<tr>
													<td class="title">통보주소</td>
													<td class="default_note" colspan="3">
														(debtBox.CUST_POST_NO})	${debtBox.CUST_ADDR_1} ${debtBox.CUST_ADDR_2}
													</td>
												</tr>
												<tr>
													<td class="title">채무구분</td>
													<td>${debtBox.DEBT_TYPE_STR}</td>
													<td class="title">채무불이행 금액</td>
													<td>${debtBox.ST_DEBT_AMT}원</td>
												</tr>
											</tbody>
										</table>
										<br> <br>

										<h4 class="normh4">▶ 관련 미수채권</h4>
										<table class="defaultTbl01_02" summary="미수채권 불러오기">
											<caption>미수채권 불러오기</caption>
											<colgroup>
												<col width="4%">
												<col width="24%">
												<col width="24%">
												<col width="24%">
												<col width="24%">
											</colgroup>
											<thead>
												<tr>
													<th scope="col" style="height: 19px;"></th>
													<th scope="col" style="height: 19px;">세금계산서작성일</th>
													<th scope="col" style="height: 19px;">채권합계금액</th>
													<th scope="col" style="height: 19px;">연체일수</th>
													<th scope="col" style="height: 19px;">미수금액</th>
												</tr>
											</thead>
										</table>
										<div class="debnList defaultTbl01_02_Content" style="height: auto;">
											<table summary="미수채권" class="defaultTbl01_02_list">
												<caption>미수채권</caption>
												<colgroup>
													<col width="39px">
													<col width="224px">
													<col width="225px">
													<col width="225px">
													<col width="224px">
												</colgroup>
												<tbody>
													<c:set var="index" value="1" />
													<c:forEach var="debentureList" items="${debentureList}" varStatus="idx">	
														<c:if test="${debentureList.DEBT_APPL_ID ne null}">
															<tr>
																<td style="height: 19px;">${index}</td>
																<td style="height: 19px;">${debentureList.BILL_DT}
																<input type="hidden" class="debtBillDt" value="${debentureList.BILL_DT}"/>
																</td>
																<td class="list_right" style="height: 19px;">${debentureList.SUM_AMT}원</td>
																<td style="height: 19px;">${debentureList.DELAY_DAY}일</td>
																<td class="list_right term last" style="height: 19px;">${debentureList.RMN_AMT}원</td>
															</tr>
														<c:set var="index" value="${index+1}" />
														</c:if>
													</c:forEach>
												</tbody>
											</table>
										</div>

										<div class="defaultSumWrap">
											<table class="defaultSumTable">
												<caption>통계</caption>
												<colgroup>
													<col width="30%">
													<col width="30%">
													<col width="40%">
												</colgroup>
												<tbody>
													<tr>
														<td id="selected_bond" style="height: 25px;">선택채권 :
															<p>3건</p>
														</td>
														<td style="height: 25px;">선택채권 미수금총액 :
															<p>${debtBox.TOT_UNPD_AMT}원</p>
														</td>
														<td style="height: 25px;">채무불이행 금액 :
															<p>${debtBox.ST_DEBT_AMT}원</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

										<h4 class="normh4">▶ 거래증빙자료</h4>
										<table class="defaultTbl01_01" summary="거래증빙자료 테이블" style="margin-bottom:10px;">
											<caption>거래증빙자료</caption>
											<colgroup>
												<col width="18%">
												<col width="82%">
											</colgroup>
											<tbody id="prfArea">
												<tr>
													<td class="title">거래명세서</td>
													<td class="default_note" style="padding: 10px 0 10px 9px;">
														<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
															<c:if test="${prfFile.TYPE_CD eq 'A'}">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-path="${prfFile.FILE_PATH}" style="color:#FF7F10;">${prfFile.FILE_NM}</a>
																</span>
																<span class="comma">, </span>
															</c:if>
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td class="title">세금계산서</td>
													<td class="default_note" style="padding: 10px 0 10px 9px;">
														<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
															<c:if test="${prfFile.TYPE_CD eq 'B'}">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-path="${prfFile.FILE_PATH}" style="color:#FF7F10;">${prfFile.FILE_NM}</a>
																</span>
																<span class="comma">, </span>
															</c:if>
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td class="title">법원판결문</td>
													<td class="default_note" style="padding: 10px 0 10px 9px;">
														<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
															<c:if test="${prfFile.TYPE_CD eq 'C'}">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-path="${prfFile.FILE_PATH}" style="color:#FF7F10;">${prfFile.FILE_NM}</a>
																</span>
																<span class="comma">, </span>
															</c:if>
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td class="title">계약서</td>
													<td class="default_note" style="padding: 10px 0 10px 9px;">
														<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
															<c:if test="${prfFile.TYPE_CD eq 'D'}">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-path="${prfFile.FILE_PATH}" style="color:#FF7F10;">${prfFile.FILE_NM}</a>
																</span>
																<span class="comma">, </span>
															</c:if>
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td class="title">기타증빙자료</td>
													<td class="default_note" style="padding: 10px 0 10px 9px;">
														<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
															<c:if test="${prfFile.TYPE_CD eq 'E'}">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-path="${prfFile.FILE_PATH}" style="color:#FF7F10;">${prfFile.FILE_NM}</a>
																</span>
																<span class="comma">, </span>
															</c:if>
														</c:forEach>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									
									<%-- 서비스 이용료 안내 테이블 시작 --%>
									<table class="defaultTbl03" summary="서비스 이용료 안내" style="width: 942px;">
										<caption>서비스 이용료 안내</caption>
										<colgroup>
											<col width="25%">
											<col width="37%">						
											<col width="38%">
										</colgroup>
										<tbody>			
											<tr>
												<td class="title"><p>안내사항</p></td>
												<td class="default_note" colspan="2" rowspan="1">
												<pre class="information_briefing">${result.informationBriefing}</pre>
												<div class="information_briefing_comment" style="color:#dc3603;">
												심사 및 신청취소, 해제 관련 문의 사항은 아래 전화번호로 문의해주시길 바랍니다.<br /> 
							                    KED연체관리서비스 담당자: 02-3215-2432
												</div>
												</td>
											</tr>
										</tbody>
									</table>
									『신용정보의 이용 및 보호에 관한 법률』 등 관련법규 및 약관 위반으로 인하여 발생하는 모든 민·형사상의 책임에 대해 당사가 부담할 것을 서약합니다.
									<br/><br/>
									위 신청인은 약관사항을 준수할 것을 서약하며 KED채무불이행 서비스를 신청합니다.
									<%-- 서비스 이용료 안내 테이블 끝 --%>
								</li>
								<%-- STEP02 끝 --%>
							</ul>
						</li>
						<p class="line"></p>
						<li class="Apply_slide">
							<span class="slideSpan">
								<a id="btn_agree_title" href="#none" class="down" style="font-family: nanumgothic, dotum, areal; font-size: 11px;">STEP 03. 이용약관에 동의해주세요</a>
							</span>
							<ul style="display: none;">

								<%-- STEP03 시작 --%>
								<li class="Apply_sub">
									<div class="ApplysubWrap">
										<table>
											<tr>
												<td align="left" width="940px;" style="border: 1px dashed #B5B5B5;padding: 20px;">
													<pre class="agreeBox" style="height:300px;overflow: scroll;overflow-x:hidden;">
													${result.debtClause }
													</pre>
												</td>
											</tr>
										</table>
										<div class="ApplyListWrap">
											<div class="ApplyList_left">
												<input type="checkbox" id="agree" /> <label for="agree">이용
													약관에 동의합니다.</label>
											</div>
										</div>
									</div>
								</li>
								<%-- STEP03 끝 --%>

							</ul></li>
						<p class="line"></p>
						<li class="Apply_slide">
							<a href="#none"><span class="fixedSpan" style="font-family: nanumgothic, dotum, areal; font-size: 11px;">STEP 04. 공인인증서를 이용하여 신청을 완료해 주세요. 심사는 최대 3일 소요됩니다.</span></a>
							<ul style="display: none;">
							</ul></li>
					</ul>
				</div>
			</div>
			<%-- 채무불이행 신청서 테이블 끝 --%>
			<div class="btmBtnWrap">
				<p class="multiBtn">
					<a id="btn_modifyDebtApply" class="ApplyBtn_Type" href="#none">신청서 수정</a>
					<a id="afterSucessCert" class="ApplyBtn_Type" href="#none">서명</a> 
					<!-- <a id="btn_signature" class="ApplyBtn_Type" href="#none">서명(공인인증)</a> 
					<a id="btn_apply_authentication" class="ApplyBtn_Type" href="#none">공인인증서 신청</a> -->
				</p>
			</div>
		</section>
		<%-- //content --%>

	</div>
	<%-- //contentArea --%>

	<%--  footer Area Start --%>
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<%-- footer Area End --%>
</body>
</html>