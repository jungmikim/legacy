<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="commonCode" value="${result.commonCode}"/>
<c:set var="pcPage" value="${result.pcPage}"/>
<c:set var="customerCrdHistoryTodayTotalCount" value="${result.customerCrdHistoryTodayTotalCount}"/>
<c:set var="customerCrdHistoryRecentDayTotalCount" value="${result.customerCrdHistoryRecentDayTotalCount}"/>
<c:set var="customerList" value="${result.customerList}"/>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
//페이지 로딩시 초기화를 위해
	 //$("#cust_kwd").val(""); 
	 //$("#crd_dt_input_txt").val(""); 
	 //$('#business_all').attr('checked',true);				  
	 //$('#crd_dt_all').attr('checked',true);				
	
	 
	  /**
	   * <pre>
	   *   회사명 선택하여 조기경보업체등록
	   * </pre>
	   * @author Jungmi Kim
	   * @since 2015. 07. 06
	   */
	   $('#btn_trans_if').click(function(){
		   
		   if('${sBox.isSessionCustEwAddGrnt}'=='true'){
			   if(!$('.chk_remove_company:checked').size()){
				   alert('등록할 거래처명을 선택해 주세요.');
				   return false;
			   } else {
				   $.fn.setTransCustomer(
						$('.chk_remove_company:checked').map(function(){
						   return $(this).val();
						}).get().join(',')	   
				   );
			   }
		   }else{
			   alert("조기경보 업체 등록 권한이 존재하지 않습니다.\n관리자에게 문의하시기 바랍니다.");
		   }
	   });
	   	  
	   /**
	    * <pre>
	    *   회사명 단일 항목 삭제
	    *     닫기 버튼 클릭 이벤트 함수
	    * </pre>
	    * @author Jong Pil Kim
	    * @since 2013. 08. 22
	    */
	    /* $('.close').click(function(){
	    	$.fn.removeCustomer(
	    		$(this).parent().find('.chk_remove_company').val()
	    	);
	    }); */
	    

	    /**
	     * <pre>
	     *   회사명 검색 함수
	     *     검색 버튼 클릭 이벤트 함수
	     * </pre>
	     * @author Jong Pil Kim
	     * @since 2013. 08. 22
	     */
	     $('.search').click(function(){
	    	 if(!$.fn.validate()) return false;
	    	 $('#num').val(1);
	    	 getList();
	     });
	      
	      /**
	       * <pre>
	       *	회사명 정보 Excel Download 함수
	       * </pre>
	       * @author sungrangkong
	       * @since 2013. 08. 23
	       */
	       $('#btn_save_excel').click(function(){
	    	   if(!$.fn.validate()) return false;
	    	   $('form').attr({'action':'${HOME}/customer/getCustomerListForExcel.do', 'method':'POST'}).submit();
	       });
	       
	        /**
	         * <pre>
	         *	정렬 조건을 변경시키면 form을 전송
	         * </pre>
	         * @author sungrangkong
	         * @since 2013. 08. 30
	         */
	        $('.tblSelect1').change(function(){
	        	if(!$.fn.validate()) return false;
	        	$('#num').val(1);
	        	getList();
	        });
	         
        /**
         * <pre>
         *	사업자등록번호 붙여넣기 할 때 '-' 삭제하기
         * </pre>
         * @author 백원태
         * @since 2014. 12. 15
         */
         $('#cust_kwd').change(function(){
        	 if($('#business_no').attr('checked')){
        	 	$('#cust_kwd').attr('value', $('#cust_kwd').attr('value').replace(/-/g, '')); 
        	 }
         });
         
        /**
  	     * <pre>
  	     *   채권 리스트로 전체채권 조회 [없음 : 전체채권] 
  	     * </pre>
  	     * @author Jungmi Kim
  	     * @since 2015. 04. 14.
  	     */
  	     $('.lnkDebnCnt').live('click',function(){	 
	    		$('#nonpaymentType').val('');
	    		$('#debnSearchType').val("business_no");
  	    	 	$('#searchPeriodType').val("NO_CONDITION");
	  	    	$('#bizKwd').val($(this).siblings('.custNo').val());
	  	    	$('form').attr({'action':'${HOME}/debenture/getDebentureList.do', 'method':'POST'}).submit();
  	     });
  	     
  	    /**
	     * <pre>
	     *  채권 리스트로 미수 조회 [N : 미납] 
	     * </pre>
	     * @author Jungmi Kim
	     * @since 2015. 04. 14.
	     */
	     $('.lnkDebnColCnt').live('click',function(){
	    	 $('#delayDayType').val('Y');
	    	 $('#stDelayDay').val(1);
	    	 $('#nonpaymentType').val('N');
	    	 $('#debnSearchType').val("business_no"); 
	    	 $('#searchPeriodType').val("NO_CONDITION"); 
	    	$('#bizKwd').val($(this).siblings('.custNo').val());
	    	$('form').attr({'action':'${HOME}/debenture/getDebentureList.do', 'method':'POST'}).submit(); 
	     });
	     
	   
	     
     /**
      * <pre>
      *	  신용정보변동일자 선택할 경우
      *    날짜 선택 input box 활성화/비활성화
      * </pre>
      * @author Jong Pil Kim
      * @since 2013. 10. 02
      */
      $('.crdDtType').live('click',function(){
    	  if($(this).is('#crd_dt_input_day')){
    		  $('.crdDtDay').val(90);
    		  $('.crdDtDay').removeAttr('disabled');
    		  $('.crdDtDay').attr('readonly', false);
    	  } else if($(this).is('#crd_dt_today')){
    		  $('.crdDtDay').val(7);
    		  $('.crdDtDay').removeAttr('disabled');
    		  $('.crdDtDay').attr('readonly', true);
    	  } else {
    		  $('.crdDtDay').val('');
    		  $('.crdDtDay').attr('disabled', 'disabled');
    	  }
      });

      /**
       * <pre>
       *   신용정보 변동사항 건수 클릭 이벤트
       * </pre>
       * @author Jong Pil Kim
       * @since 2013. 10. 02.
       */
       $('.lnkCrdHistoryCnt').click(function(){
     	  
     	  // 검색조건 초기화
     	  //$('.custType:first').attr('checked',true);
     	  $('.custSearchType:first').attr('checked',true);
     	  $('.crdLtd:first').attr('checked',true);
     	  //$('.payTermType').attr('checked',true);
     	  $('#cust_kwd').val('');
     	  $('#ewRatingType').val('');
     	  
     	  // 신용정보 변동일자 조건 세팅
     	  if($(this).is('#crd_history_today_cnt')){
     		  $('#crd_dt_today').attr('checked',true);
        	  $('.crdDtDay').val(7).removeAttr('disabled');
        	  $('.crdDtDay').attr('readonly', true);
     	  } else {
     		 $('#crd_dt_input_day').attr('checked',true);
       	  	 $('.crdDtDay').val(90).removeAttr('disabled');
     	  }
     	  $('#num').val(1);
     	  getList();
       });
       
       /**
        * <pre>
        *   회사명 [사업자/회사명] 검색에 따른 검색 키워드 활성/비활성 함수
        * </pre>
        * @author Jong Pil Kim
        * @since 2014. 05. 27.
        */
        $('.custSearchType').on({
        	change : function() {
        		$('#cust_kwd').val('');
        		if($(this).attr('id') != 'business_all') {
        			$('#cust_kwd').attr('disabled', false);
        		} else {
        			$('#cust_kwd').attr('disabled', true);
        		}
        	}
        });
        
        
        getAjaxPage = function(num){
    		 $('#num').val(num);
    		 getList();
    	 };

    	 
  		 /**
  		 * <pre>
  		 *   회사명 등록 이동하는 함수 
  		 * </pre>
  		 * @author Jung Mi Kim
  		 * @since 2015. 03. 30.
  		 */
  		$('#btn_go_customer').click(function() {
  			location.href = '${HOME}/customer/getCustomerForm.do';
  		});
  		 
  		/**
  		* <pre>
  		*   거래처 전체 선택 토글 함수
  		* </pre>
  		* @author KIM GA EUN
  		* @since 2015. 07. 03
  		**/

	    $('#btn_select_all').click(function(event) {
	        if(this.checked) {
	            $('.chk_remove_company').each(function() { 
	                this.checked = true;              
	            });
	        }else{
	            $('.chk_remove_company').each(function() { 
	                this.checked = false;                        
	            });         
	        }
	    });

});


//공통함수 (Ajax) - 조회 List
function getList(){
	 $.ajax({
			url: "${HOME}/customer/getCustomerListAjax.do",
			type: "post",
			dataType: "html",
			data:$("#customerListFrm").serialize(),
			async: true,
			error: function(result){
			},
			success: function(result){
				
				$("#custListDiv").html("");
				$("#custListDiv").html(result);
				
				$("#getPageView").html("");
				var getPage = $("#pcPage").val();
				$("#getPageView").html(getPage);
				
				$("#total").html("");
				$("#total").html($("#totalAjax").val());
				
			}
		}
	); 
}


/**
 * <pre>
 *   조기경보업체 등록  FORM 전송 함수
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 07. 27
 */
$.fn.setTransCustomer = function(customerIdList) {
	 if(!confirm('선택하신 거래처 정보를 \n조기경보 업체 등록하시겠습니까?')) return false; 
	 $('#customer_id_list').val(customerIdList);
	 $('form').attr({'action':'${HOME}/customer/sendEWCusterListIf.do', 'method':'POST'}).submit(); 
};

/**
 * <pre>
 *   FORM 전송 직전 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 10. 02 
 */
 $.fn.validate = function() {
 	
 	// 법인사업자(C), 개인사업자(I) 인경우, 결제조건이 모두 해제 상태에서는 경고창을 띄움
 	/*
	if($(".payTermType:checked").length == 0){
		alert("결제조건을 선택 해주세요.");
		return false;
	}
 	*/
 	
	// 회사명 검색 조건이 전체가 아닐 경우 인풋박스의 공백을 검증
	 if($('.custSearchType:checked').attr('id') != $('.custSearchType:first').attr('id')) {
		 if($.trim($('#cust_kwd').val()) == ""){
			 alert('회사명 검색 키워드를 입력해주세요.');
			 $('#cust_kwd').focus();
			 return false;
		 }
	 }
	 
	// 신용정보변동일자 검색 조건이 전체가 아닐 경우 인풋박스의 공백 및 숫자를 검증
	 if($('.crdDtType:checked').attr('id') != $('.crdDtType:first').attr('id')) {
		 if($.trim($('#crd_dt_input_txt').val()) == ""){
			 alert('신용정보 변동일수를 입력해주세요.');
			 $('#crd_dt_input_txt').focus();
			 return false;
		 } 
		 
		 if(!typeCheck('numCheck',$.trim($('#crd_dt_input_txt').val()))){
			 alert('신용정보 변동일수는 숫자만 입력 가능합니다.');
			 $('#crd_dt_input_txt').focus();
			 return false;
		 }
		 
		 if($('#crd_dt_input_txt').val() >= 100){
			 alert('신용정보 변동일수는 최근 100일자 이전만 조회 가능합니다.');
			 $('#crd_dt_input_txt').focus();
			 return false;
		 }
	 }
	
	// 회사명 사업자등록번호 선택할 경우 숫자를 검증
	if($('.custSearchType:checked').attr('id') == 'business_no') {
		 if(!typeCheck('numCheck',$.trim($('#cust_kwd').val()))){
			 alert('사업자등록번호는 숫자만 입력가능합니다.');
			 $('#cust_kwd').focus();
			 return false;
		 }
	}
	
	return true;
 };
 
 
 /**
  * <pre>
  *  조기경보 - 기업정보 브리핑 팝업
  * </pre>
  * @author Jungmi Kim
  * @since 2015. 6. 22 
  */
 function getBusinessBrifReport(kedCd,custNoParam){
	  if('${sBox.isSessionCustBrifGrnt}'=='true'){
		  if(kedCd!=null || kedCd !=''){
			var compId= "${sBox.compId}";
		  	var srvCd= "${sBox.srvCd}";
		 	window.open("${HOME}/customer/getCustomerBrifForKED.do?compId="+compId+"&bizNo="+custNoParam, "popup", "width=1000, height=700, resizable=no, scrollbars=yes, menubar=no, toobar=no, status=no, location=no, ");
		  }
	  }else{
		  alert("기업 정보 브리핑 권한이 존재하지 않습니다.\n관리자에게 문의하시기 바랍니다.");
	  }
 }
  
  /**
   * <pre>
   *   조기경보 서비스 결제한 회원
   * </pre>
   * @author Jungmi Kim
   * @since 2014. 10. 14 
   */
  function getEwRateReport(custNoParam, ewCd){
	   if('${sBox.isSessionCustEwDetailGrnt}'== 'true'){
		   if(custNoParam!=null || custNoParam !=''){
		  	 if(ewCd!=null || ewCd !=''){
		  		var compId= "${sBox.compId}";
			 	window.open("${HOME}/customer/getEwReportForKED.do?compId="+compId+"&bizNo="+custNoParam, "popup", "width=1000, height=700, resizable=no, scrollbars=yes, menubar=no, toobar=no, status=no, location=no, ");
		  	 }
		  	} 
	   }else{
		   alert("조기경보 상세 권한이 존재하지 않습니다.\n관리자에게 문의하시기 바랍니다.");
	   }
	}
   
   function getLnkCustomer(id){
	   $params = id;
   		location.href ="${HOME}/customer/getCustomer.do?custNo="+$params;
   }
   
   /**
    * <pre>
    *  인터페이스 전송실패시 오류메시지 출력
    * </pre>
    * @author Jungmi Kim
    * @since 2015. 07. 27 
    */
   function getIFErrorMsg(code,msg){
	   if(code!=''&&code!=null){
	   	alert("전송실패 사유: ["+code+"],"+msg);   
	   }else{
	   	alert("전송실패 사유: "+msg);   
	   }
   }
    
    
    /**
     * <pre>
     *  조기경보관리 -KED 페이지 
     * </pre>
     * @author Jungmi Kim
     * @since 2015. 7. 28 
     */
    function getKedCsutManage(){
   	  if('${sBox.sessionAdmYn}'=='Y'){
   			var compId= "${sBox.compId}";
   		 	window.open("${HOME}/customer/getCustomerManagementForKED.do?compId="+compId, "popup", "width=1000, height=700, resizable=no, scrollbars=yes, menubar=no, toobar=no, status=no, location=no, ");
   	  }else{
   		  alert("조기경보 관리 권한이 존재하지 않습니다.");
   	  }
    }
   
   
</script>
</head>
<body>
<div id="Wrapper">
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0101"/>
</jsp:include>
<%-- Top Area End --%>



<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0101"/>
</jsp:include>
<!-- Left Area End -->
	
	<!-- rightWrap -->	
	<div id="rightWrap">
		<div class="right_tit ltit01_0101"><em>거래처조회</em></div>
		
		<p class="content_tit">거래처검색</p>
		
		<!-- tbl01Wrap -->
		<form name="customerListFrm" id="customerListFrm" ><!-- style="margin-top: -14.5px;" -->
		<%-- FORM HIDDEN AREA START --%>
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/customer/getCustomerList.do">
		<%-- <input type="hidden" name="payTermType" id="pay_term_type" value="${sBox.payTermType}" /> --%>
		<input type="hidden" name="customerIdList" id="customer_id_list" value="${sBox.customerIdList}" />
		<input type="hidden" name="debnSearchType" id="debnSearchType" value="" />
		<input type="hidden" name="nonpaymentType" id="nonpaymentType" value="" />
		<input type="hidden" name="bizKwd" id="bizKwd" value="" />
		<input type="hidden" name="delayDayType" id="delayDayType" value="" />
		<input type="hidden" name="stDelayDay" id="stDelayDay" value="" />
		<input type="hidden" name="searchPeriodType" id="searchPeriodType" value="" />
		<%-- FORM HIDDEN AREA END --%>
			
		<div class="tbl01Wrap">
			<table>
			<tbody>
				<tr>
					<th width="200px">거래처</th>
					<td>
						<span class="empty_r20"> <input type="radio" class="custSearchType" name="custSearchType" value="business_all" id="business_all" <c:if test="${sBox.custSearchType eq 'business_all'}">checked="checked"</c:if>/> <label for="all">전체</label></span>
						<span class="empty_r20"> <input type="radio" class="custSearchType" name="custSearchType" value="business_no" id="business_no" <c:if test="${sBox.custSearchType eq 'business_no' }">checked="checked"</c:if>/>  <label for="conum">사업자등록번호</label></span>
						<span> <input type="radio" class="custSearchType" name="custSearchType" value="company_name" id="company_name" <c:if test="${sBox.custSearchType eq 'company_name' }">checked="checked"</c:if>/> <label for="company">회사명</label> 
							   <input type="text" id="cust_kwd" name="custKwd" class="tblInput" style="width:150px" title="회사명입력" value="${sBox.custKwd}" maxlength="70" <c:if test="${sBox.custSearchType eq 'business_all' }">disabled="disabled"</c:if>/> 
						</span>
					</td>
				</tr>
				<tr>
					<th width="200px">신용정보(EW) 변동일</th>
					<td>
						<span class="empty_r20"><input type="radio" name="crdDtType" class="crdDtType" id="crd_dt_all" value="" <c:if test="${empty sBox.crdDtType}">checked="checked"</c:if>/> <label for="all">전체</label></span>
						<span class="empty_r20"><input type="radio" name="crdDtType" class="crdDtType" id="crd_dt_today" value="Z" <c:if test="${sBox.crdDtType eq 'Z'}" >checked="checked"</c:if>/> <label for="today">일주일</label></span>
						<span> <input type="radio" name="crdDtType" class="crdDtType" id="crd_dt_input_day" value="Y" <c:if test="${sBox.crdDtType eq 'Y'}" >checked="checked"</c:if>/>  <label for="lately">최근</label>
							   <input type="text" name="crdDtDay" class="crdDtDay tblInput" style="width:25px" id="crd_dt_input_txt" value="${sBox.crdDtDay}" maxlength="4" <c:if test="${empty sBox.crdDtType}" >disabled="disabled"</c:if> <c:if test="${sBox.crdDtType ne 'Y'}" >readonly="readonly"</c:if>/> 일
						</span>
					</td>
				</tr>
				
				<tr>
					<th width="200px">신용정보(EW) 등급</th>
					<td>
						<select class="tblSelect1" name="ewRatingType" id="ewRatingType">
							<option value="" <c:if test="${empty ewRatingType}">selected="selected"</c:if>>전체</option>
							<c:forEach var="type" items="${ewTypes}" varStatus="idx">
								<option value="${type.code}" <c:if test="${sBox.ewRatingType eq type.code}">selected</c:if> >${type.desc}</option>
		                	</c:forEach>
						</select>
					</td>
				</tr>
			</tbody>
			</table>
		</div>
	
		<!-- tbl01Wrap -->
	
		<!-- btnWrap_r -->		
		<div id="btnWrap_r">
			<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_search">검색</a></div>
		</div>
			
		<div class="linedot_u20"> </div>
		<p class="content_tit">신용정보 변동사항</p>
		
		<!-- tbl01Wrap -->
		<div class="tbl01Wrap">
			<table>
			<tbody>
				<tr>
					<th>
						<div class="txt_bar1_link">
							<ul>
								<li>
									<c:choose>
										<c:when test="${(customerCrdHistoryTodayTotalCount eq 0) or (empty customerCrdHistoryTodayTotalCount)}">
											일주일 ${customerCrdHistoryTodayTotalCount}건
										</c:when>
										<c:otherwise>
											<a class="lnkCrdHistoryCnt" id="crd_history_today_cnt" href="#">일주일 ${customerCrdHistoryTodayTotalCount}건</a>
										</c:otherwise>
									</c:choose>
									|
									<c:choose>
										<c:when test="${(customerCrdHistoryRecentDayTotalCount eq 0) or (empty customerCrdHistoryRecentDayTotalCount)}">
											최근 90일 ${customerCrdHistoryRecentDayTotalCount}건
										</c:when>
										<c:otherwise>
											<a class="lnkCrdHistoryCnt" id="crd_history_recent_cnt" href="#">최근 90일 ${customerCrdHistoryRecentDayTotalCount}건</a>
										</c:otherwise>
									</c:choose>
									
								</li>
							</ul>
						</div>
					</th>
				</tr>
			</tbody>
			</table>
		</div>
		<!-- tbl01Wrap -->
		<br/>
		<div id="tbl_numbtnWrap">
			<table class="tbl_numbtn">
			<tbody>
				<tr>
					<td class="left">
						<div id="btnWrap_l">
							<div class="btn_rbbtn_01" id="btnWrap_l">
								<a id="btn_trans_if" href="#none" class="on transIf">[선택]조기경보업체등록</a>
							</div>
						</div>
					</td>
					
					<td class="right">
						<div id="btnWrap_l">
							<div class="btn_rbbtn_01" id="btnWrap_l">
								<a id="btn_manage_cust" href="#none" class="on" onclick="javascript:getKedCsutManage()">조기경보 관리</a>
							</div>
						</div>
					</td>     
				</tr>
			</tbody>
			</table>
		</div>	
		<!-- content_tit_pr -->
		<div id="content_tit_pr" style="margin-top:10px;">
		
			<div class="content_tit pru40">검색결과  [<span id="total">${result.total}</span>개] </div> 
			<%-- <c:choose> TODO: 추가 권한 작업시 해결 
				<c:when test="${sBox.isSessionCustAddGrn}">
					<a id="btn_select_all" href="#"><img src="${IMG}/common/ico_selectAll.gif" alt="전체선택" /></a>
					검색결과 [<span id="total">${result.total}</span>개]<br/>
				</c:when>
				<c:otherwise>
					<p class="totalCnt">검색결과 [<span id="total"></span>개]</p>
				</c:otherwise>
			</c:choose> --%>
			
			<ul class="select_r1">
				<li>
					<p>정렬
					<select id="order_condition_cust" class="tblSelect1" name="orderConditionByCust" title="일자별로 정렬">
						<c:forEach var="orderConditionTypeList" items="${commonCode.orderConditionTypeList}">
							<option value="${orderConditionTypeList.KEY}" <c:if test="${sBox.orderCondition eq orderConditionTypeList.KEY}">selected</c:if>>${orderConditionTypeList.VALUE}</option>	
						</c:forEach>
					</select>
					</p>
				</li>
				<li>
					<p>
					<select  id="order_type" class="tblSelect1" name="orderType" title="조건별로 정렬">
						<c:forEach var="orderTypeList" items="${commonCode.orderTypeList}">
							<option value="${orderTypeList.KEY}" <c:if test="${sBox.orderType eq orderTypeList.KEY}">selected</c:if>>${orderTypeList.VALUE}</option>	
						</c:forEach>
					</select>
					</p>
				</li>
				<li>
					<p>목록갯수
					<select name="rowSize" id="listNo" class="tblSelect1">
						<c:forEach var="rowSizeTypeList" items="${commonCode.rowSizeTypeList}">
							<option value="${rowSizeTypeList.KEY}" <c:if test="${sBox.rowSize eq rowSizeTypeList.KEY}">selected</c:if>>${rowSizeTypeList.VALUE}</option>	
						</c:forEach>
					</select>
					</p>
				</li>
			</ul>		
		</div>
		</form>
			
		<!-- content_tit_pr -->
		
		<!-- tbl02Wrap -->
		<div id="custListDiv">
			<div class="tbl02Wrap">

				
				<table>
				<tbody>
					<tr>
						<th width="5%" class="first_thl">
						<input type="checkbox" class="chk_remove_debt" title="선택" value="" id="btn_select_all"/>
						</th>
						<th width="25%">거래처명</th>
						<th width="16%">사업자번호</th>
						<th width="15%">신용정보<br>(EW)등급</th>
						<th width="14%">신용정보<br>(EW)변동일</th>
						<th width="10%">전송상태</th>
						<th width="15%">등록일</th>
					</tr>
					<c:if test="${(fn:length(customerList) eq 0) or (customerList eq null) }">
					<tr>
						<td colspan="7" align="center">검색 결과가 없습니다.</td>
					</tr>
					</c:if>

				<c:forEach var="customerList" items="${customerList}">
					<tr>
						<td class="first_tdl" align="center" style="padding-left:10px;">
						<input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_NO}"/>
						</td>
						<td style="text-align:left; padding-left:5px;"><a onclick="getLnkCustomer(${customerList.CUST_ID})" href="#none"><strong>${customerList.CUST_NM}</strong></a>
						<td>
							<c:if test="${(!empty fn:trim(customerList.KED_CD) and customerList.KED_CD ne null)}">
								<a href="#none" onclick="getBusinessBrifReport('${customerList.KED_CD}','${customerList.CUST_NO}')"> <img src="${IMG}/common/btn_cobriefing.gif" alt="기업정보 브리핑" /><br></a>
							</c:if>
							${fn:substring(customerList.CUST_NO,0,3)}-${fn:substring(customerList.CUST_NO,3,5)}-${fn:substring(customerList.CUST_NO,5,10)}
						</td>
						<td>
							<c:if test="${!empty customerList.EW_CD}">
								<a href="#none" onclick="getEwRateReport('${customerList.CUST_NO}','${customerList.EW_CD}')"> <img src="${IMG}/common/btn_ew_${ewDescMap[customerList.EW_RATING].code}.gif" alt="상세" /> </a> 	  <!-- 자체 개발 팝업 출력  -->
							</c:if>
							<c:if test="${empty customerList.EW_RATING or empty customerList.EW_CD}">없음
							</c:if>
						</td>
						<td><fmt:formatDate value="${customerList.ORI_CUST_CRD_DT_LU}" pattern="yyyy-MM-dd" /></td>
						<td>
							<c:if test="${customerList.TRANS eq '0'}"><a onclick="getIFErrorMsg('${customerList.RCODE}','${customerList.REASON}')" href="#none"><strong>전송실패</strong></a></c:if>
							<c:if test="${customerList.TRANS eq '1'}">전송완료</c:if>
							<c:if test="${customerList.TRANS eq '2'}">전송중</c:if>
						</td>
						<%-- <td>${customerList.OWN_NM}</td> --%><!-- TODO:  대표자  -->
						<td><fmt:formatDate value="${customerList.REG_DT}" pattern="yyyy-MM-dd" /></td>
					</tr>
				</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		<!-- tbl02Wrap -->
		
		<!-- page_num_wrap -->
		<div id="page_num_wrap">
			<div class="page_num" id="getPageView">
				<c:out value="${pcPage}" escapeXml="false" />
			</div>
		</div>
		<!-- page_num_wrap -->
	</div>
	<!-- rightWrap -->
	
	<div id="bodyIframe" >
		<iframe src="${HOME}/customer/getCustomerManagementForKED.do?compId=${sBox.sessionUsrNo}"  height="100%"  width="100%" name="makeSesseion" style="display:none;"></iframe>
	</div>

		  
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>
</div> <!-- containerWrap -->
</div>
</body>
</html>