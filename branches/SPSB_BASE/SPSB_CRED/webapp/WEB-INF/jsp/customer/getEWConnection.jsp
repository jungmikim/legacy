<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>



<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
});

</script>
</head>
<body>
<div id="Wrapper">
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0101"/>
</jsp:include>
<%-- Top Area End --%>



<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0103"/>
</jsp:include>
<!-- Left Area End -->
	
	<!-- rightWrap -->	
	<div id="rightWrap">
		<div class="right_tit ltit01_0103"><em>조기경보</em></div>
				
				<p class="content_tit">조기경보 등록관리</p>
				
				<div class="tbl04Wrap">
					<table>
					<tbody>
						<tr>
							<th width="%" class="first_thlwr4">상품명</th>
							<th width="%">사용기간</th>
							<th width="%">건수</th>
							<th width="%">결제일</th>
							<th width="%">서비스시작일</th>
							<th width="%">서비스종료일</th>
						</tr>
						<tr>
							<td class="first_tdlwr4">조기경보 12개월</td>
							<td>12개월</td>
							<td>50건</td>
							<td>2015-04-16</td>
							<td>2015-04-20</td>
							<td>2017-01-30</td>
						</tr>
					</tbody>
					</table>
				</div>
				
				<p class="empty_b20"> </p>
				
				<table class="tbl05Wrap">
					<tbody>
						<tr>
							<td><iframe src="{sBox.kedLinkCompSearchUrl}" width="100%" height="800px" id="InfoSearch" name="InfoSearch" onload="window.scrollTo(0,0);"></iframe></td>
						</tr>				
					</tbody>
				</table>
				
				
			</div>
		

	</div>
	<!-- rightWrap -->
	
	<div id="bodyIframe" >
		<iframe src="${sBox.kedUrl}"  height="100%"  width="100%" name="test" style="display:none;" ></iframe>
	</div>

		  
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>
</div> <!-- containerWrap -->

</body>
</html>