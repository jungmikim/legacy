<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 거래처 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>
<c:set var="customerList" value="${result.customerList}"/>
<input type="hidden" name="pcPage" id="pcPage" value="${result.pcPage}"/> 
<input type="hidden" name="totalAjax" id="totalAjax" value="${result.total}"/> 
<div class="tbl02Wrap">

	
	<table>
	<tbody>
		<tr>
			<th width="5%" class="first_thl">
			<input type="checkbox" class="chk_remove_debt" title="선택" value="" id="btn_select_all"/>
			</th>
			<th width="25%">거래처명</th>
			<th width="16%">사업자번호</th>
			<th width="15%">신용정보<br>(EW)등급</th>
			<th width="14%">신용정보<br>(EW)변동일</th>
			<th width="10%">전송상태</th>
			<th width="15%">등록일</th>
		</tr>
		<c:if test="${(fn:length(customerList) eq 0) or (customerList eq null) }">
		<tr>
			<td colspan="7" align="center">검색 결과가 없습니다.</td>
		</tr>
		</c:if>
	<c:forEach var="customerList" items="${customerList}">
		<tr>
			<td class="first_tdl" align="center" style="padding-left:10px;">
			<input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_NO}"/>
			</td>
			<!-- <input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_ID}"/> -->
			<td style="text-align:left; padding-left:5px;"><a onclick="getLnkCustomer(${customerList.CUST_ID})" href="#none"><strong>${customerList.CUST_NM}</strong></a></td>
			<td>
				<c:if test="${(!empty fn:trim(customerList.KED_CD) and customerList.KED_CD ne null)}">
					<a href="#none" onclick="getBusinessBrifReport('${customerList.KED_CD}','${customerList.CUST_NO}')">  <img src="${IMG}/common/btn_cobriefing.gif" alt="기업정보 브리핑" /><br></a>
				</c:if>
				${fn:substring(customerList.CUST_NO,0,3)}-${fn:substring(customerList.CUST_NO,3,5)}-${fn:substring(customerList.CUST_NO,5,10)}
			</td>
			<td>
				<c:if test="${!empty customerList.EW_CD}">
					<a href="#none" onclick="getEwRateReport('${customerList.CUST_NO}','${customerList.EW_CD}')"> <img src="${IMG}/common/btn_ew_${ewDescMap[customerList.EW_RATING].code}.gif" alt="상세" /> </a> 	  <!-- 자체 개발 팝업 출력  -->
				</c:if>
				<c:if test="${empty customerList.EW_RATING or empty customerList.EW_CD}">없음
				</c:if>
			</td>
			<td><fmt:formatDate value="${customerList.ORI_CUST_CRD_DT_LU}" pattern="yyyy-MM-dd" /></td>
			<td>
				<c:if test="${customerList.TRANS eq '0'}">전송실패</c:if>
				<c:if test="${customerList.TRANS eq '1'}">전송완료</c:if>
				<c:if test="${customerList.TRANS eq '2'}">전송중</c:if>
			</td>
			<%-- <td>${customerList.OWN_NM}</td> --%><!-- TODO:  대표자  -->
			<td><fmt:formatDate value="${customerList.REG_DT}" pattern="yyyy-MM-dd" /></td>
		</tr>
	</c:forEach>
	</tbody>
	</table>
</div>
