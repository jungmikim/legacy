<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 거래처 대량등록 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>거래처 대량 등록 &gt; 거래처 관리 | 스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerMultiForm.css"/>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	/**
	*	엑셀샘플파일 다운로드 함수
	*@author HWAJUNG SON
	*@since 2013.10.02
	*</pre>
	*/
	$("#btn_down_excel").click(function(){
		 $('form').attr({'action':'${HOME}/customer/getCustomerMultiForExcel.do', 'method':'POST'}).submit();
	});
	

	/**
	*	파일 업로드 함수 
	*@author HWAJUNG SON
	*@since 2013.10.08	
	*/
	$("#cust_exc_file").change(function(){
		$.fn.fileupload($('input[type=file]').val().toLowerCase());
	});

});
/**
 * 	엑셀 파일 검증 함수
 *@author HWAJUNG SON
 *@since 2013.10.04
 */
$.fn.validate = function($checkPrm){
	
	$result= true;

	 if( $.trim($checkPrm) != "") {
		 $fileReg =/(xls)$/;
			if(!$fileReg.test($checkPrm)){
				alert("엑셀업로드파일은 xls 형식만 가능합니다.");
				$result = false;	
			}
		}
	 return $result;
};

/**
 * 엑셀 업로드 함수
 *@author HWAJUNG SON
 *@since 2013 .10 .10
 */
 $.fn.fileupload = function($fileName){
	$result = true; 
	
	// 파일 형식 검증
	 if($.fn.validate(splitExtension($fileName))){
		// 10초 후 페이지 이동 
  		setTimeout( function(){
			location.href = '${HOME}/customer/getCustomerMultiHistoryList.do';
	     }, 10000 );
		
 		// 엑셀 업로드
		  $('form').attr({'action':'${HOME}/customer/addTempCustomerMulti.do', 'method':'POST'})
			 .ajaxSubmit({
			 	dataType : "json",
				async : true,
				beforeSend: function() {
					$('#dvLoading').fadeIn(300);
			    },
				success : function(msg){
					$('#dvLoading').fadeOut(3000);
					 if("00000" == msg.REPL_CD){
						location.href = '${HOME}/customer/getCustomerMultiForm.do';
					} else {
						alert(utfDecode(msg.REPL_MSG));
						location.href = '${HOME}/customer/getCustomerMultiForm.do';
					}	 				
				},
				error : function(xmlHttpRequest,textStatus,errorThrown) {
					$('#dvLoading').fadeOut(3000);
					location.href = '${HOME}/customer/getCustomerMultiForm.do';
				},
				uploadProgress: function(event, position, total, percentComplete) {
					$('#dvLoading').show();
				}
			}); 	
	 	}
	
  return $result ;
};
		
</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0103"/>
</jsp:include>
<%-- Top Area End --%>
<%-- contentArea --%>
<div class="contentArea sub02"><%-- menu별 이미지 class sub00 --%>
	<%-- content --%>
	<section id="content">
		<h2 class="tit_sub0103">거래처 대량 등록</h2>
		<p class="under_h2_sub0103">거래처 대량 등록 업로드 진행 상황에 대해 확인 및 관리를 할 수 있습니다.</p>
		<form name="multiFrm" id="multiFrm" class="multiFrm" enctype="multipart/form-data" action="${HOME}/customer/addTempCustomerMulti.do">
		<%-- hidden 값 setting --%>
		<input type="hidden" name="duplCode" value="I"/>
		<%-- loading img --%>
		<img src="${IMG}/common/loading.gif" id="dvLoading" />
		<div class="excel_down">
			<dl>
				<dt>
					<img src="${IMG}/common/dt_excel_down.gif" alt="엑셀 샘플 다운로드 받기"/> 
					<a href="#">
						<img src="${IMG}/common/btn_down_excel.gif" alt="엑셀 다운로드" id="btn_down_excel"/>
					</a>
				</dt>
				<dd>
					<img src="${IMG}/common/dd_customer_down.gif" alt="거래처를 대량으로 등록할 경우에는 사업자 유형에 관계없이 공통된 파일을 사용합니다." />
				</dd>
			</dl>
			<p>
				<a href="#">
					<input type="file" name="custExcFile" id="cust_exc_file" class="custExcFile"/>
					<img src="${IMG}/common/btn_excel_upload.gif" alt="엑셀 업로드" id="btn_excel_upload"/>
				</a>
			</p>
		</div>
		</form>
		<h3><img src="${IMG}/common/h3_how_to_customer_upload.gif" alt="거래처 대량 등록하는 방법" /></h3>
		<ul class="step_upload_customer">
			<li>STEP 01. 엑셀 샘플 다운로드</li>
			<li>STEP 02. 엑셀 입력</li>
			<li>STEP 03. 엑셀 업로드</li>
			<li>STEP 04. 업로드 결과 확인 및 수정</li>
			<li>STEP 05. 거래처 등록 완료</li>
		</ul>
	</section>
	<%-- //content --%>
</div>
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>