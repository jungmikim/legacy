<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 수금업무 조회 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="custCol" value="${result.custCol}" />
<c:set var="debnList" value="${result.debnList}" />
<c:set var="commonCode" value="${result.commonCode}" />
<c:set var="bondMbrList" value="${result.bondMbrList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/popupCol.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerCol.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	/**
	 * <pre>
	 *   체크박스 클릭시 수금 금액 활성화 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30.
	 */
	 $('.chkDebnId').live({
		 click : function(e){
			 if($(this).is(':checked')) {
				if($(this).parent().parent().find('td.last').find('input').length == 0) {
					
					<%-- 수금금액 최대값 설정 --%>
					$tmpColAmt = 0;
					$tmpRmnAmt = parseInt(replaceAll($.trim($(this).parent().siblings('.rmnAmt').text()), ',',''), 10);
					$tmpPriorColAmt = ($(this).parent().siblings('.debnColAmtInput').attr('data-prior-col-amt') != '') ? parseInt($(this).parent().siblings('.debnColAmtInput').attr('data-prior-col-amt'), 10) : 0;
					$tmpPlnColAmt = ($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') ? parseInt(replaceAll($.trim($(this).parent().siblings('.debnPlnColAmt').text()), ',',''), 10) : 0;
					$tmpOtherPlnColAmt = ($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln') == 'Y') ? parseInt($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln-amt'), 10) : 0;
					
					<%-- 수금예정금액이 있으면 수금예정금액과 (미수금액-타수금계획+수금금액) 중 큰 액수가 최대, 수금예정금액이 없으면 [미수금액 (- 타 수금금액) (+ 이전수금금액)] 수금금액이 최대 --%>
					if($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') {
						if(($tmpRmnAmt - $tmpOtherPlnColAmt + $tmpPriorColAmt) < $tmpPlnColAmt) {
							$tmpColAmt = ($tmpRmnAmt + $tmpPriorColAmt - $tmpOtherPlnColAmt);
						} else {
							$tmpColAmt = $tmpPlnColAmt;	
						}
					} else {
						$tmpColAmt = $tmpRmnAmt + $tmpPriorColAmt - $tmpOtherPlnColAmt;
					}
					
					$(this).parent().parent().find('td.last')
					.append(
						'<input type="text" id="col_amt_' + $(this).val() + '" name="colAmt" ' +
							    'class="txtInput colAmt numericMoney required" style="width:85px" title="수금 금액" ' + 
							    'value="' + (addComma($tmpColAmt)) +'" maxlength="12" >'
					); 
				}
			 } else {
				 $(this).parent().parent().find('td.last').empty();
			 }
			 
			<%-- 선택 채권 건수, 선택채권 미수금총액 --%>
		 	$.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T');
		 	$('#debn_cnt').val($('.chkDebnId:checked').length);
		 	$('#selected_debn').text($('.chkDebnId:checked').length + '건');
		 	
		 	<%-- 선택 채권 실수금 총액 --%>
		 	$('#tot_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), $('#selected_tot_col_amt'), '원', 'V'));
			
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		 }
	 });
	 
	/**
	 * <pre>
	 *   수금 금액과 비교하는 로직
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30. 
	 */
	 $('.colAmt').live({
		focus: function(e) {
			$(this).val(replaceAll($(this).val(), ',', ''));
		},
		focusout: function(e) {

			<%-- 공백 검증 --%>
			if($.trim($(this).val()) == '') {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '을 입력해주세요.');
			}
			
			<%-- 숫자 검증 --%>
			if(!typeCheck('numCheck',$.trim($(this).val()))) {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			}
			
			<%-- 입력값 대소비교 --%>
			$colAmt = parseInt(($(this).val().replace(/[^0-9]/g, '')), 10);
			$rmnAmt = parseInt(($(this).parent().siblings('.rmnAmt').text().replace(/[^0-9]/g, '')), 10);
			$priorColAmt = ($(this).parent().attr('data-prior-col-amt') != '') ? parseInt($(this).parent().attr('data-prior-col-amt'), 10) : 0;
			$plnColAmt = ($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') ? parseInt(replaceAll($.trim($(this).parent().siblings('.debnPlnColAmt').text()), ',',''), 10) : 0;
			$otherPlnColAmt = ($.trim($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln')) == 'Y') ? parseInt($.trim($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln-amt')), 10) : 0;
			
			<%-- 타수금계획, 자신의 수금계획 따른 대소비교 --%>
			<%-- 수금예정금액이 있으면 수금예정금액과 (미수금액-타수금계획+수금금액) 중 큰 액수가 최대, 수금예정금액이 없으면 [미수금액 (- 타 수금금액) (+ 이전수금금액)] 수금금액이 최대 --%>
			if($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') {
				if(($rmnAmt + $priorColAmt - $otherPlnColAmt) < $plnColAmt) {
					if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt - $otherPlnColAmt), 'LE')) {
						return $.fn.validateResultAndAlert($(this), '수금금액은 [미수총액 - 타수금계획 = '+ addComma($rmnAmt + $priorColAmt - $otherPlnColAmt) +'원] 이하로 입력되어야 합니다.');
					}
				} else {
					if(!$.fn.compareValue($colAmt, $plnColAmt, 'LE')) {
						return $.fn.validateResultAndAlert($(this), '수금금액은 수금예정금액(' + addComma($plnColAmt) + '원)이하로 입력되어야 합니다.');
					}	
				}
			} else {
				if($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln') == 'N') {
					if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt), 'LE')) {
						return $.fn.validateResultAndAlert($(this), '수금금액은 미수총액('+ addComma($rmnAmt + $priorColAmt) +'원)이하로 입력되어야 합니다.');
					}
				} else {
					if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt - $otherPlnColAmt), 'LE')) {
						return $.fn.validateResultAndAlert($(this), '수금금액은 [미수총액 - 타 수금예정금액 = '+ addComma($rmnAmt + $priorColAmt - $otherPlnColAmt) +'원] 이하로 입력되어야 합니다.');
					}						
				}
			}
			
			<%-- 입력한 값 초기화 --%>
			$(this).val(addComma($colAmt));
			
			<%-- 선택 채권 건수, 선택채권 미수금총액 --%>
		 	$.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T');
		 	$('#debn_cnt').val($('.chkDebnId:checked').length);
		 	$('#selected_debn').text($('.chkDebnId:checked').length + '건');
		 	
		 	<%-- 선택 채권 실수금 총액 --%>
		 	$('#tot_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), $('#selected_tot_col_amt'), '원', 'V'));
			
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		}
	 });
	
	/**
	 * <pre>
	 *   취소 버튼 클릭시 팝업 닫기
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	$('#btn_customer_col_close').live({
		click : function(e){
			self.close();
		}
	});
	 
	/**
	 * <pre>
	 *   체크박스 모두 선택 토글 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#all_check').live({
		click : function(){
			if($(this).is(':checked')) {
				$('.chkDebnId:not(:disabled)').attr('checked', false).trigger('click');
			} else {
				$('.chkDebnId:not(:disabled)').attr('checked', true).trigger('click');
			}
		}
	});
	 
	/**
	 * <pre>
	 *   수금완료/대손처리 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30. 
	 */
	$('.btnColSave').live({
		click : function(e){
			if($('.chkDebnId:not(:disabled):checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			
			if($('#col_stat').val() == 'C') {
				if($('#col_type').val() == 'I') {
					alert('수금완료를 선택한 경우, 수금방식에 "대손처리"를 선택할 수 없습니다.');
					$('#col_type option[value=H]').prop('selected', true);
					return false;
				}
				
			} else if($('#col_stat').val() == 'I') {
				if($('#col_type').val() != 'I') {
					alert('대손처리를 선택한 경우, 수금방식은 "대손처리"를 선택해야 합니다.');
					$('#col_type option[value=I]').prop('selected', true);
					return false;
				}
			}
			
			if(!$.fn.validate($('.chkDebnId:not(:disabled):checked').parent().parent().find('.colAmt'))) return false;
			
			$.fn.addCustomerColNoPlan($('#col_stat').val());
		}
	});
	
	/**
	 * <pre>
	 *   수정완료 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 02. 
	 */
	$('#btn_customer_col_modify').live({
		click : function(e){
			
			if($('.chkDebnId:checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			
			<%-- 상태가 수금완료, 대손처리일 경우 수금방식 검증 --%>
			if($('#col_stat').val() == 'C') {
				if($('#col_type').val() == 'I') {
					alert('상태를 수금완료를 선택한 경우, 수금방식에 "대손처리"를 선택할 수 없습니다.');
					$('#col_type option[value=H]').prop('selected', true);
					return false;	
				}
			} else if($('#col_stat').val() == 'I') {
				if($('#col_type').val() != 'I') {
					alert('상태를 대손처리를 선택한 경우, 수금방식은 "대손처리"를 선택해야 합니다.');
					$('#col_type option[value=I]').prop('selected', true);
					return false;	
				}
			}
			
			if(!$.fn.validate($('.chkDebnId:checked').parent().parent().find('.colAmt'))) return false;
			
			$.fn.modifyCustomerColNoPlan();
		}
	});
	 
	/**
	 * <pre>
	 *   검색 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 07 
	 */
	$('#btn_col_search').live({
		click : function(e){
			$custId = $('#cust_id').val();
			$orderCondition = $('#order_condition').val();
			$orderType = $('#order_type').val();
			$bondMbrId = $('#bond_mbr_id').val();
			$custColSn = $('#cust_col_sn').val();
			$.fn.getDebnListForCustomerColNoPlan($custId, $orderCondition, $orderType, $bondMbrId, $custColSn);
		}
	});
	 
	 /**
	 * <pre>
	 *   상태 셀렉트 박스 변경 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 07. 15 
	 */
	$('#col_stat').live({
		change : function(e){
			if($(this).val() == 'I') {
				$('#col_type option:not([class=irrecType])').hide();
				$('#col_type option[class=irrecType]').show();
				$('#col_type option[value=I]').prop('selected', true);
			} else {
				$('#col_type option:not([class=irrecType])').show();
				$('#col_type option[class=irrecType]').hide();
				$('#col_type option[value=H]').prop('selected', true);
			}
		}
	});
	 
	 
	<%-- 수금 상태에 따라 수금방식 셀렉트박스 옵션 값 정의 --%>
	if($('#col_stat').val() == 'I') {
		$('#col_type option:not([class=irrecType])').hide();
		$('#col_type option[class=irrecType]').show();
		$('#col_type option[value=I]').prop('selected', true);
	} else {
		$('#col_type option:not([class=irrecType])').show();
		$('#col_type option[class=irrecType]').hide();
		$('#col_type option[value=H]').prop('selected', true);
	}
	 
});

<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%> 

/**
 * <pre> 
 *   수금계획 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 19
 * @param $listEl : 검증할 엘리먼트 리스트
 *
 */
$.fn.validate = function($listEl) {
	
	$result = true;
	$zeroFlag = false;
	 
	$.each($listEl, function(index){

		$colAmt = $.trim(replaceAll($(this).val(), ',', ''));
		<%-- 공백 검증 --%>
		if($colAmt == '') {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '를 입력해주세요.');
			return $result;
		}
		
		<%-- 숫자 검증 --%>
		if(!typeCheck('numCheck',$colAmt)) {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			return $result;
		}
		
		<%-- 하나라도 0 이상의 값이 있으면 통과 --%>
		if(parseInt($colAmt, 10) > 0) {
			$zeroFlag = true;
		}
		
		<%-- 입력값 대소비교 --%>
		$colAmt = parseInt(($(this).val().replace(/[^0-9]/g, '')), 10);
		$rmnAmt = parseInt(($(this).parent().siblings('.rmnAmt').text().replace(/[^0-9]/g, '')), 10);
		$priorColAmt = ($(this).parent().attr('data-prior-col-amt') != '') ? parseInt($(this).parent().attr('data-prior-col-amt'), 10) : 0;
		$plnColAmt = ($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') ? parseInt(replaceAll($.trim($(this).parent().siblings('.debnPlnColAmt').text()), ',',''), 10) : 0;
		$otherPlnColAmt = ($.trim($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln')) == 'Y') ? parseInt($.trim($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln-amt')), 10) : 0;
		
		<%-- 타수금계획, 자신의 수금계획 따른 대소비교 --%>
		<%-- 수금예정금액이 있으면 수금예정금액과 (미수금액-타수금계획+수금금액) 중 큰 액수가 최대, 수금예정금액이 없으면 [미수금액 (- 타 수금금액) (+ 이전수금금액)] 수금금액이 최대 --%>
		if($.trim($(this).parent().siblings('.debnPlnColAmt').text()) != '') {
			if(($rmnAmt + $priorColAmt - $otherPlnColAmt) < $plnColAmt) {
				if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt - $otherPlnColAmt), 'LE')) {
					$result = $.fn.validateResultAndAlert($(this), '수금금액은 [미수총액 - 타수금계획 = '+ addComma($rmnAmt + $priorColAmt - $otherPlnColAmt) +'원] 이하로 입력되어야 합니다.');
					return $result;
				}
			} else {
				if(!$.fn.compareValue($colAmt, $plnColAmt, 'LE')) {
					$result = $.fn.validateResultAndAlert($(this), '수금금액은 수금예정금액(' + addComma($plnColAmt) + '원)이하로 입력되어야 합니다.');
					return $result;
				}	
			}
		} else {
			if($(this).parent().siblings('.otherPlnColAmt').attr('data-other-pln') == 'N') {
				if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt), 'LE')) {
					$result = $.fn.validateResultAndAlert($(this), '수금금액은 미수총액(' + addComma($rmnAmt + $priorColAmt) + '원)이하로 입력되어야 합니다.');
					return $result;
				}
			} else {
				if(!$.fn.compareValue($colAmt, ($rmnAmt + $priorColAmt - $otherPlnColAmt), 'LE')) {
					$result = $.fn.validateResultAndAlert($(this), '수금금액은 [미수총액 - 타 수금예정금액 = '+ addComma($rmnAmt + $priorColAmt - $otherPlnColAmt) +'원] 이하로 입력되어야 합니다.');
					return $result;
				}						
			}
		}
	});

	<%-- 0값 검증 --%>
	if(!$zeroFlag) {
		$result = $.fn.validateResultAndAlert('', '수금금액은 모두 0이 될 수 없습니다. ');
		return $result;
	}
	
	return $result;
};

/**
 * <pre> 
 *   <li> 태그 리스트 백그라운드 CSS 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 *
 */
 $.fn.setBackgroundEvenAndOdd = function($targetEl, $subEl, $evenColor, $oddColor) {
	 $targetEl.find($subEl + ':even').css('background-color', $evenColor);
	 $targetEl.find($subEl + ':odd').css('background-color', $oddColor);
 };
		
 /**
  * <pre> 
  *    대소 비교 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 18
  * @param $num1 : 비교할 값 1, $num2 : 비교할 값 2, $operand : 연산자[LE,GE,LT,GT]
  */
  $.fn.compareValue = function($num1, $num2, $operand) {
 	 if($operand == 'LE') {
 		 return ($num1 <= $num2);
 	 } else if($operand == 'GE') {
 		return ($num1 >= $num2);
 	 } else if($operand == 'LT') {
 		return ($num1 < $num2);
 	 } else if($operand == 'GT') {
 		return ($num1 > $num2);
 	 }  
  };
 
/**
 * <pre>
 *   선택한 값 자동 계산 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 * @param $listEl : 계산할 엘리먼트, $targetEl : 결과 세팅 엘리먼트, 
 *        $suffix : 결과 뒤에 추가할 메세지, $valueType : 함수 사용 조건[V, T] 
 */
 $.fn.selectedTargetCalc = function($listEl, $targetEl, $suffix, $valueType){
	 $result = 0;
	 
	 $listEl.each(function() {
		$value = '';
		switch($valueType) {
			case 'V' :
				$value = $(this).val();
				break;
			case 'T' :
				$value = $(this).text();
				break;
		}  
		
		$result += parseInt(($value.replace(/[^0-9]/g, '')), 10);
	 });
	 
	 $targetEl.html(addComma($result) + $suffix);
	 
	 return $result;
 };
	
/**
 * <pre>
 *   경고창 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
 */
 $.fn.validateResultAndAlert = function($el, $message){
	alert($message);
	return false;
 }; 
 
 /**
  * <pre> 
  *   파라미터 구성 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 22
  *
  * @param $targetEl : 수집 대상 엘리먼트
  * @param $selector : 수집 대상 셀렉터(클래스)
  * @param $paramEl : 파라미터 수집 대상 객체
  * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
  * @param $dataSplitStr : 구분자[데이터]
  * @param $rowSplitStr : 구분자[row]
  */
 $.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {
 	
	$result = '';
	
	$.each($targetEl, function() {
		if($(this).hasClass($selector.toString())) {
			$tmpStr = '';
			for(var i = 0 ; i < $paramEl.length; i++) {
				if($tmpStr != '') {
					$tmpStr += $dataSplitStr;
				}
				$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
			}		
			
			if($result != '') {
				$result += $rowSplitStr;
			}
			$result += $tmpStr;
		}
	});
 	
	return $result;
 };
 
 /**
  * <pre> 
  *   리스트가 없을 경우
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 17
  * @param $target : 리스트 구성 영역 Element
  * @param $noDataEl : 리스트를 구성할 Data
  */
 $.fn.reloadNoDataList = function($target, $noDataEl) {
 	 $htmlStr = '';
 	 $target.empty().append($noDataEl);
 	$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
 };
 
/**
 * <pre> 
 *   채권 리스트를 재구성 하는 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 07
 *
 * @param $target : 리스트 구성 영역 Element
 * @param $listEl : 리스트를 구성할 Data
 * @param $paramEl : sBox 파라미터 객체
 */
$.fn.reloadDebnList = function($target, $listEl, $paramEl) {
	$htmlStr = '';
 	
 	$.each($listEl, function() {
 		
		$htmlStr += 
			'<tr>' + 
				'<td class="checkDebn">' +
					'<input type="checkbox" class="chkDebnId ' + ((this.CUST_COL_SN == null || this.CUST_COL_SN == '') ? 'insertDebn' : '') 
							+ (((this.CUST_COL_SN != null && this.CUST_COL_SN != '') && (this.CUST_COL_SN == $('#cust_col_sn').val())) ? ' updateDebn' : '') 
							+ '" id="chk_' + this.DEBN_ID + '" value="' + this.DEBN_ID  + '"' +
					' />' +
				'</td>' +
				'<td class="bondMbrNm">' + (this.BOND_MBR_NM)  + '</td>' + 
				'<td class="billDt">' + this.BILL_DT + '</td>' + 
				'<td class="list_right sumAmt">' + this.SUM_AMT + '</td>' + 
				'<td class="list_right rmnAmt">' + this.RMN_AMT + '</td>' +
				'<td class="list_right otherPlnColAmt" data-other-pln="' + this.OTHER_PLN_FLAG  + '" data-other-pln-amt="' + this.PLN_COL_AMT_OTHER + '" >' +
					this.PLN_COL_AMT_OTHER_COMMA +
				'</td>' + 
				(
					($paramEl.custColSn != null && $paramEl.custColSn != '') 
					?   ('<td class="list_right debnPlnColAmt" >' + 
							(this.PLN_COL_AMT_COMMA != null ? this.PLN_COL_AMT_COMMA : '') + 
						'</td>') 
					: '') +
				'<td class="list_left last debnColAmtInput" data-prior-col-amt="' + (this.COL_AMT != null ? this.COL_AMT : '' ) + '">';

				if (($paramEl.custColSn != null) && ($paramEl.custColSn != '') && (this.CUST_COL_SN == $paramEl.custColSn )) {
					$htmlStr += '<input type="text" id="col_amt_' + this.DEBN_ID + '" name="colAmt" class="txtInput colAmt required numericMoney" style="width:85px" title="수금금액" value="' + this.COL_AMT_COMMA + '" maxlength="12" />';
				}
		$htmlStr += '</td>' + 
				'</tr>';
 	});
 	
 	$('#all_check').attr('checked', false);
 	$target.empty().append($htmlStr).find('.updateDebn').trigger('click');
 	
 	// 수금계획의 존재 여부에 따라 disabled 시키는 로직
 	if($paramEl.custPlnSn != null && $paramEl.custPlnSn != '') {
 		$target.find('.chkDebnId').attr('disabled', true);
 	}
 	$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
 	
 };
 
<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>
 
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>

/**
 * <pre> 
 *  채권 리스트 재로딩 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 07
 *
 */
 $.fn.getDebnListForCustomerColNoPlan = function($custId, $orderCondition, $orderType, $bondMbrId, $custColSn) {
	 
	 $paramObj = {
			 custId : $custId,
			 orderCondition : $orderCondition,
			 orderType : $orderType,
			 bondMbrId : $bondMbrId,
			 custColSn : $custColSn
	 };
	 
	 $param = $.param($paramObj);
	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/getDebnListForCustomerColNoPlan.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$sBox = msg.model.sBox;
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	
			    	$targetEl = $('#debn_list_content tbody');
			    	$('#all_check').attr('checked', true).trigger('click');
			    	
			    	if($debnList.length > 0) {
			    		<%-- 채권 조회 결과 리스트 조회 --%>
					    $.fn.reloadDebnList($targetEl, $debnList, $sBox);	
			    	} else {
			    		$.fn.reloadNoDataList($targetEl, '<tr><td colspan="8"><div class="noSearch">결과가 없습니다.</div></td></tr>');
			    		
			    		<%-- 선택채권, 선택채권 미수금총액 초기화 --%>
			    		$('#selected_debn').text('0건');
			    		$('#debn_cnt').val(0);
			    		$('#selected_tot_unpd_amt').text('0원');
			    		$('#tot_unpd_amt').val(0);
			    		$('#tot_col_amt').val(0);
			    		$('#selected_tot_col_amt').text('0원');
			    	}
			    	
			    	<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
					if($('.chkDebnId').length != 0 && $('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
						$('#all_check').attr('checked', true);
					} else {
						$('#all_check').attr('checked', false);
					}
			    	
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("채권 조회 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };

/**
 * <pre> 
 *   거래처 수금 입력 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 30.
 * @param insertParam : 채권 수금 Insert 파라미터, custId : 거래처 순번, colDt : 수금일자
 *		  colAmt : 수금액, colType : 수금방식, debnCnt : 채권 건수, rmkTxt : 비고, colStatType : 거래처 수금 상태
 *		  
 * $paramType[S, C] : C - 수금완료, I - 대손처리
 */
 $.fn.addCustomerColNoPlan = function($paramType) {

  	 $paramEl = new Array();
  	 $replaceEl = new Array();
  	 $paramEl.push('.chkDebnId');
  	 $replaceEl.push('');
  	 $paramEl.push('.colAmt');
  	 $replaceEl.push(/[^0-9]/g);
  	 
  	 $insertParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'chkDebnId', $paramEl, $replaceEl, ':', '|');
  	 
  	 $paramObj = {
   			insertParam : $insertParam,
   			custId : $('#cust_id').val(),
   			colDt : $('#col_dt').val(),
   			colAmt : $('#tot_col_amt').val(),
   			rmkTxt : $('#rmk_txt').val(),
   			colType : $('#col_type').val(),
   			debnCnt : $('#debn_cnt').val(),
   			colStatType : $paramType
  	 };
  	 
   	 $param = $.param($paramObj);
   	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/addCustomerColNoPlan.do",
			dataType : "json",
			data : $param,
			beforeSend : function(){
				$('#btn_customer_col_complete, #btn_customer_col_close').hide();
			},
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('거래처 수금 등록이 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("거래처 수금 등록 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };

/**
 * <pre> 
 *   거래처 수금계획 수정 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 02
 * @param insertParam : 채권 수금 예정 Update 파라미터, updateParam : 채권 수금 예정 Update 파라미터, deleteParam : 채권 수금 예정 Delete 파라미터, 
 * 		  custId : 거래처 순번, totUnpdAmt : 미수총금액, colDt : 수금 예정 일자, colAmt : 수금 예정 금액, 
 *        debnCnt : 선택채권수, rmkTxt : 비고, custColSn : 거래처 수금 순번 
 */
 $.fn.modifyCustomerColNoPlan = function() {
	
  	 $paramEl = new Array();
  	 $replaceEl = new Array();
  	 $paramEl.push('.chkDebnId');
  	 $replaceEl.push('');
  	 $paramEl.push('.colAmt');
  	 $replaceEl.push(/[^0-9]/g);
  	 
  	 $insertParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'insertDebn', $paramEl, $replaceEl, ':', '|');
 	 $updateParam = $.fn.makeParam($('.chkDebnId:checked'), 'updateDebn', $paramEl, $replaceEl, ':', '|');
 	 <%-- delete 파라미터는 수금예정 금액이 없어도 되므로 삭제 --%>
 	 $paramEl.pop();
 	 $deleteParam = $.fn.makeParam($('.chkDebnId:not(:disabled):not(:checked)'), 'updateDebn', $paramEl, $replaceEl, ':', '|');
 	 
  	 $paramObj = {
  			insertParam : $insertParam,
  			updateParam : $updateParam,
  			deleteParam : $deleteParam,
  			custId : $('#cust_id').val(),
  			colDt : $('#col_dt').val(),
  			colAmt : $('#tot_col_amt').val(),
  			rmkTxt : $('#rmk_txt').val(),
  			colType : $('#col_type').val(),
  			colStatType : $('#col_stat').val(),
  			debnCnt : $('#debn_cnt').val(),
  			custColSn : $('#cust_col_sn').val(),
  			custPlnSn : $('#cust_pln_sn').val()
	 };
  	 
 	 $param = $.param($paramObj);
  	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/modifyCustomerColNoPlan.do",
			dataType : "json",
			data : $param,
			beforeSend : function(){
				$('#btn_customer_col_modify, #btn_customer_col_close').hide();
			},
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('수금 수정이 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("수금 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
</script>
</head>
<body>

<div class="pWrap">
	<header>
		<c:choose>
			<c:when test="${empty sBox.custColSn}">
				<h1><img src="${IMG}/customer/h3_col_regist.gif" alt="수금업무 작성" /></h1>
			</c:when>
			<c:when test="${not empty sBox.custColSn}">
				<h1><img src="${IMG}/customer/h3_col_modify.gif" alt="수금업무 수정" /></h1>
			</c:when>
		</c:choose>
	</header>
	
	<form name="customerColForm" id="customer_col_form" autocomplete="off">
		<%-- INPUT HIDDEN AREA --%>
		<input type="hidden" name="custColSn" id="cust_col_sn" value="${sBox.custColSn}" />
		<input type="hidden" name="custPlnSn" id="cust_pln_sn" value="${custCol.CUST_PLN_SN}" />
		<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}" />
		
		<div class="pContent customWidthDiv740">
			<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
				<div class="pSearch_list">
					<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자, 금액별로 정렬">
						<c:forEach var="orderConditionList" items="${commonCode.colOrderConditionList}">
							<option value="${orderConditionList.KEY}" <c:if test="${orderConditionList.KEY eq sBox.orderCondition}">selected="selected"</c:if> >${orderConditionList.VALUE}</option>
						</c:forEach>
					</select>
					<select id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
						<c:forEach var="orderTypeList" items="${commonCode.orderTypeList}">
							<option value="${orderTypeList.KEY}" <c:if test="${orderTypeList.KEY eq sBox.orderType}">selected="selected"</c:if> >${orderTypeList.VALUE}</option>
						</c:forEach>
					</select>
					<label for="listNo"></label>
					<c:if test="${empty custCol.CUST_PLN_SN}">
						<select name="bondMbrId" id="bond_mbr_id" class="listOrderOption">
							<option value="" <c:if test="${empty sBox.bondMbrId }">selected="selected"</c:if> >전체</option>
							<c:forEach var="bondMbrList" items="${bondMbrList}">
								<option value="${bondMbrList.MBR_ID}" <c:if test="${empty bondMbrList.MBR_USR_ID}">class="mem_type_N"</c:if> <c:if test="${bondMbrList.MBR_ID eq sBox.bondMbrId}">selected="selected"</c:if> >${bondMbrList.USR_NM}</option>
							</c:forEach>
						</select>
					</c:if>
					<a href="#none" class="pBtn_Search" id="btn_col_search">검색</a>
				</div>
			</c:if>
		</div>

		<div class="pTableWrap customWidthDiv740">
			<div class="pTableTop customWidthDiv740">
				<table summary="수금업무 작성" class="pTableTitle debnListTitle customWidthDiv740" id="debn_list_title">
				<caption>수금업무 작성</caption>
				<colgroup>
					<col width="25px">
					<col width="74px">		
					<col width="115px">			
					<c:choose>
						<%-- 수금예정금액을 포함할 경우 --%>
						<c:when test="${not empty sBox.custColSn}">
							<col width="105px">
							<col width="105px">
							<col width="100px">
							<col width="100px">
						</c:when>
						<%-- 수금예정금액을 제외할 경우 --%>
						<c:otherwise>
							<col width="135px">
							<col width="135px">
							<col width="140px">
						</c:otherwise>
					</c:choose>
					<col width="115px">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">
							<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<input type="checkbox" id="all_check">
							</c:if>
						</th>
						<th scope="col">채권담당자</th>
						<th scope="col">세금계산서작성일</th>
						<th scope="col">합계금액</th>
						<th scope="col">미수금액</th>
						<th scope="col">타수금계획</th>
						<c:if test="${not empty sBox.custColSn}">
							<th scope="col">수금예정금액</th>
						</c:if>
						<th scope="col" class="last">수금금액</th>					
					</tr>
				</thead>
				</table>
			</div>
			<div class="pTableContent customWidthDiv740">
				<table summary="수금업무 작성" class="pTable debnListContent" id="debn_list_content">
				<caption>수금업무 작성</caption>
				<colgroup>
					<col width="25px">
					<col width="74px">
					<col width="115px">
					<c:choose>
						<%-- 수금예정금액을 포함할 경우 --%>
						<c:when test="${not empty sBox.custColSn}">
							<col width="105px">
							<col width="105px">
							<col width="100px">
							<col width="100px">
						</c:when>
						<%-- 수금예정금액을 제외할 경우 --%>
						<c:otherwise>
							<col width="135px">
							<col width="135px">
							<col width="140px">
						</c:otherwise>
					</c:choose>
					<col width="115px">
				</colgroup>
				<tbody>
					<%-- 미수총금액 합계 변수 --%>
					<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="0"/>
					
					<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
					<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
						<tr><td colspan="8"><div class="noSearch">결과가 없습니다.</div></td></tr>						
					</c:if>
					<c:forEach var="debnList" items="${debnList}">
						<tr>
							<td class="checkDebn">
								<%-- INSERT, UPDATE 구분을 위한 동적 클래스 할당 --%>
								<input type="checkbox" class="chkDebnId <c:if test="${empty debnList.CUST_COL_SN}">insertDebn</c:if> <c:if test="${(not empty debnList.CUST_COL_SN) and (debnList.CUST_COL_SN eq sBox.custColSn)}">updateDebn</c:if>" id="chk_${debnList.DEBN_ID}" value="${debnList.DEBN_ID}" <c:if test="${(not empty debnList.CUST_COL_SN) and (debnList.CUST_COL_SN eq sBox.custColSn)}">checked="checked"</c:if> <c:if test="${(not sBox.isSessionCustAddGrn or not sBox.isSessionDebnSearchGrn) or (not empty debnList.CUST_PLN_SN) }">disabled="disabled"</c:if>>							
							</td>
							<td class="bondMbrNm">${debnList.BOND_MBR_NM}</td>
							<td class="billDt">${debnList.BILL_DT}</td>
							<td class="list_right sumAmt">${debnList.SUM_AMT}</td>
							<td class="list_right rmnAmt">${debnList.RMN_AMT}</td>
							<td class="list_right otherPlnColAmt" data-other-pln="${debnList.OTHER_PLN_FLAG}" data-other-pln-amt="${debnList.PLN_COL_AMT_OTHER}">
								${debnList.PLN_COL_AMT_OTHER_COMMA}
							</td>
							<c:if test="${not empty sBox.custColSn}">
								<td class="list_right debnPlnColAmt">
									${debnList.PLN_COL_AMT_COMMA}
								</td>
							</c:if>
							<td class="list_left last debnColAmtInput" data-prior-col-amt="${debnList.COL_AMT}">
								<c:if test="${(not empty sBox.custColSn) and (debnList.CUST_COL_SN eq sBox.custColSn) }">
									<c:choose>
										<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (debnList.CUST_PLN_STATUS ne 'N') }">
											<input type="text" id="col_amt_${debnList.DEBN_ID}" name="colAmt" class="txtInput colAmt required numericMoney" style="width:85px" title="수금금액" value="${debnList.COL_AMT_COMMA}" maxlength="12" />
										</c:when>
										<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (debnList.CUST_PLN_STATUS eq 'N') }">
											<input type="text" id="col_amt_${debnList.DEBN_ID}" name="colAmt" class="txtInput colAmt required numericMoney" style="width:85px" title="수금금액" value="${debnList.COL_AMT_COMMA}" maxlength="12" />
										</c:when>
										<c:otherwise>
											<p class="noBorder">${debnList.COL_AMT_COMMA}</p>
										</c:otherwise>
									</c:choose>
								</c:if>
							</td>
						</tr>
						<%-- 선택된 채권만 미수총금액 합계 연산 --%>
						<c:if test="${(not empty debnList.CUST_COL_SN ) and (debnList.CUST_COL_SN eq sBox.custColSn)}">
							<fmt:formatNumber var="rmnAmt" type="NUMBER" value="${fn:replace(debnList.RMN_AMT, ',', '')}" pattern="#"/>
							<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="${totUnpdAmt + rmnAmt }" pattern="#"/>
						</c:if>
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		
		<div class="pSumWrap customWidthDiv740">
			<table class="pSumTable customWidthDiv740">
			<caption>통계</caption>
			<colgroup>
				<col width="50%">
				<col width="50%">
			</colgroup>
			<tbody>
				<tr>
					<td>
						선택채권 : 
						<c:choose>
							<c:when test="${empty custCol.DEBN_CNT}">
								<p id="selected_debn">
									0건
								</p>
								<input type="hidden" name="debnCnt" id="debn_cnt" value="0" />
							</c:when>
							<c:otherwise>
								<p id="selected_debn">
									${custCol.DEBN_CNT}건
								</p>
								<input type="hidden" name="debnCnt" id="debn_cnt" value="${custCol.DEBN_CNT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						선택채권 미수금총액 : 
						<fmt:formatNumber var="totUnpdAmtComma" type="NUMBER" value="${totUnpdAmt}" groupingUsed="true"/>
						<p id="selected_tot_unpd_amt">${totUnpdAmtComma}원</p>
						<input type="hidden" name="totUnpdAmt" id="tot_unpd_amt" value="${totUnpdAmt}" />
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		
		<div class="pInputWrap customWidthDiv740">
			<table class="pInputTable customWidthDiv740">
			<caption>수금업무 작성테이블</caption>
			<colgroup>
				<col width="120px">
				<col width="2px">
				<col width="198px">
				<col width="120px">
				<col width="2px">
				<col width="198px">
			</colgroup>
			<tbody>
				<tr>
					<td><p>수금일</p></td>
					<td><p>:</p></td>
					<td colspan="4">
						<c:choose>
							<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (empty sBox.custColSn) }">
								<input type="text" name="colDt" id="col_dt" class="firstDate" title="수금일" value="${result.currentDt}" readonly="readonly" />								
							</c:when>
							<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (not empty sBox.custColSn) }">
								<input type="text" name="colDt" id="col_dt" class="firstDate" title="수금일" value="${custCol.COL_DT}" readonly="readonly" />
							</c:when>
							<c:otherwise>
								${custCol.COL_DT }
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td><p>수금방식</p></td>
					<td><p>:</p></td>
					<td>
						<c:choose>
							<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<select name="colType" id="col_type">
									<c:forEach var="custColTypeList" items="${commonCode.custColTypeList}">
										<option <c:if test="${custColTypeList.KEY eq 'I'}">class="irrecType"</c:if> value="${custColTypeList.KEY}" <c:if test="${custColTypeList.KEY eq custCol.COL_TYPE }">selected="selected"</c:if> >${custColTypeList.VALUE}</option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<c:forEach var="custColTypeList" items="${commonCode.custColTypeList}">
									<c:if test="${custColTypeList.KEY eq custCol.COL_TYPE}">${custColTypeList.VALUE}</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</td>
					<td><p>상태</p></td>
					<td><p>:</p></td>
					<td>
						<c:choose>
							<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<select name="colStat" id="col_stat">
									<c:forEach var="colStatTypeList" items="${commonCode.colStatTypeList}">
										<option value="${colStatTypeList.KEY}" <c:if test="${colStatTypeList.KEY eq custCol.STAT}">selected="selected"</c:if> >${colStatTypeList.VALUE}</option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<c:forEach var="colStatTypeList" items="${commonCode.colStatTypeList}">
									<c:if test="${colStatTypeList.KEY eq custCol.STAT}">${colStatTypeList.VALUE}</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="middle">
						<p>실수금총액</p>
						<input type="hidden" name="totColAmt" id="tot_col_amt" value="${custCol.COL_AMT}" />
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_tot_col_amt" colspan="4">
						<c:choose>
							<c:when test="${empty custCol.COL_AMT}">
								0원
							</c:when>
							<c:otherwise>
								${custCol.COL_AMT_COMMA}원
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="last"><p>비고</p></td>
					<td class="last"><p>:</p></td>
					<td class="last" colspan="4">
						<c:choose>
							<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<input type="text" id="rmk_txt" name="rmkTxt" class="txtInput" style="width:400px" title="비고" value="${custCol.RMK_TXT}" maxlength="200">
							</c:when>
							<c:otherwise>
								${custCol.RMK_TXT}
							</c:otherwise>
						</c:choose>						
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		<div class="pBtnWrap">
			<div class="pBtnMiddle">
				<c:choose>
					<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (empty sBox.custColSn) }">
						<a href="#none" class="on btnColSave STAT_C" id="btn_customer_col_complete">수금완료</a>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">취소</a>
					</c:when>
					<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (not empty sBox.custColSn) }">
						<a href="#none" class="on" id="btn_customer_col_modify">수정완료</a>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">취소</a>
					</c:when>
					<c:otherwise>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">닫기</a>						
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form>
</div>
</body>
</html>