<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>사업자등록번호 중복</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/customer/getCheckDuplicationCustomerForm.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<style type="text/css">

</style>
<script type="text/javascript">

	var duplicationCheckFlag = false; // 중복여부 확인 Flag 변수

	/**
	*	Document Ready 시작
	*/
	$(document).ready(function() {
		
		/**
		*	사업자 등록번호 중복 체크 로직 Event
		*	@author sungrangkong
		*	@since 2013.08.27
		*/
		$('#btn_overlapCheck').click(function(e){
			
			var bTF = true; // Process 진행 Flag
			
			if($.trim($('#custNo').val()) == ''){
				alert("사업자 등록번호를 입력해주세요");
				$('#custNo').focus();
				return false;
			}
			if(!typeCheck('businessNoCheck',replaceAll($.trim($('#custNo').val()),'-',''))){
				alert("사업자 번호 형식에 맞지 않습니다.");
				$('#custNo').focus();
				bTF = false;
			}
			
			param = "custNo=" +  replaceAll($.trim($('#custNo').val()),'-','') + "&custType=" + $("#custType").val();
			if(bTF){
				$.ajax({
					type : "POST", 
					url : "${HOME}/customer/checkDuplicationCustomer.do",
					dataType : "json",
					data : param,
					async : false,
					success : function(json_result) {
						var result = json_result.model.resultBox;
						if(result.REPL_CD == "00000"){
							$('#resultSuccessMsg font').text(replaceAll($.trim($('#custNo').val()),'-',''));
							$('#resultSuccessMsg').show();
							$('#defaultMsg').hide();
							$('#resultFailMsg').hide();
							duplicationCheckFlag = true;
						}else {
							$('#resultFailMsg font').text(replaceAll($.trim($('#custNo').val()),'-',''));
							$('#resultFailMsg span').text(result.REPL_MSG);
							$('#resultFailMsg').show();
							$('#defaultMsg').hide();
							$('#resultSuccessMsg').hide();
						}
					},
					error : function(xmlHttpRequest, textStatus, errorThrown){
						alert("사업자 등록번호 중복 체크 로직 중 에러발생 [" + textStatus + "]");
					}
				});
			}
		});
		
		/**
		*	확인 버튼 Click Event
		*	@author sungrangkong
		*	@since 2013.08.27
		*/
		$("#btn_ok").click(function(e){
			if(!duplicationCheckFlag){
				alert("중복 확인을 해주세요.");
				return false;
			}else{
				$("form #cust_no",opener.document).val('');
				$("form #cust_no",opener.document).val($('#resultSuccessMsg').find('font').text());
				window.close();
			}
		});
		
		/**
		*	확인 버튼 Click Event
		*	@author sungrangkong
		*	@since 2013.08.27
		*/
		$("#btn_cancel").click(function(e){
			window.close();
		});
		
		// 페이지 로딩 시 focus 로직
		$('#custNo').focus();
		
	});
</script>
</head>
<body>
<%-- HIDDEN AREA --%>
<input type="hidden" id ="custType" value="${sBox.custType}" />

<div id="popup">
	<div class="content">
    	<input type="text" id="custNo" size="25" maxLength="10"><em>　</em><a id="btn_overlapCheck"><img src="${IMG}/popup/btn_overlapcheck.gif"></a><p></p>
    	
    	<%-- 중복체크 로직 등록 성공시 Msg Element --%>
        <div id="resultSuccessMsg" style="display:none">
        	<font class="code"></font>은 사용하실 수 있습니다.<br />사용하시겠습니까?
        </div>
        
    	<%-- 중복체크 로직 등록 실패 Msg Element --%>
        <div id="resultFailMsg" style="display:none">
        	<font class="code"></font>은 <span></span><br />다시 확인해 주시기 바랍니다.
        </div>
        
        <%-- 페이지 로딩시 Default Msg Element --%>
        <div id="defaultMsg" style="display:block">
        	<p>
        	(1) '-' 을 빼고 입력해주세요<br />
        	</p>
        </div>
        <div class="checkbtn">
            <a id="btn_ok"><img src="${IMG}/popup/btn_ok.gif" alt="확인" /></a>
            <a id="btn_cancel"><img src="${IMG}/popup/btn_cancel.gif" alt="취소" /></a>
        </div>
    </div>
</div>

</body>
</html>