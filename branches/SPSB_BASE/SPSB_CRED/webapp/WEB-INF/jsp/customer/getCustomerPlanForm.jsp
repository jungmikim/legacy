<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 수금계획 조회 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="custPln" value="${result.custPln}" />
<c:set var="debnList" value="${result.debnList}" />
<c:set var="commonCode" value="${result.commonCode}" />
<c:set var="bondMbrList" value="${result.bondMbrList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/popupCol.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerPlan.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
  	/**
	 * <pre>
	 *   체크박스 클릭시 수금 예정 금액 활성화 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	 $('.chkDebnId').live({
		 click : function(e){
			 if($(this).is(':checked')) {
				if($(this).parent().parent().find('td.last').find('input').length == 0) {
					$(this).parent().parent().find('td.last')
					.append(
						'<input type="text" id="pln_col_amt_' + $(this).val() + '" name="plnColAmt" ' +
							    'class="txtInput plnColAmt numericMoney required" style="width:85px" title="수금예정 금액" ' + 
							    'value="' + ($(this).parent().siblings('.rmnAmt').text()) +'" maxlength="12">'		 
					); 
				}
				 
			 } else {
				 $(this).parent().parent().find('td.last').empty();
			 }
			 
			<%-- 선택 채권 건수, 선택채권 미수금총액, 수금예정금액 설정 --%>
		 	$('#tot_unpd_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T'));
			$('#tot_pln_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.plnColAmt'), $('#selected_pln_col_amt'), '원', 'V'));
			$('#selected_debn').text($('.chkDebnId:checked').length + '건');
			$('#debn_cnt').val($('.chkDebnId:checked').length);
			 
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		 }
	 });
	 
	 /**
	 * <pre>
	 *   수금 예정 금액 입력시 미수 금액과 비교하는 로직
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	 $('.plnColAmt').live({
		focus: function(e) {
			$(this).val(replaceAll($(this).val(), ',', ''));
		},
		focusout: function(e) {
			
			<%-- 공백 검증 --%>
			if($.trim($(this).val()) == '') {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '를 입력해주세요.');
			}
			
			<%-- 숫자 검증 --%>
			if(!typeCheck('numCheck',$.trim($(this).val()))) {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			}
			
			<%-- 입력값 대소비교 --%>
			$plnColAmt = parseInt(($(this).val().replace(/[^0-9]/g, '')), 10);
			$rmnAmt = parseInt(($(this).parent().siblings('.rmnAmt').text().replace(/[^0-9]/g, '')), 10);
			if(!$.fn.compareValue($plnColAmt, $rmnAmt, 'LE')) {
				return $.fn.validateResultAndAlert($(this), '수금예정금액은 미수금액보다 적어야 합니다.');
			}
			
			<%-- 입력한 값 초기화 --%>
			$(this).val(addComma($plnColAmt));
			
			<%-- 선택 채권 건수, 선택채권 미수금총액, 수금예정금액 설정 --%>
			$('#tot_unpd_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T'));
			$('#tot_pln_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.plnColAmt'), $('#selected_pln_col_amt'), '원', 'V'));
			$('#selected_debn').text($('.chkDebnId:checked').length + '건');
			$('#debn_cnt').val($('.chkDebnId:checked').length);
		}
	 });
	 
	/**
	 * <pre>
	 *   취소 버튼 클릭시 팝업 닫기
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	$('#btn_customer_pln_close').live({
		click : function(e){
			self.close();
		}
	});
	 
	/**
	 * <pre>
	 *   검색 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_pln_search').live({
		click : function(e){
			$custId = $('#cust_id').val();
			$orderCondition = $('#order_condition').val();
			$orderType = $('#order_type').val();
			$bondMbrId = $('#bond_mbr_id').val();
			$custPlnSn = $('#cust_pln_sn').val();
			$excludeFlag = $('#exclude_flag').val();
			
			$.fn.getDebnListForCustomerPlan($custId, $orderCondition, $orderType, $bondMbrId, $custPlnSn, $excludeFlag);
		}
	});
	 
	/**
	 * <pre>
	 *   체크박스 모두 선택 토글 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#all_check').live({
		click : function(){
			if($(this).is(':checked')) {
				$('.chkDebnId:not(:disabled)').attr('checked', false).trigger('click');
			} else {
				$('.chkDebnId:not(:disabled)').attr('checked', true).trigger('click');
			}
		}
	});
	 
	/**
	 * <pre>
	 *   저장[추가] 버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_customer_pln_save').live({
		click : function(e){
			if($('.chkDebnId:not(:disabled):checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			
			if(!$.fn.validate($('.plnColAmt'))) return false;
			
			$.fn.addCustomerPlan();
		}
	});
	 
	/**
	 * <pre>
	 *   저장[수정] 버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_customer_pln_modify').live({
		click : function(e){
			if($('.chkDebnId:not(:disabled):checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			if(!$.fn.validate($('.plnColAmt'))) return false;
			$.fn.modifyCustomerPlan();
		}
	});
	 
});

<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%> 

/**
 * <pre> 
 *   수금계획 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 19
 * @param $listEl : 검증할 엘리먼트 리스트
 *
 */
$.fn.validate = function($listEl) {
	
	$result = true;
	$zeroFlag = false;
	 
	$.each($listEl, function(){
		
		$plnColAmt = $.trim(replaceAll($(this).val(), ',', ''));
		
		<%-- 공백 검증 --%>
		if($plnColAmt == '') {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '를 입력해주세요.');
			return $result;
		}
		
		<%-- 숫자 검증 --%>
		if(!typeCheck('numCheck',$plnColAmt)) {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			return $result;
		}
		
		<%-- 입력값 대소비교 --%>
		$rmnAmt = parseInt(($(this).parent().siblings('.rmnAmt').text().replace(/[^0-9]/g, '')), 10);
		$plnColAmt = parseInt($plnColAmt, 10);
		if(!$.fn.compareValue($plnColAmt, $rmnAmt, 'LE')) {
			$result = $.fn.validateResultAndAlert($(this), '수금예정금액은 미수금액보다 적어야 합니다.');
			return $result;
		}
		
		<%-- 하나라도 0 이상의 값이 있으면 통과 --%>
		if(parseInt($plnColAmt, 10) > 0) {
			$zeroFlag = true;
		}
		
	});
	
	<%-- 0값 검증 --%>
	if(!$zeroFlag) {
		$result = $.fn.validateResultAndAlert('', '수금예정금액은 모두 0이 될 수 없습니다. ');
		return $result;
	}
	 
	return $result;
};

/**
 * <pre> 
 *   <li> 태그 리스트 백그라운드 CSS 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 *
 */
 $.fn.setBackgroundEvenAndOdd = function($targetEl, $subEl, $evenColor, $oddColor) {
	 $targetEl.find($subEl + ':even').css('background-color', $evenColor);
	 $targetEl.find($subEl + ':odd').css('background-color', $oddColor);
 };
		
 /**
  * <pre> 
  *    대소 비교 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 18
  * @param $num1 : 비교할 값 1, $num2 : 비교할 값 2, $operand : 연산자[LE,GE,LT,GT]
  */
  $.fn.compareValue = function($num1, $num2, $operand) {
 	 if($operand == 'LE') {
 		 return ($num1 <= $num2);
 	 } else if($operand == 'GE') {
 		return ($num1 >= $num2);
 	 } else if($operand == 'LT') {
 		return ($num1 < $num2);
 	 } else if($operand == 'GT') {
 		return ($num1 > $num2);
 	 }  
  };
 
/**
 * <pre>
 *   선택한 값 자동 계산 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 * @param $listEl : 계산할 엘리먼트, $targetEl : 결과 세팅 엘리먼트, 
 *        $suffix : 결과 뒤에 추가할 메세지, $valueType : 함수 사용 조건[V, T] 
 */
 $.fn.selectedTargetCalc = function($listEl, $targetEl, $suffix, $valueType){
	 $result = 0;
	 
	 $listEl.each(function() {
		$value = '';
		switch($valueType) {
			case 'V' :
				$value = $(this).val();
				break;
			case 'T' :
				$value = $(this).text();
				break;
		}  
		
		$result += parseInt(($value.replace(/[^0-9]/g, '')), 10);
	 });
	 
	 $targetEl.html(addComma($result) + $suffix);
	 
	 return $result;
 };
	
/**
 * <pre>
 *   경고창 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
 */
 $.fn.validateResultAndAlert = function($el, $message){
	alert($message);
	return false;
 }; 
 
/**
 * <pre> 
 *   채권 리스트를 재구성 하는 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 *
 * @param $target : 리스트 구성 영역 Element
 * @param $listEl : 리스트를 구성할 Data
 * @param $sBox : 요청 Data
 */
$.fn.reloadDebnList = function($target, $listEl, $sBox) {
	$htmlStr = '';
 	
 	$.each($listEl, function() {
	$htmlStr +=
			'<tr>' +
				'<td class="checkDebn">';
	$htmlStr +=	'<input type="checkbox" class="chkDebnId ' + (this.CUST_PLN_STATUS == 'N' ? 'insertDebn' : '') + ' ' + (this.CUST_PLN_STATUS == 'E' ? 'updateDebn' : '') + ' " id="chk_' + (this.DEBN_ID) +'" value="' + (this.DEBN_ID) +'" ' + ((this.CUST_PLN_STATUS == 'D') ? 'disabled="disabled"' : '' ) + ' >';	
	$htmlStr +=	'</td>' +
				'<td class="bondMbrNm">' + this.BOND_MBR_NM + '</td>' + 
				'<td class="billDt">' + this.BILL_DT + '</td>' + 
				'<td class="list_right sumAmt">' + this.SUM_AMT + '</td>' +
				'<td class="list_right rmnAmt">' + this.RMN_AMT + '</td>' +
				'<td class="list_right debnPlnColAmt">' +
					(this.CUST_PLN_STATUS == 'D' ? this.PLN_COL_AMT_COMMA : '' ) +
				'</td>' +
				'<td class="list_left last debnPlnColAmtInput">';
	$htmlStr +=	'</td>' + 
			'</tr>';	
 	});
 	
 	$('#all_check').attr('checked', false);
 	$target.empty().append($htmlStr).find('.updateDebn').trigger('click');
 	$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
 	
 };
 
 /**
  * <pre> 
  *   파라미터 구성 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 22
  *
  * @param $targetEl : 수집 대상 엘리먼트
  * @param $selector : 수집 대상 셀렉터(클래스)
  * @param $paramEl : 파라미터 수집 대상 객체
  * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
  * @param $dataSplitStr : 구분자[데이터]
  * @param $rowSplitStr : 구분자[row]
  */
 $.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {
 	
	$result = '';
	
	$.each($targetEl, function() {
		if($(this).hasClass($selector.toString())) {
			$tmpStr = '';
			for(var i = 0 ; i < $paramEl.length; i++) {
				if($tmpStr != '') {
					$tmpStr += $dataSplitStr;
				}
				$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
			}		
			
			if($result != '') {
				$result += $rowSplitStr;
			}
			$result += $tmpStr;
		}
	});
 	
	return $result;
 };
 
<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>
 
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>
 
/**
 * <pre> 
 *  채권 리스트 재로딩 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 *
 */
 $.fn.getDebnListForCustomerPlan = function($custId, $orderCondition, $orderType, $bondMbrId, $custPlnSn, $excludeFlag) {
	 
	 $paramObj = {
			 custId : $custId,
			 orderCondition : $orderCondition,
			 orderType : $orderType,
			 bondMbrId : $bondMbrId,
			 custPlnSn : $custPlnSn,
			 excludeFlag : $excludeFlag 
	 };
	 
	 $param = $.param($paramObj);
	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/getDebnListForCustomerPlan.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$sBox = msg.model.sBox;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	
			    	$targetEl = $('#debn_list_content tbody');
			    	$('#all_check').attr('checked', true).trigger('click');
			    	
			    	<%-- 채권 조회 결과 리스트 조회 --%>
			    	if($debnList.length > 0) {
			    		$.fn.reloadDebnList($targetEl, $debnList, $sBox);	
			    	} else{
			    		$.fn.reloadNoDataList($targetEl, '<tr><td colspan="7"><div class="noSearch">결과가 없습니다.</div></td></tr>');
			    	}
					
					<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
					if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
						$('#all_check').attr('checked', true);
					} else {
						$('#all_check').attr('checked', false);
					}
			    	
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("채권 조회 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
/**
 * <pre> 
 *   리스트가 없을 경우
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 17
 * @param $target : 리스트 구성 영역 Element
 * @param $noDataEl : 리스트를 구성할 Data
 */
$.fn.reloadNoDataList = function($target, $noDataEl) {
	 $htmlStr = '';
	 $target.empty().append($noDataEl);
	 $.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
};
 
/**
 * <pre> 
 *   거래처 수금계획 입력 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 *
 * @param insertParam : 채권 수금 예정 Insert 파라미터, custId : 거래처 순번, totUnpdAmt : 미수총금액, 
          plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, rmkTxt : 비고 
 *
 */
 $.fn.addCustomerPlan = function() {

  	 $paramEl = new Array();
  	 $replaceEl = new Array();
  	 $paramEl.push('.chkDebnId');
  	 $replaceEl.push('');
  	 $paramEl.push('.plnColAmt');
  	 $replaceEl.push(/[^0-9]/g);
  	 
  	 $insertParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'insertDebn', $paramEl, $replaceEl, ':', '|');
  	 $paramObj = {
    			insertParam : $insertParam,
    			custId : $('#cust_id').val(),
    			totUnpdAmt : $('#tot_unpd_amt').val(),
    			plnColDt : $('#pln_col_dt').val(),
    			plnColAmt : $('#tot_pln_col_amt').val(),
    			rmkTxt : $('#rmk_txt').val(),
    			debnCnt : $('#debn_cnt').val()
  	 };
  	 
   	 $param = $.param($paramObj);
  	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/addCustomerPlan.do",
			dataType : "json",
			data : $param,
			beforeSend : function(){
				$('#btn_customer_pln_save, #btn_customer_pln_close').hide();
			},
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('거래처 수금 계획 등록이 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close(); 
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("거래처 수금 계획 등록 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
/**
 * <pre> 
 *   거래처 수금계획 수정 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 22
 * @param insertParam : 채권 수금 예정 Update 파라미터, updateParam : 채권 수금 예정 Update 파라미터, deleteParam : 채권 수금 예정 Delete 파라미터, 
 * 		  custId : 거래처 순번, totUnpdAmt : 미수총금액, plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, 
 *        totUnpdAmt : 미수총금액, debnCnt : 선택채권수, rmkTxt : 비고, custPlnSn : 거래처 수금 계획 순번 
 */
 $.fn.modifyCustomerPlan = function() {

  	 $paramEl = new Array();
  	 $replaceEl = new Array();
  	 $paramEl.push('.chkDebnId');
  	 $replaceEl.push('');
  	 $paramEl.push('.plnColAmt');
  	 $replaceEl.push(/[^0-9]/g);
  	 
  	 $insertParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'insertDebn', $paramEl, $replaceEl, ':', '|');
 	 $updateParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'updateDebn', $paramEl, $replaceEl, ':', '|');
 	 <%-- delete 파라미터는 수금예정 금액이 없어도 되므로 삭제 --%>
 	 $paramEl.pop();
 	 $deleteParam = $.fn.makeParam($('.chkDebnId:not(:disabled):not(:checked)'), 'updateDebn', $paramEl, $replaceEl, ':', '|');
 	 
  	 $paramObj = {
  			insertParam : $insertParam,
  			updateParam : $updateParam,
  			deleteParam : $deleteParam,
  			custId : $('#cust_id').val(),
  			totUnpdAmt : $('#tot_unpd_amt').val(),
  			plnColDt : $('#pln_col_dt').val(),
  			plnColAmt : $('#tot_pln_col_amt').val(),
  			rmkTxt : $('#rmk_txt').val(),
  			debnCnt : $('#debn_cnt').val(),
  			custPlnSn : $('#cust_pln_sn').val()
	 };
  	 
 	 $param = $.param($paramObj);
  	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/modifyCustomerPlan.do",
			dataType : "json",
			data : $param,
			beforeSend : function(){
				$('#btn_customer_pln_modify, #btn_customer_pln_close').hide();
			},
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('거래처 수금 계획 수정이 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("거래처 수금 계획 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
</script>
</head>
<body>

<div class="pWrap">
	<header>
		<c:choose>
			<c:when test="${sBox.custPlnSn eq null || sBox.custPlnSn eq ''}">
				<h1><img src="${IMG}/customer/h3_col_plan_regist.gif" alt="수금계획 작성" /></h1>
			</c:when>
			<c:when test="${sBox.custPlnSn ne null and sBox.custPlnSn ne ''}">
				<h1><img src="${IMG}/customer/h3_col_plan_modify.gif" alt="수금계획 수정" /></h1>
			</c:when>
		</c:choose>
	</header>
	
	<form name="customerPlanForm" id="customer_plan_form" autocomplete="off">
		<%-- INPUT HIDDEN AREA --%>
		<input type="hidden" name="custPlnSn" id="cust_pln_sn" value="${sBox.custPlnSn}" />
		<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}" />
		<input type="hidden" id="excludeFlag" name="excludeFlag" value="${sBox.excludeFlag}" />
		
		<div class="pContent">
			<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
				<div class="pSearch_list">
					<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자, 금액별로 정렬">
						<c:forEach var="orderConditionList" items="${commonCode.planOrderConditionList}">
							<option value="${orderConditionList.KEY}" <c:if test="${orderConditionList.KEY eq sBox.orderCondition}">selected="selected"</c:if> >${orderConditionList.VALUE}</option>
						</c:forEach>
					</select>
					<select id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
						<c:forEach var="orderTypeList" items="${commonCode.orderTypeList}">
							<option value="${orderTypeList.KEY}" <c:if test="${orderTypeList.KEY eq sBox.orderType}">selected="selected"</c:if> >${orderTypeList.VALUE}</option>
						</c:forEach>
					</select>
					<label for="listNo"></label>
					<select name="bondMbrId" id="bond_mbr_id" class="listOrderOption">
						<option value="" <c:if test="${empty sBox.bondMbrId}">selected="selected"</c:if> >전체</option>
						<c:forEach var="bondMbrList" items="${bondMbrList}">
							<option value="${bondMbrList.MBR_ID}" <c:if test="${empty bondMbrList.MBR_USR_ID}">class="mem_type_N"</c:if> <c:if test="${bondMbrList.MBR_ID eq sBox.bondMbrId}">selected="selected"</c:if> >${bondMbrList.USR_NM}</option>
						</c:forEach>
					</select>
					<a href="#none" class="pBtn_Search" id="btn_pln_search">검색</a>
				</div>
			</c:if>
		</div>

		<div class="pTableWrap">
			<div class="pTableTop">
				<table summary="수금계획 작성" class="pTableTitle debnListTitle" id="debn_list_title">
				<caption>수금계획 작성</caption>
				<colgroup>
					<col width="25px">
					<col width="74px">
					<col width="115px">
					<col width="105px">
					<col width="105px">
					<col width="100px">
					<col width="115px">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">
							<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<input type="checkbox" id="all_check">
							</c:if>
						</th>
						<th scope="col">채권담당자</th>
						<th scope="col">세금계산서작성일</th>
						<th scope="col">합계금액</th>
						<th scope="col">미수금액</th>
						<th scope="col">타계획 수금액</th>
						<th scope="col" class="last">수금예정금액</th>					
					</tr>
				</thead>
				</table>
			</div>
			<div class="pTableContent">
				<table summary="수금업무 작성" class="pTable debnListContent" id="debn_list_content">
				<caption>수금업무 작성</caption>
				<colgroup>
					<col width="25px">
					<col width="74px">
					<col width="115px">
					<col width="105px">
					<col width="105px">
					<col width="100px">
					<col width="115px">
				</colgroup>
				<tbody>
					<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
					<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
						<tr><td colspan="7"><div class="noSearch">결과가 없습니다.</div></td></tr>						
					</c:if>
					<c:forEach var="debnList" items="${debnList}">
						<%-- 거래처 수금계획이 계획중 상태인 것만 조회 --%>
						<tr>
							<td class="checkDebn">
								<c:if test="${(custPln.STAT eq 'P') or (empty sBox.custPlnSn)}">
									<%-- INSERT, UPDATE 구분을 위한 동적 클래스 할당 --%>
									<input type="checkbox" class="chkDebnId <c:if test="${debnList.CUST_PLN_STATUS eq 'N'}">insertDebn</c:if> <c:if test="${debnList.CUST_PLN_STATUS eq 'E'}">updateDebn</c:if>" id="chk_${debnList.DEBN_ID}" value="${debnList.DEBN_ID}" <c:if test="${debnList.CUST_PLN_STATUS eq 'E'}">checked="checked"</c:if> <c:if test="${not sBox.isSessionCustAddGrn or not sBox.isSessionDebnSearchGrn or debnList.CUST_PLN_STATUS eq 'D' }">disabled="disabled"</c:if>>
								</c:if>
							</td>
							<td class="bondMbrNm">${debnList.BOND_MBR_NM}</td>
							<td class="billDt">${debnList.BILL_DT}</td>
							<td class="list_right sumAmt">${debnList.SUM_AMT}</td>
							<td class="list_right rmnAmt">${debnList.RMN_AMT}</td>
							<td class="list_right debnPlnColAmt">
								<c:if test="${debnList.CUST_PLN_STATUS eq 'D'}">
									${debnList.PLN_COL_AMT_COMMA}
								</c:if>
							</td>
							<td class="list_left last debnPlnColAmtInput">
								<c:choose>
									<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (debnList.CUST_PLN_STATUS eq 'E') }">
										<input type="text" id="pln_col_amt_${debnList.DEBN_ID}" name="plnColAmt" class="txtInput plnColAmt required numericMoney" style="width:85px" title="수금예정 금액" value="${debnList.PLN_COL_AMT_COMMA}" maxlength="12" >
									</c:when>
									<c:when test="${(not sBox.isSessionCustAddGrn or not sBox.isSessionDebnSearchGrn) and (debnList.CUST_PLN_STATUS eq 'E') }">
										<p class="noBorder">
											${debnList.PLN_COL_AMT_COMMA}
										</p>
									</c:when>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		
		<div class="pSumWrap">
			<table class="pSumTable">
			<caption>통계</caption>
			<colgroup>
				<col width="50%">
				<col width="50%">
			</colgroup>
			<tbody>
				<tr>
					<td>
						선택채권 : 
						<c:choose>
							<c:when test="${(custPln.DEBN_CNT eq null) or (custPln.DEBN_CNT eq '')}">
							<p id="selected_debn">
								0건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="0" />
							</c:when>
							<c:otherwise>
							<p id="selected_debn">
							${custPln.DEBN_CNT}건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="${custPln.DEBN_CNT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						선택채권 미수금총액 : 
						<c:choose>
							<c:when test="${empty custPln.TOT_UNPD_AMT}">
								<p id="selected_tot_unpd_amt">0원</p>
								<input type="hidden" name="totUnpdAmt" id="tot_unpd_amt" value="0" />
							</c:when>
							<c:otherwise>
								<p id="selected_tot_unpd_amt">${custPln.TOT_UNPD_AMT_COMMA}원</p>
								<input type="hidden" name="totUnpdAmt" id="tot_unpd_amt" value="${custPln.TOT_UNPD_AMT}" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		
		<div class="pInputWrap">
			<table class="pInputTable">
			<caption>수금업무 작성테이블</caption>
			<colgroup>
				<col width="100px">
				<col width="2px">
				<col width="178px">
				<col width="100px">
				<col width="2px">
				<col width="178px">
			</colgroup>
			<tbody>
				<tr>
					<td><p>수금계획일</p></td>
					<td><p>:</p></td>
					<c:choose>
						<c:when test="${(custPln eq null or custPln eq '')}">
							<td colspan="4">
								<input type="text" name="plnColDt" id="pln_col_dt" class="firstDate" title="수금계획일" value="${result.currentDt}" readonly="readonly">
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<c:choose>
									<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
										<input type="text" name="plnColDt" id="pln_col_dt" class="firstDate" title="수금계획일" value="${custPln.PLN_COL_DT}" readonly="readonly">
									</c:when>
									<c:otherwise>
										${custPln.PLN_COL_DT}
									</c:otherwise>
								</c:choose>
							</td>
							<td><p>상태</p></td>
							<td><p>:</p></td>
							<td>
								${custPln.STAT_NAME}
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="middle">
						<p>수금예정금액</p>
						<c:choose>
							<c:when test="${(custPln.PLN_COL_AMT eq null) or (custPln.PLN_COL_AMT eq '')}">
								<input type="hidden" name="totPlnColAmt" id="tot_pln_col_amt" value="0" />
							</c:when>
							<c:otherwise>
								<input type="hidden" name="totPlnColAmt" id="tot_pln_col_amt" value="${custPln.PLN_COL_AMT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_pln_col_amt" colspan="4">
						<c:choose>
							<c:when test="${(custPln.PLN_COL_AMT eq null) or (custPln.PLN_COL_AMT eq '')}">
								0원
							</c:when>
							<c:otherwise>
								${custPln.PLN_COL_AMT_COMMA}원
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="last"><p>비고</p></td>
					<td class="last"><p>:</p></td>
					<td class="last" colspan="4">
						<c:choose>
							<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and ((custPln eq null or custPln eq '') or (custPln.STAT eq 'P')) }">
								<input type="text" id="rmk_txt" name="rmkTxt" class="txtInput" style="width:400px" title="비고" value="${custPln.RMK_TXT}" maxlength="200">
							</c:when>
							<c:otherwise>
								${custPln.RMK_TXT}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		<div class="pBtnWrap">
			<div class="pBtnMiddle">
				<c:choose>
					<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and ((sBox.custPlnSn eq null) or (sBox.custPlnSn eq '')) }">
						<a href="#none" class="on" id="btn_customer_pln_save">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">취소</a>
					</c:when>
					<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn and custPln.STAT eq 'P' }">
						<a href="#none" class="on" id="btn_customer_pln_modify">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">취소</a>
					</c:when>
					<c:otherwise>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">닫기</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form>
</div>
</body>
</html>