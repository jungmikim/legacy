<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 거래처 대량등록 이력 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>
<c:set var="commonCode" value="${result.commonCode}"/>
<c:set var="pcPage" value="${result.pcPage}"/>
<c:set var="total" value="${result.customerMultiHistoryTotalCount}"/>
<c:set var="multiHistoryList" value="${result.customerMultiHistoryList}"/>
<c:set var="rowSizeTypeList" value="${result.commonCode.rowSizeTypeList }"/>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>거래처 대량 등록 이력 &gt; 거래처 관리 | 스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerMultiHistoryList.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	/**
     * <pre>
     *	정렬 조건을 변경하는 이벤트
     * </pre>
	 * @author HWAJUNG SON
	 * @since 2013. 10. 01.
     */
    $(".listOrderOption").change(function(){
    	$("#num").val(1);
    	$("#multiHistoryFrm").attr({action:"${HOME}/customer/getCustomerMultiHistoryList.do", 'method':'POST'}).submit();
    });
	 
	/**
	 * <pre>
	 *   거래처 대량등록 내역을 조회하는 검색 이벤트
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2013. 10. 01.
	 */
	$(".search").click(function(){
		
		$('#num').val(1);
		
		//공백값체크
	   	if($('#file_name').val()!='') 
   		$('#file_name').val($.trim($('#file_name').val()));
		
		$('#multiHistoryFrm').attr({action:'${HOME}/customer/getCustomerMultiHistoryList.do', 'method':'POST'}).submit();
	});
     
    /**
     * <pre>
     *	거래처 대량등록 내역 Excel Download 함수
     * </pre>
     * @author HWAJUNG SON
     * @since 2013. 10. 01
     */
     $('#btn_save_excel').click(function(){
  	   $('#multiHistoryFrm').attr({'action':'${HOME}/customer/getCustomerMultiHistoryListForExcel.do', 'method':'POST'}).submit();
     });
     
    /**
    *<pre>
    *	전체선택시 리스트 체크박스 모두 선택되는 함수
    * </pre>
    * @author HWAJUNG SON
    * @since 2013. 10. 01
    */
    $('#btn_select_all').toggle(
  		  function(){
	  		  	$('.chk_customer:not(:disabled)').attr('checked', true);
  		 },
  		  function(){
  		  	$('.chk_customer').attr('checked', false);
  		  }
  	  );
    
	  /**
	   * <pre>
	   *   거래처 대량등록 이력 선택 항목 삭제 함수
	   * </pre>
	   * @author HWAJUNG SON
	   * @since 2013. 10. 01
	   */
	   $('#btn_selected_customer_remove').click(function(){
		   if(!$('.chk_customer:checked').size()){
			   alert('삭제하실 등록이력을 선택해주세요.');
		   } else {
			   $.fn.removeCustomerHistory(
					$('.chk_customer:checked').map(function(){
					   return $(this).val();
					}).get().join(',')	   
			   );
		   }
	   }); 
	   
	   /**
	    * <pre>
	    *   거래처 대량등록 이력 단일 항목 삭제
	    * </pre>
	    * @author HWAJUNG SON
	    * @since 2013. 10. 01
	    */
	    $('.close').click(function(){
	    	$.fn.removeCustomerHistory(
	    		$(this).parent().find('.chk_customer').val()
	    	);
	    });
	    
	    /**
	    *<pre>
	    *	파일명 길이 제약 함수 
	    *</pre>
	    *@author HWAJUNG SON
	    *@since 2013. 10. 14 
	    */
		$('#file_name').keydown(function() {
			$.fn.countByte($(this).val());
		});
	    
});

/**
 * <pre>
 *   거래처등록 이력내역 삭제 함수
 * </pre>
 * @author HWAJUNG SON
 * @since 2013. 10. 01
 * @param mltId : 대량등록순번 삭제할 대량등록번호
 */
$.fn.removeCustomerHistory = function($mltId) {
	 if(!confirm('선택하신 거래처 등록내역을 \n정말 삭제하시겠습니까?')) return false; 
	 $('#mlt_id').val($mltId);
	 $('#multiHistoryFrm').attr({'action':'${HOME}/customer/removeCustomerMultiHistory.do', 'method':'POST'}).submit();
};

/**
 * <pre>
 *   byte 갯수를 세어주는 함수
 * </pre>
 * @author HWAJUNG SON
 * @since 2013. 10. 14
 * @param fileName : 파일명
 */
$.fn.countByte = function($fileName) {
	 
	 bytes = getByteLength($fileName);
	 
		if (bytes > 70) {
			alert("한글 35자 영문 70자 이내로 입력하여 주십시오.");
			var replace_txt = $fileName.substr(0, $fileName.length - 2);
			$("#file_name").val(replace_txt);
			bytes = getByteLength($(this).val());
		}
};

</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
	<%--  Top Area Start --%>
	<jsp:include page="../common/top.jsp" flush="true">
		<jsp:param name="gnb" value="0104"/>
	</jsp:include>
	<%-- Top Area End --%>
 
<%-- contentArea --%>
<div class="contentArea sub02"><%-- menu별 이미지 class sub00 --%>
	<%-- content --%>
	<section id="content">
		<h2 class="tit_sub0104">거래처 대량 등록 이력</h2>
		<p class="under_h2_sub0104">거래처 대량 등록 업로드 진행 상황에 대해 확인 및 관리를 할 수 있습니다.</p>
		<p class="btnSearchBox"><a href="#" class="open">거래처 검색</a></p>
			
		<form name="multiHistoryFrm" id="multiHistoryFrm">
		<%-- FORM HIDDEN AREA START --%>
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/customer/getCustomerMultiHistoryList.do">
		<input type="hidden" name="searchType" id="search_type" value="L" />
		<input type="hidden" name="mltId" id="mlt_id" value="${sBox.mltList}" />
		<input type="hidden" name="mltNo" id="mlt_no" value="" />
		<input type="hidden" name="custSearchFlag" id="cust_search_flag" value="" />
		<fieldset class="searchBox">
			<legend>채권 관리 검색</legend>
		  <table summary="채권 관리 검색 테이블로 검색조건인 구분, 사업자유형, 거래처, 조회기간으로 구성되어있습니다.">
				<colgroup>
					<col width="20%" />
					<col width="80%" />
				</colgroup>
				<tr>
					<th scope="row">거래처</th>
					<td class="block">
						<span class="first"><input type="radio" name="cust_type" id="total" class="cust_type" value="T" <c:if test="${sBox.cust_type eq 'T'}">checked</c:if>/> <label for="total">전체</label></span>
					  	<span class="second"><input type="radio" name="cust_type" id="cust_year" value="Y"  class="cust_type"  <c:if test="${sBox.cust_type eq 'Y'}">checked</c:if> /> <label for="cust_year">업로드일</label></span>
						<span class="last">
                        <input type="text" name="firstDate" class="firstDate" value="${sBox.firstDate}" readonly/> ~
						<input type="text" name="lastDate" class="lastDate" value="${sBox.lastDate}" readonly/>
                      </span>
					</td>
				</tr>
				<tr>
					<th scope="row">파일명</th>
					<td class="block">
                   	  <input type="text" name="fileName" id="file_name" class="txtInput" size="70" value="${sBox.fileName}" style="width:225px" title="회사명 입력" />
					</td>
				</tr>
			</table>
			<div class="search_btn">
				<div>
					<a href="#" class="search">검색</a>
				</div>
			</div>
		</fieldset>
	<div class="tableTop">
		<span class="tableCondition">
			<a id="btn_select_all" href="#"><img src="${IMG}/common/ico_selectAll.gif" alt="전체선택" /></a>
        </span> 검색결과 [${total}개]
       <div class="rightTop">
		<span class="noList">
			<label for="listNo">목록갯수</label>
			<select name="rowSize" id="listNo" class="listOrderOption">
				<c:forEach var="rowSizeTypeList" items="${rowSizeTypeList}">
					<option value="${rowSizeTypeList.KEY}" <c:if test="${sBox.rowSize eq rowSizeTypeList.KEY}">selected</c:if>>${rowSizeTypeList.VALUE}</option>	
				</c:forEach>
			</select>
		</span>
		</div>
	  </div>	
	</form>
	<div class="tableUlWrap">
	 <c:choose>
		<c:when test="${total>0}">
			<c:forEach var="multiHistoryList" items="${multiHistoryList}">
				<ul>
				 <li>
					<input class="chk_customer" type="checkbox" value="${multiHistoryList.MLT_ID}" title="거래처내역선택"<c:if test="${(multiHistoryList.STAT ne 'T')}"> disabled="disabled"</c:if>/>
					<dl>
						<dd class="date">${multiHistoryList.REG_DT_TIME}</dd>
						<dt>
						<c:choose>
		          				<c:when test="${multiHistoryList.STAT eq 'N'}">
		                    		<b>${multiHistoryList.FILE_NM}</b> 
		                    	</c:when>
	                    		<c:otherwise><b>${multiHistoryList.FILE_NM}</b></c:otherwise>
	                    	</c:choose>
						</dt>
	                    <dd>
	                    	<em>|</em>거래처 수 :
	                    	<c:choose>
		          				<c:when test="${multiHistoryList.STAT eq 'N'}">
		                    		${multiHistoryList.MLT_CNT_VALUE}건
		                    	</c:when>
	                    		<c:otherwise>-</c:otherwise>
	                    	</c:choose>
	                    	<em>|</em>
	                    	<c:choose>
	                    		<c:when test="${multiHistoryList.FILE_NM!=null&&multiHistoryList.FILE_NM!=''}">
	                    		${multiHistoryList.STAT_VALUE}
	                    		</c:when>
	                    		<c:otherwise>
	                    		-
	                    		</c:otherwise>
	                    	</c:choose>
	                    </dd>
				    </dl>
				    <c:if test="${(multiHistoryList.STAT eq 'T')}"><p class="close"><a href="#"><img src="${IMG}/common/btn_close.gif" alt="닫기" /></a></p></c:if>
				    <span class="date">${multiHistoryList.REG_ID} : ${multiHistoryList.UPD_DT_TIME}</span>
				  </li>
				  </ul>				  
			</c:forEach>
		</c:when>
		<c:otherwise>
			<div class="noSearch">검색 결과가 없습니다.</div>
		</c:otherwise>
	</c:choose>
	</div>
	<div class="btmBtnWrap">
		<p class="leftBtn btnWrap">
			<c:if test="${sBox.isSessionCustAddGrn}">
				<a id="btn_selected_customer_remove" href="#">선택삭제</a>
			</c:if>
		</p>
		<div class="paging">
			<c:out value="${pcPage}" escapeXml="false" />
		</div>
		<p class="rightBtn"><a id="btn_save_excel" href="#"><img src="${IMG}/common/btn_save.gif" alt="저장" /></a></p>
	</div>
  </section>
  <div class="lnkDebn">* 등록 후 삭제된 거래처가 있을 경우에 실제 등록한 거래처와 다를 수 있습니다.</div>
  &nbsp;&nbsp;&nbsp;&nbsp;
	<%-- //content --%>
</div>
<%-- //contentArea --%>
 
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>