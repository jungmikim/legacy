<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<html>
<head>
<link rel="stylesheet" href="${CSS}/commonLegacy.css" />

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	if("${sBox.viewType}"=="SRCH"){
		location.href = "${kedCustomerSearchUrl}"; 
	}else if("${sBox.viewType}"=="EWRP"){
		location.href = "${kedEwDetailUrl}"; 
	}else if("${sBox.viewType}"=="BRIF"){
		location.href = "${kedCompBrifUrl}"; 	
	}else if("${sBox.viewType}"=="CUMA"){
		location.href = "${kedCustomerManagementUrl}"; 
	}
	
	
});

</script>
</head>
<body>
</body>
</html>