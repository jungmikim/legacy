<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><html class="no-js" lang="ko"><![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf" %>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debenture/getCustomerSearchForm.css" />
<link rel="stylesheet" href="${CSS}/popup2.css"/>	
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		
		// kwd 입력창 포커스
		$('#cust_kwd').focus();
		
		// 페이지 resize
		window.resizeTo(410, 320);
		
		/**
		 * <pre>
		 *   팝업 종료 Event
		 * </pre>
		 * @author yeonjinyoon
		 * @since 2013. 09. 04
		 */		
		$('#btn_cancel').click(function(e){
			window.close();
		});
		 			
		/**
		*	<pre>
		*		사업자 등록번호 중복 체크 로직 Event
		*	</pre>
		*	@author yeonjinyoon
		*	@since 2013.09.04
		*/
		$('#btn_ok').click(function(e){
			if(!$.fn.validate())
				return;
		});
		
		/**
		*	<pre>
		*		거래처 검색 Event
		*	</pre>
		*	@author yeonjinyoon
		*	@since 2013.09.04
		*/
		$('#btn_customer_search').click(function(e){
			
			$custKwd = $.trim($('#cust_kwd').val());
			$custType = $("#cust_type").val();
			$searchType =$('.radioSearchType:checked').attr('id');
			
			if($('.radioSearchType:checked').attr('id') == 'business_no') {
	    		 $("#optionType").html("사업자번호가 ");
			}else if($('.radioSearchType:checked').attr('id') == 'company_name'){
				 $("#optionType").html("회사명이 ");
			}
			
			// 검색창 검증 로직
			if($custKwd == ''){
				alert("검색어를 입력해주세요.");
				$('#cust_kwd').focus();
				return;
			}
			
			if($('.radioSearchType:checked').attr('id') == 'business_no') {
				 if(!typeCheck('numCheck',$.trim($('#cust_kwd').val()))){
					 alert('사업자등록번호는 숫자만 입력가능합니다.');
					 $('#cust_kwd').focus();
					 return;
				 }
			}
			
			// 거래처 검색 Ajax
			$.fn.getCustomerSearch($custKwd, $custType,$searchType);
		});
		
		/**
		*	<pre>
		*		거래처 검색 Keydown Event 
		*	</pre>
		*	@author yeonjinyoon
		*	@since 2013.10.25
		*/		
		$('#cust_kwd').keydown(function(e) {
			if (e.keyCode == 13) {
				$('#btn_customer_search').trigger('click'); 
			}
		});
		
		/**
	     * <pre>
	     *	사업자등록번호 붙여넣기 할 때 '-' 삭제하기
	     * </pre>
	     * @author 백원태
	     * @since 2014. 12. 15
	     */
	     $('#cust_kwd').change(function(){
	    	 if($('#business_no').attr('checked')){
	    	 	$('#cust_kwd').attr('value', $('#cust_kwd').attr('value').replace(/-/g, '')); 
	    	 }
	     });
	     
		/**
		*	<pre>
		*		사업자 등록번호 형식 변환
		*   </pre>
		*/
	     $('#cust_no').live({
		     focus: function() {
				$(this).val(replaceAll($(this).val(), '-', ''));
		     },
		     blur: function(event) {
		    	
		    	$custType = $("#cust_type").val();
		    	$custNo = replaceAll($(this).val(),"-","");
		    	 
				$custNo = $custNo.substr(0,3) + "-" + $custNo.substr(3,2) + "-" + $custNo.substr(5,5);
					
				if($custNo != "--")
					$(this).val($custNo);
				
		     }
		     
	     });
		
	     $('.radioSearchType').on({
	        	change : function() {
	        		$('#cust_kwd').val('');
	        		if($(this).attr('id') == 'business_no') {
	        			$('#cust_kwd').attr('maxlength', '12');
	        		} else {
	        			$('#cust_kwd').attr('maxlength', '70');
	        		}
	        	}
	        });
	     
	  
});
	
	
	/**
	* <pre>
	* 데이터 검증
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 09
	*/
	$.fn.validate = function(){
		
		$result = true;
		
		$custType = $("#cust_type").val();
		$custNm = $.trim($('#cust_nm').val());
		$custNo = replaceAll($.trim($('#cust_no').val()),"-","");
		
		// popup 확인버튼 상태 로직
		if($('#customer_regist_yn').val() == 'N') {
			
			if( $('.cust_radio:checked').length != 0) {
				
				$custNm = $('.cust_radio:checked').next().find('dt:eq(0)').text();
				$custNo = $('.cust_radio:checked').next().find('dt:eq(1)').text().split(':')[1];
				$custId = $('.cust_radio:checked').next().find('dt:eq(2)').text();
				
				$custOwnerNm = $('.cust_radio:checked').next().find('dt:eq(3)').text();
				$custOwnNm = ($custOwnerNm != 'null') ? $custOwnerNm : "";
				
				$custCorpNo = ($('.cust_radio:checked').next().find('dt:eq(4)').text() == 'null')? "" : $('.cust_radio:checked').next().find('dt:eq(4)').text();
				
				$custPostNo = $('.cust_radio:checked').next().find('dt:eq(5)').text();
				$custPost1 = ($custPostNo != 'null') ? $custPostNo : "";
				/* $custPost2 = ($custPostNo != 'null') ? $custPostNo.substring(3,6) : ""; */
				
				$custAddr1 = $('.cust_radio:checked').next().find('dt:eq(6)').text();
				$custAddr1 = ($custAddr1 != 'null') ? $custAddr1 : "";
				
				$custAddr2 = $('.cust_radio:checked').next().find('dt:eq(7)').text();
				$custAddr2 = ($custAddr2 != 'null') ? $custAddr2 : "";
				
				
				$.fn.popupConfirm($custNm, $custNo, $custId, $custOwnNm, $custCorpNo, $custPost1, $custAddr1, $custAddr2);
				
			} else {
				if( $('.cust_radio').length == 0 ) {
					alert("검색어를 입력해주세요.");	
				}else {
					alert("선택된 거래처가 없습니다.");
				}
				return false;
			}
		}else {	
			// 회사명, 이름 검증 로직
			if( $custNm == ''){
				alert("회사명을 입력해주세요");
				$('#cust_nm').focus();
				return false;
			}
			
			// 사업자번호 검증 로직
			$bTF = false;
			
			if( $custNo == ''){
				alert("사업자 등록번호를 입력해주세요"); 
				$bTF = true;
			}else {
				if(!typeCheck('businessNoCheck', $custNo )) {
					alert("사업자 번호 형식에 맞지 않습니다.");
					$bTF = true;
				}
			}
														
			if( $bTF ) {
				$('#cust_no').focus();
				return false;
			}
			
			// 사업자 등록번호 중복 체크 로직 ajax
			$.fn.checkDuplicationCustomer($custNo, $custType);
		}
		
		return $result;
	};
	
	/**
	* <pre>
	*   팝업 확인 Method
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 04
	* @param $custNo : 사업자번호, $custType : 거래처유형(C or I), $custId : 거래처 순번, $custOwnNm : 거래처 대표자명, $custCorpNo : 거래처 법인등록번호, 
	*        $custPost1 : 거래처 우편번호1, $custPost2 : 거래처 우편번호2 , $custAddr1 : 거래처 주소1, $custAddr2 : 거래처 주소2
	*/
	$.fn.popupConfirm = function($custNm, $custNo, $custId, $custOwnNm, $custCorpNo, $custPost1,$custAddr1, $custAddr2){
		
		$custType = $("#cust_type").val();
		
		// 부모창 메소드 실행
		opener.$.fn.exitPopup($custNm, $custNo, $custType, $custId, $custOwnNm, $custCorpNo, $custPost1, $custAddr1, $custAddr2);
		
		window.close();
	};
	
	/**
	* <pre>
	*	사업자 등록번호 중복 체크 로직 Ajax
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 04
	* @param $custNo : 사업자번호, $custType : 거래처유형(C or I)
	*/
	$.fn.checkDuplicationCustomer = function($custNo, $custType) {
		
		$param = "custNo=" + $custNo + "&custType=" + $custType;
		
		$.ajax({
			type : "POST", 
			url : "${HOME}/customer/checkDuplicationCustomer.do",
			dataType : "json",
			data : $param,
			async : false,
			success : function(json_result) {
					$result = json_result.model.resultBox;
					if($result.REPL_CD == "00000"){
						
						$custNm = $("#cust_nm").val();
						$custId = "";
						$custNo = $custNo.substr(0,3) + "-" + $custNo.substr(3,2) + "-" + $custNo.substr(5,5);
						
						$.fn.popupConfirm($custNm, $custNo, $custId);
						
					}else {
						alert($result.REPL_MSG);
					}
				},
				error : function(xmlHttpRequest, textStatus, errorThrown){
					alert("사업자 등록번호 중복 체크  로직중 에러발생 [" + textStatus + "]");
				}
		});
	};
	
	/**
	* <pre>
	*	거래처 검색 Ajax
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 04
	* @param $custKwd : 사업자번호 or 이름 or 회사명, $custType : 거래처유형(C or I)
	*/	
	$.fn.getCustomerSearch = function($custKwd, $custType, $searchType) {
		
		$param = "custKwd=" + $custKwd + "&custType=" + $custType+ "&searchType=" + $searchType;
		
		$.ajax({
			type : "POST", 
			url : "${HOME}/customer/getCustomerSearch.do",
			dataType : "json",
			data : $param,
			async : false,
			success : function(json_result) {
					$result = json_result.model.resultBox;
					
					// 거래처 등록 View 표시 여부
					if($result.customerList.length > 0) {
						window.resizeTo(410, 560);
						
						$('#regist_area').hide();
						$('#customer_regist_yn').val("N");
						$('#cust_nm').val('');
						$('#cust_no').val('');
						
						$('#table_ui_wrap').show();
						$('#table_scroll_wrap').show();
						$('.btnCenter').show();
						$("#btn_ok").show();
						
						if( "${sBox.isSessionCustAddGrn}" != "true") {
							$(".cust_if_txt").hide();
						}
						
						$customerList = "";
						$.each($result.customerList, function($i) {
							
							$radio = '<input type="radio" name="customRadio" class="cust_radio">';
							$custNm = this.CUST_NM;
							$custNo = this.CUST_NO;
							
							$custNo = $custNo.substr(0,3) + "-" + $custNo.substr(3,2) + "-" + $custNo.substr(5,5);
							
							$custId = this.CUST_ID;
							
							$customerList += '<ul><li>' + $radio + '<dl><dt><b>' + $custNm + '</b></dt>';
							$customerList += '<dt>' + '사업자등록번호:' + $custNo + '</dt>';
							$customerList += '<dt style="display:none">' + $custId + '</dt>';
							$customerList += '<dt style="display:none">' + this.OWN_NM + '</dt>';
							$customerList += '<dt style="display:none">'+ this.CORP_NO + '</dt>';
							$customerList += '<dt style="display:none">' + this.POST_NO + '</dt>';
							$customerList += '<dt style="display:none">'+ this.ADDR_1 + '</dt>';
							$customerList += '<dt style="display:none">' + this.ADDR_2 + '</dt></dl></li></ul>';				
						});
						
						$('#table_ui_wrap').empty();
						$('#table_ui_wrap').html($customerList);

					}else {
						$('#table_scroll_wrap').hide();
						$('#table_ui_wrap').hide();
						
						if("${sBox.isSessionCustAddGrn}" == "true") {
							$('.btnCenter').show();
							$('#regist_area').show();
							$('#customer_regist_yn').val("Y");
							$("#btn_ok").show();
							window.resizeTo(410, 360);
						} else {
							$('.btnCenter').hide();
							$(".cust_if_txt").show();
							window.resizeTo(410, 360);
						}
					}
				},
				error : function(xmlHttpRequest, textStatus, errorThrown){
					alert("사업자 등록번호 중복 체크  로직중 에러발생 [" + textStatus + "]");
				}
			});
		};	
</script>
</head>
<body>

	<%-- HIDDEN AREA --%>
	<input type="hidden" id ="cust_type" value="${sBox.custType}" title="구분"/>
	<input type="hidden" id ="customer_regist_yn" value="N" title="거래처 등록 여부"/>

	<%-- 회사명, 사업자등록번호 검색 팝업레이어 --%>
	<div class="popupWrap">
		<h1><p class="content_tit">회사명 · 사업자등록번호 검색</p></h1>

		<div id="viewInfo">
		<div class="noline_box">
		<%-- 
		<div class="tbl01Wrap">
			<table>
			<tbody>
			<tr>
				<td>
				<br/>
				<span class="empty_r10"><input type="radio" class="radioSearchType" id="business_no"  name="searchType"  value="business_no" title="사업자등록번호" /> <label for="business_no">사업자등록번호</label></span>
				<span class="empty_r10"><input type="radio" class="radioSearchType" id="company_name" name="searchType" value="company_name" title="회사명" <c:if test="${empty sBox.searchType}">checked="checked"</c:if> /> <label for="company_name">회사명</label></span>
				<br/><br/>
				<input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12" style="padding:4px 4px 3px 4px; border:1px solid #c4c4c4; width:320px;"/>
				<br/><br/>
				</td>
			</tr> 
			</tbody>
			</table>
		</div> --%>
		
	<%-- 	<span class=".tbl01Wrap td empty_r10"><input type="radio" class="radioSearchType" id="business_no"  name="searchType"  value="business_no" title="사업자등록번호" /> <label for="business_no">사업자등록번호</label></span>
		<span class=".tbl01Wrap td empty_r10"><input type="radio" class="radioSearchType" id="company_name" name="searchType" value="company_name" title="회사명" <c:if test="${empty sBox.searchType}">checked="checked"</c:if> /> <label for="company_name">회사명</label></span> --%>

	<%-- 	<table class="msr_pTbl01" summary="검색결과">
			<colgroup>
				<col width="100%">
			</colgroup>
			<tbody>
			<tr>
				<td>
				<input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12" style="padding:4px 4px 3px 4px; border:1px solid #c4c4c4; width:300px;"/>
				</td>			
			</tr>
		</tbody></table> --%>
		
		<!-- <div id="btnWrap_r">
			<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_customer_search">검색</a></div>
		</div> -->
		
		<div class="popupContents">
		
			<div class="tbl01Wrap" style="width:357px;">
			<table>
			<tbody>
			<tr>
				<td>
				<br/>
				<span class="empty_r10"><input type="radio" class="radioSearchType" id="business_no"  name="searchType"  value="business_no" title="사업자등록번호" /> <label for="business_no">사업자등록번호</label></span>
				<span class="empty_r10"><input type="radio" class="radioSearchType" id="company_name" name="searchType" value="company_name" title="회사명" <c:if test="${empty sBox.searchType}">checked="checked"</c:if> /> <label for="company_name">회사명</label></span>
				<br/><br/>
				<input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12" style="padding:4px 4px 3px 4px; border:1px solid #c4c4c4; width:320px;"/>
				<br/><br/>
				</td>
			</tr> 
			</tbody>
			</table>
		</div>
		
		<div id="btnWrap_r">
			<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_customer_search">검색</a></div>
		</div>
			<%-- <input type="radio" class="radioSearchType" id="business_no"  name="searchType"  value="business_no" title="사업자등록번호" /> <label for="business_no">사업자등록번호</label>
			<input type="radio" class="radioSearchType" id="company_name" name="searchType" value="company_name" title="회사명" <c:if test="${empty sBox.searchType}">checked="checked"</c:if> /> <label for="company_name">회사명</label>
			
			<br/><br/>
			
			<fieldset class="search_company">
				<legend>회사명 · 사업자등록 번호 검색</legend>
				
				<input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12"/> --%>
				<%-- <table class="msr_pTbl02" summary="검색결과">
					<colgroup>
						<col width="100%">
					</colgroup>
					<tbody>
					<tr>
						<th style="font-size: 12px; padding:19px 0 19px 0; font-weight:bold; background-color:#f9f9f9; text-align:center">
						<input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12"/>
						</th>	
					</tr>
				</tbody></table> --%>

				
				
				
				<%-- <input type="text" name="custKwd" id="cust_kwd" title="회사명·사업자등록번호 검색" maxlength="12"/> <a href="#" id="btn_customer_search"><img src="${IMG}/popup/btn_search.gif" alt="검색" /></a> --%>
				
		<!-- 		<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_customer_search">검색</a></div>
				</div>
			</fieldset> -->
			
			<br/><br/>
			
			<div class="wart1">
			<span class="warn_ico1">※ </span>입력한 키워드로 시작하는 회사명을 검색합니다.<br>
			<span class="warn_ico1">※ </span>사업자등록번호로 검색하실 때에는 숫자만 입력하세요.<br>
			</div>
			<!-- <p>*입력한 키워드로 시작하는 회사명을 검색합니다.</p> 
			<p>*사업자등록번호로 검색하실 때에는 숫자만 입력하세요.</p><br/> -->
			<!-- <div class="scroll_msr1" style="width:357px;display:none;" id="table_scroll_wrap"> -->
			<div class="tableUlWrap" id="table_ui_wrap" style="overflow:auto;height:198px;display:none;width:350px;" ></div>
			</div>
			<div id="regist_area" style="display:none;">
				<p class="if_txt">찾으시는 결과가 없을 경우 아래에 직접 입력하셔서 거래처를 등록하시기 바랍니다.</p>
				<div id="new">
					<fieldset class="newbusiness">
						<legend>새로 거래처 등록하기</legend>
						<label for="cust_nm">
							<em><font color="#ffffff"> * </font></em>
							회　　사　　명
						</label>
						<input type="text" name="custNm" id="cust_nm" title="회사명 또는 이름" style="width:100px;" maxlength="70"/>　
			            <div class="cust_no_area">
							<label for="cust_no_area">
								<em><font color="#ffffff"> * </font></em>
								사업자등록번호
							</label>
							<input type="text" name="custNo" id="cust_no" title="사업자등록번호" maxlength="10" style="width:100px;"/>
						</div>
					</fieldset>
				</div>
			</div>
			
<!-- 			<p class="cust_if_txt" style="display:none;">
				1) 검색된 거래처가 존재하지 않습니다.
				<br/>2) 거래처 등록 권한이 있는 경우 거래처 등록이 가능합니다.
			</p> -->
			<div class="wart1 cust_if_txt" style="display:none;">
				1) 검색된 거래처가 존재하지 않습니다.
				<br/>2) 거래처 등록 권한이 있는 경우 거래처 등록이 가능합니다.
			</div>
				
			<div class="btnCenter" style="display:none;">
			<div id="btnWrap_r">
				<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_cancel">취소</a></div>
			</div>
			<div id="btnWrap_r">
				<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_ok">확인</a></div>
			</div>
			</div>	
			
			
		</div>		
		</div>
	</div>	
<!-- 	</div> -->
	<%-- 회사명, 사업자등록번호 검색 팝업레이어 --%>

</body>
</html>