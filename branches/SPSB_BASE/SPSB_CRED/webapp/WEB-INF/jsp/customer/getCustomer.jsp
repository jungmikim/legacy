<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${result.REPL_CD eq '10115' }">
	<script>
		alert('이미 삭제된 거래처 입니다.');
		location.href = '${HOME}/customer/getCustomerList.do';
	</script>
</c:if>

<%-- 거래처 단건 등록 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="commonCode" value="${result.commonCodeBox}" />
<c:set var="customer" value="${result.customerBox}" />
<c:set var="pcPage" value="${result.pcPage}" />

<c:set var="payTermList" value="${result.payTermList}" />
<c:set var="customerCreditHistoryList"	value="${result.customerCreditHistoryList}" />
<c:set var="historyPcPage" value="${result.historyPcPage}" />

<c:set var="customerPlanListTotalCount"	value="${result.customerPlanListTotalCount}" />
<c:set var="customerPlanList" value="${result.customerPlanList}" />
<c:set var="customerPlanListPcPage"	value="${result.customerPlanListPcPage}" />

<c:set var="customerPrsListTotalCount"	value="${result.customerPrsListTotalCount}" />
<c:set var="customerPrsList" value="${result.customerPrsList}" />
<c:set var="customerPrsListPcPage" 	value="${result.customerPrsListPcPage}" />

<c:set var="customerColListTotalCount" 	value="${result.customerColListTotalCount}" />
<c:set var="customerColList" value="${result.customerColList}" />
<c:set var="customerColListPcPage" 	value="${result.customerColListPcPage}" />

<c:set var="phoneCodeList" value="${commonCode.phoneCodeList}" />
<c:set var="emailCodeList" value="${commonCode.emailCodeList}" />
<c:set var="cellPhoneCodeList" value="${commonCode.cellPhoneCodeList}" />
<c:set var="payTermTypeList" value="${commonCode.payTermTypeList}" />

<c:set var="debtProcess" value="${result.debtProcess}" />

<%-- 채무불이행 현재 상태 이력 --%>
<c:set var="debtBox" value="${result.debtBox}" />
<%-- 채무불이행 정보 --%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Cache-Control" content="No-Cache" />
<link rel="stylesheet" href="${CSS}/common.css" />
<%-- <link rel="stylesheet" href="${CSS}/customer/getCustomer.css" /> --%>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	//신용정보 유료회원인 경우 신용정보 이력테이블 유료정보 출력
	if ("${poYn}" == "Y") {
		$('#custCrdPdHistTable').show();
	} else {
		$('#custCrdPdHistTable').hide();
	}
	<%-- 거래처 정보 수정 모드 숨김 --%>
	$('.modifyMode').hide();
	$('.modifyCreditMode').hide();

	/**
	 * <pre>
	 *   결제 조건 추가 중 특정 option을 선택했을 경우
	 *    결제 기준일을 필수값으로 변경시키는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('.payTermType').live('change',function(e) {
		if ($(this).find('option:selected').hasClass('esentialSelect')) {
			$(this).next().addClass('required termType').attr('readonly', false);
			$(this).next().show();
			$(this).next().next().show();
		} else {
			$(this).next().removeClass('required termType').attr('readonly', true).val('');
			$(this).next().hide();
			$(this).next().next().hide();
		}
	});

	 /**
	 * <pre>
	 *   우편번호 앞자리, 뒷자리 클릭 이벤트시 검색 팝업창을 띄움
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 07. 21.
	 */
	/* $(document).on('click', '#post_no1,#post_no2',function(e) {
		open_post();
	}); */

	 /**
	 * <pre>
	 *   이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 26.
	 */
	$('#select_email').live('change',function(e) {
		if ($.trim($(this).find('option:selected').val()) == '') {
			$(this).prev().attr('readonly', false);
		} else {
			$(this).prev().attr('readonly', true);
		}
		$(this).prev().val($(this).val());
	});

	 /**
	 * <pre>
	 *   수정 버튼 클릭 이벤트 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_require_modify').live('click',function(e) {
		$('#customer_temp_area').append($('.customerArea').find('.infoView').clone());
		$.fn.convertModifyMode();
	});

	 /**
	 * <pre>
	 *  수정 취소 버튼 클릭시 수정 상태 원상복귀 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_modify_cancel').click(function(e) {
		if (confirm("거래처 정보 수정을 취소하시겠습니까?")) {
			$('.customerArea').find('.infoView').empty().append($('#customer_temp_area').find('.infoView').children());
			$('#customer_temp_area').empty();
			$.fn.convertNotModifyMode();
		}
	});

	 /**
	 * <pre>
	 *   수정 완료 버튼 클릭 이벤트 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_modify_complete').click(function(e) {
		if ($.fn.validate()&& confirm('입력하신 거래처 정보로 수정하시겠습니까?')) {
			$('#customer_temp_area').empty();
			$.fn.modifyCustomerInfo();
		}
	});

	 /**
	 * <pre>
	 *   여신한도, 초기미수금 클릭시 숫자 ,를 제거하는 함수 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('.numericMoney').live({
		focus : function() {
			$(this).val(replaceAll($(this).val(), ',', ''));
		},
		blur : function() {
			$(this).val(addComma($(this).val()));
		}
	});

	 /**
	 * <pre>
	 *   결제 조건 추가 버튼 클릭 시 엘리먼트 생성 함수 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_add_list').click(function(e) {

		// 현재 수정/추가 상태인지 판단
		if (!$.fn.alreadyActionValidate())
			return false;

		// ajax 페이지 1페이지에서만 결제조건 추가/수정 가능
		if ($('#ajax_num').val() != 1) {
			alert('첫번째 페이지에서만 결제조건을 추가할 수 있습니다.');
			return false;
		}

		// 결제조건 추가/수정 여부 등록
		$(this).addClass('pay_term_add');
		// noSearch 영역(검색 결과가 없습니다.) 삭제
		$('#pay_term_area').find('.noSearch').parent().remove();


		// 결제 조건 존재 여부 검증하여 폼을 다르게 구성
		// 결제조건이 없을 경우		  
		if ($('#pay_term_area').find('li').length == 0) {
			$('#pay_term_area').find('ul').prepend(
					'<li class="addPayTermArea" id="first_pay_term">'
						+ '<dl>'
							+ '<dd>'
								+ '초기 결제조건 :'
								+ '<select id="init_pay_term_type" name="initPayTermType" class="payTermType termType required" title="초기 결제조건">'
									+ '<c:forEach var="initPayTermTypeList" items="${payTermTypeList}">'
										+ '<option value="${initPayTermTypeList.KEY}" <c:if test="${initPayTermTypeList.DAYESSENTIAL eq \'true\'}">class="esentialSelect"</c:if>>${initPayTermTypeList.VALUE}</option>'
									+ '</c:forEach>'
								+ '</select> '
								+ '<input type="text" class="payTermDay numeric unsigned day" name="initPayTermDay" id="init_pay_term_day" title="초기 결제조건 일수" style="position: static;display:none;" size="6" readonly="readonly" maxlength="2"/><span style="display:none;">일</span> <em>|</em>'
								+ '차후 결제조건 :'
								+ '<select id="future_pay_term_type" name="futurePayTermType" class="payTermType termType required" title="차후 결제조건">'
									+ '<c:forEach var="futurePayTermTypeList" items="${payTermTypeList}">'
										+ '<option value="${futurePayTermTypeList.KEY}" <c:if test="${futurePayTermTypeList.DAYESSENTIAL eq \'true\'}">class="esentialSelect"</c:if>>${futurePayTermTypeList.VALUE}</option>'
									+ '</c:forEach>'
								+ '</select> '
								+ '<input type="text" class="payTermDay numeric unsigned day" id="future_pay_term_day" name="futurePayTermDay" title="차후 결제조건 일수" style="position: static;display:none;" size="6" readonly="readonly"/><span style="display:none;">일</span> <em>|</em>'
								+ '결제조건 적용 날짜 : <input type="text" id="pay_term_date" name="payTermDate" class="taxDate payTermDate termType required" title="결제조건 적용 날짜" readonly="readonly"/>'
								+ '<span class="save">'
									+ '<a id="btn_add_first_pay_term" href="#none"><img src="${IMG}/common/btn_s_save.gif"/></a> '
									+ '<a id="btn_add_cancel" href="#none"><img src="${IMG}/common/btn_s_cancel.gif"/></a> '
								+ '</span>'
							+ '</dd>'
						+ '</dl>'
					+ '</li>'
			);
		} else {

		// 결제조건이 있을 경우
			$('#pay_term_area').find('ul').prepend(
					'<li class="addPayTermArea" id="next_pay_term">'
						+ '	<dl>'
						+ '		<dd>'
						+ '			결제조건 : '
						+ '			<select class="payTermType required" title="결제조건">'
						+ '				<c:forEach var="payTermTypeListForAdd" items="${payTermTypeList}">'
						+ '					<option value="${payTermTypeListForAdd.KEY}" <c:if test="${payTermTypeListForAdd.DAYESSENTIAL eq \'true\'}">class="esentialSelect"</c:if>>${payTermTypeListForAdd.VALUE}</option>'
						+ '				</c:forEach>'
						+ '			</select>'
						+ '			<input type="text" class="payTermDay numeric unsigned day" title="결제조건 일수" readonly="readonly" style="position:static;display:none;" size="6" /> <span style="display:none;">일</span><em>|</em>'
						+ '			결제조건 적용기간 : '
						+ '			<input type="text" class="payTermDate required taxDate" title="결제조건 적용기간" /> ~ 차후 결제 조건 등록일까지 '
						+ '			<span class="save"> '
						+ '				<a id="btn_add_next_pay_term" href="#none"><img src="${IMG}/common/btn_s_save.gif"></a>'
						+ '				<a id="btn_add_cancel" href="#none"><img src="${IMG}/common/btn_s_cancel.gif"></a>'
						+ '				<input type="hidden" class="prior_pay_term_type pay_term_param" value="' + $('#pay_term_area li:first').find('.pay_term_type').val() + '"/>'
						+ '				<input type="hidden" class="prior_pay_term_day pay_term_param" value="' + $('#pay_term_area li:first').find('.pay_term_day').val() + '"/>'
						+ '				<input type="hidden" class="post_pay_term_type pay_term_param" value=""/>'
						+ '				<input type="hidden" class="post_pay_term_day pay_term_param" value=""/>'
						+ '			</span>'
						+ '		</dd>'
						+ '	</dl>'
					+ '</li>'
			);
			<%-- 세금계산서 작성일자를 오늘 이후는 선택불가하도록 세팅 --%>
			$('#bill_dt').datepicker('option', 'maxDate','0D');
			<%-- 결제조건이 있을경우 오늘 이후 날짜로만 세팅하도록 함 --%>
			$('.payTermDate').datepicker({minDate : new Date(dateAdd($('#pay_term_area').find('li').eq(1).find('.st_dt').val(), 1))});
		}

		// datepicker dateformat 세팅 
		$('#pay_term_area').find('.taxDate').datepicker().attr('readonly', true);

		// 리스트 백그라운드 재설정
		$.fn.setBackgroundEvenAndOdd($('#pay_term_area'),'#ffffff', '#f6f6f6');

	});

	 /**
	 * <pre>
	 *  결제 조건 추가 취소 버튼 클릭시 row 삭제
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_add_cancel').live('click',function(e) {

		if (confirm("결제 조건 추가를 취소하시겠습니까?")) {
			$('.addPayTermArea').remove();
			if ($('#pay_term_area').find('li').length == 0) {
				$('#pay_term_area>ul').append('<li><div class="noSearch">결과가 없습니다.</div></li>');
			}
			$('.pay_term_add').removeClass('pay_term_add');
			$.fn.setBackgroundEvenAndOdd($('#pay_term_area'),'#ffffff', '#f6f6f6');
		}
	});

	 /**
	 * <pre>
	 *  결제 조건 추가[결제조건이 있을 경우] 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_add_next_pay_term').live('click',function(e) {
		$custIdEl = $('#cust_id');
		$payTermType = $(this).parent().siblings('.payTermType');
		$payTermDay = $(this).parent().siblings('.payTermDay');
		$payTermDate = $(this).parent().siblings('.payTermDate');
		// 추가에서는 리스트에서 2번째 row data의 시작 날짜를 가지고 옴
		$comparisonPayTermDate = $('#pay_term_area').find('li').eq(1).find('.st_dt').val();

		if (!$.fn.validatePayTerm($custIdEl, $payTermType, $payTermDay, $payTermDate, $comparisonPayTermDate)) {
			return false;
		}

		// 직전, 직후 결제조건과 동일여부 검증
		$priorPayTermType = $(this).siblings('.prior_pay_term_type');
		$priorPayTermDay = $(this).siblings('.prior_pay_term_day');
		$postPayTermType = $(this).siblings('.post_pay_term_type');
		$postPayTermDay = $(this).siblings('.post_pay_term_day');

		if (!$.fn.validatePayTermType($(this), $priorPayTermType, $priorPayTermDay, $postPayTermType, $postPayTermDay, $payTermType, $payTermDay)) {
			return false;
		}

		if (confirm("결제 조건을 저장하시겠습니까?")) {
			$.fn.addPayTerm($custIdEl, $payTermType, $payTermDay, $payTermDate);
		}
	});

	 /**
	 * <pre>
	 *  결제 조건 삭제 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('.btn_remove_pay_term').live('click',function(e) {
		if (!$.fn.alreadyActionValidate())
			return false;

		if (confirm('해당 결제 조건을 삭제하시겠습니까?')) {
			$.fn.removePayTerm($('#cust_id'), $(this).siblings('.pay_term_sn'), $(this).siblings('.stat'));
		}
	});

	 /**
	 * <pre>
	 *  결제 조건 수정 요청 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('.btn_require_modify_pay_term').live('click',function(e) {

		// 페이지 상에서 결제 조건 추가/수정이 이루어지는지 판단
		if (!$.fn.alreadyActionValidate())
			return false;

		// 수정 상태 세팅(결제조건을 추가하거나 삭제하는 중에는 다른 추가,수정이 불가능)
		$('#btn_add_list').addClass('pay_term_modify');

		// 선택한 결제 조건 <li>태그 자식 엘리먼트 원본, 원본 복사
		$originalEl = $(this).parent().parent().parent();
		$modifyEl = $originalEl.clone();
		$('#pay_term_temp_area').html($modifyEl);

		// 최근 결제 조건 삭제 및 수정 로직 판단 플래그(적용기간수정 가능 여부 판단)
		$modifyBTF = ($originalEl.parent().is('li#first_list') && ($(this).siblings('.stat').val() != 'P'));

		// 선택한 결제 조건 상위 엘리먼트(li)에 아이디 부여 후 자식 엘리먼트 삭제
		$originalEl.parent().addClass('modifying_pay_term_area');
		$originalEl.remove();

		// 선택한 결제 조건 파라미터 셋
		$paramSet = [
						$(this).siblings('.rownum'),
						$(this).siblings('.pay_term_sn'),
						$(this).siblings('.pay_term_type'),
						$(this).siblings('.pay_term_day'),
						$(this).siblings('.terms_msg'),
						$(this).siblings('.pay_term_day_str'),
						$(this).siblings('.pay_term_type_value'),
						$(this).siblings('.ed_dt'),
						$(this).siblings('.st_dt'),
						$(this).siblings('.stat'),
						$(this).siblings('.prior_pay_term_type'),
						$(this).siblings('.prior_pay_term_day'),
						$(this).siblings('.post_pay_term_type'),
						$(this).siblings('.post_pay_term_day'), 
					];

		// 모두 수정 가능할 경우
		if ($modifyBTF) {
			$('.modifying_pay_term_area').append(
					'	<dl>'
					+ '		<dd>'
					+ '			결제조건 : '
					+ '			<select class="payTermType required" title="결제조건">'
					+ '				<c:forEach var="payTermTypeListForModify" items="${payTermTypeList}">'
					+ '					<option value="${payTermTypeListForModify.KEY}" <c:if test="${payTermTypeListForModify.DAYESSENTIAL eq \'true\'}">class="esentialSelect"</c:if> >${payTermTypeListForModify.VALUE}</option>'
					+ '				</c:forEach>'
					+ '			</select>  '
					+ '			<input type="text" class="payTermDay numeric unsigned day" title="결제조건 일수" style="position: static" size="6" maxlength="2"/> <span style="display:none;">일</span><em>|</em>'
					+ '			결제조건 적용기간 : '
					+ '			<input type="text" class="payTermDate required taxDate" title="결제조건 적용기간" /> ~ 차후 결제 조건 등록일까지 '
					+ '			<input type="hidden" class="payTermSn required" value="' + $paramSet[1].val() + '"/> '
					+ '			<span class="save"> '
					+ '				<a id="btn_modify_pay_term" href="#none"><img src="${IMG}/common/btn_s_save.gif"/></a> '
					+ '				<a id="btn_modify_cancel_pay_term" href="#none"><img src="${IMG}/common/btn_s_cancel.gif"/></a> '
					+ '				<input type="hidden" class="rownum pay_term_param" value="' + $paramSet[0].val() + '"/> '
					+ '				<input type="hidden" class="pay_term_sn pay_term_param" value="' + $paramSet[1].val() + '"/> '
					+ '				<input type="hidden" class="pay_term_type pay_term_param" value="' + $paramSet[2].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_day pay_term_param" value="' + $paramSet[3].val() + '"/>'
					+ '				<input type="hidden" class="terms_msg pay_term_param" value="' + $paramSet[4].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_day_str pay_term_param" value="' + $paramSet[5].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_type_value pay_term_param" value="' + $paramSet[6].val() + '"/>'
					+ '				<input type="hidden" class="ed_dt pay_term_param" value="' + $paramSet[7].val() + '"/>'
					+ '				<input type="hidden" class="st_dt pay_term_param" value="' + $paramSet[8].val() + '"/>'
					+ '				<input type="hidden" class="stat pay_term_param" value="' + $paramSet[9].val() + '"/>'
					+ '				<input type="hidden" class="prior_pay_term_type pay_term_param" value="' + $paramSet[10].val() + '"/>'
					+ '				<input type="hidden" class="prior_pay_term_day pay_term_param" value="' + $paramSet[11].val() + '"/>'
					+ '				<input type="hidden" class="post_pay_term_type pay_term_param" value="' + $paramSet[12].val() + '"/>'
					+ '				<input type="hidden" class="post_pay_term_day pay_term_param" value="' + $paramSet[13].val() + '"/>'
					+ '			</span>'
					+ '		</dd>'
					+ '	</dl>'
			);
			<%-- 결제조건이 있을경우 오늘 이후 날짜로만 세팅하도록 함 --%>
			$('.payTermDate').datepicker({minDate : new Date(dateAdd($('#pay_term_area').find('li').eq(1).find('.st_dt').val(), 1))});

			// datepicker에 저장 되어있던 날짜를 세팅
			$('.modifying_pay_term_area').find('.taxDate').datepicker().val($modifyEl.find('.st_dt').val()).attr('readonly', true);
			
		} else {
			
			// 결제 유형만 변경 가능할 경우
			$('.modifying_pay_term_area').append(
					'	<dl>'
					+ '		<dd>'
					+ '			결제조건 : '
					+ '			<select class="payTermType required" title="결제조건">'
					+ '				<c:forEach var="payTermTypeListForModify" items="${payTermTypeList}">'
					+ '					<option value="${payTermTypeListForModify.KEY}" <c:if test="${payTermTypeListForModify.DAYESSENTIAL eq \'true\'}">class="esentialSelect"</c:if> >${payTermTypeListForModify.VALUE}</option>'
					+ '				</c:forEach>'
					+ '			</select>  '
					+ '			<input type="text" class="payTermDay numeric unsigned day" title="결제조건 일수" style="position: static" size="6" maxlength="2" /><span style="display:none;">일</span> <em>|</em>'
					+ '			결제조건 적용기간 : ' + $paramSet[4].val()
					+ '			<input type="hidden" class="payTermSn required" value="' + $paramSet[1].val() + '"/> '
					+ '			<input type="hidden" class="payTermDate" /> '
					+ '			<span class="save"> '
					+ '				<a id="btn_modify_pay_term" href="#none"><img src="${IMG}/common/btn_s_save.gif"/></a> '
					+ '				<a id="btn_modify_cancel_pay_term" href="#none"><img src="${IMG}/common/btn_s_cancel.gif"/></a> '
					+ '				<input type="hidden" class="rownum pay_term_param" value="' + $paramSet[0].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_sn pay_term_param" value="' + $paramSet[1].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_type pay_term_param" value="' + $paramSet[2].val() + '"/> '
					+ '				<input type="hidden" class="pay_term_day pay_term_param" value="' + $paramSet[3].val() + '"/>'
					+ '				<input type="hidden" class="terms_msg pay_term_param" value="' + $paramSet[4].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_day_str pay_term_param" value="' + $paramSet[5].val() + '"/>'
					+ '				<input type="hidden" class="pay_term_type_value pay_term_param" value="' + $paramSet[6].val() + '"/>'
					+ '				<input type="hidden" class="ed_dt pay_term_param" value="' + $paramSet[7].val() + '"/>'
					+ '				<input type="hidden" class="st_dt pay_term_param" value="' + $paramSet[8].val() + '"/>'
					+ '				<input type="hidden" class="stat pay_term_param" value="' + $paramSet[9].val() + '"/>'
					+ '				<input type="hidden" class="prior_pay_term_type pay_term_param" value="' + $paramSet[10].val() + '"/>'
					+ '				<input type="hidden" class="prior_pay_term_day pay_term_param" value="' + $paramSet[11].val() + '"/>'
					+ '				<input type="hidden" class="post_pay_term_type pay_term_param" value="' + $paramSet[12].val() + '"/>'
					+ '				<input type="hidden" class="post_pay_term_day pay_term_param" value="' + $paramSet[13].val() + '"/>'
					+ '			</span>'
					+ '		</dd>'
					+ '	</dl>'
			);
		}

		// 새로 구성한 element 중 select box(결제 유형)를 저장 값에 따라 선택하고 적용 기준일 readonly 여부 판단하여 속성 추가
		$('.modifying_pay_term_area').find('.payTermType>option').each(function() {
			if ($(this).val() == $modifyEl.find('.pay_term_type').val()) {
				if ($(this).hasClass('esentialSelect')) {
					$(this).parent().next().attr('readonly', false);
					$(this).parent().next().val($modifyEl.find('.pay_term_day').val());
					$(this).parent().next().addClass('required');
					$(this).parent().next().show();
					$(this).parent().next().next().show();
				} else {
					$(this).parent().next().attr('readonly', true);
					$(this).parent().next().val('');
					$(this).parent().next().removeClass('required');
					$(this).parent().next().hide();
					$(this).parent().next().next().hide();
				}
				$(this).attr('selected', 'selected');
				return false;
			}
		});

	});

	 /**
	 * <pre>
	 *  결제 조건 수정 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_modify_pay_term').live('click',function(e) {

		$custIdEl = $('#cust_id');
		$payTermType = $(this).parent().siblings('.payTermType');
		$payTermDay = $(this).parent().siblings('.payTermDay');
		$payTermDate = $(this).parent().siblings('.payTermDate');
		$payTermSn = $(this).parent().siblings('.payTermSn');
		// 수정에서는 리스트에서 2번째 row data의 end data를 가지고 옴
		$comparisonPayTermDate = $('#pay_term_area').find('li').eq(1).find('.ed_dt').val();

		if (!$.fn.validatePayTerm($custIdEl, $payTermType, $payTermDay, $payTermDate, $comparisonPayTermDate)) {
			return false;
		}

		// 직전, 직후 결제조건과 동일여부 검증
		$priorPayTermType = $(this).siblings('.prior_pay_term_type');
		$priorPayTermDay = $(this).siblings('.prior_pay_term_day');
		$postPayTermType = $(this).siblings('.post_pay_term_type');
		$postPayTermDay = $(this).siblings('.post_pay_term_day');

		if (!$.fn.validatePayTermType($(this), $priorPayTermType, $priorPayTermDay, $postPayTermType, $postPayTermDay, $payTermType, $payTermDay)) {
			return false;
		}

		if (confirm("결제 조건을 수정하시겠습니까?")) {
			$.fn.modifyPayTerm($custIdEl, $payTermType, $payTermDay, $payTermDate, $payTermSn);
		}
	});

	 /**
	 * <pre>
	 *  결제 조건 수정 취소 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_modify_cancel_pay_term').live('click',function(e) {

		if (confirm("결제 조건 수정을 취소하시겠습니까?")) {
			$('.modifying_pay_term_area').children().remove();
			$('.modifying_pay_term_area').append($('#pay_term_temp_area').html());
			$('#pay_term_temp_area').html('');
			$('.pay_term_modify').removeClass('pay_term_modify');
			$('.modifying_pay_term_area').removeClass('modifying_pay_term_area');
		}
	});

	 /**
	 * <pre>
	 *   거래처 리스트로 이동하는 함수 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 29.
	 */
	$('#btn_get_list').click(function() {
		location.href = '${HOME}/customer/getCustomerList.do';
	});
	
	/**
	 * <pre>
	 *   채권 리스트로 전체채권 조회 [W : 전체채권] 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 13.
	 */
	$(document).on('click','#lnk_debn_cnt',	function(e) {
		$('#num').val(1);
   		$('#nonpaymentType').val('');
   		$('#debnSearchType').val("business_no");
   	 	$('#searchPeriodType').val("NO_CONDITION");
    	$('#bizKwd').val($('#cust_no').val());
		$('form').attr({'action' : '${HOME}/debenture/getDebentureList.do','method' : 'POST'}).submit();
	});

	/**
	 * <pre>
	 *   채권 리스트로 미수 조회 [A : 미수채권] 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 13.
	 */
	$(document).on('click','#lnk_debn_col_cnt',function(e) {
		$('#num').val(1);
   		$('#nonpaymentType').val('');
   		$('#debnSearchType').val("business_no");
   	 	$('#searchPeriodType').val("NO_CONDITION");
    	$('#bizKwd').val($('#cust_no').val());
    	$('form').attr({'action':'${HOME}/debenture/getDebentureList.do', 'method':'POST'}).submit();
	});
						 
	/**
	 * <pre>
	 *   미수금액 클릭 함수[미수 채권 건수 클릭과 동일] 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 01.
	 */
	$(document).on('click', '#lnk_sum_debn_col',function(e) {
		$('#lnk_debn_col_cnt').trigger('click');
	});

	/**
	 * <pre>
	 *   여신한도 초과 되었을 경우 클릭 함수 [미수 채권 건수 클릭과 동일]
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 06. 05.
	 */
	$(document).on('click', '#lnk_crd_ltd', function(e) {
		$('#lnk_debn_col_cnt').trigger('click');
	});

	 /**
	 * <pre>
	 *   여신한도 초과 되었을 경우 hover/blur 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 06. 05.
	 */
	$(document).on('mouseover', '#lnk_crd_ltd', function(e) {
		$('#lnk_crd_ltd').css({'text-decoration' : 'underline', 'cursor' : 'pointer'});
	});

	 /**
	 * <pre>
	 *   여신한도 초과 되었을 경우 hover/blur 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 06. 05.
	 */
	$(document).on('mouseleave', '#lnk_crd_ltd', function(e) {
		$('#lnk_crd_ltd').css({'text-decoration' : 'none', 'cursor' : 'normal'});
	});

	 /**
	 * <pre>
	 *  거래처 삭제 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 15
	 */
	$('#btn_remove_customer').click(function(e) {

		if (confirm('거래처 정보를 삭제하시겠습니까?')) {
			$('form').find('input[type=hidden]:first').before(
							'<input type="hidden" name="customerIdList" value="' + $('#cust_id').val() + '"/>');
			$('form').attr({'action' : '${HOME}/customer/removeCustomer.do', 'method' : 'POST' }).submit();
		}
	});

	 /**
	 * <pre>
	 *  결제 조건 추가[결제조건이 없을 경우] 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 14.
	 */
	$('#btn_add_first_pay_term').live('click', function(e) {

		$custIdEl = $('#cust_id');
		$initPayTermType = $('#init_pay_term_type');
		$initPayTermDay = $('#init_pay_term_day');
		$futurePayTermType = $('#future_pay_term_type');
		$futurePayTermDay = $('#future_pay_term_day');
		$payTermDate = $('#pay_term_date');

		// 결제조건 추가(결제조건이 없을 경우) 입력값 검증
		if (!$.fn.validatePayTerm($custIdEl, $initPayTermType, $initPayTermDay, $payTermDate)
				|| !$.fn.validatePayTerm($custIdEl, $futurePayTermType, $futurePayTermDay, $payTermDate)) {
			return false;
		}

		// 결제조건 추가(결제조건이 없을 경우) 동일 여부 검증
		if ($initPayTermType.find('option:selected').hasClass('esentialSelect') 
				|| $futurePayTermType.find('option:selected').hasClass('esentialSelect')) {
			if (($initPayTermType.val() == $futurePayTermType.val())
					&& ($initPayTermDay.val() == $futurePayTermDay.val())) {
				return $.fn.validateResultAndAlert($initPayTermType, $initPayTermType.attr('title') + '은(는) 차후 결제조건과 동일할 수 없습니다.');
			}
		} else {
			if ($initPayTermType.val() == $futurePayTermType.val()) {
				return $.fn.validateResultAndAlert($initPayTermType, $initPayTermType.attr('title') + '은(는) 차후 결제조건과 동일할 수 없습니다.');
			}
		}

		if (confirm("결제 조건을 저장하시겠습니까?")) {
			$.fn.addFirstPayTerm($custIdEl, $initPayTermType, $initPayTermDay, $futurePayTermType, $futurePayTermDay, $payTermDate);
		}

	});

	 /**
	 * <pre>
	 *   거래처 수금 계획 상태 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17.
	 */
	$('.lnkCustomerPlan').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custPlnSn = $(this).attr('id').replace(/[^0-9]/g, '');
			$custId = $('#cust_id').val();
			$url = '';
			$width = 700;
			$height = 520;

			// 계획중
			if ($(this).hasClass('STAT_P')) {
				$url = '${HOME}/customer/getCustomerPlanForm.do?';
			}
			// 연기
			else if ($(this).hasClass('STAT_D')) {
				$url = '${HOME}/customer/getCustomerPlanDelayDetail.do?';
				$width = 700;
				$height = 470;
			}
			// 부분수금
			else if ($(this).hasClass('STAT_S')) {
				$url = '${HOME}/customer/getCustomerColForm.do';
				$url += '?pageType=S';
				$width = 700;
				$height = 470;
			}
			// 완전수금
			else if ($(this).hasClass('STAT_C')) {
				$url = '${HOME}/customer/getCustomerColForm.do';
				$url += '?pageType=C';
				$width = 700;
				$height = 470;
			}

			$url += '&custId=' + $custId;
			$url += '&custPlnSn=' + $custPlnSn;
			winOpenPopup($url, 'getCustomerPlanPopup', $width, $height, 'no');
		}
	});

	 /**
	 * <pre>
	 *  수금계획 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17.
	 */
	$('#btn_cust_pln_regist').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();

			$url = '${HOME}/customer/getCustomerPlanForm.do';
			$url += '?custId=' + $custId;
			winOpenPopup($url, 'getCustomerPlanPopup', 700, 520, 'no');
		}
	});

	 /**
	 * <pre>
	 *  거래처 수금계획 연기 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 */
	$('.delayCol').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();
			$custPlnSn = $(this).parent().parent().find('.lnkCustomerPlan').attr('id').replace(/[^0-9]/g, '');

			$url = '${HOME}/customer/getCustomerPlanDelayForm.do';
			$url += '?custId=' + $custId;
			$url += '&custPlnSn=' + $custPlnSn;
			winOpenPopup($url, 'getCustomerPlanPopup', 700, 460, 'no');
		}
	});

	 /**
	 * <pre>
	 *  거래처 수금계획 삭제
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 */
	$('#btn_cust_pln_remove').live({
		click : function(e) {

			if ($('.chkCustPlnSn:checked').length == 0) {
				alert('선택항목이 존재하지 않습니다.');
				return false;
			}

			if (confirm("거래처 수금계획을 삭제하시겠습니까?")) {
				$.fn.removeCustomerPlan();
			}
		}
	});

	 /**
	 * <pre>
	 *  거래처 완전수금 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 24.
	 */
	$('.completedCol').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();
			$custPlnSn = $(this).parent().parent().find('.lnkCustomerPlan').attr('id').replace(/[^0-9]/g, '');

			$url = '${HOME}/customer/getCustomerColForm.do';
			$url += '?custId=' + $custId;
			$url += '&custPlnSn='+ $custPlnSn;
			$url += '&pageType=C';
			winOpenPopup($url,'getCustomerPlanPopup', 700, 520, 'no');
		}
	});

	 /**
	 * <pre>
	 *  거래처 부분수금 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25.
	 */
	$('.sectionCol').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();
			$custPlnSn = $(this).parent().parent().find('.lnkCustomerPlan').attr('id').replace(/[^0-9]/g, '');

			$url = '${HOME}/customer/getCustomerColForm.do';
			$url += '?custId=' + $custId;
			$url += '&custPlnSn=' + $custPlnSn;
			$url += '&pageType=S';
			winOpenPopup($url, 'getCustomerPlanPopup', 700, 550, 'no');
		}
	});

	 /**
	 * <pre>
	 *  독촉관리 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 */
	$('#btn_cust_prs_regist').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();

			$url = '${HOME}/customer/getCustomerPrsForm.do';
			$url += '?custId=' + $custId;
			winOpenPopup($url, 'getCustomerPrsPopup', 700, 570, 'yes');
		}
	});

	 /**
	 * <pre>
	 *   거래처 독촉일자 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 */
	$('.lnkCustomerPrs').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custPrsSn = $(this).attr('id').replace(/[^0-9]/g, '');
			$custId = $('#cust_id').val();
			$url = '${HOME}/customer/getCustomerPrsForm.do?';
			$url += '&custId=' + $custId;
			$url += '&custPrsSn=' + $custPrsSn;
			winOpenPopup($url, 'getCustomerPrsPopup', 700, 520, 'yes');
		}
	});

	 /**
	 * <pre>
	 *   거래처 독촉 삭제 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30.
	 */
	$('#btn_cust_prs_remove').live({
		click : function(e) {

			if ($('.chkCustPrsSn:checked').length == 0) {
				alert('선택항목이 존재하지 않습니다.');
				return false;
			}

			if (confirm("거래처 독촉을 삭제하시겠습니까?")) {
				$.fn.removeCustomerPrs();
			}
		}
	});

	 /**
	 * <pre>
	 *    수금업무 작성
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30.
	 */
	$('#btn_cust_col_regist').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custId = $('#cust_id').val();

			$url = '${HOME}/customer/getCustomerColNoPlanForm.do';
			$url += '?custId=' + $custId;
			winOpenPopup( $url, 'getCustomerColNoPlanPopup', 800, 535, 'yes');
		}
	});

	 /**
	 * <pre>
	 *    수금업무 수정
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30.
	 */
	$('.lnkCustomerCol').live({
		click : function(e) {
			<%-- 파라미터 세팅 --%>
			$custColSn = $(this).attr('id').replace(/[^0-9]/g, '');
			$custId = $('#cust_id').val();
			$url = '${HOME}/customer/getCustomerColNoPlanForm.do?';
			$url += '&custId=' + $custId;
			$url += '&custColSn=' + $custColSn;
			winOpenPopup($url, 'getCustomerColNoPlanPopup', 800, 535, 'yes');
		}
	});

	 /**
	 * <pre>
	 *   거래처 수금 삭제 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 01.
	 */
	$('#btn_cust_col_remove').live({
		click : function(e) {
			$plnCount = 0;

			if ($('.chkCustColSn:checked').length == 0) {
				alert('선택항목이 존재하지 않습니다.');
				return false;
			}

			$.each($('.chkCustColSn:checked'), function() {
				$plnCount += $(this).siblings('.hiddenPlnSn').length;
			});

			if ($plnCount > 0) {
				if (confirm("수금계획에서 수금한 내역은 수금계획도 함께 삭제됩니다.\n거래처 수금을 삭제하시겠습니까?")) {
					$.fn.removeCustomerCol();
				}
			} else {
				if (confirm("거래처 수금을 삭제하시겠습니까?")) {
					$.fn.removeCustomerCol();
				}
			}
		}
	});

	 /**
	 * <pre>
	 *   채무 불이행 등록 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 01.
	 */
	$(document).on('click', '#btn_nonpayment_regist', function() {
		location.href = "${HOME}/debt/getDebtForm.do?custId=" + $("#cust_id").val();
	});

	 /**
	 * <pre>
	 *   거래처 유형 선택할 경우 법인등록번호 필드 동적 생성/삭제 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 27.
	 */
	$(document).on('click', '.custType', function() {

		// 법인 영역 삭제  
		$('.corpNoDataArea').remove();

		// 법인 사업자일 경우 해당 영역 추가
		if ($(this).val() == 'C') {
			$('dd.emailDataArea:last').after(
				'<dt class="first corpNoDataArea">'
					+ '<span><label for="corp_no">법인등록번호</label></span>'
				+ '</dt>'
				+ '<dd class="first corpNoDataArea">'
					+ '<span class="modifyMode">'
						+ '<input type="text" name="corpNo" id="corp_no" class="txtInput numeric" title="법인등록번호" maxlength="13" value=""/>'
					+ '</span>'
					+ '<span class="notModifyMode" id="corp_no_not_modify_area">'
					+ '</span>'
				+ '</dd>');
		}
	});

	 /*
	 * <pre>
	 *  채무불이행 상태에 따른 버튼 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014.05.10
	 */
	$(document).on('click', '.ApplyBtn_Type2', function() {
		// 채무불이행 정정 요청
		if ($(this).hasClass( 'btnDebtReqCor')) {
			$url = '${HOME}/debt/getDebtRequireModify.do?';
			$url += 'debtApplId=' + $('#debt_appl_id').val();
			$url += '&custId=' + $('#cust_id').val();
			window.open($url, 'getDebtRequireModifyPopup', 'width=400,height=420,scrollbars=no');
		}
		// 채무불이행 해제 요청
		else if ($(this).hasClass('btnDebtReqRel')) {
			$url = '${HOME}/debt/getDebtRequireRelease.do?';
			$url += 'debtApplId=' + $('#debt_appl_id').val();
			$url += '&custId=' + $('#cust_id').val();
			window.open($url, 'getDebtRequireReleasePopup', 'width=400,height=270,scrollbars=no');
		}
		// 신청취소
		else if ($(this).hasClass('btnApplyCancel')) {
			$url = '${HOME}/debt/getDebtApplyCancel.do?';
			$url += 'debtApplId=' + $('#debt_appl_id').val();
			$url += '&custId=' + $('#cust_id').val();
			window.open($url, 'getDebtApplyCancelPopup', 'width=400,height=270,scrollbars=no');
		}
		// TODO 결제준비중임
		else if ($(this).hasClass('btnPayment')) {
			alert('결제 준비중입니다.');
		}
		// 목록
		else if ($(this).hasClass('btnMoveList')) {
			location.href = "${HOME}/debt/getDebtOngoingList.do";
		}
	});

	 /*
	 * <pre>
	 *  거래처 유형 label 클릭 이벤트 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 29
	 */
	$(document).on('click', '.custTypeLabel', function(e) {
		e.preventDefault();
		$('input#' + $(this).attr('for')).trigger('click');
	});

	// 기존 Tab을 클릭했을때 frontlib.js 에서 실행되는 이벤트는 중지한다.
	$("#content > .tab_t1.tab_t1View > ul > li > a").unbind('click');

	/*
	 * <pre>
	 *  Tab 메뉴 클릭시 이벤트
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.26
	 */
	$("#content > .tab_t1.tab_t1View > ul > li > a").click(function(e) {

		// [1] 현재 탭 정보를 저장한다.
		$("#current_tab").val($(this).attr("id"));

		// [2] 탭을 클릭시 마다 form을 reload한다.
		$('form').attr({'action' : '${HOME}/customer/getCustomer.do', 'method' : 'POST'}).submit();
	});
	/*
	 * <pre>
	 *  신용정보 확인하기 버튼 클릭 함수
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015.06.22
	 */
	$('.crdInfo').click(function() {
		var custNo = $('#cust_no').val();
		var ewCd = $('#ew_cd').val();
		var errorMessage = "신용정보를 조회할수 있는 필수값이 존재 하지 않습니다.\n관리자에게 문의해주세요.";
		 if('${sBox.isSessionCustEwDetailGrnt}'== 'true'){
			if($.trim(custNo).length != 0){
	  			if($.trim(ewCd).length != 0){
	  				var compId= "${sBox.compId}";
	  				
	  				window.open("${HOME}/customer/getEwReportForKED.do?compId="+compId+"&bizNo="+custNo, "popup", "width=1000, height=700, resizable=no, scrollbars=yes, menubar=no, toobar=no, status=no, location=no, ");
	  			}else{
				 	alert(errorMessage);
				}
	  		}else{
			 	alert(errorMessage);
			}
		 }else{
			 alert("조기경보 상세 권한이 존재하지 않습니다.\n관리자에게 문의하시기 바랍니다.");
		 }
		  
	});

	 /*
	 * <pre>
	 *  거래처 정보 인쇄 버튼 Click Event
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.06.08
	 */
	$(document).on('click', '.btn_custInfoPrint', function(e) {

		// [1] print 모드를 킴	
		$("#use_print").val("Y");

		// [2] 거래처 정보 탭으로 이동함
		$("#cust_info").trigger('click');

	});

	 /*
	 * <pre>
	 *  거래처 독촉 이동 버튼 Click Event
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.06.08
	 */
	$(document).on('click', '.btn_moveCustPrs', function(e) {

		// [1] 거래처 독촉 탭으로 이동함
		$("#cust_prs").trigger('click');

	});

	 /*
	 * <pre>
	 * 채권가이드 요약의 채무불이행 등록 버튼 Click Event
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.06.09
	 */
	$(document).on('click', '.btn_registDebt', function(e) {
		if ($('#btn_nonpayment_regist').length > 0) {
			$('#btn_nonpayment_regist').trigger('click');
		} else {
			alert('현재 진행중인 채무불이행이 있습니다.');
		}
	});

	 /**
	 * <pre>
	 *  거래처 요약 탭에서 처리할 것 클릭할 경우 채권 리스트로 이동하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 06. 09
	 */
	$(document).on('click', '.lnkMoveDebentureList', function() {
		if ($.trim($(this).text()) != '0건') {
			$('#summary_flag').val('Y');
			$('#dgs_mn_kw_id').val($(this).attr('data-dgs-mn-kw-id'));
			$('form').attr({'action' : '${HOME}/debenture/getDebentureList.do', 'method' : 'POST'}).submit();
		} else {
			alert("처리 해야할 채권이 존재하지 않습니다.");
			return false;
		}
	});

	 /**
	 * <pre>
	 *  거래처 요약 필요 첨부파일 다운로드
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 06. 19
	 */
	$(document).on('click', '.btn_hwp , .btn_word , .btn_wordMapping', function() {

		$className = $(this).attr('class');
		$guideType = $(this).parent().attr('data-guideType');
		$fileType = $(this).parent().attr('data-fileType');
		$fileTypeSub = $(this).attr('data-fileTypeSub'); // 별지의 경우

		// 한글 아이콘을 Click 했을경우
		if ($className == 'btn_hwp') {
			$.fn.getDebntureGuideFileDownload('hwp', $guideType, $fileType, $fileTypeSub);
		}
		// 워드 아이콘을 Click 했을경우
		if ($className == 'btn_word') {
			$.fn.getDebntureGuideFileDownload('doc', $guideType, $fileType, $fileTypeSub);
		}
		// 워드 맵핑하기 아이콘을 Click 했을경우
		if ($className == 'btn_wordMapping') {
			$.fn.getDebntureGuideFileDownload('wordMapping', $guideType, $fileType, $fileTypeSub);
		}
	});

	 /**
	 * <pre>
	 *  거래처 요약 메인 키워드 클릭 이벤트
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 07. 17
	 */
	$(document).on('click', '.mkTxt', function(e) {

		$dgsMnKwId = $(this).parent().attr('data-dgs-mn-kw-id'); // 메인 키워드

		// 서브키워드 의미 조회 팝업창 호출
		winOpenPopup('${HOME}/customer/getDebentureGuideKeywordMeanByMKeyword.do?dgsMnKwId=' + $dgsMnKwId,
				'getDebentureGuideKeywordMeanByMKeyword', 670, 350, 'yes');
	});

	 /**
	 * <pre>
	 *  거래처 요약 해제완료 버튼 클릭 이벤트
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 07. 17
	 */
	$(document).on('click', '.btnComplete', function(e) {
		$dgsMnKwId = $(this).parent().find('.lnkMoveDebentureList').attr('data-dgs-mn-kw-id'); // 메인 키워드
		// [3] 채권 진단 키워드 수정 Ajax 호출
		$.fn.modifyDebentureGuideKeywordMNG($(this), $('#cust_id').val(), $dgsMnKwId, 'T', 'C');
	});
	 
	 <%-- 최근 클릭한 탭을 보여준다. --%>
	if ($("#current_tab").val() != '') {
		$pageObj = $("#" + $("#current_tab").val());
		$pageObj.parent().parent().find('a').removeClass('tab_on');
		$pageObj.addClass('tab_on');

		$prev = $("#content > .tab_t1.tab_t1View > ul > li > a").first();
		$($prev.attr('href')).css({"z-index" : "1", "display" : "none"});
		$($pageObj.attr('href')).css({"z-index" : "2", "display" : "block"});

		prev = $pageObj;
	}
	
	<%-- 거래처 요약 Tab 을 클릭한 경우 채권가이드 요약을 호출한다. --%>
	if ($("#current_tab").val() == 'cust_summaray') {
		$.fn.getDebentureGuideAbstract();
	}
	
	<%-- 거래처 정보 Tab 을 클릭한 경우 print 모드에 따라 print 창을 호출하도록 함 --%>
	if ($("#current_tab").val() == 'cust_info' && $("#print_yn").val() == 'Y') {
		window.print();
	}
	
	<%-- 탭영역의 contents를 다시 보이게 함.(페이지 로딩이 느릴경우, 첫페이지가 잠시 보이는 현상 해결) --%>
	$('.tab_box').show();
	
	<%-- 페이지 단어단위로 줄바꿈이 출력되도록 재배치함 --%>
	$('.border').wordBreakKeepAll();
});
<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%>
/**
 * <pre> 
 *   결제 조건 유형, 결제 조건 날짜 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 07. 22
 *
 * @param $modifyBtnEl : 저장버튼 엘리먼트
 * @param $priorPayTermType : 이전 결제 유형
 * @param $priorPayTermDay : 이전 결제 기준일
 * @param $postPayTermType : 이후 결제 유형
 * @param $postPayTermDay : 이후 결제 기준일
 * @param $payTermType : 결제 유형
 * @param $payTermDay : 결제 기준일
 */

$.fn.validatePayTermType = function($modifyBtnEl, $priorPayTermType, $priorPayTermDay, $postPayTermType, $postPayTermDay, $payTermType, $payTermDay) {

	$result = true;
	
	<%-- 이전 결제조건 동일 여부 검증 --%>
	if($priorPayTermType.val() != '') {
		if ($payTermType.find('option:selected').hasClass('esentialSelect')) {
			if (($priorPayTermType.val() == $payTermType.val()) && ($priorPayTermDay.val() == $payTermDay.val())) {
				return $.fn.validateResultAndAlert($payTermType, $payTermType.attr('title') + '은(는) 이전 결제조건과 동일할 수 없습니다.');
			}
		} else {
			if ($priorPayTermType.val() == $payTermType.val()) {
				return $.fn.validateResultAndAlert($payTermType, $payTermType.attr('title') + '은(는) 이전 결제조건과 동일할 수 없습니다.');
			}
		}
	}
	
	<%-- 이후 결제조건 동일 여부 검증 --%>
	if ($postPayTermType.val() != '') {
		if ($payTermType.find('option:selected').hasClass('esentialSelect')) {
			if (($postPayTermType.val() == $payTermType.val()) && ($postPayTermDay.val() == $payTermDay.val())) {
				return $.fn.validateResultAndAlert($payTermType, $payTermType.attr('title') + '은(는) 이후 결제조건과 동일할 수 없습니다.');
			}
		} else {
			if ($postPayTermType.val() == $payTermType.val()) {
				return $.fn.validateResultAndAlert($payTermType, $payTermType.attr('title') + '은(는) 이후 결제조건과 동일할 수 없습니다.');
			}
		}
	}

	return $result;
};

	/**
	 * <pre> 
	 *   <li> 태그 리스트 백그라운드 CSS 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 */
	$.fn.setBackgroundEvenAndOdd = function($targetEl, $evenColor, $oddColor) {
		$targetEl.find('li:even').css('background-color', $evenColor);
		$targetEl.find('li:odd').css('background-color', $oddColor);
	};

	/**
	 * <pre> 
	 *   이미 수행중인 결제조건 추가/수정이 있는지 확인하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 */
	$.fn.alreadyActionValidate = function() {

		if ($('#btn_add_list').hasClass('pay_term_add')) {
			alert('신규 결제조건이 추가중인 경우에는 타 결제조건을 수정할 수 없습니다.');
			return false;
		}

		if ($('#btn_add_list').hasClass('pay_term_modify')) {
			alert('수정 중인 결제 조건의 등록을 완료해주세요.');
			return false;
		}

		return true;
	};

	/**
	 * <pre> 
	 *   결제 조건 검증 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 * @param $custId : 거래처 순번
	 * @param $payTermType : 결제 유형
	 * @param $payTermDay : 결제 기준일
	 * @param $payTermDate : 결제 적용 기간
	 * @param $comparisonPayTermDate : 결제 적용기간 비교 대상
	 */

	$.fn.validatePayTerm = function($custId, $payTermType, $payTermDay, $payTermDate, $comparisonPayTermDate) {

		$result = true;
		$paramSet = [ $custId, $payTermType, $payTermDay, $payTermDate, $comparisonPayTermDate ];

		$.each($paramSet, function(i) {
			// 결제 조건의 '선택' 값이 value 가 NONE인 것도 검증 
			if ($(this).hasClass('required') && ($.trim($(this).val()) == '' || $.trim($(this).val()) == 'NONE')) {
				$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '을(를) 입력해주세요.');
				return false;
			}
		});
		if (!$result){
			return $result;
		}

		// 결제 조건 검증[숫자검증]
		if ($payTermDay.hasClass('required')) {
			if (!typeCheck('numCheck', $.trim($payTermDay.val()))) {
				return $.fn.validateResultAndAlert($payTermDay, $payTermDay.attr('title') + '은(는) 숫자만 입력 가능합니다.');
			}
			if ($.trim($payTermDay.val()) > 31 || $.trim($payTermDay.val()) <= 0) {
				return $.fn.validateResultAndAlert($payTermDay, $payTermDay.attr('title') + '은(는) 1~31 사이의 값이어야 합니다.');
			}
		}

		// 결제 적용 기간 검증[결제조건 상태가 이후등록(N)인 경우만 결제조건에 대해서만 결제 적용기간 검증]
		if ($payTermType.find('input.stat').val() == 'N') {
			if (Date.parse($comparisonPayTermDate) > Date.parse($payTermDate.val())) {
				return $.fn.validateResultAndAlert($payTermDate, $payTermDate.attr('title') + '은(는) 직전 적용기간의 시작날짜보다 이후 일자여야 합니다.');
			}
		}

		return $result;
	};

	/**
	 * <pre> 
	 *   리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadPayTermList = function($target, $listEl) {
		$.each($listEl, function() {
			$target.append(
				'<li>' 
				+ '	<dl>' 
				+ '		<dd>'
				+ '			결제조건 : ' + this.PAY_TERM_TYPE_VALUE + ' ' + this.PAY_TERM_DAY_STR + ' <em>|</em> 결제조건 적용기간 : ' + this.TERMS_MSG
				+ '			<span class="save">'
				+ '				<a class="btn_require_modify_pay_term" href="#none"><img src="${IMG}/common/btn_s_modify.gif"></a>            '
				+ '				<input type="hidden" class="rownum pay_term_param" value="' + this.ROWNUM+ '"/>       '
				+ '				<input type="hidden" class="pay_term_sn pay_term_param" value="' + this.PAY_TERM_SN+ '"/>       '
				+ '				<input type="hidden" class="pay_term_type pay_term_param" value="' + this.PAY_TERM_TYPE+ '"/> '
				+ '				<input type="hidden" class="pay_term_day pay_term_param" value="' + (this.PAY_TERM_DAY != null ? this.PAY_TERM_DAY : '') + '"/>    '
				+ '				<input type="hidden" class="terms_msg pay_term_param" value="' + this.TERMS_MSG + '"/>'
				+ '				<input type="hidden" class="pay_term_day_str pay_term_param" value="' + this.PAY_TERM_DAY_STR + '"/>'
				+ '				<input type="hidden" class="pay_term_type_value pay_term_param" value="' + this.PAY_TERM_TYPE_VALUE + '"/>'
				+ '				<input type="hidden" class="ed_dt pay_term_param" value="' + this.ED_DT+ '"/>'
				+ '				<input type="hidden" class="st_dt pay_term_param" value="' + this.ST_DT+ '"/>'
				+ '				<input type="hidden" class="stat pay_term_param" value="' + this.STAT+ '"/>'
				+ '				<input type="hidden" class="prior_pay_term_type pay_term_param" value="' + this.PRIOR_PAY_TERM_TYPE + '"/>'
				+ '				<input type="hidden" class="prior_pay_term_day pay_term_param" value="' + this.PRIOR_PAY_TERM_DAY + '"/>'
				+ '				<input type="hidden" class="post_pay_term_type pay_term_param" value="' + this.POST_PAY_TERM_TYPE + '"/>'
				+ '				<input type="hidden" class="post_pay_term_day pay_term_param" value="' + this.POST_PAY_TERM_DAY + '"/>'
				+ '			</span>'
				+ '		</dd>      '
				+ '	</dl> ' 
				+ '</li>    '
			);

			if (this.ROWNUM == 1 && this.STAT != 'P') {
				$targetEl.find('li:first').attr('id', 'first_list');
				$targetEl.find('li:first').find('.save').prepend(
					'<a class="btn_remove_pay_term" href="#none"><img src="${IMG}/common/btn_s_remove.gif"></a>'
				);
			}
		});

		$.fn.setBackgroundEvenAndOdd($('#pay_term_area'), '#ffffff', '#f6f6f6');

		// class 삭제
		$('.pay_term_add').removeClass('pay_term_add');
		$('.pay_term_modify').removeClass('pay_term_modify');

	};

	/**
	 * <pre> 
	 *   페이징을 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 * @param $target : 페이징 구성 영역 Element
	 * @param $pagingEl : 페이징 결과 Data
	 */
	$.fn.reloadPaging = function($target, $pagingEl) {
		$target.html($pagingEl);
	};

	/**
	 * <pre>
	 *   modifyMode 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	$.fn.convertModifyMode = function() {
		$('.notModifyMode').hide();
		$('.modifyMode').show();

		// 필수값 * 표기함
		$('#companyCustomerArea .infoView em').show();
	};

	/**
	 * <pre>
	 *   notModify Mode 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	$.fn.convertNotModifyMode = function() {
		$('.notModifyMode').show();
		$('.modifyMode').hide();

		// 필수값 * 표기 숨김
		$('#companyCustomerArea .infoView em').hide();
	};

	/**
	 * <pre>
	 *   폼 전송 파라미터 검증 중 경고창 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
	 */
	$.fn.validateResultAndAlert = function($el, $message) {
		alert($message);
		$el.focus();
		return false;
	};

	/**
	 * <pre>
	 *    폼 전송 파라미터 검증 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 */
	$.fn.validate = function() {

		$result = true;

		$formEl = $('form').find('input');
		$.each($formEl, function() {

			// 필수 공백 검증
			if ($(this).hasClass('required')) {
				if ($.trim($(this).val()) == '') {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '을(를) 입력해주세요.');
					return $result;
				}
			}

			if ($.trim($(this).val()) != '') {

				// 숫자 검증
				if ($(this).hasClass('numeric')) {
					if (!typeCheck('numCheck', $.trim($(this).val()))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}

					if ($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}

				// 전화번호, 휴대폰번호, 팩스번호 자릿수 검증
				if ($(this).hasClass('phoneNo')) {
					if ($(this).hasClass('midNo') && $(this).val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 3자리보다 커야 합니다.');
						return $result;
					}
					if ($(this).hasClass('lastNo') && $(this).val().length != 4) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 4자리여야 합니다.');
						return $result;
					}
					if ($(this).siblings('.phoneNo').val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this).siblings('.phoneNo'), $(this).siblings('.phoneNo').attr('title') + '를 확인해주세요.');
						return $result;
					}
				}

				// 화폐 검증
				if ($(this).hasClass('numericMoney')) {
					if (!(typeCheck('numCheck', replaceAll($.trim($(this).val()), ',', '')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
						return $result;
					}

					if ($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
			}
			// 반복문 정지
			if (!$result)
				return $result;
		});

		// 스크립트 정지
		if (!$result)
			return $result;

		// 이메일 검증[문자열 형태 검증]
		$emailEl1 = $('form').find('#email1');
		$emailEl2 = $('form').find('#email2');
		if ($.trim($emailEl1.val()) != '' || $.trim($emailEl2.val()) != '') {
			if (!typeCheck('emailCheck', $.trim($emailEl1.val()) + '@' + $.trim($emailEl2.val()))) {
				alert('이메일을 확인해 주세요.');
				$result = false;
				return $result;
			}
		}

		// 법인 등록번호 검증[문자열 형태 검증]
		$corpNoEl = $('form').find('#corp_no');
		if ($.trim($corpNoEl.val()) != '') {
			if (!typeCheck('corpNoCheck', replaceAll($.trim($corpNoEl.val()), '-', ''))) {
				$result = $.fn.validateResultAndAlert($corpNoEl, $corpNoEl.attr('title') + '을(를) 확인해주세요.');
				return $result;
			}
		}

		return $result;
	};

	$.fn.validateCredit = function() {

		$result = true;

		$formEl = $('form').find('input');
		$.each($formEl, function() {

			// 필수 공백 검증
			if ($(this).hasClass('required')) {
				if ($.trim($(this).val()) == '') {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '을(를) 입력해주세요.');
					return $result;
				}
			}

			if ($.trim($(this).val()) != '') {

				// 화폐 검증
				if ($(this).hasClass('numericMoney')) {
					if (!(typeCheck('numCheck', replaceAll($.trim($(this).val()), ',', '')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
						return $result;
					}

					if ($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
			}
		});
		return $result;
	};

	/**
	 * <pre> 
	 *   신용정보 이력 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 06
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCrdHistoryList = function($target, $listEl) {

		$htmlStr = '';
		$payName = '';

		$.each($listEl, function() {

			if (this.PAY_TYPE == 'F') {
				$payName = '무료';
			} else {
				$payName = '유료';
			}
			$htmlStr += 
						'<li>'
						+ '<dl>'
							+ '<dd class="border">'
								+ (this.PAY_TYPE != null ? ('구분　:　 ' + $payName): '')
								+ (this.EW_DT != null ? (' <em>|</em> 날짜　:　' + this.EW_DT.substring(0, 4) + "-"
										+ this.EW_DT.substring(4, 6) + "-" + this.EW_DT.substring(6, 10)): '')
								+ (this.CRD_TYPE_VALUE != null ? (' <em>|</em> EW등급 : ' + this.CRD_TYPE_VALUE): '');
								if (this.CUR_PAY_CNT > 0) {
									$htmlStr += 
										(this.KFB_ARR_YN != null ? (' <em>|</em> 금융권단기연체 은행연 : ' + this.KFB_ARR_YN): '')
										+ (this.KED_GEN_ARR_YN != null ? (' <em>|</em> 금융권단기연체 KED일반 : ' + this.KED_GEN_ARR_YN): '')
										+ (this.KED_CRD_ARR_YN != null ? (' <em>|</em> 금융권단기연체 KED법인카드 : ' + this.KED_CRD_ARR_YN): '')
										+ (this.COMP_DEBT_YN != null ? (' <em>|</em> 금융권 채무불이행정보 기업 : ' + this.COMP_DEBT_YN): '')
										+ (this.PER_DEBT_YN != null ? (' <em>|</em> 금융권 채무불이행정보 개인 : ' + this.PER_DEBT_YN): '')
										+ (this.COMP_PUB_YN != null ? (' <em>|</em> 공공정보 기업 : ' + this.COMP_PUB_YN): '')
										+ (this.OWN_PUB_YN != null ? (' <em>|</em> 공공정보 대표자 : ' + this.OWN_PUB_YN): '')
										+ (this.SUS_ACC_YN != null ? (' <em>|</em> 당좌거래정지 : ' + this.SUS_ACC_YN): '')
										+ (this.CLS_BIZ_YN != null ? (' <em>|</em> 휴폐업 : ' + this.CLS_BIZ_YN): '')
										+ (this.FIN_ARR_YN != null ? (' <em>|</em> 상거래연체 : ' + this.FIN_ARR_YN): '')
										+ (this.WO_YN != null ? (' <em>|</em> 법정관리/Workout : ' + this.WO_YN): '')
										+ (this.ADM_MSR_YN != null ? (' <em>|</em> 행정처분정보 : ' + this.ADM_MSR_YN): '')
										+ (this.CRD_LV_YN != null ? (' <em>|</em> 신용등급 : ' + this.CRD_LV_YN): '')
										+ (this.LWST_YN != null ? (' <em>|</em> 소송 : ' + this.LWST_YN): '')
										+ (this.FIN_YN != null ? (' <em>|</em> 재무 : ' + this.FIN_YN): '')
										+ (this.SRCH_REC_YN != null ? (' <em>|</em> 조회기록 : ' + this.SRCH_REC_YN): '')
										+ (this.CLS_BIZ_TYPE != null ? (' <em>|</em> ' + this.CLS_BIZ_TYPE): '');
								}
			$htmlStr += 	'</dd>' 
						+ '</dl>' 
					+ '</li>';
		});

		$target.append($htmlStr);
		$('.border').wordBreakKeepAll();
		$.fn.setBackgroundEvenAndOdd($target, 'li', '#f6f6f6', '#ffffff');
	};

	/**
	 * <pre> 
	 *   신용정보 이력 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 06
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCrdPdHistoryList = function($target, $listEl) {

		$htmlStr = '';

		$.each($listEl, function() {

			$htmlStr += '<li>'
					+ '<dl>'
					+ '<dd class="border">'
					+ (this.REG_DT != null ? ('날짜　:　' + this.REG_DT): '')
					+ (this.CRD_TYPE_VALUE != null ? (' <em>|</em> EW등급 : ' + this.CRD_TYPE_VALUE): '') + '</dd>' + '</dl>' + '</li>';
		});

		$target.append($htmlStr);
		$('.border').wordBreakKeepAll();
		$.fn.setBackgroundEvenAndOdd($target, 'li', '#f6f6f6', '#ffffff');
	};

	// 우편번호 검색 팝업창
	/* function open_post() {
		open_win('${HOME}/zip/zipSearchPopup.do');
	}; */

	/**
	 * <pre> 
	 *   수금계획 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCustomerPlanList = function($target, $listEl) {

		$htmlStr = '';

		$.each($listEl, function() {
			$htmlStr += '<tr>' 
						+ '<td>'
						+ (('${sBox.isSessionCustAddGrn}' == 'true' && '${sBox.isSessionDebnSearchGrn}' == 'true') ? 
								'<input id="cust_pln_sn_' + this.CUST_PLN_SN + '" class="chkCustPlnSn" type="checkbox" value="' + this.CUST_PLN_SN + '" '
								+ (this.STAT != 'P' ? 'disabled="true"' : '') + ' autocomplete="off">' : ' ')
						+ '</td>'
						+ '<td>' + this.REG_DT + '</td>'
						+ '<td>'
							+ '<a class="textLink lnkCustomerPlan STAT_' + this.STAT + '" id="lnk_customer_plan_' + this.CUST_PLN_SN +'" href="#none">'
							+ this.STAT_NAME
							+ '</a>'
						+ '</td>'
						+ '<td>' + this.DEBN_CNT + '</td>'
						+ '<td>' + this.PLN_COL_DT + '</td>'
						+ '<td class="list_right">' + this.PLN_COL_AMT_COMMA + '</td>'
						+ '<td class="list_right">' + (this.COL_AMT != null ? this.COL_AMT_COMMA : '') + '</td>'
						+ '<td>' + (this.RMK_TXT != null ? this.RMK_TXT : '') + '</td>' 
						+ '<td class="list_left last">';
						if (('${sBox.isSessionCustAddGrn}' == 'true' && '${sBox.isSessionDebnSearchGrn}' == 'true') && (this.STAT == 'P')) {
							$htmlStr += ('<a class="smallBtnType02 completedCol" href="#none">완전수금</a> '
										+ '<a class="smallBtnType02 sectionCol" href="#none">부분수금</a> '
										+ '<a class="smallBtnType01 delayCol" href="#none">연기</a>');
						}
			$htmlStr += '</td>' + '</tr>';
		});
		$target.append($htmlStr);
	};

	/**
	 * <pre> 
	 *   리스트가 없을 경우
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17
	 * @param $target : 리스트 구성 영역 Element
	 * @param $noDataEl : 리스트를 구성할 Data
	 */
	$.fn.reloadNoDataList = function($target, $noDataEl) {
		$htmlStr = '';
		$target.empty().append($noDataEl);
		$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
	};

	/**
	 * <pre> 
	 *   파라미터 구성 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22
	 *
	 * @param $targetEl : 수집 대상 엘리먼트
	 * @param $selector : 수집 대상 셀렉터(클래스)
	 * @param $paramEl : 파라미터 수집 대상 객체
	 * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
	 * @param $dataSplitStr : 구분자[데이터]
	 * @param $rowSplitStr : 구분자[row]
	 */
	$.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {

		$result = '';

		$.each($targetEl, function() {
			if ($(this).hasClass($selector.toString())) {
				$tmpStr = '';
				for ( var i = 0; i < $paramEl.length; i++) {
					if ($tmpStr != '') {
						$tmpStr += $dataSplitStr;
					}
					$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
				}

				if ($result != '') {
					$result += $rowSplitStr;
				}
				$result += $tmpStr;
			}
		});

		return $result;
	};

	/**
	 * <pre> 
	 *   독촉 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCustomerPrsList = function($target, $listEl) {

		$htmlStr = '';

		$.each($listEl, function() {
			$htmlStr += '<tr>'
						+ '<td>'
						+ (('${sBox.isSessionCustAddGrn}' == 'true' && '${sBox.isSessionDebnSearchGrn}' == 'true') ? 
								'<input type="checkbox" class="chkCustPrsSn" id="cust_prs_sn_' + this.CUST_PRS_SN + '" value="' + this.CUST_PRS_SN + '" autocomplete="off">': ' ')
						+ '</td>'
						+ '<td>'
							+ '<a class="textLink lnkCustomerPrs" id="lnk_customer_prs_' + this.CUST_PRS_SN + '" href="#none">'
							+ this.PRS_DT
							+ '</a>'
						+ '</td>'
						+ '<td>' + this.DEBN_CNT + '</td>'
						+ '<td>' + this.PRS_TYPE_NAME + '</td>'
						+ '<td>' + (this.FILE_CNT != null ? this.FILE_CNT: '') + '</td>'
						+ '<td class="list_left last">' + (this.RMK_TXT != null ? this.RMK_TXT : '') + '</td>' 
						+'</tr>';
		});
		$target.append($htmlStr);
	};

	/**
	 * <pre> 
	 *   수금 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 30.
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCustomerColList = function($target, $listEl) {

		$htmlStr = '';

		$.each($listEl, function() {
			$htmlStr += '<tr>' 
						+ '<td>'
						+ (('${sBox.isSessionCustAddGrn}' == 'true' && '${sBox.isSessionDebnSearchGrn}' == 'true') ? '<input type="checkbox" class="chkCustColSn" id="cust_col_sn_' + this.CUST_COL_SN + '" value="' + this.CUST_COL_SN + '" autocomplete="off">'
								: ' ')
						+ (this.CUST_PLN_SN != null ? '<input type="hidden" class="hiddenPlnSn" id="hidden_pln_' + this.CUST_PLN_SN + '" value="' + this.CUST_PLN_SN + '">'
								: '')
						+ '</td>'
						+ '<td>' + (this.PLN_COL_DT != null ? this.PLN_COL_DT : '') + '</td>'
						+ '<td>'
							+ '<a class="textLink lnkCustomerCol" id="lnk_customer_col_' + this.CUST_COL_SN + '" href="#none">'
							+ this.COL_DT
						+ '</a>'
						+ '</td>'
						+ '<td>' + (this.PLN_COL_AMT_COMMA != null ? this.PLN_COL_AMT_COMMA : '') + '</td>'
						+ '<td>' + this.STAT_NAME + '</td>'
						+ '<td>' + this.COL_AMT_COMMA + '</td>'
						+ '<td>' + this.COL_TYPE_NAME + '</td>'
						+ '<td>' + this.DEBN_CNT + '</td>'
						+ '<td class="list_left last">' + (this.RMK_TXT != null ? this.RMK_TXT : '') + '</td>' 
						+'</tr>';
		});
		$target.append($htmlStr);
	};

	/*
	 * <pre>
	 * 	채무불이행 상태별 버튼 CallBack Function
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.21
	 */
	$.fn.callBackDebtProcessStatChange = function(stat) {

		switch (stat) {
		// [해제요청] callBack
		case "RR":
			$.fn.getCustomer($("#cust_id").val());
			break;
		// [채무불이행 정정요청] callBack
		case 'RequireRelease':
			$.fn.getCustomer($("#cust_id").val());
			break;
		// [신청취소] callBack
		case 'AC':
			$.fn.getCustomer($("#cust_id").val());
			break;
		// TODO [결제] 버튼 CallBack 작성해야함.
		}
	};

	/*
	 * <pre>
	 * 	채무불이행 상태 Reload Function
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.21
	 */
	$.fn.reloadDebtProcess = function($debtProcess, $debt, $sBox) {

		if ($debtProcess != null) {

			// 채무불이행 진행 중인 상태이력을 다시 출력한다.
			$(".default_step_n").eq(0).text(utfDecode($debtProcess.STEP1));
			$(".default_step_n").eq(1).text(utfDecode($debtProcess.STEP2));
			$(".default_step_n").eq(2).text(utfDecode($debtProcess.STEP3));
			$(".default_step_n").eq(3).text(utfDecode($debtProcess.STEP4));
			$(".default_step_n").eq(4).text(utfDecode($debtProcess.STEP5));
			$(".default_step_n").eq(5).text(utfDecode($debtProcess.STEP6));

			$(".default_step_d").eq(0).text(utfDecode($debtProcess.STEP1_DT));
			$(".default_step_d").eq(1).text(utfDecode($debtProcess.STEP2_DT));
			$(".default_step_d").eq(2).text(utfDecode($debtProcess.STEP3_DT));
			$(".default_step_d").eq(3).text(utfDecode($debtProcess.STEP4_DT));
			$(".default_step_d").eq(4).text(utfDecode($debtProcess.STEP5_DT));
			$(".default_step_d").eq(5).text(utfDecode($debtProcess.STEP6_DT));

		} else {

			// 채무불이행 진행 중인 것이 없기 때문에 초기화 한다.
			$("#defaultWrap> .default_stepbox").empty();
			$htmlStr = '<div class="default_stepbox_none">';
			$htmlStr += '<p>현재 진행중인 채무 불이행 상태가 없습니다.</p>';
			$htmlStr += '<a href="#none" class="longBtnOn" id="btn_nonpayment_regist" >채무 불이행 등록</a>';
			$htmlStr += '</div>';
			$("#defaultWrap> .default_stepbox").append($htmlStr);

		}

		// 채무불이행 상태이력 버튼 출력
		$("#defaultWrap> .btmBtnWrap").empty();
		if ($debt != null && $sBox.isSessionDebtApplGrn == true) {
			$stat = $debt.STAT;
			$htmlStr = '<p class="rightBtn">';
			// 심사대기, 심사중
			if ($stat == 'AW' || $stat == 'EV') {
				$htmlStr += '<a class="ApplyBtn_Type2 btnApplyCancel" href="#none" data-stat="${debtBox.STAT}">신청취소</a>';

				// 결제대기
			} else if ($stat == 'AA') {
				$htmlStr += '<a class="ApplyBtn_Type2 btnPayment" href="#none" data-stat="${debtBox.STAT}">결제</a>';
				$htmlStr += '<a class="ApplyBtn_Type2 btnApplyCancel" href="#none" data-stat="${debtBox.STAT}">신청취소</a>';

				// 통보서 준비(결제완료), 통보서 발송, 등록전민원발생(채무불이행등록전통보서발생시),등록완료,  등록후민원발생(정보등록완료 후), 등록전민원발생(추가증빙), 등록후민원발생(추가증빙)
			} else if ($stat == 'PY' || $stat == 'NT' || $stat == 'CA'
					|| $stat == 'RC' || $stat == 'CB' || $stat == 'FA'
					|| $stat == 'FB') {
				// 현재 채무금액 정정요청 진행중 상태에서는 채무금액정정요청을 할 수 없음
				if ($debt.MOD_STAT != 'R') {
					$htmlStr += '<a class="ApplyBtn_Type2 btnDebtReqCor" href="#none" data-stat="${debtBox.STAT}">채무금액 정정 요청</a>';
				}
				$htmlStr += '<a class="ApplyBtn_Type2 btnDebtReqRel" href="#none" data-stat="${debtBox.STAT}">해제 요청</a>';
			}
			$htmlStr += '</p>';
			$('#defaultWrap > .btmBtnWrap').append($htmlStr);
		}
	};

	/**
	 * <pre>
	 *  페이지 reload 함수
	 * </pre>
	 */
	$.fn.pageReload = function() {
		$('.tab_on').trigger('click');
	};

	/**
	 * <pre> 
	 *   채권가이드 관련 첨부 파일 다운로드
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 06. 19
	 * @param debnGuideDownloadType : 가이드 다운로드 타입 (hwp, doc , docMapping) 
	 *        guideType : 조치사항 별 상황 (J~R)
	 *        fileName : 다운로드 파일명
	 */
	$.fn.getDebntureGuideFileDownload = function($debnGuideDownloadType, $guideType, $fileType, $fileTypeSub) {
		$paramObj = {
			debnGuideDownloadType : $debnGuideDownloadType,
			guideType : $guideType,
			fileType : $fileType,
			fileTypeSub : $fileTypeSub,
			custId : $("#cust_id").val()
		};
		$param = $.param($paramObj);
		location.href = "${HOME}/customer/getDebntureGuideFileDownload.do?" + $param;
	};
<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>
	
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>
/**
 * <pre> 
 *   결제 조건 수정 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 *
 * @param $custId : 거래처 순번
 * @param $payTermType : 결제 유형
 * @param $payTermDay : 결제 기준일
 * @param $payTermDate : 결제 적용 기간
 * @param $payTermSn : 결제 조건 순번

$.fn.modifyPayTerm = function($custId, $payTermType, $payTermDay, $payTermDate, $payTermSn) {

	$param = '';
	$param += 'custId=' + $custId.val();
	$param += '&payTermSn=' + $payTermSn.val();
	$param += '&payTermType=' + $payTermType.val();
	$param += '&payTermDay=' + $payTermDay.val();
	$param += '&payTermDate=' + encodeURIComponent($payTermDate.val());

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/modifyPayTerm.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$payTermList = $result.payTermList;
			$pcPagingEl = $result.pcPage;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$('#pay_term_area').find('ul').children().remove();
				$targetEl = $('#pay_term_area').find('ul');

				// 결제 조건 추가 결과 리스트 조회
				$.fn.reloadPayTermList($targetEl, $payTermList);
				$.fn.reloadPaging($("#pay_term_paging"), $pcPagingEl);
				alert('결제 조건 수정이 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("결제 조건 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
}; */

/**
 * <pre> 
 *   거래처 정보 삭제 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28

$.fn.removePayTerm = function($custId, $payTermSn, $payTermStat) {

	$param = '';
	$param += 'custId=' + $custId.val();
	$param += '&payTermSn=' + $payTermSn.val();
	$param += '&payTermStat=' + $payTermStat.val();

	$.ajax({
				type : "POST",
				url : "${HOME}/customer/removePayTerm.do",
				dataType : "json",
				data : $param,
				success : function(msg) {

					$result = msg.model.result;
					$payTermList = $result.payTermList;
					$pcPagingEl = $result.pcPage;
					$replCd = $result.REPL_CD;
					$replMsg = $result.REPL_MSG;

					if ('00000' == $replCd) {
						$('#pay_term_area').find('ul').children().remove();
						$targetEl = $('#pay_term_area').find('ul');

						// 결제 조건 추가 결과 리스트 조회
						if ($payTermList.length > 0) {
							$.fn.reloadPayTermList($targetEl, $payTermList);
						} else {
							$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
						}

						$.fn.reloadPaging($("#pay_term_paging"), $pcPagingEl);
						alert('결제 조건 삭제가 완료되었습니다.');
					} else {
						alert(utfDecode($replMsg));
					}
				},
				error : function(xmlHttpRequest, textStatus, errorThrown) {
					alert("결제 조건 삭제 중 오류가 발생하였습니다. [" + textStatus + "]");
				}
			});
}; */

/**
 * <pre> 
 *   결제 조건 추가[결제 조건이 있을 경우] ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 *
 * @param $custId : 거래처 순번
 * @param $payTermType : 결제 유형
 * @param $payTermDay : 결제 기준일
 * @param $payTermDate : 결제 적용 기간

$.fn.addPayTerm = function($custId, $payTermType, $payTermDay, $payTermDate) {

	$param = '';
	$param += 'custId=' + $custId.val();
	$param += '&payTermType=' + $payTermType.val();
	$param += '&payTermDay=' + $payTermDay.val();
	$param += '&payTermDate=' + encodeURIComponent($payTermDate.val());

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/addPayTerm.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$payTermList = $result.payTermList;
			$pcPagingEl = $result.pcPage;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$('#pay_term_area').find('ul').children().remove();
				$targetEl = $('#pay_term_area').find('ul');

				// 결제 조건 추가 결과 리스트 조회
				$.fn.reloadPayTermList($targetEl, $payTermList);
				$.fn.reloadPaging($("#pay_term_paging"), $pcPagingEl);
				alert('결제 조건 추가가 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("결제 조건 추가 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
}; */


/**
 * <pre> 
 *   거래처 정보 수정 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28

$.fn.modifyCustomerInfo = function() {

	$('.numericMoney').each(function() {
		$(this).val(replaceAll($(this).val(), ',', ''));
	});

	$param = $('form').find('input:not(.pay_term_param), select')
			.serialize();

	$('.numericMoney').each(function() {
		$(this).val(addComma($(this).val()));
	});

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/modifyCustomer.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$sBox = msg.model.sBox;
			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$updatedCustomer = $result.updatedCustomerBox;

				// 수정 결과 세팅
				$('#cust_nm_not_modify_area').html($updatedCustomer.CUST_NM);
				$('#email_not_modify_area').html($updatedCustomer.EMAIL);

				if ($updatedCustomer.POST_NO == null)
					$('#addr_not_modify_area').html("");
				else
					$('#addr_not_modify_area').html($updatedCustomer.POST_NO + '<br/>' + $updatedCustomer.ADDR_1 + ' ' + $updatedCustomer.ADDR_2);

				$('#tel_no_not_modify_area').html($updatedCustomer.TEL_NO);
				$('#crd_ltd_not_modify_area').html(
						(($updatedCustomer.CRD_LTD - $updatedCustomer.SUM_DEBN_AMT + $updatedCustomer.COL_SUM_DEBN_AMT) < 0) ? 
								'<a id="lnk_crd_ltd">' + addComma('' + $updatedCustomer.CRD_LTD) + '원' + '(초과)</a>'
								: addComma('' + $updatedCustomer.CRD_LTD) + '원');
				$('#mb_no_not_modify_area').html($updatedCustomer.MB_NO);
				$('#upd_dt_not_modify_area').html(
								'[ 업데이트 날짜 : ' + $updatedCustomer.UPD_DT + ' ] '
								+ '<a id="btn_require_modify" class="btnType01" href="#none">수정</a>'
				);

				$('#own_nm_not_modify_area').html($updatedCustomer.OWN_NM);
				$('#biz_type_not_modify_area').html($updatedCustomer.BIZ_TYPE);
				$('#biz_cond_not_modify_area').html($updatedCustomer.BIZ_COND);
				$('#fax_no_not_modify_area').html($updatedCustomer.FAX_NO);
				$('#cust_type_not_modify_area').html(($updatedCustomer.CUST_TYPE == 'C' ? '법인 사업자' : '개인 사업자'));

				if ($updatedCustomer.CUST_TYPE == 'C') {
					$('#corp_no_not_modify_area').html(($updatedCustomer.CORP_NO != null ? $updatedCustomer.CORP_NO : ''));
				}

				// not modify mode로 전화
				$.fn.convertNotModifyMode();
				alert('거래처 수정이 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}

		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 정보 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
}; */

/**
 * <pre> 
 *   거래처 정보 수정 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 */
$.fn.modifyCreditInfo = function() {
	$('.numericMoney').each(function() {
		$(this).val(replaceAll($(this).val(), ',', ''));
	});

	$param = $('form').find('input:not(.pay_term_param), select')
			.serialize();
	$('.numericMoney').each(function() {
		$(this).val(addComma($(this).val()));
	});
	$.ajax({
		type : "POST",
		url : "${HOME}/customer/modifyCreditInfo.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			//$sBox = msg.model.sBox;
			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$updatedCustomer = $result.updatedCustomerBox;

				// 수정 결과 세팅

				$('#tel_no_not_modify_area').html($updatedCustomer.TEL_NO);
				$('#crd_ltd_not_modify_area').html(
							(($updatedCustomer.CRD_LTD - $updatedCustomer.SUM_DEBN_AMT + $updatedCustomer.COL_SUM_DEBN_AMT) < 0) ? 
									'<a id="lnk_crd_ltd">' + addComma('' + $updatedCustomer.CRD_LTD) + '원' + '(초과)</a>'
									: addComma('' + $updatedCustomer.CRD_LTD) + '원');

				// not modify mode로 전화
				$.fn.convertNotModifyCreditMode();
				alert('거래처 여신한도 수정이 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}

		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 정보 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    Ajax를 활용한 Paging 처리 함수
 * </pre>
 * @author SungRangKong
 * @since 2013. 08. 27
 */
getPayTermAjaxPage = function(num) {
	$param = "num=" + num + "&custId=" + $("#cust_id").val();

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getPayTermList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 결제 조건 데이터 다시 뿌려줘야 함
				$('#pay_term_area').find('ul').children().remove();
				$targetEl = $('#pay_term_area').find('ul');

				// 결제 조건 추가 결과 리스트 조회
				if (parseInt($result.payTermListTotalCount, 10) > 0) {
					$.fn.reloadPayTermList($targetEl, $result.payTermList);
				} else {
					$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
				}

				// PC Paging 모듈 연동
				$("#ajax_num").val(num);
				$.fn.reloadPaging($("#pay_term_paging"), $result.pcPage);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    신용정보 조회 
 *       Ajax를 활용한 Paging 처리 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 09. 26
 * @param num : 현재페이지
 */
getHistoryAjaxPage = function(num) {
	$param = "EW_CD=" + $('#ew_cd').val() + "&historyNum=" + num
			+ "&custId=" + $("#cust_id").val()
			+ "&custNo=" + $("#cust_no").val();

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerAllCreditHistoryList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 신용정보 이력 리스트 재구성
				$('#crd_history_list ul').empty();
				$targetEl = $('#crd_history_list ul');

				$("#crd_ajax_num").val(num);

				// 결제 조건 추가 결과 리스트 조회
				if (parseInt($result.customerAllCreditHistoryListCount, 10) > 0) {
					$.fn.reloadCrdHistoryList($targetEl, $result.customerAllCreditHistoryList);
				} else {
					$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#history_pc_page"), $result.historyPcPage);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    신용정보 조회 
 *       Ajax를 활용한 Paging 처리 함수(유료)
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 09. 26
 * @param num : 현재페이지
 */
getHistoryPdAjaxPage = function(num) {
	$param = "EW_CD=" + $('#ew_cd').val() + "&historyNum=" + num
			+ "&custId=" + $("#cust_id").val()
			+ "&custNo=" + $("#cust_no").val();
	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerCreditPdHistoryList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 신용정보 이력 리스트 재구성
				$('#crd_pd_history_list ul').empty();
				$targetEl = $('#crd_pd_history_list ul');

				$("#crdPd_ajax_num").val(num);

				// 결제 조건 추가 결과 리스트 조회
				if (parseInt(
						$result.customerCreditHistoryPdListTotalCount, 10) > 0) {
					$.fn.reloadCrdPdHistoryList($targetEl, $result.customerCreditPdHistoryList);
				} else {
					$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#historyPd_pc_page"), $result.historyPdPcPage);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   결제 조건 추가[결제 조건이 없을 경우] ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 14
 *
 * @param $custId : 거래처 순번
 * @param $initPayTermType : 초기 결제 유형
 * @param $initPayTermDay : 초기 결제 기준일
 * @param $futurePayTermType : 차후 결제 유형
 * @param $futurePayTermDay : 차후 결제 기준일
 * @param $payTermDate : 결제 적용 기간
 */
$.fn.addFirstPayTerm = function($custId, $initPayTermType, $initPayTermDay,
		$futurePayTermType, $futurePayTermDay, $payTermDate) {

	$paramObj = {
		custId : $custId.val(),
		initPayTermType : $initPayTermType.val(),
		initPayTermDay : $initPayTermDay.val(),
		futurePayTermType : $futurePayTermType.val(),
		futurePayTermDay : $futurePayTermDay.val(),
		payTermDate : $payTermDate.val()
	};

	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/addFirstPayTerm.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$payTermList = $result.payTermList;
			$pcPagingEl = $result.pcPage;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$('#pay_term_area').find('ul').children().remove();
				$targetEl = $('#pay_term_area').find('ul');

				// 결제 조건 추가 결과 리스트 조회
				$.fn.reloadPayTermList($targetEl, $payTermList);

				$.fn.reloadPaging($("#pay_term_paging"), $pcPagingEl);

				alert('결제 조건 추가가 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}

		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("결제 조건 추가 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    Ajax를 활용한 거래처 수금계획 Paging 처리 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 17
 */
getCustomerPlanAjaxPage = function(num) {

	$paramObj = {
		customerPlanListNum : num,
		custId : $("#cust_id").val()
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerPlanList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 거래처 수금 계획 조회 결과 재출력
				$('#col_plan_table').find('tbody').children().remove();
				$targetEl = $('#col_plan_table').find('tbody');

				// 거래처 수금 계획 결과 리스트 조회
				if ($result.customerPlanList.length > 0) {
					$.fn.reloadCustomerPlanList($targetEl, $result.customerPlanList);
				} else {
					$.fn.reloadNoDataList($targetEl, '<tr><td colspan="9"><div class="noSearch">결과가 없습니다.</div></td></tr>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#customer_plan_page_area"), $result.customerPlanListPcPage);

				// 수금계획 건수 새로 고침
				$('#cust_col_pln').find('div.list_results').text($result.customerPlanListTotalCount + '건');

				// 수금계획 페이징 번호 세팅
				$('#customer_plan_list_num').val(num);
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   거래처 수금계획 삭제 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 23
 * @param  
 */
$.fn.removeCustomerPlan = function() {

	$paramEl = new Array();
	$replaceEl = new Array();
	$paramEl.push('.chkCustPlnSn');
	$replaceEl.push('');

	$deleteParam = $.fn.makeParam($('.chkCustPlnSn:not(:disabled):checked'), 'chkCustPlnSn', $paramEl, $replaceEl, ':', '|');

	$paramObj = {
		custId : $('#cust_id').val(),
		deleteParam : $deleteParam
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/removeCustomerPlan.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$debnList = $result.debnList;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				alert('거래처 수금 계획 삭제가 완료되었습니다.');
				$('form').attr({'action' : '${HOME}/customer/getCustomer.do', 'method' : 'POST'}).submit();
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("수금계획 삭제 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   거래처 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 28
 * @param  
 */
$.fn.getCustomer = function($custId) {

	$paramObj = {
		custId : $custId
	};

	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerAjax.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$sBox = msg.model.sBox;
			$customer = $result.customerBox;
			$debnList = $result.debnList;
			$debtProcess = $result.debtProcess;
			$debtBox = $result.debtBox;

			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 거래처 미수금액, 미수채권 새로고침
				$htmlStr = '';
				$htmlStr += '미수금액 : <a ';

				if (($customer.DEBN_COL_SUM) > 0) {
					$htmlStr += 'id="lnk_sum_debn_col" ';
				}
				$htmlStr += 'href="#none">';
				$htmlStr += addComma($customer.DEBN_COL_SUM) + '원</a>';
				$htmlStr += ' | 미수채권 : <a ';
				if (($customer.DEBN_COL_CNT) > 0) {
					$htmlStr += 'id="lnk_debn_col_cnt" ';
				}
				$htmlStr += 'href="#">';
				$htmlStr += $customer.DEBN_COL_CNT + '건</a> / <a ';
				if (($customer.DEBN_CNT) > 0) {
					$htmlStr += 'id="lnk_debn_col_cnt" ';
				}
				$htmlStr += 'href="#">' + $customer.DEBN_CNT + '건</a>';

				$('.totalNews').html($htmlStr);

				// 채무불이행 상태 변경
				$.fn.reloadDebtProcess($debtProcess, $debtBox, $sBox);
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 조회 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    Ajax를 활용한 거래처 독촉 Paging 처리 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 29
 */
getCustomerPrsAjaxPage = function(num) {

	$paramObj = {
		customerPrsListNum : num,
		custId : $("#cust_id").val()
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerPrsList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 거래처 독촉 조회 결과 재출력
				$('#prs_table').find('tbody').children().remove();
				$targetEl = $('#prs_table').find('tbody');

				// 거래처 독촉 결과 리스트 조회
				if ($result.customerPrsList.length > 0) {
					$.fn.reloadCustomerPrsList($targetEl, $result.customerPrsList);
				} else {
					$.fn.reloadNoDataList($targetEl, '<tr><td colspan="6"><div class="noSearch">결과가 없습니다.</div></td></tr>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#customer_prs_page_area"), $result.customerPrsListPcPage);

				// 독촉 건수 새로 고침
				$('#cust_prs').find('div.list_results').text($result.customerPrsListTotalCount + '건');

				// 독촉 페이징 번호 세팅
				$('#customer_prs_list_num').val(num);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   거래처 독촉 삭제 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 29
 * @param  
 */
$.fn.removeCustomerPrs = function() {

	$paramEl = new Array();
	$replaceEl = new Array();
	$paramEl.push('.chkCustPrsSn');
	$replaceEl.push('');

	$deleteParam = $.fn.makeParam($('.chkCustPrsSn:not(:disabled):checked'), 'chkCustPrsSn', $paramEl, $replaceEl, ':', '|');

	$paramObj = {
		custId : $('#cust_id').val(),
		deleteParam : $deleteParam
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/removeCustomerPrs.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$debnList = $result.debnList;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				alert('거래처 독촉 삭제가 완료되었습니다.');
				$('form').attr({'action' : '${HOME}/customer/getCustomer.do', 'method' : 'POST'}).submit();
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 독촉 삭제 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    Ajax를 활용한 거래처 수금 Paging 처리 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 30
 */
getCustomerColAjaxPage = function(num) {

	$paramObj = {
		customerColListNum : num,
		custId : $("#cust_id").val()
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerColList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 거래처 수금 조회 결과 재출력
				$('#col_table').find('tbody').children().remove();
				$targetEl = $('#col_table').find('tbody');

				// 거래처 수금 결과 리스트 조회
				if ($result.customerColList.length > 0) {
					$.fn.reloadCustomerColList($targetEl, $result.customerColList);
				} else {
					$.fn.reloadNoDataList($targetEl, '<tr><td colspan="9"><div class="noSearch">결과가 없습니다.</div></td></tr>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#customer_col_page_area"), $result.customerColListPcPage);

				// 수금 건수 새로 고침
				$('#cust_col').find('div.list_results').text($result.customerColListTotalCount + '건');

				// 수금 페이징 번호 세팅
				$('#customer_col_list_num').val(num);
			}
		},
		complete : function() {
			$.fn.getCustomer($('#cust_id').val());
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   거래처 수금 삭제 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 01.
 * @param  
 */
$.fn.removeCustomerCol = function() {

	$paramEl = new Array();
	$replaceEl = new Array();
	$paramEl.push('.chkCustColSn');
	$replaceEl.push('');

	$deleteParam = $.fn.makeParam($('.chkCustColSn:not(:disabled):checked'), 'chkCustColSn', $paramEl, $replaceEl, ':', '|');

	$paramObj = {
		custId : $('#cust_id').val(),
		deleteParam : $deleteParam
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/removeCustomerCol.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$debnList = $result.debnList;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				alert('거래처 수금 삭제가 완료되었습니다.');
				$('form').attr({'action' : '${HOME}/customer/getCustomer.do', 'method' : 'POST'}).submit();
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 수금 삭제 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};


/**
 * <pre> 
 *   거래처 요약 : 채권가이드 요약 조회 Ajax 함수
 * </pre>
 * @author sungrangkong
 * @since 2014. 06. 05.
 * @param  
 */
$.fn.getDebentureGuideAbstract = function() {

	$paramObj = {
		custId : $('#cust_id').val()
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getDebentureGuideAbstract.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				$debentureGuideList = $result.debentureGuideList;
				if ($debentureGuideList != null && $debentureGuideList.length != 0) {
					for ( var i = 0; i < $debentureGuideList.length; i++) {
						$data = $debentureGuideList[i];

						// 이름을 비교하여 처리할 것의 개수를 삽입함
						$(".mk").each(function(e) {

							if ($(this).attr('data-dgs-mn-kw-id') == $data.DGS_MN_KW_ID) {
								$cntData = '<a href="#" class="blue noneDeco lnkMoveDebentureList" data-dgs-mn-kw-id="' + $data.DGS_MN_KW_ID + '">'
										+ $data.T_STAT_CNT + '건';
								if ($data.T_STAT_CNT > 0) {
									$cntData += '&nbsp;<a class="btnComplete" href="#none">처리완료</a>';
								}
								$(this).next().html($cntData);
							}
						});
					}
				} else {
					// 처리할 것이 없으면 0건을 설정하도록 함.
					$(".mk").next().html('<a href="#" class="blue noneDeco lnkMoveDebentureList" data-dgs-mn-kw-id="">0건');
				}

			} else {
				alert(utfDecode($replCd));
			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 요약(채권가이드 요약정보 조회) 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre> 
 *   채권진단메인키워드관리 수정
 * </pre>
 * @author sungrangkong
 * @since 2014. 07. 18
 * @param $obj : 해당 객체 , $custId : 거래처 순번 , $dgsMnKwId : 채권진단메인키워드순번 , $searchStat : 검색할 상태값 , $stat : 변경 할 상태
 */
$.fn.modifyDebentureGuideKeywordMNG = function($obj, $custId, $dgsMnKwId,
		$searchStat, $stat) {

	$paramObj = {
		custId : $custId,
		dgsMnKwId : $dgsMnKwId,
		searchStat : $searchStat,
		stat : $stat
	};
	$param = $.param($paramObj);

	$.ajax({
		type : "POST",
		url : "${HOME}/debenture/modifyDebentureGuideKeywordMNG.do",
		dataType : "json",
		data : $param,
		success : function(msg) {
			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			if ('00000' == $replCd) {

				// [1] 성공의 경우 처리할 것 관련 UI를 삭제함
				$objParent = $obj.parent();
				$objParent.html('<a href="#" class="blue noneDeco lnkMoveDebentureList" data-dgs-mn-kw-id="">0건');

				// 이메일 장점 ~ 공증 까지 모두 처리할 것이 없는지 체크함
				$stat3BTF = false;
				if ($(".mk").eq(2).next().text() == '0건' && $(".mk").eq(3).next().text() == '0건'
						&& $(".mk").eq(4).next().text() == '0건' && $(".mk").eq(5).next().text() == '0건'
						&& $(".mk").eq(6).next().text() == '0건' && $(".mk").eq(7).next().text() == '0건'
						&& $(".mk").eq(8).next().text() == '0건' && $(".mk").eq(9).next().text() == '0건'
						&& $(".mk").eq(10).next().text() == '0건') {
					$stat3BTF = true;
				}

				// 가압류 ~ 가처분 까지 모두 처리할 것이 없는지 체크함
				$stat15BTF = false;
				if ($(".mk").eq(14).next().text() == '0건' && $(".mk").eq(15).next().text() == '0건'
						&& $(".mk").eq(16).next().text() == '0건' && $(".mk").eq(17).next().text() == '0건'
						&& $(".mk").eq(18).next().text() == '0건' && $(".mk").eq(19).next().text() == '0건'
						&& $(".mk").eq(20).next().text() == '0건') {
					$stat15BTF = true;
				}

				// 지급명령 ~ 소액사건심판
				$stat22BTF = false;
				if ($(".mk").eq(21).next().text() == '0건' && $(".mk").eq(22).next().text() == '0건'
						&& $(".mk").eq(23).next().text() == '0건' && $(".mk").eq(24).next().text() == '0건') {
					$stat22BTF = true;
				}

				// 일반민사소송 ~ 전자민사소송
				$stat26BTF = false;
				if ($(".mk").eq(25).next().text() == '0건' && $(".mk").eq(26).next().text() == '0건') {
					$stat26BTF = true;
				}

				// 재산명시신청 ~ 재산조회
				$stat28BTF = false;
				if ($(".mk").eq(27).next().text() == '0건' && $(".mk").eq(28).next().text() == '0건') {
					$stat28BTF = true;
				}

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
	/**
	 * <pre>
	 *  editCreditline 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	function editCreditline() {
		$('.notModifyCreditMode').hide();
		$('.modifyCreditMode').show();
	}

	/**
	 * <pre>
	 *  setCreditline 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	function setCreditline() {
		if ($.fn.validateCredit() && confirm('여신한도를 수정하시겠습니까?')) {
			$.fn.modifyCreditInfo();
		} else {

		}

	}

	/**
	 * <pre>
	 *  notModifyCreditMode 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	$.fn.convertNotModifyCreditMode = function() {
		$('.notModifyCreditMode').show();
		$('.modifyCreditMode').hide();
	};
	
	
	/**
	 * <pre>
	 *   기술료 등록 이동하는 함수 
	 * </pre>
	 * @author Jung Mi Kim
	 * @since 2015. 03. 30.
	 */
	function goDebenture(){
		 location.href = '${HOME}/debenture/getDebentureForm.do';
	 }

</script>
</head>
<body>
	<%--  Top Area Start --%>
	<jsp:include page="../common/top.jsp" flush="true">
		<jsp:param name="mgnb" value="0101" />
	</jsp:include>
	<%-- Top Area End --%>

<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0101"/>
</jsp:include>
<!-- Left Area End -->

<!-- rightWrap -->	
	<div id="rightWrap">
		<div class="right_tit ltit01_0101"><em>거래처조회</em></div>
		
		<p class="content_tit">거래처정보</p>
		
		
		<div class="tbl01Wrap">
		<form name="modifyCustomerFrm" id="modify_customer_frm" autocomplete="off">
				<%-- FORM HIDDEN AREA START --%>
				<input type="hidden" name="EWCd" id="ew_cd"	value="${customer.EW_CD}" /> 
				<input type="hidden" name="custId"	id="cust_id" value="${customer.CUST_ID}" /> 
				<input type="hidden" name="debnSearchFlag" id="debn_search_flag" value="" /> 
				<input type="hidden" name="custNo" id="cust_no" value="${customer.CUST_NO}" /> 
				<input type="hidden" name="ewRatingPd" id="ew_rating_pd" value="${customer.EW_RATING_PD}" /> 
				<input type="hidden" name="debtApplId" id="debt_appl_id" value="${debtBox.DEBT_APPL_ID}" /> 
				<input type="hidden" id="current_tab" name="currentTab" value="${sBox.currentTab}" />
				<input type="hidden" id="use_print" name="usePrint" value="" />
				<input type="hidden" id="print_yn" name="printYn" value="${sBox.printYn }" /> 
				<input type="hidden" id="summary_flag" name="summaryFlag" value="" /> 
				<input type="hidden" id="dgs_mn_kw_id" name="dgsMnKwId" value="" /> 
				<input type="hidden" id="customer_plan_list_num" name="customerPlanListNum" value="${sBox.customerPlanListNum}" />
				<input type="hidden" id="customer_prs_list_num" name="customerPrsListNum" value="${sBox.customerPrsListNum}" />
				<input type="hidden" id="customer_col_list_num" name="customerColListNum" value="${sBox.customerColListNum}" />
				<input type="hidden" name="nonpaymentType" id="nonpaymentType" value="" />
				<input type="hidden" name="debnSearchType" id="debnSearchType" value="" />
				<input type="hidden" name="searchPeriodType" id="searchPeriodType" value="" />
				<input type="hidden" name="bizKwd" id="bizKwd" value="" />
				<%-- FORM HIDDEN AREA END --%>
		
					<table>
						<colgroup>
							<col width="150px">
							<col width="240px">						
							<col width="150px">
							<col width="240px">									
						</colgroup>
					<tbody>						
						<tr>
							<th>거래처</th>
							<td>${customer.CUST_NM}</td>
							<th class="second_thl">사업자등록번호</th>
							<td>${fn:substring(customer.CUST_NO,0,3)}-${fn:substring(customer.CUST_NO,3,5)}-${fn:substring(customer.CUST_NO,5,10)}</td>
						</tr>
						<tr>
							<th>대표자</th>
							<td>${customer.OWN_NM}</td>
							<th class="second_thl">이메일</th>
							<td>
							<c:set var="EMAIL" value="${fn:split(customer.EMAIL,'@')}" />
							${fn:join(EMAIL,'@')}
							</td>
						</tr>
						<tr>
							<th>법인등록번호</th>
							<td colspan="3">${customer.CORP_NO}</td>
						</tr>
						<tr>
							<th rowspan="2">회사주소</th>
							<td colspan="3">
							<%-- <c:set var="POST_NO" value="${fn:split(customer.POST_NO,'-')}" />
							${fn:join(POST_NO,'-')} --%> (${customer.POST_NO})  ${fn:trim(customer.ADDR_1)} 
							</td>
						</tr>
						<tr>							
							<td colspan="3">${fn:trim(customer.ADDR_2)}</td>
						</tr>
						<tr>
							<th>업종</th>
							<td>${customer.BIZ_TYPE}</td>
							<th class="second_thl">업태</th>
							<td>${customer.BIZ_COND}</td>
						</tr>
						<tr>
							<th>전화</th>
							<td>
							<c:set var="TEL_NO" value="${fn:split(customer.TEL_NO,'-')}" />
							${fn:join(TEL_NO,'-')}
							</td>
							<th class="second_thl">팩스</th>
							<td>
							<c:set var="FAX_NO" value="${fn:split(customer.FAX_NO,'-')}" />
							${fn:join(FAX_NO,'-')}
							</td>
						</tr>
						<tr>
							<th>휴대폰</th>
							<td colspan="3">
							<c:set var="MB_NO" value="${fn:split(customer.MB_NO,'-')}" />
							${fn:join(MB_NO,'-')} 
							</td>
						</tr>
					</tbody>
					</table>
					</form>
				</div>
				
				<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_get_list">목록</a></div><!-- TODO: transIf 추가개발될 사항 -->
				</div>
				
				
			<c:if test="${sBox.isSessionDebtGrnt eq true}">
				<p class="content_tit_u40">채무불이행등록</p>
				<div class="tbl03Wrap">
					<table>
					<tbody>
						<!-- <tr>
							<th>현재 진행중인 채무불이행 상태가 없습니다.</th>
						</tr> -->
						<tr>
							<td><div class="btn_cbbtn_01">
							<a href="#none" class="on" id="btn_nonpayment_regist">채무불이행등록</a></div></td>
						</tr>
					</tbody>
					</table>
				</div> 
			</c:if>
				
				<%-- 진행상태 영역 시작 
				<div id="defaultWrap">

					<div class="default_stepbox">
						채무불이행 진행상태가 없는 경우
						<c:if test="${debtProcess.STEP1 eq '' || debtProcess.STEP1 eq null}">
							<div class="default_stepbox_none">
								<p>현재 진행중인 채무 불이행 상태가 없습니다.</p>
								<a href="#none" class="longBtnOn" id="btn_nonpayment_regist">채무불이행 등록</a>
							</div>
						</c:if>

						//채무불이행 진행상태가 있는 경우 
						<c:if test="${debtProcess.STEP1 ne '' && debtProcess.STEP1 ne null}">
							<ul>
								<c:if test="${debtProcess.STEP1 ne ''}">
									<li class="s1">
										<p class="default_step_n">${debtProcess.STEP1 }</p>
										<p class="default_step_d">${debtProcess.STEP1_DT }</p> <em></em>
									</li>
								</c:if>

								<c:if test="${debtProcess.STEP2 ne ''}">
									<li class="s2">
										<p class="default_step_n">${debtProcess.STEP2 }</p>
										<p class="default_step_d">${debtProcess.STEP2_DT }</p> <em></em>
									</li>
								</c:if>

								<c:if test="${debtProcess.STEP3 ne ''}">
									<li class="s2">
										<p class="default_step_n">${debtProcess.STEP3 }</p>
										<p class="default_step_d">${debtProcess.STEP3_DT }</p> <em></em>
									</li>
								</c:if>

								<c:if test="${debtProcess.STEP4 ne ''}">
									<li class="s2">
										<p class="default_step_n">${debtProcess.STEP4 }</p>
										<p class="default_step_d">${debtProcess.STEP4_DT }</p> <em></em>
									</li>
								</c:if>

								<c:if test="${debtProcess.STEP5 ne ''}">
									<li class="s2">
										<p class="default_step_n">${debtProcess.STEP5 }</p>
										<p class="default_step_d">${debtProcess.STEP5_DT }</p> <em></em>
									</li>
								</c:if>

								<c:if test="${debtProcess.STEP6 ne ''}">
									<li class="s2">
										<p class="default_step_n">${debtProcess.STEP6 }</p>
										<p class="default_step_d">${debtProcess.STEP6_DT }</p> <em></em>
									</li>
								</c:if>
							</ul>
						</c:if>
					</div>

					<div class="btmBtnWrap">
						<p class="rightBtn">

							 ERP 회원이 아니면서 && 채무불이행 신청 권한 권한이 있는경우만 버튼이 보임 
							<c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtApplGrn eq true}">
								<c:choose>
									심사대기(AW), 심사중(EV)
									<c:when test="${(debtBox.STAT eq 'AW') or (debtBox.STAT eq 'EV')}">
										<a class="ApplyBtn_Type2 btnApplyCancel" href="#none" data-stat="${debtBox.STAT}">신청취소</a>
									</c:when>
									 결제대기(AA)
									<c:when test="${debtBox.STAT eq 'AA'}">
										<a class="ApplyBtn_Type2 btnPayment" href="#none" data-stat="${debtBox.STAT}">결제</a>
										<a class="ApplyBtn_Type2 btnApplyCancel" href="#none" data-stat="${debtBox.STAT}">신청취소</a>
									</c:when>
									통보서 준비(PY) 통보서 발송(NT), 등록전민원발생(채무불이행등록전통보서발생시)(CA),등록완료(RC),등록전민원발생(추가증빙)(FA) 등록후민원발생(정보등록완료 후)(CB),등록후민원발생(추가증빙)(FB)
									<c:when test="${(debtBox.STAT eq 'PY') or (debtBox.STAT eq 'NT') or (debtBox.STAT eq 'CA') or (debtBox.STAT eq 'RC') or (debtBox.STAT eq 'CB') or (debtBox.STAT eq 'FA') or (debtBox.STAT eq 'FB')}">
										 현재 채무금액 정정요청 진행중 상태에서는 채무금액정정요청을 할 수 없음
										<c:if test="${debtBox.MOD_STAT ne 'R'}">
											<a class="ApplyBtn_Type2 btnDebtReqCor" href="#none" data-stat="${debtBox.STAT}">채무금액 정정 요청</a>
										</c:if>
										<a class="ApplyBtn_Type2 btnDebtReqRel" href="#none" data-stat="${debtBox.STAT}">해제 요청</a>
									</c:when>
								</c:choose>
							</c:if>
						</p>
					</div>

				</div>

				 진행상태 영역 종료 --%>
				
				
				<div id="content_tit_pr">
					<div class="content_tit pru40">신용정보 변동이력 <p class="point_txt1"></p> &nbsp;    </div>					
					<div id="btnWrap_rpt">
						<c:choose>
							<c:when test="${fn:length(customerCreditHistoryList) eq 0 or customerCreditHistoryList eq null }">
								<div class="btn_rdgbtn_01"><a class="on" id="btnGetCrdList" href="javascript:alert('신용정보가 없습니다.')">신용정보 확인하기</a></div>
							</c:when>
							<c:otherwise>
								<div class="btn_rdgbtn_01"><a class="smallBtnType01 crdInfo" id="btnGetCrdList" href="#none">신용정보 확인하기</a></div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				
				<div class="tbl04Wrap" id="crd_history_list">
					<table>
					<tbody>
						<tr>
							<th width="50%" class="first_thlwr4">신용변동일</th>
							<th width="50%">신용정보(EW)등급</th>
						</tr>
						<c:if test="${fn:length(customerCreditHistoryList) eq 0 or customerCreditHistoryList eq null }">
							<td class="first_tdlwr4 noSearch" colspan="2">결과가 없습니다.</td>
						</c:if>
						<c:forEach var="customerCreditHistoryList" items="${customerCreditHistoryList}">
						<tr>
							<td class="first_tdlwr4">
							<c:if test="${not empty customerCreditHistoryList.EW_DT }">
								${fn:substring(customerCreditHistoryList.EW_DT,0,4)}-${fn:substring(customerCreditHistoryList.EW_DT,4,6)}-${fn:substring(customerCreditHistoryList.EW_DT,6,10)} 
							</c:if>
							</td>
							<td>
							<c:if test="${not empty customerCreditHistoryList.CRD_TYPE_VALUE }">
								${customerCreditHistoryList.CRD_TYPE_VALUE } 
							</c:if>
							</td>
						</tr>						
						</c:forEach>
					</tbody>
					</table>
				</div>
				
				<!-- page_num_wrap -->
				<div id="page_num_wrap">
					<div class="page_num" id="getPageView">
						<c:out value="${historyPcPage}" escapeXml="false" />
					</div>
				</div>
				<!-- page_num_wrap -->
			</div>
		
</div>
	<%--  footer Area Start --%>
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<%-- footer Area End --%>
</body>
</html>