<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 독촉관리 조회 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="custPrs" value="${result.custPrs}" />
<c:set var="commonCode" value="${result.commonCode}" />
<c:set var="bondMbrList" value="${result.bondMbrList}" />
<c:set var="debnList" value="${result.debnList}" />
<c:set var="debnCount" value="${result.debnCount}" />
<c:set var="fileList" value="${result.fileList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/popupCol.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerPrs.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 
	/**
	 * <pre>
	 *   취소 버튼 클릭시 팝업 닫기
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	$('#btn_customer_pln_close').live({
		click : function(e){
			self.close();
		}
	});
	 
	 /**
	 * <pre>
	 * 파일 추가하기 (+) 버튼 Click Event
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 15.
	 */
	 $(document).on('click', '.btn_plus', function(){

		 $parentEL = $(this).parent().parent();
		 $cloneEL = $parentEL.clone();
		 
		 $cloneEL.find('input').val('');
		 
		 if($cloneEL.find('.btn_minus').length == 0){
		 	$cloneEL.find('.dataUpWrap02').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
		 }
		 
		 $parentEL.find('.dataUpWrap02 > a').remove();
		 $parentEL.find('.dataUpWrap02').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
		 
		 $parentEL.after("<div class='spliter'></div>");
		 $parentEL.next().after($cloneEL);
		 
	 });
	 
	 /**
	 * <pre>
	 * 파일 추가하기 (-) 버튼 Click Event
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 15.
	 */
	 $(document).on('click', '.btn_minus', function(){
		 
		 $parentEL = $(this).parent().parent();
		 $tdEL = $parentEL.parent();
		 
		 // 이미 삭제 대상일 경우 로직 수행하지 않음
		 if($parentEL.find('span').hasClass('selectDeleteFileSpan')) {
			 return false;
		 }
		 
		 // anchor일 경우
		 if($parentEL.find('a').hasClass('browsing')) {
			 $parentEL.find('span').addClass('selectDeleteFileSpan');
			 $parentEL.find('span').css('text-decoration','line-through'); 
		 }
		 
		 // input box가 마지막이 아닐 경우[엘리먼트 삭제]
		 else if($tdEL.find('.dataUploadArea > input[type=file]').length > 1) {
			 if($parentEL.next().attr('class') == 'spliter'){
				 $parentEL.next().remove();	 
			 }else{
				 $parentEL.prev().remove();
			 }
			 $parentEL.remove(); 
		 }
		 
		 // input box가 마지막일 경우[input box value만 공백으로 변경]
		 else if($tdEL.find('.dataUploadArea > input[type=file]').length == 1) {
			// Firefox 용 input file 초기화
			$tdEL.find('.dataUploadArea > input[type=file]').val('');
			
			// IE & Chrome 용 input file 초기화
			$tdEL.find('.dataUploadArea > input[type=file]').replaceWith( $tdEL.find('.dataUploadArea > input[type=file]').clone(true) );
		 }
		 
		 $last = $tdEL.find('.dataUploadArea').last();
		 $last.find('.dataUpWrap02').remove();
		 $last.append('<div class="dataUpWrap02"><a class="btn_plus" href="#none"><img src="${IMG}/common/ico_data_p.gif"></a>&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a></div>');
		 
		 // 삭제 대상 파일 순번 설정
		 if($parentEL.find('span.selectDeleteFileSpan > a').is('[data-custPrsFileSn != ""]')) {
			 $custPrsFileSn = $('#delete_file_param').val();
			 $custPrsFileSn += ($custPrsFileSn.length > 0 ? "|" : "") + $parentEL.find('span.selectDeleteFileSpan > a').attr('data-custPrsFileSn');
			 $('#delete_file_param').val($custPrsFileSn); 
		 }
	 });
	 
	/**
	 * <pre>
	 *   증빙 파일 찾기 Click Event
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 */
	 $(".ApplyBtn02").live('click',function(e){
		 $(this).parent().find('input[type=file]').click();
	 });
	 
	 /**
	 * <pre>
	 * 증빙 파일 선택되어지면 디자인 되어진 input box에 값을 넣는 Event
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 */
	 $("input[type=file]").live('change',function(e){
		 e.preventDefault();
		 $(this).parent().find('input[type=text]').val($(this).val());
	 });
	 
	 /**
	 * <pre>
	 *   저장[추가] 버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_customer_prs_save').dblclick(function(e){
		e.stopPropagation();
		e.preventDefault();
		console.log("dbclick");
		return false;
	});

	$('#btn_customer_prs_save').click(function(e){
		if($('.chkDebnId:not(:disabled):checked').length == 0) {
			alert('채권을 하나 이상 선택해주세요.');
			return false;
		}
		
		// 파일  검증
		$fileRegistTF = true;
		$('.default_txtFileInput').each(function(e){
			var result = checkUploadFile(this);
			if(!result){
				$fileRegistTF = false;
				return false;
			}
		});
		if(!$fileRegistTF){
			return false;
		}
		
		$.fn.addCustomerPrs();		
	});

	 
	/**
	 * <pre>
	 *   체크박스 클릭시 모두 체크 활성화 여부 판단하여 체크하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29.
	 */
	 $('.chkDebnId').live({
		 click : function(e){

			<%-- 선택 채권 건수, 선택채권 미수금총액 설정 --%>
		 	$.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T');
			$('#selected_debn').text($('.chkDebnId:checked').length + '건');
			$('#debn_cnt').val($('.chkDebnId:checked').length);
			 
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		 }
	 });
	 
	/**
	 * <pre>
	 *   체크박스 모두 선택 토글 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#all_check').live({
		click : function(){
			if($(this).is(':checked')) {
				$('.chkDebnId:not(:disabled)').attr('checked', false).trigger('click');
			} else {
				$('.chkDebnId:not(:disabled)').attr('checked', true).trigger('click');
			}
		}
	});
	 
	/**
	 * <pre>
	 *   검색 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29 
	 */
	$('#btn_prs_search').live({
		click : function(e){
			$custId = $('#cust_id').val();
			$orderCondition = $('#order_condition').val();
			$orderType = $('#order_type').val();
			$bondMbrId = $('#bond_mbr_id').val();
			$custPrsSn = $('#cust_prs_sn').val();
			
			$.fn.getDebnListForPrs($custId, $orderCondition, $orderType, $bondMbrId, $custPrsSn);
		}
	});
	 
	/**
	 * <pre>
	 *   저장[수정] 버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 29. 
	 */
	$('#btn_customer_prs_modify').live({
		click : function(e){
			if($('.chkDebnId:not(:disabled):checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			// 파일  검증
			$fileRegistTF = true;
			$('.default_txtFileInput').each(function(e){
				var result = checkUploadFile(this);
				if(!result){
					$fileRegistTF = false;
					return false;
				}
			});
			if(!$fileRegistTF){
				return false;
			}
			$.fn.modifyCustomerPrs();
		}
	});
	 
	/*
	* <pre>
	*  첨부파일 다운로드 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014.06.13
	*/
	$(document).on('click', '.lnkPrsFileDown', function(e){		
		$('#prs_file_sn').remove();
		$('form').attr({'action':'${HOME}/customer/getPrsFileDown.do', 'method':'POST'})
		.append('<input type="hidden" id="prs_file_sn" name="fileSn" value="' + $(this).attr('data-custPrsFileSn') + '"/>')
		.submit();
	});
	 
});
 
<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%>
 
/**
 * <pre>
 *   선택한 값 자동 계산 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 * @param $listEl : 계산할 엘리먼트, $targetEl : 결과 세팅 엘리먼트, 
 *        $suffix : 결과 뒤에 추가할 메세지, $valueType : 함수 사용 조건[V, T] 
 */
 $.fn.selectedTargetCalc = function($listEl, $targetEl, $suffix, $valueType){
	 $result = 0;
	 
	 $listEl.each(function() {
		$value = '';
		switch($valueType) {
			case 'V' :
				$value = $(this).val();
				break;
			case 'T' :
				$value = $(this).text();
				break;
		}  
		
		$result += parseInt(($value.replace(/[^0-9]/g, '')), 10);
	 });
	 
	 $targetEl.html(addComma($result) + $suffix);
	 
	 return $result;
 };
 
/**
 * <pre> 
 *   파라미터 구성 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 22
 *
 * @param $targetEl : 수집 대상 엘리먼트
 * @param $selector : 수집 대상 셀렉터(클래스)
 * @param $paramEl : 파라미터 수집 대상 객체
 * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
 * @param $dataSplitStr : 구분자[데이터]
 * @param $rowSplitStr : 구분자[row]
 */
$.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {
	
	$result = '';
	
	$.each($targetEl, function() {
		if($(this).hasClass($selector.toString())) {
			$tmpStr = '';
			for(var i = 0 ; i < $paramEl.length; i++) {
				if($tmpStr != '') {
					$tmpStr += $dataSplitStr;
				}
				$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
			}		
			
			if($result != '') {
				$result += $rowSplitStr;
			}
			$result += $tmpStr;
		}
	});
	
	return $result;
};


/**
 * <pre> 
 *   채권 리스트를 재구성 하는 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 29
 *
 * @param $target : 리스트 구성 영역 Element
 * @param $listEl : 리스트를 구성할 Data
 */
$.fn.reloadDebnList = function($target, $listEl) {
	$htmlStr = '';
 	
 	$.each($listEl, function() {
		$htmlStr +=
				'<tr>' +
					'<td class="checkDebn">' +
						'<input type="checkbox" class="chkDebnId ' + (this.CUST_PRS_STATUS == 'N' ? 'insertDebn' : '') 
						+ ' ' + (this.CUST_PRS_STATUS == 'E' ? 'updateDebn' : '') + '" id="chk_' + (this.DEBN_ID) 
						+ '" value="' + this.DEBN_ID +'" >' +
					'</td>' +
					'<td class="bondMbrNm">' + 
						this.BOND_MBR_NM + 
						'<input type="hidden" id="input_' + this.DEBN_ID + '" class="debnId insertDebn" value="' + this.DEBN_ID +'" />' +  
					'</td>' + 
					'<td class="billDt">' + this.BILL_DT + '</td>' + 
					'<td class="list_right sumAmt">' + 
						'<p class="noBorder">' +
							this.SUM_AMT +
						'</p>' +
					'</td>' +
					'<td class="list_right rmnAmt last">' + 
						'<p class="noBorder">' + 
							this.RMN_AMT +
						'</p>' +
					'</td>' +
				'</tr>';
 	});
 	
 	$target.empty().append($htmlStr).find('.updateDebn').trigger('click');
 	$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
 	
 };
 
 /**
  * <pre> 
  *   <li> 태그 리스트 백그라운드 CSS 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2013. 08. 28
  *
  */
  $.fn.setBackgroundEvenAndOdd = function($targetEl, $subEl, $evenColor, $oddColor) {
 	 $targetEl.find($subEl + ':even').css('background-color', $evenColor);
 	 $targetEl.find($subEl + ':odd').css('background-color', $oddColor);
  };
  
  /**
   * <pre> 
   *   리스트가 없을 경우
   * </pre>
   * @author Jong Pil Kim
   * @since 2014. 04. 17
   * @param $target : 리스트 구성 영역 Element
   * @param $noDataEl : 리스트를 구성할 Data
   */
  $.fn.reloadNoDataList = function($target, $noDataEl) {
  	 $htmlStr = '';
  	 $target.empty().append($noDataEl);
  	 $.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
  };

<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>

<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%> 
  
/**
 * <pre> 
 *   거래처 독촉 등록 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 29
 * @param insertParam : INSERT 채권, custId : 거래처 순번, 
 *		  prsType : 독촉 유형, rmkTxt : 비고, prsDt : 독촉일자, prsFile : 첨부파일
 */
 $.fn.addCustomerPrs = function() {
 	 
	$paramEl = new Array();
	$replaceEl = new Array();
	$paramEl.push('.chkDebnId');
	$replaceEl.push('');
	 
	// insertParameter 세팅
	$insertParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'insertDebn', $paramEl, $replaceEl, '', '|');
	$('#insert_debn_param').val($insertParam);
	 
	$('form').attr({'action':'${HOME}/customer/addCustomerPrs.do', 'method':'POST', 'enctype':'multipart/form-data'})
	 .ajaxSubmit({
		 beforeSend : function(){
			$('#btn_customer_prs_save, #btn_customer_pln_close').hide();
		},
	 	dataType : "json",
		async : true,
		success : function(msg){
			$result = msg;
			$replCd = $result.REPL_CD;
		    $replMsg = $result.REPL_MSG;
		    
			if($replCd == '00000') {
				alert("독촉 등록이 완료되었습니다.");
				if($('.customerSubTab', window.opener.document).length > 0) {
		    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
		    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
		    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
		    	} 
		    	window.open("about:blank","_self").close();
			} else {
				alert(utfDecode($replMsg));
			}
		},
		error : function(xmlHttpRequest,textStatus,errorThrown) {
			alert("독촉 등록 도중 에러발생 [" + textStatus + "]");
		}
	});	
 };
 
 
/**
 * <pre> 
 *  채권 리스트 재로딩 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 29.
 *
 */
 $.fn.getDebnListForPrs = function($custId, $orderCondition, $orderType, $bondMbrId, $custPrsSn) {
	 
	 $paramObj = {
			 custId : $custId,
			 orderCondition : $orderCondition,
			 orderType : $orderType,
			 bondMbrId : $bondMbrId,
			 custPrsSn : $custPrsSn 
	 };
	 
	 $param = $.param($paramObj);
	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/getDebnListForPrs.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	
			    	$targetEl = $('#debn_list_content tbody');
			    	$('#all_check').attr('checked', true).trigger('click');
			    	
			    	<%-- 채권 조회 결과 리스트 조회 --%>
			    	if($debnList.length > 0) {
			    		$.fn.reloadDebnList($targetEl, $debnList);	
			    	} else{
			    		$.fn.reloadNoDataList($targetEl, '<tr><td colspan="5"><div class="noSearch">결과가 없습니다.</div></td></tr>');
			    	}
			    	
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("채권 조회 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
 /**
  * <pre> 
  *   거래처 독촉 수정 Ajax 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 29
  * @param custId : 거래처 순번, prsType : 독촉 유형, rmkTxt : 비고, prsDt : 독촉일자, prsFile : 첨부파일
  *        insertDebnParam : Insert 대상 채권, deleteDebnParam : Delete 대상 채권
  */
  $.fn.modifyCustomerPrs = function() {
  	 
 	$paramEl = new Array();
 	$replaceEl = new Array();
 	$paramEl.push('.chkDebnId');
 	$replaceEl.push('');
 	 
 	// insertDebnParam 세팅
 	$insertDebnParam = $.fn.makeParam($('.chkDebnId:not(:disabled):checked'), 'insertDebn', $paramEl, $replaceEl, '', '|');
 	$('#insert_debn_param').val($insertDebnParam);
 	
 	// deleteDebnParam 세팅
 	$deleteDebnParam = $.fn.makeParam($('.chkDebnId:not(:disabled):not(:checked)'), 'updateDebn', $paramEl, $replaceEl, '', '|');
 	$('#delete_debn_param').val($deleteDebnParam);
 	
 	$('form').attr({'action':'${HOME}/customer/modifyCustomerPrs.do', 'method':'POST'})
 	 .ajaxSubmit({
 	 	dataType : "json",
 		async : true,
		beforeSend : function(){
			$('#btn_customer_prs_modify, #btn_customer_pln_close').hide();
		},
 		success : function(msg){
 			
 			$result = msg;
 			$replCd = $result.REPL_CD;
 		    $replMsg = $result.REPL_MSG;
 		    
 			if($replCd == '00000') {
 				alert("독촉 수정이 완료되었습니다.");
 				if($('.customerSubTab', window.opener.document).length > 0) {
		    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
		    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
		    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
		    	} 
		    	window.open("about:blank","_self").close();
 			} else {
 				alert(utfDecode($replMsg));
 			}
 		},
 		error : function(xmlHttpRequest,textStatus,errorThrown) {
 			alert("독촉 수정 도중 에러발생 [" + textStatus + "]");
 		}
 	});	
  };
  
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
</script>
</head>
<body>

<div class="pWrap">
	<header>
		<c:choose>
			<c:when test="${sBox.custPrsSn eq null or sBox.custPrsSn eq ''}">
				<h1><img src="${IMG}/customer/h3_prs_regist.gif" alt="독촉관리 작성" /></h1>
			</c:when>
			<c:otherwise>
				<h1><img src="${IMG}/customer/h3_prs_modify.gif" alt="독촉관리 수정" /></h1>
			</c:otherwise>
		</c:choose>
	</header>
	
	<div class="pContent">
		<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
			<div class="pSearch_list">
				<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자, 금액별로 정렬">
					<c:forEach var="orderConditionList" items="${commonCode.prsOrderConditionList}">
						<option value="${orderConditionList.KEY}" <c:if test="${orderConditionList.KEY eq sBox.orderCondition}">selected="selected"</c:if> >${orderConditionList.VALUE}</option>
					</c:forEach>
				</select>
				<select id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
					<c:forEach var="orderTypeList" items="${commonCode.orderTypeList}">
						<option value="${orderTypeList.KEY}" <c:if test="${orderTypeList.KEY eq sBox.orderType}">selected="selected"</c:if> >${orderTypeList.VALUE}</option>
					</c:forEach>
				</select>
				<label for="listNo"></label>
				<select name="bondMbrId" id="bond_mbr_id" class="listOrderOption">
					<option value="" <c:if test="${sBox.bondMbrId eq null || sBox.bondMbrId eq '' }">selected="selected"</c:if> >전체</option>
					<c:forEach var="bondMbrList" items="${bondMbrList}">
						<option value="${bondMbrList.MBR_ID}" <c:if test="${bondMbrList.MBR_USR_ID eq null or bondMbrList.MBR_USR_ID eq ''}">class="mem_type_N"</c:if> <c:if test="${bondMbrList.MBR_ID eq sBox.bondMbrId}">selected="selected"</c:if> >${bondMbrList.USR_NM}</option>
					</c:forEach>
				</select>
				<a href="#none" class="pBtn_Search" id="btn_prs_search">검색</a>
			</div>
		</c:if>
	</div>
	
	<form name="customerPrsForm" id="customer_prs_form" autocomplete="off" encType="multipart/form-data">
		<%-- INPUT HIDDEN AREA --%>
		<input type="hidden" name="custPrsSn" id="cust_prs_sn" value="${sBox.custPrsSn}" />
		<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}" />
		<input type="hidden" id="insert_debn_param" name="insertDebnParam" value="" />
		<input type="hidden" id="delete_debn_param" name="deleteDebnParam" value="" />
		<input type="hidden" id="delete_file_param" name="deleteFileParam" value="" />
		
		<div class="pTableWrap">
			<div class="pTableTop">
				<table summary="독촉관리 작성" class="pTableTitle debnListTitle" id="debn_list_title">
				<caption>독촉관리 작성</caption>
				<colgroup>
					<col width="45px">
					<col width="99px">
					<col width="165px">
					<col width="165px">
					<col width="165px">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">
							<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<input type="checkbox" id="all_check">
							</c:if>
						</th>
						<th scope="col">채권담당자</th>
						<th scope="col">세금계산서작성일</th>
						<th scope="col">합계금액</th>
						<th scope="col" class="last">미수금액</th>					
					</tr>
				</thead>
				</table>
			</div>
			<div class="pTableContent">
				<table summary="독촉관리 작성" class="pTable debnListContent" id="debn_list_content">
				<caption>독촉관리 작성</caption>
				<colgroup>
					<col width="45px">
					<col width="99px">
					<col width="165px">
					<col width="165px">
					<col width="165px">
				</colgroup>
				<tbody>
					<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
					<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
						<tr><td colspan="5"><div class="noSearch">결과가 없습니다.</div></td></tr>						
					</c:if>
					<%-- 미수총금액 합계 변수 --%>
					<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="0" pattern="#"/>
					<c:forEach var="debnList" items="${debnList}">
						<tr>
							<td class="checkDebn">
								<input type="checkbox" class="chkDebnId <c:if test="${debnList.CUST_PRS_STATUS ne 'E'}">insertDebn</c:if> <c:if test="${debnList.CUST_PRS_STATUS eq 'E'}">updateDebn</c:if>" id="chk_${debnList.DEBN_ID}" value="${debnList.DEBN_ID}" <c:if test="${debnList.CUST_PRS_STATUS eq 'E'}">checked="checked"</c:if> <c:if test="${not sBox.isSessionCustAddGrn or not sBox.isSessionDebnSearchGrn }">disabled="disabled"</c:if> >
							</td>
							<td class="bondMbrNm">
								${debnList.BOND_MBR_NM}
								<input type="hidden" id="input_${debnList.DEBN_ID}" class="debnId insertDebn" value="${debnList.DEBN_ID }" /> 
							</td>
							<td class="billDt">${debnList.BILL_DT}</td>
							<td class="list_right sumAmt">
								<p class="noBorder">
									${debnList.SUM_AMT}	
								</p>
							</td>
							<td class="list_right rmnAmt last">
								<p class="noBorder">
									${debnList.RMN_AMT}
									<fmt:formatNumber var="rmnAmt" type="NUMBER" value="${fn:replace(debnList.RMN_AMT, ',', '')}" pattern="#"/>
								</p>
							</td>
							<%-- 미수총금액 합계 연산 --%>
							<c:if test="${debnList.CUST_PRS_STATUS eq 'E'}">
								<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="${totUnpdAmt + rmnAmt}" pattern="#"/>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		
		<div class="pSumWrap">
			<table class="pSumTable">
			<caption>통계</caption>
			<colgroup>
				<col width="50%">
				<col width="50%">
			</colgroup>
			<tbody>
				<tr>
					<td>
						선택채권 : 
						<c:choose>
							<c:when test="${(custPrs.DEBN_CNT eq null) or (custPrs.DEBN_CNT eq '')}">
							<p id="selected_debn">
								0건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="0" />
							</c:when>
							<c:otherwise>
							<p id="selected_debn">
								${custPrs.DEBN_CNT}건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="${custPrs.DEBN_CNT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						선택채권 미수금총액 : 
						<c:choose>
							<c:when test="${sBox.custPrsSn eq null or sBox.custPrsSn eq ''}">
								<p id="selected_tot_unpd_amt">0원</p>
							</c:when>
							<c:otherwise>
								<fmt:formatNumber var="totUnpdAmtComma" type="NUMBER" value="${totUnpdAmt}" groupingUsed="true"/>
								<p id="selected_tot_unpd_amt">${totUnpdAmtComma}원</p>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		
		<div class="pInputWrap">
			<table class="pInputTable defaultTbl01_01">
			<caption>독촉관리 작성테이블</caption>
			<colgroup>
				<col width="100px">
				<col width="2px">
				<col width="538px">
			</colgroup>
			<tbody>
				<tr>
					<td><p>독촉일</p></td>
					<td><p>:</p></td>
					<td>
						<c:choose>
							<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (empty sBox.custPrsSn) }">
								<input type="text" name="prsDt" id="prs_dt" class="firstDate" title="독촉일" value="${result.currentDt}" readonly="readonly">
							</c:when>
							<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (not empty sBox.custPrsSn) }">
								<input type="text" name="prsDt" id="prs_dt" class="firstDate" title="독촉일" value="${custPrs.PRS_DT}" readonly="readonly">
							</c:when>
							<c:otherwise>
								${custPrs.PRS_DT}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="middle">
						<p>독촉유형</p>
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_prs_type">
						<c:choose>
							<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<select id="prs_type" class="listOrderOption" name="prsType" title="됵촉 유형">
									<c:forEach var="prsTypeList" items="${commonCode.prsTypeList}">
										<option value="${prsTypeList.KEY}" <c:if test="${prsTypeList.KEY eq custPrs.PRS_TYPE}">selected="selected"</c:if> >${prsTypeList.VALUE}</option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<c:forEach var="prsTypeList" items="${commonCode.prsTypeList}">
									<c:if test="${prsTypeList.KEY eq custPrs.PRS_TYPE}">${prsTypeList.VALUE}</c:if>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="middle"><p>첨부파일 <br/>(최대 20MB)</p></td>
					<td class="middle"><p>:</p></td>
					<td class="middle">
						<c:forEach var="fileList" items="${fileList}">
							<div class="dataUploadArea">
								<span>
									<a class="browsing lnkPrsFileDown" data-custPrsFileSn="${fileList.CUST_PRS_FILE_SN}" data-file-nm="${fileList.FILE_NM}" data-deleteFilePath="${fileList.FILE_PATH}" >
										${fileList.FILE_NM}
									</a>
								</span>
								<div class="dataUpWrap02">
									<c:if test="${sBox.isSessionDebnAddGrn and (sBox.isMyPageView ne 'Y')}">
										<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
									</c:if>
								</div>
							</div>
							<div class="spliter"></div>
						</c:forEach>
						<c:if test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
							<div class="dataUploadArea">
								<input type="file" name="prsFile" class="default_txtFileInput" maxlength="100" />
								<div class="dataUpWrap02">
								<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
								<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
								</div>
							</div>
						</c:if>
					</td>
				</tr>
				<tr>
					<td class="last"><p>비고</p></td>
					<td class="last"><p>:</p></td>
					<td class="last">
						<c:choose>
							<c:when test="${sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn }">
								<input type="text" id="rmk_txt" name="rmkTxt" class="txtInput" style="width:400px" title="비고" value="${custPrs.RMK_TXT}" maxlength="200">
							</c:when>
							<c:otherwise>
								${custPrs.RMK_TXT}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		<div class="pBtnWrap">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※  문서 (txt, rtf, xls, xlsx, doc, docx, ppt, pptx, pps, ppsx, hwp, pdf), 이미지 (gif,jpg, jpeg, png, bmp), 압축 (zip) 파일 첨부가능<br />
			<div class="pBtnMiddle">
				<c:choose>
					<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (sBox.custPrsSn ne null and sBox.custPrsSn ne '') }">
						<a href="#none" class="on" id="btn_customer_prs_modify">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">닫기</a>
					</c:when> 
					<c:when test="${(sBox.isSessionCustAddGrn and sBox.isSessionDebnSearchGrn) and (sBox.custPrsSn eq null or sBox.custPrsSn eq '') }">
						<a href="#none" class="on" id="btn_customer_prs_save">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">취소</a>
					</c:when>
					<c:otherwise>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">닫기</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form>
</div>
</body>
</html>