<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>

<script type="text/javascript">

$(document).ready(function(){
});
</script>
</head>

<body>

<div id="Wrapper">

<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0001" />
</jsp:include>
<!-- Top Area End -->
	
<!-- contentArea -->
<div id="containerWrap">
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0004"/>
</jsp:include>
<!-- Left Area End -->

<!-- contentArea -->
	<div id="rightWrap">
	<div class="right_tit ltit00_0401"><em>1:1문의사항</em></div>
		<iframe  src="${inquiryDomain}${sBox.sessionUsrId}" height="360" width="780"  frameborder="0" style="overflow-y:hidden;"></iframe>
	</div>
<!-- //contentArea -->

	<!--  footer Area Start -->
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<!-- footer Area End -->
</div>
</div>
</body>
</html>
