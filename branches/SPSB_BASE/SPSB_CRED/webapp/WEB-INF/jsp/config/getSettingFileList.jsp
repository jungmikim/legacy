<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="sBox" value="${sBox}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
});

</script>
</head>
<body>
<div id="Wrapper">
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0301"/>
</jsp:include>
<%-- Top Area End --%>



<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0304"/>
</jsp:include>
<!-- Left Area End -->
	
	<!-- rightWrap -->	
	<div id="rightWrap">
				<div class="right_tit ltit03_0401"><em>설정파일관리</em></div>
				
					<div class="content_tit">설정파일 관리</div>
					
					<strong><font color="red">개발 여부 검토 필요</font></strong>
					
										
				</div>
				
				
				
				
			</div>
		</div>


		  
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>
</div> <!-- containerWrap -->

</body>
</html>