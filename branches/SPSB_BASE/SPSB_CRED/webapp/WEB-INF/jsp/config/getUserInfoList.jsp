<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="userList" value="${result.userList}"/>
<c:set var="sBox" value="${sBox}" />
<c:set var="pcPage" value="${result.pcPage}"/>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
	
	/**
	 * <pre>
	 *   사용자권환 및 회원정보 수정
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 07. 07
	 */
	$('#btn_modify').click(function(){
		var chkUser = $('.chk_remove_user:checked').size();
	    if(chkUser == 0){
		    alert('수정할 회원을 선택해 주세요.');
	    } else if(chkUser > 1){
		    alert('수정을 원하시는 한명의  회원을 선택해 주세요.');
	    } else if(chkUser == 1){
	    	var mbrId = $('input:checkbox:checked.chk_remove_user').map(function () {
    		  return this.value;
    		}).get(); 
		    window.open('${HOME}/config/modifyUserPopup.do?mbrId='+mbrId, "popup", "width=500, height=530, resizable=no, scrollbars=no, menubar=no, toobar=no, status=no, location=no, ");
	    }
	});
	 
	 /**
	 * <pre>
	 *   사용자권환 및 회원정보 삭제
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 07. 07
	 */
   $('#btn_del').click(function(){
	   if(!$('.chk_remove_user:checked').size()){
		   alert('삭제할 회원을 선택해주세요.');
	   } else {
		   // [1] 삭제할 경우 삭제할 유저가 관리자일 경우 check 
		   // [1-1] 관리자가 1개 있으나 관리자를 제외한 나머지 유저가 없는 경우 : 삭제 
		   // [1-2] 관리자가 1개 있으나 관리자를 제외한 나머지 유저가 있는 경우 : 삭제 불가능
		   // [1-3] 관리자가 n개 있는 경우 : 삭제 
		   
		   //TODO
		   // 해당 아이디가 관리자YN, 선택한 아이디를 제외한 관리자의 갯수, 관리자가 아닌 유저가 몇명 존재하는지  받아옴
		   
		   
	var deleteUserList = $('.chk_remove_user:checked').map(function(){return $(this).val();}).get().join(',');	   
		   
	$paramObj = {
		 mbrIdList : deleteUserList,
		 compUsrId : $("#com_usr_id").val()
  	 }; 
   	 $param = $.param($paramObj);
		   
		   $.ajax({
				url: "${HOME}/config/checkAdminDeleteYN.do",
				type: "POST",
				data: $param,
				dataType: "json",
				async: true,
				error:function(result){
		 		},
				success: function(result){

					var chk_y = result.CHK_Y;
					var chk_n = result.CHK_N;

					// n = 0 y = 0 일반인 유저도 없고 관리자가 1~n개 있으면 삭제가능 
					// n > 0 y = 0 일반인 유저는 있는데 관리자가 없음 >> 삭제불가능 
					// n = 0 y > 0 일반인 유저는 없고 관리자만 존재함 >> 삭제가능
					// n > 0 y > 0 일반인 유저도 있고 관리자도 있음 >> 삭제가능
					if(chk_n >= 0 && chk_y == 0){
						alert("관리자는 최소 1명 이상 존재해야 합니다.");
					}else{
						$.fn.removeUser(deleteUserList);
					}
				}
			}); 	   
	   }
   }); 
	 
	
});

/**
 * <pre>
 *   사용자권환 및 회원정보 삭제 FORM 전송 함수
 * </pre>
 * @author KIM GA EUN
 * @since 2015. 07. 07
 * @param mbrIdList : 회원 순번 리스트 문자열(구분자 : ,)
 */
$.fn.removeUser = function(mbrIdList) {
	 if(!confirm('선택하신 회원 정보를 \n정말 삭제하시겠습니까?')) return false; 
	 $('#mbr_id_list').val(mbrIdList);
	 $('form').attr({'action':'${HOME}/config/removeUser.do', 'method':'POST'}).submit();
	 
	 if ( mbrIdList.indexOf($("#mbrId").val()) != -1 ) {
			location.href="${HOME}/login/logout.do";
	 }
	 
};

function addUser(){
	window.open('${HOME}/config/addUserPopup.do', "popup", "width=500, height=550, resizable=no, scrollbars=no, menubar=no, toobar=no, status=no, location=no, ");
}


function getNewRequestList(){
	location.href=('${HOME}/config/getUserInfoList.do');
}

</script>
</head>
<body>
<div id="Wrapper">
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0301"/>
</jsp:include>
<%-- Top Area End --%>



<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0301"/>
</jsp:include>
<!-- Left Area End -->
	
	<!-- rightWrap -->	
	<div id="rightWrap">
				<div class="right_tit ltit03_0101"><em>사용자권한 및 회원정보</em></div>
				
				<div id="content_tit_pr_di1">
					<div class="content_tit pru40">사용자권한 및 회원정보 관리 </div>					
					<div id="btnWrap_duo">
						<div class="btn_bluebtn_01">
						<a href="#none" class="on" id="btn_aduser" onclick="javascript:addUser();">사용자 추가</a>
						</div>
						<!-- <div class="btn_rdgbtn_01"><a href="#none" class="on" id="btn_sbillidmap" style="height:20px;">스마트빌 아이디매핑</a></div> -->
					</div>
				</div>
				
				<form id="userListFrm" name="userListFrm">
				<%-- FORM HIDDEN AREA START --%>
				<input type="hidden" name="mbrIdList" id="mbr_id_list" value="${sBox.mbrIdList}" />
				<input type="hidden" id="target" value="${HOME}/config/getUserInfoList.do">
				<input type="hidden" name="num" id="num" value="${sBox.num}" />
				<input type="hidden" name="comUsrId" id="com_usr_id" value="${sBox.sessionCompUsrId}"/>
				<input type="hidden" name="mbrId" id="mbrId" value="${sBox.sessionMbrId}"/>
				<%-- FORM HIDDEN AREA END --%>
				</form>
				
				<div class="tbl04Wrap">
					<table>
					<tbody>
						<tr>
							<th width="5%" class="first_thlwr4">선택</th>
							<th width="10%">ID</th>
							<th width="8%">이름</th>
							<th width="8%">사번</th>
							<th width="14%">전화번호</th>
							<th width="22%">이메일</th>
							<th width="13%">스마트채권<br />Web ID</th>
							<th width="20%">권한</th>
						</tr>
						<c:forEach var="userList" items="${userList}">
						<tr>
							<td class="first_tdlwr4">
								<input type="checkbox" class="choice chk_remove_user" title="선택" value="${userList.MBR_ID}" id="mbrId">
							</td>
							<td style="text-align:left; padding-left:5px;">${userList.LOGIN_ID}</td>
							<td>${userList.USR_NM}</td>
							<td>${userList.COMP_PERSONAL_ID}</td>
							<td>${userList.TEL_NO}</td>
							<td style="text-align:left; padding-left:5px;">${userList.EMAIL}</td>
							<td>${userList.SB_LOGIN_ID}</td>
							<td class="padding" style="text-align:left; padding-left:5px;">
							<c:if test="${userList.ADM_YN eq 'Y'}">
								관리자<br>
							</c:if>
							<c:set var="GRN" value="${fn:split(userList.GRN,'|')}" />
							<c:forEach var="grnList" items="${GRN}">
								<c:if test="${grnList eq 'CRED1C'}">
									채무불이행 작성/신청 권한<br>
								</c:if>
								<c:if test="${grnList eq 'SBDE1R'}">
									기업정보검색 권한<br>
								</c:if>
								<c:if test="${grnList eq 'SBDE2R'}">
									기업정보브리핑 권한<br>
								</c:if>
								<c:if test="${grnList eq 'SBDE3R'}">
									조기경보상세 권한<br>
								</c:if>
								<c:if test="${grnList eq 'SBDE4R'}">
									조기경보등록 업체 권한<br>
								</c:if>
							</c:forEach>
							</td>
						</tr>
						</c:forEach>
					</tbody>
					</table>
				</div>
				
				<div id="tbl_numbtnWrap">
					<table class="tbl_numbtn">
					<tbody>
						<tr>
							<td class="left">
								<div id="btnWrap_l">
									<div class="btn_whitebtn_01"><a href="#none" class="on" id="btn_modify" onclick="javascript:modifyUser();">수정</a></div>
								</div>
								<div class="btn_graybtn_01"><a href="#none" class="on" id="btn_del">삭제</a></div>								
							</td>
							<!-- <td class="right">
								<div id="btnWrap_l">
									<div class="btn_graybtn_01"><a href="#none" class="on" id="btn_modify">저장</a></div>
								</div>
								<div class="btn_whitebtn_01"><a href="#none" class="on" id="btn_del">인쇄</a></div>
							</td> -->
						</tr>
					</tbody>
					</table>
					<div class="page_num" id="getPageView">
						<c:out value="${pcPage}" escapeXml="false" />
					</div>
				</div>
			</div>
		
		</div>
	<!-- rightWrap -->
	


		  
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>
</div> <!-- containerWrap -->

</body>
</html>