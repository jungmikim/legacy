<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--// footer //-->
	<!-- // 푸터 시작 //-->
		<div id="footerWrap">
			<div class="footer">
				<div class="address">
				서울특별시 성동구 광나루로 172 린하우스 3층
				<span class="footline">|</span>
				<span class="bold">(주)비즈니스온커뮤니케이션</span>
				<span class="footline">|</span>
				대표자 : 박혜린
				<span class="footline">|</span>
				<span class="bold">고객센터 1588-8064</span>
				<span class="footline">|</span>
				통신판매업 : 강남-16190
				<span class="footline">|</span>
				사업자등록번호 220-87-58882
				<span class="footline">|</span>
				<br>
				개인정보관리 : 오동균 (<a href="mailto:dkoh@businesson.co.kr">dkoh@businesson.co.kr</a>)
				<span class="footline">|</span>
				<div class="copyright">Copyright(C)2009 (주)비즈니스온커뮤니케이션.All rights reserved.</div>				
			</div>
		</div>
	</div>
	<!-- // 푸터 끝 //-->
<!--// footer //-->