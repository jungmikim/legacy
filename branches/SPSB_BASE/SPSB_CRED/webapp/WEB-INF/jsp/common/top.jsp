<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
$(document).ready(function() {
	$('.gnb > li').hover(function(e) {
		$('.gnb > li').removeClass('on');
		$('.gnb > li > ul').removeClass('on');
		$(this).addClass('on');
		$(this).stop(true,true).children('ul').addClass('on');

	},function(){
		$('.gnb > li').removeClass('on');
		$('.gnb > li > ul').removeClass('on');
	});
		
	$('.gnb > li').bind('mouseenter, focusin',function(e) {
		$('.gnb > li').removeClass('on');
		$('.gnb > li > ul').removeClass('on');	
		$(this).addClass('on');
		$(this).children('ul').addClass('on');	
				
	});	
		
	$('.gnb > li').bind('mouseleave, focusout',function(e) {
		$(this).removeClass('on');
		$(this).children('ul').removeClass('on');	
	});

	$('.gnb > li > ul > li > a').hover(function(e) {
		$('.gnb > li > ul > li > a').removeClass('on');
		$(this).addClass('on');	

	},function(){
		$('.gnb > li > ul > li > a').removeClass('on');
	});

	$('.gnb > li > ul > li > a').bind('mouseenter, focusin',function(e) {
		$('.gnb > li > ul > li > a').removeClass('on');
		$(this).addClass('on');		
				
	});	
		
	$('.gnb > li > ul > li > a').bind('mouseleave, focusout',function(e) {
		$(this).removeClass('on');	
	});
	//alert('${sBox.isSessionCustSearchGrnt}');
	
});


</script>
<!-- header -->
<!-- // 헤더 시작 //-->	
<div id="headerWrap">
	<div class="headerCon">
			<div class="headerTop">
				<div class="hT_menu">
					<ul>
						<c:choose>
						<c:when test="${not empty sessionScope.sbDebnSessions}">
							<li><a href="#none" font-size="8pt">${sessionScope.sessionLoginName}님이 로그인하셨습니다.</a></li>
							<li><a href="${HOME}/mypage/myPageMain.do">마이 페이지</a></li>
							<li><a href="${HOME}/login/logout.do">로그아웃</a></li>
						</c:when>
						<%-- <c:when test="${not empty sessionScope.sbDebnSessionsForGuest}">
							<li><a href="#none">${sessionScope.sessionLoginName }님이 로그인하셨습니다.</a></li>
							<li><a href="${sBox.smartBillDomain}"><img src="${IMG}/header/go_smartbill.gif" /></a></li>
							<li><a href="${HOME }/login/logout.do">로그아웃</a></li>
						</c:when> --%>
						<c:otherwise>
							<%-- <li><a href="${sBox.smartBillDomain}"><img src="${IMG}/header/go_smartbill.gif" /></a></li> --%>
							<li><a href="${HOME }/login/loginForm.do">로그인</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
				</div>
			</div>
			<div class="headerGnb">
					<a href="${HOME}/mypage/myPageMain.do"><h1><em>스마트비즈 for Legacy</em></h1></a>
					<c:choose>
						<c:when test="${fn:substring(param.mgnb, 0, 2) eq '00'}">
						<div class="hG_menu">
							<ul>
								<li><a href="${HOME}/customer/getCustomerList.do" class="mgnb off">거래처관리</a></li>
								
								<c:if test="${sBox.isSessionDebtGrnt eq 'true'}">
									<li><a href="${HOME}/debt/getDebtInquiryList.do" class="mgnb off">채무불이행관리</a></li>
								</c:if>								
								<c:if test="${sBox.sessionAdmYn eq 'Y'}">
									<li><a href="${HOME}/config/getUserInfoList.do" class="mgnb off">환경설정</a></li>
								</c:if>
							</ul>
						</div>
						</c:when>
						<c:otherwise>
						<div class="hG_menu">
							<ul>
								<li><a href="${HOME}/customer/getCustomerList.do" class="mgnb ${fn:substring(param.mgnb, 0, 2) eq '01' ? 'on':'off'}">거래처관리</a></li>
								<c:if test="${sBox.isSessionDebtGrnt eq 'true'}">
									<li><a href="${HOME}/debt/getDebtInquiryList.do" class="mgnb ${fn:substring(param.mgnb, 0, 2) eq '02' ? 'on':'off'}">채무불이행관리</a></li>
								</c:if>
								<c:if test="${sBox.sessionAdmYn eq 'Y'}">
									<li><a href="${HOME}/config/getUserInfoList.do" class="mgnb ${fn:substring(param.mgnb, 0, 2) eq '03' ? 'on':'off'}">환경설정</a></li>
								</c:if>
							</ul>
						</div>
						</c:otherwise>
					</c:choose>
			</div>	
	</div>
</div>
<!-- // 헤더 끝 //-->
			

