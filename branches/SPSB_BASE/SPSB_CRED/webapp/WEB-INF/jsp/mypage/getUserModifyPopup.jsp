<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<c:set var="phoneCodeList" value="${result.commonCodeBox.phoneCodeList}" />
<c:set var="cellPhoneCodeList" value="${result.commonCodeBox.cellPhoneCodeList}" />
<c:set var="emailCodeList" value="${result.commonCodeBox.emailCodeList}" />
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 

<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;

$(document).ready(function(){
	setInfo();
	
	// Email Combobox Action : 기업
	 $('#emailDept3').change(function(){
		$('#emailDept2').val(this.value);	
	 });
});

//로그인 사용자 정보 표시
function setInfo(){
	
	//주민 등록번호
	var usrNo="${result.resultSbox.USR_NO}";
	var usrNo1 = usrNo.substring(0,6); 
	var usrNo2 = usrNo.substring(6,12); 
	$('#usrNo1').val(usrNo1);
	$('#usrNo2').val(usrNo2);
	
	//우편번호
	var postNo="${result.resultSbox.POST_NO}"; 
	var postNo1 = postNo.substring(0,3);
	var postNo2 = postNo.substring(3,6);
	$('#post_no1').val(postNo1);
	$('#post_no2').val(postNo2);

	//회사의 나의정보
	//전화번호
	var telnoDept="${result.resultDeptSbox.TEL_NO}"; 
	var telnosDept = telnoDept.split("-");
	$('#telNoDept1').val(telnosDept[0]);
	$('#telNoDept2').val(telnosDept[1]);
	$('#telNoDept3').val(telnosDept[2]);
	
	//팩스번호
	var faxNo="${result.resultDeptSbox.FAX_NO}"; 
	var faxNos = faxNo.split("-");
	$('#faxNo1').val(faxNos[0]);
	$('#faxNo2').val(faxNos[1]);
	$('#faxNo3').val(faxNos[2]);
	
	//휴대전화번호
	var cellnoDept="${result.resultDeptSbox.MB_NO}"; 
	var cellnosDept = cellnoDept.split("-");
	$('#cellNoDept1').val(cellnosDept[0]);
	$('#cellNoDept2').val(cellnosDept[1]);
	$('#cellNoDept3').val(cellnosDept[2]);
	
	//이메일
	var emailDept= "${result.resultDeptSbox.EMAIL}";
	$('#emailTypeDept01').val(emailDept);
	var emailsDept = emailDept.split("@");
	$('#emailDept1').val(emailsDept[0]);
	$('#emailDept2').val(emailsDept[1]);
	
	
	var loginId= "${result.resultSbox.LOGIN_ID}";
	$('#loginId').val(loginId);
	
}

 function fnValidate(){
	 
	if( $.trim($("#usrNm").val()).length == 0 ) {
			alert("이름을 입력해주세요.");
			$("#usrNm").focus();
			return false;
	}
	 
	if( $.trim($("#psw0").val()).length == 0 ) {
			alert("비밀번호를  입력해주세요.");
			$("#psw0").focus();
			return false;
	}
	 
	//이전 비밀번호와 입력한 비밀번호가 동일한지 확인
	if( $.trim($("#psw0").val()) != $.trim($("#loginPw").val()) ) {
		alert("비밀번호가 일치하지 않습니다.");
		$("#psw0").focus();
		return false;
	} 
	
		
	if( ($.trim($("#emailDept1").val()).length == 0 || $.trim($("#emailDept2").val()).length == 0) 
			&& ($.trim($("#emailTypeDept01").val()).length == 0 )) {
		alert("이메일을 입력해주세요.");
		return false;
	}
	var emailAddr = $.trim($("#emailTypeDept01").val());
	if( $.trim($("#emailDept1").val()).length > 0 && $.trim($("#emailDept2").val()).length > 0){
		emailAddr = $.trim($("#emailDept1").val()) + "@" + $.trim($("#emailDept2").val());
	}
	if( !typeCheck('emailCheck', emailAddr) ) {
		alert('이메일을 형식을 확인해 주세요.');
		return false;
	}
	$("#emailTypeDept01").val(emailAddr);
	
	if( $.trim($("#telNoDept2").val()).length == 0 || $.trim($("#telNoDept3").val()).length == 0 ) {
		alert("전화번호를 입력해주세요.");
		$("#telNoDept2").focus();
		return false;
	}
	if(!typeCheck('numCheck', $.trim($("#telNoDept2").val()) + $.trim($("#telNoDept3").val()))){
		alert("전화번호는 숫자만 입력 가능합니다.");
		return false;
	}
	
	if( $.trim($("#cellNoDept2").val()).length < 3 || $.trim($("#cellNoDept3").val()).length < 4 ) {
		alert("휴대전화를 입력해주세요.");
		$("#cellNoDept2").focus();
		return false;
	}
	if(!typeCheck('numCheck', $.trim($("#cellNoDept2").val()) + $.trim($("#cellNoDept2").val()))){
		alert("전화번호는 숫자만 입력 가능합니다.");
		return false;
	}
	
	if( $.trim($("#deptNm").val()).length == 0 ) {
		alert("부서명을 입력해주세요.");
		$("#deptNm").focus();
		return false;
}
	
	return true;
}
 
function setUserInfo(){
	
	if(!fnValidate()) {
		return false;
	}
	if(confirm("정보를 수정하시겠습니까?")){
		$.ajax({
		     url: "${HOME}/mypage/modifyUserModify.do",
		     type: "POST",
		     data:$("#updateUserFrm").serialize(),
		     dataType: "json",
		     async: true,
		     error:function(result){
		       },
		     success: function(result){
		      opener.$.fn.modifyUserInfo($('#usrNm').val() , $('#deptNm').val(), $('#jobTlNm').val(), $('#telNoDept1').val(), $('#telNoDept2').val(), $('#telNoDept3').val(), $('#cellNoDept1').val(), $('#cellNoDept2').val(), $('#cellNoDept3').val(), $('#faxNo1').val(),  $('#faxNo2').val(), $('#faxNo3').val(), $('#emailDept1').val(), $('#emailDept2').val(), $('#emailDept3').val());
		      window.close();
		     }
		    }); 
	}
} 

function setCancel(){
	 if(confirm('회원 정보 수정이 취소 됩니다.')){
		 window.close();
	 }else{
		 //진행중이던 사업자 정보 수정화면
	 }
	
}
	
//우편번호 검색 팝업창
function open_post(){ 
	open_win('${HOME}/zip/zipSearchPopup.do');
} 
 
function getUserInfo(){
	location.href='${HOME}/mypage/getUserInfo.do';
}
 

</script>

</head>
<body>
	<div class="contentArea sub06"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<input type="hidden" id="loginPw" name="loginPw" class="txtInput" value="${result.resultSbox.LOGIN_PW}" /> 
		<form name="updateUserFrm" id="updateUserFrm">
		<div class="infoInputWrap">
			<h3 class="first2 titB"><img src="${IMG}/common/h3_mypage_per06.gif" alt="개인 정보 수정" /></h3>
					<div class="infoInput">
						<dl class="comMemInput">
						
						<dt class="id"><span><label for="id">이름</label><em>*</em></span></dt>
						<dd class="id"><span>
							<input type="text" id="usrNm" name="usrNm" class="txtInput" value="${result.resultDeptSbox.USR_NM}" /> 
						</span></dd>
						<dt class="id"><span><label for="id">아이디</label></span></dt>
						<dd class="id"><span>
							${result.resultSbox.LOGIN_ID}
						</span></dd>
						
						<dt class="pwd"><span><label for="psw0">비밀번호</label> <em>*</em></span></dt>
						<dd class="pwd"><span>
							<input type="password" id="psw0"  name="psw0" class="txtInput" />
						</span></dd>
						
						<dt class="deptNm"><span><label for="deptNm">부서명</label> <em>*</em></span></dt>
						<dd class="deptNm"><span>
							<input type="text" id="deptNm"  name="deptNm" class="txtInput" value="${result.resultDeptSbox.DEPT_NM}"/>
						</span></dd>
						
						<dt class="deptNm"><span><label for="jobTlNm">직위</label></span></dt>
						<dd class="deptNm"><span>
							<input type="text" id="jobTlNm"  name="jobTlNm" class="txtInput" value="${result.resultDeptSbox.JOB_TL_NM}"/>
						</span></dd>
						
						<dt class="address"><span>주소</span></dt>
						<dd class="address"><span>
							<input type="text" class="txtInput wsmall" name="postNo1" id="post_no1" title="우편번호 앞자리" /> -
							<input type="text" class="txtInput wsmall" name="postNo2" id="post_no2" title="우편번호 뒷자리" /> 
							<a href="#"><img src="${IMG}/common/btn_s_zipcode02.gif" alt="우편번호" class="imgBtn" onClick="javascript:open_post()"/></a></span>
							<span><input type="text" name="custAddr1" id="cust_addr_1" class="txtInput address1" title="주소" value="${result.resultSbox.ADDR_1}"/></span>
							<span><input type="text" name="custAddr2" id="cust_addr_2" class="txtInput address2" title="상세주소" value="${result.resultSbox.ADDR_2}"/> 
						</span></dd>
							
						<dt class="mail"><span>이메일 <em>*</em></span></dt>
						<dd class="mail"><span>
							<input type="text" id="emailTypeDept01" name="emailTypeDept01" class="txtInput" title="이메일" /> 
							<span id="emailType02">
								<input type="text" id="emailDept1" name="emailDept1" class="txtInput" title="이메일 주소" />
								@ <input type="text"  id="emailDept2" name="emailDept2" class="txtInput" title="이메일 도메인" /> 
								<select id="emailDept3" name="emailDept3" title="이메일 도메인 선택">
									<c:forEach var="companyEmailCodeList" items="${emailCodeList}">
										<option value="${companyEmailCodeList}" <c:if test="${companyEmailCodeList eq EMAIL[1]}">selected</c:if>>
											<c:choose>
												<c:when test="${not empty companyEmailCodeList}">${companyEmailCodeList}</c:when>
												<c:otherwise>직접입력</c:otherwise>
											</c:choose>
										</option>
									</c:forEach>
								</select>
							</span>
						</span></dd>
						
						<dt class="phone"><span>전화번호<em>*</em></span></dt>
						<dd class="phone"><span>
							<select id="telNoDept1" name="telNoDept1" title="전화번호 국번">
								<c:forEach var="companyPhoneCodeList" items="${phoneCodeList}">
									<option value="${companyPhoneCodeList}">${companyPhoneCodeList}</option>
								</c:forEach>
							</select> -  
							<input type="text" id="telNoDept2" name="telNoDept2" class="txtInput wsmall" title="전화번호 중간자리" maxlength="4" /> -
							<input type="text" id="telNoDept3" name="telNoDept3" class="txtInput wsmall" title="전화번호 뒷자리"  maxlength="4" />
						</span></dd>
						
						<dt class="phone"><span>팩스번호 </span></dt>
						<dd class="phone"><span>
							<select id="faxNo1" name="faxNo1" title="팩스번호 국번">
								<c:forEach var="companyPhoneCodeList" items="${phoneCodeList}">
									<option value="${companyPhoneCodeList}">${companyPhoneCodeList}</option>
								</c:forEach>
							</select> -  
							<input type="text" id="faxNo2" name="faxNo2" class="txtInput wsmall" title="팩스번호 중간자리" maxlength="4" /> -
							<input type="text" id="faxNo3" name="faxNo3" class="txtInput wsmall" title="팩스번호 뒷자리"  maxlength="4" />
						</span></dd>
						
						<dt class="phone"><span><label for="cellNoDept1">휴대폰번호</label> <em>*</em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="cellNoDept1" name="cellNoDept1" title="휴대폰번호 국번">
								<c:forEach var="personalCellCodeList" items="${cellPhoneCodeList}">
									<option value="${personalCellCodeList}">${personalCellCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="cellNoDept2" name="cellNoDept2" title="휴대폰번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="cellNoDept3" name="cellNoDept3" title="휴대폰번호 뒷자리" maxlength="4"/> 
						</span></dd>
									
						</dl>
					</div>	
			</div>
		</form>
		<div class="btnWrap btnMember">
			<p><em>*</em> 항목은 필수 입력 사항입니다.</p>
			<div class="btnMiddle">
				<a href="#" class="on" onclick="javascript:setUserInfo()">저장</a>
				<a href="#" onclick="javascript:setCancel()">취소</a>
			</div>
		</div>
	</section>
	</div>
	<!-- //content -->
</body>
</html>