<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<%-- <c:if test="${sBox.custType eq 'C'}"><title>사업자등록번호 중복</title></c:if>
<c:if test="${sBox.custType eq 'P'}"><title>주민등록번호 중복</title></c:if> --%>
<link rel="stylesheet" href="${CSS}/popup2.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<style type="text/css">

</style>
<script type="text/javascript">

	$(document).ready(function() {
		erpAdm();
		
		$("#btn_ok").click(function(){
				$.ajax({
					url: "${HOME}/mypage/addGrantForm.do",
					type: "POST",
					data:$("#frm").serialize(),
					dataType: "json",
					async: true,
					error:function(result){
			 			//alert("fail");	
			 			},
					success: function(result){
						 $(opener.location).attr("href", "javascript:getNewRequestList(1)"); 
						window.close();
					}
				}); 
		});
		
		$("#btn_cancel").click(function(e){
			window.close();
		});
		
	});
	
	function erpAdm(){
		var erpYn= $('#erpYn').val();
		
		if(erpYn=="Y"){
			$('.employeeCheck').attr('checked', false);
			$('.erpCheck'+':not(:disabled)').attr('disabled', true);
		}else{
			$('#debnRegistorYn').attr('checked', true);
		}
	}
	
	function chgYnAll(){
		
		var isCheck = $('#admYn').is(":checked");
		var erpYn= $('#erpYn').val();
		
			if(erpYn=="N"){
				if(isCheck){
					$('.employeeCheck'+':not(:disabled)').attr('checked', true);
					$('.employeeCheck'+':not(:disabled)').attr('disabled', true);
				}else{
					$('.employeeCheck').attr('checked', false);
					$('.employeeCheck').attr('disabled', false);
				}
			}else{
				if(isCheck){
					$('.noErpCheck'+':not(:disabled)').attr('checked', true);
					$('.noErpCheck'+':not(:disabled)').attr('disabled', true);
				}else{
					$('.noErpCheck').attr('checked', false);
					$('.noErpCheck').attr('disabled', false);
				}
			}
	 }
	
	//채무불이행 신청서 작성은 채무불이행 신청 권한도 부여된다. 하지만 아닐수도 있으므로 disable처리는 안함
	function chgDefault(){
		var isCheck = $('#defaultApplyYn').is(":checked");
		
			if(isCheck){
				$('#defaultRegistorYn'+':not(:disabled)').attr('checked', true);
				
			}else{
				$('#defaultRegistorYn').attr('checked', false);
		}
	 }
	
	function chgRegister(){
		var isCheck = $('#venderRegistorYn').is(":checked");
			if(isCheck){
				$('#debnReadYn'+':not(:disabled)').attr('checked', true);
				$('#debnReadYn'+':not(:disabled)').attr('disabled', true);
				$('#debnReadYnDefault').val("B");
			}else{
				$('#debnReadYn').attr('checked', false);
				$('#debnReadYn').attr('disabled', false);
				$('#debnReadYnDefault').val("B");
			} 
	 }
</script>
</head>

<body>

	<div class="popupWrap">
	<h1><img src="${IMG}/common/h3_mypage_03newcall_01.gif" alt="신규 요청 권한설정" /></h1>

	<div class="popupNewcall">
	<p class="pop_txt01">해당 직원의 요청을 승인하시겠습니까?<br>
	그렇다면 권한을 설정해 주세요.</p>
	<form method="post" name="frm" id='frm'>
		<input type="hidden" id="erpYn" name="erpYn" value="${sBox.erpYn}" >
		<input type="hidden" id="mbrId" name="mbrId" value="${sBox.mbrId}" >
		<input type="hidden" id="mbrUsrId" name="mbrUsrId" value="${sBox.mbrUsrId}" >
		<input type="hidden" id="debnReadYnDefault" name="debnReadYnDefault" value="" >
		<table class="basicTable_Newcall2" summary="신규 요청 권한설정">
			<colgroup>
				<col width="5%" />
				<col width="8%" />
				<col width="10%" />
				<col width="7%" />
				<col width="10%" />
				<col width="10%" />
			</colgroup>
			<tr>
				<th>관리자</th>
				<th>거래처 등록</th>
				<th>전체 채권조회</th>
				<th>채권 등록</th>				
				<th>채무불이행<br>신청서 작성</th>				
				<th>채무불이행<br>신청</th>				
			</tr>
			<tr>
				<td><input type="checkbox" id="admYn" name="admYn" size="20" title="관리자" value="Y" onclick="javascript:chgYnAll()" ></td>
    			<td><input type="checkbox" class="employeeCheck erpCheck" id="venderRegistorYn" name="grnCdArray" size="20" title="거래처 등록" value="A" onclick="javascript:chgRegister()"></td>
    			<td><input type="checkbox" class="employeeCheck noErpCheck" id="debnReadYn" name="grnCdArray" size="20" title="전체 채권조회" value="B"></td>
    			<td><input type="checkbox" class="employeeCheck erpCheck" id="debnRegistorYn" name="grnCdDebn" size="20" title="채권등록" value="C"></td>
    			<td><input type="checkbox" class="employeeCheck erpCheck" id="defaultRegistorYn" name="grnCdArray" size="20" title="채무불이행 신청서 작성" value="D" ></td>
    			<td><input type="checkbox" class="employeeCheck noErpCheck" id="defaultApplyYn" name="grnCdArray" size="20" title="채무불이행 신청" value="E" onclick="javascript:chgDefault()"  ></td>
			</tr>
		</table>
	</form>	
	<p class="txt01">* 체크를 하지 않으시면 [권한 없음]으로 설정됩니다.</p>
	
	<div class="btnWrap02">
		<a id="btn_ok"><img src="${IMG}/popup/btn_ok02.gif" alt="확인" /></a>
        <a id="btn_cancel"><img src="${IMG}/popup/btn_cancel02.gif" alt="취소" /></a>
	</div>
		
	 	</div>
	 </div> 

</body>
</html> 