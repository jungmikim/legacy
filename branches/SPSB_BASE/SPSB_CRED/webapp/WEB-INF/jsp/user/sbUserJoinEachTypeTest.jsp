<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	<%--개인회원가입--%>
	$("#btnUserJoin").click(function(){
		$("#userJoinForm").attr({"action":"${HOME}/user/sbUserJoinTermsAgree.do"}).submit();
	});
});
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		<!-- <form id="userJoinForm" name="userJoinForm" method="post" >
			<input type="text" name="usrNo" value="1118105565"/>
			<input type="text" name="compUsrNm" value="(주)자산유리"/>
			<input type="text" name="signInfo" value=""/>
			<input type="text" name="postNo" value="150840"/>
			<input type="text" name="addr" value="서울특별시 영등포구 영등포로64길 26  "/>
			<input type="text" name="ownNm" value="이용현"/>
			<input type="text" name="bizType" value="제조업"/>
			<input type="text" name="bizCond" value="도매"/>
			<input type="text" name="usrNm" value="자산테스트1"/>
			<input type="text" name="loginId" value="jasan1"/>
			<input type="text" name="email" value="jasan1@js.com"/>
			<input type="text" name="telNo" value="070-2345-6550"/>
			<input type="text" name="mbNo" value="010-1111-4444"/>
		</form> -->
		<form id="userJoinForm" name="userJoinForm" method="post" >
			<table class="basicTable" style="width:650px">
	            <tr>
	                <td>사업자번호 </td>
	                <td>
	                	<input type="text" name="usrNo" value="1111111119"/>
	                </td>
	            </tr>
	            <tr>
	                <td>회사명 </td>
	                <td>
	                	<input type="text" name="compUsrNm" value="(주)공인인증테스트"/>
	                </td>
	            </tr>
	            <tr>
	                <td>직인정보</td>
	                <td>
	                	<input type="text" name="signInfo" value=""/>
	                </td>
	            </tr>
	            <tr>
	                <td>우편번호</td>
	                <td>
	                	<input type="text" name="postNo" value="150840"/>
	                </td>
	            </tr>
	            <tr>
	                <td>주소</td>
	                <td>
	                	<input type="text" name="addr" value="서울특별시 영등포구 영등포로64길 26"/>
	                </td>
	            </tr>
	            <tr>
	                <td>대표자이름</td>
	                <td>
	                	<input type="text" name="ownNm" value="공인이"/>
	                </td>
	            </tr>
	            <tr>
	                <td>업종</td>
	                <td>
	                	<input type="text" name="bizType" value="제조업"/>
	                </td>
	            </tr>
	            <tr>
	                <td>업태</td>
	                <td>
	                	<input type="text" name="bizCond" value="도매"/>
	                </td>
	            </tr>
	            <tr>
	                <td>회원이름</td>
	                <td>
	                	<input type="text" name="usrNm" value="공인_테스트3"/>
	                </td>
	            </tr>
	            <tr>
	                <td>로그인아이디</td>
	                <td>
	                	<input type="text" name="loginId" value="certTest3"/>
	                </td>
	            </tr>
	            <tr>
	                <td>이메일</td>
	                <td>
	                	<input type="text" name="email" value="certTest3@ct.com"/>
	                </td>
	            </tr>
	            <tr>
	                <td>전화번호</td>
	                <td>
	                	<input type="text" name="telNo" value="070-2345-6550"/>
	                </td>
	            </tr>
	            <tr>
	                <td>핸드폰</td>
	                <td>
	                	<input type="text" name="mbNo" value="010-1111-4444"/>
	                </td>
	            </tr>
	        </table>
		</form>
		<div class="joinTypeArea">
			<p class="box03"><span><a href="#"><img id="btnUserJoin" src="${IMG }/common/jointype_btn_03.gif" alt="개인회원가입" /></a></span></p>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>