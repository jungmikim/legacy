<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</head>
<body>
<!-- contentArea -->
<div class="contentArea">
	<!-- content -->
	<section id="content">
		<div class="default_txt01">
			<img src="${IMG}/common/sorry.gif" alt="죄송합니다." />
				<ul>
					<li> 죄송합니다.</li>
					<li>&nbsp;</li>
					<li>일시적인 장애로 스마트채권 연결에 실패했습니다.</li>
					<li>&nbsp;</li>
					<li>잠시후 다시 시도해주시기 바랍니다.</li>
					<li>&nbsp;<br/></li>
				</ul>
		</div>
	</section>
</div>
</body>
</html>