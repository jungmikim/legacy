<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	<%--약관, 개인정보수집 동의--%>
	$("#btnAgree").click(function(){
		var isTermsAgree01 = $("input[id='termsAgree01']").is(":checked");
		var isTermsAgree02 = $("input[id='termsAgree02']").is(":checked");
		if(isTermsAgree01 && isTermsAgree02){
			fnJoinNextStep();
		}else{
			alert("이용약관과 개인정보 제공에 동의해 주세요.");
			return false;
		}
	});
	
	<%--약관, 개인정보수집 동의 않함--%>
	$("#btnNotAgree").click(function(){
		alert("이용약관과 개인정보 제공에 동의해 주세요.");
		return false;
	});
});
	<%--약관동의 후 다음스텝으로 이동--%>
	function fnJoinNextStep(){
		var url = "${HOME}/user/sbUserJoinMemberUserForm.do";
		if("${joinType}" == "COMP1"){
			url = "${HOME}/user/sbUserJoinCompForm.do";
		}
		$("#userJoinForm").attr({"action":url}).submit();
	}
	
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<c:choose>
			<c:when test="${joinType eq 'COMP1' }">
				<h3 class="norm"><img src="${IMG }/common/h3_member_s01.gif" alt="기업회원가입" /></h3>
				<div class="stepbox">
					<ul>
						<li class="s1 on"><p><em>STEP 01. 약관동의</em></p></li>
						<li class="s2"><p><em>STEP 02. 정보입력 (다중 선택 가능)</em></p> <i></i></li>
						<li class="s3"><p><em>STEP 03. 기업담당자 등록</em></p> <i></i></li>
						<li class="s4"><p><em>STEP 04. 가입완료</em></p> <i></i></li>
					</ul>
				</div>
			</c:when>
			<c:otherwise>
				<h3 class="norm"><img src="${IMG }/common/h3_member_s03.gif" alt="개인회원가입" /></h3>
				<div class="stepbox2">
					<ul>
						<li class="s1 on"><p><em>STEP 01. 약관동의</em></p></li>
						<li class="s2"><p><em>STEP 02. 정보입력</em></p> <i></i></li>
						<li class="s3"><p><em>STEP 03. 가입완료</em></p> <i></i></li>
					</ul>
				</div>
			</c:otherwise>
		</c:choose>
		
		<form id="userJoinForm" name="userJoinForm" method="post" >
			<input type="hidden" name="compUsrId" value="${sbInfo.compUsrId }"/>
			<input type="hidden" name="usrNo" value="${sbInfo.usrNo }"/>
			<input type="hidden" name="compUsrNm" value="${sbInfo.compUsrNm }"/>
			<input type="hidden" name="signInfo" value="${sbInfo.signInfo }"/>
			<input type="hidden" name="postNo" value="${sbInfo.postNo }"/>
			<input type="hidden" name="addr" value="${sbInfo.addr }"/>
			<input type="hidden" name="ownNm" value="${sbInfo.ownNm }"/>
			<input type="hidden" name="bizType" value="${sbInfo.bizType }"/>
			<input type="hidden" name="bizCond" value="${sbInfo.bizCond }"/>
			<input type="hidden" name="usrNm" value="${sbInfo.usrNm }"/>
			<input type="hidden" name="loginId" value="${sbInfo.loginId }"/>
			<input type="hidden" name="email" value="${sbInfo.email }"/>
			<input type="hidden" name="telNo" value="${sbInfo.telNo }"/>
			<input type="hidden" name="mbNo" value="${sbInfo.mbNo }"/>
		</form>
		<h3 class="norm"><img src="${IMG }/common/h3_member_s11.gif" alt="약관동의" /></h3>
		<div class="policybox policyAgree">
			<ul class="list">
				<li><a href="#pAgree01" class="on">서비스 이용약관</a> <i>|</i></li>
				<li><a href="#pAgree02">채권관리 서비스 이용약관</a></li>
			</ul>
			<div class="pcont">
				<div class="policy" id="pAgree01">
					<div>
						<dl>
							<dt>제1조 [목적] 서비스 이용약관</dt>
							<dd>이 약관(이하 '본 약관'이라 합니다)은.... 내용이 들어갑니다.</dd>
						</dl>
						<dl>
							<dt>제2조 [약관의 적용 등]</dt>
							<dd>
								1. 서비스 이용에 관하여는 본 약관을 적용합니다.<br />
								2. 본 약관에 명시되지 않은 사항에 관하여는 전기통신사업법, 수표법, 여신전문금융업법, 통신비밀보호법, 정보통신망 
								이용촉진 및 정보보호 등에 관한 법률 등 관계법령 및 회사와 별도로 정한 개별 이용계약서(특약서 등)의 
								규정을 적용합니다. 
							</dd>
						</dl>
					</div>
				</div>

				<div class="policy" id="pAgree02">
					<div>
						<dl>
							<dt>제1조 [목적] 채권관리 서비스 이용약관</dt>
							<dd>이 약관(이하 '본 약관'이라 합니다)은.... 내용이 들어갑니다.</dd>
						</dl>
						<dl>
							<dt>제2조 [약관의 적용 등]</dt>
							<dd>
								1. 서비스 이용에 관하여는 본 약관을 적용합니다.<br />
								2. 본 약관에 명시되지 않은 사항에 관하여는 전기통신사업법, 수표법, 여신전문금융업법, 통신비밀보호법, 정보통신망 
								이용촉진 및 정보보호 등에 관한 법률 등 관계법령 및 회사와 별도로 정한 개별 이용계약서(특약서 등)의 
								규정을 적용합니다. 
							</dd>
						</dl>
						<dl>
							<dt>제3조 [약관의 적용 등]</dt>
							<dd>
								1. 서비스 이용에 관하여는 본 약관을 적용합니다.<br />
								2. 본 약관에 명시되지 않은 사항에 관하여는 전기통신사업법, 수표법, 여신전문금융업법, 통신비밀보호법, 정보통신망 
								이용촉진 및 정보보호 등에 관한 법률 등 관계법령 및 회사와 별도로 정한 개별 이용계약서(특약서 등)의 
								규정을 적용합니다. 
							</dd>
						</dl>
						<dl>
							<dt>제4조 [약관의 적용 등]</dt>
							<dd>
								1. 서비스 이용에 관하여는 본 약관을 적용합니다.<br />
								2. 본 약관에 명시되지 않은 사항에 관하여는 전기통신사업법, 수표법, 여신전문금융업법, 통신비밀보호법, 정보통신망 
								이용촉진 및 정보보호 등에 관한 법률 등 관계법령 및 회사와 별도로 정한 개별 이용계약서(특약서 등)의 
								규정을 적용합니다. 
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<div class="btnWrap btnMember">
			<p><input type="checkbox" id="termsAgree01" /> <label for="termsAgree01">이용약관에 동의합니다.</label></p>
		</div>
		
		<h3 class="norm"><img src="${IMG }/common/h3_member_s12.gif" alt="개인정보 수집 및 이용에 대한 안내" /></h3>
		<div class="policybox policyGuide">
			<ul class="list">
				<li><a href="#pGuide01" class="on">수집하는 개인정보의 항목</a> <i>|</i></li>
				<li><a href="#pGuide02">개인정보의 수집 및 이용 목적</a> <i>|</i></li>
				<li><a href="#pGuide03">개인정보의 보유 및 이용기간</a> <i>|</i></li>
				<li><a href="#pGuide04">정보의 제3자 제공</a></li>
			</ul>
			<div class="pcont">
				<div class="policy" id="pGuide01">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
						<p>
							위 '개인정보 수집 및 이용에 대한 안내'에 따른 
							활용과 '서비스 제공 및 정산, 고객상담 등의 
							원활한 업무처리'를 위하여 본인이 기재한 
							정보 중 아래의 정보를 제3자에게 제공하는 
							것에 동의합니다.
						</p>
					</div>
				</div>

				<div class="policy" id="pGuide02">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
					</div>
				</div>
				
				<div class="policy" id="pGuide03">
					<div>
						<p>
							위 '개인정보 수집 및 이용에 대한 안내'에 따른 
							활용과 '서비스 제공 및 정산, 고객상담 등의 
							원활한 업무처리'를 위하여 본인이 기재한 
							정보 중 아래의 정보를 제3자에게 제공하는 
							것에 동의합니다.
						</p>
					</div>
				</div>

				<div class="policy" id="pGuide04">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
					</div>
				</div>

			</div>
		</div>

		<div class="btnWrap btnMember">
			<p><input type="checkbox" id="termsAgree02" /> <label for="termsAgree02">개인정보 수집 및 이용에 동의합니다.</label></p>
			<div class="btnMiddle">
				<a href="#" class="on" id="btnAgree">동의 함</a>
				<a href="#" id="btnNotAgree">동의 안함</a>
			</div>
		</div>
	</section>

	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>