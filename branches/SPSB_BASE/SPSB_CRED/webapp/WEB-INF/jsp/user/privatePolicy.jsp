<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0706"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0706">개인정보 취급방침</h2>
			<p class="under_h2_sub0706">고객이 채권관리 서비스에 제공한 개인정보가 어떠한 용도와 방식으로 이용되고 있는지 안내해드립니다</p>
		</div>
		
		<div class="policybox2 policyAgree">
			<ul class="list">
				<li><a href="#pGuide01" class="on">수집하는 개인정보의 항목</a> <i>|</i></li>
				<li><a href="#pGuide02">개인정보의 수집 및 이용 목적</a> <i>|</i></li>
				<li><a href="#pGuide03">개인정보의 보유 및 이용기간</a> <i>|</i></li>
				<li><a href="#pGuide04">정보의 제3자 제공</a></li>
			</ul>
			<div class="pcont">
				<div class="policy" id="pGuide01">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
						<p>
							위 '개인정보 수집 및 이용에 대한 안내'에 따른 
							활용과 '서비스 제공 및 정산, 고객상담 등의 
							원활한 업무처리'를 위하여 본인이 기재한 
							정보 중 아래의 정보를 제3자에게 제공하는 
							것에 동의합니다.
						</p>
						<p>
							...
						</p>
					</div>
				</div>
				<div class="policy" id="pGuide02">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
						<p>
							...
						</p>
					</div>
				</div>
				
				<div class="policy" id="pGuide03">
					<div>
						<p>
							위 '개인정보 수집 및 이용에 대한 안내'에 따른 
							활용과 '서비스 제공 및 정산, 고객상담 등의 
							원활한 업무처리'를 위하여 본인이 기재한 
							정보 중 아래의 정보를 제3자에게 제공하는 
							것에 동의합니다.
						</p>
						<p>
							...
						</p>
					</div>
				</div>

				<div class="policy" id="pGuide04">
					<div>
						<p>
							귀사가 본인으로부터 취득한 개인(신용)정보는 
							개인정보 보호법 제15조, 제17조, 제24조 및 
							신용정보의 이용 및 보호에 관한 법률 제32조에 
							따라 타인에게 제공할 경우에는 본인의 사전 
							동의를 얻어야 하는 정보입니다.
						</p>
						<p>
							...
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>