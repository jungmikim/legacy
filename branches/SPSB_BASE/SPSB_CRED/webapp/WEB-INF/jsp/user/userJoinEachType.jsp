<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	<%--기업회원과 개인회원관리자 가입--%>
	$("#btnCompAndUserJoin").click(function(){
		location.href = "${HOME}/user/userJoinTermsAgree.do?joinType=COMP1";
	});
	<%--개인회원가입--%>
	$("#btnUserJoin").click(function(){
		location.href = "${HOME}/user/userJoinTermsAgree.do";
	});
	<%--게스트회원가입--%>
	$("#btnGuestJoin").click(function(){
		location.href = "${HOME}/user/guestUserJoin.do";
	});
});
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<div class="joinTypeArea">
			<section id="boxWrap2">
				<p class="box01"><span><a href="#"><img id="btnCompAndUserJoin" src="${IMG }/common/jointype_btn_01.gif" alt="기업회원가입1" /></a></span></p>
				<p class="box03"><span><a href="#"><img id="btnUserJoin" src="${IMG }/common/jointype_btn_03.gif" alt="개인회원가입" /></a></span></p>
				<p class="box04"><span><a href="#"><img id="btnGuestJoin" src="${IMG }/common/jointype_btn_04.gif" alt="체험계정받기" /></a></span></p>
			</section>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>