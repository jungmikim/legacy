package com.spsb.connector.wsc;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.bind.JAXBElement;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.dto.FileData;
import com.spsb.common.event.MessageEvent;
import com.spsb.common.exception.BizException;
import com.spsb.common.session.SessionManager;
import com.spsb.common.util.FileUtil;
import com.spsb.dao.config.ConfigDaoForMssql;
import com.spsb.dao.config.ConfigDaoForOracle;
import com.spsb.vo.MessageTagVo;
import com.spsb.ws.ArrayOfManifestItem;
import com.spsb.ws.ArrayOfReceiver;
import com.spsb.ws.BusinessScope;
import com.spsb.ws.DocumentIdentification;
import com.spsb.ws.IService;
import com.spsb.ws.Manifest;
import com.spsb.ws.ManifestItem;
import com.spsb.ws.ObjectFactory;
import com.spsb.ws.Receiver;
import com.spsb.ws.Result;
import com.spsb.ws.Sender;
import com.spsb.ws.StandardBusinessDocument;
import com.spsb.ws.StandardBusinessDocumentHeader;
import com.sun.istack.ByteArrayDataSource;

public class WSClient{ 

	
	@Autowired JaxWsProxyFactoryBean proxyFactory;
	
	@Autowired 	protected  ObjectFactory objFactory;
	
	@Autowired 	LoggingInInterceptor loggingInInterceptor;
	
	@Autowired  LoggingOutInterceptor loggingOutInterceptor;
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired  ConfigDaoForOracle configDaoForOracle;
	
	@Autowired  ConfigDaoForMssql configDaoForMssql;
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("#{service['WSDL_URL'].trim()}")
	private String wsdlUrl;
	
	//private String conURL ="http://192.168.100.146:8080/WSAdapter/services/SANTMessageService?wsdl";
	
	//private String conURL = wsdlUrl;

	//@BeforeClass
	//static public WSClient() {
	public WSClient() {
		proxyFactory = new JaxWsProxyFactoryBean();
		objFactory = new ObjectFactory();
		loggingInInterceptor = new LoggingInInterceptor();
		loggingOutInterceptor = new LoggingOutInterceptor();
	//	basicSyncClient = new BasicSyncClient();
	}
	
	public MessageTagVo remoteSyncMessageCall(MessageEvent msgEvent) throws IOException {
		
		String bizMessage = msgEvent.getDocumentData();
		logger.debug("=================" + bizMessage + "================");

		// input param fill. & remote call to Webservice
		StandardBusinessDocument bizDocument = null;
		StandardBusinessDocument response = null;

		// TODO :
		bizDocument = makeRequestBusinessDocument(bizMessage, msgEvent);
		
		Map<String,Object> props = new HashMap<String, Object>();
	    props.put("mtom-enabled", Boolean.TRUE);
	    proxyFactory.setProperties(props); 
		
		proxyFactory.setServiceClass(IService.class );
		//proxyFactory.setAddress("http://192.168.100.146:8090/WSAdapter/services/SANTMessageService?wsdl");
		proxyFactory.setAddress(wsdlUrl);
	
		IService serviceInterface = (IService) proxyFactory.create();
		       logger.debug("Remote Call " + proxyFactory.getAddress());
     
        Client client = ClientProxy.getClient(serviceInterface);
        if(true) {
	        
	        client.getInInterceptors().add(loggingInInterceptor);
	        client.getOutInterceptors().add(loggingOutInterceptor); 
        } 
        
        HTTPConduit httpConduit = (HTTPConduit)client.getConduit();
        HTTPClientPolicy policy = httpConduit.getClient();
        // set time to wait for response in milliseconds. zero means unlimited
	        policy.setReceiveTimeout(90000);
	        policy.setAllowChunking(false);
	        
	        response = serviceInterface.requestMessage(bizDocument); 
	        
	        String responseString = response.getMessage().getValue();
	        JAXBElement<String> serviceCode = response.getStandardBusinessDocumentHeader().getValue().getServiceCode();
	        JAXBElement<String> documentType = response.getStandardBusinessDocumentHeader().getValue().getDocumentIdentification().getValue().getType();
	        JAXBElement<String> instanceId = response.getStandardBusinessDocumentHeader().getValue().getDocumentIdentification().getValue().getInstanceID();
	        JAXBElement<String> reultTypeCode = response.getStandardBusinessDocumentHeader().getValue().getResult().getValue().getTypeCode();
	        
	        
	        MessageTagVo messageTagVo = new MessageTagVo();
	        messageTagVo.setXmlMessage(responseString);
	        messageTagVo.setServiceCode(serviceCode);
	        messageTagVo.setDocumentType(documentType);
	        messageTagVo.setInstanceId(instanceId);
	        messageTagVo.setResultCode(reultTypeCode);
	        
	       return messageTagVo;
	}
	
	
	
	 
	public void remoteASyncMessageCall(MessageEvent msgEvent) throws IOException {
		String bizMessage = msgEvent.getDocumentData();
		logger.debug("=================" + bizMessage + "================");

		// input param fill. & remote call to Webservice
		StandardBusinessDocument  bizDocument = null;

		// TODO :
		bizDocument = makeRequestBusinessDocument(bizMessage, msgEvent);
		
		Map<String,Object> props = new HashMap<String, Object>();
	    props.put("mtom-enabled", Boolean.TRUE);
	    proxyFactory.setProperties(props); 
		
		proxyFactory.setServiceClass(IService.class );
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!"+wsdlUrl);
		proxyFactory.setAddress(wsdlUrl);//http://192.168.100.212:8080/SPSB_CRED/ws/SANTMessageService
	
		IService serviceInterface = (IService) proxyFactory.create();
		
        logger.debug("Remote Call " + proxyFactory.getAddress());
        Client client = ClientProxy.getClient(serviceInterface);
        if(true) {
	        
	        client.getInInterceptors().add(loggingInInterceptor);
	        client.getOutInterceptors().add(loggingOutInterceptor); 
        } 
        
        HTTPConduit httpConduit = (HTTPConduit)client.getConduit();
        HTTPClientPolicy policy = httpConduit.getClient();
        // set time to wait for response in milliseconds. zero means unlimited
	        policy.setReceiveTimeout(90000);
	        policy.setAllowChunking(false);
	        serviceInterface.requestAsyncMessage(bizDocument); 
	        logger.debug("++++++++++++++++++++++");
	        logger.debug("Async message called");
	}
	

	public  StandardBusinessDocument makeRequestBusinessDocument(String message, MessageEvent msgEvent) throws IOException {
		
		System.out.println("makeRequestBusinessDocument");
		StandardBusinessDocument response = objFactory.createStandardBusinessDocument();
		
		// TODO:
		//encryptions if required.
		//boolean encrypted = false;
		
		JAXBElement<String> jaxbMessage = objFactory.createStandardBusinessDocumentMessage(message);
		response.setMessage(jaxbMessage);
		
		
		StandardBusinessDocumentHeader responseHeader = makeDocumentHeader(msgEvent);
		JAXBElement<StandardBusinessDocumentHeader> jaxbResponseHeader = objFactory.createStandardBusinessDocumentHeader(responseHeader);
		
		response.setStandardBusinessDocumentHeader(jaxbResponseHeader);
		
		//Session생성하여 현재 사용자의 정보를 가져온다
		SessionManager sessionManager = SessionManager.getInstance();
		SBox sBox = sessionManager.getCurrentSession();
		
		SBox ifmsghistSBox = new SBox();
		ifmsghistSBox.set("compMngCd",sBox.getString("sessionCompMngCd"));
		ifmsghistSBox.set("serviceCd", msgEvent.getServiceCode());
		ifmsghistSBox.set("docTypeCd", msgEvent.getDocumentType());
		ifmsghistSBox.set("typeCd", msgEvent.getTypeCode());
		ifmsghistSBox.set("instanceId", msgEvent.getInstanceId());
		ifmsghistSBox.set("referenceId", msgEvent.getReferenceId());
		ifmsghistSBox.set("groupId",msgEvent.getGroupId() );
		ifmsghistSBox.set("senderId", msgEvent.getSenderId());
		ifmsghistSBox.set("senderNm", msgEvent.getSenderName());
		ifmsghistSBox.set("receiverId", msgEvent.getReceiveId());
		ifmsghistSBox.set("receiverNm", msgEvent.getReceiveName());
		ifmsghistSBox.set("manifestNo", msgEvent.getFileCount());
		ifmsghistSBox.set("actionType", msgEvent.getActionType());
		ifmsghistSBox.set("xmlMsg", message);//message
		ifmsghistSBox.set("rcode", null);
		ifmsghistSBox.set("reason", null);
		ifmsghistSBox.set("xmlMsgRe",null);
		ifmsghistSBox.set("regId",sBox.getInt("sessionMbrId"));
		
		try {
			if("oracle".equals(dbmsType)){
				configDaoForOracle.insertIfMsgHist(ifmsghistSBox);
			}else{
				configDaoForMssql.insertIfMsgHist(ifmsghistSBox);
			}
			
		} catch (BizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//로컬 파일로 넣는 작업
		/*Calendar calendar = Calendar.getInstance();
		java.util.Date date = calendar.getTime();
		String todayStr = CommonUtil.getFormatDateTimeMiliSecond(date);
		String docType = msgEvent.getDocumentType();
		String fileName ="E:\\xmlMessage/"+todayStr+"_"+docType+".xml";
		File file = new File(fileName);
		FileOutputStream fos = new FileOutputStream(file);
		byte[] contentInBytes = message.getBytes();
		fos.write(contentInBytes);
		fos.flush();
		fos.close();
		System.out.println("Message wirted on textFile");
		*/
		
		return response;
	}

	
/**
 * com.spsb.ws.ObjectFactory 
 * @param msgEvent
 * @param objFactory
 * @return
 */
	public  StandardBusinessDocumentHeader makeDocumentHeader(MessageEvent msgEvent){//, com.spsb.ws.ObjectFactory  objFactory
		
		
		StandardBusinessDocumentHeader header = objFactory.createStandardBusinessDocumentHeader();

		JAXBElement<String> jaxbReceiveId = objFactory.createReceiverID(msgEvent.getReceiveId());
		JAXBElement<String> jaxbReceiveName = objFactory.createReceiverName(msgEvent.getReceiveName());
		
		// 송신자 영역
		Sender sender = objFactory.createSender();
		JAXBElement<String> jaxbId = objFactory.createSenderID(msgEvent.getSenderId());
		JAXBElement<String> jaxbSenderName = objFactory.createSenderName(msgEvent.getSenderName());
		sender.setID(jaxbId);
		sender.setName(jaxbSenderName);

		JAXBElement<Sender> jaxbSender = objFactory.createSender(sender);
		header.setSender(jaxbSender);
		
		
		
		if(msgEvent.getReceiveId() != null ) {
			Receiver receiver = objFactory.createReceiver();
			receiver.setID(jaxbReceiveId);
			receiver.setName(jaxbReceiveName);
			//TODO 수신자 서버 URL정보  
		}
		
		//수신자 정보 영역
		if(msgEvent.getReceiveId() != null ) {
			
			Receiver receiver = objFactory.createReceiver();
			receiver.setID(jaxbReceiveId);
			receiver.setName(jaxbReceiveName);
			
			ArrayOfReceiver receiversArray = objFactory.createArrayOfReceiver();
			List<Receiver> receiverList = receiversArray.getReceiver();
			receiverList.add(receiver);
			JAXBElement<ArrayOfReceiver> jaxbReceiversArray = objFactory.createArrayOfReceiver(receiversArray);
			header.setReceiver(jaxbReceiversArray);
		}
		
		
		// 문서 정보 영역
		DocumentIdentification docIdentity = objFactory.createDocumentIdentification();
		
		JAXBElement<String> resType = objFactory.createDocumentIdentificationResponseType(msgEvent.getResponseType()); // always 'S' SANTApplication.RESPONSE_PATTERN_TYPE_S
		docIdentity.setResponseType(resType);
		
		JAXBElement<String> jaxbInstanceId = objFactory.createDocumentIdentificationInstanceID(msgEvent.getInstanceId());
		docIdentity.setInstanceID(jaxbInstanceId);
		
		JAXBElement<String> refrenceID = objFactory.createDocumentIdentificationReferenceID(msgEvent.getReferenceId());
		docIdentity.setReferenceID(refrenceID);
		
		JAXBElement<String> jaxbGroupID = objFactory.createDocumentIdentificationGroupID(msgEvent.getGroupId());
		docIdentity.setGroupID(jaxbGroupID);
	
		JAXBElement<String> jaxbType = objFactory.createDocumentIdentificationType(msgEvent.getDocumentType());
		docIdentity.setType(jaxbType);
		
		JAXBElement<String> jaxbActionType = objFactory.createDocumentIdentificationActionType(msgEvent.getActionType());
		docIdentity.setActionType(jaxbActionType);
		
		JAXBElement<String> jaxbDateAndTime = objFactory.createDocumentIdentificationCreationDateAndTime(msgEvent.getDocCreationDateTime());
		docIdentity.setCreationDateAndTime(jaxbDateAndTime);
		
		JAXBElement<DocumentIdentification> jaxbDocIdentity = objFactory.createDocumentIdentification(docIdentity);
		header.setDocumentIdentification(jaxbDocIdentity);
		
		// 서비스코드
		JAXBElement<String> jaxbServiceCode = objFactory.createStandardBusinessDocumentHeaderServiceCode(msgEvent.getServiceCode());
		header.setServiceCode(jaxbServiceCode);
		
		// 유형코드
		//if(msgEvent.getTypeCode().length()>0){
			JAXBElement<String> jaxbTypeCode = objFactory.createResultTypeCode(msgEvent.getTypeCode());
			header.setTypeCode(jaxbTypeCode);	// always "REQ" cause it's client server
		/*}else{
			header.setTypeCode("REQ");	// always "REQ" cause it's client server
		}*/
		// files 처리 TODO 파일 처리시 List파일 넣어서 확인 필요
		if(msgEvent.getFileCount() > 0) {
			try {
				//List<FileData> fileDatas ;//= mapHelper.selectFileDataList(msgEvent.getMessageTagId());
				//TODO 파일 데이터 가져오기
		// 첨부파일 유무		
				header.setHasFile(true);
				
				Manifest manifest = objFactory.createManifest();
				ArrayOfManifestItem itemArray = objFactory.createArrayOfManifestItem();
				List<ManifestItem> itemList = itemArray.getManifestItem();
				
				for(FileData fileData : msgEvent.getFileDatas()) {
					ManifestItem item = objFactory.createManifestItem();
					
					//일련번호
					item.setSequenceNumber(objFactory.createManifestItemSequenceNumber(fileData.getFileSeq()));
					//실제 파일 크기
					item.setUniformResourceID(objFactory.createManifestItemUniformResourceID(fileData.getFileName()));
					//실제 파일 명
					item.setSize(objFactory.createManifestItemSize(String.valueOf(fileData.getFileSize())));
					//실제 파일의 바이너리 정보
					byte[] bytes = fileData.getFileData();
					DataSource dataSource = new ByteArrayDataSource(bytes, "application/binary");
					DataHandler binData = new DataHandler(dataSource); 
					item.setBinaryData(objFactory.createManifestItemBinaryData(binData));
					// 적하 항목 설명
					item.setDescription(objFactory.createManifestItemDescription(fileData.getFileDesc()));
					
				/*	Calendar calendar = Calendar.getInstance();
					java.util.Date date = calendar.getTime();
					String todayStr = CommonUtil.getFormatDateTimeMiliSecond(date);
				*/	 
					File wFile = new File(fileData.getFileName());
					try {
						FileUtil.write(wFile, fileData.getFileData());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//logger.error(e.getMessage());
						e.printStackTrace();
					}
					
					itemList.add(item);
				}
				manifest.setManifestItem(objFactory.createArrayOfManifestItem(itemArray));
				
				String itemSize = Integer.toString(itemList.size());
				// 항목수량
				manifest.setNumberOfItems(objFactory.createManifestNumberOfItems(itemSize));
				objFactory.createStandardBusinessDocumentHeaderManifest(manifest);
				
				header.setManifest(objFactory.createStandardBusinessDocumentHeaderManifest(manifest));
			}catch( Exception e) {
				//logger.error(e.getMessage());
			}
		} else {
			// 첨부파일 유무	
			header.setHasFile(false);
		}
		
		//set BusinessScope
		BusinessScope bizScope = objFactory.createBusinessScope();
		JAXBElement<BusinessScope> jaxbBizScope = objFactory.createBusinessScope(bizScope);
		header.setBusinessScope(jaxbBizScope);
		
		// Default : False;
		boolean encrypted = false;
		
		//logger.debug("Encrypted is "+ encrypted);
		if(encrypted){
			bizScope.setIsEncrypted(encrypted);
			//JAXBElement<String> algorithm = objFactory.createBusinessScopeEncryptionAlgorithm(this.algorithm);
			//bizScope.setEncryptionAlgorithm(algorithm);
			
		} else {
			bizScope.setIsEncrypted(encrypted);
		}
		
		//Set Result
		Result result = objFactory.createResult();
		JAXBElement<Result> jaxbResult = objFactory.createResult(result);
		header.setResult(jaxbResult);
		
		if (msgEvent.getResultCode() != null) {
			// result code 값이 없으면 Node를 만들지 앟음.
			
			JAXBElement<String> jaxbString = objFactory.createResultDescription(msgEvent.getResultMessage());
			//TODO ResultCode
			//JAXBElement<String> jaxbResultCode = objFactory.createResultTypeCode(msgEvent.getResultCode());
			//result.setTypeCode(jaxbResultCode);
			result.setDescription(jaxbString);
			
		}
		
		return header;
	}
	
	

	
	/*public void setObjFactory(ObjectFactory objFactory) {
		this.objFactory = objFactory;
	}

	public void setProxyFactory(JaxWsProxyFactoryBean proxyFactory) {
		this.proxyFactory = proxyFactory;
	}*/
	
}
