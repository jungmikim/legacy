package com.spsb.connector.wss;

import java.util.List;

import com.spsb.common.dto.MessageTag;
import com.spsb.ws.ArrayOfReceiver;
import com.spsb.ws.BusinessScope;
import com.spsb.ws.DocumentIdentification;
import com.spsb.ws.Manifest;
import com.spsb.ws.ManifestItem;
import com.spsb.ws.StandardBusinessDocument;
import com.spsb.ws.StandardBusinessDocumentHeader;


// TODO : common package로 옮기기
public class SANTMessageConvert {

	public MessageTag toMessageTag(StandardBusinessDocument bizDoc, SANTMessageInfos msgInfo) {
		MessageTag msgTag = new MessageTag();
		
		msgTag.setServiceCode(msgInfo.getServiceCode());
		msgTag.setInstanceId(msgInfo.getInstanceId());
		msgTag.setReferenceId(msgInfo.getReferenceId());
		msgTag.setDocumentType(msgInfo.getDocumenType());	//ex)MRQRGU
		msgTag.setResponseType(msgInfo.getResponseType());
		msgTag.setDocCreationDateTime(msgInfo.getDocCreationDateTime());
		
		// added 2114.06.06 - groupId
		msgTag.setGroupId(msgInfo.getGroupId());
		msgTag.setActionType(msgInfo.getActionType());
		
		msgTag.setSenderId(msgInfo.getSenderId());
		msgTag.setSenderName(msgInfo.getSenderName());
		msgTag.setReceiveId(msgInfo.getReceiveId());
		msgTag.setReceiveName(msgInfo.getReceiveName());
		
		msgTag.setFileCount(msgInfo.getFileCount());
		msgTag.setTotalSize(msgInfo.getTotalSize());
		
		msgTag.setMessageTypeCode(msgInfo.getMessageType());	//res or req
		String resMsg = bizDoc.getMessage().getValue();
		
		if(resMsg != null){
			// 복호화, if encrypted
			if(msgInfo.isEncrypted()){
				//try {
					//String plainDoc = decryptDocument(msgInfo, resMsg);
					//msgTag.setDocumentData(plainDoc);
				/*} catch (SANTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// decrypt error, send to public error response
				}*/
				
			}
			msgTag.setDocumentData(resMsg);
		}
		
		msgTag.setDirection("I");
		//msgTag.setMessageStatus(SANTApplication.MSG_STATUS_RECEIVED);
		return msgTag;
	}
/*	public MessageTag toMessageTag(StandardBusinessDocument bizDoc, SANTMessageInfos msgInfo) {
		MessageTag msgTag = new MessageTag();
		
		msgTag.setServiceCode(msgInfo.getServiceCode());
		msgTag.setInstanceId(msgInfo.getInstanceId());
		msgTag.setReferenceId(msgInfo.getReferenceId());
		msgTag.setDocumentType(msgInfo.getDocumenType());	//ex)MRQRGU
		msgTag.setResponseType(msgInfo.getResponseType());
		msgTag.setDocCreationDateTime(msgInfo.getDocCreationDateTime());
		
		// added 2114.06.06 - groupId
		msgTag.setGroupId(msgInfo.getGroupId());
		msgTag.setActionType(msgInfo.getActionType());
		
		msgTag.setSenderId(msgInfo.getSenderId());
		msgTag.setSenderName(msgInfo.getSenderName());
		msgTag.setReceiveId(msgInfo.getReceiveId());
		msgTag.setReceiveName(msgInfo.getReceiveName());
		
		msgTag.setFileCount(msgInfo.getFileCount());
		msgTag.setTotalSize(msgInfo.getTotalSize());
		
		msgTag.setMessageTypeCode(msgInfo.getMessageType());	//res or req
		String resMsg = bizDoc.getMessage().getValue();
		
		if(resMsg != null){
			// 복호화, if encrypted
			if(msgInfo.isEncrypted()){
				//try {
				//String plainDoc = decryptDocument(msgInfo, resMsg);
				//msgTag.setDocumentData(plainDoc);
				} catch (SANTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					// decrypt error, send to public error response
				}
				
			}
			msgTag.setDocumentData(resMsg);
		}
		
		msgTag.setDirection("I");
		//msgTag.setMessageStatus(SANTApplication.MSG_STATUS_RECEIVED);
		return msgTag;
	}
*/	
	/*public String decryptDocument(SANTMessageInfos headerInfo, String encDocument) throws SANTException{
		
		return null;
	}*/

	public MessageTag toMessageTag(StandardBusinessDocument response, String messageTagId) {
		MessageTag msgTag = new MessageTag(messageTagId);
				
		
		StandardBusinessDocumentHeader msgHeader = response.getStandardBusinessDocumentHeader().getValue();
		DocumentIdentification docIdentity = msgHeader.getDocumentIdentification().getValue();
//		DocumentIdentification docIdentity = msgHeader.getDocumentIdentification();
		
		BusinessScope bizParam = msgHeader.getBusinessScope().getValue();
		boolean encrypted = false;
		String encAlgorithm = null;
		String securityToken = null;
		
		if(bizParam != null){
			if(bizParam.isIsEncrypted()){
				encrypted = true;
				encAlgorithm = bizParam.getEncryptionAlgorithm().getValue();
				securityToken = bizParam.getSecurityToken().getValue();
			}
			else
				encrypted = false;
		}
		
		msgTag.setServiceCode( msgHeader.getServiceCode().getValue());
//		msgTag.setServiceCode( msgHeader.getServiceCode());
		
		msgTag.setInstanceId(docIdentity.getInstanceID().getValue());
//		msgTag.setInstanceId(docIdentity.getInstanceID());
		
		msgTag.setReferenceId(docIdentity.getReferenceID().getValue());
		msgTag.setDocumentType(docIdentity.getType().getValue());	//ex)MRQRGU
//		msgTag.setDocumentType(docIdentity.getType());	//ex)MRQRGU
		
		msgTag.setResponseType(docIdentity.getResponseType().getValue());	// S,A,P
		msgTag.setDocCreationDateTime(docIdentity.getCreationDateAndTime().getValue());
//		msgTag.setDocCreationDateTime(docIdentity.getCreationDateAndTime());
		
		// added 2114.06.06 - groupId
//		msgTag.setGroupId(docIdentity.getGroupID());
		msgTag.setGroupId(docIdentity.getGroupID().getValue());
				
//		
		
		msgTag.setSenderId(msgHeader.getSender().getValue().getID().getValue());
//		msgTag.setSenderId(msgHeader.getSender().getID());
		msgTag.setSenderName(msgHeader.getSender().getValue().getName().getValue());
//		msgTag.setSenderName(msgHeader.getSender().getName());
		
		ArrayOfReceiver receivers = msgHeader.getReceiver().getValue();
//		ArrayOfReceiver receivers = msgHeader.getReceiver();
		msgTag.setReceiveId(receivers.getReceiver().get(0).getID().getValue());
//		msgTag.setReceiveId(receivers.getReceiver().get(0).getID());
		msgTag.setReceiveName(receivers.getReceiver().get(0).getName().getValue());
//		msgTag.setReceiveName(receivers.getReceiver().get(0).getName());


		if(msgHeader.isHasFile() != null ){
			boolean hasFile = msgHeader.isHasFile().booleanValue();
			if( hasFile){
				Manifest manifest = msgHeader.getManifest().getValue();
				// 목록 valisation
				
				if(manifest != null){
					msgTag.setFileCount( Integer.parseInt(manifest.getNumberOfItems().getValue()));
//					msgTag.setFileCount( Integer.parseInt(manifest.getNumberOfItems()));
					long total = 0;
					
					List<ManifestItem> manifestItems = manifest.getManifestItem().getValue().getManifestItem();
//					List<ManifestItem> manifestItems = manifest.getManifestItem().getManifestItem();
					
					for(ManifestItem item : manifestItems){
						total += Integer.parseInt(item.getSize().getValue());
//						total += Integer.parseInt(item.getSize());
					}
					msgTag.setTotalSize(total);
				} 
			} 
		}
		msgTag.setMessageTypeCode(msgHeader.getTypeCode().getValue());	//res or req
//		msgTag.setMessageTypeCode(msgHeader.getTypeCode());	//res or req
		
		String resMsg = response.getMessage().getValue();
		
		if(resMsg != null){
			// 복호화, if encrypted
			if( encrypted){
				String plainDoc = decryptDocument(encAlgorithm, securityToken, resMsg);
				msgTag.setDocumentData(plainDoc);
				
			}
			msgTag.setDocumentData(resMsg);
		}
		
		msgTag.setDirection("I");
		//msgTag.setMessageStatus(SANTApplication.MSG_STATUS_RECEIVED);
		return msgTag;
	}

	private String decryptDocument(String encAlgorithm, String securityToken, String value) {
		// TODO Auto-generated method stub
		return null;
	}

}
