package com.spsb.service.zip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.parent.SuperService;
import com.spsb.dao.zip.ZipManageDaoForMssql;
import com.spsb.dao.zip.ZipManageDaoForOracle;

/**
 * <pre>
 * 우편번호 찾기 Service Implements Class
 * </pre>
 * @author KIM GA EUN
 * @since 2014. 1. 7.
 * @version 1.0
 */
public class ZipManageServiceImpl extends SuperService implements ZipManageService{
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private ZipManageDaoForOracle zipManageDaoForOracle;
	@Autowired
	private ZipManageDaoForMssql zipManageDaoForMssql;

	/**
	 * <pre>
	 * 	우편번호 팝업 시도 리스트 호출 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 *
	 * 	
	 * @return
	 */	
	@Override
	public SBoxList<SBox> getSidoList() {
		return "oracle".equals(dbmsType)? zipManageDaoForOracle.selectSidoForWebList() : zipManageDaoForMssql.selectSidoForWebList();
	}
	
	/**
	 * <pre>
	 * 	군/구 리스트 호출 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도
	 * 	
	 * @return
	 */
	@Override
	public SBoxList<SBox> getGunguForWebList(String sido) {
		return "oracle".equals(dbmsType)? zipManageDaoForOracle.selectGunguForWebList(sido):zipManageDaoForMssql.selectGunguForWebList(sido);
	}

	/**
	 * <pre>
	 * 	도로명 주소 + 건물번호 우편번호 검색 리스트 호출  Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, rdNm : 도로명 주소, mnNo : 건물번호 본번, subNo : 건물번호 부번
	 * 	
	 * @return
	 */
	@Override
	public SBoxList<SBox> getAddressByStreetSearchForWebList(SBox sBox) {
		return "oracle".equals(dbmsType)? zipManageDaoForOracle.selectAddressByStreetSearchForWebList(sBox):zipManageDaoForMssql.selectAddressByStreetSearchForWebList(sBox);
	}

	/**
	 * <pre>
	 * 	동(읍/면) + 지번 우편번호 검색 리스트 호출  Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, dong : 동(읍/면), mnNo : 지번 본번, subNo : 지번 부번
	 * 	
	 * @return
	 */
	@Override
	public SBoxList<SBox> getAddressByDongSearchForWebList(SBox sBox) {
		return "oracle".equals(dbmsType)? zipManageDaoForOracle.selectAddressByDongSearchForWebList(sBox) : zipManageDaoForMssql.selectAddressByDongSearchForWebList(sBox);
	}

	/**
	 * <pre>
	 * 	건물명(아파트명) 우편번호 검색 리스트 호출  Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, bldNm : 건물명(아파트명)
	 * 	
	 * @return
	 */
	@Override
	public SBoxList<SBox> getAddressByBuildingSearchForWebList(SBox sBox) {
		return "oracle".equals(dbmsType)? zipManageDaoForOracle.selectAddressByBuildingSearchForWebList(sBox) : zipManageDaoForMssql.selectAddressByBuildingSearchForWebList(sBox);
	}

}
