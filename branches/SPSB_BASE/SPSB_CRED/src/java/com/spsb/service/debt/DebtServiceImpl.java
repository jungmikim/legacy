package com.spsb.service.debt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.FileData;
import com.spsb.common.dto.MessageTag;
import com.spsb.common.enumtype.EDebtOtherStatType;
import com.spsb.common.enumtype.EDebtStatType;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.event.MessageEvent;
import com.spsb.common.exception.BizException;
import com.spsb.common.message.CommonMessageIF;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonCode;
import com.spsb.common.util.CommonExcel;
import com.spsb.common.util.CommonPage;
import com.spsb.common.util.CommonUtil;
import com.spsb.common.util.DateUtil;
import com.spsb.common.util.SysUtil;
import com.spsb.connector.wsc.WSClient;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.dao.debt.DebtDaoForMssql;
import com.spsb.dao.debt.DebtDaoForOracle;
import com.spsb.dao.mypage.MyPageUserModifyDaoForMssql;
import com.spsb.dao.mypage.MyPageUserModifyDaoForOracle;
import com.spsb.vo.DebtAmendRequestVo;
import com.spsb.vo.DebtRegisterRequestVo;
import com.spsb.vo.RequestCommonVo;
import com.spsb.vo.ResponseDocumentCommonVo;
import com.spsb.ws.ObjectFactory;

/**
 * <pre>
 * 채무불이행 Service Implements Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.04.15
 * 
 */
@Service
public class DebtServiceImpl extends SuperService implements DebtService {
	
	@Autowired
	WSClient wsClient;
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private MyPageUserModifyDaoForOracle myPageUserModifyDaoForOracle;
	@Autowired
	private MyPageUserModifyDaoForMssql myPageUserModifyDaoForMssql;

	@Autowired
	private CustomerDaoForOracle customerDaoForOracle;
	@Autowired
	private CustomerDaoForMssql customerDaoForMssql;
	
	@Autowired
	private DebtDaoForOracle debtDaoForOracle;
	@Autowired
	private DebtDaoForMssql debtDaoForMssql;

	@Autowired
	private CommonPage commonPage;

	@Value("#{common['PRF_DOC_FILE'].trim()}")
	private String prfilePath;

	// 채무불이행 첨부파일 경로
	@Value("#{common['DEBTFILE'].trim()}")
	private String debtFilePath;

	// 첨부파일 임시저장 경로
	@Value("#{common['TMPFILE'].trim()}")
	private String tmpFilePath;

	// 채무불이행 상태이력 첨부파일 경로
	@Value("#{common['DEBT_STAT_FILE'].trim()}")
	private String debtStatFilePath;

	// 채무불이행 약관 및 안내사항 첨부파일 경로
	@Value("#{common['DEBT_TXT'].trim()}")
	private String debtTxt;

	// 크레딧서비스회원목록요청서	  인터페이스 서비스 코드
	@Value("#{service['NOTICE_CONCLUSION'].trim()}")
	private String serviceCodeNoticeConclusion;
	
	
	// 크레딧서비스채무불이행등록요청서  인터페이스 서비스 코드
	@Value("#{service['CREDIT_REQUEST_APPLICATION'].trim()}")
	private String serviceCodeCreditRequestApplication;
	
	//웹서비스 
	protected ObjectFactory objFactory;
	@Autowired
	JaxWsProxyFactoryBean proxyFactory;
	
	// 라우팅메시지 생성을 위해 보내는이 ID
	@Value("#{service['RECEIVER_ID'].trim()}")
	private String receiveId;

	// / 라우팅메시지 생성을 위해 보내는이 이름
	@Value("#{service['RECEIVER_NAME'].trim()}")
	private String receiveName;
	
	//크레딧 서비스 코드
	@Value("#{service['SERVICE_CODE_CRED'].trim()}")
	private String serviceCodeCred;
	
	//CreditRequestAmendDefault 크레딧서비스채무불이행등록변경요청서(OUT)
	@Value("#{service['CREDIT_REQUEST_AMEND'].trim()}")
	private String serviceCodeCreditRequestAmend;
	
	/**
	 * <pre>
	 * 채무불이행 신청서 Form 조회 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014.04.15
	 * @param sessionUsrId
	 *            : 개인회원 순번, sessionCompUsrId : 기업회원 순번
	 * @return
	 */
	@Override
	public SBox getDebtForm(SBox sBox) {
		SBox result = new SBox();

		try {
			SBox paramBox = new SBox();
			SBox commonCodeBox = new SBox();

			// PARAMETER 초기화
			sBox.setIfEmpty("custType", "C");

			// 공통코드 초기화(전화번호, 이메일, 핸드폰번호, 정렬조건, 정렬순서)
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("npmOrderConditionTypeList", CommonCode.npmOrderConditionTypeList);
			commonCodeBox.set("orderTypeList", CommonCode.orderTypeList);

			result.set("commonCodeBox", commonCodeBox);
			paramBox.set("sessionCompUsrId", null);
			paramBox.set("grantCode", null);
			paramBox.set("mbrStat", null);
			paramBox.set("mbrId", sBox.get("sessionMbrId"));

			// 채무불이행 신청서 정보등록 담당자  조회
			result.set("userInfoBox", "oracle".equals(dbmsType)? debtDaoForOracle.selectMbrEmployeeListByCondition(paramBox).get(0)
															    :debtDaoForMssql.selectMbrEmployeeListByCondition(paramBox).get(0) 
													  );

			// 채무불이행 신청서 회사정보 조회
			Integer compUsrId = sBox.get("sessionCompUsrId");
			result.set("companyInfoBox", "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.selectCompUser(compUsrId)
																   :myPageUserModifyDaoForMssql.selectCompUser(compUsrId));

			paramBox.set("sessionCompUsrId", sBox.get("sessionCompUsrId"));
			paramBox.set("sessionCompMngCd", sBox.get("sessionCompMngCd"));
			paramBox.set("grantCode", null);
			paramBox.set("mbrStat", "N");
			paramBox.set("usrId", null);

			// 채권 담당자 후보군 리스트 검색
			//result.set("debentureContactList", debtDao.selectMbrEmployeeListByCondition(paramBox));
			
			// 파라미터에 거래처 순번이 있을 경우 
			if (!sBox.isEmpty("custId")) {
				result.set("custInfo", 
							"oracle".equals(dbmsType)? customerDaoForOracle.selectCustomer(sBox.getString("custId"),sBox.getInt("sessionCompUsrId"),sBox.getInt("sessionUsrId")) 
													 : customerDaoForMssql.selectCustomer(sBox.getString("custId"),sBox.getInt("sessionCompUsrId"),sBox.getInt("sessionUsrId")) 
						  );
			}

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			log.e("Unknown Exception[" + EErrorCodeType.search("30499").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 특정 거래처의 채권을 조회 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 22.
	 * @param bondMbrId
	 *            : 채권담당자멤버순번, custId : 거래처 순번, orderCondition : 정렬조건, orderType : 정렬순서
	 * @return
	 */
	@Override
	public SBox getDebentureListOfCustomer(SBox sBox) {
		SBox result = new SBox();

		try {
			SBoxList<SBox> debentureList = null;

			// PARAMETER 초기화
			sBox.setIfEmpty("orderCondition", "BILL_DT"); // 정렬 조건
			sBox.setIfEmpty("orderType", "DESC"); // 정렬 순서

			// 전체 채권 조회 권한이 있는 경우 채권담당자멤버순번을 null로 초기화
			if ((Boolean) sBox.get("isSessionDebnSearchGrn")) {
				// 전체 검색 OR 담당자 검색
				sBox.setIfEmpty("bondMbrId", (!sBox.isEmpty("bondMbrId")) ? sBox.get("bondMbrId") : null);
			} else {
				// 나의 채권 검색
				sBox.setIfEmpty("bondMbrId", sBox.get("sessionMbrId"));
			}
			
			// 채권 가이드 건만 보기 조건 설정
			if (!"on".equals(sBox.getString("debtGuideView"))) {
				sBox.set("debtGuideView", null);
			}
			
			// 채권 검색 리스트 조회
			//debentureList = debtDao.selectDebentureList(sBox);

			// resultCode, resultMsg 초기화
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

			// 조회 결과 저장
			result.set("param", sBox);
			result.set("debentureList", debentureList);

		} catch (Exception e) {
			result.set("REPL_CD", "30499");
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30499").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 작성 파라미터 정제 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 19.
	 * @param deptApplSeq  : 채무불이행신청서 순번, usrId : 채무불이행신청자 유저 순번, applyCustType : 신청자 회사 구분, applyCorpName : 신청자 회사명, custNo : 신청자 회사 사업자 등록번호, applyCorpNum : 신청자 회사 법인등록번호, applyOwnName : 신청자 회사 대표자명, postNo
	 *            : 신청자 회사 우편번호, applyAddr1 : 신청자 회사 주소1, applyAddr2 : 신청자 회사 주소2, applyBizType : 신청자 회사 업종, managerName : 정보등록담당자 이름, managerPart : 정보등록담당자 부서, managerPosition : 정보등록담당자 직위, telNo :
	 *            정보등록담당자 전화번호, mbNo : 정보등록담당자 핸드폰번호, faxNo : 정보등록담당자 팩스번호, email : 정보등록담당자 이메일, compUsrId : 기업회원순번, custId : 거래처 순번, custType : 거래처 회사구분, custNm : 거래처 회사명, ownNm : 거래처 대표자명, corpNo :
	 *            거래처 법인등록번호, postNo : 거래처 우편번호, addr1 : 거래처 주소1, addr2 : 거래처 상세주소, debt_type : 거래처 채무구분, totUnpdAmt : 미수총금액, stDebtAmt : 채무불이행금액, dataUploadA : A유형 파일, dataUploadB : B유형 파일,
	 *            dataUploadC : C유형 파일, dataUploadD : D유형 파일, dataUploadE : E유형 파일, debnId : 채권 순번, existFileSn : 채권 증빙파일 순번
	 * @return
	 */
	public SBox makeParameterForDebt(int step, SBox sBox) throws BizException {
		SBox paramBox = new SBox();
		SBox debtData = null;
		
		try {
			// 신청서 순번 초기화
			String debtApplSeq = (sBox.isEmpty("debtApplSeq")) ? null : sBox.getString("debtApplSeq");
			paramBox.set("debtApplSeq", debtApplSeq);
			
			// 기존 신청서 DB 조회
			if (!sBox.isEmpty("debtApplSeq")) {
				SBox tmpBox = new SBox();
				tmpBox.set("debtApplId", sBox.get("debtApplSeq"));
				debtData = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(tmpBox)
						                            : debtDaoForMssql.selectDebt(tmpBox);
			}
			
			// 신청서 상태 초기화
			if (!sBox.isEmpty("stat")) {
				paramBox.set("stat", sBox.get("stat"));
			}

			// 채무불이행 폴더 생성
			if (!sBox.isEmpty("debtApplSeq")) {
				File debtFileDir = new File(debtFilePath + "/" + sBox.getString("debtApplSeq"));
				if (!debtFileDir.isDirectory()) {
					if(debtFileDir.mkdir()) {
						log.i("[2-2] 채무불이행신청서 증빙파일 디렉토리 생성[ 경로 : "+ debtFilePath + sBox.getString("debtApplSeq") +" ]");
					}
				}
			}
			
			switch (step) {
			// STEP 1의 경우
			case 1:
				/* STEP 1 필수 파라미터 */

				paramBox.set("usrId", sBox.get("sessionUsrId")); // 정보등록담당자 순번
				paramBox.set("managerName", sBox.get("managerName")); // 정보등록담당자 이름
				paramBox.set("mbNo", sBox.getString("mbNo1") + "-" + sBox.getString("mbNo2") + "-" + sBox.getString("mbNo3")); // 핸드폰번호
				paramBox.set("email", sBox.getString("email1") + "@" + sBox.getString("email2")); // 이메일
				paramBox.set("postNo", sBox.getString("applyPostNo1")); // 우편번호
				paramBox.set("applyAddr1", sBox.get("applyAddr1")); // 신청인 회사 주소1
				paramBox.set("applyAddr2", sBox.get("applyAddr2")); // 신청인 회사 주소2
				
				
				/* 선택 파라미터 */
				if (!sBox.isEmpty("applyCorpNum")) {
					paramBox.set("applyCorpNum", sBox.get("applyCorpNum")); // 법인등록번호
				} else {
					paramBox.set("applyCorpNum", "");
				}
				if (!sBox.isEmpty("managerPart")) {
					paramBox.set("managerPart", sBox.get("managerPart")); // 정보등록담당자 부서
				}
				if (!sBox.isEmpty("managerPosition")) {
					paramBox.set("managerPosition", sBox.get("managerPosition")); // 정보등록담당자 직위
				}
				if (!"".equals(sBox.getString("telNo2").trim()) && !"".equals(sBox.getString("telNo3").trim())) {
					paramBox.set("telNo", sBox.getString("telNo1") + "-" + sBox.getString("telNo2") + "-" + sBox.getString("telNo3")); // 전화번호
				} else {
					paramBox.set("telNo", "");
				}
				if (!"".equals(sBox.getString("faxNo2").trim()) && !"".equals(sBox.getString("faxNo3").trim())) {
					paramBox.set("faxNo", sBox.getString("faxNo1") + "-" + sBox.getString("faxNo2") + "-" + sBox.getString("faxNo3")); // 팩스번호
				} else {
					paramBox.set("faxNo", "");
				}
	
				break;
			// STEP 2의 경우
			case 2:
				/* STEP 1 필수 파라미터 */
				paramBox.set("usrId", sBox.get("sessionUsrId")); // 정보등록담당자 순번
				paramBox.set("compUsrId", sBox.get("sessionCompUsrId")); // 기업회원 순번
				paramBox.set("applyCustType", sBox.get("applyCustType")); // 신청인 회사구분
				paramBox.set("applyCorpName", sBox.get("applyCorpName")); // 신청인 회사명
				paramBox.set("compNo", sBox.getString("applyCustNum").replaceAll("-", "")); // 사업자 등록번호
				paramBox.set("applyOwnName", sBox.get("applyOwnName")); // 신청인 회사 대표자명
				paramBox.set("postNo", sBox.getString("applyPostNo1")); // 우편번호
				paramBox.set("applyAddr1", sBox.get("applyAddr1")); // 신청인 회사 주소1
				paramBox.set("applyAddr2", sBox.get("applyAddr2")); // 신청인 회사 주소2
				paramBox.set("applyBizType", sBox.get("applyBizType")); // 신청인 회사 업종
				paramBox.set("managerName", sBox.get("managerName")); // 정보등록담당자 이름
				paramBox.set("compMngCd", sBox.get("sessionCompMngCd")); // 관리회사코드
				paramBox.set("mbNo", sBox.getString("mbNo1") + "-" + sBox.getString("mbNo2") + "-" + sBox.getString("mbNo3")); // 핸드폰번호
				paramBox.set("email", sBox.getString("email1") + "@" + sBox.getString("email2")); // 이메일
	
				/* 선택 파라미터 */
				if (!sBox.isEmpty("applyCorpNum")) {
					paramBox.set("applyCorpNum", sBox.get("applyCorpNum")); // 법인등록번호
				} else {
					paramBox.set("applyCorpNum", "");
				}
				if (!sBox.isEmpty("managerPart")) {
					paramBox.set("managerPart", sBox.get("managerPart")); // 정보등록담당자 부서
				}
				if (!sBox.isEmpty("managerPosition")) {
					paramBox.set("managerPosition", sBox.get("managerPosition")); // 정보등록담당자 직위
				}
				if (!"".equals(sBox.getString("telNo2").trim()) && !"".equals(sBox.getString("telNo3").trim())) {
					paramBox.set("telNo", sBox.getString("telNo1") + "-" + sBox.getString("telNo2") + "-" + sBox.getString("telNo3")); // 전화번호
				} else {
					paramBox.set("telNo", "");
				}
				if (!"".equals(sBox.getString("faxNo2").trim()) && !"".equals(sBox.getString("faxNo3").trim())) {
					paramBox.set("faxNo", sBox.getString("faxNo1") + "-" + sBox.getString("faxNo2") + "-" + sBox.getString("faxNo3")); // 팩스번호
				} else {
					paramBox.set("faxNo", "");
				}
	
				/* STEP 2 필수 파라미터 */
				paramBox.set("custType", sBox.getString("deptorCustType"));
				paramBox.set("custNm", sBox.getString("deptorCorpName"));
				paramBox.set("custNo", sBox.getString("deptorCustNum").replaceAll("-", ""));
				paramBox.set("ownNm", sBox.getString("deptorName"));
				paramBox.set("custPostNo",sBox.getString("postNo1"));
				paramBox.set("addr1", sBox.getString("addr1"));
				paramBox.set("addr2", sBox.getString("addr2"));
				paramBox.set("debt_type", ("delayPayment".equals(sBox.getString("dept_division")) ? "P" : "J"));
				paramBox.set("stDebtAmt", sBox.getString("nonpaySum").replaceAll(",", ""));
				paramBox.set("custId", !sBox.isEmpty("custId") ? sBox.getString("custId") : null);
	
				/* 선택 파라미터 */
				if (!sBox.isEmpty("deptorCorpNum")) {
					paramBox.set("corpNo", sBox.get("deptorCorpNum"));
				} else {
					paramBox.set("corpNo", "");
				}
				
				String custNo = (debtData != null) ? debtData.getString("CUST_NO") : "";
				if (paramBox.get("debtApplSeq") == null || (!"".equals(custNo) && !custNo.equals(paramBox.getString("custNo")))) {
					// ERP_ID 파라미터
					//String erpId = serviceCodeCred + paramBox.getString("compNo") + paramBox.getString("custNo") + CommonUtil.getTimeMiliSecond() + "000";
					String deptApplId = paramBox.getString("compNo") + paramBox.getString("custNo") + CommonUtil.getTimeMiliSecond();
					paramBox.set("deptApplId", deptApplId);
				}
				
				paramBox.set("overStDt", sBox.getString("overStDt"));
				
				
				
				break;
	
			// STEP 3의 경우
			/*case 3:
				 STEP 3 필수 파라미터 
				sBox.setIfEmpty("totUnpdAmt", "0");
				paramBox.set("totUnpdAmt", sBox.get("totUnpdAmt"));
				paramBox.set("stDebtAmt", !"0".equals(sBox.getString("bondSum").replaceAll(",", "")) ? (sBox.getString("bondSum").replaceAll(",", "")) : paramBox.get("stDebtAmt"));
				break;*/
			// 작성완료의 경우
			case 0:
				paramBox.set("stat", "SC");
				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			
		}
		return paramBox;
	}

	
	/**
	 * <pre>
	 * 채무불이행신청 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param deptApplSeq
	 *            : 채무불이행신청서 순번, usrId : 채무불이행신청자 유저 순번, applyCustType : 신청자 회사 구분, applyCorpName : 신청자 회사명, custNo : 신청자 회사 사업자 등록번호, applyCorpNum : 신청자 회사 법인등록번호, applyOwnName : 신청자 회사 대표자명, postNo
	 *            : 신청자 회사 우편번호, applyAddr1 : 신청자 회사 주소1, applyAddr2 : 신청자 회사 주소2, applyBizType : 신청자 회사 업종, managerName : 정보등록담당자 이름, managerPart : 정보등록담당자 부서, managerPosition : 정보등록담당자 직위, telNo :
	 *            정보등록담당자 전화번호, mbNo : 정보등록담당자 핸드폰번호, faxNo : 정보등록담당자 팩스번호, email : 정보등록담당자 이메일, compUsrId : 기업회원순번, custId : 거래처 순번, custType : 거래처 회사구분, custNm : 거래처 회사명, ownNm : 거래처 대표자명, corpNo :
	 *            거래처 법인등록번호, postNo : 거래처 우편번호, addr1 : 거래처 주소1, addr2 : 거래처 상세주소, debt_type : 거래처 채무구분, totUnpdAmt : 미수총금액, stDebtAmt : 채무불이행금액, dataUploadA : A유형 파일, dataUploadB : B유형 파일,
	 *            dataUploadC : C유형 파일, dataUploadD : D유형 파일, dataUploadE : E유형 파일, debnId : 채권 순번, existFileSn : 채권 증빙파일 순번
	 * @return
	 */
	@Override
	@Transactional
	public SBox addDeptApply(SBox sBox) {
		SBox result = new SBox();
		String stepStr = sBox.getString("step").replaceAll("\\p{Alpha}", "");
		int step = (!"".equals(stepStr)) ? Integer.parseInt(stepStr) : 0;

		try {
			// 파라미터 정제
			SBox paramBox = makeParameterForDebt(step, sBox);

			// 채무불이행 증빙파일 리스트
			SBoxList<SBox> debtFileList = new SBoxList<SBox>();
			switch (step) {
			/*
			// STEP 1의 경우
			case 1:
				
				// [1] 회원정보도 함께 수정한다고 체크한 경우 정보등록담당자 회원정보 수정
				if ("on".equals(sBox.getString("applyRemYn"))) {
					SBox userBox = new SBox();
					userBox.set("usrId", paramBox.getString("usrId"));
					userBox.set("usrNm", paramBox.getString("managerName"));
					userBox.set("deptNm", paramBox.getString("managerPart"));
					userBox.set("telNo", paramBox.get("telNo"));
					userBox.set("mbNo", paramBox.get("mbNo"));
					userBox.set("faxNo", paramBox.get("faxNo"));
					userBox.set("email", paramBox.get("email"));
					userBox.set("managerPosition", paramBox.get("managerPosition"));

					SBox managerResult = debtDao.updateDebtManager(userBox);

					if (!"00000".equals(managerResult.getString("REPL_CD"))) {
						throw new BizException(managerResult.getString("REPL_CD"), EErrorCodeType.search(managerResult.getString("REPL_CD")).getErrMsg());
					}

					managerResult.set("REPL_MSG", EErrorCodeType.search(managerResult.getString("REPL_CD")).getErrMsg());
					log.i("[1] 정보등록 담당자 정보 수정 응답 결과 : REPL_CD[" + managerResult.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(managerResult.getString("REPL_CD")).getErrMsg() + "]");
				}

				break;*/
			// STEP 2의 경우
			case 2:
				// [1] 거래처 신규 등록/수정 remYn:on,off 체크박스값
				/*if ("on".equals(sBox.getString("remYn"))) {
					SBox tmpBox = debtDao.insertOrUpdateCustForDebt(paramBox);

					if (!"00000".equals(tmpBox.getString("REPL_CD"))) {
						throw new BizException(tmpBox.getString("REPL_CD"), EErrorCodeType.search(tmpBox.getString("REPL_CD")).getErrMsg());
					}

					paramBox.set("custId", tmpBox.get("CUST_ID")); // 거래처 수정결과 CUST_ID 저장함
					log.i("[1] 거래처 등록/수정 응답 결과 : REPL_CD[" + tmpBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(tmpBox.getString("REPL_CD")).getErrMsg() + "]");
				
					// [1-2] 신용변동 테이블에 해당 거래처가 존재하지 않을 시 등록
					if (!customerDao.selectCustomerCreditCount(paramBox.getString("custNo"))) {
						SBox CrdParamBox = new SBox();
						CrdParamBox.set("custNm", paramBox.getString("custNm")); // 거래처명
						CrdParamBox.set("custNo", paramBox.getString("custNo")); // 거래처 사업자등록번호
						CrdParamBox.set("stat", "R"); // 신용변동테이블의 상태 컬럼 값 setting (R:예약)
						
						SBox insertCustCrdBox = customerDao.insertCustomerCredit(CrdParamBox);
						if (!"00000".equals(insertCustCrdBox.getString("REPL_CD"))) {
							throw new BizException(insertCustCrdBox.getString("REPL_CD"), EErrorCodeType.search(insertCustCrdBox.getString("REPL_CD")).getErrMsg());
						}
						log.i("[1-2] 신용변동 테이블에 거래처["+CrdParamBox.getString("custNm")+"] 등록 완료");
					}
				}*/
				
				// [2] 신청서 순번이 없다면 새로운 채무불이행신청서 등록
				if (paramBox.isEmpty("debtApplSeq")) {
					SBox insertResult = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtApply(paramBox): debtDaoForMssql.insertDebtApply(paramBox);

					if (!"00000".equals(insertResult.getString("REPL_CD"))) {
						throw new BizException(insertResult.getString("REPL_CD"), EErrorCodeType.search(insertResult.getString("REPL_CD")).getErrMsg());
					}

					result.set("NPM_ID", insertResult.getString("NPM_ID")); // NPM_ID : 새롭게 INSERT한 DEPT_APPL_ID 값
					log.i("[2-1] 채무불이행신청서 등록 응답 결과 : REPL_CD[" + insertResult.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(insertResult.getString("REPL_CD")).getErrMsg() + "]");
					//paramBox.set("debtApplSeq", insertResult.getString("DEBT_APPL_ID"));
					

					// [2-2] 해당 채무불이행 순번의 디렉토리가 있는지 확인하고 없으면 디렉토리 생성
					File debtFileDir = new File(debtFilePath + "/" + result.getString("NPM_ID"));
					if (!debtFileDir.isDirectory()) {
						if(debtFileDir.mkdir()) {
							log.i("[2-2] 채무불이행신청서 증빙파일 디렉토리 생성[ 경로 : "+ debtFilePath + result.getString("NPM_ID") +" ]");
						}
					}
				}
				
				if (!paramBox.isEmpty("debtApplSeq")) {
					SBox debtParamBox = new SBox();
					
					debtParamBox.set("debtApplId", paramBox.get("debtApplSeq"));
					debtParamBox.set("sessionCompUsrId", sBox.get("sessionCompUsrId"));
					String custNo = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(debtParamBox).getString("CUST_NO")
							                                 : debtDaoForMssql.selectDebt(debtParamBox).getString("CUST_NO");
					
					if (!custNo.equals(paramBox.getString("custNo"))) {
						
						// [3] 기존 저장된 미수 채권 삭제함
						/*SBox deleteResult = debtDao.deleteDebtDebenture(paramBox.getString("debtApplSeq"));
						
						if (!"00000".equals(deleteResult.getString("REPL_CD"))) {
							throw new BizException(deleteResult.getString("REPL_CD"), EErrorCodeType.search(deleteResult.getString("REPL_CD")).getErrMsg());
						}*/

						//log.i("[3] 채무불이행신청서 기존 미수채권 삭제 응답 결과 : REPL_CD[" + deleteResult.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(deleteResult.getString("REPL_CD")).getErrMsg() + "]");

						// [4] 삭제할 채무불이행신청 증빙파일 조회
						SBoxList<SBox> deleteFileSNList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox) : 
																					 debtDaoForMssql.selectDebtFileByCondition(paramBox) ;
						if (deleteFileSNList.size() > 0) {
							SBox deleteDebtFileResult = "oracle".equals(dbmsType)? debtDaoForOracle.deleteDebtFile(paramBox):
																				   debtDaoForMssql.deleteDebtFile(paramBox);
							
							if (!"00000".equals(deleteDebtFileResult.getString("REPL_CD"))) {
								throw new BizException(deleteDebtFileResult.getString("REPL_CD"), EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg());
							}
							
							log.i("[4-1] 채무불이행신청서 기존 미수채권 삭제 응답 결과 : REPL_CD[" + deleteDebtFileResult.getString("REPL_CD") + ", REPL_MSG["
									+ EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg() + "]");

							Iterator<SBox> fileSNIT = deleteFileSNList.iterator();
							while (fileSNIT.hasNext()) {
								SBox fileBox = (SBox) fileSNIT.next();
								if (!fileBox.isEmpty("FILE_PATH")) {
									if (CommonUtil.fileDelete(debtFilePath + "/" + fileBox.getString("FILE_PATH"))) {
										log.i("[4-2] 증빙파일 삭제 완료 : " + fileBox.getString("FILE_PATH"));
									}
								}
							}
						}
					}
				}
				
				SBox prfDebtParamBox = new SBox();
				prfDebtParamBox.set("debtApplSeq", paramBox.get("debtApplSeq"));
				prfDebtParamBox.set("deleteDebtFileSN", null);
				SBoxList<SBox> prfDebtFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(prfDebtParamBox):
																		    debtDaoForMssql.selectDebtFileByCondition(prfDebtParamBox);
				result.set("prfFileList", (prfDebtFileList.size() > 0) ? prfDebtFileList : "");
				
				break;
			// STEP 3의 경우
			case 3:

				//ArrayList<String> paramDebnList = new ArrayList<String>(); // 파라미터 기술료 코드 리스트
				//ArrayList<String> tempparamDebnList = new ArrayList<String>(); // 파라미터 기술료 코드 리스트
				//ArrayList<String> debnIdList = new ArrayList<String>(); // 저장되어있던 DB 채권 코드 리스트
				//SBoxList<SBox> addPrfFileList = new SBoxList<SBox>(); // 신규 채권 관련 첨부파일 조회 리스트
				//SBoxList<SBox> addDebtDebentureList = new SBoxList<SBox>(); // 채무불이행 채권 insert
				
				
				// [1] 파라미터 정제작업 
				// [1-1] 채권 코드 파라미터 정제 (체크박스가 여러개인 경우 String [] 로 데이터가 넘어옴)
				/*if (sBox.get("debnId") != null) {
					String[] debnArray = (sBox.get("debnId") instanceof String) ? new String[] { sBox.getString("debnId") } : (String[]) sBox.get("debnId");
					int i = 0;
					for (String debnId : debnArray) {
						SBox debtDebentureSbox = new SBox(); // 채무불이행 채권 insert
						String[] str = debnId.split("#");
						debnIdList.add(str[0]);
						debtDebentureSbox.set("debnId", str[0]);
						debtDebentureSbox.set("contId", str[1]);
						debtDebentureSbox.set("debtApplSeq", paramBox.get("debtApplSeq"));
						addDebtDebentureList.add(i, debtDebentureSbox);
						i++;
					}
					
					for (int i=0; i<tempparamDebnList.size(); i++) {
						SBox debtDebentureSbox = new SBox(); // 채무불이행 채권 insert
						String tmpStr = tempparamDebnList.get(i);
						String[] str = tmpStr.split("|");
						debtDebentureSbox.set("debnId", str[0]);
						debtDebentureSbox.set("contId", str[1]);
						paramDebnList.add(str[0]);
						debtDebentureSbox.set("debtApplSeq", paramBox.get("debtApplSeq"));
						addDebtDebentureList.add(i, debtDebentureSbox);
					}
				}*/
				/*if (sBox.get("debnId") != null) {
					String[] debnArray = (sBox.get("debnId") instanceof String) ? new String[] { sBox.getString("debnId") } : (String[]) sBox.get("debnId");
					String[] contArray = (sBox.get("contId") instanceof String) ? new String[] { sBox.getString("contId") } : (String[]) sBox.get("contId");
					for (String debnId : debnArray) {
						debnIdList.add(debnId);
					}
					
					for (int i=0; i<debnArray.length; i++) {
						SBox debtDebentureSbox = new SBox(); // 채무불이행 채권 insert
						debtDebentureSbox.set("debnId", debnArray[i]);
						debtDebentureSbox.set("contId", contArray[i]);
						debtDebentureSbox.set("debtApplSeq", paramBox.get("debtApplSeq"));
						addDebtDebentureList.add(i, debtDebentureSbox);
					}
				}*/
				// [1-2] 미수 채권 정보 조회를 위한 파라미터 정제
				/*paramBox.set("custNo", sBox.get("custNo"));
				paramBox.set("orderCondition", "REG_DT");
				paramBox.set("orderType", "ASC");
				paramBox.set("debtApplId", paramBox.get("debtApplSeq"));*/

				// 미수 채권 정보 조회
				//SBoxList<SBox> debnList = debtDao.selectDebentureList(paramBox);
				
				// [1-3] 채무불이행신청에 저장된 채권번호를 리스트에 저장
				/*for (SBox debnBox : debnList) {
					if(debnBox.get("DEBT_APPL_ID") != null) {
						debnIdList.add(debnBox.getString("DEBN_ID"));
					}
				}*/
				
				// TODO 채권번호는 CHAR(35), 채권을 10개만 넣어도 350자가 되어버림. 일단 프로시저 상에서 데이터 범위를 MAX로 해놨으나 채권이 100개 이상이 되어버리면 진행이 안됨.
				// [2] DE_DEBT_DEBN 에서 채권 DB 삭제
				//SBox deleteDebtDebnResult = debtDao.deleteDebtDebenture(paramBox.getString("debtApplSeq"));

				/*if (!"00000".equals(deleteDebtDebnResult.getString("REPL_CD"))) {
					throw new BizException(deleteDebtDebnResult.getString("REPL_CD"), EErrorCodeType.search(deleteDebtDebnResult.getString("REPL_CD")).getErrMsg());
				}
				
				log.i("[2] 채무불이행신청서 기존 미수채권 삭제 응답 결과 : REPL_CD[" + deleteDebtDebnResult.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(deleteDebtDebnResult.getString("REPL_CD")).getErrMsg() + "]");
				*/
				// [3] DE_DEBT_DEBN 에서 채권 DB INSERT
				/*for (String debnId : paramDebnList) {
					paramBox.set("debnId", debnId);
					
					SBox insertDebtDebnResult = debtDao.insertDebtDebenture(paramBox);
					
					if (!"00000".equals(insertDebtDebnResult.getString("REPL_CD"))) {
						throw new BizException(EErrorCodeType.search(insertDebtDebnResult.getString("REPL_CD")).getErrMsg());
					}
				}*/
				
				
				/*for (String debnId : paramDebnList) {
						paramBox.set("debnId", debnId);
						SBox insertDebtDebnResult = debtDao.insertDebtDebenture(paramBox);
						if (!"00000".equals(insertDebtDebnResult.getString("REPL_CD"))) {
							throw new BizException(EErrorCodeType.search(insertDebtDebnResult.getString("REPL_CD")).getErrMsg());
						}
				}*/
				/*Iterator<SBox> insertDD = addDebtDebentureList.iterator();
				while(insertDD.hasNext()){
					SBox dd = (SBox) insertDD.next();
					SBox insertDebtDebnResult = debtDao.insertDebtDebenture(dd);
					if (!"00000".equals(insertDebtDebnResult.getString("REPL_CD"))) {
						throw new BizException(EErrorCodeType.search(insertDebtDebnResult.getString("REPL_CD")).getErrMsg());
					}
				}*/
					
				// 삭제할 채권이 있을 때만 진행
				//String deleteDebnId = debnIdList.toString().substring(1, debnIdList.toString().length()-1).replaceAll(" ", "");
				//paramBox.set("debnIdList", deleteDebnId);
				
				/*SBoxList<SBox> deleteDebtFileSNList = null;
				if (!"".equals(deleteDebnId)) {
					// [4] DE_DEBT_FILE에서 삭제 채권에 해당하는 채무불이행 증빙파일 DB 조회
					deleteDebtFileSNList = debtDao.selectDebtFileByCondition(paramBox);
					
					// [5] DE_DEBT_FILE에서 삭제 채권에 해당하는 채무불이행 증빙파일 DB 삭제
					if (deleteDebtFileSNList.size() > 0) {
						SBox deleteDebtFileResult = debtDao.deleteDebtFile(paramBox);
						log.i("[5] 채무불이행신청서 기존 미수채권 삭제 응답 결과 : REPL_CD[" + deleteDebtFileResult.getString("REPL_CD") + ", REPL_MSG["
								+ EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg() + "]");
						
						if (!"00000".equals(deleteDebtFileResult.getString("REPL_CD"))) {
							throw new BizException(deleteDebtFileResult.getString("REPL_CD"), EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg());
						}
					}
				}*/
				
				// [6] DE_DEBN_FILE에서 신규 채권에 해당하는 채무불이행 증빙파일 DB 조회
				/*for (String debnId : paramDebnList) {
					// 선택된 채권 관련 첨부파일 조회 후 저장함
					SBoxList<SBox> addPrfList = debtDao.selectDebenturePrfFile(debnId);
					if (addPrfList.size() > 0) {
						addPrfFileList.addAll(addPrfList);
					}
					log.i("[6] 채무불이행 선택된 채권 조회 후 첨부파일 저장 완료 / 채권번호:[" + debnId + "]" + " , 첨부파일 갯수[" + addPrfList.size() + "]");
				}*/
				
				// [7] 해당 신청서의 이미 저장된 증빙파일 조회
				/*SBox debtFileParamBox = new SBox();
				debtFileParamBox.set("debtApplSeq", paramBox.get("debtApplSeq"));
				SBoxList<SBox> saveDebtPrfList = debtDao.selectDebtFileByCondition(debtFileParamBox);
				
				// [8] 신규 채권 증빙파일을 DE_DEBT_FILE에 DB INSERT
				
				SBoxList<SBox> newPrfFileCopyList = new SBoxList<SBox>();
				for (int i = 0; i < addPrfFileList.size(); i++) {
					SBox addPrfFile = addPrfFileList.get(i); 
					
					boolean BTF = false;
					for (int j=0; j<saveDebtPrfList.size(); j++) {
						String saveDebnId = saveDebtPrfList.get(j).getString("DEBN_ID");
						
						if (saveDebnId.equals(addPrfFile.get("DEBN_ID"))) {
							BTF = true;
							break;
						}
					}
					
					if (!BTF) {
						SBox prfParamBox = new SBox();
						prfParamBox.set("debtApplSeq", paramBox.get("debtApplSeq"));
						prfParamBox.set("typeCd", addPrfFile.get("TYPE_CD"));
						prfParamBox.set("fileNm", addPrfFile.get("FILE_NM"));
						
						// 파일 경로 생성
						String[] fileNm = CommonUtil.separateFileName(addPrfFile.getString("FILE_NM"));
						String newPath = prfParamBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (i+1) + "." + fileNm[1];
						
						prfParamBox.set("filePath", newPath);
						
						SBox debtDebnFileInsertResult = debtDao.insertDebtFile(prfParamBox);
						if (!"00000".equals(debtDebnFileInsertResult.getString("REPL_CD"))) {
							throw new BizException(debtDebnFileInsertResult.getString("REPL_CD"), EErrorCodeType.search(debtDebnFileInsertResult.getString("REPL_CD")).getErrMsg());
						}
						
						// 채무불이행 증빙파일 경로 추가
						prfParamBox.set("FILE_PATH", addPrfFile.get("FILE_PATH"));
						prfParamBox.set("NEW_FILE_PATH", newPath);
						
						newPrfFileCopyList.set(prfParamBox);
					}
				}*/
				
				// [9] 삭제 채권에 해당하는 채무불이행 증빙파일 물리 삭제
				/*if (deleteDebtFileSNList != null) {
					Iterator<SBox> fileSNIT = deleteDebtFileSNList.iterator();
					while (fileSNIT.hasNext()) {
						SBox fileBox = (SBox) fileSNIT.next();
						if (!fileBox.isEmpty("FILE_PATH")) {
							if (CommonUtil.fileDelete(debtFilePath + "/" + fileBox.getString("FILE_PATH"))) {
								log.i("증빙파일 삭제 완료 : " + fileBox.getString("FILE_PATH"));
							}
						}
					}
				}*/
				
				// [10] 신규 채권에 해당하는 채무불이행 증빙파일 물리 복사
				/*for (SBox addPrfFile : newPrfFileCopyList) {
					
					String originPath = prfilePath + "/" + addPrfFile.getString("FILE_PATH");
					String newPath = debtFilePath + "/" + addPrfFile.getString("NEW_FILE_PATH");
					
					CommonUtil.fileCopy(originPath, newPath);
				}*/
				
				// [11] 채권 증빙파일 파라미터 저장함.
				/*SBox prfParamBox = new SBox();
				prfParamBox.set("debtApplSeq", paramBox.get("debtApplSeq"));
				prfParamBox.set("deleteDebtFileSN", null);
				SBoxList<SBox> debtPrfFileList = debtDao.selectDebtFileByCondition(prfParamBox);
				result.set("prfFileList", (debtPrfFileList.size() > 0) ? debtPrfFileList : "");*/
				
				break;
			// STEP 4의 경우
			case 4:

				paramBox.set("usrId", sBox.get("sessionUsrId"));
				
				int index = 0; // 파일 순번
				
				// [1-1] 파일 정보 추출(거래명세표)
				if (sBox.get("dataUploadA") != null) {
					List<MultipartFile> files = sBox.get("dataUploadA");
					for (MultipartFile file : files) {
						SBox fileBox = new SBox();
						fileBox.set("file", file);
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("typeCd", "A");
						fileBox.set("fileNm", file.getOriginalFilename());
						
						if (!"".equals(file.getOriginalFilename())) {
							String[] fileNm = CommonUtil.separateFileName(file.getOriginalFilename());
							fileBox.set("filePath", paramBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." + fileNm[1]);
							index++;
						}
						debtFileList.add(fileBox);
					}
				}
				// [1-2] 파일 정보 추출(세금계산서)
				if (sBox.get("dataUploadB") != null) {
					List<MultipartFile> files = sBox.get("dataUploadB");
					for (MultipartFile file : files) {
						SBox fileBox = new SBox();
						fileBox.set("file", file);
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("typeCd", "B");
						fileBox.set("fileNm", file.getOriginalFilename());
						
						if (!"".equals(file.getOriginalFilename())) {
							String[] fileNm = CommonUtil.separateFileName(file.getOriginalFilename());
							fileBox.set("filePath", paramBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." + fileNm[1]);
							index++;
						}
						debtFileList.add(fileBox);
					}
				}

				// [1-3] 파일 정보 추출(법원판결문)
				if (sBox.get("dataUploadC") != null) {
					List<MultipartFile> files = sBox.get("dataUploadC");
					for (MultipartFile file : files) {
						SBox fileBox = new SBox();
						fileBox.set("file", file);
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("typeCd", "C");
						fileBox.set("fileNm", file.getOriginalFilename());
						
						if (!"".equals(file.getOriginalFilename())) {
							String[] fileNm = CommonUtil.separateFileName(file.getOriginalFilename());
							fileBox.set("filePath", paramBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." + fileNm[1]);
							index++;
						}
						debtFileList.add(fileBox);
					}
				}

				// [1-4] 파일 정보 추출(계약서)
				if (sBox.get("dataUploadD") != null) {
					List<MultipartFile> files = sBox.get("dataUploadD");
					for (MultipartFile file : files) {
						SBox fileBox = new SBox();
						fileBox.set("file", file);
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("typeCd", "D");
						fileBox.set("fileNm", file.getOriginalFilename());
						
						if (!"".equals(file.getOriginalFilename())) {
							String[] fileNm = CommonUtil.separateFileName(file.getOriginalFilename());
							fileBox.set("filePath", paramBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." + fileNm[1]);
							index++;
						}
						debtFileList.add(fileBox);
					}
				}

				// [1-5] 파일 정보 추출(기타 증빙자료)
				if (sBox.get("dataUploadE") != null) {
					List<MultipartFile> files = sBox.get("dataUploadE");
					for (MultipartFile file : files) {
						SBox fileBox = new SBox();
						fileBox.set("file", file);
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("typeCd", "E");
						fileBox.set("fileNm", file.getOriginalFilename());
						
						if (!"".equals(file.getOriginalFilename())) {
							String[] fileNm = CommonUtil.separateFileName(file.getOriginalFilename());
							fileBox.set("filePath", paramBox.getString("debtApplSeq") + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." + fileNm[1]);
							index++;
						}
						debtFileList.add(fileBox);
					}
				}
				log.i("[1] 첨부파일 파라미터 정제완료 , 총 개수 [" + debtFileList.size() + "]");

				// [2] 삭제할 채무불이행신청 증빙파일 조회
				if (!sBox.isEmpty("deleteDebtFileSN")) {
					paramBox.set("deleteDebtFileSN", sBox.get("deleteDebtFileSN"));
					SBoxList<SBox> deleteDEBTFileSNList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
																		             debtDaoForMssql.selectDebtFileByCondition(paramBox);
					if (deleteDEBTFileSNList.size() > 0) {
						SBox deleteDebtFileResult = "oracle".equals(dbmsType)? debtDaoForOracle.deleteDebtFile(paramBox):
																			   debtDaoForMssql.deleteDebtFile(paramBox);

						if (!"00000".equals(deleteDebtFileResult.getString("REPL_CD"))) {
							throw new BizException(deleteDebtFileResult.getString("REPL_CD"), EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg());
						}

						log.i("[2] 채무불이행신청서 기존 미수채권 삭제 응답 결과 : REPL_CD[" + deleteDebtFileResult.getString("REPL_CD") + ", REPL_MSG["
								+ EErrorCodeType.search(deleteDebtFileResult.getString("REPL_CD")).getErrMsg() + "]");
	
						Iterator<SBox> fileIT = deleteDEBTFileSNList.iterator();
						while (fileIT.hasNext()) {
							SBox fileBox = (SBox) fileIT.next();
							if (!fileBox.isEmpty("FILE_PATH")) {
								if (CommonUtil.fileDelete(debtFilePath + "/" + fileBox.getString("FILE_PATH"))) {
									log.i("증빙파일 삭제 완료 : " + fileBox.getString("FILE_PATH"));
								}
							}
						}
					}
				}

				// [3] 새롭게 등록한 채권 증빙파일 등록함
				Iterator<SBox> fileIT = debtFileList.iterator();
				while (fileIT.hasNext()) {
					SBox fileBox = (SBox) fileIT.next();
					if (!fileBox.isEmpty("fileNm")) {
						//fileBox.set("debnId", null); // 기존증빙파일은 채권첨부파일 테이블에서 가져오기 때문에 null 처리함
						SBox fileInsertResult = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtFile(fileBox) :
																		   debtDaoForMssql.insertDebtFile(fileBox);

						if ("00000".equals(fileInsertResult.getString("REPL_CD"))) {
							File saveFile = new File(debtFilePath + "/" + fileBox.getString("filePath"));
							MultipartFile file = fileBox.get("file");
							file.transferTo(saveFile);
						} else {
							throw new BizException(fileInsertResult.getString("REPL_CD"), EErrorCodeType.search(fileInsertResult.getString("REPL_CD")).getErrMsg());
						}

						log.i("[3] 채무불이행신청 신규증빙파일 INSERT 결과코드 : " + fileInsertResult.getString("REPL_CD") + " / 파일명 : " + fileBox.getString("fileNm") + " / 변경 파일명 : " + fileBox.getString("filePath"));
						result.setIfEmpty("REPL_CD", fileInsertResult.getString("REPL_CD"));

					}
				}

				// [5] 채무불이행 증빙파일 총괄 조회
				SBox prfFileParamBox = new SBox();
				prfFileParamBox.set("debtApplSeq", sBox.get("debtApplSeq"));
				prfFileParamBox.set("deleteDebtFileSN", null);
				SBoxList<SBox> debtPrfList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(prfFileParamBox):
																	    debtDaoForMssql.selectDebtFileByCondition(prfFileParamBox);
				result.set("debtPrfFileList", debtPrfList);

				break;
			// 작성 완료일시
			case 0:
				SBox validationBox = validateDebtForm(paramBox.getString("debtApplSeq"));
				if (!"00000".equals(validationBox.getString("REPL_CD"))) {
					throw new BizException(validationBox.getString("REPL_CD"), EErrorCodeType.search(validationBox.getString("REPL_CD")).getErrMsg());
				}
				break;
			}

			// STAT 조정을 위한 채무불이행신청 증빙파일 조회 및 size 검증
			if (!paramBox.isEmpty("stat")) {
				paramBox.set("deleteDebtFileSN", null);
				//paramBox.set("debnIdList", null);
				SBoxList<SBox> tmpDebtFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
																		    debtDaoForMssql.selectDebtFileByCondition(paramBox);// SIZE 검증을 위한 임시 증빙파일 SBOX
				paramBox.set("stat", (tmpDebtFileList.size() > 0) ? "SC" : null);
			}

			// 채무불이행 수정 (DE_DEBT_APPL)
			if (!paramBox.isEmpty("debtApplSeq")) {
				SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtApply(paramBox):
														    debtDaoForMssql.updateDebtApply(paramBox);

				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
				}

				result.set("NPM_ID", updateBox.get("NPM_ID"));
				result.set("REPL_CD", updateBox.get("REPL_CD"));
				log.i("채무불이행신청서 수정 응답 결과 : REPL_CD[" + updateBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg() + "]");
			}

			result.set("STAT", (!paramBox.isEmpty("stat")) ? paramBox.get("stat") : "TS");
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

		} catch (BizException biz) {
			System.out.println(biz.getErrCode());
			result.set("REPL_CD", biz.getErrCode());
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			result.set("REPL_CD", "30499");
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30499").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * DB에 저장되어 있는 채무불이행신청서 데이터 VALIDATION SERVICE METHOD
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 20.
	 * @param debtApplId
	 *            : 채무불이행신청순번
	 * @return
	 */
	public SBox validateDebtForm(String debtApplId) {
		SBox result = new SBox();

		try {
			boolean BTF = true;

			// 정보 조회를 위한 임시 파라미터 SBox
			SBox paramBox = new SBox();

			// [1] 채무불이행신청서 조회 후 필수 데이터 NULL 체크
			paramBox.set("debtApplId", debtApplId);
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(paramBox):
													  debtDaoForMssql.selectDebt(paramBox);
			// 필수 DATA CHECK
			if (debtBox.isEmpty("COMP_USR_ID")) {
				BTF = false; // 기업회원순번 NULL 체크
			} else if (debtBox.isEmpty("REG_ID")) {
				BTF = false; // 등록회원순번 NULL 체크
			} else if (debtBox.isEmpty("UPD_ID")) {
				BTF = false; // 수정회원순번 NULL 체크
			} else if (debtBox.isEmpty("STAT")) {
				BTF = false; // 채무불이행상태 NULL 체크
			} else if (debtBox.isEmpty("COMP_TYPE")) {
				BTF = false; // 신청자 회사 구분 NULL 체크
			/*} else if (debtBox.isEmpty("DOC_CD")) {
				BTF = false; // 채무불이행신청코드 NULL 체크
*/			} else if (debtBox.isEmpty("COMP_NM")) {
				BTF = false; // 신청자 회사명 NULL 체크
			} else if (debtBox.isEmpty("COMP_NO")) {
				BTF = false; // 신청자 회사 사업자등록번호 NULL 체크
			} else if (debtBox.isEmpty("OWN_NM")) {
				BTF = false; // 신청자 회사 대표자명 NULL 체크
			} else if (debtBox.isEmpty("POST_NO")) {
				BTF = false; // 신청자 회사 우편번호 NULL 체크
			} else if (debtBox.isEmpty("ADDR_1")) {
				BTF = false; // 신청자 회사1 NULL 체크
			} else if (debtBox.isEmpty("BIZ_TYPE")) {
				BTF = false; // 신청자 회사 업종 NULL 체크
			} else if (debtBox.isEmpty("USR_NM")) {
				BTF = false; // 정보등록담당자 이름 NULL 체크
			} else if (debtBox.isEmpty("MB_NO")) {
				BTF = false; // 정보등록담당자 핸드폰 번호 NULL 체크
			} else if (debtBox.isEmpty("EMAIL")) {
				BTF = false; // 정보등록담당자 이메일 NULL 체크
			} else if (debtBox.isEmpty("CUST_TYPE")) {
				BTF = false; // 채무자 회사 구분 NULL 체크
			} else if (debtBox.isEmpty("CUST_NM")) {
				BTF = false; // 채무자 회사명 NULL 체크
			} else if (debtBox.isEmpty("CUST_NO")) {
				BTF = false; // 채무자 회사 사업자등록번호 NULL 체크
			} else if (debtBox.isEmpty("CUST_OWN_NM")) {
				BTF = false; // 채무자 회사 대표자명 NULL 체크
			} else if (debtBox.isEmpty("CUST_POST_NO")) {
				BTF = false; // 채무자 회사 우편번호 NULL 체크
			} else if (debtBox.isEmpty("CUST_ADDR_1")) {
				BTF = false; // 채무자 회사 주소1 NULL 체크
			} else if (debtBox.isEmpty("DEBT_TYPE")) {
				BTF = false; // 채무구분 NULL 체크
			} else if (debtBox.isEmpty("ST_DEBT_AMT")) {
				BTF = false; // 채무불이행금액 NULL 체크
			}

			if (!BTF) {
				throw new BizException("30416", EErrorCodeType.search("30416").getErrMsg());
			}

			// [2] 채무불이행신청 증빙파일 조회 및 size 검증
			paramBox.set("debtApplSeq", debtApplId);
			SBoxList<SBox> debtFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
														             debtDaoForMssql.selectDebtFileByCondition(paramBox);
			if (debtFileList.size() == 0) {
				throw new BizException("30415", EErrorCodeType.search("30415").getErrMsg());
			}

			result.set("REPL_CD", "00000");
		} catch (BizException biz) {
			result.set("REPL_CD", biz.getErrCode());
			result.set("REPL_MSG", biz.getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 조회 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 04. 29.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지
	 *            순번, sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번
	 * @return sBox : VIEW단으로 넘겨줄 결과 Object
	 */
	@Override
	public SBox getDebtInquiryList(SBox sBox) {

		SBox result = new SBox();

		try {

			// [1] PARAMETER 초기화
			// [1-1] 최초 로딩시 파라미터 설정
			if (sBox.isEmpty("num")) {

				// 모든 결제조건의 CheckBox 값을 checked 할 수 있도록 함.
				sBox.setIfEmpty("processStatusALL", "ALL");
				sBox.setIfEmpty("processStatusTS", "TS");
				sBox.setIfEmpty("processStatusSC", "SC");
				sBox.setIfEmpty("processStatusAW", "AW");
				sBox.setIfEmpty("processStatusEV", "EV");
				sBox.setIfEmpty("processStatusAA", "AA");
				sBox.setIfEmpty("processStatusPY", "PY");
				sBox.setIfEmpty("processStatusNT", "NT");
				sBox.setIfEmpty("processStatusCA", "CA");
				sBox.setIfEmpty("processStatusFA", "FA");
				sBox.setIfEmpty("processStatusRC", "RC");
				sBox.setIfEmpty("processStatusRR", "RR");
				sBox.setIfEmpty("processStatusCB", "CB");
				sBox.setIfEmpty("processStatusFB", "FB");
				sBox.setIfEmpty("processStatusAC", "AC");
				sBox.setIfEmpty("processStatusAU", "AU");
				sBox.setIfEmpty("processStatusKF", "KF");
				sBox.setIfEmpty("processStatusCR", "CR");
				sBox.setIfEmpty("processStatusRU", "RU");
			}

			// [1-2] 기타 파라미터 초기화
			// PARAMETER 초기화를 위해 시작날짜, 종료날짜 세팅
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");

			// 종료 날짜 세팅
			String edDt = frm.format(cal.getTime());
			// 시작 날짜 세팅 ( 1달전 )
			cal.add(Calendar.MONTH, -1);
			String stDt = frm.format(cal.getTime());

			// 검색조건 : 진행상태 체크 설정
			if (sBox.get("processStatusType") instanceof String[]) {
				String[] processStatusTypeArray = sBox.get("processStatusType");
				String processStatusTypeParam = "";
				for (int i = 0; i < processStatusTypeArray.length; i++) {
					String processStatusCheck = "processStatus" + processStatusTypeArray[i];
					sBox.set(processStatusCheck, processStatusTypeArray[i]);

					processStatusTypeParam += (processStatusTypeParam.length() > 0 ? "," : "") + processStatusTypeArray[i];
				}
				sBox.set("processStatusType", processStatusTypeParam);
			} else if (sBox.get("processStatusType") instanceof String) {
				String processStatusCheck = "processStatus" + sBox.get("processStatusType");
				sBox.set(processStatusCheck, sBox.get("processStatusType"));
			}

			sBox.setIfEmpty("processStatusType", "SC,TS,AW,EV,AA,PY,NT,CA,FA,RC,RR,CB,FB,AU,AC,CR,RU,KF");
			sBox.setIfEmpty("periodConditionType", "REG_DT"); // 검색조건 : 작성일자 타입(신청일자가 기본)
			sBox.setIfEmpty("periodStDt", stDt); // 검색조건 : 조회기간 시작날짜(현재 월 기준 1일)
			sBox.setIfEmpty("periodEdDt", edDt); // 검색조건 : 조회기간 종료날짜(현재 월 기준 말일)
			sBox.setIfEmpty("custSearchType", "business_all"); // 검색조건 : 거래처(default 전체)
			sBox.setIfEmpty("num", 1); // 검색조건 : 현재 페이지
			sBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			sBox.setIfEmpty("debtMbrType", ((Boolean) sBox.get("isSessionDebtGrnt")) ? "ALL" : "none"); // 검색조건 : 등록담당자(전체선택)
			sBox.setIfEmpty("orderCondition", "REG_DT"); // 검색조건 : 정렬옵션값(신청자코드가 기본)
			sBox.setIfEmpty("orderType", "DESC"); // 검색조건 : 정렬옵션값(오름차순 기본)
			sBox.setIfEmpty("searchType", "L"); // 검색조건 : 리스트 or 엑셀 (리스트 기본)
			//sBox.setIfEmpty("pageType", "PREAPPLICATION"); // PageType (PREAPPLICATION : 신청서 조회)

			// [2] 채무불이행 등록 담당자 조회
			sBox.setIfEmpty("grantCode", "D"); // D권한의 담당자 : 채무불이행 신청서 작성,수정,삭제 권한
			sBox.setIfEmpty("mbrStat", "N"); // 상태 : 정상
			SBoxList<SBox> debtMbrList = "oracle".equals(dbmsType)? debtDaoForOracle.selectMbrEmployeeListByCondition(sBox):
																    debtDaoForMssql.selectMbrEmployeeListByCondition(sBox);

			// [3] 채무불이행 신청서 TotlCnt 조회
			int debtTotalCount = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtTotalCount(sBox):
															debtDaoForMssql.selectDebtTotalCount(sBox);

			// [4] 채무불이행 신청서 List 조회
			SBoxList<SBox> debtInquiryList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtList(sBox):
																		debtDaoForMssql.selectDebtList(sBox);
			Iterator<SBox> it = debtInquiryList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			// [5] Paging 모듈 호출함
			String pcPage = commonPage.getPCPagingPrint(debtTotalCount, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");

			sBox.setIfEmpty("classType", "down");
			String classType = sBox.get("classType");
			result.set("classType", classType);
			
			result.set("pcPage", pcPage); // 채무불이행 페이징
			result.set("total", debtTotalCount); // 채무불이행 신청서 리스트 전체 갯수
			result.set("debtMbrList", debtMbrList); // 채무불이행 등록담당자 리스트
			result.set("debtInquiryList", debtInquiryList); // 채무불이행 신청서 리스트
			result.set("debtApplicationListOrderConditionTypeList", CommonCode.debtApplicationListOrderConditionTypeList); // 정렬조건
			result.set("orderTypeList", CommonCode.orderTypeList); // 오름 or 내림
			result.set("rowSizeTypeList", CommonCode.rowSizeTypeList); // 목록개수

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 EXCEL 다운로드 조회 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 04. 30.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지
	 *            순번, sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번 , fileName : 엑셀파일 이름
	 * @return sBox : 결과 Object
	 */
	@Override
	public SBox getDebtInquiryListForExcel(SBox sBox, String fileName) {

		SBox result = new SBox();
		CommonExcel poi = null;
		try {

			// Excel Type으로 프로시져 호출함
			sBox.set("searchType", "E");
			SBox debtInquiryListForExcel = this.getDebtInquiryList(sBox);

			String columnListText = "신청서코드$C,작성일$C,거래처명$C,사업자번호$C,대표자(채무자)$C,법인등록번호$C,채무금액(원)$C,진행상태$C,등록 담당자$C";

			String columnListValue = "DOC_CD,REG_DT,CUST_NM,CUST_NO,CUST_OWN_NM,CUST_CORP_NO,ST_DEBT_AMT,STAT,USR_NM";

			// [1] Poi Excel 파일 생성
			poi = new CommonExcel(tmpFilePath + fileName);

			// [2] Sheet Name 설정
			String sheetsName[] = { "채무불이행 신청서 조회" };

			poi.createSheet(sheetsName);

			// [3] Title Style 생성
			ArrayList<CellStyle> titleStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListText.split("\\,").length; i++) {

				// [3-1] titleStyle 생성
				CellStyle titleStyle = poi.createNewStyle();
				// [3-2] FONT 설정
				poi.setFontStyleSetting(titleStyle, "돋움", (short) 10, "BOLD", false, false);
				// [3-3] 배경색상 설정
				poi.setBackgroundColorSetting(titleStyle, "LIGHT-YELLOW"); // 배경색상 설정
				// [3-4] 라인두께 설정
				poi.setLineBorder(titleStyle, "THICK");

				titleStyles.add(i, titleStyle);

			}

			// [4] Title Excel 파일 기록
			poi.setBulkColsDataFromStringArray(titleStyles, 1, 1, columnListText.split("\\,"));

			// [5] Data Style 생성
			ArrayList<CellStyle> dataStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListValue.split("\\,").length; i++) {

				// [5-1] dataStyle 생성
				CellStyle dataStyle = poi.createNewStyle();
				// [5-2] FONT 설정
				poi.setFontStyleSetting(dataStyle, "돋움", (short) 10, "NORMAL", false, false);
				// [5-3] 배경색상 설정
				poi.setBackgroundColorSetting(dataStyle, "WHITE"); // 배경색상 설정
				// [5-4] 라인두께 설정
				poi.setLineBorder(dataStyle, "THIN");

				dataStyles.add(i, dataStyle);
			}

			// [6] Excel 파일 기록
			@SuppressWarnings("unchecked")
			Iterator<SBox> it = ((SBoxList<SBox>) debtInquiryListForExcel.get("debtInquiryList")).iterator();
			int row = 2;

			while (it.hasNext()) {
				SBox data = (SBox) it.next();
				String[] keys = columnListValue.split(",");
				data.set("DOC_CD", data.getString("DOC_CD") + "$C");
				data.set("REG_DT", data.getString("REG_DT") + "$C");
				data.set("CUST_NM", data.getString("CUST_NM") + "$C");
				String custNo = "";
				if (!data.isEmpty("CUST_NO")) {
					custNo = data.getString("CUST_NO");
					custNo = custNo.substring(0, 3) + "-" + custNo.substring(3, 5) + "-" + custNo.substring(5);
				}
				data.set("CUST_NO", custNo + "$C");
				data.set("CUST_OWN_NM", data.getString("CUST_OWN_NM") + "$C");
				data.set("CUST_CORP_NO", data.getString("CUST_CORP_NO") + "$C");
				data.set("ST_DEBT_AMT", data.getString("ST_DEBT_AMT") + "원$R");
				data.set("STAT", data.getString("STAT") + "$C");
				data.set("USR_NM", data.getString("USR_NM") + "$C");
				poi.setBulkColsDataFromSBox(dataStyles, row, 1, data, keys);
				row++;
			}

			poi.write();

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30200").getErrMsg() + "]");
			ex.printStackTrace();
		} finally {
			if (poi != null) {
				try {
					poi.close();
				} catch (IOException e) {
				}
			}
		}
		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 삭제 Service Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2015. 07. 30.
	 * @param debtIdList  : 채무불이행 신청서 삭제 대상 리스트(구분자 ,)
	 * @return sBox : 결과 Object
	 */
	@Override
	@Transactional
	public SBox removeDebt(SBox sBox) {

		SBox result = new SBox();
		try {

			// [1] 채무불이행 첨부파일 삭제할 파일 경로 조회
			SBoxList<SBox> deleteDebtFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDeletedDebtFilePath(sBox.getString("debtIdList")):
																		   debtDaoForMssql.selectDeletedDebtFilePath(sBox.getString("debtIdList"));
			log.i("[1] 채무불이행 삭제할 파일 경로 조회 완료 / 건수[" + deleteDebtFileList.size() + "]");

			SBox paramBox = new SBox();
			paramBox.set("debtApplId", sBox.get("debtIdList"));
			paramBox.set("usrId", sBox.get("sessionUsrId"));
			paramBox.set("searchType", "N");

			// [2] 채무불이행 상태이력 첨부파일 삭제할 파일경로 조회(손화정 프로시져 제작 후 개발할 예정임)
			SBoxList<SBox> deleteDebtStatHistoryList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryList(paramBox):
																				  debtDaoForMssql.selectDebtStatHistoryList(paramBox);
			log.i("[1] 채무불이행 상태이력 첨부파일 삭제할 파일 경로 조회 완료 / 건수[" + deleteDebtStatHistoryList.size() + "]");

			// [2] 채무 불이행 상태값 DE 변경 및 첨부파일, 채권 ROW 삭제
			result = "oracle".equals(dbmsType)? debtDaoForOracle.deleteDebt(paramBox) : debtDaoForMssql.deleteDebt(paramBox);
			if (!"00000".equals(result.getString("REPL_CD"))) {
				throw new BizException("30413", EErrorCodeType.search("30413").getErrMsg());
			}
			log.i("[2] 채무불이행 삭제 성공");

			// [3] 채무불이행 첨부파일 실제 경로 파일 삭제
			Iterator<SBox> it = deleteDebtFileList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				if (!temp.isEmpty("FILE_PATH")) {
					String filePath = debtFilePath + "/" + temp.getString("FILE_PATH");
					CommonUtil.fileDelete(filePath);
				}
			}
			log.i("[3] 채무불이행 물리 파일 경로 삭제 성공");

			// [4] 채무불이행 상태이력 첨부파일 실제 경로 파일 삭제
			it = deleteDebtStatHistoryList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				if (!temp.isEmpty("FILE_PATH")) {
					String filePath = debtStatFilePath + "/" + temp.getString("FILE_PATH");
					CommonUtil.fileDelete(filePath);
				}
			}
			log.i("[4] 채무불이행 물리 파일 경로 삭제 성공");

		} catch (BizException biz) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30199").getErrMsg() + "]");
		}
		return result;
	}

	
	/**
	 * <pre>
	 * 채무불이행 신청페이지 인터페이스 Service Method
	 * </pre>
	 * @author Jung mi Kim
	 * @since 2015. 5. 11.
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getDebtIFConfirmation(SBox sBox) {
		SBox result = new SBox();
		
		try {
			String chgStat = "AW"; // AW : 심사대기(Accept Waiting)
			DateFormat regDtfromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			DateFormat overStDtfromFormat = new SimpleDateFormat("yyyy-MM-dd");
			// 채무불이행(DE_DEBT_APPL) 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox) : debtDaoForMssql.selectDebt(sBox) ;
			debtBox.set("STAT_TXT", EDebtStatType.search(debtBox.getString("STAT")).getText());
			result.set("debtBox", debtBox);
			

			//증빙 파일 조회 
    		//바이너리로 첨부파일 변환,원본으로 돌려보기 작업수행
			SBox paramBox = new SBox();
			sBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));
			paramBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));
			paramBox.set("debtApplSeq", sBox.get("debtApplId"));
			SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
																	debtDaoForMssql.selectDebtFileByCondition(paramBox);
			result.set("prfFileList", prfFileList);
			
			//요청 문서의 instanceId
			String senderId = debtBox.getString("COMP_NO");
			String senderName = debtBox.getString("COMP_NM");
			String instanceId = SysUtil.getInstanceId(serviceCodeCreditRequestApplication,senderId);
					 
					 
			//첨부파일
			SBoxList<FileData>  manifestList = new SBoxList<FileData>();
			// 적하목록 정보 영역
			int numberOfItem = prfFileList.size();
				
			for(int i = 0; i< numberOfItem; i++){
				
				FileData manifestItem = new FileData();
				File singleFile = new File(debtFilePath + prfFileList.get(i).getString("FILE_PATH"));
				FileInputStream fis = new FileInputStream(debtFilePath + prfFileList.get(i).getString("FILE_PATH"));
				manifestItem.setFileSeq(String.valueOf(i+1));// 일련번호
				manifestItem.setFileName(prfFileList.get(i).getString("FILE_NM"));// 실제 파일명
				manifestItem.setFileSize(fis.available());// 실제 파일 크기
				manifestItem.setFileData(CommonUtil.singleFileToString(singleFile));//실제 파일 바이너리 정보
				manifestItem.setFileDesc(prfFileList.get(i).getString("TYPE_CD"));//적하 항목 설명
				manifestList.add(manifestItem);
			}
				 
			//라우팅 VO 생성
				 MessageEvent msgEvent = new MessageEvent();
				 
				 msgEvent.setSenderId(senderId);
				 msgEvent.setSenderName(senderName);
				
				 msgEvent.setReceiveId(receiveId);
				 msgEvent.setReceiveName(receiveName);
				 msgEvent.setResponseType("A");//ASync타입
				 
				 msgEvent.setInstanceId(instanceId);
				 msgEvent.setGroupId(debtBox.getString("DEBT_APPL_ID"));//채무불이행순번
				 msgEvent.setDocumentType(serviceCodeCreditRequestApplication);//고정값
				 msgEvent.setActionType(chgStat); // AW : 심사대기(Accept Waiting)
				 msgEvent.setDocCreationDateTime(DateUtil.datetime());
				 msgEvent.setServiceCode(serviceCodeCred);//CRED 
				 msgEvent.setTypeCode("REQ");
				 msgEvent.setFileCount(numberOfItem);
				 msgEvent.setFileDatas(manifestList);
		
				//1. 채무불이행 등록 메시지 파라미터 생성
				RequestCommonVo  requestVo = new RequestCommonVo();
				 requestVo.setID(instanceId);
				 requestVo.setRequestDocumentTypeCode(serviceCodeCreditRequestApplication);
				 requestVo.setVersionInformation("1.0.0");
				 requestVo.setDescription("크레딧서비스채무불이행등록요청서");
				 requestVo.setUserBusinessAccountTypeCode("E");// ERP회원
				 requestVo.setUserID(sBox.getString("sessionSbusrId")); // sbUserId 스마트채권의 회원식별자
				 requestVo.setLoginID(sBox.getString("sessionSbLoginId")); // sbdebnId 스마트 채권의 아이디
				 requestVo.setLoginPasswordID("123");//아무값이나 들어가도 됨 IF필수아님
				 
				DebtRegisterRequestVo debtReqisterRequstVo = new DebtRegisterRequestVo();
				 debtReqisterRequstVo.setCompUsrId(sBox.getString("sessionSbCompUsrId"));//sbCompUsrId 스마트 채권의 아이디
				//생성일자 날짜포멧 변경
				 String regdateStr = debtBox.getString("REG_DT");
				 
				 //Date regdate = regDtfromFormat.parse(regdateStr); // MSSQL
				 Date regdate = overStDtfromFormat.parse(regdateStr); //ORACLE
				 debtReqisterRequstVo.setRegDt(CommonUtil.getFormatDateTimeMiliSecond(regdate));//debtBox.getDouble("REG_DT")
				 debtReqisterRequstVo.setRegId(sBox.getString("sessionSbusrId")); //debtBox.getString("REG_ID") TODO: 회원정보에서 넘겨받은 회원아이디
				 debtReqisterRequstVo.setTypeCode(debtBox.getString("COMP_TYPE"));
				 debtReqisterRequstVo.setCompNm(senderName);
				 debtReqisterRequstVo.setCompNo(senderId);
				 if(!debtBox.getString("CORP_NO").isEmpty()){
					 debtReqisterRequstVo.setCorpNo(debtBox.getString("CORP_NO"));
				 }
				 debtReqisterRequstVo.setBizType(debtBox.getString("BIZ_TYPE"));
				 debtReqisterRequstVo.setOwnName(debtBox.getString("OWN_NM"));
				 debtReqisterRequstVo.setUserNm(debtBox.getString("USR_NM"));
				 debtReqisterRequstVo.setDeptNm(debtBox.getString("DEPT_NM"));
				 debtReqisterRequstVo.setJobTlNm(debtBox.getString("JOB_TL_NM"));
				 debtReqisterRequstVo.setTelNo(debtBox.getString("TEL_NO"));
				 debtReqisterRequstVo.setMbNo(debtBox.getString("MB_NO"));
				 debtReqisterRequstVo.setFaxNo(debtBox.getString("FAX_NO"));
				 debtReqisterRequstVo.setEmail(debtBox.getString("EMAIL"));
				 debtReqisterRequstVo.setPostNo(debtBox.getString("POST_NO"));
				 debtReqisterRequstVo.setAdressOne(debtBox.getString("ADDR_1"));
				 debtReqisterRequstVo.setAdressTwo(debtBox.getString("ADDR_2"));
				 debtReqisterRequstVo.setCustType(debtBox.getString("CUST_TYPE"));
				 debtReqisterRequstVo.setCustNm(debtBox.getString("CUST_NM"));
				 debtReqisterRequstVo.setCustNo(debtBox.getString("CUST_NO"));
				 if(!debtBox.getString("CUST_CORP_NO").isEmpty()){
					 debtReqisterRequstVo.setCustCorpNo(debtBox.getString("CUST_CORP_NO"));
				 }
				 debtReqisterRequstVo.setCustOwnNm(debtBox.getString("CUST_OWN_NM"));
				 debtReqisterRequstVo.setCustPostNo(debtBox.getString("CUST_POST_NO"));
				 debtReqisterRequstVo.setCustAddrOne(debtBox.getString("CUST_ADDR_1"));
				 debtReqisterRequstVo.setCustAddrTwo(debtBox.getString("CUST_ADDR_2"));
				 debtReqisterRequstVo.setDebtType(debtBox.getString("DEBT_TYPE"));
				 debtReqisterRequstVo.setDebtCompType("E");
				//연체개시일자 날짜포멧 변경 
				 String overStdateStr = debtBox.getString("OVER_ST_DT");
				 Date overStdate = overStDtfromFormat.parse(overStdateStr);
					
				 debtReqisterRequstVo.setOverStDt(CommonUtil.getFormatDate(overStdate));//debtBox.getDouble("OVER_ST_DT")연체개시일자(yyyyMMdd)
				 debtReqisterRequstVo.setStDebtAmt(debtBox.getInt("ST_DEBT_AMT_IF"));
				 
				//2. XML메시지 생성
				 String strXml = null;
				 strXml = CommonMessageIF.makeCreditRequestRegisterDefaultApplicationForXML(requestVo, debtReqisterRequstVo ); 
				
				//실 XML추가
				msgEvent.setDocumentData(strXml);
				wsClient.remoteASyncMessageCall(msgEvent);
				
				
				SBox snedResultSBox = new SBox();
				snedResultSBox.set("debtApplId", debtBox.getString("DEBT_APPL_ID")); //GroupId
				snedResultSBox.set("chgStat", chgStat); //ActionTyp
				snedResultSBox.set("rmkTxt", null);
				snedResultSBox.set("trans", "2"); //0:실패/ 1:성공/ 2:전송중
				snedResultSBox.set("rcode",null);
				snedResultSBox.set("reason","인터페이스 전송중 상태입니다.");
				snedResultSBox.set("sessionUsrNm", "SYSTEM");
				snedResultSBox.set("sessionUsrId", "0");
				snedResultSBox.set("docYn", "N");
				snedResultSBox.set("docCd", null);
				snedResultSBox.set("debtStatSubCd", null);
	        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(snedResultSBox):
	        										 		debtDaoForMssql.updateDebtStatSendEndForWS(snedResultSBox);
				String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
				
				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
				}
				 
		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30500").getErrMsg() + "]");
		}

		return result;
	}
	
	
	
		
	/**
	 * <pre>
	 * 채무불이행 등록 신청서 , 거래처 KED 등록  I/F 실패   Result  NoticeConclusion
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public void getIFResultForNoticeConclusionType(MessageTag messageTag){
		
		SBox xmlResult = null;
		SBox paramBox = new SBox();
		try{
			xmlResult = CommonMessageIF.getNoticeConclusionForXML(messageTag.getDocumentData());
            
			if(messageTag.getReferenceId()!=null){
            	if(messageTag.getReferenceId().substring(0, 6).equals("CRQREC")){ // groupId가 존재하지 않고, ReferenceId가 조기경보대상 거래처등록이면
	                
	                xmlResult.set("custNo", messageTag.getGroupId()); 
	                xmlResult.set("rcode",xmlResult.get("typeCode"));//0:실패/ 1:성공
	            	xmlResult.set("reason",xmlResult.get("description"));		
	            	
	                // 파라미터 생성
	    			paramBox.set("custNo", xmlResult.getString("custNo"));
	    			paramBox.set("sessionCompUsrId","5" );//sBox.getString("sessionCompUsrId") 추후에 조회해서 넘겨야함
	    			paramBox.set("compMngCd","ERP1" );//sBox.getString("compMngCd")
	    			paramBox.set("trans", xmlResult.getString("rcode")); //처리 성공, 실패 여부. 0: 실패, 1: 성공 , 2: 전송중
	            	paramBox.set("reason", xmlResult.getString("reason"));
	            	paramBox.set("sessionUsrId", "0");
	            	SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(paramBox)
	            											   : customerDaoForMssql.updateCustResponseForIF(paramBox);
	    			
	    			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
	    				throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
	    			}
            	}else{
                	paramBox.set("debtApplId", messageTag.getGroupId()); //GroupId
   				 if("0".equals(xmlResult.get("typeCode"))){ //실패할경우
   					/*if("AW".equals(messageTag.getActionType())){
   						paramBox.set("chgStat", "SC"); //ActionTyp I/F시  actionType이 null이 들어옴 문제해결해야함
   					}else if("FA".equals(messageTag.getActionType())){ //등록전 민원발생 추가증빙(FA) IF실패일경우 등록전 민원발생(CA)로 변경
   						paramBox.set("chgStat", "CA"); 
   					}else if("FB".equals(messageTag.getActionType())){ //등록전 민원발생 추가증빙(FB) IF실패일경우 등록전 민원발생(CB)로 변경
   						paramBox.set("chgStat", "CB"); 
   					}else{
   						//이전상태의 HIST를 조회한다
   						paramBox.set("chgStat", messageTag.getActionType());
   					}*/
   				}else{ //성공한경우
   					//TODO NTCCON을 호출하는 인터페잉스 대상을 조사하여 분기 처리해야함
   					paramBox.set("chgStat", messageTag.getActionType());
   				}
   				 
   				 	//paramBox.set("rmkTxt", "");
   		        	paramBox.set("trans", xmlResult.get("typeCode")); //0:실패/ 1:성공
   		        	paramBox.set("rcode",xmlResult.get("responseTypeCode"));
   		        	paramBox.set("reason",xmlResult.get("description"));
   		        	paramBox.set("sessionUsrNm", "SYSTEM");
   		        	paramBox.set("sessionUsrId", "0");
   		        	paramBox.set("docYn", "N");
   		        	//paramBox.set("docCd", "");
   		        	//paramBox.set("debtStatSubCd", ""); 
   				 
   				if("R".equals(messageTag.getActionType())){
   					
   					SBox sdebtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(paramBox):
   															  debtDaoForMssql.selectDebt(paramBox);
   					paramBox.set("modStat", messageTag.getActionType());
   					paramBox.set("debtAmt", !"0".equals(sdebtBox.getString("DEBT_AMT").replaceAll(",", "")) ? (sdebtBox.getString("DEBT_AMT").replaceAll(",", "")) : sdebtBox.get("DEBT_AMT"));
   					
   					SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtModHistForIF(paramBox):debtDaoForMssql.updateDebtModHistForIF(paramBox);
   					
   					
   					if (!"00000".equals(updateBox.getString("REPL_CD"))) {
   						throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
   					}
   					log.i("채무불이행 정정 요청 이력 추가 응답 결과 : REPL_CD[" + updateBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg() + "]");
   	
   				}else{
   					
   		        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox):
   		        												debtDaoForMssql.updateDebtStatSendEndForWS(paramBox);
   					String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
   					
   					if (!"00000".equals(updateBox.getString("REPL_CD"))) {
   						throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
   					}
   				}
               }
        	}
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31000").getErrMsg() + "]");
		}
	}
	
	/**
	 * 
	 * <pre>
	 * 채무불이행 등록 신청서 I/F 성공 Result  - CreditResponseRegisterDefaultApplication
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public void getIFResultForSucessResponse(MessageTag messageTag){
		try{
	        
	        SBox xmlResult = CommonMessageIF.getCreditResponseRegisterDefaultApplicationForXML(messageTag.getDocumentData());
	        SBox paramBox = new SBox();
			paramBox.set("debtApplId", messageTag.getGroupId()); //GroupId
	        if("1".equals(xmlResult.getString("typeCode"))){ //0:실패 | 1: 성공일경우 
	        	paramBox.set("chgStat", "AW"); //messageTag.getActionType()
	        }else{
	        	log.e("채무불이행 상태변경 신청시 IF가 정상적으로 이루어지지 않았습니다 : CreditResponseRegisterDefaultApplication");
	        }
	    	paramBox.set("rmkTxt", null);
	    	paramBox.set("trans", xmlResult.getString("typeCode")); //2:저장완료
	    	paramBox.set("rcode", xmlResult.getString("responseTypeCode"));
	    	paramBox.set("reason",xmlResult.getString("description"));
	    	paramBox.set("docCd", xmlResult.getString("id"));
	    	paramBox.set("docYn", "Y");
	    	paramBox.set("sessionUsrNm", "SYSTEM");
	    	paramBox.set("sessionUsrId", "0");
	    	paramBox.set("debtStatSubCd", null);
	    	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox):
	    												debtDaoForMssql.updateDebtStatSendEndForWS(paramBox);
			String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
			}
		
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31000").getErrMsg() + "]");
		}
	}
	
	

	
	/**
	 * <pre>
	 * 채무불이행 신청서 상세페이지 조회 Service Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 05. 01.
	 * @param debtApplId : 채무불이행 순번
	 * @return sBox : 결과 Object
	 */
	@Override
	public SBox getDebtInquiry(SBox sBox) {
		SBox result = new SBox();

		try {
			SBox paramBox = new SBox();
			SBox commonCodeBox = new SBox();

			// [1] PARAMETER 초기화
			sBox.setIfEmpty("custType", "C");

			// [2] 공통코드 초기화(전화번호, 이메일, 핸드폰번호, 정렬조건, 정렬순서)
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("npmOrderConditionTypeList", CommonCode.npmOrderConditionTypeList);
			commonCodeBox.set("orderTypeList", CommonCode.orderTypeList);

			result.set("commonCodeBox", commonCodeBox);

			paramBox.set("sessionCompUsrId", null);
			paramBox.set("grantCode", null);
			paramBox.set("mbrStat", null);
			paramBox.set("usrId", sBox.get("sessionUsrId"));

			// 채무불이행(DE_DEBT_APPL) 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):
													  debtDaoForMssql.selectDebt(sBox);
			result.set("debtBox", debtBox);

			// 채권담당자 조회를 위한 파라미터 설정함
			paramBox.set("sessionCompUsrId", sBox.get("sessionCompUsrId"));
			paramBox.set("mbrStat", "N");
			paramBox.set("usrId", null);

			// 채권 담당자 후보군 리스트 검색
			//result.set("debentureContactList", debtDao.selectMbrEmployeeListByCondition(paramBox));

			// 미수채권 리스트
			//sBox.set("custId", debtBox.get("CUST_ID"));
			
			//SBoxList<SBox> debentureList = getDebentureListOfCustomer(sBox).get("debentureList");
			//result.set("debentureList", debentureList);

			// 미수채권 관련 파일 리스트 조회
			sBox.set("debtApplSeq", sBox.getString("debtApplId"));
			SBoxList<SBox> debentureFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(sBox):
																	      debtDaoForMssql.selectDebtFileByCondition(sBox);
			result.set("debentureFileList", debentureFileList);

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}
	
	
	/**
	 * <pre>
	 * 채무불이행 작성완료 접수문서 상세보기 조회 Service Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 9.
	 * @param debtApplId : 채무불이행신청서 순번
	 * @return
	 */
	@Override
	public SBox getDebtCompleteInquiry(SBox sBox) {
		SBox result = new SBox();

		try {

			// [1] 채무불이행(DE_DEBT_APPL) 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):
												      debtDaoForMssql.selectDebt(sBox);
			debtBox.set("STAT_TXT", EDebtStatType.search(debtBox.getString("STAT")).getText());

			result.set("debtBox", debtBox);

			// 미수 채권 정보 조회
			SBox paramBox = new SBox();
			paramBox.set("custId", debtBox.get("CUST_ID"));
			paramBox.set("orderCondition", "REG_DT");
			paramBox.set("orderType", "ASC");
			paramBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));

			//result.set("debentureList", debtDao.selectDebentureList(paramBox));

			// [2] 상태 이력 조회
			if (!"TS".equals(debtBox.getString("STAT")) && !"SC".equals(debtBox.getString("STAT"))) {
				// TODO KED, NICE 등 회사 고려는 하지 않고 STAT과 STAT_SUBCODE로만 이력을 조회함
				result.set("debtStat", "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryForRecentStatus(paramBox.getString("debtApplId"))
																: debtDaoForMssql.selectDebtStatHistoryForRecentStatus(paramBox.getString("debtApplId"))
						);
			}
			
			// [3] 사유 관리 리스트 조회
			paramBox.setIfEmpty("num", "1");
			paramBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			paramBox.setIfEmpty("searchType", "L"); // 검색타입 : L(리스트), N(리스트아님)
			
			int histStatCnt = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryTotalCount(paramBox.getString("debtApplId")):
														 debtDaoForMssql.selectDebtStatHistoryTotalCount(paramBox.getString("debtApplId"));

			SBoxList<SBox> histStatusList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryList(paramBox):
																	   debtDaoForMssql.selectDebtStatHistoryList(paramBox);
			result.set("histStatusList", histStatusList);
			
			Iterator<SBox> it = histStatusList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("CH_STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			String pcPage = commonPage.getPcAjaxPagingPrint(histStatCnt, paramBox.getInt("num"), paramBox.getInt("rowSize"), "getPage");
			result.set("pcPage", pcPage); // 채무불이행 페이징

			// [4] 채무불이행 상태 이력 조회
			SBox debtProcess = this.getDebtStatProcess(sBox.getString("debtApplId"));
			result.set("debtProcess", debtProcess);

			// [5] 증빙 파일 조회
			paramBox.set("debtApplSeq", paramBox.get("debtApplId"));

			SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
														            debtDaoForMssql.selectDebtFileByCondition(paramBox);
			result.set("prfFileList", prfFileList);

			// [6] 안내사항 파일 읽기
			StringBuffer informationBriefing = CommonUtil.fileRead(debtTxt + "/" + "debtInformationBriefing.txt");
			result.set("informationBriefing", informationBriefing.toString());

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 진행상태 조회 Biz Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 21.
	 * @param debtApplId
	 *            : 채무불이행 순번
	 * @return 채무불이행 상태값 6단계
	 */
	@Override
	public SBox getDebtStatProcess(String debtApplId) {
		SBox debtProcess = null;
		try {
			// 채무불이행 상태 이력 조회
			debtProcess = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtProcess(debtApplId): debtDaoForMssql.selectDebtProcess(debtApplId);

			String step1 = "", step2 = "", step3 = "", step4 = "", step5 = "", step6 = "";
			if (!debtProcess.isEmpty("STEP1")) {
				step1 = (EDebtStatType.search(debtProcess.getString("STEP1")) != null) ? EDebtStatType.search(debtProcess.getString("STEP1")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP1")).getDesc();
			}
			if (!debtProcess.isEmpty("STEP2")) {
				step2 = (EDebtStatType.search(debtProcess.getString("STEP2")) != null) ? EDebtStatType.search(debtProcess.getString("STEP2")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP2")).getDesc();
			}
			if (!debtProcess.isEmpty("STEP3")) {
				step3 = (EDebtStatType.search(debtProcess.getString("STEP3")) != null) ? EDebtStatType.search(debtProcess.getString("STEP3")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP3")).getDesc();
			}
			if (!debtProcess.isEmpty("STEP4")) {
				step4 = (EDebtStatType.search(debtProcess.getString("STEP4")) != null) ? EDebtStatType.search(debtProcess.getString("STEP4")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP4")).getDesc();
			}
			if (!debtProcess.isEmpty("STEP5")) {
				step5 = (EDebtStatType.search(debtProcess.getString("STEP5")) != null) ? EDebtStatType.search(debtProcess.getString("STEP5")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP5")).getDesc();

			}
			if (!debtProcess.isEmpty("STEP6")) {
				step6 = (EDebtStatType.search(debtProcess.getString("STEP6")) != null) ? EDebtStatType.search(debtProcess.getString("STEP6")).getDesc() : EDebtOtherStatType.search(
						debtProcess.getString("STEP6")).getDesc();
			}

			debtProcess.set("STEP1", step1);
			debtProcess.set("STEP2", step2);
			debtProcess.set("STEP3", step3);
			debtProcess.set("STEP4", step4);
			debtProcess.set("STEP5", step5);
			debtProcess.set("STEP6", step6);

			debtProcess.setIfEmpty("STEP1_DT", "");
			debtProcess.setIfEmpty("STEP2_DT", "");
			debtProcess.setIfEmpty("STEP3_DT", "");
			debtProcess.setIfEmpty("STEP4_DT", "");
			debtProcess.setIfEmpty("STEP5_DT", "");
			debtProcess.setIfEmpty("STEP6_DT", "");

		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}
		return debtProcess;
	}

	/**
	 * <pre>
	 * 특정 거래처의 이미 진행중인 채무불이행이 있는지 조회하는 Service method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 13.
	 * @param sessionUsrId
	 *            : 개인 회원 순번, sessionCompUsrId : 기업회원순번, npmCustId : 거래처 순번
	 * @return
	 */
	@Override
	public SBox getUnderWayDebtTotalCount(String sessionUsrId, String sessionCompUsrId, String npmCustId) {
		SBox result = new SBox();

		try {
			SBox paramBox = new SBox();
			npmCustId=npmCustId.replaceAll("-", "");
			paramBox.set("orderCondition", "REG_DT");
			paramBox.set("orderType", "ASC");
			paramBox.set("sessionUsrId", sessionUsrId);
			paramBox.set("sessionCompUsrId", sessionCompUsrId);
			paramBox.set("npmCustId",npmCustId);
			int temp = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtTotalCount(paramBox) : debtDaoForMssql.selectDebtTotalCount(paramBox);
			
			if (temp > 0) {
				result.set("REPL_CD", "30414");
				result.set("REPL_MSG", EErrorCodeType.search("30414").getErrMsg());
			} else {
				result.set("REPL_CD", "00000");
				result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			}

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 진행 중 접수문서 상세페이지 조회 Service Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 09.
	 * @param debtApplId : 채무불이행 순번
	 * @return sBox : 결과
	 * 
	 */
	@Override
	public SBox getDebtOngoingInquiry(SBox sBox) {
		SBox result = new SBox();

		try {

			sBox.setIfEmpty("num", "1");
			sBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			sBox.setIfEmpty("searchType", "L"); // 검색타입 : L(리스트), N(리스트아님)

			// [1] 사유관리 리스트 조회
			int histStatCnt = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryTotalCount(sBox.getString("debtApplId")) : 
														 debtDaoForMssql.selectDebtStatHistoryTotalCount(sBox.getString("debtApplId"));

			SBoxList<SBox> histStatusList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryList(sBox) :
																	   debtDaoForMssql.selectDebtStatHistoryList(sBox);
			result.set("histStatusList", histStatusList);

			Iterator<SBox> it = histStatusList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("CH_STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			String pcPage = commonPage.getPcAjaxPagingPrint(histStatCnt, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");
			result.set("pcPage", pcPage); // 채무불이행 페이징

			// [2] 채무 불이행 정정 관리 리스트 조회
			SBoxList<SBox> modHistList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtModHistoryList(sBox.getString("debtApplId")):
																    debtDaoForMssql.selectDebtModHistoryList(sBox.getString("debtApplId"));
			result.set("modHistList", modHistList);

			SBox paramBox = new SBox();

			paramBox.set("sessionCompUsrId", null);
			paramBox.set("grantCode", null);
			paramBox.set("mbrStat", null);
			paramBox.set("usrId", sBox.get("sessionUsrId"));

			// [3] 채무불이행(DE_DEBT_APPL) 정보 조회
			SBox pyBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox): debtDaoForMssql.selectDebt(sBox);
			pyBox.set("CH_STAT", EDebtStatType.search(pyBox.getString("STAT")).getDesc());
			pyBox.set("STAT_DESC", EDebtStatType.search(pyBox.getString("STAT")).getText());
			result.set("debtBox", pyBox);

			// [4] 미수채권 리스트 조회
			sBox.set("custId", pyBox.get("CUST_ID"));
			
			//SBoxList<SBox> debentureList = getDebentureListOfCustomer(sBox).get("debentureList");
			//result.set("debentureList", debentureList);

			// [5] 거래증빙서류 리스트 조회
			sBox.set("debtApplSeq", sBox.getString("debtApplId"));
			SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(sBox)
															       :debtDaoForMssql.selectDebtFileByCondition(sBox);
			result.set("prfFileList", prfFileList);

			// [6] 안내사항 파일 읽기
			StringBuffer informationBriefing = CommonUtil.fileRead(debtTxt + "/" + "debtInformationBriefing.txt");
			result.set("informationBriefing", informationBriefing.toString());

			// [7] 채무불이행 상태 이력 조회
			SBox debtProcess = this.getDebtStatProcess(sBox.getString("debtApplId"));
			result.set("debtProcess", debtProcess);
			result.set("num", sBox.get("num"));
			result.set("REPL_CD", "00000");

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행 진행 중 접수문서 상세페이지 조회 Service Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 09.
	 * @param
	 * @return sBox : 결과
	 * 
	 */
	@Override
	public SBox getDebtOngoingStatHistoryList(SBox sBox) {
		SBox result = new SBox();

		try {

			sBox.setIfEmpty("num", 1);
			sBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			sBox.setIfEmpty("searchType", "L"); // 검색타입 : L(리스트), N(리스트아님)

			// [1] 사유관리 리스트 조회
			int histStatCnt = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryTotalCount(sBox.getString("debtApplId")):
														 debtDaoForMssql.selectDebtStatHistoryTotalCount(sBox.getString("debtApplId"));

			SBoxList<SBox> histStatusList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryList(sBox):
																       debtDaoForMssql.selectDebtStatHistoryList(sBox);
			result.set("histStatusList", histStatusList);

			Iterator<SBox> it = histStatusList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("CH_STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			String pcPage = commonPage.getPCPagingPrint(histStatCnt, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");
			result.set("pcPage", pcPage); // 채무불이행 페이징

			result.set("REPL_CD", "00000");

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 * 추가증빙서류 첨부 Service Method
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2014. 05. 10.
	 * @param debtApplSeq : 채무불이행순번, sessionUsrId : 등록자 순번, write_explain : 비고, dataUpload : 채무불이행 증빙파일, chFileNm : 저장할 파일명
	 * @return
	 */
	@Override
	//@Transactional
	public SBox addDeptOngoingAppl(SBox sBox) {
		SBox result = new SBox();
		try {

			String debtApplSeq = sBox.get("debtApplSeq");
			SBox paramBox = new SBox();
			
			// [1] 파일 사이즈 체크
			if (sBox.get("dataUpload") != null) {
				List<MultipartFile> files = sBox.get("dataUpload");
				for (MultipartFile file : files) {
					if ((file.getOriginalFilename() != null && !"".equals(file.getOriginalFilename())) && file.getSize() == 0) {
						throw new BizException("30505", EErrorCodeType.search("30505").getErrMsg());
					}
				}
			}
			
			// 증빙파일 등록후 상태값 변경
			if ("CA".equals(sBox.getString("currentStat"))) {
				//paramBox.set("stat", "FA"); // 상태: 등록전 민원발생(추가증빙) TODO 디버깅후  둘중하나 삭제
				paramBox.set("chgStat", "FA"); // 상태: 등록전 민원발생(추가증빙)
			} else if ("CB".equals(sBox.getString("currentStat"))) {
				//paramBox.set("stat", "FB"); // 상태: 등록후 민원발생(추가증빙) TODO 디버깅후  둘중하나 삭제
				paramBox.set("chgStat", "FB"); // 상태: 등록후 민원발생(추가증빙)
			}
			//result.set("currentStat", paramBox.getString("stat"));// TODO 디버깅후  둘중하나 삭제
			result.set("currentStat", paramBox.getString("chgStat"));

			paramBox.set("debtApplSeq", debtApplSeq); // 채무불이행신청순번
			paramBox.set("usrId", sBox.get("sessionUsrId")); // 등록자 순번
			String rmkTxt = (sBox.isEmpty("write_explain"))?null:sBox.getString("write_explain") ;
//			paramBox.set("rmk_txt", rmkTxt); // 사유  TODO 디버깅후  둘중하나 삭제
			paramBox.set("rmkTxt", rmkTxt); // 사유  
			
			// [2] 채무불이행 신청 밒 채무 불이행 상태 이력 UPDATE
			/*SBox modiDebtBox = debtDao.insertDebtStatus(paramBox);
			String debtStatHistId = modiDebtBox.getString("DEBT_STAT_HIST_ID");*/

			// [3] 채무불이행 상태 이력 증빙파일 디렉토리 생성
			File debtStatDir = new File(debtStatFilePath);
			if (!debtStatDir.isDirectory()) {
				debtStatDir.mkdir();
			}
			
			File debtFileDir = new File(debtStatFilePath + "/" + debtApplSeq);
			if (!debtFileDir.isDirectory()) {
				if(debtFileDir.mkdir()) {
					log.i("[3] 채무불이행신청서 상태이력 증빙파일 디렉토리 생성[ 경로 : "+ debtStatFilePath + "/" + debtApplSeq +" ]");
				}
				
			}
			
			SBoxList<SBox> debtFileList = new SBoxList<SBox>();

			// [4] 파일 정보 저장
			if (sBox.get("dataUpload") != null) {
				List<MultipartFile> files = sBox.get("dataUpload");
				for (MultipartFile file : files) {
					if (file.getOriginalFilename() != null && file.getSize() > 0) {
						SBox fileBox = new SBox();
						fileBox.set("file", file); // 실제 파일이 들어있는 곳
						fileBox.set("fileNm", file.getOriginalFilename());
						fileBox.set("debtApplSeq", sBox.get("debtApplSeq"));
						fileBox.set("fileBinary", CommonUtil.fileToString(file)); // 실제 파일이 들어있는 곳
						debtFileList.add(fileBox);
					}
				}
			}
			
			sBox.set("debtApplId", debtApplSeq);
			sBox.set("searchType", "L");
			sBox.set("rowSize", "10");
			sBox.set("MOD_STAT", paramBox.get("chgStat"));
			sBox.set("RMK_TXT", paramBox.get("rmkTxt"));
			String debtStatHistId = CommonAmendRequest(sBox);
				
			// [5] 파일 업로드 등록
			Iterator<SBox> fileIT = debtFileList.iterator();
			int i = 0;
			while (fileIT.hasNext()) {
				SBox fileBox = (SBox) fileIT.next();

				if (!fileBox.isEmpty("fileNm")) {
					if (sBox.get("chFileNm") != null) {
						if (sBox.getString("chFileNm").split(",") instanceof String[]) {
							// [5-1] 파일 명 변경
							String chgFileNM[] = sBox.getString("chFileNm").split(",");
							fileBox.set("ch_fileNm", chgFileNM[i]);
						} else {
							String chgFileNM = sBox.get("chg_dataUploadType");
							fileBox.set("ch_fileNm", chgFileNM);
						}
					}
					// [5-2] param setting
					if (!fileBox.isEmpty("ch_fileNm")) {
						String[] fileNm = CommonUtil.separateFileName(fileBox.getString("ch_fileNm"));
						fileBox.set("filePath", debtApplSeq + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (i+1) + "." + fileNm[1]);
					}
					fileBox.set("debtStatHistId", debtStatHistId);
					fileBox.set("debtApplSeq", debtApplSeq);

					// [5-2] 추가증빙파일 저장
					SBox fileInsertResult = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtStatFile(fileBox):
																	   debtDaoForMssql.insertDebtStatFile(fileBox);

					if ("00000".equals(fileInsertResult.getString("REPL_CD"))) {
						File saveFile = new File(debtStatFilePath + "/" + fileBox.getString("filePath"));
						MultipartFile file = fileBox.get("file");
						file.transferTo(saveFile);
					}

					log.i("채무불이행 진행 중 추가 증빙 서류 첨부  INSERT 결과코드 : " + fileInsertResult.getString("REPL_CD") + " / 파일명 : " + fileBox.getString("fileNm") + " / 변경 파일명 : " + fileBox.getString("filePath"));
					result.setIfEmpty("REPL_CD", fileInsertResult.getString("REPL_CD"));

				}
				i++;
				}
				
				 
				// [6] 사유관리 리스트 조회
					int histStatCnt = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryTotalCount(sBox.getString("debtApplSeq")):
																 debtDaoForMssql.selectDebtStatHistoryTotalCount(sBox.getString("debtApplSeq"));

					SBoxList<SBox> histStatusList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtStatHistoryList(sBox):
																			   debtDaoForMssql.selectDebtStatHistoryList(sBox);
					
					if(histStatusList.size()!=0){
						Iterator<SBox> it = histStatusList.iterator();
						while (it.hasNext()) {
							SBox temp = (SBox) it.next();
							temp.set("CH_STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
						}
						
						result.set("histStatusList",histStatusList);
					}
					
					
					String pcPage = commonPage.getPcAjaxPagingPrint(histStatCnt, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");
					result.set("pcPage", pcPage); // 채무불이행 페이징

					result.set("debtStatHistId", debtStatHistId );
					result.set("recentStatus", EDebtStatType.search(result.getString("currentStat")).getDesc());

					// [7] 채무불이행 상태 이력 조회
					SBox debtProcess = this.getDebtStatProcess(sBox.getString("debtApplId"));
					result.set("debtProcess", debtProcess);

					result.set("REPL_CD", "00000");
					result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());
					
			
		} catch (BizException biz) {
			result.set("REPL_CD", biz.getErrCode());
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			result.set("REPL_CD", "30499");
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());

			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}
	


	/**
	 * <pre>
	 * 채무불이행 정정 요청 팝업페이지 조회 Service Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번
	 * @return result : 채무불이행 정정 요청 정보
	 */
	@Override
	public SBox getDebtRequireModify(SBox sBox) {

		SBox result = new SBox();

		try {

			// [1] 정정 요청일 오늘 날짜 조회
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date currentTime = new Date();
			String currentDt = format.format(currentTime);

			// [2] 채무불이행 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):debtDaoForMssql.selectDebt(sBox);

			// [3] 결과 값 셋팅
			result.set("currentDt", currentDt); // 오늘날짜
			result.set("debt", debtBox);
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30200").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 *   채무불이행 정정요청 Controller Method CreditRequestAmendDefault
	 * </pre>
	 * 
	 * @author Jung Mi Kim
	 * @since 2015. 5. 10.
	 * @param sBox   debtApplId : 채무불이행 순번, debtAmt : 정정 요청 금액, modStat : 채무금액정정상태, rmkTxt : 정정사유 sessionUsrId : 회원 순번, sessionAdmYn : 관리자 여부
	 * @return
	 */
	@Override
	@Transactional
	public SBox modifyDebtRequire(SBox sBox) {

		SBox resultBox = new SBox();
		try {
			// [1] 채무불이행 정정 금액, 정정 상태 수정
			resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtApplForRequireModify(sBox):
												   debtDaoForMssql.updateDebtApplForRequireModify(sBox);

			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("채무불이행 정정 요청 금액 수정 응답 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");

			sBox.set("trans", "2"); //2:저장완료
			sBox.set("rcode",null);
			sBox.set("reason","인터페이스 전송중 상태입니다.");
			// [2] 채무불이행 정정 요청 이력 정보 추가

			resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtModHist(sBox):debtDaoForMssql.insertDebtModHist(sBox);

			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("채무불이행 정정 요청 이력 추가 응답 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");

			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			
			//I/F시작
			sBox.setIfEmpty("RMK_TXT", sBox.get("rmkTxt"));
			sBox.setIfEmpty("MOD_STAT", sBox.get("modStat"));
			sBox.setIfEmpty("debtApplSeq", sBox.get("debtApplId"));
			CommonAmendRequest(sBox);
			
            
		} catch (BizException biz) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", "31001");
			resultBox.set("REPL_MSG", EErrorCodeType.search("31001").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31001").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	/**
	 * <pre>
	 * 상태변경을 위한 로직
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 1.
	 * @version 1.0
	 * @param sBox
	 */
	public String CommonAmendRequest(SBox sBox){
		String debtStatHistId = null;
		try {
			//1. sBox의  debtApplId로 조회하여 현재상태의 정보를 가지고 온다. 
			//TODO : sBox에 들어있어야하는 파라미터 debtApplId, sessionCompUsrId
			SBox debtbasicInfoSBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):debtDaoForMssql.selectDebt(sBox); 
		
			 String senderId = debtbasicInfoSBox.getString("COMP_NO");
			 String senderName = debtbasicInfoSBox.getString("COMP_NM");
			 String instanceId = SysUtil.getInstanceId(serviceCodeCreditRequestAmend,senderId);
			 
			SBoxList<FileData>  manifestList = new SBoxList<FileData>(); 
			int numberOfItem = 0;
			
		//첨부파일 TODO: 첨부파일 검색하는 프로시져 확인필요 
		String	modStat = sBox.getString("MOD_STAT");
			 
		if("FA".equals(modStat)||"FB".equals(modStat)){
			// 적하목록 정보 영역
			SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(sBox):
																	debtDaoForMssql.selectDebtFileByCondition(sBox);
			numberOfItem = prfFileList.size();
			
			for(int i = 0; i< numberOfItem; i++){
				FileData manifestItem = new FileData();
				File singleFile = new File(debtFilePath + prfFileList.get(i).getString("FILE_PATH"));
				FileInputStream fis = new FileInputStream(debtFilePath + prfFileList.get(i).getString("FILE_PATH"));
				manifestItem.setFileSeq(String.valueOf(i+1));// 일련번호
				manifestItem.setFileName(prfFileList.get(i).getString("FILE_NM"));// 실제 파일명
				manifestItem.setFileSize(fis.available());// 실제 파일 크기
				manifestItem.setFileData(CommonUtil.singleFileToString(singleFile));//실제 파일 바이너리 정보
				manifestItem.setFileDesc(prfFileList.get(i).getString("TYPE_CD"));//적하 항목 설명
				manifestList.add(manifestItem);
			}
		}
			 
		//라우팅 VO 생성
			 MessageEvent msgEvent = new MessageEvent();
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//ASync타입
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setGroupId(sBox.getString("debtApplSeq"));//sBox.getString("DEBT_APPL_ID"));//채무불이행순번
			 msgEvent.setDocumentType(serviceCodeCreditRequestAmend);//고정값
			 msgEvent.setActionType(sBox.getString("MOD_STAT"));
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED TODO:Propertie로 변경
			 msgEvent.setTypeCode("REQ");
			 msgEvent.setFileCount(numberOfItem);
			 msgEvent.setFileDatas(manifestList);
			
			
			//1. 채무불이행 정정  메시지 파라미터 생성
			RequestCommonVo  commonVo = new RequestCommonVo();
			 commonVo.setID("크레딧서비스채무불이행등록변경요청서");
			 commonVo.setRequestDocumentTypeCode(serviceCodeCreditRequestAmend);
			 commonVo.setVersionInformation("1.0.0");
			 commonVo.setDescription(null);
			 commonVo.setUserBusinessAccountTypeCode("E");// ERP회원
			 commonVo.setUserID(sBox.getString("sessionSbusrId")); //usrId 스마트채권의 회원식별자 TODO
			 commonVo.setLoginID(sBox.getString("sessionSbLoginId")); //sbdebnId 스마트 채권의 아이디 TODO
			 commonVo.setLoginPasswordID(null);
		
		
			DebtAmendRequestVo amendVo = new DebtAmendRequestVo();
			 amendVo.setDocCd(debtbasicInfoSBox.getString("DOC_CD"));// DE_DEBT_APPL.DOC_CD
			 amendVo.setUpdDt(DateUtil.datetime());//DE_DEBT_APPL.UPD_DT
			 amendVo.setUpdId(sBox.getString("sessionSbusrId")); //sBox.getString("UPD_ID") DE_DEBT_APPL.UPD_ID
			 amendVo.setModStat(sBox.getString("MOD_STAT")); //DE_DEBT_MOD_HIST.STAT 
			 amendVo.setUsrNm(debtbasicInfoSBox.getString("USR_NM"));// 필수값아니라 null표기 DE_DEBT_STAT_HIST.USR_NM
			 amendVo.setRmkTxt(sBox.getString("RMK_TXT")); //DE_DEBT_STAT_HIST.RMK_TXT 
	        //연체정보
			 //TODO: 파리미터 확인해야함 채무불이행 수정인경우 금액을 담아야함
			 if("R".equals(sBox.getString("MOD_STAT"))){
				 amendVo.setDebtAmt(sBox.getInt("debtAmt")); //DE_DEBT_MOD_HIST.DEBT_AMT
			 }
			 
			//2. XML메시지 생성
			 String strXml = null;
			 strXml = CommonMessageIF.makeCreditRequestAmendDefaultForXML(commonVo,amendVo ); 
			//실 XML추가
			 msgEvent.setDocumentData(strXml);
			 wsClient.remoteASyncMessageCall(msgEvent);
			 
			 
			 SBox snedResultSBox = new SBox();
			 snedResultSBox.set("debtApplId", sBox.getString("debtApplSeq")); //GroupId
			 snedResultSBox.set("chgStat", sBox.getString("MOD_STAT")); //ActionTyp I/F시  actionType이 null이 들어옴 문제해결해야함
			 
			 if("FA".equals(sBox.getString("MOD_STAT")) || "FB".equals(sBox.getString("MOD_STAT"))){
				 snedResultSBox.set("rmkTxt", sBox.getString("RMK_TXT"));
			 }else{
				 snedResultSBox.set("rmkTxt", null);
			 }
			 
			 if("RR".equals(sBox.getString("MOD_STAT"))||"AC".equals(sBox.getString("MOD_STAT"))){
				 snedResultSBox.set("debtStatSubCd", sBox.get("debtStatSubCd"));
			 }else{
				 snedResultSBox.set("debtStatSubCd", null);
			 }
			 
			 //IF전송을 위한 채무불이행 테이블  상태변경 및 히스톨 삽입
			if(!("R".equals(modStat))){
				snedResultSBox.set("trans", "2"); //0:실패/ 1:성공/ 2:전송중
				snedResultSBox.set("rcode",null);
				snedResultSBox.set("reason","인터페이스 전송중 상태입니다.");
				snedResultSBox.set("sessionUsrNm", "SYSTEM");
				snedResultSBox.set("sessionUsrId", "0");
				snedResultSBox.set("docYn", "N");
				snedResultSBox.set("docCd", null);
	        	 SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(snedResultSBox):
	        		 										 debtDaoForMssql.updateDebtStatSendEndForWS(snedResultSBox);
				 debtStatHistId = updateBox.getString("DEBT_STAT_HIST_ID");
				
				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
				}
			}
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31002").getErrMsg() + "]");
		} //TODO 추후 비동기 메시지로 변경해야함
		 catch (BizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31002").getErrMsg() + "]");
		}
		return debtStatHistId; 
	}
	
	

	/**
	 * <pre>
	 *   채무불이행 해제 요청 팝업페이지 조회 Service Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번
	 * @return result : 채무불이행 해제 요청 정보
	 */
	@Override
	public SBox getDebtRequireRelease(SBox sBox) {

		SBox result = new SBox();

		try {

			// 필수 파라미터 초기화
			sBox.setIfEmpty("rsnTxtStat", "N");
			sBox.setIfEmpty("debtCompType", "E");
			sBox.set("debtStatCd", "RR");

			// [1] 채무불이행 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):debtDaoForMssql.selectDebt(sBox);

			// [2] 채무불이행 해제 요청 사유 조회
			SBoxList<SBox> rmkTxtList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtRsnTxt(sBox): debtDaoForMssql.selectDebtRsnTxt(sBox);

			// [3] 결과 값 셋팅
			result.set("debt", debtBox);
			result.set("rmkTxtList", rmkTxtList);
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30503").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 *   채무불이행 해제요청(AA) Controller Method
	 * </pre>
	 * 
	 * @author Jung mi Kim
	 * @since 2015. 7. 22.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, rmkTxt : 해제사유, debtStatSubCd : 채무불이행 상태 서브 코드, debtStatCd : 채무불이행 상태 코드 sessionUsrId : 회원 순번, sessionAdmYn : 관리자 여부
	 * @return
	 */
	@Override
	@Transactional
	public SBox releaseDebtRequire(SBox sBox) {

		SBox resultBox = new SBox();
		try {

        	//I/F시작
			sBox.setIfEmpty("RMK_TXT", sBox.get("rmkTxt"));
			sBox.setIfEmpty("MOD_STAT", sBox.get("debtStatCd"));
			sBox.setIfEmpty("debtApplSeq", sBox.get("debtApplId"));
			CommonAmendRequest(sBox);
			
			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			

		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", "30503");
			resultBox.set("REPL_MSG", EErrorCodeType.search("30503").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30503").getErrMsg() + "]");
		}
		return resultBox;
	}

	/**
	 * <pre>
	 *   채무불이행 접수취소 팝업페이지 조회 Service Method 
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번
	 * @return result : 채무불이행 접수취소 정보
	 */
	@Override
	public SBox getDebtApplyCancel(SBox sBox) {

		SBox result = new SBox();
		try {
			// 필수 파라미터 초기화
			sBox.setIfEmpty("rsnTxtStat", "N");
			sBox.setIfEmpty("debtCompType", "E");
			sBox.set("debtStatCd", "AC");

			// [1] 채무불이행 정보 조회
			SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox) : debtDaoForMssql.selectDebt(sBox);

			// [2] 채무불이행 접수취소 사유 조회
			SBoxList<SBox> rmkTxtList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtRsnTxt(sBox):debtDaoForMssql.selectDebtRsnTxt(sBox);

			// [3] 결과 값 셋팅
			result.set("debt", debtBox);
			result.set("rmkTxtList", rmkTxtList);
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30503").getErrMsg() + "]");
		}

		return result;
	}

	/**
	 * <pre>
	 *   채무불이행 접수취소(AC) Controller Method
	 * </pre>
	 * 
	 * @author Jung Mi Kim
	 * @since 2015. 7. 22.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, rmkTxt : 해제사유, debtStatSubCd : 채무불이행 상태 서브 코드, debtStatCd : 채무불이행 상태 코드 sessionUsrId : 회원 순번, sessionAdmYn : 관리자 여부
	 * @return
	 */
	@Override
	@Transactional
	public SBox cancelDebtApply(SBox sBox) {

		SBox resultBox = new SBox();
		try {

        	//I/F시작
			sBox.setIfEmpty("RMK_TXT", sBox.get("rmkTxt"));
			sBox.setIfEmpty("MOD_STAT", sBox.get("debtStatCd"));
			sBox.setIfEmpty("debtApplSeq", sBox.get("debtApplId"));
			CommonAmendRequest(sBox);
			
			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", "30503");
			resultBox.set("REPL_MSG", EErrorCodeType.search("30503").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30503").getErrMsg() + "]");
		}
		return resultBox;
	}

	/**
	 * <pre>
	 * 채무불이행신청 상태이력 등록 Service Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 10.
	 * @param debtApplId
	 *            : 채무불이행신청서 순번, overStDt : 연체개시일자, regDueDt : 등록예정일, sessionUsrId : 등록자 순번, sessionAdmYn : 등록자 회원 여부, sessionUsrNm : 등록자 이름
	 * @return
	 */
	@Override
	@Transactional
	public SBox insertDebtApplication(SBox sBox) {
		SBox result = new SBox();

		try {
			// [1] 파라미터 세팅
			sBox.set("stat", "AW");
			// TODO 추후 업체를 파라미터를 받을 수 있음. 일단 KED로 설정
			sBox.set("debtCompType", "E");

			// [2] 채무불이행 신청 상태 변경 및 상태 이력 추가
			result = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtApplication(sBox):debtDaoForMssql.insertDebtApplication(sBox);

			log.i("채무불이행 상태 이력 추가 응답 결과 : REPL_CD[" + result.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg() + "]");

			result.setIfEmpty("REPL_CD", "00000");
			result.setIfEmpty("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			result.set("REPL_CD", biz.getErrCode());
			result.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			result.set("REPL_CD", "30503");
			result.set("REPL_MSG", EErrorCodeType.search("30503").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30503").getErrMsg() + "]");
		}

		return result;
	}
	
	
	
	/**
	 * 
	 * <pre>
	 * 크레딧서비스채무불이행등록상태변경통지서 I/F  in  CreditNoticeDefaultUpdateStatus
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public String getIFUpdateStatus(MessageTag messageTag){
		String debtStatHistId ="";
		try{
			
	        SBox paramBox = new SBox();
			SBox xmlResult = CommonMessageIF.getCreditUpdateStatusForXml(messageTag.getDocumentData());
			String trans = "1";
			String rcode = "SCMN01";
			String reason = "정상 처리됨";

	        paramBox.set("debtApplId", messageTag.getGroupId());//xmlResult.getString("id"));//sBox.get("DEBT_APPL_ID"));
	        paramBox.set("chgStat", xmlResult.getString("typeCode"));//updateStatus.getDefaultUpdateStatusNoticeDocument().getTypeCode());
	    	paramBox.set("rmkTxt",  xmlResult.getString("description"));
	    	paramBox.set("trans", trans); //정상처리될때 에러처리될떄 들어가는 trans값과 rcode값 reason값을 확인해야함
	    	paramBox.set("rcode", rcode);
	    	paramBox.set("reason", reason);
	    	paramBox.set("docCd", null);
	    	paramBox.set("docYn", "N");
	    	paramBox.set("sessionUsrNm", "SYSTEM");
	    	paramBox.set("sessionUsrId", "0");
	    	paramBox.set("debtStatSubCd", null);
	    	
	    	SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(paramBox):debtDaoForMssql.selectDebt(paramBox);
	    	SBox resultBox = new SBox();
	    	
	    	//채무불이행 정정 ( 정정히스토리테이블에 insert/ 채무불이행테이블에 update) out
			//(정정히스토리테이블에 insert [이전 참조번호의 채무불이행 정정금액 조회해서] / 채무불이행테이블에 update) in
	    	//채무불이행 정정상태
	    	if("C".equals(paramBox.getString("chgStat"))){
	    		paramBox.set("modStat",  paramBox.getString("chgStat"));
	    		
	    		paramBox.set("debtAmt", !"0".equals(debtBox.getString("DEBT_AMT").replaceAll(",", "")) ? (debtBox.getString("DEBT_AMT").replaceAll(",", "")) : debtBox.get("DEBT_AMT"));// TODO 최신 채무불이행 정정히스토리에서  참조번호를 조회해서 금액을 입력한다.
	    		resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtApplForRequireModify(paramBox)
	    											  :debtDaoForMssql.updateDebtApplForRequireModify(paramBox);
				// [2] 채무불이행 정정 요청 이력 정보 추가
	    		paramBox.set("stat", paramBox.getString("chgStat"));
	    		//resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtModHist(paramBox): debtDaoForMssql.insertDebtModHist(paramBox);
	    		
	    		resultBox= "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtModHistForIF(paramBox):debtDaoForMssql.updateDebtModHistForIF(paramBox);//TODO MSSQL쿼리작성후 테슽
	    		
	    		
	    		if (!"00000".equals(resultBox.getString("REPL_CD"))) { 
	    			 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "처리 중 예외발생["+EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
	    			 log.e("Unknown Exception :" + resultBox.getString("REPL_CD")+","+ EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+ "]");
				}
	    	//채무불이행 상태변경	
	        }else{
	        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox) :
	        											    debtDaoForMssql.updateDebtStatSendEndForWS(paramBox) ;
				debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "debtStatHistId["+debtStatHistId+"] 채무불이행 상태변경 저장 중 예외발생";
	    			 log.e("Unknown Exception : debtStatHistId ["+debtStatHistId+":" + updateBox.getString("REPL_CD")+","+EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg()+ "]");
				}
	        }
	    	
	    	//NoticeConclusion 응답메시지 전송
			 String senderId = messageTag.getReceiveId();
			 String senderName = messageTag.getReceiveName();
			 String instanceId = SysUtil.getInstanceId(serviceCodeNoticeConclusion,senderId); //요청 문서의 instanceId
			//라우팅 메시지 VO 생성
			MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//S/A/P
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setReferenceId(messageTag.getInstanceId());
			 msgEvent.setGroupId(messageTag.getGroupId());//채무불이행순번 필수 
			 msgEvent.setDocumentType(serviceCodeNoticeConclusion);//NOTICE_CONCLUSION
			 msgEvent.setActionType(null); //채무불이행 신청 상태
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED 
			 msgEvent.setFileCount(0);
			 msgEvent.setTypeCode("RES");
	    	
	    	ResponseDocumentCommonVo responseVo = new ResponseDocumentCommonVo();
			// 응답문서 정보 영역
			responseVo.setId(instanceId);
			responseVo.setReferenceID(messageTag.getInstanceId());
			responseVo.setRDocumentTypeCode(serviceCodeNoticeConclusion);
			responseVo.setVersionInformation("1.0.0");
			responseVo.setRDDescription("채무불이행 상태 변경 처리결과통보서");
			
			//응답 결과 영역
			responseVo.setRRDTypeCode(trans);//1정상처리
			responseVo.setResponseTypeCode(rcode);//정상 처리됨
			responseVo.setRRDDescription(reason);
			
			String strXml = CommonMessageIF.makeNoticeConclusionForXML(responseVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent); 
			
	
		}catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31003").getErrMsg() + "]");
		}
		return debtStatHistId;
	}
	
	
	

/*	public void setWsClientBasic(WSClient wsClient) {
		this.wsClient = wsClient;
	}*/
	
	
	/**
	 * <pre>
	 * 채무불이행 진행완료 접수문서 조회 Service Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 7.
	 * @param periodConditionType
	 *            : 작성일자 타입, periodStDt : 조회 시작 일자, periodEdDt : 조회 종료 일자, processStatusType : 진행 상태, custSearchType : 검색 조건, custKwd : 검색 키워드, debtMbrType : 등록담당자 타입, registMbrType : 신청자 타입, mbrList
	 *            : 등록담당자 리스트, registMbrList : 신청자 리스트, orderCondition : 정렬조건, orderType : 정렬순서
	 * @return
	 */
	/*public SBox getDebtCompleteList(SBox sBox) {
		SBox result = new SBox();

		try {

			// [1] PARAMETER 초기화
			// [1-1] 최초 로딩시 파라미터 설정
			if (sBox.isEmpty("num")) {

				// 모든 결제조건의 CheckBox 값을 checked 할 수 있도록 함.
				sBox.setIfEmpty("processStatusALL", "ALL");
				sBox.setIfEmpty("processStatusAC", "AC");
				sBox.setIfEmpty("processStatusAU", "AU");
				sBox.setIfEmpty("processStatusKF", "KF");
				sBox.setIfEmpty("processStatusCR", "CR");
				sBox.setIfEmpty("processStatusRU", "RU");
			}

			// [1-2] 기타 파라미터 초기화
			// PARAMETER 초기화를 위해 시작날짜, 종료날짜 세팅
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");

			// 종료 날짜 세팅
			String edDt = frm.format(cal.getTime());
			// 시작 날짜 세팅 ( 1달전 )
			cal.add(Calendar.MONTH, -1);
			String stDt = frm.format(cal.getTime());

			// 검색조건 : 진행상태 체크 설정
			if (sBox.get("processStatusType") instanceof String[]) {
				String[] processStatusTypeArray = sBox.get("processStatusType");
				String processStatusTypeParam = "";
				for (int i = 0; i < processStatusTypeArray.length; i++) {
					String processStatusCheck = "processStatus" + processStatusTypeArray[i];
					sBox.set(processStatusCheck, processStatusTypeArray[i]);

					processStatusTypeParam += (processStatusTypeParam.length() > 0 ? "," : "") + processStatusTypeArray[i];
				}
				sBox.set("processStatusType", processStatusTypeParam);
			} else if (sBox.get("processStatusType") instanceof String) {
				String processStatusCheck = "processStatus" + sBox.get("processStatusType");
				sBox.set(processStatusCheck, sBox.get("processStatusType"));
			}

			sBox.setIfEmpty("processStatusType", "AU,AC,CR,RU,KF"); // 검색조건 : 진행상태(모든 상태 체크가 기본)
			sBox.setIfEmpty("periodConditionType", "APPL_DT"); // 검색조건 : 작성일자 타입(신청일자가 기본)
			sBox.setIfEmpty("periodStDt", stDt); // 검색조건 : 조회기간 시작날짜(현재 월 기준 1일)
			sBox.setIfEmpty("periodEdDt", edDt); // 검색조건 : 조회기간 종료날짜(현재 월 기준 말일)
			sBox.setIfEmpty("custSearchType", "business_all"); // 검색조건 : 거래처(default 전체)
			sBox.setIfEmpty("num", 1); // 검색조건 : 현재 페이지
			sBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			sBox.setIfEmpty("debtMbrType", ((Boolean) sBox.get("isSessionDebtApplGrn")) ? "ALL" : "none"); // 검색조건 : 등록담당자(전체선택)
			sBox.setIfEmpty("registMbrType", "ALL"); // 검색조건 : 신청담당자(전체선택)
			sBox.setIfEmpty("orderCondition", "APPL_DT"); // 검색조건 : 정렬옵션값(신청일자가 기본)
			sBox.setIfEmpty("orderType", "DESC"); // 검색조건 : 정렬옵션값(내림차순 기본)
			sBox.setIfEmpty("searchType", "L"); // 검색조건 : 리스트 or 엑셀 (리스트 기본)
			sBox.setIfEmpty("pageType", "COMPLETE"); // PageType (COMPLETE : 진행완료 접수문서 조회)

			// [2] 채무불이행 등록 담당자 조회
			sBox.setIfEmpty("grantCode", "D"); // D권한의 담당자 : 채무불이행 신청서 작성,수정,삭제 권한
			sBox.setIfEmpty("mbrStat", "N"); // 상태 : 정상
			SBoxList<SBox> debtMbrList = debtDao.selectMbrEmployeeListByCondition(sBox);

			// [3] 채무불이행 신청 담당자 조회
			sBox.set("grantCode", "E"); // E권한의 담당자 : 채무불이행 신청, 접수 권한
			SBoxList<SBox> debtRegistMbrList = debtDao.selectMbrEmployeeListByCondition(sBox);

			// [4] 채무불이행 신청서 TotlCnt 조회
			int debtTotalCount = debtDao.selectDebtTotalCount(sBox);

			// [5] 채무불이행 신청서 List 조회
			SBoxList<SBox> debtCompleteList = debtDao.selectDebtList(sBox);
			Iterator<SBox> it = debtCompleteList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			// [6] Paging 모듈 호출함
			String pcPage = commonPage.getPCPagingPrint(debtTotalCount, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");

			result.set("pcPage", pcPage); // 채무불이행 페이징
			result.set("total", debtTotalCount); // 채무불이행 신청서 리스트 전체 갯수
			result.set("debtMbrList", debtMbrList); // 채무불이행 등록담당자 리스트
			result.set("debtRegistMbrList", debtRegistMbrList); // 채무불이행 신청담당자 리스트
			result.set("debtCompleteList", debtCompleteList); // 채무불이행 신청완료 접수문서 리스트
			result.set("debtCompleteListOrderConditionTypeList", CommonCode.debtCompleteListOrderConditionTypeList); // 정렬조건
			result.set("orderTypeList", CommonCode.orderTypeList); // 오름 or 내림
			result.set("rowSizeTypeList", CommonCode.rowSizeTypeList); // 목록개수

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30199").getErrMsg() + "]");
		}

		return result;
	}*/
	
	
	
	
	
	/**
	 * <pre>
	 * 결제 후 채무불이행 상태를 변경하는 Service Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 28.
	 * @param debtApplSeq : 채무불이행 순번
	 * @return
	 */
	/*@Override
	public SBox payDebtApplication(SBox sBox) {
		SBox result = new SBox();
		
			// [1] 파라미터 Setting
			sBox.set("stat", "PY");
			sBox.set("usrId", sBox.get("sessionUsrId"));
			
			result = debtDao.insertDebtStatus(sBox);
			
			if (!"00000".equals(result.getString("REPL_CD"))) {
				throw new BizException(result.getString("REPL_CD"), EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());
			}
			
			result.set("REPL_CD", "00000");
			result.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			
		} catch (BizException biz) {
			result.set("REPL_CD", biz.getErrCode());
			result.set("REPL_MSG", EErrorCodeType.search(result.getString("REPL_CD")).getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			result.set("REPL_CD", "30499");
			result.set("REPL_MSG", EErrorCodeType.search("30499").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30499").getErrMsg() + "]");
		}
		
		return result;
	}*/
	
	/**
	 * <pre>
	 * 채무불이행 진행완료 접수문서 Excel Download Service Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 8.
	 * @param periodConditionType
	 *            : 작성일자 타입, periodStDt : 조회 시작 일자, periodEdDt : 조회 종료 일자, processStatusType : 진행 상태, custSearchType : 검색 조건, custKwd : 검색 키워드, debtMbrType : 등록담당자 타입, registMbrType : 신청자 타입, mbrList
	 *            : 등록담당자 리스트, registMbrList : 신청자 리스트, orderCondition : 정렬조건, orderType : 정렬순서
	 * @param fileName
	 * @return
	 */
	/*@Override
	public SBox getDebtCompleteInquiryListForExcel(SBox sBox, String fileName) {
		SBox result = new SBox();
		CommonExcel poi = null;
		try {

			// Excel Type으로 프로시져 호출함
			sBox.set("searchType", "E");
			SBox debtInquiryListForExcel = this.getDebtCompleteList(sBox);

			String columnListText = "접수번호$C,신청일$C,거래처명$C,사업자번호$C,대표자(채무자)$C,법인등록번호$C,채무금액(원)$C,진행상태$C,진행상태 변경일$C,등록 담당자$C,신청자$C,";

			String columnListValue = "DOC_CD,REG_DT,CUST_NM,CUST_NO,CUST_OWN_NM,CUST_CORP_NO,ST_DEBT_AMT,STAT,UPT_DT,USR_NM,APPL_USR_NM";

			// [1] Poi Excel 파일 생성
			poi = new CommonExcel(tmpFilePath + fileName);

			// [2] Sheet Name 설정
			String sheetsName[] = { "채무불이행 진행완료 접수문서 조회" };

			poi.createSheet(sheetsName);

			// [3] Title Style 생성
			ArrayList<CellStyle> titleStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListText.split("\\,").length; i++) {

				// [3-1] titleStyle 생성
				CellStyle titleStyle = poi.createNewStyle();
				// [3-2] FONT 설정
				poi.setFontStyleSetting(titleStyle, "돋움", (short) 10, "BOLD", false, false);
				// [3-3] 배경색상 설정
				poi.setBackgroundColorSetting(titleStyle, "LIGHT-YELLOW"); // 배경색상 설정
				// [3-4] 라인두께 설정
				poi.setLineBorder(titleStyle, "THICK");

				titleStyles.add(i, titleStyle);

			}

			// [4] Title Excel 파일 기록
			poi.setBulkColsDataFromStringArray(titleStyles, 1, 1, columnListText.split("\\,"));

			// [5] Data Style 생성
			ArrayList<CellStyle> dataStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListValue.split("\\,").length; i++) {

				// [5-1] dataStyle 생성
				CellStyle dataStyle = poi.createNewStyle();
				// [5-2] FONT 설정
				poi.setFontStyleSetting(dataStyle, "돋움", (short) 10, "NORMAL", false, false);
				// [5-3] 배경색상 설정
				poi.setBackgroundColorSetting(dataStyle, "WHITE"); // 배경색상 설정
				// [5-4] 라인두께 설정
				poi.setLineBorder(dataStyle, "THIN");

				dataStyles.add(i, dataStyle);
			}

			// [6] Excel 파일 기록
			@SuppressWarnings("unchecked")
			Iterator<SBox> it = ((SBoxList<SBox>) debtInquiryListForExcel.get("debtCompleteList")).iterator();
			int row = 2;

			while (it.hasNext()) {
				SBox data = (SBox) it.next();
				String[] keys = columnListValue.split(",");
				data.set("DOC_CD", data.getString("DOC_CD") + "$C");
				data.set("REG_DT", data.getString("REG_DT") + "$C");
				data.set("CUST_NM", data.getString("CUST_NM") + "$C");
				String custNo = "";
				if (!data.isEmpty("CUST_NO")) {
					custNo = data.getString("CUST_NO");
					custNo = custNo.substring(0, 3) + "-" + custNo.substring(3, 5) + "-" + custNo.substring(5);
				}
				data.set("CUST_NO", custNo + "$C");
				data.set("CUST_OWN_NM", data.getString("CUST_OWN_NM") + "$C");
				data.set("CUST_CORP_NO", data.getString("CUST_CORP_NO") + "$C");
				data.set("ST_DEBT_AMT", data.getString("ST_DEBT_AMT") + "원$R");
				data.set("STAT", data.getString("STAT") + "$C");
				data.set("UPT_DT", data.getString("UPT_DT") + "$C");
				data.set("USR_NM", data.getString("USR_NM") + "$C");
				data.set("APPL_USR_NM", data.getString("APPL_USR_NM") + "$C");
				poi.setBulkColsDataFromSBox(dataStyles, row, 1, data, keys);
				row++;
			}

			poi.write();

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30199").getErrMsg() + "]");
			ex.printStackTrace();
		} finally {
			if (poi != null) {
				try {
					poi.close();
				} catch (IOException e) {
				}
			}
		}
		return result;
	}*/

	/**
	 * <pre>
	 * 진행 중 접수문서 조회 EXCEL 다운로드 조회 Service
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 08.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusType : 진행상태 (구분자,) ,custSearchType : 거래처 검색조건 , custKwd : 거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택), registMbrType : 신청담당자(전체,
	 *            여러개 선택) , mbrList : 등록담당자 리스트(,구분자) , registMbrList : 신청담당자 리스트(,구분자) orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지 순번,
	 *            sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번 , fileName : 엑셀파일 이름, modStatR : 채무금액정정요청 , modStatC : 채무금액 정정완료
	 * @return sBox : 결과 Object
	 */
	/*@Override
	public SBox getDebtOngoingListForExcel(SBox sBox, String fileName) {

		SBox result = new SBox();
		CommonExcel poi = null;
		try {

			// Excel Type으로 프로시져 호출함
			sBox.set("searchType", "E");
			SBox debtOngoingListForExcel = this.getDebtOngoingList(sBox);

			String columnListText = "접수코드$C,신청일$C,거래처명$C,사업자번호$C,대표자(채무자)$C,법인등록번호$C,채무금액(원)$C,연체개시일$C,진행상태$C,진행상태변경일$C,등록예정일$C,등록담당자$C,신청자$C";

			String columnListValue = "ACPT_ID,APPL_DT_TIME,CUST_NM,CUST_NO,CUST_OWN_NM,CUST_CORP_NO,ST_DEBT_AMT,OVER_ST_DT_TIME,STAT,CH_STAT_DT_TIME,REG_DUE_DT_TIME,USR_NM,APPL_USR_NM";

			// [1] Poi Excel 파일 생성
			poi = new CommonExcel(tmpFilePath + fileName);

			// [2] Sheet Name 설정
			String sheetsName[] = { "채무불이행 진행 중 접수문서 조회" };

			poi.createSheet(sheetsName);

			// [3] Title Style 생성
			ArrayList<CellStyle> titleStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListText.split("\\,").length; i++) {

				// [3-1] titleStyle 생성
				CellStyle titleStyle = poi.createNewStyle();
				// [3-2] FONT 설정
				poi.setFontStyleSetting(titleStyle, "돋움", (short) 10, "BOLD", false, false);
				// [3-3] 배경색상 설정
				poi.setBackgroundColorSetting(titleStyle, "LIGHT-YELLOW"); // 배경색상 설정
				// [3-4] 라인두께 설정
				poi.setLineBorder(titleStyle, "THICK");

				titleStyles.add(i, titleStyle);

			}

			// [4] Title Excel 파일 기록
			poi.setBulkColsDataFromStringArray(titleStyles, 1, 1, columnListText.split("\\,"));

			// [5] Data Style 생성
			ArrayList<CellStyle> dataStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListValue.split("\\,").length; i++) {

				// [5-1] dataStyle 생성
				CellStyle dataStyle = poi.createNewStyle();
				// [5-2] FONT 설정
				poi.setFontStyleSetting(dataStyle, "돋움", (short) 10, "NORMAL", false, false);
				// [5-3] 배경색상 설정
				poi.setBackgroundColorSetting(dataStyle, "WHITE"); // 배경색상 설정
				// [5-4] 라인두께 설정
				poi.setLineBorder(dataStyle, "THIN");

				dataStyles.add(i, dataStyle);
			}

			// [6] Excel 파일 기록
			@SuppressWarnings("unchecked")
			Iterator<SBox> it = ((SBoxList<SBox>) debtOngoingListForExcel.get("debtOngoingList")).iterator();
			int row = 2;

			while (it.hasNext()) {
				SBox data = (SBox) it.next();
				String[] keys = columnListValue.split(",");
				data.set("ACPT_ID", data.getString("ACPT_ID") + "$C");
				data.set("APPL_DT_TIME", data.getString("APPL_DT_TIME") + "$C");
				data.set("CUST_NM", data.getString("CUST_NM") + "$C");
				String custNo = "";
				if (!data.isEmpty("CUST_NO")) {
					custNo = data.getString("CUST_NO");
					custNo = custNo.substring(0, 3) + "-" + custNo.substring(3, 5) + "-" + custNo.substring(5);
				}
				data.set("CUST_NO", custNo + "$C");
				data.set("CUST_OWN_NM", data.getString("CUST_OWN_NM") + "$C");
				data.set("CUST_CORP_NO", data.getString("CUST_CORP_NO") + "$C");
				data.set("ST_DEBT_AMT", data.getString("ST_DEBT_AMT") + "원$R");
				data.set("OVER_ST_DT_TIME", data.getString("OVER_ST_DT_TIME") + "$C");
				data.set("STAT", data.getString("STAT") + "$C");
				data.set("CH_STAT_DT_TIME", data.getString("CH_STAT_DT_TIME") + "$C");
				data.set("REG_DUE_DT_TIME", data.getString("REG_DUE_DT_TIME") + "$C");
				data.set("USR_NM", data.getString("USR_NM") + "$C");
				data.set("APPL_USR_NM", data.getString("APPL_USR_NM") + "$C");
				poi.setBulkColsDataFromSBox(dataStyles, row, 1, data, keys);
				row++;
			}
			poi.write();

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("30199").getErrMsg() + "]");
			ex.printStackTrace();
		} finally {
			if (poi != null) {
				try {
					poi.close();
				} catch (IOException e) {
				}
			}
		}
		return result;
	}
*/
	
	/**
	 * <pre>
	 * 채무불이행 신청페이지 인터페이스 Service Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 9.
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getDebtConfirmation(SBox sBox) {
		SBox result = new SBox();
		/*
		try {

			// 채무불이행(DE_DEBT_APPL) 정보 조회
			SBox debtBox = debtDao.selectDebt(sBox);
			debtBox.set("STAT_TXT", EDebtStatType.search(debtBox.getString("STAT")).getText());

			result.set("debtBox", debtBox);

			// 진행 수수료 SETTING
			debtBox.set("REG_CHAG", debtDao.selectDebtRegisterCharge(debtBox.getString("ST_DEBT_AMT").trim().replaceAll("[^0-9]", "")));
			
			// certInfo 정보 조회
			SBox tmpCompBox = myPageUserModifyDao.selectCompUser(sBox.getInt("sessionCompUsrId"));
			result.set("CERT_INFO", tmpCompBox.getString("CERT_INFO"));
			
			// 미수 채권 정보 조회
			SBox paramBox = new SBox();
			paramBox.set("custId", debtBox.get("CUST_ID"));
			paramBox.set("orderCondition", "BILL_DT");
			paramBox.set("orderType", "DESC");
			paramBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));

			//result.set("debentureList", debtDao.selectDebentureList(paramBox));

			// 상태 이력 조회
			if (!"TS".equals(debtBox.getString("STAT")) && !"SC".equals(debtBox.getString("STAT"))) {
				// TODO KED, NICE 등 회사 고려는 하지 않고 STAT과 STAT_SUBCODE로만 이력을 조회함
				result.set("debtStat", debtDao.selectDebtStatHistoryForRecentStatus(paramBox.getString("debtApplId")));
			}

			// 증빙 파일 조회
			paramBox.set("debtApplSeq", paramBox.get("debtApplId"));

			SBoxList<SBox> prfFileList = debtDao.selectDebtFileByCondition(paramBox);
			result.set("prfFileList", prfFileList);

			// 약관 조회
			StringBuffer debtClause = CommonUtil.fileRead(debtTxt + "/" + "debtClause.txt");
			result.set("debtClause", debtClause);

			// 안내사항 파일 읽기
			StringBuffer informationBriefing = CommonUtil.fileRead(debtTxt + "/" + "debtInformationBriefing.txt");
			result.set("informationBriefing", informationBriefing.toString());
			
		} catch (BizException biz) {
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30499").getErrMsg() + "]");
		}
*/
		return result;
	}
	
	/**
	 * <pre>
	 * 채무불이행 신청서 복사 후 작성 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 9.
	 * @param sBox
	 * @return debtApplId : 채무불이행순번, custId : 거래처 순번
	 */
	/*@Override
	@Transactional
	public SBox getDebtCopyAndWrite(SBox sBox) {
		SBox result = new SBox();
		try {

			// [1] 기존 채무불이행 Bulk Insert
			sBox.setIfEmpty("stat", "TS"); // 복사 후 저장은 [임시저장] 상태로 저장됨
			
			// ERP_ID 파라미터
			String erpId = CommonUtil.getTimeMiliSecond() + "000";
			sBox.set("erpId", erpId);
			
			SBox copyResultBox = debtDao.insertDebtCopy(sBox);
			if ("00000".equals(copyResultBox.getString("REPL_CD"))) {
				log.i("[1] 채무불이행 [STEP1, STEP2, STEP3] 복사완료 / 채무불이행 순번 [" + copyResultBox.getInt("NPM_ID") + "]");
			} else {
				throw new BizException(copyResultBox.getString("REPL_CD"), EErrorCodeType.search(copyResultBox.getString("REPL_CD")).getErrMsg());
			}
			int npmID = copyResultBox.getInt("NPM_ID"); // 복사 후 저장한 채무불이행 순번

			// [2] 해당 채무불이행 순번의 디렉토리가 있는지 확인하고 없으면 디렉토리 생성
			File debtFileDir = new File(debtFilePath + "/" + npmID);
			if (!debtFileDir.isDirectory()) {
				if(debtFileDir.mkdir()) {
					log.i("[2] 채무불이행신청서 증빙파일 디렉토리 생성[ 경로 : "+ debtFilePath + npmID +" ]");
				}
				
			}
			
			// 파일을 복사한다고 했을 시에만 파일 복사 진행
			if ("Y".equals(sBox.getString("copyfile"))) {
				// [2] 기존 미수채권 관련 파일 리스트 조회
				sBox.set("debtApplSeq", sBox.getInt("debtApplId"));
				SBoxList<SBox> debentureFileList = debtDao.selectDebtFileByCondition(sBox);
				log.i("[3] 기존 미수채권 관련 파일 리스트 조회 총개수[" + debentureFileList.size() + "]");

				// [4] 파일 복사 후 새로운 파일 등록
				Iterator<SBox> it = debentureFileList.iterator();
				int index = 0;
				while (it.hasNext()) {
					SBox prevFileBox = (SBox) it.next();

					// [4-1] 파일 복사
					String originPath = debtFilePath + "/" + prevFileBox.getString("FILE_PATH");
					String[] fileNm = CommonUtil.separateFileName(prevFileBox.getString("FILE_NM"));
					String newFile = npmID + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (index+1) + "." +  fileNm[1];
					String newPath = debtFilePath + "/" + newFile;
					CommonUtil.fileCopy(originPath, newPath);
					log.i("[4-1] 파일 복사 완료 / 기존파일[" + prevFileBox.getString("FILE_PATH") + "] 변경파일[" + newFile + "]");

					// [4-2] 새롭게 파일 등록함 (DE_DEBT_FILE)
					SBox afterFileBox = new SBox();
					afterFileBox.set("debtApplSeq", npmID);
					afterFileBox.set("debnFileSn", null);
					afterFileBox.set("typeCd", prevFileBox.getString("TYPE_CD"));
					afterFileBox.set("fileNm", prevFileBox.getString("FILE_NM"));
					afterFileBox.set("filePath", newFile);
					afterFileBox.set("debnId", prevFileBox.getString("DEBN_ID"));

					SBox insertNewFileResult = debtDao.insertDebtFile(afterFileBox);
					if ("00000".equals(insertNewFileResult.getString("REPL_CD"))) {
						log.i("[4-2] 채무불이행 [첨부파일] 복사완료");
					} else {
						throw new BizException(copyResultBox.getString("REPL_CD"), EErrorCodeType.search(copyResultBox.getString("REPL_CD")).getErrMsg());
					}
					
					index++;
				}
			}
			
			// [4] 정보를 호출할 파라미터 설정
			sBox.set("debtApplId", npmID);
			sBox.set("custId", sBox.getString("custId"));

			result.set("debtBox", getDebtInquiry(sBox));
			
		} catch (BizException biz) {
			biz.printStackTrace();
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30499").getErrMsg() + "]");
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return result;
	}*/
	
	/**
	 * <pre>
	 * 진행 중 접수문서 조회 리스트 Service Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 07.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusType : 진행상태 (구분자,) ,custSearchType : 거래처 검색조건 , custKwd : 거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택), registMbrType : 신청담당자(전체,
	 *            여러개 선택) , mbrList : 등록담당자 리스트(,구분자) , registMbrList : 신청담당자 리스트(,구분자) orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지 순번,
	 *            sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번 , modStatR : 채무금액정정요청 , modStatC : 채무금액 정정완료
	 * @return sBox : 결과 Object
	 */
	/*@Override
	public SBox getDebtOngoingList(SBox sBox) {

		SBox result = new SBox();

		try {

			// [1] PARAMETER 초기화
			// [1-1] 기타 파라미터 초기화

			if (sBox.isEmpty("num")) {

				// 모든 결제조건의 CheckBox 값을 checked 할 수 있도록 함.
				sBox.setIfEmpty("processStatusALL", "");
				sBox.setIfEmpty("processStatusAW", "AW");
				sBox.setIfEmpty("processStatusEV", "EV");
				sBox.setIfEmpty("processStatusAA", "AA");
				sBox.setIfEmpty("processStatusPY", "PY");
				sBox.setIfEmpty("processStatusNT", "NT");
				sBox.setIfEmpty("processStatusCA", "CA");
				sBox.setIfEmpty("processStatusFA", "FA");
				sBox.setIfEmpty("processStatusRC", "RC");
				sBox.setIfEmpty("processStatusRR", "RR");
				sBox.setIfEmpty("processStatusCB", "CB");
				sBox.setIfEmpty("processStatusFB", "FB");
			}

			// PARAMETER 초기화를 위해 시작날짜, 종료날짜 세팅
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");

			// 종료 날짜 세팅
			String edDt = frm.format(cal.getTime());
			// 시작 날짜 세팅 ( 1달전 )
			cal.add(Calendar.MONTH, -1);
			String stDt = frm.format(cal.getTime());

			sBox.setIfEmpty("periodStDt", stDt); // 검색조건 : 조회기간 시작날짜(현재 월 기준 1일)
			sBox.setIfEmpty("periodEdDt", edDt); // 검색조건 : 조회기간 종료날짜(현재 월 기준 말일)
			sBox.setIfEmpty("periodConditionType", "ALL_DT"); // 작성일자 타입
			sBox.setIfEmpty("custSearchType", "business_all"); // 검색조건 : 거래처(default 전체)
			sBox.setIfEmpty("num", 1); // 검색조건 : 현재 페이지
			sBox.setIfEmpty("rowSize", 10); // 검색조건 : 목록 갯수
			sBox.setIfEmpty("registMbrType", "ALL"); // 검색조건 : 등록담당자(전체선택)
			sBox.setIfEmpty("orderCondition", "APPL_DT"); // 검색조건 : 정렬옵션값(신청일자가 기본)
			sBox.setIfEmpty("orderType", "DESC"); // 검색조건 : 정렬옵션값(내림차순 기본)
			sBox.setIfEmpty("searchType", "L"); // 검색조건 : 리스트 or 엑셀 (리스트 기본)
			sBox.setIfEmpty("pageType", "ONGOING"); // PageType (ONGOING : )
			sBox.setIfEmpty("periodConditionType", "APPL_DT");
			sBox.setIfEmpty("processStatusType", "AW,EV,AA,NT,PY,CA,FA,RC,RR,CB,FB");
			sBox.setIfEmpty("debtMbrType", ((Boolean) sBox.get("isSessionDebtApplGrn")) ? "ALL" : "none"); // 검색조건 : 등록담당자(전체선택)

			// [2] 채무불이행 등록 담당자 조회
			sBox.set("grantCode", "D"); // D권한의 담당자 : 채무불이행 신청서 작성,수정,삭제 권한
			sBox.setIfEmpty("mbrStat", "N"); // 상태 : 정상
			SBoxList<SBox> debtMbrList = debtDao.selectMbrEmployeeListByCondition(sBox);

			// [3] 채무불이행 신청인 조회
			sBox.set("grantCode", "E"); // E권한의 담당자 : 채무불이행 신청서 작성,수정,삭제 권한
			SBoxList<SBox> debtRegistMbrList = debtDao.selectMbrEmployeeListByCondition(sBox);

			// [3] 채무불이행 진행 중 리스트 조회
			int debtOngoingTotalCount = debtDao.selectDebtTotalCount(sBox);

			// [4] 채무불이행 신청서 List 조회
			SBoxList<SBox> debtOngoingList = debtDao.selectDebtList(sBox);
			Iterator<SBox> it = debtOngoingList.iterator();
			while (it.hasNext()) {
				SBox temp = (SBox) it.next();
				temp.set("STAT_CODE",temp.getString("STAT"));
				temp.set("STAT", EDebtStatType.search(temp.getString("STAT")).getDesc());
			}

			// [5] Paging 모듈 호출함
			String pcPage = commonPage.getPCPagingPrint(debtOngoingTotalCount, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");

			result.set("pcPage", pcPage); // 채무불이행 페이징
			result.set("total", debtOngoingTotalCount); // 채무불이행 신청서 리스트 전체 갯수
			result.set("debtMbrList", debtMbrList); // 채무불이행 등록담당자 리스트
			result.set("debtRegistMbrList", debtRegistMbrList); // 채무불이행 신청자 리스트
			result.set("debtOngoingList", debtOngoingList); // 채무불이행 신청서 리스트
			result.set("registPeriodConditionTypeList", CommonCode.registPeriodConditionTypeList); // 작성일자검색조건
			result.set("debtOngoingListOrderConditionTypeList", CommonCode.debtOngoingListOrderConditionTypeList); // 정렬조건
			result.set("orderTypeList", CommonCode.orderTypeList); // 오름 or 내림
			result.set("rowSizeTypeList", CommonCode.rowSizeTypeList); // 목록개수

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30199").getErrMsg() + "]");
		}

		return result;
	}*/
	
}
