package com.spsb.service.dgdebn;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonUtil;
import com.spsb.dao.dgdebn.DgDebentureDaoForMssql;
import com.spsb.dao.dgdebn.DgDebentureDaoForOracle;

/**
 * <pre>
 *    채권진단 Service Implements Class
 * </pre>
 * 
 * @author KIM GA EUN
 * @since 2014. 04. 18.
 * @version 1.0
 * @package com.sbdebn.serviceImpl.dgdebn.DgDebentureServiceImpl.java
 */
@Service
public class DgDebentureServiceImpl extends SuperService implements DgDebentureService{
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private DgDebentureDaoForOracle DgDebentureDaoForOracle;
	@Autowired
	private DgDebentureDaoForMssql DgDebentureDaoForMssql;
	
	
	/**
	 * <pre>
	 *   채권진단 리스트 조회 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param usrId : 개인회원슌번
	 * @return result dgDebentureList : 채권진단 리스트
	 */
	@Override
	public SBoxList<SBox> getDgDebentureList(Integer usrId) {
		SBoxList<SBox> dgDebentureList = null;
		try {
			
			dgDebentureList = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDgDebentureList(usrId):
														 DgDebentureDaoForMssql.selectDgDebentureList(usrId);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return dgDebentureList;

	}

	/**
	 * <pre>
	 * 채권진단 등록 Service Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param sBox
	 * 			usrId : 개인회원순번, tmpUsrId : 임시회원순번, stat : 상태, usrType : 사용자유형, usrNo : 사업자등록번호, usrNm : 회사명, mbNo : 휴대폰번호, postNo : 직인정보, addr1 : 주소1
	 * 			, addr2 : 주소2, custType : 거래처유형, custNo : 거래처사업자등록번호, custNm : 거래처명, ownNm : 대표자명, custMbNo : 거래처휴대폰번호, stDtType : 일자유형, stDt : 작성일자, debnAmt : 매출채권금액
	 * 			, custPostNo : 거래처우편번호, custAddr1 : 거래처주소1, custAddr2 : 거래처주소2
	 * @return
	 */
	@Override
	public Integer addDgDebenture(SBox sBox) {
		Integer result = 0;
		String usrId =  sBox.getString("sessionUsrId");
		
		try {
			if(sBox.getString("isSbDebnSession").equals("Y")){ //회원
				sBox.set("usrId", usrId); // 개인회원순번
				sBox.set("tmpUsrId", null); // 임시회원순번
				sBox.set("usrType", "J"); // 사용자유형 : 회원(J)
			}
			if(sBox.getString("isSbDebnSession").equals("N")){ //게스트회원
				sBox.set("usrId", null); // 개인회원순번
				sBox.set("tmpUsrId", usrId); // 임시회원순번
				sBox.set("usrType", "G"); // 사용자유형 : 비회원(G)
			}
				sBox.set("stat", "N"); // 상태 : 정상(N)
				String usrNo1 = sBox.getString("usrNo1");
				String usrNo2 = sBox.getString("usrNo2");
				String usrNo3 = sBox.getString("usrNo3");
				String usrNo = usrNo1 + usrNo2 + usrNo3;
				sBox.set("usrNo", usrNo); // 사업자등록번호
				
				sBox.set("usrNm", sBox.getString("usrNm")); // 회사명
				
				String mbNo1 = sBox.getString("mbNo1");
				String mbNo2 = sBox.getString("mbNo2");
				String mbNo3 = sBox.getString("mbNo3");
				String mbNo = mbNo1+"-"+mbNo2+"-"+mbNo3;
				sBox.set("mbNo", mbNo); // 휴대폰번호
				
				String postNo1 = sBox.get("postNo1");
				String postNo2 = sBox.get("postNo2");
				String postNo = postNo1+postNo2;
				sBox.set("postNo", postNo); // 우편번호
				if((sBox.isEmpty("postNo1"))){
					sBox.set("postNo", null); 
				}
				
				sBox.set("addr1", sBox.get("addr1")); // 주소1
				if((sBox.isEmpty("addr1"))){
					sBox.set("addr1", null); 
				}
			
				sBox.set("addr2", sBox.get("addr2")); // 주소2
				if((sBox.isEmpty("addr2"))){
					sBox.set("addr2", null); 
				}
				
				sBox.set("custType", sBox.getString("custType")); // 거래처유형
				
				String custNo1 = sBox.getString("custNo1");
				String custNo2 = sBox.getString("custNo2");
				String custNo3 = sBox.getString("custNo3");
				String custNo = custNo1 + custNo2 + custNo3;
				sBox.set("custNo", custNo); // 사업자등록번호
	
				sBox.set("custNm", sBox.getString("custNm")); // 거래처명
				
				sBox.set("ownNm", sBox.getString("ownNm")); // 대표자명
				if((sBox.isEmpty("ownNm"))){
					sBox.set("ownNm", null);
				}
				
				String custMbNo1 = sBox.get("custMbNo1");
				String custMbNo2 = sBox.get("custMbNo2");
				String custMbNo3 = sBox.get("custMbNo3");
				String custMbNo = custMbNo1+"-"+custMbNo2+"-"+custMbNo3;
				if((sBox.isEmpty("custMbNo2"))){
					custMbNo = null;
				}
				sBox.set("custMbNo", custMbNo); // 거래처휴대폰번호
	
				sBox.set("stDtType", sBox.getString("stDtType")); // 일자유형
	
				sBox.set("stDt", sBox.getString("stDt")); // 작성일자
		
				sBox.set("debnAmt", sBox.getString("debnAmt")); // 매출채권금액
	
				String applyPostNo1 = sBox.get("apply_post_no1");
				String applyPostNo2 = sBox.get("apply_post_no2");
				String custPostNo = applyPostNo1+applyPostNo2;
				sBox.set("custPostNo", custPostNo); // 거래처우편번호
				if((sBox.isEmpty("apply_post_no1"))){
					sBox.set("custPostNo", null);
				}
	
				sBox.set("custAddr1", sBox.getString("custAddr1")); // 거래처주소1
				if((sBox.isEmpty("custAddr1"))){
					sBox.set("custAddr1", null);
				}
				
				sBox.set("custAddr2", sBox.getString("custAddr2")); // 거래처주소2
				if((sBox.isEmpty("custAddr2"))){
					sBox.set("custAddr2", null);
				}
		
			result = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.insertDgDebn(sBox): DgDebentureDaoForMssql.insertDgDebn(sBox);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return result;
	}

	/**
	 * <pre>
	 *   Guest 회원  채권진단 리스트 조회   Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return result dgDebentureList : 채권진단 리스트
	 */
	@Override
	public SBoxList<SBox> getDgDebentureListForTmpUsr(Integer tmpUsrId) {
		SBoxList<SBox> dgDebentureList = null;
		try {
			
			dgDebentureList = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDgDebentureListForTmpUsr(tmpUsrId):
													     DgDebentureDaoForMssql.selectDgDebentureListForTmpUsr(tmpUsrId);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return dgDebentureList;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 최근 채권진단 정보 조회   Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return result tmpUsr : 최근 채권진단 정보
	 */
	@Override
	public SBoxList<SBox> getGuestDgDebenture(Integer tmpUsrId) {
		SBoxList<SBox> tmpUsr = null;
		try {
			
			tmpUsr = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectGuestDgDebenture(tmpUsrId):
												DgDebentureDaoForMssql.selectGuestDgDebenture(tmpUsrId);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return tmpUsr;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 채권 진단회수 변경   Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return
	 */
	@Override
	public Integer modifyTmpUsrDgCnt(Integer tmpUsrId) {
		Integer result = 0;
		try {
			
			result = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.updateTmpUsrDgCnt(tmpUsrId):
												DgDebentureDaoForMssql.updateTmpUsrDgCnt(tmpUsrId);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 정보 조회   Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return result tmpUsr : Guest 회원 정보
	 */
	@Override
	public SBox getTmpUsr(Integer tmpUsrId) {
		SBox tmpUsr = null;
		try {
			
			tmpUsr = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectTmpUsr(tmpUsrId): DgDebentureDaoForMssql.selectTmpUsr(tmpUsrId);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return tmpUsr;
	}

	/**
	 * <pre>
	 *   Guest 회원 상태 변경   Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return
	 */
	@Override
	public Integer modifyTmpUsrStat(Integer tmpUsrId, String stat) {
		Integer result = 0;
		try {
			
			result = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.updateTmpUsrStat(tmpUsrId, stat)
										       :DgDebentureDaoForMssql.updateTmpUsrStat(tmpUsrId, stat);
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 *  채권 진단 가이드 보기 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 7. 03.
	 * @param sBox
	 *            custType : 거래처유형, debnAmt : 매출채권금액, stDt : 작성일자
	 * @return
	 */
	@Override
	public SBox getDgDebentureResult(SBox sBox) {
		
		SBox resultBox = new SBox();
		try {
			String custType =  sBox.getString("custType");
			sBox.set("custType", custType); // 거래처유형
			String debnAmt = sBox.getString("debnAmt");
			sBox.set("debnAmt", debnAmt); // 매출채권금액
			String stDt = sBox.getString("stDt");
			sBox.set("stDt", stDt); // 작성일자
			String stDtType = sBox.getString("stDtType"); //일자유형
			
			// [1] 채권가이드 정보 조회
			long date = CommonUtil.getDateDiff(stDt, CommonUtil.getDate("-"));
			String custDayTerm = Long.toString(date);
			
			SBox debuntureGuideBox = getDgDebentureGuide(sBox);
			SBoxList<SBox> debentureGuideList = debuntureGuideBox.get("debentureGuideList");
			SBox contentColorBox = debuntureGuideBox.get("contentColorBox");
			
			// [2] 채권가이드에 표기해야할 부가정보 설정
			SBox debentureGuideInfo = new SBox();
			debentureGuideInfo.set("dateDiff", CommonUtil.getDateDiff(stDt, CommonUtil.getDate("-")));
			
			resultBox.set("custType", custType); // 거래처유형
			resultBox.set("stDt", stDt); // 작성일자
			resultBox.set("debnAmt", debnAmt); // 매출채권금액
			resultBox.set("stDtType", stDtType); // 일자유형
			resultBox.set("custDayTerm", custDayTerm); // 경과일자
			resultBox.set("debentureGuideList", debentureGuideList); // 채권 가이드 정보 조회
			resultBox.set("debentureGuideInfo", debentureGuideInfo); // 채권 가이드 부가정보
			resultBox.set("contentColorBox", contentColorBox); // 채권 가이드 컨텐츠 색상정보
			
		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return resultBox;
	}

	/**
	 * <pre>
	 *  채권 진단 가이드 보기 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 7. 03.
	 * @param sBox
	 *            custType : 거래처유형, debnAmt : 매출채권금액, stDt : 작성일자
	 * @return
	 */
	@Override
	public SBox getDgDebentureGuide(SBox sBox) {
		SBox resultBox = new SBox();
		SBox contentColorBox = new SBox();
		
		try {	
			sBox.set("custType", sBox.getString("custType")); // 거래처유형
			sBox.set("debnAmt", sBox.getString("debnAmt")); // 매출채권금액
			sBox.set("stDt", sBox.getString("stDt")); // 작성일자
			
			// [1] 채권가이드 정보 조회
			SBoxList<SBox> debentureGuideList = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDebentureGuide(sBox):
																		   DgDebentureDaoForMssql.selectDebentureGuide(sBox);
			
			// [2] 이전 채권 가이드 정보 조회 (경과일수가 0이상인경우만 이전 가이드를 조회함) : 채권 컨텐츠의 색상을 구별하기 위함.
			if (debentureGuideList != null && debentureGuideList.size() > 0) {
				int term = debentureGuideList.get(0).getInt("DAY_TERM");
	
				if (term >= 1 && term <= 30) {
					term = 0;
				} else if (term >= 31 && term <= 60) {
					term = 1;
				} else if (term >= 61 && term <= 90) {
					term = 31;
				} else if (term >= 91 && term <= 330) {
					term = 61;
				} else if (term >= 331) {
					term = 91;
				}
				
				sBox.set("custDayTerm", Integer.toString(term)); // 작성일자
				
			// [2-1] 이전 채권 가이드 조회
			SBoxList<SBox> debenturePrevGuideList = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDebentureGuideAllView(sBox):
																			   DgDebentureDaoForMssql.selectDebentureGuideAllView(sBox);
			
			// [2-1] 이전 채권 가이드 진단 결과문구순번만 추출함
			Iterator<SBox> prevIt = debenturePrevGuideList.iterator();
			String prevDgsRsltClsIdList = ","; // 이전 채권진단결과문구순번
			while (prevIt.hasNext()) {
				SBox temp = (SBox) prevIt.next();
				prevDgsRsltClsIdList += temp.getString("DGS_RSLT_CLS_ID") + ",";
			}

			// [2-2] 이전 채권가이드 내용과 비교하여 겹치는게 있는것은 파랑색으로 표기함.
			Iterator<SBox> currentIt = debentureGuideList.iterator();
			while (currentIt.hasNext()) {
				SBox temp = (SBox) currentIt.next();
				if (prevDgsRsltClsIdList.indexOf("," + temp.getString("DGS_RSLT_CLS_ID") + ",") > 0) {
					temp.set("COLOR_TYPE", "al02");
					contentColorBox.set("color" + temp.getString("DGS_RSLT_CLS_ID"), "al02");
				} else {
					temp.set("COLOR_TYPE", "al03");
					contentColorBox.set("color" + temp.getString("DGS_RSLT_CLS_ID"), "al03");
				}
			}
		}
		
		// 결과값 셋팅
		resultBox.set("debentureGuideList", debentureGuideList); // 채권가이드 리스트
		resultBox.set("contentColorBox", contentColorBox); // 채권 컨텐츠 색상정보
		
		resultBox.set("REPL_CD", "00000");
		resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());
			
		}catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20599").getErrMsg() + "]");
		}
		return resultBox;
	}

	/**
	 * <pre>
	 *  채권 진단 키워드 조회 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 7. 03.
	 * @param dgsRsltClsId
	 *            : 채권진단결과문구순번 , debnId : 채권번호
	 * @return
	 */
	@Override
	public SBox getDgDebentureGuideKeyword(String dgsRsltClsId) {
		SBox resultBox = new SBox();
		try {

			// [1] 채권 진단 키워드 리스트 처리
			SBoxList<SBox> debentureGuideKeywordList = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDgDebentureGuideKeyword(dgsRsltClsId):
																				  DgDebentureDaoForMssql.selectDgDebentureGuideKeyword(dgsRsltClsId);

			// [2] 가장 첫번째 SubKeyword의 값을 이용해서 의미를 조회한다.
			Iterator<SBox> it = debentureGuideKeywordList.iterator();

			String dgsSubKwId = ""; // 채권진단서브키워드순번
			while (it.hasNext()) {
				SBox tempBox = (SBox) it.next();
				if ("S".equals(tempBox.getString("GUBUN"))) {

					// 가장 1번째 SubKeyword만 가져온다.
					dgsSubKwId = tempBox.getString("DGS_SUB_KW_ID");
					break;
				}
			}

			// [3] 채권 진단 키워드 리스트 처리
			SBox debentureSubKeywordMean = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDgDebentureGuideKeywordMean(dgsSubKwId):
																	  DgDebentureDaoForMssql.selectDgDebentureGuideKeywordMean(dgsSubKwId);

			// [4] 결과 값 셋팅
			resultBox.set("debentureGuideKeywordList", debentureGuideKeywordList);
			resultBox.set("debentureSubKeywordMean", debentureSubKeywordMean);

			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			resultBox.set("REPL_CD", "20199");
			resultBox.set("REPL_MSG", EErrorCodeType.search("20199").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20199").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	/**
	 * <pre>
	 *  채권 진단 서브키워드 의미 조회 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 7. 03.
	 * @param dgsSubKwId
	 *            : 채권진단서브키워드순번
	 * 
	 * @return resultList : 채권 진단 서브키워드 의미 조회 결과
	 */
	@Override
	public SBox getDgDebentureGuideKeywordMean(String dgsSubKwId) {
		SBox resultBox = new SBox();
		try {

			// [1] 채권 진단 키워드 리스트 처리
			SBox debentureSubKeywordMean = "oracle".equals(dbmsType)? DgDebentureDaoForOracle.selectDgDebentureGuideKeywordMean(dgsSubKwId):
																	  DgDebentureDaoForMssql.selectDgDebentureGuideKeywordMean(dgsSubKwId);

			// [2] 결과 값 셋팅
			resultBox.set("debentureSubKeywordMean", debentureSubKeywordMean);
			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			resultBox.set("REPL_CD", "20199");
			resultBox.set("REPL_MSG", EErrorCodeType.search("20199").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20199").getErrMsg() + "]");
		}
		return resultBox;
	}
}
