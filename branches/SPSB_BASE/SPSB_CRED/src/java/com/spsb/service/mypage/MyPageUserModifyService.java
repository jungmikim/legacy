
package com.spsb.service.mypage;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 *	마이페이지 - 정보조회 화면 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
public interface MyPageUserModifyService {

	public SBox getCompUserInfo(SBox sBox);
	public SBox getCustCreditSummaryList(SBox sBox);
	public SBox getUserInfo(SBox sBox);
	public SBox getDebentureListCountByMypage(SBox sBox);
	public SBox getSummaryList(SBox sBox);
	public SBox modifyUserForPW(SBox sBox);
	
	public SBox getBelongingtoCompanyList(SBox sBox);
	/*public SBox modifyCompUser(SBox sBox);
	public SBox modifyUser(SBox sBox);
	
	public SBox modifyUsrSecession(SBox sBox);*/
	/*public SBox modifyBelongtoCompanyListInfo(SBox sBox);
	public SBox modifyCertInfo(SBox sBox);*/
	/*public SBox modifyCompUserForSB(SBox sBox);
	public SBox modifyUserForSB(SBox sBox);
	public SBox modifyCompAdmInfoForSB(SBox sBox);*/
	
	
	
}
