package com.spsb.service.dgdebn;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;

/**
 * <pre>
 *  채권진단 Interface 정의
 * </pre>
 * @author KIM GA EUN
 * @since 2014. 04. 18.
 * @version 1.0
 * @package com.portal.service.dgdebn.DgDebentureService.java
 */
public interface DgDebentureService {
	
	public SBoxList<SBox> getDgDebentureList(Integer usrId);
	
	public SBoxList<SBox> getDgDebentureListForTmpUsr(Integer tmpUsrId);
	
	public Integer addDgDebenture(SBox sBox);
	
	public SBoxList<SBox> getGuestDgDebenture(Integer tmpUsrId);
	
	public Integer modifyTmpUsrDgCnt(Integer dgCnt);
	
	public Integer modifyTmpUsrStat(Integer tmpUsrId, String stat);
	
	public SBox getTmpUsr(Integer tmpUsrId);
	
	public SBox getDgDebentureResult(SBox sBox);
	
	public SBox getDgDebentureGuide(SBox sBox);
	
	public SBox getDgDebentureGuideKeyword(String dgsRsltClsId);
	
	public SBox getDgDebentureGuideKeywordMean(String dgsSubKwId);

}
