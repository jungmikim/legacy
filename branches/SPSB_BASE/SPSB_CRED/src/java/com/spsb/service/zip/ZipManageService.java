package com.spsb.service.zip;
import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;

/**
 * <pre>
 * 우편번호 찾기 Service Interface Class
 * </pre>
 * @author KIM GA EUN
 * @since 2014. 1. 7.
 * @version 1.0
 */
public interface ZipManageService {
	public SBoxList<SBox> getSidoList();
	public SBoxList<SBox> getGunguForWebList(String sido);
	public SBoxList<SBox> getAddressByStreetSearchForWebList(SBox sBox);
	public SBoxList<SBox> getAddressByDongSearchForWebList(SBox sBox);
	public SBoxList<SBox> getAddressByBuildingSearchForWebList(SBox sBox);
}
