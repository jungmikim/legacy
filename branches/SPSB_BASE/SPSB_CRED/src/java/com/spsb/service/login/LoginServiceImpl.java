package com.spsb.service.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.parent.SuperService;
import com.spsb.dao.login.LoginDaoForMssql;
import com.spsb.dao.login.LoginDaoForOracle;

/**
 * <pre>
 * 로그인Service Implements Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 26.
 * @version 1.0
 */
public class LoginServiceImpl extends SuperService implements LoginService{
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private LoginDaoForOracle loginDaoForOracle;
	
	@Autowired
	private LoginDaoForMssql loginDaoForMssql;
	
	
	/**
	 * <pre>
	 * 로그인 개인회원 조회 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param loginId : 로그인 식별자
	 * @param loginPw : 로그인 비밀번호
	 * @return loginUser : 로그인회원 정보
	 */
	@Override
	public SBox getLoginMemberUser(String loginId, String loginPw) {
		SBox result = new SBox();
		SBox loginUser  = new SBox();
		result.set("result", EUserErrorCode.FAIL);
		try {
			loginUser = "oracle".equals(dbmsType)? loginDaoForOracle.selectLoginMemberUser(loginId):loginDaoForMssql.selectLoginMemberUser(loginId);
			if(loginUser != null && !loginUser.isEmpty("LOGIN_ID")){
				if(loginPw.equals(loginUser.get("LOGIN_PW"))){
					if("R".equals(loginUser.get("USR_STAT")) || "D".equals(loginUser.get("USR_STAT"))){
						result.set("result", EUserErrorCode.LOGIN_RETIRED);
					}else if("N".equals(loginUser.get("USR_STAT"))){
						loginUser.set("LOGIN_PW", null);
						result = loginUser;
						result.set("result", EUserErrorCode.SUCCESS);
					}
				}else{
					result.set("result", EUserErrorCode.LOGIN_FAILED_PASSWD);
				}
			}else{
				result.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
			}
		} catch (Exception e) {
			EUserErrorCode errorCode = result.get("result");
			log.e(getLogMessage("getLoginMemberUser", "로그인 결과 ", errorCode.getDesc()));
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 로그인 개인회원 조회(by loginId ) Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 14.
	 * @version 1.0
	 * @param loginId : 로그인 식별자
	 * @return loginUser : 로그인회원 정보
	 */
	@Override
	public SBox getLoginMemberUser(String loginId) {
		SBox result = new SBox();
		SBox loginUser  = new SBox();
		result.set("result", EUserErrorCode.FAIL);
		try {
			loginUser = "oracle".equals(dbmsType)? loginDaoForOracle.selectLoginMemberUser(loginId):loginDaoForMssql.selectLoginMemberUser(loginId);
			if(loginUser != null && !loginUser.isEmpty("LOGIN_ID")){
				if("R".equals(loginUser.get("USR_STAT")) || "R".equals(loginUser.get("COMP_STAT"))){
					result.set("result", EUserErrorCode.LOGIN_RETIRED);
				}else if("C".equals(loginUser.get("COMP_STAT")) || "C".equals(loginUser.get("MBR_STAT"))){
					result.set("result", EUserErrorCode.LOGIN_CANCELED);
				}else if("T".equals(loginUser.get("MBR_STAT"))){
					result.set("result", EUserErrorCode.LOGIN_TERMINATE);
				}else if("N".equals(loginUser.get("USR_STAT")) & ("N".equals(loginUser.get("COMP_STAT")) || "F".equals(loginUser.get("COMP_STAT")))){
					loginUser.set("LOGIN_PW", null);
					result = loginUser;
					result.set("result", EUserErrorCode.SUCCESS);
				}
			}else{
				result.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
			}
		} catch (Exception e) {
			EUserErrorCode errorCode = result.get("result");
			log.e(getLogMessage("getLoginMemberUser", "로그인 결과 ", errorCode.getDesc()));
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 임시회원 로그인 조회(by loginId ) Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 21.
	 * @version 1.0
	 * @param tmpUsrId
	 * @param shaLoginPw
	 * @return
	 */
	/*@Override
	public SBox getLoginTempUser(Integer tmpUsrId, String shaLoginPw) {
		SBox result = new SBox();
		SBox loginUser  = new SBox();
		result.set("result", EUserErrorCode.FAIL);
		try {
			loginUser = loginDao.selectLoginTempUser(tmpUsrId);
			if(loginUser != null && !loginUser.isEmpty("TMP_USR_ID")){
				String loginPw = CommonCrypto.encryptSha1(loginUser.getString("LOGIN_PW"));
				if(loginPw.equals(shaLoginPw)){
					if("E".equals(loginUser.get("STAT"))){
						result.set("result", EUserErrorCode.GUEST_EXPIRED);
					}else if("I".equals(loginUser.get("STAT")) || "U".equals(loginUser.get("STAT"))){
						loginUser.set("LOGIN_PW", null);
						result = loginUser;
						result.set("result", EUserErrorCode.SUCCESS);
					}
				}else{
					result.set("result", EUserErrorCode.LOGIN_FAILED_PASSWD);
				}
			}else{
				result.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
			}
		} catch (Exception e) {
			EUserErrorCode errorCode = result.get("result");
			log.e(getLogMessage("getLoginTempUser", "로그인 결과 ", errorCode.getDesc()));
			e.printStackTrace();
		}
		return result;
	}*/
	
	/**
	 * <pre>
	 * 임시회원  최초 로그인정보 변경 업데이트 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 06. 24.
	 * @version 1.0
	 * @param tmpUsrId : 임시회원순번
	 * @return
	
	@Override
	public boolean modifyTmpUsrStLoginInfo(Integer tmpUsrId) {
		boolean result = false;
		SBox sBox = new SBox();
		sBox.set("tmpUsrId", tmpUsrId);
		sBox.set("stat", "U");	//사용중상태로 수정
		Integer updateResult = loginDao.updateTmpUsrStLoginInfo(sBox);
		if(updateResult != null && updateResult > 0){
			result = true;
		}
		return result;
	} */
}
