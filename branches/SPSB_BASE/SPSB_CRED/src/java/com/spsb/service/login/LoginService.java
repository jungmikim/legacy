package com.spsb.service.login;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 로그인 Service Interface Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 26.
 * @version 1.0
 */
public interface LoginService {

	public SBox getLoginMemberUser(String loginId, String shaLoginPw);
	public SBox getLoginMemberUser(String loginId);
	//public SBox getLoginTempUser(Integer tmpUsrId, String loginPw);
	//public boolean modifyTmpUsrStLoginInfo(Integer tmpUsrId);
}
