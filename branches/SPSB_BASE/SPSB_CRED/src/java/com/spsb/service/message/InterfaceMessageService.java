package com.spsb.service.message;
import com.spsb.common.collection.SBox;

/**
 * 
 * <pre>
 * 인테페이스 메시지 Service Interface Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
public interface InterfaceMessageService {
	
	
	//크레딧 서비스 조기 경보 대상 거래처 등록 요청서 CRQREC
	public void setEwCustRequestMessage(SBox paramBox);
	
	//크레딧 서비스 조기 경보 대상 거래처 등록 응답서 CRSREC
	public SBox modifyCustRegistorResponseSucess(String xmlFile);
	
	public SBox modifyCustRegistorResponseFail(String xmlFile);
	
	public void setCreditRequestUserMessage(SBox paramBox);
	
}
