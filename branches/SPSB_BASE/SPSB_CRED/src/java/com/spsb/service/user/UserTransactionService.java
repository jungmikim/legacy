package com.spsb.service.user;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 회원가입 (채권DB와 포탈DB의 분산 트랜잭션 적용하는  Service Interface Class)
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 30.
 * @version 1.0
 */
public interface UserTransactionService {

	public void addCompanyAndAdminUser(SBox compSBox, SBox userSBox) throws UserException;
	public void addCompanyUser(SBox sBox) throws UserException;
	public void addMemberUser(SBox sBox) throws UserException;
	public SBox addDebnMemberUser(SBox sBox) throws UserException;
	
	public void addCompanyAndUserForSB(SBox compSBox, SBox userSBox) throws UserException;
	public SBox addCompanyUserForSB(SBox sBox) throws UserException;
	public SBox addDebnMemberUserForSB(SBox sBox) throws UserException;
	
}
