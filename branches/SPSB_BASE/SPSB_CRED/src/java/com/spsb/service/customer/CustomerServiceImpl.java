package com.spsb.service.customer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.MessageTag;
import com.spsb.common.enumtype.EDebntureCrdHistCode;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.enumtype.EEwRatingType;
import com.spsb.common.event.MessageEvent;
import com.spsb.common.exception.BizException;
import com.spsb.common.message.CommonMessageIF;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonCode;
import com.spsb.common.util.CommonExcel;
import com.spsb.common.util.CommonPage;
import com.spsb.common.util.DateUtil;
import com.spsb.common.util.SysUtil;
import com.spsb.connector.wsc.WSClient;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.dao.user.UserDaoForMssql;
import com.spsb.dao.user.UserDaoForOracle;
import com.spsb.service.debt.DebtService;
import com.spsb.vo.EWCustRequestVo;
import com.spsb.vo.RequestCommonVo;
import com.spsb.vo.ResponseDocumentCommonVo;

/**
 * <pre>
 *	 거래처 Service Implements Class  
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 2.
 * @version 1.0
 */
@Service
public class CustomerServiceImpl extends SuperService implements CustomerService {

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	// 크레딧서비스회원목록요청서	  인터페이스 서비스 코드
	@Value("#{service['NOTICE_CONCLUSION'].trim()}")
	private String serviceCodeNoticeConclusion;
	
	@Autowired
	private CustomerDaoForOracle customerDaoForOracle;
	@Autowired
	private CustomerDaoForMssql customerDaoForMssql;

	//@Autowired
	//private DebentureDao debentureDao;

	//@Autowired
	//private DebtDao debtDao; // 채무불이행 관련 DAO DI함

	@Autowired
	private DebtService debtService; // 채무불이행 관련 Service DI 함

	@Autowired
	private UserDaoForOracle userDaoForOracle; // 유저 DAO DI함
	@Autowired
	private UserDaoForMssql userDaoForMssql;

	@Autowired
	private CommonPage commonPage;
	
	@Value("#{common['TMPFILE'].trim()}")
	private String tmpFilePath;
	
	// 크레딧서비스조기경보대상거래처등록요청서 (OUT)	  인터페이스 서비스 코드
	@Value("#{service['CREDIT_REQUEST_EW_CUSTOMER'].trim()}")
	private String serviceCodeRequestEwCustomer;
	
	// 크레딧서비스조기경보대상거래처등록응답서(IN)  인터페이스 서비스 코드
	@Value("#{service['CREDIT_RESPONSE_EW_CUSTOMER'].trim()}")
	private String serviceCodeResponseEwCustomer;
	
	// 라우팅메시지 생성을 위해 보내는이 ID
	@Value("#{service['RECEIVER_ID'].trim()}")
	private String receiveId;

	// / 라우팅메시지 생성을 위해 보내는이 이름
	@Value("#{service['RECEIVER_NAME'].trim()}")
	private String receiveName;
	
	//크레딧 서비스 코드
	@Value("#{service['SERVICE_CODE_CRED'].trim()}")
	private String serviceCodeCred;
		
	
	@Autowired
	WSClient wsClient;
	

	// 거래처 대량등록 엑셀열 갯수
	//static int CUSTOMER_CELL_NUM = 17;

	// 거래처 row Max 갯수
	//static int CUSTOMER_ROW_NUM = 1000;

	
	/*@Value("#{common['MLTFILE'].trim()}")
	private String mltFilePath;*/

	/*@Value("#{common['PRSFILE'].trim()}")
	private String prsFilePath;*/
	
	/**
	 * OVERRIDING METHOD
	 * 
	 * <pre>
	 *    거래처 검색 Service Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 19.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월 말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일],
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순, 내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지, isSessionDebnSearchGrn : 채권 전체조회 권한여부,
	 *            crdDtType : 신용정보변동이력[전체, 오늘, 직접입력], stCrdDt : 신용정보 변동이력 시작일자, edCrdDt : 신용정보 변동이력 종료일자, sessionPayTermType:세션에 저장된 결제타입, sessionPayTermDay:세션에 저장된 결제일자
	 * 
	 * @return result customerList : 거래처 검색 결과 LIST,
	 */
	@Override
	public SBox getCustomerList(SBox sBox) {

		SBox result = new SBox();
		SBoxList<SBox> customerList = null;
		SBox commonCodeBox = new SBox();
		String pcPage = null;
		
		try {

			// [1] PARAMETER 초기화
			// [1-1] 최초 로딩시에는 모든 결제조건의 CheckBox 값을 checked 할 수 있도록 함.
			if (sBox.isEmpty("num")) {
				sBox.setIfEmpty("payTermTypeAll", "ALL"); //
				sBox.setIfEmpty("payTermTypeEndOfThisMonth", "A"); //
				sBox.setIfEmpty("payTermTypeAFewDayOfThisMonth", "B"); //
				sBox.setIfEmpty("payTermTypeBeginningOfAfterMonth", "C"); //
				sBox.setIfEmpty("payTermTypeEndOfAfterMonth", "D"); //
				sBox.setIfEmpty("payTermTypeAFewDayOfAfterMonth", "E"); //
				sBox.setIfEmpty("payTermTypeRegistDate", "F"); //
			}

			// [1-2] 기타 파라미터 초기화
			sBox.setIfEmpty("num", 1); // 현재 페이지
			sBox.setIfEmpty("rowSize", 10); // 목록 갯수
			sBox.setIfEmpty("orderConditionByCust", "REG_DT"); // 정렬조건(등록날짜)
			sBox.setIfEmpty("orderType", "DESC"); // 정렬유형(내림차순)
			sBox.setIfEmpty("custSearchType", "business_all"); // 거래처 검색 유형(전체)
			sBox.setIfEmpty("searchType", "L"); // 검색 유형(리스트, 엑셀)

			// [2] 권한 조회(isSessionDebnSearchGrn : 전체 채권 조회 권한에 따른 거래처 보유 채권 개수 조정을 위함)
			/*if (!sBox.isEmpty("isSessionDebnSearchGrn") && (Boolean) sBox.get("isSessionDebnSearchGrn")) {
				sBox.set("isSessionDebnSearchGrnY", "Y"); // 전체 채권 검색 가능
			} else {
				sBox.set("isSessionDebnSearchGrnY", "N"); // 나의 채권 검색 가능
			}*/

			// [3] Common Code 초기화
			commonCodeBox.set("payTermTypeList", CommonCode.payTermTypeList);
			commonCodeBox.set("orderConditionTypeList", CommonCode.orderConditionTypeList);
			commonCodeBox.set("orderTypeList", CommonCode.orderTypeList);
			commonCodeBox.set("rowSizeTypeList", CommonCode.rowSizeTypeList);

			
			//[4] 신용정보 변동이력 오늘 건
			if(sBox.isEmpty("ewRatingType")){
				sBox.set("ewRatingType",null);
			}
			
			// [5] 거래처 검색 리스트 TOTAL COUNT 조회
			int customerTotalCount = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerTotalCount(sBox)
															   : customerDaoForMssql.selectCustomerTotalCount(sBox);

			// [6] Paging 모듈 호출
			pcPage = commonPage.getPCPagingPrint(customerTotalCount, sBox.getInt("num"), sBox.getInt("rowSize"), "getAjaxPage");

			// [7] 거래처 검색 리스트 조회
			customerList = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerList(sBox) 
													 : customerDaoForMssql.selectCustomerList(sBox);

			// [8] 공통코드 코드값 실 데이터 매핑 작업 ex) A -> 당월 초
			/*for (SBox customerBox : customerList) {
				for (SBox tempBox : CommonCode.payTermTypeList) {
					if (tempBox.getString("KEY").equals(customerBox.getString("PAY_TERM_TYPE"))) {
						customerBox.set("PAY_TERM_TYPE_VALUE", tempBox.getString("VALUE"));
					}
				}
			}*/
			
			// [9] 신용정보 변동이력 일주일 건수 조회
			int customerCrdHistoryTodayTotalCount = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerCreditHistoryTotalCount(sBox.getInt("sessionCompUsrId"), "WEEK")
																			  : customerDaoForMssql.selectCustomerCreditHistoryTotalCount(sBox.getInt("sessionCompUsrId"), "WEEK");
			
			// [10] 신용정보 변동이력 최근90일 건수 조회
			int customerCrdHistoryRecentDayTotalCount = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerCreditHistoryTotalCount(sBox.getInt("sessionCompUsrId"), "RECENT")
					                                  							  : customerDaoForMssql.selectCustomerCreditHistoryTotalCount(sBox.getInt("sessionCompUsrId"), "RECENT");

			// [11] 조회 결과 저장
			result.set("customerList", customerList);
			result.set("total", customerTotalCount);
			result.set("customerCrdHistoryTodayTotalCount", customerCrdHistoryTodayTotalCount);
			result.set("customerCrdHistoryRecentDayTotalCount", customerCrdHistoryRecentDayTotalCount);
			result.set("pcPage", pcPage);
			result.set("commonCode", commonCodeBox);

			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10199").getErrMsg() + "]");
		}
		return result;
	}

	/**
	 * OVERRIDING METHOD
	 * 
	 * <pre>
	 *    거래처 엑셀 다운로드 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 19.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월 말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일],
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순, 내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지
	 * @param fileName
	 *            : 엑셀다운로드 저장 파일이름
	 * 
	 * @return result customerList : 거래처 검색 결과 LIST,
	 */
	@Override
	public SBox getCustomerListForExcel(SBox sBox, String fileName) {

		SBox result = new SBox();
		CommonExcel poi = null;
		try {

			// Excel Type으로 프로시져 호출함
			sBox.set("searchType", "E");
			SBox customerListForExcel = this.getCustomerList(sBox);

			String columnListText = "사업자유형$C,거래처명$C,사업자등록번호$C,결제조건$C,여신한도$C,사업자 신용$C,미수금액$C,수금예정금액$C,전체채권$C,미수채권$C,등록일$C";

			// TODO 사업자 신용, 대표자 신용 기입해야함 (해당작업은 추후 배치에서 넣어준 경우 작성할 예정임)
			String columnListValue = "CUST_TYPE_VALUE,CUST_NM,CUST_NO,PAY_TERM_TYPE_VALUE,CRD_LTD_COMMA,CUST_CRD_CD,RMN_AMT,PLN_COL_AMT,DEBN_CNT,DEBN_COL_CNT,REG_DT";

			// [1] Poi Excel 파일 생성
			poi = new CommonExcel(tmpFilePath + fileName);

			// [2] Sheet Name 설정
			String sheetsName[] = { "거래처 정보" };

			poi.createSheet(sheetsName);

			// [3] Title Style 생성
			ArrayList<CellStyle> titleStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListText.split("\\,").length; i++) {

				// [3-1] titleStyle 생성
				CellStyle titleStyle = poi.createNewStyle();
				// [3-2] FONT 설정
				poi.setFontStyleSetting(titleStyle, "돋움", (short) 10, "BOLD", false, false);
				// [3-3] 배경색상 설정
				poi.setBackgroundColorSetting(titleStyle, "LIGHT-YELLOW"); // 배경색상 설정
				// [3-4] 라인두께 설정
				poi.setLineBorder(titleStyle, "THICK");

				titleStyles.add(i, titleStyle);

			}

			// [4] Title Excel 파일 기록
			poi.setBulkColsDataFromStringArray(titleStyles, 1, 1, columnListText.split("\\,"));

			// [5] Data Style 생성
			ArrayList<CellStyle> dataStyles = new ArrayList<CellStyle>();
			for (int i = 0; i < columnListValue.split("\\,").length; i++) {

				// [5-1] dataStyle 생성
				CellStyle dataStyle = poi.createNewStyle();
				// [5-2] FONT 설정
				poi.setFontStyleSetting(dataStyle, "돋움", (short) 10, "NORMAL", false, false);
				// [5-3] 배경색상 설정
				poi.setBackgroundColorSetting(dataStyle, "WHITE"); // 배경색상 설정
				// [5-4] 라인두께 설정
				poi.setLineBorder(dataStyle, "THIN");

				dataStyles.add(i, dataStyle);
			}

			// [6] Excel 파일 기록
			@SuppressWarnings("unchecked")
			Iterator<SBox> it = ((SBoxList<SBox>) customerListForExcel.get("customerList")).iterator();
			int row = 2;

			while (it.hasNext()) {
				SBox data = (SBox) it.next();
				String[] keys = columnListValue.split(",");
				data.set("CUST_TYPE_VALUE", data.getString("CUST_TYPE_VALUE") + "$C");
				data.set("CUST_NM", data.getString("CUST_NM") + "$C");
				String custNo = "";
				if (!data.isEmpty("CUST_NO")) {
					custNo = data.getString("CUST_NO");
					custNo = custNo.substring(0, 3) + "-" + custNo.substring(3, 5) + "-" + custNo.substring(5);
				}
				data.set("CUST_NO", custNo + "$C");

				// 결제조건의 유형이 A(당월 말),C(익월 초),D(익월 말)를 제외한 경우만 결제조건일자를 엑셀에 출력한다.
				String payTermType = data.getString("PAY_TERM_TYPE");
				if ("A".equals(payTermType) || "C".equals(payTermType) || "D".equals(payTermType)) {
					data.set("PAY_TERM_DAY_STR", null);
				}
				data.set("PAY_TERM_TYPE_VALUE", data.getString("PAY_TERM_TYPE_VALUE") + data.getString("PAY_TERM_DAY_STR"));
				data.set("RMN_AMT", data.getString("RMN_AMT") + "$R");
				data.set("PLN_COL_AMT", data.getString("PLN_COL_AMT") + "$R");
				data.set("CRD_LTD_COMMA", data.getString("CRD_LTD_COMMA") + "$R");
				data.set("DEBN_CNT", data.getString("DEBN_CNT") + "건$R");
				data.set("DEBN_COL_CNT", data.getString("DEBN_COL_CNT") + "건$R");
				data.set("REG_DT", data.getString("REG_DT") + "$C");
				poi.setBulkColsDataFromSBox(dataStyles, row, 1, data, keys);
				row++;
			}

			poi.write();

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10199").getErrMsg() + "]");
			ex.printStackTrace();
		} finally {
			if (poi != null) {
				try {
					poi.close();
				} catch (IOException e) {
				}
			}
		}
		return result;
	}
	

	/**
	 * OVERRIDING METHOD
	 * 
	 * <pre>
	 *    거래처 단건 등록 폼 조회 Service Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 9. 3.
	 * @version 1.0
	 * @param
	 * @return
	 */
	@Override
	public SBox getCustomerForm(SBox sBox) {
		SBox result = new SBox();
		SBox commonCodeBox = new SBox();

		try {

			// PARAMETER 초기화
			sBox.setIfEmpty("custType", "C");

			// resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

			// 공통코드 초기화[전화번호, 이메일, 핸드폰번호, 결제조건유형]
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("payTermTypeList", CommonCode.payTermTypeList);

			result.set("commonCodeBox", commonCodeBox);

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10299").getErrMsg() + "]");
		} finally {

		}
		return result;
	}

	/**
	 * <pre>
	 * 거래처 중복 체크 / 개인 주민번호 중복체크 로직 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param custNo
	 *            : 거래처 사업자 등록증 or 개인 주민번호, sessionCompUsrId : 기업회원 순번, custType : 거래처유형(P or C)
	 * @return
	 */
	@Override
	public SBox checkDuplicationCustomer(String custNo, int sessionCompUsrId, String custType) {
		SBox resultBox = new SBox();
		try {

			// [1] 사업자 중복 체크
			resultBox = "oracle".equals(dbmsType) ? customerDaoForOracle.selectDuplicationCheckCustomer(custNo, sessionCompUsrId, custType)
											      : customerDaoForMssql.selectDuplicationCheckCustomer(custNo, sessionCompUsrId, custType);
			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("사업자 중복체크 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");

		} catch (BizException biz) {
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10299").getErrMsg() + "]");
		}
		return resultBox;

	}

	/**
	 * OVERRIDING METHOD
	 * 
	 * <pre>
	 *    거래처 상세 페이지 호출 Service Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번, sessionCompUsrId : 기업회원 순번, isSessionDebnSearchGrn : 채권 조회 권한, sessionUsrId:로그인 개인회원 순번
	 * @return
	 */
	@Override
	public SBox getCustomer(SBox sBox) {
		SBox resultBox = new SBox();
		SBox commonCodeBox = new SBox();
		//String isSessionDebnSearchGrnY = "";

		try {

			// PARAMETER 초기화
			resultBox.setIfEmpty("custType", "C");

			//boolean isSessionDebnSearchGrn = (Boolean) sBox.get("isSessionDebnSearchGrn");
			String custNo = sBox.get("custNo");
			int sessionCompUsrId = sBox.getInt("sessionCompUsrId");
			int sessionUsrId = sBox.getInt("sessionUsrId");

			// [1] 공통코드 초기화[전화번호, 이메일, 핸드폰번호, 결제조건유형]
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("payTermTypeList", CommonCode.payTermTypeList);
			commonCodeBox.set("crdTypeList", CommonCode.crdTypeList);
			commonCodeBox.set("custColTypeList", CommonCode.custColTypeList);

			resultBox.set("commonCodeBox", commonCodeBox);

			// [2] 권한 조회(isSessionDebnSearchGrn : 전체 채권 조회 권한에 따른 거래처 보유 채권 개수 조정을 위함)
			/*if (isSessionDebnSearchGrn) {
				isSessionDebnSearchGrnY = "Y"; // 전체 채권 검색 가능
			} else {
				isSessionDebnSearchGrnY = "N"; // 나의 채권 검색 가능
			}*/

			// [3] 거래처 정보 조회
			SBox customerBox = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomer(custNo, sessionCompUsrId, sessionUsrId)
														 : customerDaoForMssql.selectCustomer(custNo, sessionCompUsrId, sessionUsrId);
			if (customerBox == null) {
				throw new BizException("10113", EErrorCodeType.search("10113").getErrMsg());
			}
			sBox.set("custNo", customerBox.get("CUST_NO"));
			// 거래처가 이미 삭제상태(D)인 경우 검증
			if ("D".equals(customerBox.getString("STAT"))) {
				throw new BizException("10115", EErrorCodeType.search("10115").getErrMsg());
			}

			// [4] 결제조건 정보 조회
			sBox.setIfEmpty("sessionCompUsrId", sessionCompUsrId); // 기업 회원 순번
			sBox.setIfEmpty("custNo", custNo); // 거래처 순번

			// [5] 결제 조건 정보 조회
			//SBox payTermBox = this.getPayTermList(sBox);

			// [6] 거래처 신용변동 이력 조회
			SBox customerCreditHistory = this.getCustomerAllCreditHistoryList(sBox);
			SBoxList<SBox> customerCreditHistoryList = customerCreditHistory.get("customerAllCreditHistoryList"); //유료
			
			
			// [7] 공통코드 코드값 실 데이터 매핑 작업 ex) A -> 휴폐업 여부
			for (SBox crdTypeBox : customerCreditHistoryList) {
				crdTypeBox.set("CRD_TYPE_VALUE", EEwRatingType.search(crdTypeBox.getString("EW_RATING")).getDesc());
			}

			// [8] 거래처 수금 계획 리스트 토탈 카운조회
			/*sBox.setIfEmpty("customerPlanListNum", 1); // 거래처 수금계획 페이징 번호
			sBox.setIfEmpty("rowSize", 10);
			int customerPlanListTotalCount = customerDao.selectCustomerPlanListTotalCount(sBox);*/

			// [9] 거래처 수금 계획 리스트 조회
			/*SBoxList<SBox> customerPlanList = customerDao.selectCustomerPlanList(sBox);*/

			// 공통코드 코드값 실 데이터 매핑 작업 ex) P -> 계획 중
			/*for (SBox customerPlan : customerPlanList) {
				for (SBox tempBox : CommonCode.planStatTypeList) {
					if (tempBox.getString("KEY").equals(customerPlan.getString("STAT"))) {
						customerPlan.set("STAT_NAME", tempBox.getString("VALUE"));
					}
				}
			}*/

			// [10] 거래처 수금 계획 페이징 처리
			/*String customerPlanListPcPage = commonPage.getPCPagingPrint(customerPlanListTotalCount, sBox.getInt("customerPlanListNum"), sBox.getInt("rowSize"), "getCustomerPlanAjaxPage");*/

			// [11] 거래처 독촉 리스트 토탈 카운트 조회
			sBox.setIfEmpty("customerPrsListNum", 1); // 거래처 독촉 페이징 번호
			sBox.setIfEmpty("rowSize", 10);
			//int customerPrsListTotalCount = customerDao.selectCustomerPrsListTotalCount(sBox);

			// [12] 거래처 독촉 리스트 조회
			/*SBoxList<SBox> customerPrsList = customerDao.selectCustomerPrsList(sBox);

			// [13] 거래처 독촉 유형 이름 공통코드 세팅 ex) A -> 전화
			for (SBox prsBox : customerPrsList) {
				for (SBox tempBox : CommonCode.prsTypeList) {
					if (tempBox.getString("KEY").equals(prsBox.getString("PRS_TYPE"))) {
						prsBox.set("PRS_TYPE_NAME", tempBox.getString("VALUE"));
					}
				}
			}*/

			// [14] 거래처 독촉 페이징 처리
			/*String customerPrsListPcPage = commonPage.getPCPagingPrint(customerPrsListTotalCount, sBox.getInt("customerPrsListNum"), sBox.getInt("rowSize"), "getCustomerPrsAjaxPage");*/

			// [15] 거래처 수금업무 리스트 토탈 카운트 조회
			/*sBox.setIfEmpty("customerColListNum", 1); // 거래처 독촉 페이징 번호
			sBox.setIfEmpty("rowSize", 10);
			int customerColListTotalCount = customerDao.selectCustomerColListTotalCount(sBox);
			*/
			// [16] 거래처 수금업무 리스트 조회
			//SBoxList<SBox> customerColList = customerDao.selectCustomerColList(sBox);

			// [17] 거래처 수금방식 공통코드 세팅 ex) H -> 현금
			/*for (SBox colBox : customerColList) {
				for (SBox tempBox : CommonCode.custColTypeList) {
					if (tempBox.getString("KEY").equals(colBox.getString("COL_TYPE"))) {
						colBox.set("COL_TYPE_NAME", tempBox.getString("VALUE"));
					}
				}
			}*/

			// [18] 거래처 수금업무 페이징 처리
			/*String customerColListPcPage = commonPage.getPCPagingPrint(customerColListTotalCount, sBox.getInt("customerColListNum"), sBox.getInt("rowSize"), "getCustomerColAjaxPage");*/

			// [19] 채무 불이행 조회
			/*sBox.set("statList", "AW,EV,AA,PY,NT,CA,FA,RC,FB,CB,RR"); // 진행중 상태 List나열함
			sBox.set("custId",custId); // 진행중 상태 List나열함
			*/
			/*SBox debtBox = debtDao.selectDebtDetail(sBox);
			SBox debtProcess = null;
			if (debtBox != null) {
				// 채무불이행이 존재 한다면, 현재 진행상태 조회함
				debtProcess = debtService.getDebtStatProcess(debtBox.getString("DEBT_APPL_ID"));
				log.i("[19] 현재 진행상태 조회 완료 [" + debtProcess.toString("STEP1,STEP2,STEP3,STEP4,STEP5,STEP6") + "]");
			} else {
				log.i("[19] 채무불이행 진행상태가 존재하지 않습니다.");
			}*/

			// [20] 결과값 셋팅
			resultBox.set("customerBox", customerBox); // 거래처 정보
			//resultBox.set("payTermListTotalCount", payTermBox.getInt("payTermListTotalCount")); // 결제정보 토탈 카운트
			//resultBox.set("payTermList", payTermBox.get("payTermList")); // 결제 정보
			//resultBox.set("pcPage", payTermBox.get("pcPage")); // PC 버젼 Paging 정보
			resultBox.set("customerCreditHistoryListTotalCount", customerCreditHistory.getInt("customerCreditHistoryListTotalCount")); // 거래처 신용변동 토탈 카운트 (유료/무료)
			resultBox.set("customerCreditHistoryList", customerCreditHistoryList); // 거래처 신용변동 정보(유료/무료)
			resultBox.set("historyPcPage", customerCreditHistory.get("historyPcPage")); // 신용변동 정보 PC 버젼 Paging 정보(유료/무료)

			//resultBox.set("customerPlanListTotalCount", customerPlanListTotalCount); // 거래처 수금 계획 리스트 토탈카운트
			//resultBox.set("customerPlanList", customerPlanList); // 거래처 수금 계획 리스트
			//resultBox.set("customerPlanListPcPage", customerPlanListPcPage); // 거래처 수금 계획 리스트 Paging 정보

			//resultBox.set("customerPrsListTotalCount", customerPrsListTotalCount); // 거래처 독촉 리스트 토탈카운트
			//resultBox.set("customerPrsList", customerPrsList); // 거래처 독촉 리스트
			//resultBox.set("customerPrsListPcPage", customerPrsListPcPage); // 거래처 독촉 리스트 Paging 정보

			//resultBox.set("customerColListTotalCount", customerColListTotalCount); // 거래처 수금 리스트 토탈카운트
			//resultBox.set("customerColList", customerColList); // 거래처 수금 리스트
			//resultBox.set("customerColListPcPage", customerColListPcPage); // 거래처 수금 리스트 Paging 정보

			//resultBox.set("debtBox", debtBox); // 채무불이행 정보
			//resultBox.set("debtProcess", debtProcess); // 채무불이행 현재 진행상태 정보

			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10199").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	
	
	/**
	 * <pre>
	 *  거래처 수정 페이지 내에서의 신용정보 변동 이력(유료/무료) List 조회 Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 12. 2.
	 * @version 1.0
	 * @param custId: 거래처 순번, sessionCompUsrId : 기업회원 순번, historyNum : 순번 , historyRowSize : 개수
	 * @return
	 */
	@Override
	public SBox getCustomerAllCreditHistoryList(SBox sBox) {

		SBox resultBox = new SBox();
		try {

			//boolean isSessionDebnSearchGrn = (Boolean) sBox.get("isSessionDebnSearchGrn");
			String custNo = sBox.getString("custNo");
			int sessionCompUsrId = sBox.getInt("sessionCompUsrId");
			int sessionUsrId = sBox.getInt("sessionUsrId");
			//String isSessionDebnSearchGrnY = "";

			// [2] 권한 조회(isSessionDebnSearchGrn : 전체 채권 조회 권한에 따른 거래처 보유 채권 개수 조정을 위함)
			/*if (isSessionDebnSearchGrn) {
				isSessionDebnSearchGrnY = "Y"; // 전체 채권 검색 가능
			} else {
				isSessionDebnSearchGrnY = "N"; // 나의 채권 검색 가능
			}*/
			
			SBox customerBox = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomer(custNo, sessionCompUsrId, sessionUsrId)
														 : customerDaoForMssql.selectCustomer(custNo, sessionCompUsrId, sessionUsrId);
			if (customerBox == null) {
				throw new BizException("10113", EErrorCodeType.search("10113").getErrMsg());
			}
			sBox.set("custNo", customerBox.get("CUST_NO"));
			// [1] 거래처 신용변동 이력 조회
			sBox.setIfEmpty("historyNum", 1); // 현재 페이지
			sBox.setIfEmpty("historyRowSize", 10); // 목록 갯수

			// [2] 거래처 신용변동 이력 TOTAL COUNT 조회
			SBox customerAllCreditHistoryListCount = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerAllCreditHistoryListCount(sBox)	 
												  							   : customerDaoForMssql.selectCustomerAllCreditHistoryListCount(sBox);
			if (customerAllCreditHistoryListCount.size() == 0) {
				log.i("유료/무료 거래처 신용변동 이력 정보가 존재하지 않습니다.");
			}
			
			// [3] 거래처 신용변동 이력 조회
			SBoxList<SBox> customerAllCreditHistoryList = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerAllCreditHistoryList(sBox)
																				    : customerDaoForMssql.selectCustomerAllCreditHistoryList(sBox);
			
			// [3-1] 거래처 휴폐업 코드 Enum값으로 맵핑함
			Iterator<SBox> it = customerAllCreditHistoryList.iterator();
			while(it.hasNext()){
				SBox codeBox = (SBox) it.next();
				String payType = codeBox.getString("PAY_TYPE");
				if(payType.equals("F")){
					String clsBizType = codeBox.getString("CLS_BIZ_TYPE");
					if(clsBizType != null && !clsBizType.equals("")){
						EDebntureCrdHistCode histCode = EDebntureCrdHistCode.search(clsBizType);
						if(histCode != null){
							String desc = EDebntureCrdHistCode.search(clsBizType).getDesc();
							codeBox.set("CLS_BIZ_TYPE",desc);
						}
					}
				}
			}
			
			// [4] PC 버젼 페이징 모듈 조회
			String historyPcPage = commonPage.getPCPagingPrint(customerAllCreditHistoryListCount.getInt("totalCnt"), sBox.getInt("historyNum"), sBox.getInt("historyRowSize"),"getHistoryAjaxPage"); 
			
			// [5] 공통코드 코드값 실 데이터 매핑 작업 ex) A -> 휴폐업 여부
			for (SBox crdTypeBox1 : customerAllCreditHistoryList) {
				crdTypeBox1.set("CRD_TYPE_VALUE", EEwRatingType.search(crdTypeBox1.getString("EW_RATING")).getDesc()); 
			}

			// [5] 결과 값 셋팅
			resultBox.set("customerAllCreditHistoryListCount", customerAllCreditHistoryListCount.getInt("totalCnt"));
			resultBox.set("customerAllCreditHistoryList", customerAllCreditHistoryList); // 거래처 신용변동 정보
			resultBox.set("historyPcPage", historyPcPage); // 신용변동 정보 PC 버젼 Paging 정보
			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10199").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	/**
	 * OVERRIDING METHOD
	 * <pre>
	 *    회사명·사업자등럭번호 /개인 검색 로직 Service Method
	 * </pre>
	 * @author Jungmi Kim
	 * @since 2015. 02. 10.
	 * @version 1.0
	 * @param custKwd: 검색내용
	 * @param custType: 거래처유형(P or C)
	 * @param sessionCompUsrId : 기업멤버 식별자
	 * @return
	 */
	@Override
	public SBox getCustomerSearch(SBox sBox) {

		SBox result = new SBox();
		SBoxList<SBox> customerList = null;

		try {
			// resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

			// 거래처 검색 리스트 조회
			customerList = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerListForSearch(sBox)
													 : customerDaoForMssql.selectCustomerListForSearch(sBox);
			
			log.i(customerList.println());

			// 조회 결과 저장
			result.set("customerList", customerList);

		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("20299").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 조기경보업체 등록 인터페이스 전송
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 3.
	 * @version 1.0
	 * @param sBox
	 * @throws BizException 
	 */
	@Override
	public void sendEWCusterListIf(SBox sBox) throws BizException{
		
		try{
		//SBox debtbasicInfoSBox = debtDao.selectDebt(sBox); //TODO 뽑아내야할 데이터 COMP_NO, COMP_NM
		
		 String senderId = sBox.getString("sessionUsrNo");
		 String senderName = sBox.getString("sessionCompUsrNm");
		 String instanceId = SysUtil.getInstanceId(serviceCodeRequestEwCustomer,senderId);
		 String[] custNoIfList = sBox.getString("customerIdList").split(",");
		 int custNoIfListSize = custNoIfList.length;
		 
		 
		 for(int i=0; i<custNoIfListSize; i++){
			 //라우팅 VO 생성
			 MessageEvent msgEvent = new MessageEvent();
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//ASync타입
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setGroupId(custNoIfList[i]);//sBox.getString("DEBT_APPL_ID"));//채무불이행순번
			 msgEvent.setDocumentType(serviceCodeRequestEwCustomer);//고정값
			 //msgEvent.setActionType(sBox.getString("MOD_STAT"));
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED TODO:Propertie로 변경
			 msgEvent.setTypeCode("REQ");
			 msgEvent.setFileCount(0);
			/*String usrId = "130952";
			String sbdebnId = "kbkorea";*/
			String usrId = sBox.getString("sessionSbusrId");
			String sbdebnId = sBox.getString("sessionSbLoginId");
			
			
			//1. 채무불이행 정정  메시지 파라미터 생성
			RequestCommonVo  commonVo = new RequestCommonVo();
			 commonVo.setID("크레딧서비스채무불이행등록변경요청서");
			 commonVo.setRequestDocumentTypeCode(serviceCodeRequestEwCustomer);
			 commonVo.setVersionInformation("1.0.0");
			 commonVo.setDescription(null);
			 commonVo.setUserBusinessAccountTypeCode("E");// ERP회원
			 commonVo.setUserID(usrId); //usrId 스마트채권의 회원식별자 TODO
			 commonVo.setLoginID(sbdebnId); //sbdebnId 스마트 채권의 아이디 TODO
			 commonVo.setLoginPasswordID(null);
		
	
			 EWCustRequestVo requstVo = new EWCustRequestVo();
			 requstVo.setFolderName("포털팀");
			 requstVo.setId(custNoIfList[i]);
			
			
			String strXml = CommonMessageIF.makeEwCustRequestMessageForXML(commonVo, requstVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent);
			
			SBox paramBox = new SBox();
			paramBox.set("custNo", custNoIfList[i]);
			paramBox.set("sessionCompUsrId",sBox.getString("sessionCompUsrId") );//sBox.getString("sessionCompUsrId") 추후에 조회해서 넘겨야함
			paramBox.set("compMngCd",sBox.getString("sessionCompMngCd") );//sBox.getString("compMngCd")
			paramBox.set("trans", "2"); //처리 성공, 실패 여부. 0: 실패, 1: 성공 , 2: 전송중
        	paramBox.set("reason", "Progress In Asyn Interface");
        	paramBox.set("sessionUsrId", "0");
        	SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(paramBox)
        											   : customerDaoForMssql.updateCustResponseForIF(paramBox);
			
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
			}
		}
		 
		
		} catch (IOException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10799").getErrMsg() + "]");
		}
	}
	
	
	/**
	 * <pre>
	 * 조기경보업체 등록 인터페이스 응답결과
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 30.
	 * @version 1.0
	 * @param messageTag
	 */
	@Override
	@Transactional
	public void getIFResultForCustRegister(MessageTag messageTag){
		//SBox result = new SBox();
		try{

            // XML메시지 class 구조로 파싱
            SBox xmlResult = CommonMessageIF.getEwCustRegistorReponseMessageForXML(messageTag.getDocumentData());
            
            xmlResult.set("custNo", xmlResult.get("id")); 
            xmlResult.set("rcode",xmlResult.get("typeCode"));//0:실패/ 1:성공
        	xmlResult.set("reason",xmlResult.get("description"));		
        	
            // 파라미터 생성
            SBox paramBox = new SBox();
			paramBox.set("custNo", xmlResult.getString("custNo"));
			paramBox.set("sessionCompUsrId","5" );//sBox.getString("sessionCompUsrId") 추후에 조회해서 넘겨야함
			paramBox.set("compMngCd","ERP1" );//sBox.getString("compMngCd")
			paramBox.set("trans", xmlResult.getString("rcode")); //처리 성공, 실패 여부. 0: 실패, 1: 성공 , 2: 전송중
        	paramBox.set("reason", xmlResult.getString("reason"));
        	paramBox.set("sessionUsrId", "0");
        	SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(paramBox)
        											   : customerDaoForMssql.updateCustResponseForIF(paramBox);
			
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("10800").getErrMsg() + "]");
		}
	}
	
	/**
	 * <pre>
	 * 크레딧서비스거래처신용등급변경 통지서
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 9. 25.
	 * @version 1.0
	 * @param messageTag
	 */
	@Override
	@Transactional
	public void getCreditNoticeUpdateCustomerLevel(MessageTag messageTag){
		SBoxList<SBox>	resultList = new SBoxList<SBox>();
		SBox resultBox = new SBox();
		try{

			// XML메시지 class 구조로 파싱
			resultList = CommonMessageIF.getCreditNoticeUpdateCustomerLevelForXML(messageTag.getDocumentData());
			String trans = "1";
			String rcode = "SCMN01";
			String reason = "정상 처리됨";
			
			//NoticeConclusion 응답메시지 전송
			 String senderId = messageTag.getReceiveId();
			 String senderName = messageTag.getReceiveName();
			 String instanceId = SysUtil.getInstanceId(serviceCodeNoticeConclusion,senderId); //요청 문서의 instanceId
			//라우팅 메시지 VO 생성
			MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//S/A/P
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setReferenceId(messageTag.getInstanceId());
			 msgEvent.setGroupId(messageTag.getGroupId());//채무불이행순번 필수 
			 msgEvent.setDocumentType(serviceCodeNoticeConclusion);//NOTICE_CONCLUSION
			 msgEvent.setActionType(null); //채무불이행 신청 상태
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED 
			 msgEvent.setFileCount(0);
			 msgEvent.setTypeCode("RES");
	    	
	    	ResponseDocumentCommonVo responseVo = new ResponseDocumentCommonVo();
			// 응답문서 정보 영역
			responseVo.setId(instanceId);
			responseVo.setReferenceID(messageTag.getInstanceId());
			responseVo.setRDocumentTypeCode(serviceCodeNoticeConclusion);
			responseVo.setVersionInformation("1.0.0");
			responseVo.setRDDescription("크레딧서비스거래처신용등급변경통지서");
			
			//응답 결과 영역
			responseVo.setRRDTypeCode(trans);//1정상처리
			responseVo.setResponseTypeCode(rcode);//정상 처리됨
			responseVo.setRRDDescription(reason);
			
			String strXml = CommonMessageIF.makeNoticeConclusionForXML(responseVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent); 
				

			for(SBox sBox : resultList ){
				resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustCrdForIF(sBox):
													   customerDaoForMssql.updateCustCrdForIF(sBox);
				
				if (!"00000".equals(resultBox.getString("REPL_CD"))) {
					 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "처리 중 예외발생["+EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
					throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
				}
				log.i("조기경보 등록된 거래처 EW등급 받아오기 결과: REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10801").getErrMsg() + "]");
		}
	}
	
	/**
	 * <pre>
	 * [KED연계페이지] EW(조기경보)상세
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param custNo
	 * @return 
	 */
	@Override
	//@Transactional
	public SBox getEwReportForKED(String custNo){
		SBox result = new SBox();
		try{
			result = "oracle".equals(dbmsType) ? customerDaoForOracle.selectEwReportForKED(custNo)
        									   : customerDaoForMssql.selectEwReportForKED(custNo);
			if (result.getString("EW_CD").isEmpty()) {
				throw new BizException("사업자번호 ["+custNo+"]의 EW_CD가 존재하지 않습니다.");
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("19901").getErrMsg() + "]");
		}
		return result;
	}
	
	
	
}
