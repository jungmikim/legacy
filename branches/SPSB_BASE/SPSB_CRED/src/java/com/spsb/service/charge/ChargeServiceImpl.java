package com.spsb.service.charge;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonPage;
import com.spsb.dao.charge.ChargeDaoForMssql;
import com.spsb.dao.charge.ChargeDaoForOracle;
import com.spsb.dao.product.ProductDaoForMssql;
import com.spsb.dao.product.ProductDaoForOracle;

/**
 * <pre>
 * 요금관리 Service Implements Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
public class ChargeServiceImpl extends SuperService implements ChargeService{

	@Autowired
	private ProductDaoForOracle productDaoForOracle;	
	@Autowired
	private ProductDaoForMssql productDaoForMssql;
	
	@Autowired
	private ChargeDaoForOracle chargeDaoForOracle;
	@Autowired
	private ChargeDaoForMssql chargeDaoForMssql;
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private CommonPage commonPage;
	
	/**
	 * <pre>
	 * 상품타입별 결제상품 사용가능 여부 확인  Map Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 27.
	 * @version 1.0
	 * @param compUsrId :기업회원순번
	 * @return
	 */
	@Override
	public Map<String, SBox> getUserPoCheckPerPrdTypeMap(Integer compUsrId){
				
		return "oracle".equals(dbmsType)? chargeDaoForOracle.selectUserPoCheckPerPrdTypeMap(compUsrId) 
										: chargeDaoForMssql.selectUserPoCheckPerPrdTypeMap(compUsrId);
	}
	
	/**
	 * <pre>
	 * KED 채무불이행등록수수료 조회 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @return
	 */
	@Override
	public SBoxList<SBox> getDebtRegChargeList(){
		return "oracle".equals(dbmsType)? chargeDaoForOracle.selectDebtRegChargeList() 
										: chargeDaoForMssql.selectDebtRegChargeList();
	}
	
	/**
	 * <pre>
	 * 기업회원순번의 구매리스트 조회  Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @param sBox -
	 *			compUsrId : 기업회원순번, num : 페이징 순번 , rowSize: row수
	 * @return
	 */
	@Override
	public SBox getUserPoList(SBox sBox){
		SBox result = new SBox();
		sBox.setIfEmpty("num", 1); // 현재 페이지
		sBox.setIfEmpty("rowSize", 10); // 목록 갯수
		int num = sBox.getInt("num");
		int rowSize = sBox.getInt("rowSize");
		Integer compUsrId = sBox.getInt("compUsrId");
		if(num == 0){
			sBox.set("num", 1);
		}
		int getTotalCount = "oracle".equals(dbmsType) ? chargeDaoForOracle.selectUserPoListTotalCount(compUsrId)
													  : chargeDaoForMssql.selectUserPoListTotalCount(compUsrId);
		SBoxList<SBox>	userPoList = "oracle".equals(dbmsType) ? chargeDaoForOracle.selectUserPoList(sBox)
																:chargeDaoForMssql.selectUserPoList(sBox);
		String pcPage = commonPage.getPCPagingPrint(getTotalCount, num, sBox.getInt("rowSize"), "getAjaxPage");
		result.set("userPoList", userPoList);
		result.set("pcPage", pcPage);
		result.set("getTotalCount",getTotalCount);
		result.set("num", num);
		result.set("rowSize", rowSize);
		return result;
	}
	
	/**
	 * <pre>
	 * 상품코드에 따른 회원의 구매check 결과 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 12. 2.
	 * @version 1.0
	 * @param compUsrId
	 * @param prdCd
	 * @return
	 */
	@Override
	public boolean isUserPoCheckByPrdCd(int compUsrId, String prdCd){
		int userPo = "oracle".equals(dbmsType) ? chargeDaoForOracle.selectUserPoCheckByPrdCd(compUsrId, prdCd) 
											   : chargeDaoForMssql.selectUserPoCheckByPrdCd(compUsrId, prdCd);
		if(userPo > 0){
			return true;
		}
		return false;
	}
	
}
