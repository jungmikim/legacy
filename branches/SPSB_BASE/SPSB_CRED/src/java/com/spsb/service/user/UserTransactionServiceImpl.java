package com.spsb.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.enumtype.EUserTypeCode;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperService;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.dao.user.UserDaoForMssql;
import com.spsb.dao.user.UserDaoForOracle;

/**
 * <pre>
 * 회원가입 (채권DB와 포탈DB의 분산 트랜잭션 적용하는  Service Implements Class)
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 30.
 * @version 1.0
 */
public class UserTransactionServiceImpl extends SuperService implements UserTransactionService{
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private UserDaoForOracle userDaoForOracle;
	@Autowired
	private UserDaoForMssql userDaoForMssql;
	@Autowired
	private CustomerDaoForOracle customerDaoForOracle;
	@Autowired
	private CustomerDaoForMssql customerDaoForMssql;
	
	/**
	 * <pre>
	 * 기업회회원과 Admin개인회원 등록 분산 Transaction Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 20.
	 * @version 1.0
	 * @param compSBox
	 * @param userSBox
	 * @throws UserException
	 */
	@Override
	public void addCompanyAndAdminUser(SBox compSBox, SBox userSBox) throws UserException {
		addCompanyUser(compSBox);
		if(EUserErrorCode.SUCCESS.equals(compSBox.get("result"))){
			userSBox.set("selectCompUsrId", compSBox.getInt("selectCompUsrId"));
			userSBox.set("compUsrNo", compSBox.getString("usrNo"));
			addMemberUser(userSBox);
		}
	}
	
	/**
	 * <pre>
	 * 기업회회원 등록 분산 Transaction Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 2.
	 * @version 1.0
	 * @param stat : 상태, compType : 사업자유형, usrNo : 사업자등록번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호, telNo1 : 전화번호1, telNo2 : 전화번호2, telNo3 : 전화번호3
	 * 			, signInfo : 직인정보, certInfo : 인증서정보, mbNo : 휴대폰번호, mbNo1 : 휴대폰번호1, mbNo2 : 휴대폰번호2, mbNo3 : 휴대폰번호3, postNo1 : 우편번호1, postNo2 : 우편번호2
	 * 			, addr1 : 주소1, addr2 : 주소2, ownNm : 대표자명, corpNo : 법인등록번호, bizType : 업종, bizCond : 업태
	 * @return result : 등록 결과 코드
	 */
	@Override
	public void addCompanyUser(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		sBox.set("stat", "N");	
		if (sBox.isEmpty("usrType")) {
			sBox.set("usrType", EUserTypeCode.DEFAULT.getCode());	// 사용자유형이 ERP회원이 아닌것이 default
		}
		if (sBox.isEmpty("email")) {
			sBox.set("email", null);
		}
		if (sBox.isEmpty("corpNo")) {
			sBox.set("corpNo", null);
		}
		if (sBox.isEmpty("bizCond")) {
			sBox.set("bizCond", null);
		}
		if (sBox.isEmpty("telNo") && (!sBox.isEmpty("telNo1") && !sBox.isEmpty("telNo2") && !sBox.isEmpty("telNo3"))) {
			sBox.set("telNo", sBox.getString("telNo1") + "-" + sBox.getString("telNo2") + "-" + sBox.getString("telNo3")); // 전화번호
		} else {
			if(sBox.isEmpty("telNo")){
				sBox.set("telNo", null);
			}
		}
		if (sBox.isEmpty("mbNo") && (!sBox.isEmpty("mbNo1") && !sBox.isEmpty("mbNo2") && !sBox.isEmpty("mbNo3"))) {
			sBox.set("mbNo", sBox.getString("mbNo1") + "-" + sBox.getString("mbNo2") + "-" + sBox.getString("mbNo3")); // 휴대폰번호
		} else {
			if(sBox.isEmpty("mbNo")){
				sBox.set("mbNo", null);
			}
		}
		if (sBox.isEmpty("postNo") && (!sBox.isEmpty("postNo1") && !sBox.isEmpty("postNo2"))) {
			sBox.set("postNo", sBox.getString("postNo1") + sBox.getString("postNo2")); // 거래처 우편번호
		} else {
			if (sBox.isEmpty("postNo")){
				sBox.set("postNo", null);
			}
		}
		if(sBox.isEmpty("addr1")){
			sBox.set("addr1", null);
		}
		if(sBox.isEmpty("addr2")){
			sBox.set("addr2", null);
		}
		if(sBox.isEmpty("signInfo")){
			sBox.set("signInfo", null);
		}
		if(sBox.isEmpty("payTermDay")){
			sBox.set("payTermDay", null);
		}
		if(sBox.isEmpty("certInfo")){
			sBox.set("certInfo", null);
		}
		try {
			SBox debnCompanyUser = "oracle".equals(dbmsType)? userDaoForOracle.selectCompanyUserByUsrNo(sBox.getString("usrNo")):
															  userDaoForMssql.selectCompanyUserByUsrNo(sBox.getString("usrNo"));
			if(debnCompanyUser != null && debnCompanyUser.getInt("COMP_USR_ID") > 0){
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.DUPLICATION_COMP_USR_NO);
				throw new UserException(EUserErrorCode.DUPLICATION_COMP_USR_NO);
			}else{
				Integer compUsrId = "oracle".equals(dbmsType)? userDaoForOracle.insertCompanyUser(sBox):userDaoForMssql.insertCompanyUser(sBox);
				if(compUsrId != null && compUsrId > 0){
					sBox.set("selectCompUsrId", compUsrId);
					sBox.set("result", EUserErrorCode.SUCCESS);
				}else if(compUsrId == 0){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					sBox.set("result", EUserErrorCode.DUPLICATION_COMP_USR_NO);
					throw new UserException(EUserErrorCode.DUPLICATION_COMP_USR_NO);
				}else{
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
					throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
				}
			}
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
			throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL, e);
		}
	}
	
	/**
	 * <pre>
	 * 개인과 멤버회원 등록 분산 Transaction Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 2.
	 * @version 1.0
	 * @param sBox
	 * 			usrStat : 상태, loginId : 로그인식별자, loginPw : 로그인비밀번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호, telNo1 : 전화번호1, telNo2 : 전화번호2, telNo3 : 전화번호3
	 * 			, faxNo : 팩스번호, faxNo1 : 팩스번호1, faxNo2 : 팩스번호2, faxNo3 : 팩스번호3, mbNo : 휴대폰번호, mbNo1 : 휴대폰번호1, mbNo2 : 휴대폰번호2, mbNo3 : 휴대폰번호3, postNo1 : 우편번호1, postNo2 : 우편번호2
	 * 			, addr1 : 주소1, addr2 : 주소2, erpUsrId : ERP회원순번, selectCompUsrId : 기업회원순번, admYn : 관리자여부, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위
	 * @throws UserException
	 */
	@Override
	public void addMemberUser(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		//로그인식별자가 존재하는 경우 체크
		SBox userInfo = "oracle".equals(dbmsType)? userDaoForOracle.selectUserByLoginId(sBox.getString("loginId")):
												   userDaoForMssql.selectUserByLoginId(sBox.getString("loginId"));
		boolean isDuplicationMemberUser = (userInfo == null) ? false:true;
		if(isDuplicationMemberUser){	
			sBox.set("result", EUserErrorCode.DUPLICATION_LOGIN_ID);
			throw new UserException(EUserErrorCode.DUPLICATION_LOGIN_ID);
		}
		
		//채권에서 기업회원순번
		Integer selectCompUsrId = sBox.getInt("selectCompUsrId");
		if(selectCompUsrId != null && selectCompUsrId <= 0 ){
			sBox.set("selectCompUsrId", null);	//기업회원순번
		}
		sBox.set("usrStat", "N");	//정상
		sBox.set("mbrType", "J");	//서비스 가입 회원
		if (sBox.isEmpty("email")) {
			sBox.set("email", null);
		}
		if (sBox.isEmpty("telNo") && (!sBox.isEmpty("telNo1") && !sBox.isEmpty("telNo2") && !sBox.isEmpty("telNo3"))) {
			sBox.set("telNo", sBox.getString("telNo1") + "-" + sBox.getString("telNo2") + "-" + sBox.getString("telNo3")); // 전화번호
		} else {
			if(sBox.isEmpty("telNo")){
				sBox.set("telNo", null);
			}
		}
		if (sBox.isEmpty("faxNo") && (!sBox.isEmpty("faxNo1") && !sBox.isEmpty("faxNo2") && !sBox.isEmpty("faxNo3"))) {
			sBox.set("faxNo", sBox.getString("faxNo1") + "-" + sBox.getString("faxNo2") + "-" + sBox.getString("faxNo3")); // 팩스번호
		} else {
			if(sBox.isEmpty("faxNo")){
				sBox.set("faxNo", null);
			}
		}
		if (sBox.isEmpty("mbNo") && (!sBox.isEmpty("mbNo1") && !sBox.isEmpty("mbNo2") && !sBox.isEmpty("mbNo3"))) {
			sBox.set("mbNo", sBox.getString("mbNo1") + "-" + sBox.getString("mbNo2") + "-" + sBox.getString("mbNo3")); // 휴대폰번호
		} else {
			if(sBox.isEmpty("mbNo")){
				sBox.set("mbNo", null);
			}
		}
		if (sBox.isEmpty("postNo") && (!sBox.isEmpty("postNo1") && !sBox.isEmpty("postNo2"))) {
			sBox.set("postNo", sBox.getString("postNo1") + sBox.getString("postNo2")); // 거래처 우편번호
		} else {
			if (sBox.isEmpty("postNo")){
				sBox.set("postNo", null);
			}
		}
		if(sBox.isEmpty("addr1")){
			sBox.set("addr1", null);
		}
		if(sBox.isEmpty("addr2")){
			sBox.set("addr2", null);
		}
		if(sBox.isEmpty("erpUsrId")){
			sBox.set("erpUsrId", null);
		}
		if(sBox.isEmpty("jobTlNm")){
			sBox.set("jobTlNm", null);
		}
		
		if(!sBox.isEmpty("selectCompUsrId")){
			try {
				//채권서비스에서 등록한 기업회원 정보 조회
				SBox debnCompanyUser = "oracle".equals(dbmsType)? userDaoForOracle.selectCompanyUserByUsrNo(sBox.getString("compUsrNo")):
																  userDaoForMssql.selectCompanyUserByUsrNo(sBox.getString("compUsrNo"));
				if(debnCompanyUser == null || !(debnCompanyUser.getString("STAT").equals("N") || debnCompanyUser.getString("STAT").equals("F")) ){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_NOT_EXIST);
					throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_NOT_EXIST);
				}
				sBox.set("compUsrId", debnCompanyUser.get("COMP_USR_ID"));
			} catch (Exception e) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.USERJOIN_GET_COMP_USR_FAIL);
				throw new UserException(EUserErrorCode.USERJOIN_GET_COMP_USR_FAIL, e);
			}
		}
		//채권서비스에 회원등록
		EUserErrorCode debnResult = EUserErrorCode.FAIL;
		try {
			SBox debnInfo = addDebnMemberUser(sBox);
			debnResult = debnInfo.get("resultCode");
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", debnResult);
			throw new UserException(debnResult, e);
		}
		log.i(getLogMessage("addMemberUser", "채권에 회원등록 결과", debnResult.getDesc()));
		System.out.println(getLogMessage("addMemberUser", "채권에 회원등록 결과", debnResult.getDesc()));
		sBox.set("result", debnResult);
		if(!EUserErrorCode.SUCCESS.equals(debnResult)){	//채권서비스에 회원등록 실패일 경우
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", debnResult);
			throw new UserException(debnResult);
		}
	}
	
	/**
	 * <pre>
	 * 채권DB에 개인회원 등록 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 17.
	 * @version 1.0
	 * @param sBox
	 * 			usrStat : 상태, loginId : 로그인식별자, loginPw : 로그인비밀번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호
	 * 			, faxNo : 팩스번호, mbNo : 휴대폰번호, postNo : 우편번호, addr1 : 주소1, addr2 : 주소2, erpUsrId : ERP회원순번
	 * 			, compUsrId : 기업회원순번, admYn : 관리자여부, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위, mbrType : 멤버유형
	 * @return
	 * @throws UserException 
	 */
	@Override
	public SBox addDebnMemberUser(SBox sBox) throws UserException {
		EUserErrorCode resultCode = EUserErrorCode.FAIL;
		Integer usrId = "oracle".equals(dbmsType)? userDaoForOracle.insertUser(sBox):userDaoForMssql.insertUser(sBox);
		if(usrId != null && usrId > 0){
			//System.out.println("debn:"+usrId);
			sBox.set("mbrUsrId", usrId);
			if("Y".equals(sBox.getString("admYn")) || !sBox.isEmpty("erpUsrId")){	//관리자 이거나 ERP회원은 정상 등록
				sBox.set("mbrStat", "N");	//정상
			}else{
				sBox.set("mbrStat", "P");	//승인대기
			}
			Integer mbrId = "oracle".equals(dbmsType)? userDaoForOracle.insertMbr(sBox):userDaoForMssql.insertMbr(sBox);
			if(mbrId != null && mbrId > 0){
				resultCode = EUserErrorCode.SUCCESS;
			}else{
				resultCode = EUserErrorCode.USERJOIN_DEBN_MBR_ADD_FAIL;
			}
		}else if(usrId == 0){
			resultCode = EUserErrorCode.DUPLICATION_LOGIN_ID;
		}else{
			resultCode = EUserErrorCode.USERJOIN_DEBN_USR_ADD_FAIL;
		}
		sBox.set("resultCode", resultCode);
		return sBox;
	}
	
	
	/**
	 * <pre>
	 * 기업회회원, 개인회원, 기업회원멤버 등록 분산 Transaction Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 14.
	 * @version 1.0
	 * @param compSBox
	 * @param userSBox
	 * @throws UserException
	 */
	@Override
	public void addCompanyAndUserForSB(SBox compSBox, SBox userSBox) throws UserException {
		
		//채권 서비스에 기업회원 등록 여부와 등록
		SBox debnCompanyUser = "oracle".equals(dbmsType)? userDaoForOracle.selectCompanyUserByUsrNo(compSBox.getString("usrNo")):
														  userDaoForMssql.selectCompanyUserByUsrNo(compSBox.getString("usrNo"));
		log.i(getLogMessage("addCompanyAndUserForSB", "사업자번호 조회결과", debnCompanyUser));
		System.out.println(getLogMessage("addCompanyAndUserForSB", "사업자번호 조회결과", debnCompanyUser));
		
		if(debnCompanyUser != null && debnCompanyUser.getInt("COMP_USR_ID") > 0){
			userSBox.set("selectCompUsrId", debnCompanyUser.getInt("COMP_USR_ID"));
			log.i(getLogMessage("addCompanyAndUserForSB", "기존에 존재하는 기업회원 순번 조회", debnCompanyUser.getInt("COMP_USR_ID")));
			System.out.println(getLogMessage("addCompanyAndUserForSB", "기존에 존재하는 기업회원 순번 조회", debnCompanyUser.getInt("COMP_USR_ID")));
		}else{
			SBox resultCompSBox = addCompanyUserForSB(compSBox); // 기업회원등록
			if(EUserErrorCode.SUCCESS.equals(resultCompSBox.get("result"))){
				userSBox.set("selectCompUsrId", resultCompSBox.getInt("selectCompUsrId"));
				log.i(getLogMessage("addCompanyAndUserForSB", "새로 추가한 기업회원 순번 조회", resultCompSBox.getInt("selectCompUsrId")));
				System.out.println(getLogMessage("addCompanyAndUserForSB", "새로 추가한 기업회원 순번 조회", resultCompSBox.getInt("selectCompUsrId")));
			}
		}
		
		//채권서비스에 회원등록
		EUserErrorCode debnResult = EUserErrorCode.FAIL;
		try {
			userSBox.set("compUsrNo", compSBox.getString("usrNo"));
			log.i(getLogMessage("addCompanyAndUserForSB", "compUsrNo", compSBox.getString("usrNo")));
			System.out.println(getLogMessage("addCompanyAndUserForSB", "compUsrNo", compSBox.getString("usrNo")));
			
			SBox debnInfo = addDebnMemberUserForSB(userSBox); // 개인회원, 기업회원멤버 
			debnResult = debnInfo.get("resultCode");
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw new UserException(debnResult, e);
		}
		log.i(getLogMessage("addMemberUserForSB", "채권에 회원등록 결과", debnResult.getDesc()));
		System.out.println(getLogMessage("addMemberUserForSB", "채권에 회원등록 결과", debnResult.getDesc()));
		if(!EUserErrorCode.SUCCESS.equals(debnResult)){	//채권서비스에 회원등록 실패일 경우
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			throw new UserException(debnResult);
		}
	}
	
	
	/**
	 * <pre>
	 * 기업회회원 등록 분산 Transaction Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 14.
	 * @version 1.0
	 * @param stat : 상태, compType : 사업자유형, usrNo : 사업자등록번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호, telNo1 : 전화번호1, telNo2 : 전화번호2, telNo3 : 전화번호3
	 * 			, signInfo : 직인정보, certInfo : 인증서정보, mbNo : 휴대폰번호, mbNo1 : 휴대폰번호1, mbNo2 : 휴대폰번호2, mbNo3 : 휴대폰번호3, postNo1 : 우편번호1, postNo2 : 우편번호2
	 * 			, addr1 : 주소1, addr2 : 주소2, ownNm : 대표자명, corpNo : 법인등록번호, bizType : 업종, bizCond : 업태
	 * @return result : 등록 결과 코드
	 */
	@Override
	public SBox addCompanyUserForSB(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		
		sBox.set("stat", "N");	
		if (sBox.isEmpty("email")) {
			sBox.set("email", null);
		}
		if (sBox.isEmpty("corpNo")) {
			sBox.set("corpNo", null);
		}
		if (sBox.isEmpty("bizCond")) {
			sBox.set("bizCond", null);
		}
		if(sBox.isEmpty("telNo")){
			sBox.set("telNo", null);
		}
		if(sBox.isEmpty("mbNo")){
			sBox.set("mbNo", null);
		}
		if (sBox.isEmpty("postNo")){
			sBox.set("postNo", null);
		}
		if(sBox.isEmpty("addr2")){
			sBox.set("addr2", null);
		}
		if(sBox.isEmpty("payTermDay")){
			sBox.set("payTermDay", null);
		}
		if(sBox.isEmpty("signInfo")){
			sBox.set("signInfo", null);
		}
		if(sBox.isEmpty("certInfo")){
			sBox.set("certInfo", null);
		}
		try {
			Integer compUsrId = "oracle".equals(dbmsType)? userDaoForOracle.insertCompanyUser(sBox):userDaoForMssql.insertCompanyUser(sBox);
			if(compUsrId != null && compUsrId > 0){
				sBox.set("selectCompUsrId", compUsrId);
				sBox.set("result", EUserErrorCode.SUCCESS);
			}else if(compUsrId == 0){
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.DUPLICATION_COMP_USR_NO);
				throw new UserException(EUserErrorCode.DUPLICATION_COMP_USR_NO);
			}else{
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
				throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
			}
			
			// [6] 신용변동 테이블에 해당 거래처가 존재하지 않을 시 등록
			sBox.set("custNo",sBox.get("usrNo"));
			sBox.set("custNm",sBox.get("usrNm"));
			boolean temp = "oracle".equals(dbmsType)? customerDaoForOracle.selectCustomerCreditCount(sBox.getString("custNo")) : 
													  customerDaoForMssql.selectCustomerCreditCount(sBox.getString("custNo")) ; 
			if (!temp) {
				sBox.set("stat", "R"); // 신용변동테이블의 상태 컬럼 값 setting (R:예약)
				
				SBox insertCustCrdBox = null;//= customerDao.insertCustomerCredit(sBox);
				if (!"00000".equals(insertCustCrdBox.getString("REPL_CD"))) {
					throw new BizException(insertCustCrdBox.getString("REPL_CD"), EErrorCodeType.search(insertCustCrdBox.getString("REPL_CD")).getErrMsg());
				}
				log.i("신용변동 테이블에 거래처["+sBox.getString("custNm")+"] 등록 완료");
			}
			
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL);
			throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_ADD_FAIL, e);
		}
		return sBox;
	}
	
	/**
	 * <pre>
	 * 채권DB에 개인회원 등록 Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 14.
	 * @version 1.0
	 * @param sBox
	 * 			usrStat : 상태, loginId : 로그인식별자, loginPw : 로그인비밀번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호
	 * 			, faxNo : 팩스번호, mbNo : 휴대폰번호, postNo : 우편번호, addr1 : 주소1, addr2 : 주소2, erpUsrId : ERP회원순번
	 * 			, compUsrId : 기업회원순번, admYn : 관리자여부, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위, mbrType : 멤버유형
	 * @return
	 * @throws UserException 
	 */
	@Override
	public SBox addDebnMemberUserForSB(SBox sBox) throws UserException {
		EUserErrorCode resultCode = EUserErrorCode.FAIL;
		
		sBox.set("usrStat", "N");	
		if(sBox.isEmpty("loginPw")){
			sBox.set("loginPw", null);
		}
		if(sBox.isEmpty("addr2")){
			sBox.set("addr2", null);
		}
		if(sBox.isEmpty("erpUsrId")){
			sBox.set("erpUsrId", null);
		}
		
		//개인회원 테이블  회원추가
		Integer usrId = "oracle".equals(dbmsType)? userDaoForOracle.insertUser(sBox):userDaoForMssql.insertUser(sBox);
		if(usrId != null && usrId > 0){
			sBox.set("compUsrId",sBox.getString("selectCompUsrId"));
			sBox.set("mbrUsrId", usrId);
			//if("Y".equals(sBox.getString("admYn")) || !sBox.isEmpty("erpUsrId")){	//관리자 이거나 ERP회원은 정상 등록
			sBox.set("mbrStat", "N");	//정상
			//}else{
			//	sBox.set("mbrStat", "P");	//승인대기
			//}
			sBox.set("mbrType", "J");	//서비스 가입 회원
			if (sBox.isEmpty("email")) {
				sBox.set("email", null);
			}
			if(sBox.isEmpty("telNo")){
				sBox.set("telNo", null);
			}
			if(sBox.isEmpty("faxNo")){
				sBox.set("faxNo", null);
			}
			if(sBox.isEmpty("mbNo")){
				sBox.set("mbNo", null);
			}
			if (sBox.isEmpty("postNo")){
				sBox.set("postNo", null);
			}
			if(sBox.isEmpty("addr1")){
				sBox.set("addr1", null);
			}
			if(sBox.isEmpty("addr2")){
				sBox.set("addr2", null);
			}
			if(sBox.isEmpty("erpUsrId")){
				sBox.set("erpUsrId", null);
			}
			if(sBox.isEmpty("jobTlNm")){
				sBox.set("jobTlNm", null);
			}
			if(sBox.isEmpty("deptNm")){
				sBox.set("deptNm", null);
			}
			
			//기업회원멤버 추가
			Integer mbrId = "oracle".equals(dbmsType)? userDaoForOracle.insertMbrForSB(sBox):userDaoForMssql.insertMbrForSB(sBox);
			if(mbrId != null && mbrId > 0){
				resultCode = EUserErrorCode.SUCCESS;
			}else{
				resultCode = EUserErrorCode.USERJOIN_DEBN_MBR_ADD_FAIL;
			}
		}else if(usrId == 0){
			resultCode = EUserErrorCode.DUPLICATION_LOGIN_ID;
		}else{
			resultCode = EUserErrorCode.USERJOIN_DEBN_USR_ADD_FAIL;
		}
		sBox.set("resultCode", resultCode);
		return sBox;
	}
	
}
