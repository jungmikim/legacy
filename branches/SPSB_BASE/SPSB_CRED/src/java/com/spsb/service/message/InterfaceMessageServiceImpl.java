package com.spsb.service.message;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.FileData;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.message.CommonMessageIF;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonUtil;
import com.spsb.common.util.FileUtil;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.vo.EWCustRequestVo;
import com.spsb.vo.RequestCommonVo;
import com.spsb.ws.ArrayOfManifestItem;
import com.spsb.ws.Manifest;
import com.spsb.ws.ManifestItem;
/**
 * <pre>
 * 인터페이지 메시지 Service Implements Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
public class InterfaceMessageServiceImpl extends SuperService implements InterfaceMessageService{

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private CustomerDaoForOracle customerDaoForOracle;
	@Autowired
	private CustomerDaoForMssql customerDaoForMssql;
	
	
	/**
	 * <pre>
	 * 조기경보 신규 거래처  등록  I/F ServiceImple
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 1.
	 * @version 1.0
	 * @param paramBox
	 */
	@Override
	public void setEwCustRequestMessage(SBox paramBox){
	
	SBoxList<SBox> resultList = null;
	SBox resultBox = new SBox();
	
	try {
		//1. I/F 보낼  대상 거래처 가져오기
		resultList = "oracle".equals(dbmsType)? customerDaoForOracle.selectCustomerListForIF(paramBox):
											    customerDaoForMssql.selectCustomerListForIF(paramBox);
		
		//Start For문
		for(SBox sBox : resultList ){
			String strXml= null;
			//공통 메시지 (요청 문서 정보 영역, 계정 정보 영역)
			RequestCommonVo  requestVo = new RequestCommonVo();
			 requestVo.setID("메시지 고유의 식별 번호");
			 requestVo.setRequestDocumentTypeCode("CRQREC");
			 requestVo.setVersionInformation("1.0.0");
			 requestVo.setDescription(null);
			 requestVo.setUserBusinessAccountTypeCode("E");// ERP회원
			 requestVo.setUserID("3"); //스마트채권의 회원식별자
			 requestVo.setLoginID("admin"); //스마트 채권의 아이디
			 requestVo.setLoginPasswordID(null);
			
			// 크레딧서비스 조기경보 대상 거래처 신규등록 요청 메시지
			EWCustRequestVo  ewCustRequstVo = new EWCustRequestVo();
			 ewCustRequstVo.setId(sBox.getString("CUST_NO"));
			 ewCustRequstVo.setFolderName(sBox.getString("POLDER_NM"));
			
		//2. 메시지생성 
			strXml =  CommonMessageIF.makeEwCustRequestMessageForXML(requestVo,ewCustRequstVo );
			//System.out.println("##########################"+strXml);
			
			//메시지 잘꺼내지나 테스트 TEST CODE 운영배포시 삭제 CommonMessageIF.getEwCustRequestMessageForXML(strXml);
		//TODO 3. 건당 라우팅 메시지 보내고 (비동기) 
			
			//보내고 처리결과에 따라 DE_CUST의 Trans를 E(ERP)에서 P(Progress)로 변경
			resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustResponseForIF(sBox)
												   :customerDaoForMssql.updateCustResponseForIF(sBox);

			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("크레딧서비스 조기경보 대상 거래처 신규등록 응답 메시지 상태변경 수정 응답 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
		}//END FOR문
		
		} catch (BizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10799").getErrMsg() + "]");
		}
	
	}
	
	
	/**
	 * <pre>
	 * 조기경보 대상 거래처 등록 요청에 대한 응답 메시지 : 성공 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 1.
	 * @version 1.0
	 * @param xmlFile
	 */
	@Override
	public SBox modifyCustRegistorResponseSucess(String xmlFile) {
		SBox sBox  = new SBox();
		SBox resultBox = new SBox();
		try{
			
			resultBox = CommonMessageIF.getEwCustRegistorReponseMessageForXML(xmlFile);
			//Basic Parameter
			sBox.set("compUsrId", "5");
			sBox.set("compMngCd", "ERP1");
			sBox.set("trans", "E");
			
			//XMl파싱데이터 
			sBox.set("id", resultBox.get("id"));
			sBox.set("typeCode", resultBox.get("typeCode")); //처리 성공, 실패 여부. 0: 실패, 1: 성공
			sBox.set("description", resultBox.get("description"));
			
			resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustResponseForIF(sBox):
												   customerDaoForMssql.updateCustResponseForIF(sBox);
			
			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("크레딧서비스 조기경보 대상 거래처 신규등록 응답 메시지 상태변경 수정 응답 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10800").getErrMsg() + "]");
		}
		return sBox;
	}
	
	
	
	/**
	 * <pre>
	 * 조기경보 대상 거래처 등록 요청에 대한 응답 메시지 : 실패  (비동기)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 1.
	 * @version 1.0
	 * @param xmlFile
	 */
	@Override
	public SBox modifyCustRegistorResponseFail(String xmlFile) {
		SBox sBox  = new SBox();
		SBox resultBox = new SBox();
		try{
			
			resultBox = CommonMessageIF.getNoticeConclusionForXML(xmlFile);
			//Basic Parameter
			sBox.set("compUsrId", "5");
			sBox.set("compMngCd", "ERP1");
			sBox.set("trans", "E");
			
			//XMl파싱데이터 
			sBox.set("typeCode", resultBox.get("typeCode"));
			sBox.set("responseTypeCode", resultBox.get("responseTypeCode")); //처리 성공, 실패 여부. 0: 실패, 1: 성공
			sBox.set("description", resultBox.get("description"));
			
			resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustResponseForIF(sBox)
												  :customerDaoForMssql.updateCustResponseForIF(sBox);
			
			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
			log.i("크레딧서비스 조기경보 대상 거래처 신규등록 응답 메시지 상태변경 수정 응답 결과 : REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10800").getErrMsg() + "]");
		}
		return sBox;
	}

	
	/**
	 * <pre>
	 * 크레딧서비스 회원목록 요청 - 채무불이행 전단계
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param paramBox
	 */
	@Override
	public void setCreditRequestUserMessage(SBox paramBox){
		//1. 파라미터 생성
	/*	RequestCommonVo  requestVo = new RequestCommonVo();
		 requestVo.setID("메시지 고유의 식별 번호");
		 requestVo.setRequestDocumentTypeCode("CRQREC");
		 requestVo.setVersionInformation("1.0.0");
		 requestVo.setDescription(null);
		 requestVo.setUserBusinessAccountTypeCode("E");// ERP회원
		 requestVo.setUserID("3"); //스마트채권의 회원식별자
		 requestVo.setLoginID("admin"); //스마트 채권의 아이디
		 requestVo.setLoginPasswordID(null);
		 
		EWCustRequestVo  ewCustRequstVo = new EWCustRequestVo();
		 ewCustRequstVo.setId(paramBox.getString("CUST_NO"));
		 
		//2. XML메시지 생성
		 String strXml = null;
		 strXml = CommonMessageIF.makeCreditRequestUserMessageForXML(requestVo,ewCustRequstVo );
		*/
		//TODO 3. 건당 라우팅 메시지 보내고
		 
		//4. 결과값 받아와서 분기 성공
		//결과 : 성공 : CreditResponseUserList
		 //String xmlFile= null;
		 //CommonMessageIF.getCrecditUsrListReponseMessage(xmlFile);
		 
		//결과 : 실패 : NoticeReceipt
		
	}
	
	//첨부파일 추출하는 로직
	public List<FileData> extractFiles(Manifest manifest) throws BizException {
		// 목록 valisation
		List<FileData> files = new ArrayList<FileData>();
		
		if(manifest != null){
			int seq = 1;
			
			if(manifest.getManifestItem() != null) {
				ArrayOfManifestItem itemArray = manifest.getManifestItem().getValue();
//				ArrayOfManifestItem itemArray = manifest.getManifestItem();
				
				if(itemArray != null) {
					List<ManifestItem> itemList = itemArray.getManifestItem();
					for(ManifestItem fileItem : itemList ) {
						System.out.println("fileNmae : " + fileItem.getUniformResourceID());
						try {
							FileData fileData = new FileData();
							
							fileData.setFileName(fileItem.getUniformResourceID().getValue());
//							fileData.setFileName(fileItem.getUniformResourceID());
							
//							byte[] fileBinary = fileItem.getBinaryData();
							
							DataHandler binObj = fileItem.getBinaryData().getValue();
							byte[] fileBinary = CommonUtil.toByteArray(binObj);
							
							fileData.setFileData(fileBinary);
							fileData.setFileSize(fileBinary.length);
							
							fileData.setFileSeq(fileItem.getSequenceNumber().getValue());
//							fileData.setFileSeq(fileItem.getSequenceNumber());
							
							files.add(fileData);
							// TODO : test
							File file = new File("D:/"+fileData.getFileName());
							FileUtil.write(file, fileBinary);
							System.out.println("fileSize:" +fileBinary.length);
						} catch (IOException e) {
								throw new BizException("FAIl", "첨부된 파일을 중계 플랫폼에서 처리 할 수 없음" + e.getMessage());
								//StandardBusinessDocument A = makeFaultResponseDocument(inStandardBusinessDocument, WSApplication.RESULT_FAIL, "첨부된 파일을 중계 플랫폼에서 처리 할 수 없음");
		//						return response;
						}
					}
				} else {
					// TODO: exception 처리, hasFile attribute is true but no manifest infos.
				}
			}
		} else {
			// TODO: exception 처리, hasFile attribute is true but no manifest infos.
		}
		return files;
	}
	
	
}
