package com.spsb.service.user;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 회원(개인, 기업) Service Interface Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 9.
 * @version 1.0
 */
public interface UserService {

	public SBox getCompanyUser(int compUsrId);
	public SBox getCompanyUserByUsrNo(String usrNo);
	public SBox getUserByLoginId(String loginId);
	public boolean isDuplicationCompany(String usrNo);
	public boolean isDuplicationMemberUser(String loginId);
	public boolean isDefaultMember(Integer usrId);
	public SBox addGuestUser(String email);
	public void sendEmailGuestUser(String email, String content) throws MessagingException, UnsupportedEncodingException;
}
