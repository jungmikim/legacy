package com.spsb.controller.help;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;

/**
 * <pre>
 *	고객지원 Controller 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 11.
 * @version 1.0
 */
@Controller
public class HelpController extends SuperController{

	@Value("#{linkSB['NOTICE_DOMAIN'].trim()}") 
	protected String noticeDomain;
	
	@Value("#{linkSB['FAQ_DOMAIN'].trim()}") 
	protected String faqDomain;
	
	@Value("#{linkSB['INQUIRY_DOMAIN'].trim()}") 
	protected String inquiryDomain;
	
	/**
	 * <pre>
	 * 스마트채권 SAP 프로젝트와 IFrame으로 연결
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 8.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/help/getNoticeList.do")
	public ModelAndView getNoticeList(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("help/getNoticeList");
		mav.addObject("sBox", sBox);
		mav.addObject("noticeDomain", noticeDomain);
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트채권 SAP 프로젝트와 IFrame으로 연결
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 8.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/help/getFaqList.do")
	public ModelAndView getFaqList(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("help/getFaqList");
		mav.addObject("sBox", sBox);
		mav.addObject("faqDomain", faqDomain);
		return mav;
	}
	
	
	
	/**
	 * <pre>
	 * 고객지원  1:1문의 사항 리스트   Ajax by BIZ_PORTAL
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 14.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/help/getInquiryList.do")
	public ModelAndView getInquireList(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("help/getInquiryList");
		mav.addObject("sBox", sBox);
		mav.addObject("inquiryDomain", inquiryDomain);
		return mav;
	}

	
	
}
