package com.spsb.controller.customer;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EEwRatingType;
import com.spsb.common.enumtype.ESessionNameType;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperController;
import com.spsb.common.session.SessionManager;
import com.spsb.common.util.CommonDesForSBUtil;
import com.spsb.common.util.CommonUtil;
import com.spsb.service.customer.CustomerService;
import com.spsb.service.login.LoginService;

/**
 * <pre>
 *   거래처 관리 Controller Class
 * </pre>
 * 
 * @author Jong Pil Kim
 * @since 2013. 8. 21.
 * @version 1.0
 */
@Controller
public class CustomerManagementController extends SuperController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private LoginService loginService;
	
	@Value("#{common['TMPFILE'].trim()}")
	private String tmpFilePath;

	@Value("#{common['DEBN_GUIDE_HWP_FILE'].trim()}")
	private String debnGuideHwpFilePath;

	@Value("#{common['DEBN_GUIDE_WORD_FILE'].trim()}")
	private String debnGuideWordFilePath;

	@Value("#{common['DEBN_GUIDE_WORD_MAPPING_FILE'].trim()}")
	private String debnGuideWordMappingFilePath;

	@Value("#{common['PRSFILE'].trim()}")
	private String prsFilePath;
	
	@Value("#{service['SERVICE_CODE_CRED'].trim()}")
	private String serviceCode;
	
	@Value("#{linkKed['KED_LINK_BRIF_URL'].trim()}")
	private String kedLinkBrifUrl;
	
	@Value("#{linkKed['KED_EW_DETAIL_REPORT_URL'].trim()}")
	private String kedLinkEwReportUrl;
	
	@Value("#{linkKed['KED_LINK_COMP_SEARCH_URL'].trim()}")
	private String kedLinkCompSearchUrl;

	@Value("#{linkKed['KED_CUST_MANAGEMENT_URL'].trim()}")
	private String kedCustManagementUrl;
	
	
	/**
	 * <pre>
	 *    거래처 관리 리스트 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월
	 *            말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일], orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순,
	 *            내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지
	 * 
	 * @return mav 거래처 관리 리스트 JSP 페이지 URL result : 거래처 리스트 검색 결과 sBox : 요청 파라미터 리다이렉트 셋
	 */
	@RequestMapping(value = "/customer/getCustomerList.do")
	public ModelAndView getCustomerList(@ModelAttribute("initBoxs") SBox sBox, HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("customer/getCustomerList");
		sBox.set("compId", sBox.get(ESessionNameType.USR_NO.getName()));
		sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		sBox.set("prdType","W");//조기경보
		mav.addObject("ewTypes", EEwRatingType.values());
		mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
		mav.addObject("result", customerService.getCustomerList(sBox));
		mav.addObject("sBox", sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 *    거래처 관리 리스트 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월
	 *            말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일], orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순,
	 *            내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지
	 * 
	 * @return mav 거래처 관리 리스트 JSP 페이지 URL result : 거래처 리스트 검색 결과 sBox : 요청 파라미터 리다이렉트 셋
	 */
	@RequestMapping(value = "/customer/getCustomerListAjax.do")
	public ModelAndView getCustomerListAjax(@ModelAttribute("initBoxs") SBox sBox, 
											HttpServletRequest request) {

		ModelAndView mav = new ModelAndView("customer/getCustomerListAjax");
	
		sBox.set("custNo", sBox.get(ESessionNameType.USR_NO.getName()));
		sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		sBox.set("prdType","W");//조기경보
		//SBox usrPoSbox = customerService.getUsrPoInfo(sBox);
		
		mav.addObject("ewTypes", EEwRatingType.values());
		mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
		mav.addObject("result", customerService.getCustomerList(sBox));
		mav.addObject("sBox", sBox);
		return mav;
	}


	/**
	 * <pre>
	 *    거래처 관리 엑셀 다운로드 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월
	 *            말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일], orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순,
	 *            내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지
	 * 
	 * @return mav 거래처 관리 리스트 JSP 페이지 URL result : 거래처 리스트 검색 결과 sBox : 요청 파라미터 리다이렉트 셋
	 */
	@RequestMapping(value = "/customer/getCustomerListForExcel.do")
	public void getCustomerListForExcel(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {

		String fileName = "거래처정보" + CommonUtil.getDate() + "_" + CommonUtil.getRandomString(5) + ".xls";

		try {

			ServletOutputStream res_out = response.getOutputStream();
			response.setContentType("application/vnd.ms-excel");
			String docName = getDocNameByBrowser(fileName);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");

			customerService.getCustomerListForExcel(sBox, fileName);

			FileInputStream fis = new FileInputStream(tmpFilePath + fileName);

			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					res_out.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			res_out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * <pre>
	 *   거래처 상세 페이지 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomer.do")
	public ModelAndView getCustomer(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomer");
		sBox.set("compId", sBox.get(ESessionNameType.USR_NO.getName()));
		mav.addObject("result", customerService.getCustomer(sBox));
		mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
		mav.addObject("sBox", sBox);
		// [1] 거래처 정보 print 모드 설정
		if ("Y".equals(sBox.getString("usePrint"))) {
			sBox.set("printYn", "Y");
		} else {
			sBox.set("printYn", null);
		}
		return mav;
	}
	
	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 신용정보 변동이력 (유료)List 조회 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 9. 01.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, historyNum : 순번 , historyRowSize : 개수
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerAllCreditHistoryList.do")
	@ResponseBody
	public ModelAndView getCustomerAllCreditHistoryList(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerAllCreditHistoryList(sBox));
		mav.addObject("sBox", sBox);
		
		return mav;
	}

	/**
	 * <pre>
	 *   거래처 상세 페이지 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerAjax.do")
	@ResponseBody
	public ModelAndView getCustomerAjax(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomer(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	
	/**
	 * <pre>
	 *   거래처 요약 : 채권가이드 요약 조회 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 6. 05.
	 * @version 1.0
	 * @param custid
	 *            : 거래처 순번
	 * @return
	 */
	@RequestMapping(value = "/customer/getDebentureGuideAbstract.do")
	@ResponseBody
	public ModelAndView getDebentureGuideAbstract(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result",null); // customerService.getDebentureGuideAbstract(sBox.getInt("custId")
		mav.addObject("sBox", sBox);

		return mav;
	}

	/**
	 * <pre>
	 *   거래처 요약 : 채권가이드 서브키워드 의미 조회 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 7. 17.
	 * @version 1.0
	 * @param dgsMnKwId
	 *            : 메인키워드 순번
	 * @return
	 */
	@RequestMapping(value = "/customer/getDebentureGuideKeywordMeanByMKeyword.do")
	public ModelAndView getDebentureGuideKeywordMeanByMKeyword(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getDebentureGuideKeywordMeanByMKeyword");
		//mav.addObject("result", customerService.getDebentureGuideKeywordMeanByMKeyword(sBox.getInt("dgsMnKwId")));
		mav.addObject("sBox", sBox);
		return mav;
	}

	/**
	 * <pre>
	 *   독촉 첨부파일 다운로드 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 7. 23.
	 * @param sBox
	 *            fileSn : 파일 순번
	 * @param response
	 */
	@RequestMapping(value = "/customer/getPrsFileDown.do")
	public void getPrsFileDown(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response, HttpServletRequest request) {

		try {

			SBox result = null;//customerService.getPrsFileDown(sBox.getInt("fileSn"));

			ServletOutputStream os = response.getOutputStream();
			String docName = getDocNameByBrowser(result.getString("FILE_NM"));

			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Type", "application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");

			FileInputStream fis = new FileInputStream(prsFilePath + result.getString("FILE_PATH"));

			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					os.write(outputByte, 0, 4096);
				}
			}

			//fis.close();
			os.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	/**
	 * <pre>
	 * 조기경보 서비스를 결제한 사용자 인지 여부 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014.10.14
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getUserPoInfo.do")
	@ResponseBody
	public ModelAndView getUserPoInfo(@ModelAttribute("initBoxs") SBox sBox,
			 						  @RequestParam(value = "custNo", required = true) String custNo) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox paramVo = new SBox();
		paramVo.set("custNo", custNo);
		paramVo.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		paramVo.set("prdType","W");//조기경보
		mav.addObject("result", null ); //customerService.getUsrPoInfo(paramVo)
		return mav;
	}
	
	/**
	 * <pre>
	 *  거래처 조기경보서비스 검색하는 로직
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 12. 3.
	 * @version 1.0
	 * @param sBox
	 * @param custNo
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustCreditUserInfo.do")
	@ResponseBody
	public ModelAndView getCustCreditUserInfo(@ModelAttribute("initBoxs") SBox sBox,
			 						  		  @RequestParam(value = "custNo", required = true) String custNo) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox paramVo = new SBox();
		paramVo.set("custNo", custNo);
		paramVo.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		//mav.addObject("poResult",  customerService.getUsrPoInfoCheck(sBox.getInt("sessionCompUsrId")));
		mav.addObject("result",  null); //customerService.getCustCreditUserInfo(paramVo)
		return mav;
	}
	
	/**
	 * <pre>
	 * 조기경보 서비스를 결제한 사용자 인지 여부 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014.10.14
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getUserPoInfoForTop.do")
	@ResponseBody
	public ModelAndView getUserPoInfo(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox paramVo = new SBox();
		
		paramVo.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		paramVo.set("custNo", sBox.get(ESessionNameType.USR_NO.getName()));
		
		StringBuffer valueCreditInfo = new StringBuffer();
		StringBuffer valueRegister = new StringBuffer();
		
		//기업정보검색 결제내역 조회
		paramVo.set("prdType", "C");//기업정보
		SBox resultC = null;//customerService.getUsrPoInfo(paramVo);
		
		//조기경보 결제내역 조회
		paramVo.set("prdType", "W");//조기경보
		SBox resultW  = null;//customerService.getUsrPoInfo(paramVo);
		
		String decodeCreditInfo =resultC.getString("REQ_ID").trim()+"$"+sBox.getString("sessionUsrNo");
		String decodeRegister =resultW.getString("REQ_ID").trim();
		
		valueCreditInfo.append(decodeCreditInfo);
		valueRegister.append(decodeRegister);
		
		byte[] b = valueCreditInfo.toString().getBytes();
		byte[] c = valueRegister.toString().getBytes();
		
		String encoderCreditInfo = new String(Base64.encodeBase64(b));
		String encoderRegister = new String(Base64.encodeBase64(c));
		try {
			encoderCreditInfo = URLEncoder.encode(encoderCreditInfo, "UTF-8");
			encoderRegister = URLEncoder.encode(encoderRegister, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.e(e.getMessage());
		}
		
		/*String ewCustCreditInfoUrl = kedLinkDomain+kedCompSearchUrl+encoderCreditInfo;
		log.i("ewRegisterUrl"+ewCustCreditInfoUrl);
		String ewRegisterUrl = kedLinkDomain+kedEwUrl+encoderRegister;
		log.i("ewCustCreditInfoUrl"+ewRegisterUrl);*/
		
		/*mav.addObject("ewRegisterUrl", ewRegisterUrl);
		mav.addObject("ewCustCreditInfoUrl", ewCustCreditInfoUrl);*/
		mav.addObject("resultC",  resultC);
		mav.addObject("resultW",  resultW);
		return mav;
	}
	
	/**
	 * <pre>
	 * 조기경보 무료회원 EW상세 팝업창 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 10. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerEwReportUnpaidPopup.do")
	public ModelAndView getCustomerEwReportUnpaidPopup(@ModelAttribute("initBoxs") SBox sBox,
													   @RequestParam(value = "ewCd", required = true) String ewCd,
													   @RequestParam(value = "ewDt", required = true) String ewDt) {
		ModelAndView mav = new ModelAndView("/customer/getCustomerEwReportUnpaidPopup");
		sBox.set("ewCd", ewCd);
		sBox.set("ewDt", ewDt);
		
		//조기경보 결제내역 조회
		SBox paramVo = new SBox();
		paramVo.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		//paramVo.set("custNo", sBox.get(ESessionNameType.USR_NO.getName())); //로그인회원의 사업자번호
		paramVo.set("prdType", "W");//조기경보
		StringBuffer valueRegister = new StringBuffer();
		
		//SBox resultW = customerService.getUsrPoInfo(paramVo);

		String decodeRegister= null;//=resultW.getString("REQ_ID");
		valueRegister.append(decodeRegister);//decodeRegister
		
		byte[] b = valueRegister.toString().getBytes();
		String encoderRegister = new String(Base64.encodeBase64(b));
		try {
			encoderRegister = URLEncoder.encode(encoderRegister, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.e(e.getMessage());
		}
		//String kedUrl = kedLinkDomain+kedEwUrl+encoderRegister;
		//mav.addObject("kedUrl", kedUrl);
		mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
		//mav.addObject("result", customerService.getCustCrdHistForEW(sBox));
		//mav.addObject("resultW",  resultW);
		mav.addObject("sBox", sBox);
		//mav.addObject("aspKedLinkDomain", aspKedLinkDomain);
		//mav.addObject("kedEwRegisterUrl", kedEwRegisterUrl);

		return mav;
	}
	
	
	/**
	 * <pre>
	 * 스마트빌 연동 조기경보 무료회원 EW상세 팝업창 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 10. 15.
	 * @version 1.0
	 * @param sBox - custNo: 거래처 사업자번호, ewDt:기준일자
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustEwReportPopupForSB.do")
	public ModelAndView removeCustomerForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/customer/getCustomerEwReportUnpaidPopup");
		String resultMsg = "스마트빌 조기경보 무료회원 조회 성공 하였습니다.";  
		log.i(getLogMessage("getCustEwReportPopupForSB", "value", value));
		try{
			
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("getCustEwReportPopupForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
			
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String custNo  = paramSBox.getString("CUST_NO");
			String ewDt = paramSBox.getString("EW_DT");
			String loginId = paramSBox.getString("LOGIN_ID");
			String param = custNo  +"|" + ewDt+"|" + loginId;
			log.i(getLogMessage("getCustEwReportPopupForSB", "param", param));
			
			SBox result = loginService.getLoginMemberUser(loginId);
			if(result == null){
				log.i(getLogMessage("getCustEwReportPopupForSB", "result", "is null."));
				mav.setViewName("/user/interfaceConnectionErrorurl");
			}else{
				EUserErrorCode resultCode = result.get("result");
				if(EUserErrorCode.SUCCESS.equals(resultCode)){
					SessionManager.getInstance(request, result, sessionMaxInactiveTime);
					
					SBox paramVo = new SBox();
					paramVo.set("compUsrId", result.get("COMP_USR_ID"));
					paramVo.set("custNo", custNo);
					paramVo.set("ewDt", ewDt);
					paramVo.set("prdType", "W");//조기경보
					StringBuffer valueRegister = new StringBuffer();
					//SBox resultW = customerService.getUsrPoInfo(paramVo);
					
					String decodeRegister = null;//=resultW.getString("REQ_ID");
					valueRegister.append(decodeRegister);
					
					byte[] b = valueRegister.toString().getBytes();
					String encoderRegister = new String(Base64.encodeBase64(b));
					try {
						encoderRegister = URLEncoder.encode(encoderRegister, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						log.e(e.getMessage());
					}
					
					//String kedUrl = kedLinkDomain+kedEwUrl+encoderRegister;
					//mav.addObject("kedUrl", kedUrl);
					
					//mav.addObject("custResult", customerService.getCustCreditUserInfo(paramVo) );
					mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
					//mav.addObject("result", customerService.getCustCrdHistForSB(paramVo));
					mav.addObject("paramVo", paramVo);
					//mav.addObject("resultW",  resultW);
					//mav.addObject("aspKedLinkDomain", aspKedLinkDomain);
					//mav.addObject("kedEwRegisterUrl", kedEwRegisterUrl);
					
				}else if(EUserErrorCode.LOGIN_NOT_EXIST.equals(resultCode)){
					log.i(getLogMessage("getCustEwReportPopupForSB", "login", "not exist."));
					mav.setViewName("/user/interfaceConnectionErrorurl");
				}else{
					mav.addObject("message", resultCode == null ? EUserErrorCode.FAIL.getDesc() : resultCode.getDesc());
					log.i(getLogMessage("getCustEwReportPopupForSB", "making session", "fail."));
					mav.setViewName("/user/interfaceConnectionErrorurl");
				}
			}

		}catch(Exception e) {
			resultMsg = "스마트빌 조기경보 무료회원 조회 실패하였습니다.";     
			mav.setViewName("/user/interfaceConnectionErrorurl");
			e.printStackTrace();
		};
		mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}
	
	/**
	 * <pre>
	 * 회사명+사업자등록번호 찾기 팝업
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 11.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerSearchForm.do")
	public ModelAndView getCustomerSearchForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("customer/getCustomerSearchForm");
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 회사명+사업자등록번호 찾기 팝업화면 보여주는 부분
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerSearch.do")
	@ResponseBody
	public ModelAndView getCustomerSearch(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonview");
		SBox resultBox = customerService.getCustomerSearch(sBox);
		mav.addObject("resultBox", resultBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * SB_DEBN_ERP 프로젝트 연결 [기업정보검색]
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustCreditInfoConnection.do")
	public ModelAndView getCustCreditInfoConnection(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("customer/getCustCreditInfoConnection");
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 조기경보업체등록 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 3.
	 * @version 1.0
	 * @param sBox
	 * @param request
	 * @return
	 * @throws BizException 
	 */
	@RequestMapping(value = "/customer/sendEWCusterListIf.do")
	public ModelAndView sendEWCustIf(@ModelAttribute("initBoxs") SBox sBox) throws BizException {
		ModelAndView mav = new ModelAndView("redirect:/customer/getCustomerList.do");
		
		/*sBox.set("custNo", sBox.get(ESessionNameType.USR_NO.getName()));
		sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		sBox.set("prdType","W");//조기경보
		//SBox usrPoSbox = customerService.getUsrPoInfo(sBox);
		
		mav.addObject("ewTypes", EEwRatingType.values());
		mav.addObject("ewDescMap", EEwRatingType.getEwStatMap());
		mav.addObject("result", customerService.getCustomerList(sBox));*/
		//mav.addObject("result", customerService.sendEWCusterListIf(sBox));
		customerService.sendEWCusterListIf(sBox);
		mav.addObject("sBox", sBox);
		return mav;
	}
	
	
	/**
	 * <pre>
	 * [KED연계페이지] EW(조기경보)상세
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param compId : 본사 사업자번호
	 * @param bizNo : 거채처 사업자번호
	 * @return
	 */
	@RequestMapping(value = "/customer/getEwReportForKED.do")
	public ModelAndView getEwReportForKED(@RequestParam(value = "compId", required = true) String compId,
										  @RequestParam(value = "bizNo", required = true) String bizNo) {
		ModelAndView mav = new ModelAndView();
		
		String param = compId+  "|" + bizNo;
		log.i(getLogMessage("getEwReportForKED", "param", param));
		
		
		if(compId.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] EW(조기경보)상세  /n 필수 파라메터 중 본사 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((compId.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] EW(조기경보)상세  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		if(bizNo.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] EW(조기경보)상세  /n 필수 파라메터 중 거래처 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((bizNo.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] EW(조기경보)상세  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		SBox paramSBox = customerService.getEwReportForKED(bizNo);
		
		if(!paramSBox.getString("EW_CD").isEmpty()){ // 필수값이 존재하면	paramSBox
			paramSBox.set("viewType", "EWRP");
			//KED필수 파라미터[BizNo:거래처, ewCd:EW등급, compId:본사, srvCd:서비스코드] ex) compId=2208758882&ewCd=EWN000000213066&compId=1048651732&srvCd=CRED
			String kedEwDetailUrl = kedLinkEwReportUrl+bizNo+"&ewCd="+paramSBox.getString("EW_CD")+"&compId="+compId+"&srvCd="+serviceCode;
			log.i(getLogMessage("getEwReportForKED", "param[kedEwDetailUrl]", kedEwDetailUrl));
			
			mav.addObject("kedEwDetailUrl", kedEwDetailUrl);
			mav.addObject("sBox", paramSBox);
			mav.setViewName("/customer/commonKEDViews");
			return mav;
		}else{
			log.i(getLogMessage("getEwReportForKED","EW_CD","가 존재하지 않습니다."));
			mav.setViewName("/error/msgError");
			return mav;
		}
	}
	
	
	/**
	 * <pre>
	 * [KED연계페이지] 기업정보 브리핑
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param compId : 본사 사업자번호
	 * @param bizNo : 거채처 사업자번호
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerBrifForKED.do")
	public ModelAndView getCustomerBrifForKED(@RequestParam(value = "compId", required = true) String compId,
										      @RequestParam(value = "bizNo", required = true) String bizNo) {
		ModelAndView mav = new ModelAndView();
		
		String param = compId+  "|" + bizNo;
		log.i(getLogMessage("getCustomerBrifForKED", "param", param));
		
		
		if(compId.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보 브리핑  /n 필수 파라메터 중 본사 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((compId.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보 브리핑  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		if(bizNo.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보 브리핑  /n 필수 파라메터 중 거래처 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((bizNo.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보 브리핑  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		SBox paramSBox = customerService.getEwReportForKED(bizNo);
		
		if(!paramSBox.getString("EW_CD").isEmpty()){ // 필수값이 존재하면	paramSBox
			paramSBox.set("viewType", "BRIF");
			//필수 파라미터[ kedCd, compId:본사사업자번호, srvCd] ex)  kedCd=0000841904&compId=1048651732&srvCd=CRED
			String kedCompBrifUrl = kedLinkBrifUrl+paramSBox.getString("KED_CD")+"&compId="+compId+"&srvCd="+serviceCode;
			log.i(getLogMessage("getCustomerBrifForKED", "param[kedCompBrifUrl]", kedCompBrifUrl));
			
			mav.addObject("kedCompBrifUrl", kedCompBrifUrl);
			mav.addObject("sBox", paramSBox);
			mav.setViewName("/customer/commonKEDViews");
			return mav;
		}else{
			log.i(getLogMessage("kedLinkBrifUrl","KED_CD","가 존재하지 않습니다."));
			mav.setViewName("/error/msgError");
			return mav;
		}
	}
	
	/**
	 * <pre>
	 * [KED연계페이지] 조기경보관리
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param compId : 본사 사업자번호
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerSearchForKED.do")
	public ModelAndView getCustomerSearchForKED(@RequestParam(value = "compId", required = true) String compId) {
		ModelAndView mav = new ModelAndView();
		
		String param = compId;
		log.i(getLogMessage("getCustomerSearchForKED", "param", param));
		
		
		if(compId.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보검색  /n 필수 파라메터 중 본사 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((compId.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] 기업정보검색  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		
		SBox paramSBox = new SBox();
		
		paramSBox.set("viewType", "SRCH");
		//필수 파라미터[ kedCd, compId:본사사업자번호, srvCd] ex)  compId=1048651732&srvCd=CRED
		String kedCustomerSearchUrl = kedLinkCompSearchUrl+compId+"&srvCd="+serviceCode;
		log.i(getLogMessage("getCustomerSearchForKED", "param[kedCustomerSearchUrl]", kedCustomerSearchUrl));
		
		mav.addObject("kedCustomerSearchUrl", kedCustomerSearchUrl);
		mav.addObject("sBox", paramSBox);
		mav.setViewName("/customer/commonKEDViews");
		return mav;
	}
	
	/**
	 * <pre>
	 * [KED연계페이지] 기업정보검색
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param bizNo : 본사 사업자번호
	 * @return
	 */
	@RequestMapping(value = "/customer/getCustomerManagementForKED.do")
	public ModelAndView getCustomerManagementForKED(@RequestParam(value = "compId", required = true) String compId) {
		ModelAndView mav = new ModelAndView();
		
		String param = compId;
		log.i(getLogMessage("getCustomerManagementForKED", "param", param));
		
		
		if(compId.isEmpty()){
			mav.setViewName("/error/error");
			//[KED연계페이지] 조기경보관리  /n 필수 파라메터 중 본사 사업자번호가 존재하지 않습니다.
			return mav;
		}else if(!((compId.trim()).length()==10)){
			mav.setViewName("/error/error");
			//[KED연계페이지] 조기경보관리  /n 필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.
			return mav;
		}
		
		
		SBox paramSBox = new SBox();
		
		paramSBox.set("viewType", "CUMA");
		//필수 파라미터[compId:본사사업자번호, srvCd] ex)  compId=1048651732&srvCd=CRED
		String kedCustomerManagementUrl = kedCustManagementUrl+compId+"&srvCd="+serviceCode;
		log.i(getLogMessage("getCustomerManagementForKED", "param[kedCustomerManagementUrl]", kedCustomerManagementUrl));
		
		mav.addObject("kedCustomerManagementUrl", kedCustomerManagementUrl);
		mav.addObject("sBox", paramSBox);
		mav.setViewName("/customer/commonKEDViews");
		return mav;
	}
	
	
	
	
	
	/**
	 * <pre>
	 *   거래처 삭제 Controller Method
	 *   removeCustomer.do 의 파라미터로 custId = 10,11,33 과 같이 처리함.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 21.
	 * @version 1.0
	 * @param
	 * @return
	 */
/*	@RequestMapping(value = "/customer/removeCustomer.do")
	public ModelAndView removeCustomer(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("redirect:/customer/getCustomerList.do");
		
		mav.addObject("result",
				customerService.removeCustomer(sBox.getString("customerIdList"), sBox.getInt("sessionCompUsrId"), sBox.getInt("sessionUsrId")));
		mav.addObject("sBox", sBox);
		
		return mav;
	}
*/
	

	/**
	 * <pre>
	 *   거래처 수정 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,ownNo : 주민등록번호 ,corpNo : 법인등록번호 ,eMail : 이메일 ,postNo :
	 *            우편번호 ,custAddr1 : 회사주소 앞자리 ,custAddr2 : 회사주소 뒷자리 ,bizType : 업종 ,bizCond : 업태 ,telNo : 전화번호 ,faxNo : 팩스번호 ,crdLtd : 여신한도
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyCustomer.do")
	@ResponseBody
	public ModelAndView modifyCustomer(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.modifyCustomer(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/
	
	/**
	 * <pre>
	 *   거래처 수정(여신한도) Controller Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param sessionCompUsrId, crdLtd : 여신한도
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyCreditInfo.do")
	@ResponseBody
	public ModelAndView modifyCreditInfo(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.modifyCreditInfo(sBox));
		mav.addObject("sBox", sBox);
		
		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건[결제 조건이 있는 경우] 등록 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermType : 결제조건 유형 , payTermDay : 결제 조건 기준일, payTermDate : 결제 조건 적용날짜
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addPayTerm.do")
	@ResponseBody
	public ModelAndView addPayTerm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.addPayTerm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 삭제 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermSn : 결제조건순번, num : 현재 페이지 순번, rowSize : 목록 갯수
	 * @return
	 */
	/*@RequestMapping(value = "/customer/removePayTerm.do")
	@ResponseBody
	public ModelAndView removePayTerm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.removePayTerm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 수정 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermSn : 결제조건순번, num : 현재 페이지 순번, rowSize : 목록 갯수,payTermType : 결제 조건 유형, payTermDay : 결제
	 *            조건 기준일, payTermDate : 결제 조건 적용날짜
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyPayTerm.do")
	@ResponseBody
	public ModelAndView modifyPayTerm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.modifyPayTerm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 List 조회 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 29.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, num : 순번 , rowSize : 개수
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getPayTermList.do")
	@ResponseBody
	public ModelAndView getPayTermList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getPayTermList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	
	

	/**
	 * <pre>
	 *   거래처 수정 페이지 내에서의 결제조건[결제 조건이 없는 경우] 등록 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 14.
	 * @param custId
	 *            : 거래처 순번, initPayTermType : 초기 결제조건 유형, initPayTermDay : 초기 결제 조건 기준일, futurePayTermType : 차후 결제조건 유형, futurePayTermDay : 차후 결제 조건
	 *            기준일, payTermDate : 결제 조건 적용날짜
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addFirstPayTerm.do")
	@ResponseBody
	public ModelAndView addFirstPayTerm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.addFirstPayTerm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수정 페이지 내에서의 수금계획 List 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 17.
	 * @param custId
	 *            : 거래처 순번, customerPlanListNum : 거래처 수금계획 페이지 번호
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPlanList.do")
	@ResponseBody
	public ModelAndView getCustomerPlanList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		//mav.addObject("result", customerService.getCustomerPlanList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 계획 작성/수정 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 17.
	 * @param custId
	 *            : 거래처 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPlanForm.do")
	public ModelAndView getCustomerPlanForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerPlanForm");
		mav.addObject("result", customerService.getCustomerPlanForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 계획 작성/수정을 위한 채권 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 18.
	 * @version 1.0
	 * @param custid
	 *            : 거래처 순번, orderCondition : 정렬기준, orderType : 정렬순서, bondMbrId : 채권담당자
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getDebnListForCustomerPlan.do")
	@ResponseBody
	public ModelAndView getDebnListForCustomerPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerPlanForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금계획 등록 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param insertParam
	 *            : 채권 수금 예정 Insert 파라미터, custId : 거래처 순번, totUnpdAmt : 미수총금액, plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, rmkTxt : 비고, debnCnt : 채권수
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addCustomerPlan.do")
	@ResponseBody
	public ModelAndView addCustomerPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.addCustomerPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금계획 수정 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param insertParam
	 *            : 채권 수금 예정 Update 파라미터, updateParam : 채권 수금 예정 Update 파라미터, deleteParam : 채권 수금 예정 Delete 파라미터, custId : 거래처 순번, totUnpdAmt : 미수총금액,
	 *            plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, totUnpdAmt : 미수총금액, debnCnt : 선택채권수, rmkTxt : 비고, custPlnSn : 거래처 수금 계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyCustomerPlan.do")
	@ResponseBody
	public ModelAndView modifyCustomerPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.modifyCustomerPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 계획 연기 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param custId
	 *            : 거래처 순번, custPlnSn : 거래처 수금계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPlanDelayForm.do")
	public ModelAndView getCustomerPlanDelayForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerPlanDelayForm");
		mav.addObject("result", customerService.getCustomerPlanDelayForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 계획 연기 상세 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param custId
	 *            : 거래처 순번, custPlnSn : 거래처 수금계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPlanDelayDetail.do")
	public ModelAndView getCustomerPlanDelayDetail(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerPlanDelayForm");
		mav.addObject("result", customerService.getCustomerPlanDelayDetail(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금계획 연기 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param insertParam
	 *            : 채권 수금 예정 Update 파라미터, custId : 거래처 순번, totUnpdAmt : 미수총금액, plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, totUnpdAmt : 미수총금액, debnCnt
	 *            : 선택채권수, rmkTxt : 비고, custPlnSn : 거래처 수금 계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/delayCustomerPlan.do")
	@ResponseBody
	public ModelAndView delayCustomerPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.delayCustomerPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금계획 삭제 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param custId
	 *            : 거래처 순번, custPlnSn : 거래처 수금계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/removeCustomerPlan.do")
	@ResponseBody
	public ModelAndView removeCustomerPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.removeCustomerPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 작성/상세 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 24.
	 * @param custId
	 *            : 거래처 순번, custPlnSn : 거래처 수금 계획 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerColForm.do")
	public ModelAndView getCustomerColForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerColForm");
		mav.addObject("result", customerService.getCustomerColForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
*/
	/**
	 * <pre>
	 *   거래처 수금 작성을 위한 채권 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @version 1.0
	 * @param custid
	 *            : 거래처 순번, orderCondition : 정렬기준, orderType : 정렬순서, bondMbrId : 채권담당자
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getDebnListForCustomerCol.do")
	@ResponseBody
	public ModelAndView getDebnListForCustomerCol(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerColForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금 등록 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @param insertParam
	 *            : 채권 수금 예정 Insert 파라미터, custId : 거래처 순번, colDt : 수금일자 colAmt : 수금액, colType : 수금방식, debnCnt : 채권 건수, rmkTxt : 비고, custPlnSn :
	 *            수금예정순번, plnStatType : 거래처 수금 계획 상태
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addCustomerCol.do")
	@ResponseBody
	public ModelAndView addCustomerCol(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.addCustomerCol(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 관리 작성/수정 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @param custId
	 *            : 거래처 순번, custPrsSn : 독촉 관리 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPrsForm.do")
	public ModelAndView getCustomerPrsForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerPrsForm");
		mav.addObject("result", customerService.getCustomerPrsForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 독촉 작성 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @param mltRequest
	 * @param sBox
	 *            insertParam : INSERT 채권, custId : 거래처 순번, prsType : 독촉 유형, rmkTxt : 비고, prsDt : 독촉일자, prsFile : 첨부파일
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addCustomerPrs.do")
	@ResponseBody
	public void addCustomerPrs(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response, MultipartHttpServletRequest mltRequest)
			throws IOException {

		sBox.set("prsFile", ((mltRequest.getFiles("prsFile").size() > 0) ? mltRequest.getFiles("prsFile") : null));

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = customerService.addCustomerPrs(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());

	}*/

	/**
	 * <pre>
	 *   거래처 수정 페이지 내에서의 독촉 List 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @param custId
	 *            : 거래처 순번, customerPrsListNum : 거래처 독촉 페이지 번호
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerPrsList.do")
	@ResponseBody
	public ModelAndView getCustomerPrsList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerPrsList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 작성/수정을 위한 채권 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param custid
	 *            : 거래처 순번, orderCondition : 정렬기준, orderType : 정렬순서, bondMbrId : 채권담당자
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getDebnListForPrs.do")
	@ResponseBody
	public ModelAndView getDebnListForPrs(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerPrsForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 독촉 수정 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param sBox
	 *            insertDebnParam : 채권 독촉 Insert 파라미터, deleteDebnParam : 채권 독촉 Delete 파라미터, deleteFileParam : Delete 대상 첨부파일 custId : 거래처 순번, debnCnt
	 *            : 선택채권수, rmkTxt : 비고, custPrsSn : 거래처 독촉 순번, prsDt : 독촉일자, prsType : 독촉유형, prsFile : 첨부파일
	 * 
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyCustomerPrs.do")
	@ResponseBody
	public void modifyCustomerPrs(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response, MultipartHttpServletRequest mltRequest)
			throws IOException {

		sBox.set("prsFile", ((mltRequest.getFiles("prsFile").size() > 0) ? mltRequest.getFiles("prsFile") : null));

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = customerService.modifyCustomerPrs(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());

	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 독촉 삭제 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param custId
	 *            : 거래처 순번, custPrsSn : 거래처 독촉 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/removeCustomerPrs.do")
	@ResponseBody
	public ModelAndView removeCustomerPrs(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.removeCustomerPrs(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금업무 작성/수정 팝업 호출 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param custId
	 *            : 거래처 순번, custColSn : 수금업무 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerColNoPlanForm.do")
	public ModelAndView getCustomerColNoPlanForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/customer/getCustomerColNoPlanForm");
		mav.addObject("result", customerService.getCustomerColNoPlanForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 계획중이 아닌 수금 등록 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param insertParam
	 *            : 채권 수금 Insert 파라미터, custId : 거래처 순번, colDt : 수금일자 colAmt : 수금액, colType : 수금방식, debnCnt : 채권 건수, rmkTxt : 비고, statType : 거래처 수금 상태
	 * @return
	 */
	/*@RequestMapping(value = "/customer/addCustomerColNoPlan.do")
	@ResponseBody
	public ModelAndView addCustomerColNoPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.addCustomerColNoPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수정 페이지 내에서의 수금 List 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param custId
	 *            : 거래처 순번, customerColListNum : 거래처 수금 페이지 번호
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerColList.do")
	@ResponseBody
	public ModelAndView getCustomerColList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerColList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금 수정 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 02.
	 * @param insertParam
	 *            : 채권 수금 Insert 파라미터, updateParam : 채권 수금 Update 파라미터, deleteParam : 채권 수금 예정 Delete 파라미터, custId : 거래처 순번, colDt : 수금 일자, colAmt :
	 *            수금 금액, debnCnt : 선택채권수, rmkTxt : 비고, custColSn : 거래처 수금 순번, colType : 수금방식, statType : 상태
	 * @return
	 */
	/*@RequestMapping(value = "/customer/modifyCustomerColNoPlan.do")
	@ResponseBody
	public ModelAndView modifyCustomerColNoPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.modifyCustomerColNoPlan(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 페이지 내에서의 수금 삭제 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 07.
	 * @param custId
	 *            : 거래처 순번, deleteParam : 삭제대상 수금 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/removeCustomerCol.do")
	@ResponseBody
	public ModelAndView removeCustomerCol(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.removeCustomerCol(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 작성/수정을 위한 채권 조회 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 07.
	 * @version 1.0
	 * @param custid
	 *            : 거래처 순번, orderCondition : 정렬기준, orderType : 정렬순서, bondMbrId : 채권담당자, custColSn : 거래처 수금 순번
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getDebnListForCustomerColNoPlan.do")
	@ResponseBody
	public ModelAndView getDebnListForCustomerColNoPlan(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", customerService.getCustomerColNoPlanForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	
	
	/**
	 * <pre>
	 *    채권가이드 관련 첨부 파일 다운로드 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param debnGuideDownloadType
	 *            : 가이드 다운로드 타입 (hwp, doc , docMapping), guideType : 조치사항 별 상황 (J~R) fileName : 다운로드 파일명, fileType : ENum의 FileName Key값
	 * 
	 */
	/*@RequestMapping(value = "/customer/getDebntureGuideFileDownload.do")
	public void getDebntureGuideFileDownload(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {

		String fileName = "";
		String filePath = "";
		String mimeType = "";
		if ("hwp".equals(sBox.getString("debnGuideDownloadType"))) {
			fileName = EDebntureGuideFileNameType.search(sBox.getString("fileType")).getFileName() + "." + sBox.getString("debnGuideDownloadType");
			filePath = debnGuideHwpFilePath + sBox.getString("guideType") + "/" + fileName;
			mimeType = "application/hwp";
		} else if ("doc".equals(sBox.getString("debnGuideDownloadType"))) {
			if (!sBox.isEmpty("fileTypeSub")) {
				fileName = EDebntureGuideFileNameType.search(sBox.getString("fileTypeSub")).getFileName() + ".doc";
			} else {
				fileName = EDebntureGuideFileNameType.search(sBox.getString("fileType")).getFileName() + "."
						+ sBox.getString("debnGuideDownloadType");
			}
			filePath = debnGuideWordFilePath + sBox.getString("guideType") + "/" + fileName;
			mimeType = "application/doc";
		} else if ("wordMapping".equals(sBox.getString("debnGuideDownloadType"))) {
			fileName = EDebntureGuideFileNameType.search(sBox.getString("fileType")).getFileName() + ".doc";
			filePath = debnGuideWordMappingFilePath + sBox.getString("guideType") + "/" + fileName;
			sBox.set("filePath", filePath);
			SBox resultBox = null;//customerService.getDebntureGuideFileDownload(sBox);
			filePath = resultBox.getString("targetFilePath");
			mimeType = "application/doc";
		}

		try {

			ServletOutputStream res_out = response.getOutputStream();
			String docName = getDocNameByBrowser(fileName);
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");

			FileInputStream fis = new FileInputStream(filePath);

			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					res_out.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			res_out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}*/
	
	
}
