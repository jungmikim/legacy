package com.spsb.controller.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.enumtype.EUserTypeCode;
import com.spsb.common.parent.SuperController;
import com.spsb.common.session.SessionManager;
import com.spsb.common.util.CommonCode;
import com.spsb.common.util.CommonDesForSBUtil;
import com.spsb.common.util.CommonUtil;
import com.spsb.service.login.LoginService;
import com.spsb.service.user.UserService;
import com.spsb.service.user.UserTransactionService;

/**
 * <pre>
 * 회원(개인, 기업) Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 9.
 * @version 1.0
 */
@Controller
public class UserJoinController extends SuperController{

	@Autowired
	private UserTransactionService userTransactionService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LoginService loginService;
	
	/**
	 * <pre>
	 * 개인회원 LoginId 중복체크 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 7.
	 * @version 1.0
	 * @param loginId : 로그인식별자
	 * @return
	 */
	@RequestMapping(value = "/user/isDuplicationLoginIdAjax.do")
	@ResponseBody
	public ModelAndView isDuplicationLoginIdAjax(
			@RequestParam(value = "loginId", required = true) String loginId) {
		ModelAndView mav = new ModelAndView("jsonView");
		boolean result = userService.isDuplicationMemberUser(loginId);
		mav.addObject("result", result);
		return mav;
	}
	
	/**
	 * <pre>
	 * 기업회원 사업자번호 중복체크 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 25.
	 * @version 1.0
	 * @param usrNo : 사업자등록번호
	 * @return
	 */
	@RequestMapping(value = "/user/isDuplicationCompUsrNoAjax.do")
	@ResponseBody
	public ModelAndView isDuplicationCompUsrNoAjax(
			@RequestParam(value = "usrNo", required = true) String usrNo) {
		ModelAndView mav = new ModelAndView("jsonView");
		boolean result = userService.isDuplicationCompany(usrNo);
		mav.addObject("result", result);
		return mav;
	}
	
	/**
	 * <pre>
	 * 유형별 회원가입 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 7.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinEachType.do")
	public ModelAndView userJoinEachType(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinEachType");
		return mav;
	}
	
	/**
	 * <pre>
	 * 회원가입 _01.약관동의 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 7.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinTermsAgree.do")
	public ModelAndView userJoinTermsAgree(HttpServletRequest request,
			@RequestParam(value = "joinType", required = false, defaultValue="USR") String joinType){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinTermsAgree");
		mav.addObject("joinType", joinType);
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인회원가입_02-1.정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 7.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinMemberUserForm.do")
	public ModelAndView userJoinMemberUserForm(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinMemberUserForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("sbInfo", sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인회원가입_02-2.저장 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 8.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/user/userAddJoinMemberUser.do")
	public ModelAndView userAddJoinMemberUser(HttpServletRequest request
			,@ModelAttribute("initBoxs") SBox sBox){
		EUserErrorCode result = EUserErrorCode.FAIL;
		sBox.set("result", result);
		try {
			sBox.set("selectCompUsrId", sBox.getInt("compUsrId"));
			sBox.set("compUsrNo", sBox.getString("usrNo"));
			sBox.set("admYn", "N");
			sBox.set("grnCd", "C");
			userTransactionService.addMemberUser(sBox);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			result = sBox.get("result");
		}
		RedirectView rv =  new RedirectView(request.getContextPath() + "/user/userJoinMemberUserEnd.do?result="+result);
		rv.setExposeModelAttributes(false);
		ModelAndView mav = new ModelAndView(rv);
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인회원가입_03.가입완료 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 8.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinMemberUserEnd.do")
	public ModelAndView userJoinMemberUserEnd(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinMemberUserEnd");
		mav.addObject("result", sBox.get("result"));
		mav.addObject("joinType", sBox.get("joinType"));
		return mav;
	}
	
	/**
	 * <pre>
	 * 기업회원가입_02.기업정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 18.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinCompForm.do")
	public ModelAndView userJoinCompForm(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinCompForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		mav.addObject("joinType", "COMP1");
		return mav;
	}

	/**
	 * <pre>
	 * 기업회원가입_03-1.관리자정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 18.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/userJoinCompAdminUserForm.do")
	public ModelAndView userJoinCompAdminUserForm( @ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/userJoinCompAdminUserForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("compSBox", sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 기업과 관리자회원가입_03-2.저장 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 20.
	 * @version 1.0
	 * @param request
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/user/userAddJoinCompAndAdminUser.do")
	public ModelAndView userAddJoinCompAndAdminUser(HttpServletRequest request
			,@ModelAttribute("initBoxs") SBox sBox){
		SBox compSBox = new SBox();
		compSBox.set("compType", sBox.getString("compType"));
		compSBox.set("usrNm", sBox.getString("compUsrNm"));
		compSBox.set("usrNo", sBox.getString("compUsrNo"));
		compSBox.set("ownNm", sBox.getString("ownNm"));
		compSBox.set("corpNo", sBox.getString("corpNo"));
		compSBox.set("bizCond", sBox.getString("bizCond"));
		compSBox.set("bizType", sBox.getString("bizType"));
		compSBox.set("postNo", sBox.getString("compPostNo"));
		compSBox.set("addr1", sBox.getString("compAddr1"));
		compSBox.set("addr2", sBox.getString("compAddr2"));
		compSBox.set("email", sBox.getString("compEmail"));
		compSBox.set("telNo", sBox.getString("compTelNo"));
		compSBox.set("mbNo", sBox.getString("compMbNo"));
		compSBox.set("usrType", EUserTypeCode.DEFAULT.getCode());
		compSBox.set("payTermType", sBox.getString("payTermType"));
		compSBox.set("payTermDay", sBox.getString("payTermDay"));
		compSBox.set("certInfo", sBox.getString("certInfo"));
		
		EUserErrorCode result = EUserErrorCode.FAIL;
		compSBox.set("result", result);
		sBox.set("result", result);
		try {
			sBox.set("admYn", "Y");
			sBox.set("grnCd", "A,B,D,E");
			userTransactionService.addCompanyAndAdminUser(compSBox, sBox);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			result = compSBox.get("result");
			if(EUserErrorCode.SUCCESS.equals(compSBox.get("result"))){
				result = sBox.get("result");
			}
		}
		RedirectView rv =  new RedirectView(request.getContextPath() + "/user/userJoinMemberUserEnd.do?joinType=COMP1&result="+result);
		rv.setExposeModelAttributes(false);
		ModelAndView mav = new ModelAndView(rv);
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌 회원가입 TestController Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 7.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoinEachTypeTest.do")
	public ModelAndView sbUserJoinEachTypeTest(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/sbUserJoinEachTypeTest");
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌 로그인 or 회원가입 
	 * 로그인아이디가 존재하면 로그인
	 * 아이디가 존재하지 않으면 회원가입_01.약관동의 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 03. 31.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoin.do")
	public ModelAndView sbUserJoin(HttpServletRequest request,
			@RequestParam(value = "usrNo", required = true) String usrNo,
			@RequestParam(value = "compUsrNm", required = true) String compUsrNm,
			@RequestParam(value = "signInfo", required = false, defaultValue="" ) String signInfo,
			@RequestParam(value = "postNo", required = true) String postNo,
			@RequestParam(value = "addr", required = true) String addr,
			@RequestParam(value = "ownNm", required = true) String ownNm,
			@RequestParam(value = "bizType", required = true) String bizType,
			@RequestParam(value = "bizCond", required = false, defaultValue="") String bizCond,
			@RequestParam(value = "usrNm", required = true) String usrNm,
			@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "mbNo", required = false) String mbNo){
		ModelAndView mav = new ModelAndView();
		
		if(usrNo.isEmpty() || compUsrNm.isEmpty() || postNo.isEmpty() || addr.isEmpty() || ownNm.isEmpty() || bizType.isEmpty() 
				|| usrNm.isEmpty() || loginId.isEmpty()){
			mav.setViewName("/error/403Error");
			return mav;
		}
		
		mav.setViewName("/user/sbUserJoinTermsAgree");
		String joinType = "COMP1";
		
		/**
		 * 스마트빌에서 넘어온 parameta값 체크
		 */
		String param = usrNo + "|" + compUsrNm + "|" + signInfo + "|" + postNo + "|" + addr + "|" + ownNm + "|" +  bizType + "|" 
				+ bizCond + "|" + usrNm + "|" + loginId + "|" + email + "|" + telNo + "|" + mbNo;
		log.i(getLogMessage("sbUserJoin", "param", param));
		boolean isAbleUsrNo = CommonUtil.checkFormatBusinessNo(usrNo);
		boolean isAblePostNo = CommonUtil.checkFormatZipcode(postNo);
		boolean isAbleLoginId = CommonUtil.checkFormatLoginId(loginId);
		boolean isAbleEmail = true;
		if(email != null && !email.equals("")){
			isAbleEmail = CommonUtil.checkFormatEmail(email);
		}
		boolean isAbleTelNo = true;
		if(telNo != null && !telNo.equals("")){
			isAbleTelNo = CommonUtil.checkFormatTelCode(telNo);
		}
		boolean isAbleMbNo = true;
		if(mbNo != null && !mbNo.equals("")){
			isAbleMbNo = CommonUtil.checkFormatPhoneCode(mbNo);
		}
		if(!(isAbleUsrNo &&isAblePostNo && isAbleLoginId && isAbleEmail && isAbleTelNo && isAbleMbNo)){
			log.i(getLogMessage("sbUserJoin", "isAbleUsrNo", String.valueOf(isAbleUsrNo)));
			log.i(getLogMessage("sbUserJoin", "isAblePostNo", String.valueOf(isAblePostNo)));
			log.i(getLogMessage("sbUserJoin", "isAbleLoginId", String.valueOf(isAbleLoginId)));
			log.i(getLogMessage("sbUserJoin", "isAbleEmail", String.valueOf(isAbleEmail)));
			log.i(getLogMessage("sbUserJoin", "isAbleTelNo", String.valueOf(isAbleTelNo)));
			log.i(getLogMessage("sbUserJoin", "isAbleMbNo", String.valueOf(isAbleMbNo)));
			EUserErrorCode result = EUserErrorCode.USERJOIN_PARAM_DATA_FAIL;
			mav.setViewName("/error/msgError");
			mav.addObject("resultMsg", result.getDesc());
			return mav;
		}
		
		/**
		 * 로그인아이디가 존재하면 로그인
		 */
		SBox loginInfo = loginService.getLoginMemberUser(loginId);
		if(loginInfo == null){
			mav.addObject("message", EUserErrorCode.FAIL.getDesc());
			mav.setViewName("/login/notLogin");
		}else{
			EUserErrorCode resultCode = loginInfo.get("result");
			log.i(getLogMessage("sbUserJoin", "로그인 가능여부 체크", resultCode.getDesc()));
			if(EUserErrorCode.SUCCESS.equals(resultCode)){
				SessionManager.getInstance(request, loginInfo, sessionMaxInactiveTime);
				RedirectView rv = new RedirectView(request.getContextPath() + "/mypage/myPageMain.do");
				rv.setExposeModelAttributes(false);
				mav.setView(rv);
			}else if(EUserErrorCode.LOGIN_NOT_EXIST.equals(resultCode)){
				/**
				 * 스마틀회원이 채권에 가입하기위해 체크 후 회원가입타입진행
				 */
				SBox sbInfo = new SBox();
				sbInfo.set("usrNo", usrNo);
				sbInfo.set("compUsrNm", compUsrNm);
				sbInfo.set("signInfo", signInfo);
				sbInfo.set("postNo", postNo);
				sbInfo.set("addr", addr);
				sbInfo.set("ownNm", ownNm);
				sbInfo.set("bizType", bizType);
				sbInfo.set("bizCond", bizCond);
				sbInfo.set("usrNm", usrNm);
				sbInfo.set("loginId", loginId);
				sbInfo.set("email", email);
				sbInfo.set("telNo", telNo);
				sbInfo.set("mbNo", mbNo);
				
				//채권에 기업이 존재하는지 여부에 따라 가입방법 다름
				SBox userInfo = userService.getCompanyUserByUsrNo(usrNo);
				if(userInfo != null){
					joinType = "USR";
					sbInfo.set("compUsrId", userInfo.getInt("COMP_USR_ID"));
				}
				log.i(getLogMessage("sbUserJoin", "회원가입 진행", "회워가입타입 : " + joinType));
				boolean isMemberUser = userService.isDuplicationMemberUser(loginId);
				if(isMemberUser){
					EUserErrorCode result = EUserErrorCode.DUPLICATION_LOGIN_ID;
					RedirectView rv =  new RedirectView(request.getContextPath() + "/user/userJoinMemberUserEnd.do?joinType=" + joinType + "&result="+result);
					rv.setExposeModelAttributes(false);
					mav.setView(rv);
				}
				mav.addObject("joinType", joinType);
				mav.addObject("sbInfo", sbInfo);
			}else{
				mav.addObject("message", resultCode == null ? EUserErrorCode.FAIL.getDesc() : resultCode.getDesc());
				mav.setViewName("/login/notLogin");
			}
		}
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌 회원가입 _01.약관동의 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 03. 31.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoinTermsAgree.do")
	public ModelAndView sbUserJoinTermsAgree(HttpServletRequest request,
			@RequestParam(value = "usrNo", required = true) String usrNo,
			@RequestParam(value = "compUsrNm", required = true) String compUsrNm,
			@RequestParam(value = "signInfo", required = false, defaultValue="" ) String signInfo,
			@RequestParam(value = "postNo", required = true) String postNo,
			@RequestParam(value = "addr", required = true) String addr,
			@RequestParam(value = "ownNm", required = true) String ownNm,
			@RequestParam(value = "bizType", required = true) String bizType,
			@RequestParam(value = "bizCond", required = false, defaultValue="") String bizCond,
			@RequestParam(value = "usrNm", required = true) String usrNm,
			@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telNo", required = false) String telNo,
			@RequestParam(value = "mbNo", required = false) String mbNo){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/sbUserJoinTermsAgree");
		String joinType = "COMP1";
		
		String param = usrNo + "|" + compUsrNm + "|" + signInfo + "|" + postNo + "|" + addr + "|" + ownNm + "|" +  bizType + "|" 
					+ bizCond + "|" + usrNm + "|" + loginId + "|" + email + "|" + telNo + "|" + mbNo;
		log.i(getLogMessage("sbUserJoinTermsAgree", "param", param));
//		boolean isAbleUsrNo = CommonUtil.checkFormatBusinessNo(usrNo);
		boolean isAbleUsrNo = true;
		boolean isAblePostNo = CommonUtil.checkFormatZipcode(postNo);
		boolean isAbleLoginId = CommonUtil.checkFormatLoginId(loginId);
		boolean isAbleEmail = true;
		if(email != null && !email.equals("")){
			isAbleEmail = CommonUtil.checkFormatEmail(email);
		}
		boolean isAbleTelNo = true;
		if(telNo != null && !telNo.equals("")){
			isAbleTelNo = CommonUtil.checkFormatTelCode(telNo);
		}
		boolean isAbleMbNo = true;
		if(mbNo != null && !mbNo.equals("")){
			isAbleMbNo = CommonUtil.checkFormatPhoneCode(mbNo);
		}
		if(!(isAbleUsrNo &&isAblePostNo && isAbleLoginId && isAbleEmail && isAbleTelNo && isAbleMbNo)){
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAbleUsrNo", String.valueOf(isAbleUsrNo)));
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAblePostNo", String.valueOf(isAblePostNo)));
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAbleLoginId", String.valueOf(isAbleLoginId)));
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAbleEmail", String.valueOf(isAbleEmail)));
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAbleTelNo", String.valueOf(isAbleTelNo)));
			log.i(getLogMessage("sbUserJoinTermsAgree", "isAbleMbNo", String.valueOf(isAbleMbNo)));
			EUserErrorCode result = EUserErrorCode.USERJOIN_PARAM_DATA_FAIL;
			mav.setViewName("/error/msgError");
			mav.addObject("resultMsg", result.getDesc());
			return mav;
		}
		SBox sbInfo = new SBox();
		sbInfo.set("usrNo", usrNo);
		sbInfo.set("compUsrNm", compUsrNm);
		sbInfo.set("signInfo", signInfo);
		sbInfo.set("postNo", postNo);
		sbInfo.set("addr", addr);
		sbInfo.set("ownNm", ownNm);
		sbInfo.set("bizType", bizType);
		sbInfo.set("bizCond", bizCond);
		sbInfo.set("usrNm", usrNm);
		sbInfo.set("loginId", loginId);
		sbInfo.set("email", email);
		sbInfo.set("telNo", telNo);
		sbInfo.set("mbNo", mbNo);
		
		//채권에 기업이 존재하는지 여부에 따라 가입방법 다름
		SBox userInfo = userService.getCompanyUserByUsrNo(usrNo);
		if(userInfo != null){
			joinType = "USR";
			sbInfo.set("compUsrId", userInfo.getInt("COMP_USR_ID"));
		}
		
		boolean isMemberUser = userService.isDuplicationMemberUser(loginId);
		if(isMemberUser){
			EUserErrorCode result = EUserErrorCode.DUPLICATION_LOGIN_ID;
			RedirectView rv =  new RedirectView(request.getContextPath() + "/user/userJoinMemberUserEnd.do?joinType=" + joinType + "&result="+result);
			rv.setExposeModelAttributes(false);
			mav.setView(rv);
		}
		mav.addObject("joinType", joinType);
		mav.addObject("sbInfo", sbInfo);
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌 회원가입_02-1.정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 1.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoinMemberUserForm.do")
	public ModelAndView sbUserJoinMemberUserForm(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/sbUserJoinMemberUserForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("sbInfo", sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌 기업회원가입_02.기업정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 1.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoinCompForm.do")
	public ModelAndView sbUserJoinCompForm(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/sbUserJoinCompForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		mav.addObject("joinType", "COMP1");
		mav.addObject("sbInfo", sBox);
		return mav;
	}

	/**
	 * <pre>
	 * 스마트빌 기업회원가입_03-1.관리자정보입력 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 1.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/sbUserJoinCompAdminUserForm.do")
	public ModelAndView sbUserJoinCompAdminUserForm( @ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/sbUserJoinCompAdminUserForm");
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("compSBox", sBox);
		return mav;
	}
	
	
	/**
	 * 
	 * <pre>
	 * 스마트빌 연계 회원가입
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 14.
	 * @version 1.0
	 * @param request
	 * @param loginId : 회원아이디, usrNm: 회원이름, email: 이메일, telNo: 전화번호, mbNo: 전화번호, postNo: 우편번호, addr: 주소, compUsrNm: 회사이름, usrNo: 사업자등록번호, ownNm : 대표자명, corpNo: 법인등록번호 , bizType : 업종, bizCond : 업태 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "user/addUserInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView addUserInfoForSB(HttpServletRequest request,
									    @RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 회원 채권회원으로  가입적용 완료";  
		String encodeValue;
		log.i(getLogMessage("addUserInfoForSB", "value", value));
		try{
			
			if( value==null||"".equals(value)){
				mav.addObject("resultMsg", resultMsg.toString());
				return mav;
			}
			
			encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("addUserInfoForSB", "encodeValue", encodeValue));
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();

			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String usrNm  = paramSBox.getString("USR_NM");
			String email = paramSBox.getString("EMAIL");
			String telNo  = paramSBox.getString("TEL_NO");
			String mbNo  = paramSBox.getString("MB_NO");
			String postNo  = paramSBox.getString("POST_NO");
			String addr  = paramSBox.getString("ADDR_1");
			String compUsrNm  = paramSBox.getString("COMP_NM");
			String usrNo  = paramSBox.getString("USR_NO");
			String ownNm  = paramSBox.getString("OWN_NM");
			String corpNo  = paramSBox.getString("CORP_NO");
			String bizType  = paramSBox.getString("BIZ_TYPE");
			String bizCond  = paramSBox.getString("BIZ_COND");
			String admYn  = paramSBox.getString("ADM_YN");
			
			String param = loginId + "|" + usrNm + "|" + email + "|" + telNo + "|" + mbNo + "|" + postNo + "|" +  addr + "|" 
						+ compUsrNm + "|" + usrNo + "|" + ownNm + "|" + corpNo + "|" + bizType + "|" + bizCond;
			log.i(getLogMessage("addUserInfoForSB", "param", param));
			
			// USR
			SBox sbInfo = new SBox();
			sbInfo.set("loginId", loginId);
			sbInfo.set("usrNm", usrNm);
			sbInfo.set("email", email);
			sbInfo.set("telNo", telNo);
			sbInfo.set("mbNo", mbNo);
			sbInfo.set("postNo", postNo);
			sbInfo.set("addr", addr);
			sbInfo.set("postNo", postNo);
			sbInfo.set("addr1", addr);
			sbInfo.set("email", email);
			sbInfo.set("telNo", telNo);
			sbInfo.set("mbNo", mbNo);
			sbInfo.set("grnCd", "A,B,D,E");
			sbInfo.set("admYn", admYn); 
			
			//기업회원 정보
			SBox compSBox = new SBox();
			compSBox.set("compType", "C");//default : 회원
			compSBox.set("usrNm", compUsrNm);
			compSBox.set("usrNo", usrNo);
			compSBox.set("ownNm", ownNm);
			compSBox.set("corpNo", corpNo);
			compSBox.set("bizCond", bizCond);
			compSBox.set("bizType", bizType);
			compSBox.set("postNo", postNo);
			compSBox.set("addr1", addr);
			compSBox.set("email", email);
			compSBox.set("telNo", telNo);
			compSBox.set("mbNo", mbNo);
			compSBox.set("usrType", EUserTypeCode.DEFAULT.getCode()); // default : 0
			compSBox.set("payTermType", "D"); //default : 익월 말일 D
			
			userTransactionService.addCompanyAndUserForSB(compSBox, sbInfo); // 기업회원, 개인회원, 기업회원멤버 등록 함수
			
		}catch(Exception e) {
			resultMsg = "기업회원, 개인회원, 기업회원멤버 가입이 실패하였습니다.";   
			e.printStackTrace();
		}
		//암호화한 정보 쪼개서 각 값에 셋팅
		 mav.addObject("resultMsg", resultMsg.toString());
		 
		return mav;
	}

	/**
	 * <pre>
	 * TEST 문서 불러오기
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 14.
	 * @version 1.0
	 * @param value
	 */
	@RequestMapping(value = "/user/testForm.do")
	public ModelAndView addUserInfoForSB(HttpServletRequest request) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/testForm");
	
		return mav;
	}
		
	
}
