package com.spsb.controller.mypage;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonCode;
import com.spsb.service.mypage.MyPageUserModifyService;


/**
 * <pre>
 * 마이페이지 정보수정  
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Controller
public class MyPageUserModifyController extends SuperController{

	@Autowired
	private MyPageUserModifyService myPageUserModifyService;

	
	/**
	 * <pre>
	 * 마이페이지 - 관리자  정보조회 화면
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번, sessionCustType:거래처 유형
	 * @return
	 */
	@RequestMapping(value = "/mypage/getCompUserInfo.do")
	public ModelAndView getCompUserInfo(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getCompUserInfo");
		boolean isUpdatedCert = false;
		mav.addObject("isUpdatedCert", isUpdatedCert);
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		mav.addObject("result", myPageUserModifyService.getCompUserInfo(sBox));
		return mav;
	}
	
	
	/**
	 * <pre>
	 * 마이페이지 - 개인회원 정보조회 화면
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 5.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/mypage/getUserInfo.do")
	public ModelAndView getUserInfo(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getUserInfo");
		mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지 : 개인  - 탈퇴 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형
	 * @return
	 */
	@RequestMapping(value = "/mypage/getUserSecession.do")
	public ModelAndView getUserSecession(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getUserSecession");
		mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지 - 관리자 수정 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형
	 * @return
	 */
	@RequestMapping(value = "/mypage/getCompUserModifyPopup.do")
	public ModelAndView getCompUserModifyPopup(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getCompUserModifyPopup");
		mav.addObject("result", myPageUserModifyService.getCompUserInfo(sBox));
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		return mav;
	}
	
	
	/**
	 * <pre>
	 *  마이페이지 - 개인회원 비밀번호 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 7. 8.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/mypage/getUserModifyForPW.do")
	public ModelAndView getUserModifyForPW(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getUserModifyForPW");
		mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지 : 개인 - 비밀번호 수정 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 07. 20.
	 * @version 1.0
	 * @param sBox : usrId:개인회원순번,pw:비밀번호
	 * @return
	 */
	@RequestMapping(value = "/mypage/modifyUserForPW.do")
	public ModelAndView modifyUserForPW(@ModelAttribute("initBoxs") SBox sBox, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/myPageMain");
		SBox resultBox = myPageUserModifyService.modifyUserForPW(sBox);
		// modifyUserCurrentSession의  새로운세션정보 불러오기
		modifyUserCurrentSession(request, sBox);
		mav.addObject("resultCustCreditSummery", myPageUserModifyService.getCustCreditSummaryList(sBox));
		mav.addObject("resultCompanyList", myPageUserModifyService.getBelongingtoCompanyList(sBox));
		mav.addObject("summery",  myPageUserModifyService.getSummaryList(sBox));
		mav.addObject("sBox",sBox);	
		mav.addObject("result", resultBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지 - 관리자 수정 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/getCompUserModify.do")
	public ModelAndView getCompUserModify(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getCompUserModify");
		mav.addObject("result", myPageUserModifyService.getCompUserInfo(sBox));
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		return mav;
		if("N".equals(sBox.get(ESessionNameType.ADM_YN.getName()))){
			//일반회원
			mav.setViewName("/mypage/getUserModify");
			mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		}
		else { 
			//mav.setViewName("/mypage/getUserModify");
			//mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		
			///관리자 기업페이지 기업회원
		//}
	}*/
	
	
	
	/**
	 * <pre>
	 * 마이페이지 - 개인회원 정보조회 화면
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 5.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/getUserModifyPopup.do")
	public ModelAndView getUserModifyPopup(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getUserModifyPopup");
		mav.addObject("result", myPageUserModifyService.getUserInfo(sBox));
		mav.addObject("sBox",sBox);
		return mav;
	}*/
	
	
	/**
	 * <pre>
	 * 마이페이지 : 관리자 - 정보 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형,compUsrId:기업회원순번 ,ownNm:대표자명,usrNm:회원명,bizCond:업태,bizType:업종,
	 *               address1:주소1,address2:주소2,postNo:우편번호,telNo:전화번호,certInfo:인증서정보
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/modifyCompUser.do")
	public ModelAndView modifyCompUser(@ModelAttribute("initBoxs") SBox sBox
			 							 , HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getCompUserInfo");
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		SBox resultBox = myPageUserModifyService.modifyCompUser(sBox);
		// modifyUserCurrentSession의  새로운세션정보 불러오기
		modifyUserCurrentSession(request, sBox);
		mav.addObject("result", resultBox);
		return mav;
	}*/
	
	
	
	/**
	 * <pre>
	 * 마이페이지 : 개인 - 정보수정 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 4.
	 * @version 1.0
	 * @param sBox : usrId:개인회원순번,telNo:개인전화번호,cellNo:개인휴대폰번호,pstNo:개인주소,adress:개인 주소,emailType01:개인이메일,loginPw:비밀번호, mbrId:기업회원멤버순번,deptNm:부서명,telNoDept:회사전화번호,cellNoDept:회사 휴대폰번호,emailTypeDept01:회사이메일
	 * @return
	 */
/*	@RequestMapping(value = "/mypage/modifyUser.do")
	public ModelAndView modifyUser(@ModelAttribute("initBoxs") SBox sBox
									 , HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getUserInfo");
		SBox resultBox = myPageUserModifyService.modifyUser(sBox);
		// modifyUserCurrentSession의  새로운세션정보 불러오기
		modifyUserCurrentSession(request, sBox);
		mav.addObject("result", resultBox);
		return mav;
	}*/
	

	
	
	
	
	/**
	 * <pre>
	 * 마이페이지: 개인 - 회원탈퇴 과정 처리
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 31.
	 * @version 1.0
	 * @param sBox
	 * @param stat
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/modifyUsrSecession.do")
	@ResponseBody
	public ModelAndView modifyUsrSecession(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView("jsonview");
		mav.addObject("result",  myPageUserModifyService.modifyUsrSecession(sBox));
		return mav;
	}*/
	
	
	/**
	 * <pre>
	 * 마이페이지 : 관리자 - 기업인증서 등록 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 03.
	 * @version 1.0
	 * @param sBox : usrId:개인회원순번, certinfo:공인인증서
	 * @return
	 */
/*	@RequestMapping(value = "/mypage/modifyCertInfo.do")
	public ModelAndView modifyCertInfo(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/getCompUserInfo");		
		boolean isUpdatedCert = true;
		mav.addObject("payTermTypeList", CommonCode.payTermTypeList);
		mav.addObject("isUpdatedCert", isUpdatedCert);
		mav.addObject("result", myPageUserModifyService.modifyCertInfo(sBox));
		return mav;
	}*/
	
	
	
	
	/**
	 * <pre>
	 * 마이페이지 : 관리자 - 정보 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형,compUsrId:기업회원순번 ,ownNm:대표자명,usrNm:회원명,bizCond:업태,bizType:업종,
	 *               address1:주소1,address2:주소2,postNo:우편번호,telNo:전화번호,certInfo:인증서정보
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/modifyCompUserModify.do")
	@ResponseBody
	public ModelAndView modifyCompUserModify(@ModelAttribute("initBoxs") SBox sBox
			 							 , HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox resultBox = myPageUserModifyService.modifyCompUser(sBox);
		// modifyUserCurrentSession의  새로운세션정보 불러오기
		modifyUserCurrentSession(request, sBox);
		mav.addObject("result", resultBox);
		return mav;
	}*/
	
	

	/**
	 * <pre>
	 * 마이페이지 : 개인 - 정보수정 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 4.
	 * @version 1.0
	 * @param sBox : usrId:개인회원순번,telNo:개인전화번호,cellNo:개인휴대폰번호,pstNo:개인주소,adress:개인 주소,emailType01:개인이메일,loginPw:비밀번호, mbrId:기업회원멤버순번,deptNm:부서명,telNoDept:회사전화번호,cellNoDept:회사 휴대폰번호,emailTypeDept01:회사이메일
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/modifyUserModify.do")
	@ResponseBody
	public ModelAndView modifyUserModify(@ModelAttribute("initBoxs") SBox sBox
									 , HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox resultBox = myPageUserModifyService.modifyUser(sBox);
		// modifyUserCurrentSession의  새로운세션정보 불러오기
		modifyUserCurrentSession(request, sBox);
		mav.addObject("result", resultBox);
		return mav;
	}
	*/
	
}
