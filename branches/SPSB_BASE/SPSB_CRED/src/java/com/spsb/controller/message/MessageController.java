package com.spsb.controller.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.service.message.InterfaceMessageService;

/**
 * <pre>
 *  메시지 인터페이스 Controller Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
@Controller
public class MessageController extends SuperController {

	@Autowired
	private InterfaceMessageService interfaceMessageService;
	
	
	/**
	 * <pre>
	 *  크레딧서비스의 조기경보 대상 거래처 사업자번호의 신규등록을 하기 위한 XML 메시지 생성을 위해 
	 *  파라미터 셋팅
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 29.
	 * @version 1.0
	 */
	@RequestMapping(value="/message/makeEwCustRequestMessage.do")
	public void makeEwCustRequestMessage() {
		SBox paramBox = new SBox();
		
		try{
			paramBox.set("compUsrId", "5");
			paramBox.set("compMngCd", "ERP1");
			paramBox.set("trans", "E");
			interfaceMessageService.setEwCustRequestMessage(paramBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * <pre>
	 * 크레딧서비스 조기 경보  거래처 등록 성공시 응답서 XML 메시지 생성을 위해  :CRSREC
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 29.
	 * @version 1.0
	 * @param commonVo
	 * @param requstVo
	 * @return
	 */
	@RequestMapping(value="/message/getEwCustReponseMessage.do")
	public void getEwCustReponseMessage(String xmlFile) {
		
		if(xmlFile != null){
			//크레딧 서비스 조기 경보 대상 거래처 등록응답서 넘겨서 상태 변경
			String type="SUCESS";
			
			if(type == "SUCESS"){
				interfaceMessageService.modifyCustRegistorResponseSucess(xmlFile);
			}else if(type == "FAIL"){
				interfaceMessageService.modifyCustRegistorResponseFail(xmlFile);
			}
			
		}else{
			log.e("xmlFile dose not found");
		}
	}
	
}
