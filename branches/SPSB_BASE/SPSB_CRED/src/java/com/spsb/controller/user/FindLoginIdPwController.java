package com.spsb.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.parent.SuperController;
import com.spsb.service.login.LoginService;

/**
 * <pre>
 * 아이디/비밀번호찾기 Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 12. 5.
 * @version 1.0
 */
@Controller
public class FindLoginIdPwController extends SuperController{

	@Autowired
	private LoginService loginService;
	
	/**
	 * <pre>
	 * 로그인 아이디 찾기 form Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 5.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/findLoginIdForm.do")
	public ModelAndView findLoginIdForm(){
		ModelAndView mav = new ModelAndView("/user/findLoginIdForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 로그인 아이디 찾기  Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 5.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/findLoginId.do")
	public ModelAndView findLoginId(
			@RequestParam(value = "key", required = true) String key){
		String loginId = "sbuser";
		ModelAndView mav = new ModelAndView("/user/findLoginId");
		mav.addObject("loginId", loginId);
		return mav;
	}
	
	/**
	 * <pre>
	 * 로그인 비밀벊노 찾기 form Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 5.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/findLoginPwForm.do")
	public ModelAndView findLoginPwForm(){
		ModelAndView mav = new ModelAndView("/user/findLoginPwForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 로그인 비밀벊노 찾기 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 5.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/findLoginPw.do")
	public ModelAndView findLoginPw(
			@RequestParam(value = "loginId", required = true) String loginId){
		SBox result = loginService.getLoginMemberUser(loginId);
		ModelAndView mav = new ModelAndView();
		EUserErrorCode resultCode = result.get("result");
		if(EUserErrorCode.SUCCESS.equals(resultCode)){
			mav.setViewName("/user/findLoginPw");
			mav.addObject("result", result);
		}else{
			mav.setViewName("/user/findLoginPwForm");
			mav.addObject("loginId", loginId);
			mav.addObject("message", resultCode.getDesc());
		}
		return mav;
	}
}
