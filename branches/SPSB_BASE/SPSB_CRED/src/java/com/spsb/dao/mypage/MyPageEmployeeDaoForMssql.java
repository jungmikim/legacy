/**
 * 
 */
package com.spsb.dao.mypage;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 마이페이지 - 정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Repository
public class MyPageEmployeeDaoForMssql extends SuperDao{

	//기업회원 :직원관리 관리자추출
	public SBoxList<SBox> selectEmployeeAdmList(SBox sBox)throws BizException {
		SBoxList<SBox> resultList;
		
		try {
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectEmployeeAdmList_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectEmployeeAdmList Dao ERROR");
		}
		return resultList;
	}
	
	//기업회원 :직원관련 정보 가지고 오기
	public SBoxList<SBox> selectEmployeeList(SBox sBox)throws BizException {
		SBoxList<SBox> resultList;
		
		try {
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectEmployeeList_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectEmployeeList Dao ERROR");
		}
		return resultList;
	}
	
	// 기업회원 :가입/비가입된 직원 전체 Count
	public int  selectEmployeeListTotalCount(SBox sBox)throws BizException {
		int result = 0;
		
		try {
			result  = ((SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectEmployeeListTotalCount_SQL", sBox)).getInt("totalCnt");
			
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectEmployeeListTotalCount Dao ERROR");
		}
		return result;
	}
	

	//기업회원 : 신규요청리스트 팝업창 업데이트
	public SBox updateEmployeeRequst(SBox sBox)throws BizException {
		
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateMemberGrn_SQL", sBox);
		return result;
	}
	//기업회원 : 가입 직원 리스트 update - 멤버테이블
	public SBox updateEmployeeJoin(SBox sBox)throws BizException {
		
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateMember_SQL", sBox);
		return result;
	}
	//기업회원 : 가입 직원 리스트 update - 개인테이블
	public SBox updateEmployeeJoinByUser(SBox sBox)throws BizException {
		
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateUserByEmployee_SQL", sBox);
		return result;
	}
	
	//기업회원 : 비가입 직원 리스트 update	
	public SBox updateEmployeeDisJoin(SBox sBox)throws BizException {
		
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateMember_SQL", sBox);
		return result;
	}
		
	// 기업회원 :비가입 직원 리스트 insert	
	public SBox insertEmployeeDisjoin(SBox sBox)throws BizException {
		
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.insertMember_SQL", sBox);
		return result;
	}
	
	
	
	//개인회원 :직원관련 정보 가지고 오기
	/*public SBoxList<SBox> selectEmployeeByPersonalList(SBox sBox)throws BizException {
		SBoxList<SBox> resultList;
		
		try {
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectEmployeeList_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectEmployeeList Dao ERROR");
		}
		return resultList;
	}
	
	// 개인회원 :가입/비가입된 직원 전체 Count
	public int  selectEmployeeByPersonalListTotalCount(SBox sBox)throws BizException {
		int result = 0;
		
		try {
			result  = ((SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectEmployeeListTotalCount_SQL", sBox)).getInt("totalCnt");
			
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectEmployeeListTotalCount Dao ERROR");
		}
		return result;
	}*/
		
	
	
	
}
