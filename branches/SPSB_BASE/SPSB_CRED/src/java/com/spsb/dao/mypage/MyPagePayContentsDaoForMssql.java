/**
 * 
 */
package com.spsb.dao.mypage;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 마이페이지 - 정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Repository
public class MyPagePayContentsDaoForMssql extends SuperDao{

	//연계스케쥴 리스트 출력
	public SBoxList<SBox>  selectCustLnkSchId(SBox sBox){
		SBoxList<SBox> resultList = null;
		try {
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectCustLnkSchId_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	//연계스케쥴 리스트 출력  Count
	public int  selectLnkSchIdListTotalCount(SBox sBox)throws BizException {
		int result = 0;
		
		try {
			result  = ((SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectLnkSchIdListTotalCount_SQL", sBox)).getInt("totalCnt");
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	
	//연계스케줄 예약 등록
	public SBox insertDeLnkSch(SBox sBox)  {

		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.insertDeLnkSch_SQL", sBox);
		return result;
	}
	
	
	//연계스케줄 예약 수정
	public SBox updateDeLnkSch(SBox sBox) throws BizException {
		SBox result=null;
	try {	
			result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateDeLnkSch_SQL", sBox);
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	//연계스케줄 예약 취소
	public SBox deleteLnkSch(SBox sBox) throws BizException {
		SBox result=null;
		try {	
			result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.deleteLnkSch_SQL", sBox);
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	//연계스케쥴 리스트 상태값별로 출력
	public SBox  selectCustLnkSchIdStat(SBox sBox){
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.selectCustLnkSchIdStat_SQL", sBox);
		
		return result;
	}
}
