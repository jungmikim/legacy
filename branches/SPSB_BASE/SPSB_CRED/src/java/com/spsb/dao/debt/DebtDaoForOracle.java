package com.spsb.dao.debt;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 채무불이행신청 Dao Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.04.15
 */
@Repository
public class DebtDaoForOracle extends SuperDao {
	
	
	
	/**
	 * <pre>
	 * 채무불이행 신청서 신청인 정보  DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 29.
	 * @param sessionCompUsrId  : 기업회원순번 , mbrStat : 회원상태 , usrId : 멤버회원순번
	 * @throws BizException
	 */
	public SBoxList<SBox> selectMbrEmployeeListByCondition(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectMbrEmployeeListByCondition_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return resultList;
	}
	

	/**
	 * <pre>
	 * 채무불이행등록 정보등록담당자 수정 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 21.
	 * @param usrId
	 *            : 개인회원순번, usrNm : 회원이름, deptNm : 부서명, telNo : 전화번호, mbNo : 휴대폰번호, faxNo : 팩스번호, email : 이메일
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebtManager(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtManager_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행등록 STEP02 거래처 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 21.
	 * @param compUsrId
	 *            : 기업회원순번, custId : 거래처순번, deptApplSeq : 채무불이행신청서 순번, custType : 채무자 거래처 유형, custNm : 채무자 거래처명, custNo : 채무자 사업자등록번호, ownNm : 채무자명, corpNo : 채무자 법인등록번호, postNo : 우편번호, debt_type : 채무
	 *            구분, stDebtAmt : 채무불이행금액
	 * @return
	 * @throws BizException
	 */
	public SBox insertOrUpdateCustForDebt(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertOrUpdateCustForDebt_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	

	/**
	 * <pre>
	 * 특정 채권의 증빙 파일 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 25.
	 * @param debnId
	 *            : 채권순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebenturePrfFile(String debnId) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.SelectDebenturePrfFile_SQL", debnId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무불이행신청 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param usrId
	 *            : 채무불이행신청자 유저 순번, applyCustType : 신청자 회사 구분, applyCorpName : 신청자 회사명, custNo : 신청자 회사 사업자 등록번호, applyCorpNum : 신청자 회사 법인등록번호, applyOwnName : 신청자 회사 대표자명, postNo : 신청자 회사 우편번호,
	 *            applyAddr1 : 신청자 회사 주소1, applyAddr2 : 신청자 회사 주소2, applyBizType : 신청자 회사 업종, managerName : 정보등록담당자 이름, managerPart : 정보등록담당자 부서, managerPosition : 정보등록담당자 직위, telNo : 정보등록담당자 전화번호,
	 *            mbNo : 정보등록담당자 핸드폰번호, faxNo : 정보등록담당자 팩스번호, email : 정보등록담당자 이메일
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtApply(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtApply_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행신청 수정 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param deptApplSeq
	 *            : 채무불이행신청서 순번, usrId : 채무불이행신청자 유저 순번, applyCustType : 신청자 회사 구분, applyCorpName : 신청자 회사명, custNo : 신청자 회사 사업자 등록번호, applyCorpNum : 신청자 회사 법인등록번호, applyOwnName : 신청자 회사 대표자명, postNo
	 *            : 신청자 회사 우편번호, applyAddr1 : 신청자 회사 주소1, applyAddr2 : 신청자 회사 주소2, applyBizType : 신청자 회사 업종, managerName : 정보등록담당자 이름, managerPart : 정보등록담당자 부서, managerPosition : 정보등록담당자 직위, telNo :
	 *            정보등록담당자 전화번호, mbNo : 정보등록담당자 핸드폰번호, faxNo : 정보등록담당자 팩스번호, email : 정보등록담당자 이메일, compUsrId : 기업회원순번, custId : 거래처 순번, custType : 거래처 회사구분, custNm : 거래처 회사명, ownNm : 거래처 대표자명, corpNo :
	 *            거래처 법인등록번호, postNo : 거래처 우편번호, addr1 : 거래처 주소1, addr2 : 거래처 상세주소, debt_type : 거래처 채무구분, totUnpdAmt : 미수총금액, stDebtAmt : 채무불이행금액, stat : 상태
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebtApply(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtApply_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행신청 미수채권 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번, debnId : 채권 순번
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtDebenture(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtDebenture_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}



	/**
	 * <pre>
	 * 채무불이행신청 증빙파일 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번, debnFileSn : 채권 증빙파일 순번, typeCd : 파일 유형, fileNm : 파일명, filePath : 파일 경로 , debnId : 채권번호
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtFile(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtFile_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행신청 증빙파일 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 2.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번, deleteDebtFileSN : 채무불이행신청 증빙파일 순번 리스트
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebtFileByCondition(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtFileByCondition_SQL", sBox));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무불이행신청 기존 증빙파일 삭제 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번, deleteDebtFileSN : 채무불이행신청 증빙파일 순번 리스트
	 * @return
	 * @throws BizException
	 */
	public SBox deleteDebtFile(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.deleteDebtFile_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}

	

	/**
	 * <pre>
	 * 채무불이행 신청서 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 29.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지
	 *            순번
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebtList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 총 개수 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 29.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지
	 *            순번
	 * @throws BizException
	 */
	public int selectDebtTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debt.selectDebtTotalCount_SQL", sBox)).getInt("TOTALCNT");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 * 채무불이행신청서 삭제 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 05. 01.
	 * @param debtIdList
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	public SBox deleteDebt(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.deleteDebt_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 삭제할 파일 경로 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 08.
	 * @param debtIdList
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDeletedDebtFilePath(String debtIdList) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDeletedDebtFilePath_SQL", debtIdList));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 상세 조회 DAO METHOD
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.07
	 * @param debtApplId : 채무불이행신청순번, sessionCompUsrId : 기업회원 순번, statList : 상태리스트
	 * @return : 채무불이행 결과
	 * @throws BizException
	 */
	public SBox selectDebt(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			SBoxList<SBox> returnList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebt_SQL", sBox));
			returnBox = (returnList.size() > 0) ? returnList.get(0) : null;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 거래처에서 채무불이행 신청서 상세 조회 DAO METHOD
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.07
	 * @param debtApplId : 채무불이행신청순번, sessionCompUsrId : 기업회원 순번, statList : 상태리스트
	 * @return : 채무불이행 결과
	 * @throws BizException
	 */
	public SBox selectDebtDetail(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			SBoxList<SBox> returnList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtDetail_SQL", sBox));
			returnBox = (returnList.size() > 0) ? returnList.get(0) : null;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 진행완료 접수문서 상세보기 상태 이력 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 9.
	 * @param debtApplId
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	public SBox selectDebtStatHistoryForRecentStatus(String debtApplId) throws BizException {
		SBox returnBox = null;

		try {
			SBoxList<SBox> returnList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtStatHistoryForRecentStatus_SQL", debtApplId));

			returnBox = (returnList.size() > 0) ? returnList.get(0) : null;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 사유관리 리스트 조회  DAO METHOD
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 09.
	 * @param debtApplId
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebtStatHistoryList(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtStatHistoryList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무 불이행 정정 관리 리스트 조회  DAO METHOD
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 09.
	 * @param debtApplId
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebtModHistoryList(String debtApplId) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtModHistoryList_SQL", debtApplId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 채무불이행신청 추가증빙 파일 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 10.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번, ch_fileNm : 파일명 , debtStatHistId : 채무불이행상태이력순번
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtStatFile(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			sBox.set("debtStatHistId", sBox.get("debtStatHistId").toString());
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtStatFile_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 신청 수정 및 채무 불이행 상태 이력 등록 DAO METHOD 20150518
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 10.
	 * @param debtApplSeq  : 채무불이행신청서 순번, stat : 상태: 등록전 민원발생(추가증빙) , usrId : 등록자 순번, rmk_txt: 사유
	 * 
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtStatus(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtStatus_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 결제 시 채무불이행 신청 수정 및 채무 불이행 상태 이력 등록 DAO METHOD
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 26.
	 * @version 1.0
	 * @param sBox
	 * 			debtApplSeq: 채무불이행신청서 순번, stat : 상태(결제) , usrId : 등록자 순번, rmk_txt: 사유, payAmt : 납입금액
	 * @return
	 * @throws BizException 
	 */
	public SBox insertDebtStatusForPay(SBox sBox) throws BizException{
		try {
			return (SBox) super.getSqlMapClientTemplate().queryForObject("debt.insertDebtStatusForPay_SQL", sBox);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}
	}
	


	/**
	 * <pre>
	 *   채무불이행등록 정정요청금액, 정정요청 상태 수정 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, debtAmt : 정정 요청 금액, sessionUsrId : 회원 순번, modStat : 채무금액정정상태
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebtApplForRequireModify(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			sBox.set("sessionUsrId", sBox.get("sessionUsrId").toString());
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtApplForRequireModify_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 *   채무불이행 정정요청 이력 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, debtAmt : 정정 요청 금액, modStat : 채무금액정정상태, regType : 등록자유형
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtModHist(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			sBox.set("sessionUsrId", sBox.get("sessionUsrId").toString());
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtModHist_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 *   채무불이행 사유 문구 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *            rsnTxtStat : 문구 상태, debtCompType : 채무불이행 업체 유형, debtStatCd : 채무불이행 현재 상태
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebtRsnTxt(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtRsnTxt_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	

	/**
	 * <pre>
	 *   채무불이행 상태 이력 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, regType : 등록자유형, debtStatCd : 상태코드, debtStatSubCd : 상태 서브코드, rmkTxt : 사유 sessionUsrId : 회원순번, sessionUsrNm : 회원이름
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtStatHist(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtStatHist_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 신청 상태 이력 등록 Dao Method
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 10.
	 * @param debtApplId
	 *            : 채무불이행신청서 순번, overStDt : 연체개시일자, regDueDt : 등록예정일, stat : 채무불이행신청서 상태, sessionUsrId : 등록자 순번, regType : 등록자 회원 유형, sessionUsrNm : 등록자 이름
	 * @return
	 * @throws BizException
	 */
	public SBox insertDebtApplication(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtApplication_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 *   사유관리 TOTAL COUNT Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 13.
	 * @version 1.0
	 * @param debtApplId : 채무불이행 신청서 시퀀스
	 * 
	 * @return result : 사유관리 TOTAL COUNT
	 */
	public int selectDebtStatHistoryTotalCount(String debtApplId) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debt.selectDebtStatHistoryTotalCount_SQL", debtApplId)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   채무불이행 상태 프로세스 조회 DAO
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 05 .19.
	 * @version 1.0
	 * @param debtApplId
	 *            : 채무불이행 순번
	 * 
	 * @return returnBox : 채무불이행 상태 리스트
	 */
	public SBox selectDebtProcess(String debtApplId) throws BizException {
		SBox returnBox = null;
		
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.selectDebtProcess_SQL", debtApplId)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return returnBox;
	}

	/**
	 * <pre>
	 * 채무불이행 금액에 대한 등록 수수료 조회 DAO
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 7. 4.
	 * @param stDebtAmt : 채무붏이행 금액
	 * @return
	 * @throws BizException
	 */
	public String selectDebtRegisterCharge(String stDebtAmt) throws BizException {
		String result = null;
		
		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debt.selectDebtRegisterCharge_SQL", stDebtAmt)).getString("REG_CHAG");
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		
		return result;
	}
	
	
	/**
	 * <pre>
	 *  채무불이행 전송 인터페이스 후 상태 업데이트  DAO METHOD
	 * </pre>
	 * 
	 * @author Jung mi Kim
	 * @since 2015. 5. 11.
	 * @param sBox debtApplId : 채무불이행 순번, sndEdYn : 전송여부
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebtStatSendEndForWS(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtStatSendEndForWS_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}

		return returnBox;
	}
	
	
	/**
	 * <pre>
	 * 채무불이행금액 정정 관리
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 9. 7.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebtModHistForIF(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtModHistForIF_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("31001", EErrorCodeType.search("31001").getErrMsg());
		}

		return returnBox;
	}
	
	
	
	/**
	 * <pre>
	 * 채무불이행 복사 후 작성 프로시져 상세 조회 DAO METHOD
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.05.10
	 * @param debtApplId : 채무불이행신청서순번, stat : 채무불이행신청서 상태값, sessionUsrId : 로그인 회원 순번
	 * @return 채무불이행 결과
	 * @throws BizException
	
	public SBox insertDebtCopy(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.insertDebtCopy_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return returnBox;
	} */
	
	/**
	 * <pre>
	 * 채무불이행신청 기존 미수채권 삭제 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param debtApplSeq
	 *            : 채무불이행신청서 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBox deleteDebtDebenture(String debtApplSeq) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.deleteDebtDebenture_SQL", debtApplSeq)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 채무불이행등록 STEP03 채권 리스트 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 4. 22.
	 * @param bondMbrId
	 *            : 채권담당자멤버순번, custId : 거래처 순번, orderCondition : 정렬조건, orderType : 정렬순서
	 * @return
	 * @throws BizException
	public SBoxList<SBox> selectDebentureList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.SelectDebentureListForDebt_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 *   채무불이행등록 상태 수정 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @param sBox
	 *            debtApplId : 채무불이행 순번, debtStatCd : 채무불이행 상태 코드, sessionUsrId : 회원 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBox updateDebtApplStat(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debt.updateDebtApplStat_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}

		return returnBox;
	}*/
	
}
