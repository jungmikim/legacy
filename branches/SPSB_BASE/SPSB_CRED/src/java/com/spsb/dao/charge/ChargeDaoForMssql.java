package com.spsb.dao.charge;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 요금관리 Dao Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 25.
 * @version 1.0
 */
@Repository
public class ChargeDaoForMssql extends SuperDao{

	/**
	 * <pre>
	 * 상품타입별 결제상품 사용가능 여부 확인 Map Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 25.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, SBox> selectUserPoCheckPerPrdTypeMap(Integer compUsrId){
		return (Map<String, SBox>) getSqlMapClientTemplate().queryForMap("charge.selectUserPoCheckPerPrdTypeMap_SQL", compUsrId, "PRD_TYPE");
	}
	
	/**
	 * <pre>
	 * KED 채무불이행등록수수료 조회 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @return
	 */
	public SBoxList<SBox> selectDebtRegChargeList(){
		String debtCompType = "E";
		return new SBoxList<SBox> (getSqlMapClientTemplate().queryForList("charge.selectDebtRegChargeList_SQL", debtCompType));
	}
	
	/**
	 * <pre>
	 * 기업회원순번의 구매리스트 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 27.
	 * @version 1.0
	 * @param sBox -
	 *			compUsrId : 기업회원순번, num : 페이징 순번 , rowSize: row수
	 * @return
	 */
	public SBoxList<SBox> selectUserPoList(SBox sBox){
		return new SBoxList<SBox> (getSqlMapClientTemplate().queryForList("charge.selectUserPoList_SQL", sBox));
	}
	
	/**
	 * <pre>
	 * 기업회원순번의 구매리스트 카운트 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 27.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @return
	 */
	public Integer selectUserPoListTotalCount(Integer compUsrId){
		return (Integer) getSqlMapClientTemplate().queryForObject("charge.selectUserPoListTotalCount_SQL", compUsrId);
	}
	
	/**
	 * <pre>
	 * 상품유형에 따른 회원의 구매check Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 28.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @param prdType : 상품유형
	 * @return
	 */
	public Integer selectUserPoCheckByPrdType(Integer compUsrId, String prdType){
		SBox sBox = new SBox();
		sBox.set("compUsrId", compUsrId);
		sBox.set("prdType", prdType);
		return (Integer) getSqlMapClientTemplate().queryForObject("charge.selectUserPoCheckByPrdType_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 상품코드에 따른 회원의 구매check Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 12. 02.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @param prdCd : 상품코드
	 * @return
	 */
	public Integer selectUserPoCheckByPrdCd(Integer compUsrId, String prdCd){
		SBox sBox = new SBox();
		sBox.set("compUsrId", compUsrId);
		sBox.set("prdCd", prdCd);
		return (Integer) getSqlMapClientTemplate().queryForObject("charge.selectUserPoCheckByPrdCd_SQL", sBox);
	}
	
}
