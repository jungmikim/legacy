package com.spsb.dao.config;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 환경설정  Dao Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 06. 25.
 * @version 1.0
 */
@Repository
public class ConfigDaoForOracle extends SuperDao{

	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보  개인회원멤버 등록 Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 07. 06.
	 * @version 1.0
	 * @param sBox	compUsrId : 기업회원순번, mbrUsrId : 멤버회원순번, mbrStat : 상태, mbrType : 멤버유형, admYn : 관리자여부, usrNm : 회원이름
	 * 			, email : 이메일주소, telNo : 전화번호, faxNo : 전화번호, mbNo : 휴대폰번호, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위
	 * @return
	 */
	public Integer insertMbr(SBox sBox)throws BizException{
		
		int result = 0;
		try {
			sBox.set("sbCompUsrId",sBox.getInt("sbCompUsrId"));
			sBox.set("sbUsrId",sBox.getInt("sbUsrId"));
			
			SBox temp = (SBox) getSqlMapClientTemplate().queryForObject("config.insertMbr_SQL", sBox);
			result=temp.getInt("MBR_SEQ");
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}
		
		return result;
		
	}

	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보  개인회원멤버 권한 등록 Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	public SBox insertMbrGrnt(SBox sBox) throws BizException{
		SBox resultBox = null;
		try {
			resultBox = (SBox) getSqlMapClientTemplate().queryForObject("config.insertMbrGrnt_SQL", sBox);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}
		return resultBox;
	}
	
	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보 총 갯수조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public int selectUserTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("config.selectUserListTotalCount_SQL", sBox)).getInt("TOTALCNT");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보  조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectUserList(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("config.selectUserList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보 회원 삭제 Service Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 7. 7.
	 * @version 1.0
	 * @param debentureIdList
	 *            : 회원 순번 String Array Data
	 * @param sessionUsrId
	 *            : 로그인한 개인회원 순번 Data
	 * @return
	 */
	public SBox deleteUser(String mbrIdList, int sessionUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mbrIdList", mbrIdList);
		sBox.set("sessionUsrId", sessionUsrId);
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("config.deleteUser_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보 사용자 조회 
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 7. 07.
	 * @version 1.0
	 * @param mbrId : 회원 순번
	 * @return
	 * @throws BizException
	 */
	public SBox selectUser(int mbrId)throws BizException {

		SBox result = new SBox();
		try {
			result = (SBox) super.getSqlMapClientTemplate().queryForObject("config.selectUser_SQL", mbrId);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보 회원 권한 리스트 조회
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 7. 07.
	 * @version 1.0
	 * @param mbrId : 회원 순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectUserGrnList(int mbrId) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("config.selectUserGrnList_SQL", mbrId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * 환경설정 - 사용자권한 및 회원정보 개인회원멤버 수정 Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 7. 07.
	 * @version 1.0
	 * @param sBox	mbrId : 멤버회원순번, updId : 수정자, compPersonalNo : 사번, email : 이메일주소, telNo : 전화번호
	 * @return
	 */
	public Integer updateMbr(SBox sBox)throws BizException{
		Integer result = null;
		try {
			result = (Integer) getSqlMapClientTemplate().queryForObject("config.updateUser_SQL", sBox);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39904", EErrorCodeType.search("39904").getErrMsg());
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 인터페이스 메시지 히스토리  등록 Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 23.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox insertIfMsgHist(SBox sBox)throws BizException {
		SBox result = null;
		try {
			result = (SBox) getSqlMapClientTemplate().queryForObject("config.insertIfMsgHist_SQL", sBox);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}
		return result;
		
	}
	
	/**
	 * <pre>
	 *  환경설정 - 사용자권한 및 회원정보 회원 권한 삭제 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 27.
	 * @version 1.0
	 * @param sBox - mbrId : 멤버회원순번
	 * @return
	 * @throws BizException
	 */
	public SBox deleteMbrGrnt(int mbrId) throws BizException {
		SBox returnBox = null;
		try {
			SBox temp = new SBox();
			temp.set("mbrId",mbrId);
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("config.deleteMbrGrnt_SQL", temp);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	/**
	 * <pre>
	 *   환경설정 - 사용자권한 및 회원정보 개인회원 조회 Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 27.
	 * @version 1.0
	 * @param loginId
	 * @return
	 */
	public SBox selectUserByLoginId(String loginId) throws BizException {
		
		SBox result = new SBox();
		try {
			SBox temp = new SBox();
			temp.set("loginId",loginId);
			result = (SBox) super.getSqlMapClientTemplate().queryForObject("config.selectUserByLoginId_SQL", temp);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}
		return result;
		
	}
	
	
	/**
	 * <pre>
	 * 채무불이행 등록전 회원정보 임시회원테이블에 등록
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 24.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox isertDeUsrTempForIF(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox =(SBox) super.getSqlMapClientTemplate().queryForObject("config.isertDeUsrTempForIF_SQL", sBox);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("39902", EErrorCodeType.search("39902").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * <pre>
	 * 스마트빌 매핑해서 가져온 정보에서 사용자 이름으로 검색 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 31.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectSearchUserNameList(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("config.selectSearchUserNameList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * <pre>
	 *  사용자 삭제여부 유뮤 체크 DAO
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015. 09.03
	 * @version 1.0
	 * @throws BizException
	 */
	public SBox checkAdminDeleteYN(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("config.checkAdminDeleteYN_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * <pre>
	 *  사용자 관리자 유뮤 체크 DAO
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015. 09.03
	 * @version 1.0
	 * @throws BizException
	 */
	public SBox checkAdminYN(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("config.checkAdminYN_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return returnBox;
	}
	
}
