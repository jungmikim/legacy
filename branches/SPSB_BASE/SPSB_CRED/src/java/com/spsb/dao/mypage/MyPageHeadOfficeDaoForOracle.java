/**
 * 
 */
package com.spsb.dao.mypage;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 마이페이지 - 정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Repository
public class MyPageHeadOfficeDaoForOracle extends SuperDao{

	// 마이페이지 - 본사관계 관리 
	/*public SBoxList<SBox> selectHeadOfficeList(SBox sBox)throws BizException {
		SBoxList<SBox> resultList;
		
		try {
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectOfficeList_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectOfficeList_SQL Dao ERROR");
		}
		return resultList;
	}*/
	
	//마이페이지 - 본사관계 관리  Count
		/*public int  selectOfficeListTotalCount(SBox sBox)throws BizException {
			int result = 0;
			
			try {
				result  = ((SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectOfficeListTotalCount_SQL", sBox)).getInt("totalCnt");
				
			}catch (Exception ex) {
				ex.printStackTrace();
				throw new BizException("21", "selectOfficeListTotalCount_SQL Dao ERROR");
			}
			return result;
		}*/
	
	
	
	// 마이페이지 - 본사관계 관리  update	
		public SBox updateHeadOffice(SBox sBox)throws BizException {
			SBox result;
			
			try {
			
			 result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.updateOffice_SQL", sBox);
			
			}catch (Exception ex) {
				ex.printStackTrace();
				throw new BizException("21", "updateOffice_SQL Dao ERROR");
			}
			return result;
		}
		
	// 마이페이지 - 본사관계 관리  	- 팝업창 사업자등록번호 
		public SBox selectHeadOfficeByUserNo(SBox sBox)throws BizException {
			SBox result;
			
			try {
			
			 result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.selectOfficeByUserNo", sBox);
			
			}catch (Exception ex) {
				ex.printStackTrace();
				throw new BizException("21", "selectOfficeByUserNo Dao ERROR");
			}
			return result;
		}	
		
	// 마이페이지 - 본사관계 관리  	- 팝업창 사업자등록번호  본사요청
			public SBox insertOfficeRelation(SBox sBox)throws BizException {
				SBox returnBox;
				
				try {
					returnBox = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.insertOfficeRelation", sBox)).get(0);
				
				}catch (Exception ex) {
					ex.printStackTrace();
					throw new BizException("21", "insertOfficeRelation Dao ERROR");
				}
				return returnBox;
			}
	
}
