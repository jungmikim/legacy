package com.spsb.dao.user;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 회원(개인, 기업) Dao Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 9.
 * @version 1.0
 */
@Repository
public class UserDaoForMssql extends SuperDao{

	/**
	 * <pre>
	 * 기업회원정보 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 30.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @return
	 */
	public SBox selectCompanyUser(int compUsrId){
		return (SBox) getSqlMapClientTemplate().queryForObject("user.selectCompanyUser_SQL", compUsrId);
	}
	
	/**
	 * <pre>
	 * 사업자등록번호로 기업회원 정보  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 21.
	 * @version 1.0
	 * @param usrNo : 사업자등록번호
	 * @return result : 기업회원 정보
	 */
	public SBox selectCompanyUserByUsrNo(String usrNo){
		return (SBox) getSqlMapClientTemplate().queryForObject("user.selectCompanyUserByUsrNo_SQL", usrNo);
	}
	
	/**
	 * <pre>
	 * 개인회원 조회 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 28.
	 * @version 1.0
	 * @param loginId : 로그인 식별자
	 * @return result : 개인회원 정보
	 */
	public SBox selectUserByLoginId(String loginId){
		return (SBox) getSqlMapClientTemplate().queryForObject("user.selectUserByLoginId_SQL", loginId);
	}
	
	/**
	 * <pre>
	 * 개인회원순번으로 개인회원 정보  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @param usrId : 개인회원순번
	 * @return result : 개인회원 정보
	 */
	public SBox selectUser(Integer usrId){
		return (SBox) getSqlMapClientTemplate().queryForObject("user.selectUser_SQL", usrId);
	}
	
	/**
	 * <pre>
	 * 개인회원순번으로 디폴트 기업회원멤버 카운트 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @param usrId : 개인회원순번
	 * @return
	 */
	public Integer selectDefaultMbrTotalCountByUserId(Integer usrId){
		return (Integer) getSqlMapClientTemplate().queryForObject("user.selectDefaultMbrTotalCountByUserId_SQL", usrId);
	}
	
	/**
	 * <pre>
	 * 임시회원순번으로 임시회원 정보  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 2. 5.
	 * @version 1.0
	 * @param tmpUsrId : 임시회원순번
	 * @return result : 임시회원 정보
	 */
	public SBox selectTempUser(Integer tmpUsrId){
		return (SBox) getSqlMapClientTemplate().queryForObject("user.selectTempUser_SQL", tmpUsrId);
	}
	
	/**
	 * <pre>
	 * 기업회원 등록 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 9.
	 * @version 1.0
	 * @param sBox
	 * 			stat : 상태, compType : 사업자유형, usrNo : 사업자등록번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호, mbNo : 휴대폰번호, signInfo : 직인정보, certInfo : 인증서정보
	 * 			, usrType : 사용자유형, postNo : 우편번호, addr1 : 주소1, addr2 : 주소2, ownNm : 대표자명, corpNo : 법인등록번호, bizType : 업종, bizCond : 업태
	 * @return result : 기업회원 순번
	 */
	public Integer insertCompanyUser(SBox sBox) {
		return (Integer) getSqlMapClientTemplate().queryForObject("user.insertCompanyUser_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 개인회원 등록 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 17.
	 * @version 1.0
	 * @param sBox
	 * 			usrStat : 상태, loginId : 로그인식별자, loginPw : 로그인비밀번호, usrNm : 회원이름, email : 이메일주소, telNo : 전화번호
	 * 			, mbNo : 휴대폰번호, postNo : 우편번호, addr1 : 주소1, addr2 : 주소2, erpUsrId : ERP회원순번
	 * @return
	 */
	public Integer insertUser(SBox sBox){
		return (Integer) getSqlMapClientTemplate().queryForObject("user.insertUser_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 개인회원멤버 등록 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 17.
	 * @version 1.0
	 * @param sBox
	 * 			compUsrId : 기업회원순번, mbrUsrId : 멤버회원순번, mbrStat : 상태, mbrType : 멤버유형, admYn : 관리자여부, usrNm : 회원이름
	 * 			, email : 이메일주소, telNo : 전화번호, faxNo : 전화번호, mbNo : 휴대폰번호, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위
	 * @return
	 */
	public Integer insertMbr(SBox sBox){
		return (Integer) getSqlMapClientTemplate().queryForObject("user.insertMbr_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 개인회원멤버 등록 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 17.
	 * @version 1.0
	 * @param sBox
	 * 			compUsrId : 기업회원순번, mbrUsrId : 멤버회원순번, mbrStat : 상태, mbrType : 멤버유형, admYn : 관리자여부, usrNm : 회원이름
	 * 			, email : 이메일주소, telNo : 전화번호, faxNo : 전화번호, mbNo : 휴대폰번호, grnCd : 권한코드, deptNm : 부서명, jobTlNm : 직위
	 * @return
	 */
	public Integer insertMbrForSB(SBox sBox){
		return (Integer) getSqlMapClientTemplate().queryForObject("user.insertMbrForSB_SQL", sBox);
	}
	
	
	
	/**
	 * <pre>
	 * 임시회원 등록 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 2. 5.
	 * @version 1.0
	 * @param sBox
	 * 			loginId : 로그인식별자, loginPw : 로그인비밀번호, stat : 상태
	 * @return
	 */
	public Integer insertTempUser(SBox sBox){
		return (Integer) getSqlMapClientTemplate().queryForObject("user.insertTempUser_SQL", sBox);
	}
}
