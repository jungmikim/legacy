package com.spsb.dao.debenture;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 *   채권관리 Dao Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2013. 09.02
 * @version 1.0
 */
@Repository
public class DebentureDaoForMssql extends SuperDao {

	/**
	 * <pre>
	 * 기술료 계약정보 단건 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 16.
	 * @param contId : 계약서 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBox selectContract(int contId) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectContract_SQL", contId)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}*/
	
	/**
	 * <pre>
	 * 기술료 계약서 증빙파일 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 24.
	 * @param fileSn : 기술료 계약서 증빙파일 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBox selectContractPrfFile(int fileSn) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectContractPrfFile_SQL", fileSn)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}*/
	
	/**
	 * <pre>
	 * 기술료 계약정보 증빙파일 리스트 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 20.
	 * @param contId
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectContractPrfFileList(int contId) throws BizException {
		SBoxList<SBox> resultList = new SBoxList<SBox>();
		
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectContractPrfFileList_SQL", contId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultList;
	}*/
	
	/**
	 * <pre>
	 * 기술료 계약서 조회를 위한 연관 기술료 단건 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 23.
	 * @param contId : 계약서 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectOtherDebentureListForContract(int contId) throws BizException {
		SBoxList<SBox> resultList = new SBoxList<SBox>();
		
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectOtherDebentureListForContract_SQL", contId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultList;
	}
	*/
	/**
	 * <pre>
	 *   수금조회 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param rowSize
	 *            : 목록갯수[10, 30, 50, 100] , num : 현재 페이지, sessionCompUsrId : 기업회원순번, sessionUsrId : 개인회원순번, bondMine : 구분[나의채권, 전체채권] , businessType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명],
	 *            custKwd : 거래처 검색키워드, firstDate : 수금조회 초기날짜, lastDate : 수금조회 마지막날짜, bondChargeUser : 채권담당자, orderCondition : 정렬조건[등록일자, 신용정보 확인일자], orderType : 정렬유형[오름차순, 내림차순]
	 * 
	 * @return resultList : 채권 수금조회 검색 리스트
	 */
	/*public SBoxList<SBox> selectDebentureCollectList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureCollectList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   채권 수금조회 Total Count Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param sessionUsrId
	 *            : 개인회원순번, sessionCompUsrId : 기업회원순번, bondMine : 구분[나의채권, 전체채권] , businessType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, firstDate : 수금조회 초기날짜, lastDate
	 *            : 수금조회 마지막날짜, bondChargeUser : 채권담당자
	 * 
	 * @return result : 채권 수금조회 Total Count
	 */
	/*public SBox selectDebentureCollectTotalCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureCollectTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권담당자 리스트 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번
	 * 
	 * @return resultList : 채권담당자 리스트 stat : 상태값
	 */
	/*public SBoxList<SBox> selectDebentureContactList(int sessionCompUsrId, String stat) throws BizException {

		SBox sBox = new SBox();
		SBoxList<SBox> resultList;
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("stat", stat);
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureContactList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}
*/
	/**
	 * <pre>
	 * 채권별 수금계획 조회 리스트 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 09. 06
	 * @version 1.0
	 * @param rowSize
	 *            : 목록갯수[10, 30, 50, 100] , num : 현재 페이지, sessionCompUsrId : 기업회원순번, sessionUsrId : 개인회원순번, bondMine : 구분[나의채권, 전체채권] , businessType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명],
	 *            custKwd : 거래처 검색키워드, firstDate : 수금조회 초기날짜, lastDate : 수금조회 마지막날짜, prsType : 독촉유형[A: 독촉 B: 추심 C:내용증명 D:채무불이행등록 E:소송] , dtType : 조회기간유형 (작성일자, 수금계획일자) rmnType : 잔액유형(전체,0원, 직접입력)
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자], orderType : 정렬유형[오름차순, 내림차순], firSearchRMN : 잔액 최소범위, lastSearchRMN : 잔액 최대범위
	 */
	/*public SBoxList<SBox> selectDebentureCollectPlanList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureCollectPlanList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   채권별 수금계획 조회 Total Count Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 05. 22.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번, sessionUsrId : 개인회원순번, bondMine : 구분[나의채권, 전체채권] , businessType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, firstDate : 수금조회 초기날짜, lastDate :
	 *            수금조회 마지막날짜, prsType : 독촉유형[A: 독촉 B: 추심 C:내용증명 D:채무불이행등록 E:소송] , dtType : 조회기간유형 (작성일자, 수금계획일자), rmnType : 잔액유형(전체,0원, 직접입력), firSearchRMN : 잔액 최소범위, lastSearchRMN : 잔액 최대범위
	 * 
	 * 
	 * @return result : 채권 수금조회 Total Count
	 */
	/*public SBox selectDebentureCollectPlanTotalCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureCollectPlanTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 기술료 수금 계획 정보 수정 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 11.
	 * @param colSn : 기술료 수금계획 순번, debnId : 기술료 순번, sessionUsrId : 수정자 회원 순번, stat : 수금계획 상태, colDt : 수금일자, colAmt : 수금금액
	 * @return
	 * @throws BizException
	
	public SBox updateCollectPlan(SBox sBox) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.updateCollectPlan_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	} */
	
	/**
	 * <pre>
	 * 기술료 수금 정보 수정 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 12.
	 * @param debnId : 기술료 순번, sessionUsrId : 수정자 회원 순번, colStat : 수금 상태, colDt : 수금일자, colSum : 수금액
	 * @return
	 * @throws BizException
	 */
	/*public SBox updateDebentureCollectInfo(SBox sBox) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.updateDebentureCollectInfo_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}
	*/
	/**
	 * <pre>
	 * 기술료 수금 계획 정보 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 13.
	 * @param colSn : 기술료 수금 계획 순번
	 * @return
	 * @throws BizException
	 */
	/*public SBox selectDebentureCollectPlan(int colSn) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureCollectPlan_SQL", colSn);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}*/
	
	/**
	 * <pre>
	 * 기술료(채권) 독촉 등록 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 16.
	 * @param sBox contId : 기술료 계약서 순번, debnId : 기술료(채권) 순번, usrId : 회원순번, prsType : 독촉 유형, prsDt : 독촉 일자, rmkTxt : 비고
	 * @return
	 * @throws BizException
	 */
	/*public SBox insertDebenturePrs(SBox sBox) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.insertDebenturePrs_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		
		return resultBox;
	}*/
	
	/**
	 * <pre>
	 * 기술료(채권) 독촉 첨부파일 등록 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 16.
	 * @param sBox prsSn : 독촉 순번, fileNm : 파일명, filePath : 파일 경로
	 * @return
	 * @throws BizException
	 */
	/*public SBox insertDebenturePrsFile(SBox sBox) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			resultBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.insertDebenturePrsFile_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}*/
	
	/**
	 * <pre>
	 *   금일 수금 계획 리스트 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 9. 9.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번
	 * @param isSessionDebnSearchGrn
	 *            : 채권 조회 권한
	 * @param sessionUsrId
	 *            : 개인회원순번
	 * @param sessionMbrId
	 *            : 기업회원멤버순번
	 * 
	 * @return resultList : 금일 수금 계획 리스트
	 */
	/*public SBoxList<SBox> selectTodayDebenturePlanList(int sessionCompUsrId, boolean isSessionDebnSearchGrn, int sessionUsrId, int sessionMbrId) throws BizException {

		SBox sBox = new SBox();
		SBoxList<SBox> resultList;
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("isSessionDebnSearchGrn", (isSessionDebnSearchGrn ? "Y" : "N"));
		sBox.set("sessionUsrId", sessionUsrId);
		sBox.set("sessionMbrId", sessionMbrId);
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectTodayDebenturePlanList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 * 채권담당자 중복체크 로직 Dao Method
	 * </pre>
	 * 
	 * @author yeonjinyoon
	 * @since 2013. 09. 06.
	 * @param usrNm
	 *            : 이름
	 * @param sessionCompUsrId
	 *            : 기업회원 순번
	 * @return
	 */
	/*public SBox selectDuplicationCheckBondMember(String usrNm, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("usrNm", usrNm);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDuplicationCheckBondMember_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 기업회원맴버 순번 등록 Dao Method
	 * </pre>
	 * 
	 * @author yeonjinyoon
	 * @since 2013. 09. 08.
	 * @version 1.0
	 * @param sBox
	 *            : [ 세션 ] sessionCompUsrId : 기업회원순번, #sessionUsrId : 채권등록자회원순번, sessionMbrId : 채권담당자맵버순번 sessionMbrId : 기업회원멤버 순번, #sessionUsrId : 개인회원 순번 [ 채권 등록 ] CustType : 거래처 유형, custNm : 회사명 or
	 *            이름, custNo : 사업자등록번호 or 주민등록번호, billDt : 세금계산서작성일자, itmNm : 품목명, amt : 공급가액, tax : 세액, sumAmt : 채권합계금액, billType : 세금계산서유형, subCompCd : 종사업장코드, bondMbrId : 채권담당자 코드, bondMbrNm : 채권담당자,
	 *            prfDocFile : 증빙파일, prfDocCd : 증빙서류코드, ltdDt : 만기일, custId : 거래처 순번 [ 거래처 등록 ] ownNm : 대표자, ownNo1 : 대표자 주민등록번호[앞자리], ownNo2 : 대표자 주민등록번호[뒷자리], custNo : 사업자등록번호, eMail1 : 이메일 주소,
	 *            eMail2 : 이메일 도메인, postNo1 : 우편번호[앞자리], postNo2 : 우편번호[뒷자리], custAddr1 : 주소, custAddr2 : 상세주소, bizCond : 업태, bizType : 업종, telNo1 : 전화번호 국번, telNo2 : 전화번호[앞자리], telNo3 : 전화번호[뒷자리],
	 *            faxNo1 : 팩스번호 국번, faxNo2 : 팩스번호[앞자리], faxNo2 : 팩스번호[뒷자리], crdLtd : 여신한도, firstDebn : 초기미수금, initPayTermType : 초기 결제조건, initPayTermDay : 초기 결제조건 일수, futurePayTermType : 차후 결제조건,
	 *            futurePayTermDay : 차후 결제 일수, payTermDate : 결제조건 적용 날짜,
	 * @return
	 */
	/*public SBox insertDebentureMember(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertDebentureMember_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 기간별 통계 페이지 로딩 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 11. 20.
	 * @version 1.0
	 * @param firstDate
	 *            : 검색 시작일자, lastDate : 검색 종료일자, sessionCompUsrId : 기업회원순번
	 * @return
	 */
	/*public SBoxList<SBox> selectDebentureStatisticsForPeriodical(String firstDate, String lastDate, int sessionCompUsrId) throws BizException {

		SBoxList<SBox> resultList;

		SBox paramBox = new SBox();
		paramBox.set("firstDate", firstDate);
		paramBox.set("lastDate", lastDate);
		paramBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsForPeriodical_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 * 거래처 별 통계 페이지 로딩 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2015.02.24.
	 * @version 1.0
	 * @param firstDate
	 *            : 검색 시작일자, lastDate : 검색 종료일자, rowSize : 거래처별 통계 조회 출력목록개수, num : 거래처별 통계 조회 현재 페이지 순번, sessionCompUsrId : 기업회원순번
	 * @return
	 */
	/*public SBoxList<SBox> selectDebentureStatisticsForCustomer(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsForCustomer_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 * 거래처 별 통계 TOTAL 합계 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2015.03.13.
	 * @version 1.0
	 * @param firstDate
	 *            : 검색 시작일자, lastDate : 검색 종료일자, sessionCompUsrId : 기업회원순번
	 * @return
	 */
	/*public SBoxList<SBox> selectDebentureStatisticsForCustomerTotal(SBox sBox) throws BizException {

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsForCustomerTotal_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 * 통계 페이지 토탈 카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2015.02.24.
	 * @version 1.0
	 * @param firstDate
	 *            : 검색 시작일자, lastDate : 검색 종료일자, sessionCompUsrId : 기업회원순번
	 * @return
	 */
	/*public SBox selectDebentureStatisticstTotalCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 거래처 별 통계 페이지 엑셀 다운로드 Dao Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2015.02.24.
	 * @param sBox sessionCompUsrId : 기업회원순번, firstDate : 검색 시작 기간, lastDate : 검색 종료 기간
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDebentureStatisticsForCustomerExcel(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsForCustomerExcel_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 * 기간 별 통계 페이지 엑셀 다운로드 Dao Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2015. 2. 9.
	 * @param sBox sessionCompUsrId : 기업회원순번, firstDate : 검색 시작 기간, lastDate : 검색 종료 기간
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDebentureStatisticsForPeriodicalExcel(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureStatisticsForPeriodicalExcel_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   대량등록 체크 여부를 조회하는 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @return : 대량등록유형 (B: 채권) ,MLT_ID : 대량등록번호 순번
	 */
	/*public SBox selectMulti(String mltType, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("mltType", mltType);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 채권 대량등록 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @version 1.0
	 * @param fileName
	 *            : 파일이름 , mltType: 대량등록 유형, stat: 대량등록 상태 sessionCompUsrId : 기업회원순번, sessionUsrId : 개인회원순번
	 * @return
	 */
	/*public SBox insertMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 임시채권 대량등록 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @version 1.0
	 * @param cust_type
	 *            : 거래처 유형 , cust_nm : 거래처 명, cust_no : 사업자등록번호, bill_dt : 세금작성일자, itm_nm : 품목명 amt : 공급가액 , tax : 세액 , bill_type :세금계산서유형 , sub_comp_code : 종사업장번호 ,usr_nm :채권담당자 ,ltd_dt : 만기일자,
	 *            aprv_no : 세금계산서 승인번호
	 * @return
	 */
	/*public SBox insertTempDebentureMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertTempDebentureMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *  대량등록이력 카운트 조회Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @param num
	 *            : 현재 페이지 번호 , rowSize : 출력될 목록의 갯수, sessionCompUsrId : 기업회원 순번 , searchType : 검색 유형(리스트, 엑셀) , fileName : 파일이름, debnType : 채권검색유형 , firstDate : 업로드 초기날짜 ,lastDate :업로드 마지막 날짜
	 * @return
	 */
	/*public SBox selectMultiHistoryTotalCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectMultiHistoryTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 채권 대량등록이력 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @param num
	 *            : 현재 페이지 번호 , rowSize : 출력될 목록의 갯수, sessionCompUsrId : 기업회원 순번 , searchType : 검색 유형(리스트, 엑셀) , fileName : 파일이름, debnType : 거래처 유형 , firstDate : 업로드 초기날짜 ,lastDate :업로드 마지막 날짜
	 * @return
	 */
	/*public SBoxList<SBox> selectMultiHistoryList(SBox sBox) throws BizException {

		SBoxList<SBox> returnBoxList = null;
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectMultiHistoryList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBoxList;
	}*/

	/**
	 * <pre>
	 *   오류난 채권 대량등록 삭제 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번 , stat: 상태(Y:오류있음 N:오류없음) , mltId: 대량등록 유형(거래처, 채권)
	 */
	/*public SBox deleteTempDebentureMulti(String stat, int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("stat", stat);
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteTempDebentureMulti_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   대량이력 삭제 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 */
	/*public SBox deleteMulti(int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteMulti_SQL", sBox)).get(0);

		} catch (Exception ex) {
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 임시저장 채권 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author YOUKYUNG HONG
	 * @since 2015. 3. 2.
	 * @param sessionCompUsrId
	 *            : 기업회원 순번 , stat : 상태 (Y: 오류있음,N: 오류없음)
	 * 
	 * @return
	 */
	/*public SBoxList<SBox> selectDebentureTemporary(int sessionCompUsrId, String stat) throws BizException {

		SBoxList<SBox> returnBoxList = null;

		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("stat", stat);
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureTemporary_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBoxList;
	}*/

	/**
	 * <pre>
	 *   성공한 임시채권 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 * @return resultList : 성공한 임시채권 리스트
	 */
	/*public SBoxList<SBox> selectTempDebentureList(int mltId, int sessionCompUsrId) throws BizException {

		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectTempDebentureList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *  성공한 임시채권 카운트 조회   Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 * @return
	 */
	/*public SBox selectTempDebentureTotalCount(int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectTempDebentureTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 대량이력 수정 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번 , stat : 상태 totalCnt : 성공한 임시거래처 카운트수
	 * @return
	 */
	/*public SBox updateMulti(int mltId, int sessionCompUsrId, String stat, int sessionUsrId, int totalCnt) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("stat", stat);
		sBox.set("sessionUsrId", sessionUsrId);
		sBox.set("totalCnt", totalCnt);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   대량등록 상태값을 조회하는 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 12. 03.
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @return : 대량등록유형 (B: 채권) ,MLT_ID : 대량등록번호 순번
	 */
	/*public SBox selectMultiStat(String mltType, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("mltType", mltType);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectMultiStat_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	// 수정 및 추가 영역

	/**
	 * <pre>
	 *   채권 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 9. 3.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업회원순번, debnSearchType : 기업 검색 조건, bizKwd : 기업 검색 키워드, debnMngNoType : 관리번호 검색 조건, mngNoKwd : 관리번호 검색 키워드, 
	 *            searchPeriodType : 기간 검색 조건, periodStDt : 기간 시작일자, periodEdDt : 기간 종료일자, stDelayDay : 연체일수 시작조건 일수, edDelayDay : 연체일수 종료조건 일수, 
	 *            nonpaymentType : 미납여부 검색 조건, contStat : 계약서 상태, num : 페이지 번호, rowSize : 페이지 목록 갯수, orderCondition : 정렬 조건, orderType : 정렬 순서
	 * @return resultList : 채권 검색 리스트
	 */
	/*public SBoxList<SBox> selectDebentureList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   채권 Total Count Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 9. 3.
	 * @version 1.0
	 * @param sessionCompUsrId : 기업회원순번, debnSearchType : 기업 검색 조건, bizKwd : 기업 검색 키워드, debnMngNoType : 관리번호 검색 조건, mngNoKwd : 관리번호 검색 키워드, 
	 * 			searchPeriodType : 기간 검색 조건, periodStDt : 기간 시작일자, periodEdDt : 기간 종료일자, stDelayDay : 연체일수 시작조건 일수, edDelayDay : 연체일수 종료조건 일수, 
	 * 			nonpaymentType : 미납여부 검색 조건, contStat : 계약서 상태
	 * 
	 * @return result : 채권 검색 TOTAL COUNT
	 */
	/*public int selectDebentureTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}*/

	/**
	 * <pre>
	 * 엑셀 출력을 위한 기술료(채권) 리스트 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 9.
	 * @param sessionCompUsrId : 기업회원순번, debnSearchType : 기업 검색 조건, bizKwd : 기업 검색 키워드, debnMngNoType : 관리번호 검색 조건, mngNoKwd : 관리번호 검색 키워드, 
	 * 			searchPeriodType : 기간 검색 조건, periodStDt : 기간 시작일자, periodEdDt : 기간 종료일자, stDelayDay : 연체일수 시작조건 일수, edDelayDay : 연체일수 종료조건 일수, 
	 * 			nonpaymentType : 미납여부 검색 조건, contStat : 계약서 상태, orderCondition : 정렬 조건, orderType : 정렬 순서
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDebentureListForExcel(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureListForExcel_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 * 기술료 증빙 파일 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 15
	 * @version 1.0
	 * @param debnId : 채권 코드 , contId : 계약순번, typeCd : 서류유형, fileNm : 첨부파일명, filePath : 첨부파일경로
	 * @return
	 */
	/*public SBox insertDebentureFiles(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertDebentureFile_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("20205", EErrorCodeType.search("20205").getErrMsg());
		}
		return returnBox;
	}*/
	

	/**
	 * <pre>
	 * 채권 정보 단건 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 09.
	 * @param debnId
	 *            : 채권번호, sessionCompUsrId : 기업회원 순번
	 * @return
	 */
	/*public SBox selectDebenture(String debnId, int sessionCompUsrId) throws BizException {

		// Parameter Box Setting
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebenture_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 채권 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 15.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 기술료 순번, sessionUsrId : 수정회원순번, colStat : 수금상태
	 *         	  mngNo : 계약관리번호, plnColDt : 수금예정일, plnColAmt : 수금예정금액, colSum : 수금금액
	 *            colDt : 수금일, rmkTxt : 비고
	 * @return
	 */
	public SBox updateDebenture(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebenture_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *  거래증빙파일 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 15.
	 * @param debnId
	 *            : 채권 번호, deleteDebnFileSN : 거래증빙파일 순번 리스트
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDebentureFileListInfo(String debnId, String deleteDebnFileSN) throws BizException {
		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);
		sBox.set("deleteDebnFileSN", deleteDebnFileSN);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureFileListInfo_SQL", sBox));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 *   거래증빙자료 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 17.
	 * @version 1.0
	 * 
	 * @param debnId
	 *            : 채권번호, deleteDebnFileSN : 삭제 대상 파일 정보 순번
	 * @return
	 */
	public SBox deleteDebentureFileInfo(String debnId, String deleteDebnFileSN) throws BizException {

		SBox sBox = new SBox();
		sBox.set("debnId", debnId);
		sBox.set("deleteDebnFileSN", deleteDebnFileSN);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteDebentureFileInfo_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *   채권 수금계획 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 16.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호
	 * 
	 * @return resultList : 채권 수금계획 리스트
	 */
	public int selectDebenturePlanListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebenturePlanListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   채권 수금계획 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 20.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호, debenturePlanListNum : 채권 수금계획 페이지 번호, rowSize : 페이지 당 결과 개수
	 * 
	 * @return resultList : 거래처 수금계획 리스트
	 */
	public SBoxList<SBox> selectDebenturePlanList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebenturePlanList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 * 기술료 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 11.
	 * @version 1.0
	 * @param sBox 
	 * 		   contId : 계약서 순번, usrId : 등록회원순번, stat : 상태, colStat : 수금상태
	 *         mngNo : 계약관리번호, plnColDt : 수금예정일, plnColAmt : 수금예정금액, colSum : 수금금액
	 *         colDt : 수금일, rmkTxt : 비고 
	 * @return
	 */
	public SBox insertDebenture(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertDebenture_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *   채권 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 9. 6.
	 * @version 1.0
	 * @param debentureIdList
	 *            : 채권 순번 String (ex : 1,2,3,)
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @param sessionUsrId
	 *            : 로그인한 회원 순번 Data
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	public SBox deleteDebenture(String debentureIdList, int sessionCompUsrId, int sessionUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("debentureIdList", debentureIdList);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("sessionUsrId", sessionUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteDebenture_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *   채권 독촉 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 21.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호
	 * 
	 * @return result : 채권 독촉 토탈카운트
	 */
	public int selectDebenturePrsListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebenturePrsListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   채권 독촉 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호, debenturePrsListNum : 채권 독촉 페이지 번호, rowSize : 페이지 당 결과 개수
	 * 
	 * @return resultList : 채권 수금계획 리스트
	 */
	public SBoxList<SBox> selectDebenturePrsList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebenturePrsList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 *   채권 수금업무 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 22.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호
	 * 
	 * @return result : 채권 수금 토탈카운트
	 */
	public int selectDebentureColListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureColListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   채권 수금업무 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 번호, debentureColListNum : 채권 수금업무 페이지 번호, rowSize : 페이지 당 결과 개수
	 * 
	 * @return resultList : 채권 수금계획 리스트
	 */
	public SBoxList<SBox> selectDebentureColList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureColList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 *   채권 가이드 모아보기 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014 05. 22.
	 * @version 1.0
	 * @param debnId
	 *            : 채권번호 , customDayTerm : 경과일수
	 * 
	 * @return resultList : 채권 가이드 조회 결과
	 */
	public SBoxList<SBox> selectDebentureGuideAllView(SBox guideParamBox) throws BizException {
		SBoxList<SBox> resultList;
		guideParamBox.set("customDayTerm", "".equals(guideParamBox.getString("customDayTerm")) ? null : guideParamBox.getString("customDayTerm"));
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureGuideAllView_SQL", guideParamBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 *   채권 진단 키워드 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014 05. 28.
	 * @version 1.0
	 * @param dgsRsltClsId
	 *            : 채권진단결과문구순번, debnId : 채권번호
	 * 
	 * @return resultList : 채권 진단 키워드 조회 결과
	 */
	public SBoxList<SBox> selectDebentureGuideKeyword(String dgsRsltClsId, String debnId) throws BizException {
		SBox paramBox = new SBox();
		paramBox.set("dgsRsltClsId", dgsRsltClsId);
		paramBox.set("debnId", debnId);

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureGuideKeyword_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}

	/**
	 * <pre>
	 *   채권 진단 서브 키워드 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014 05. 28.
	 * @version 1.0
	 * @param dgsSubKwId
	 *            : 채권진단서브키워드순번
	 * 
	 * @return resultList : 채권 진단 서브키워드 의미 조회 결과
	 */
	public SBox selectDebentureGuideKeywordMean(String dgsSubKwId) throws BizException {

		SBox returnBox = null;
		SBox paramBox = new SBox();
		paramBox.set("dgsSubKwId", dgsSubKwId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureGuideKeywordMean_SQL", paramBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 * 채권 관리 메인 키워드 등록
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014 . 05. 29
	 * @version 1.0
	 * @param debnId
	 *            : 채권순번, dgsRsltClsIdTypeTList : 채권진단결과문구순번 리스트 ex) 4,5,3,2,
	 * @return resultList : 채권진단메인키워드관리 조회 결과
	 */
	public SBoxList<SBox> insertDebentureMainKeywordMNG(String debnId, String dgsRsltClsIdTypeTList) throws BizException {

		SBoxList<SBox> resultList;

		SBox paramBox = new SBox();
		paramBox.set("debnId", debnId);
		paramBox.set("dgsRsltClsIdTypeTList", dgsRsltClsIdTypeTList);
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertDebentureMainKeywordMNG_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return resultList;
	}

	/**
	 * <pre>
	 * 채권별 메인 키워드 상태 수정 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 06. 02.
	 * @version 1.0
	 * @param debnId
	 *            : 채권번호, dgsMnKwId : 채권진단메인키워드순번 , dgsRsltClsId : 채권진단결과문구순번 , stat : 상태 (T: 처리할것(ToDo) , C: 처리완료(Completed) , N: 처리안함(DoNot)) , searchStat : 검색할 상태값, custId : 거래처 순번
	 * @return
	 */
	public SBox updateDebentureGuideKeywordMNG(String debnId, String dgsMnKwId, String dgsRsltClsId, String stat, String searchStat, String custId) throws BizException {

		SBox paramBox = new SBox();
		paramBox.set("debnId", debnId);
		paramBox.set("dgsMnKwId", ("".equals(dgsMnKwId) ? null : dgsMnKwId));
		paramBox.set("dgsRsltClsId", ("".equals(dgsRsltClsId) ? null : dgsRsltClsId));
		paramBox.set("stat", stat);
		paramBox.set("searchStat", searchStat);
		paramBox.set("custId", ("".equals(custId) ? null : custId));

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureGuideKeywordMNG_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 * 채권 가이드 메인키워드 삭제 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014 . 06. 09
	 * @version 1.0
	 * @param debnId
	 *            : 채권순번
	 * @return resultList : 채권진단메인키워드관리 조회 결과
	 */
	public SBox deleteDebentureGuideMainKeywordMNG(String debnId) throws BizException {

		SBox resultBox;

		SBox paramBox = new SBox();
		paramBox.set("debnId", debnId);
		try {
			resultBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteDebentureGuideMainKeywordMNG_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return resultBox;
	}

	/**
	 * <pre>
	 *   증빙자료 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 6. 13.
	 * @version 1.0
	 * @param fileSn
	 *            : 증빙자료 순번
	 * 
	 * @return result : 증빙자료 정보
	 */
	public SBox selectPrfFile(int fileSn) throws BizException {

		SBox returnBox = null;

		SBox sBox = new SBox();
		sBox.set("fileSn", fileSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectPrfFile_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *   기술료 수금계획 계획중 존재 여부 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 3. 02.
	 * @version 1.0
	 * @param sBox
	 * 			debnId : 기술료 순번, contId : 계약서 순번, stat : 상태
	 * @return
	 */
	public int selectDebenturePlanExist(int debnId, int contId, String stat) throws BizException {

		int result;
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);
		sBox.set("contId", contId);
		sBox.set("stat", stat);

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebenturePlanExist_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   채권가이드 진행 이력 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 07. 23.
	 * @version 1.0
	 * @param debnId
	 *            : 채권번호
	 * 
	 * @return 메인키워드 갯수
	 */
	public int selectDebentureGuideHistoryCheck(String debnId) throws BizException {

		int result;
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureGuideHistoryCheck_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   결제 조건 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 07. 23.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권번호, custId : 거래처 순번, billDt : 세금계산서 작성일자, sessionPayTermType : 세션의 결제조건 유형, sessionPayTermDay : 세션의 결제조건 일수
	 * 
	 * @return 결제 조건 정보
	 */
	public SBox selectPayTermInfo(SBox sBox) throws BizException {

		SBox returnBox;

		try {

			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectPayTermInfo_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return returnBox;
	}
	
	/* ********************************* NEW METHOD ********************************* */
	
	/**
	 * <pre>
	 *   계약관리번호 중복 체크 조회 Dao Method
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2015. 02. 08.
	 * @version 1.0
	 * @param sBox
	 *         contCd : 계약관리번호, sessionCompUsrId : 기업멤버 식별자
	 * @return 계약관리번호 중복 카운트
	 */
	public int selectContCdDuplicateCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectContCdDuplicateCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 기술료관리번호 중복 체크 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 02. 26.
	 * @param mngNo : 기술료관리번호, sessionCompUsrId : 기업멤버 식별자
	 * @return 계약관리번호 중복 카운트
	 */
	public int selectMngNoDuplicateCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectMngNoDuplicateCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 계약서 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 10.
	 * @version 1.0
	 * @param sBox 
	 * 		   custId : 거래처순번, sessionCompUsrId : 기업회원순번, sessionUsrId : 등록자,
	 * 		   contCd : 계약관리번호, custNm : 거래처명, custNo : 사업자등록번호,
	 *         ownNm : 대표자, contDt : 계약일자, contAmt : 계약금액, rmkTxt : 비고,
	 * 		   stat : 상태, contMltId : 대량등록식별자, fileNm : 대량등록파일명
	 * @return
	 */
	public SBox insertContract(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertContract_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 계약 증빙 파일 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 11
	 * @version 1.0
	 * @param sBox
	 * 			contId : 계약순번, typeCd : 증빙유형, fileNm : 파일명, filePath : 파일경로
	 * @return
	 */
	public SBox insertContractFile(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertContractFile_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("20205", EErrorCodeType.search("20205").getErrMsg());
		}
		return returnBox;
	}

	/**
	 * <pre>
	 *  연관 기술료 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 13.
	 * @param contId : 계약순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectOtherDebentureList(int contId) throws BizException{
		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("contId", contId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectOtherDebentureList_SQL", sBox));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * <pre>
	 *   메인 키워드 존재 유무 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 23.
	 * @version 1.0
	 * @param debnId : 채권 번호
	 * 
	 */
	public int selectDebentureGuideMainKeywordMNG(String debnId) throws BizException {

		int result;
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureGuideMainKeywordMNG_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 기술료 수정/삭제에 따른 계약 정보 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 24.
	 * @version 1.0
	 * @param sBox 
	 * 		   contId : 계약서 순번, debnId : 기술료 순번 
	 * @return
	 */
	public SBox updateContractForModifyRemoveDebenture(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateContractForModifyRemoveDebenture_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *    기술료 독촉 삭제[실제로는 수정] Dao Method
	 *    CustomerDao 에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 2. 24.
	 * @version 1.0
	 * @param sBox
	 *            deleteParam : 삭제 대상 독촉 순번
	 * @return
	 */
	public SBox deleteDebenturePrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteDebenturePrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 독촉 정보 조회 Dao Method
	 *   CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 24.
	 * @param prsSn
	 *            : 독촉 순번
	 * @return
	 */
	public SBox selectDebenturePrs(int prsSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("prsSn", prsSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebenturePrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 독촉 파일 조회 Dao Method
	 *   CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 24.
	 * @version 1.0
	 * @param sBox
	 * 			prsSn : 독촉 순번
	 * @return resultList : 파일 리스트
	 */
	public SBoxList<SBox> selectFileListForDebenturePrs(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectFileListForDebenturePrs_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * <pre>
	 *   독촉 증빙파일 삭제 대상 조회 DAO METHOD
	 *   CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 24.
	 * @param deleteFileParam
	 *            : 독촉 파일 순번 리스트
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDeleteDebenturePrsFileListInfo(String deleteFileParam) throws BizException {
		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("deleteFileParam", deleteFileParam);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDeleteDebenturePrsFileListInfo_SQL", sBox));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * <pre>
	 *   기술료 독촉 첨부파일 정보 삭제 Dao Method
	 *   CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 2. 24.
	 * @version 1.0
	 * @param deleteFileParam
	 *            : 삭제할 파일 정보 순번
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	public SBox deleteFileDebenturePrs(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteFileDebenturePrs_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 기술료 독촉 마스터 수정 Dao Method
	 * CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 25.
	 * @version 1.0
	 * @param sBox
	 *            prsSn : 독촉 순번, rmkTxt : 비고, debnCnt : 채권수, prsType : 독촉유형, prsDt : 독촉일자 sessionCompUsrId : 기업회원 순번, sessionUsrId : 회원 순번
	 * @return
	 */
	public SBox updateDebenturePrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebenturePrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   독촉 첨부파일 조회 Dao Method
	 *   CustomerDao에서 이동
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 25.
	 * @version 1.0
	 * @param fileSn
	 *            : 증빙자료 순번
	 * 
	 */
	public SBox selectDebenturePrsFile(int fileSn) throws BizException {

		SBox returnBox = null;

		SBox sBox = new SBox();
		sBox.set("fileSn", fileSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebenturePrsFile_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금계획실적 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 5.
	 * @param colSn
	 *            : 수금계획실적순번
	 * @return
	 */
	public SBox selectDebentureCol(int colSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("colSn", colSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금계획실적 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 2. 25.
	 * @version 1.0
	 * @param sBox
	 *            contId : 계약서순번, debnId : 기술료 순번, sessionUsrId : 로그인회원순번, stat : 상태, 
	 *            plnColDt : 수금예정일, plnColAmt : 수금예정금액, colDt : 수금일자, colAmt : 수금금액,
	 *            rmkTxt : 비고
	 * @return
	 */
	public SBox insertDebentureCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금계획실적 연기 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 26.
	 * @version 1.0
	 * @param sBox
	 *            colSn : 기술료 수금계획실적 순번, newColSn : 새로운 기술료 수금계획실적 순번
	 *            stat : 수금계획 상태, rmkTxt : 비고, sessionUsrId : 로그인 회원 순번
	 * @return
	 */
	public SBox updateDebentureColStatForDelay(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureColStatForDelay_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금계획실적 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 27.
	 * @version 1.0
	 * @param sBox
	 *            colSn : 수금계획실적 순번, colDt : 수금일자, colAmt : 수금액, 
	 *            rmkTxt : 비고, colStatType : 기술료 수금계획실적 상태(부분수금/완전수금), sessionUsrId : 회원 순번
	 * @return
	 */
	public SBox updateDebentureCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금금액합계 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 27.
	 * @version 1.0
	 * @param sBox
	 *            contId : 계약서 순번, debnId : 채권 순번
	 * @return
	 */
	public SBox updateDebentureForColSum(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureForColSum_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   기술료 수금계획 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 27.
	 * @version 1.0
	 * @param sBox
	 *            colSn : 기술료 수금계획실적 순번, plnColDt : 수금계획일자, plnColAmt : 수금예정금액, rmkTxt : 비고, sessionUsrId : 로그인회원순번
	 * @return
	 */
	public SBox updateDebenturePlan(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebenturePlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *    기술료 수금계획실적 삭제[실제로는 수정] Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 3. 02.
	 * @version 1.0
	 * @param sBox
	 *            contId : 계약서 순번, debnId : 기술료 순번, deleteParam : 삭제 대상 수금계획실적 순번
	 * @return
	 */
	public SBox deleteDebentureCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   삭제대상 수금계획실적 연기전 원본순번 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 03. 03.
	 * @version 1.0
	 * @param sBox
	 *            colSn : 수금계획실적 순번
	 * 
	 * @return 결제 조건 정보
	 */
	public SBox selectDebentureDlyColSn(SBox sBox) throws BizException {

		SBox returnBox;

		try {

			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("debenture.selectDebentureDlyColSn_SQL", sBox);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * <pre>
	 *   연기된 순번이 있으면 해당 수금계획 실적을 계획중으로 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 03. 03.
	 * @version 1.0
	 * @param sBox
	 *            colSn : 수금계획실적 순번, sessionUsrId : 회원 순번
	 * @return
	 */
	public SBox updateDebentureColStatForRemove(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureColStatForRemove_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/* Hong You Kyung AREA START */
	
	/**
	 * <pre>
	 * 기술료 계약서 정보 수정 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 24.
	 * @param contId : 계약서 순번, sessionUsrId : 수정자 회원 순번, contAmt : 계약금액, contDt : 계약일, rmkTxt : 비고
	 * @return
	 * @throws BizException
	 */
	public SBox updateContract(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateContract_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 계약서 수정 페이지에서의 기술료 정보 수정 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 24.
	 * @param debnId : 기술료 순번, sessionUsrId : 수정회원순번, colStat : 수금상태, plnColDt : 수금계획일, plnColAmt : 수금계획금액, colSum : 수금금액, colDt : 수금일
	 * @return
	 * @throws BizException
	 */
	public SBox updateDebentureForContractModify(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateDebentureForContractModify_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 기술료 계약서 증빙파일 정보 삭제 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 24.
	 * @param deleteContFileSn : 기술료 계약서 증빙파일 순번 리스트 (구분자 :,)
	 * @return
	 * @throws BizException
	 */
	public SBox deleteContractFile(String deleteContFileSn) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteContractFile_SQL", deleteContFileSn)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 기술료 계약서 삭제할 증빙파일 정보 리스트 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 24.
	 * @param deleteContFileSn : 기술료 계약서 증빙파일 순번 리스트 (구분자 :,)
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectDeleteContractFile(String deleteContFileSn) throws BizException {
		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectDeleteContractFile_SQL", deleteContFileSn));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * 기술료 계약서 상태 수정 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 25.
	 * @param contId : 계약서 순번, stat : 계약서 상태, updId : 수정회원순번
	 * @return
	 * @throws BizException
	 */
	public SBox updateContractStat(SBox sBox) throws BizException {
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.updateContractStat_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29904", EErrorCodeType.search("29904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 임시 기술료 수금 정보 대량등록 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 27.
	 * @param sessionCompUsrId : 기업회원순번, mltId : 대량등록순번, bondTmpId : 임시저장계약서순번, 
	 * 				pln_col_dt : 수금예정일, pln_col_amt : 수금예정금액, col_dt : 수금일, col_amt : 수금금액
	 * @return
	 * @throws BizException
	 */
	public SBox insertTempDebentureCollectMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.insertTempDebentureCollectMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29902", EErrorCodeType.search("29902").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 임시 기술료 계약서 중복 카운트 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 27.
	 * @param sessionCompUsrId : 기업회원순번, mltId : 대량등록순번, cont_cd : 계약관리번호
	 * @return
	 * @throws BizException
	 */
	public SBox selectTempDebentureMultiExist(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectTempDebentureMultiExist_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 임시 대량 등록된 기술료 수금 정보 삭제 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 2. 27.
	 * @param sessionCompUsrId : 기업회원순번, mltId : 대량등록순번
	 * @return
	 * @throws BizException
	 */
	public SBox deleteTempDebentureCollectMulti(int sessionCompUsrId, int mltId) throws BizException {
		SBox returnBox = null;
		
		try {
			SBox paramBox = new SBox();
			paramBox.set("sessionCompUsrId", sessionCompUsrId);
			paramBox.set("mltId", mltId);
			
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.deleteTempDebentureCollectMulti_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29903", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 대량등록 기술료계약서 중복 검증을 위한 기술료 계약정보 단건 조회 Dao Method
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 3. 5.
	 * @param sessionCompUsrId : 기업회원순번, contCd : 계약서관리번호
	 * @return
	 * @throws BizException
	 */
	public SBox selectContractForAddMulti(int sessionCompUsrId, String contCd) throws BizException {
		SBox resultBox = new SBox();
		
		try {
			SBox paramBox = new SBox();
			paramBox.set("sessionCompUsrId", sessionCompUsrId);
			paramBox.set("contCd", contCd);
			
			SBoxList<SBox> resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("debenture.selectContractForAddMulti_SQL", paramBox));
			if (resultList.size() > 0) {
				resultBox = resultList.get(0);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		
		return resultBox;
	}
	
	/* Hong You Kyung AREA End */
	
}
