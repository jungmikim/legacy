/**
 * 
 */
package com.spsb.dao.mypage;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 마이페이지 - 정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Repository
public class MyPageUserModifyDaoForMssql extends SuperDao{

	//기업회원 - 정보수정 데이터 읽기(관리자)
	public SBox selectCompUser(Integer compUsrId){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectCompanyUser_SQL", compUsrId);
		return result;
	}
	
	/**
	 * <pre>
	 * 채무불이행 진행상태요약
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 4. 28.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> selectSummaryList(SBox sBox) {

		Map<String,Object>  resultList = null;
		try {
			resultList =(Map<String,Object>) super.getSqlMapClientTemplate().queryForMap("mypage.selectSummaryListForMypage_SQL",  sBox, "STAT");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	
	
	/**
	 * <pre>
	 * 마이페이지 - 거래처 신용정보 변동현황
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 28.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> selectCustomerEWRatingStat(SBox sBox) {

		Map<String,Object>  resultList = null;
		try {
			resultList =(Map<String,Object>)  super.getSqlMapClientTemplate().queryForMap("mypage.selectCustomerEWRatingStat_SQL",  sBox, "EW_RATING" );
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	//개인회원 - 정보수정 데이타 읽기
	public SBox selectUser(Integer usrId){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectUser_SQL", usrId);
		return result;
	}
	
	//개인 기업 회원 - 정보수정 데이타 읽기(회사정보)
	public SBox selectMbr(Integer usrId){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectMbr_SQL", usrId);
		return result;
	}
	
	// 기업회원 회원 정보 수정(관리자)
	public SBox updateCompUser(SBox sBox){
		
		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateCompanyUser_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
		
	}
	
	//개인회원 정보 수정
	public SBox updateUser(SBox sBox){
		
		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateUser_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
		
	}
	
	//개인회원 정보 수정
	public SBox updateUserForPW(SBox sBox){
		
		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateUserForPW_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
		
	}
	
	//개인 소속기업 정보 수정
	public SBox updateMbr(SBox sBox){
		
		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateMbrByUser_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
		
	}
	
	// 기업회원 회원탈퇴 후 상태 값 수정 : 기업회원 테이블
	public SBox updateCompUsrSecession(SBox sBox)  {

		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateCompUsrSecession_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}
	
	// 기업회원멤버 회원탈퇴 후 상태 값 수정 : 기업회원멤버 테이블
	public SBox updateMbrUsrSecession(SBox sBox)  {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("mypage.updateMbrUsrSecession_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}
	
	
	// 개인회원탈퇴 후 상태 값 수정 : 기업회원 테이블
	public SBox updateUsrSecession(SBox sBox)  {
		SBox returnBox = null;
		try{
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateUsrSecession_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}
		
	// 개인회원멤버 회원탈퇴 후 상태 값 수정 : 기업회원멤버 테이블
	public SBox updateMbrSecession(SBox sBox)  {

		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateMbrSecession_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}	
	
	//직원관리 리스트의 권한 취소후 해지 취소 누를경우 REG_DT상태 변경(정렬을 위해)  
	public SBox updateMbrStatRevoke(SBox sBox)  {

		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateMbrStatRevoke_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}	
	
	
	//마이페이지 - 메인 (채권관리 현황)
	/*@SuppressWarnings("unchecked")
	public Map<String,Object>  selectDebentureListCountByMypage(SBox sBox){
		Map<String,Object>  returnBox = null;
		returnBox =(Map<String,Object>) super.getSqlMapClientTemplate().queryForMap("mypage.selectDebentureListCountByMypage", sBox, "DELAY_CD");
		return returnBox;
	}*/
	
	//마이페이지 소속기업에 속한 사용자 정보변경 수정(이름/전화번호/이메일) 
	public SBox updateBelongtoCompanyListInfo(SBox sBox)  {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("mypage.updateBelongtoCompanyListInfo_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}	
	
	//마이페이지 관리자 인증서 갱신(업데이트) 
	public SBox updateCertInfo(SBox sBox)  {
		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateCompUsrCertInfo_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}
	
	// 멤버테이블 관리자여부별 합계 가지고오기 Count
	public int  selectMbrListTotalCountForAdm(SBox sBox)throws BizException {
		int result = 0;
		
		try {
			result  = ((SBox) super.getSqlMapClientTemplate().queryForObject("mypage.selectMbrListTotalCountForAdm_SQL", sBox)).getInt("totalCnt");
			
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectMbrListTotalCountForAdm_SQL Dao ERROR");
		}
		return result;
	}
	
	

	
	/**
	 * <pre>
	 * 마이페이지 수금계획 및 실적의 당월 수금계획 과 당월 수금실적 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 4. 28.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> selectColListForMypage(SBox sBox) {

		Map<String,Object>  resultList = null;
		try {
			resultList =(Map<String,Object>) super.getSqlMapClientTemplate().queryForMap("mypage.selectColListForMypage_SQL",  sBox, "DIV");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	
	/**
	 * <pre>
	 *  스마트빌 기업정보 변경 인터페이스
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
		public Integer modifyCompanyUserForSB(SBox sBox)  {
			Integer result = 0;
			try {
				result = (Integer) super.getSqlMapClientTemplate().queryForObject("mypage.updateCompanyUserForSB_SQL", sBox);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		}
		
		
	/**
	 * <pre>
	 *  스마트빌  개인회원 정보(기업회원멤버, 개인회원) 변경 인터페이스
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
		public Integer modifyUserForSB(SBox sBox)  {
			Integer result = 0;
			try {
				result = (Integer) super.getSqlMapClientTemplate().queryForObject("mypage.updateUserForSB_SQL", sBox);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		}
		
	/**
	 * <pre>
	 *  스마트빌  기업관리자 변경 인터페이스
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
		public Integer modifyCompAdmInfoForSB(SBox sBox)  {
			Integer result = 0;
			try {
				result = (Integer) super.getSqlMapClientTemplate().queryForObject("mypage.updateCompAdmInbfoForSB_SQL", sBox);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		}
		

			

}
