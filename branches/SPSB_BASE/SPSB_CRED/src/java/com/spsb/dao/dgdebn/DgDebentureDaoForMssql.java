package com.spsb.dao.dgdebn;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 *   채권진단 Dao Class
 * </pre>
 * 
 * @author KIM GA EUN
 * @since 2014. 04. 25
 * @version 1.0
 */

@Repository
public class DgDebentureDaoForMssql extends SuperDao {
	
	/**
	 * <pre>
	 *   채권진단 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param usrId : 개인회원슌번
	 * @return resultList : 채권진단 리스트
	 */
	public SBoxList<SBox> selectDgDebentureList(int usrId) throws BizException {

		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureList_SQL", usrId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 최근 채권진단 정보 조회   Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return returnBox : 최근 채권진단 정보
	 */
	public SBoxList<SBox> selectGuestDgDebenture(int tmpUsrId){

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectGuestDgDebenture_SQL", tmpUsrId));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 *   Guest 회원  채권진단 리스트 조회   Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return resultList : 채권진단 리스트
	 */
	public SBoxList<SBox> selectDgDebentureListForTmpUsr(int tmpUsrId) throws BizException {

		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureListForTmpUsr_SQL", tmpUsrId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * 채권진단 등록 Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param sBox
	 * 			usrId : 개인회원순번, tmpUsrId : 임시회원순번, stat : 상태, usrType : 사용자유형, usrNo : 사업자등록번호, usrNm : 회사명, mbNo : 휴대폰번호, postNo : 직인정보, addr1 : 주소1
	 * 			, addr2 : 주소2, custType : 거래처유형, custNo : 거래처사업자등록번호, custNm : 거래처명, ownNm : 대표자명, custMbNo : 거래처휴대폰번호, stDtType : 일자유형, stDt : 작성일자, debnAmt : 매출채권금액
	 * 			, custPostNo : 거래처우편번호, custAddr1 : 거래처주소1, custAddr2 : 거래처주소2
	 * @return
	 */
	public Integer insertDgDebn(SBox sBox) {
		return (Integer) getSqlMapClientTemplate().queryForObject("dgDebenture.insertDgDebn_SQL", sBox);
	}
	
	/**
	 * <pre>
	 *   Guest 회원 채권 진단회수 변경   Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return
	 */
	public Integer updateTmpUsrDgCnt(Integer tmpUsrId) throws BizException {
		
		Integer returnBox = null;
	
		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("dgDebenture.updateTmpUsrDgCnt_SQL", tmpUsrId);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 정보 조회   Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return returnBox : Guest 회원 정보
	 */
	public SBox selectTmpUsr(int tmpUsrId) throws BizException {

		SBox returnBox;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectTmpUsr_SQL", tmpUsrId)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   Guest 회원 상태 변경   Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 04. 25
	 * @version 1.0
	 * @param tmpUsrId : 임시회원슌번
	 * @return
	 */
	public Integer updateTmpUsrStat(Integer tmpUsrId, String stat) throws BizException {
		
		SBox sBox = new SBox();
		Integer returnBox = null;
		sBox.set("tmpUsrId", tmpUsrId);
		sBox.set("stat", stat);
	
		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("dgDebenture.updateTmpUsrStat_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 *   채권 가이드 모아보기 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 06. 13.
	 * @version 1.0
	 * @param sBox
	 *             custType : 거래처유형, debnAmt : 매출채권금액, stDt : 작성일자
	 * 
	 * @return resultList : 채권 가이드 조회 결과
	 */
	public SBoxList<SBox> selectDebentureGuide(SBox sBox){
		
		SBoxList<SBox> resultList = null;
		
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureGuide_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 *   채권 가이드 모아보기 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 06. 13.
	 * @version 1.0
	 * @param sBox
	 *             custType : 거래처유형, debnAmt : 매출채권금액, custDayTerm : 경과일수
	 * 
	 * @return resultList : 채권 가이드 조회 결과
	 */
	public SBoxList<SBox> selectDebentureGuideAllView(SBox sBox){
		
		SBoxList<SBox> resultList = null;
		
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureGuideAllView_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 *   채권 진단 키워드 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014 06. 23.
	 * @version 1.0
	 * @param dgsRsltClsId
	 *            : 채권진단결과문구순번
	 * 
	 * @return resultList : 채권 진단 키워드 조회 결과
	 */
	public SBoxList<SBox> selectDgDebentureGuideKeyword(String dgsRsltClsId) throws BizException {
		
		SBox paramBox = new SBox();
		paramBox.set("dgsRsltClsId", dgsRsltClsId);

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureGuideKeyword_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29901").getErrMsg());
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 *   채권 진단 서브 키워드 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014 06. 23.
	 * @version 1.0
	 * @param dgsSubKwId
	 *            : 채권진단서브키워드순번
	 * 
	 * @return resultList : 채권 진단 서브키워드 의미 조회 결과
	 */
	public SBox selectDgDebentureGuideKeywordMean(String dgsSubKwId) throws BizException {
		
		SBox returnBox = null;
		SBox paramBox = new SBox();
		paramBox.set("dgsSubKwId", dgsSubKwId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("dgDebenture.selectDgDebentureGuideKeywordMean_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("29901", EErrorCodeType.search("29903").getErrMsg());
		}
		return returnBox;
	}
}
