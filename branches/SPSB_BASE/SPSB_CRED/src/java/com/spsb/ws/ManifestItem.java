
package com.spsb.ws;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ManifestItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManifestItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BinaryData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UniformResourceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManifestItem", propOrder = {
    "binaryData",
    "description",
    "sequenceNumber",
    "size",
    "uniformResourceID"
})
public class ManifestItem {

    @XmlElementRef(name = "BinaryData", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<DataHandler> binaryData;
    @XmlElementRef(name = "Description", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "SequenceNumber", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> sequenceNumber;
    @XmlElementRef(name = "Size", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> size;
    @XmlElementRef(name = "UniformResourceID", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> uniformResourceID;

    /**
     * Gets the value of the binaryData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataHandler }{@code >}
     *     
     */
    public JAXBElement<DataHandler> getBinaryData() {
        return binaryData;
    }

    /**
     * Sets the value of the binaryData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataHandler }{@code >}
     *     
     */
    public void setBinaryData(JAXBElement<DataHandler> value) {
        this.binaryData = ((JAXBElement<DataHandler> ) value);
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSequenceNumber(JAXBElement<String> value) {
        this.sequenceNumber = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSize(JAXBElement<String> value) {
        this.size = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the uniformResourceID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUniformResourceID() {
        return uniformResourceID;
    }

    /**
     * Sets the value of the uniformResourceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUniformResourceID(JAXBElement<String> value) {
        this.uniformResourceID = ((JAXBElement<String> ) value);
    }

}
