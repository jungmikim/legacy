package com.spsb.vo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 27.
 * @version 1.0
 */
public class JSONTest {
	private ArrayList<HashMap<String, String>> arryList ;

	/**
	 * @return the arryList
	 */
	public ArrayList<HashMap<String, String>> getArryList() {
		return arryList;
	}
	/**
	 * @param arryList the arryList to set
	 */
	public void setArryList(ArrayList<HashMap<String, String>> arryList) {
		this.arryList = arryList;
	}
	
}