package com.spsb.vo;

/**
 * <pre>
 *  요청 공통메시지
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 29.
 * @version 1.0
 */
public class responseCommonVo {
	
	private String ID;
	private String RequestDocumentTypeCode;
	private String VersionInformation;
	private String Description;
	private String UserBusinessAccountTypeCode;
	private String UserID;
	private String LoginID;
	private String LoginPasswordID;
	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}
	/**
	 * @return the requestDocumentTypeCode
	 */
	public String getRequestDocumentTypeCode() {
		return RequestDocumentTypeCode;
	}
	/**
	 * @param requestDocumentTypeCode the requestDocumentTypeCode to set
	 */
	public void setRequestDocumentTypeCode(String requestDocumentTypeCode) {
		RequestDocumentTypeCode = requestDocumentTypeCode;
	}
	/**
	 * @return the versionInformation
	 */
	public String getVersionInformation() {
		return VersionInformation;
	}
	/**
	 * @param versionInformation the versionInformation to set
	 */
	public void setVersionInformation(String versionInformation) {
		VersionInformation = versionInformation;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @return the userBusinessAccountTypeCode
	 */
	public String getUserBusinessAccountTypeCode() {
		return UserBusinessAccountTypeCode;
	}
	/**
	 * @param userBusinessAccountTypeCode the userBusinessAccountTypeCode to set
	 */
	public void setUserBusinessAccountTypeCode(String userBusinessAccountTypeCode) {
		UserBusinessAccountTypeCode = userBusinessAccountTypeCode;
	}
	/**
	 * @return the userID
	 */
	public String getUserID() {
		return UserID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(String userID) {
		UserID = userID;
	}
	/**
	 * @return the loginID
	 */
	public String getLoginID() {
		return LoginID;
	}
	/**
	 * @param loginID the loginID to set
	 */
	public void setLoginID(String loginID) {
		LoginID = loginID;
	}
	/**
	 * @return the loginPasswordID
	 */
	public String getLoginPasswordID() {
		return LoginPasswordID;
	}
	/**
	 * @param loginPasswordID the loginPasswordID to set
	 */
	public void setLoginPasswordID(String loginPasswordID) {
		LoginPasswordID = loginPasswordID;
	}

	
	
}
