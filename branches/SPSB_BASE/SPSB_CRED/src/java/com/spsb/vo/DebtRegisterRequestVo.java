package com.spsb.vo;

/**
 * 
 * <pre>
 *  채무불이행 등록 신청서 메시지 를 보낼 request Vo Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 12.
 * @version 1.0
 */
public class DebtRegisterRequestVo {

    private String compUsrId;
    private String regDt;
    private String regId;
    
    //자사정보내역
    private String typeCode;
    private String compNm;
    private String compNo;
    private String corpNo;
	private String bizType;
    private String ownName;
    private String userNm;
    private String deptNm;
    private String jobTlNm;
    private String telNo;
    private String mbNo;
    private String faxNo;
    private String email;
    private String postNo;
    private String adressOne;
    private String adressTwo;
    
    //거래처 정보 영역
    private String custType;
    private String custNm;
    private String custNo;
    private String custCorpNo;
    private String custOwnNm;

	private String custPostNo;
    private String custAddrOne;
    private String custAddrTwo;
    
    //연체정보
    private String debtType;
    private String debtCompType;
    private String overStDt;
    private int stDebtAmt;
    private int debtAmt;
   
    

    public String getCompUsrId() {
		return compUsrId;
	}
	public void setCompUsrId(String compUsrId) {
		this.compUsrId = compUsrId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getCompNm() {
		return compNm;
	}
	public void setCompNm(String compNm) {
		this.compNm = compNm;
	}
	public String getCompNo() {
		return compNo;
	}
	public void setCompNo(String compNo) {
		this.compNo = compNo;
	}
	public String getCorpNo() {
		return corpNo;
	}
	public void setCorpNo(String corpNo) {
		this.corpNo = corpNo;
	}
	public String getBizType() {
		return bizType;
	}
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	public String getOwnName() {
		return ownName;
	}
	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getDeptNm() {
		return deptNm;
	}
	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
	public String getJobTlNm() {
		return jobTlNm;
	}
	public void setJobTlNm(String jobTlNm) {
		this.jobTlNm = jobTlNm;
	}
	public String getTelNo() {
		return telNo;
	}
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	public String getMbNo() {
		return mbNo;
	}
	public void setMbNo(String mbNo) {
		this.mbNo = mbNo;
	}
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPostNo() {
		return postNo;
	}
	public void setPostNo(String postNo) {
		this.postNo = postNo;
	}
	public String getAdressOne() {
		return adressOne;
	}
	public void setAdressOne(String adressOne) {
		this.adressOne = adressOne;
	}
	public String getAdressTwo() {
		return adressTwo;
	}
	public void setAdressTwo(String adressTwo) {
		this.adressTwo = adressTwo;
	}
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getCustNm() {
		return custNm;
	}
	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustCorpNo() {
		return custCorpNo;
	}
	public void setCustCorpNo(String custCorpNo) {
		this.custCorpNo = custCorpNo;
	}
	public String getCustOwnNm() {
		return custOwnNm;
	}
	public void setCustOwnNm(String custOwnNm) {
		this.custOwnNm = custOwnNm;
	}
	public String getCustPostNo() {
		return custPostNo;
	}
	public void setCustPostNo(String custPostNo) {
		this.custPostNo = custPostNo;
	}
	public String getCustAddrOne() {
		return custAddrOne;
	}
	public void setCustAddrOne(String custAddrOne) {
		this.custAddrOne = custAddrOne;
	}
	public String getCustAddrTwo() {
		return custAddrTwo;
	}
	public void setCustAddrTwo(String custAddrTwo) {
		this.custAddrTwo = custAddrTwo;
	}
	public String getDebtType() {
		return debtType;
	}
	public void setDebtType(String debtType) {
		this.debtType = debtType;
	}
    public String getDebtCompType() {
		return debtCompType;
	}
	public void setDebtCompType(String debtCompType) {
		this.debtCompType = debtCompType;
	}
	public String getOverStDt() {
		return overStDt;
	}
	public void setOverStDt(String overStDt) {
		this.overStDt = overStDt;
	}
	public int getStDebtAmt() {
		return stDebtAmt;
	}
	public void setStDebtAmt(int stDebtAmt) {
		this.stDebtAmt = stDebtAmt;
	}
	public int getDebtAmt() {
		return debtAmt;
	}
	public void setDebtAmt(int debtAmt) {
		this.debtAmt = debtAmt;
	}
    
}
