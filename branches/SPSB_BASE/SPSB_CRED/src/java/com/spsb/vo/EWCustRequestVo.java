package com.spsb.vo;


/**
 * <pre>
 *  크레딧서비스의 조기경보 대상 거래처 사업자번호의 신규등록을 하기 위한 메시지
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 29.
 * @version 1.0
 */
public class EWCustRequestVo {

	private String id;
	private String folderName;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}
	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	
	
}
