package com.spsb.common.session;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EMemberUserGrantCode;
import com.spsb.common.enumtype.ESessionNameType;

/**
 * <pre>
 * Sesseion관리 class
 * </pre>
 * @author Wontae
 * @since 2013. 9. 12.
 * @version 1.0
 */
public class SessionManager {

	public static String DEBN_ATTRIBUTE_NAME = "sbDebnSessions";
	//public static String GUEST_DEBN_ATTRIBUTE_NAME = "sbDebnSessionsForGuest";
	private static String ATTRIBUTE_NAME = "sessionAttributeName";
	private static String LOGIN_NAME = "sessionLoginName";
	private static SessionManager sessionManager;
	
	private String sessionAttributeName;
	
	/**
	 * <pre>
	 * 세션 메니져 생성 or 기존 세션메니져 가져오는 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @return
	 */
	public static SessionManager getInstance(){
		if (sessionManager == null){
			sessionManager =  new SessionManager();
		}
		return sessionManager;
	}
	
	/**
	 * <pre>
	 * 세션메니져 초기화 (로그인)
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 * @param sBox : 로그인 정보
	 * @return
	 */
	public static SessionManager getInstance(HttpServletRequest request, SBox sBox, double sessionMaxInactiveTime){
		HttpSession session = request.getSession();
		if (sessionManager == null){
			//session.invalidate();
			sessionManager =  new SessionManager();
		}
		LinkedHashMap<String, SessionVo> sessionObjects = new LinkedHashMap<String, SessionVo> (); 
		session.setAttribute(DEBN_ATTRIBUTE_NAME, sessionObjects);
		session.setAttribute(ATTRIBUTE_NAME, DEBN_ATTRIBUTE_NAME);
		session.setMaxInactiveInterval((int) (60 * sessionMaxInactiveTime));
		SessionVo sessionVo = sessionManager.sBoxToSessionVo(sBox);
		sessionObjects.put(sessionVo.getSessionKey(), sessionVo);
		session.setAttribute(ESessionNameType.LOGIN_ID.getName(), sessionVo.getSessionLoginId());
		session.setAttribute(LOGIN_NAME, sessionVo.getSessionUsrNm());
		return sessionManager;
	}
	
	/**
	 * <pre>
	 * 세션메니져 초기화 (Guest 로그인)
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 18.
	 * @version 1.0
	 * @param request
	 * @param sBox : 로그인 정보
	 * @return
	 */
	/*public static SessionManager getInstanceForGuest(HttpServletRequest request, SBox sBox, double sessionMaxInactiveTime){
		HttpSession session = request.getSession();
		if (sessionManager == null){
			//session.invalidate();
			sessionManager =  new SessionManager();
		}
		LinkedHashMap<String, SessionVo> sessionObjects = new LinkedHashMap<String, SessionVo> (); 
		session.setAttribute(GUEST_DEBN_ATTRIBUTE_NAME, sessionObjects);
		session.setAttribute(ATTRIBUTE_NAME, GUEST_DEBN_ATTRIBUTE_NAME);
		session.setMaxInactiveInterval((int) (60 * sessionMaxInactiveTime));
		
		SessionVo sessionVo = sessionManager.sBoxToSessionVoForGuest(sBox);
		sessionObjects.put(sessionVo.getSessionKey(), sessionVo);
		session.setAttribute(LOGIN_NAME, sBox.get("LOGIN_ID"));
		return sessionManager;
	}*/
	
//	/**
//	 * <pre>
//	 * 현재세션의 attribute이름을 가져오는 메소드
//	 * </pre>
//	 * @author JUNG EUN LIM
//	 * @since 2014. 4. 18.
//	 * @version 1.0
//	 * @param request
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public String getCurrentSessionAttributeName(HttpServletRequest request){
//		HttpSession session = request.getSession();
//		Enumeration<String> sessionNames = session.getAttributeNames();
//		while (sessionNames.hasMoreElements()) {
//			String name = (String) sessionNames.nextElement();
//			return name;
//		}
//		return null;
//	}
	
	/**
	 * <pre>
	 * 세션에 저장된 세션오브젝트값들을 가져오는 메소드
	 * </pre>
	 * @author Wontae
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private LinkedHashMap<String, SessionVo> getSessionObjects(HttpSession session){
		String attributeName = (String) session.getAttribute(ATTRIBUTE_NAME);
		this.sessionAttributeName = attributeName;
		return (LinkedHashMap<String, SessionVo>)session.getAttribute(attributeName);
	}
	
	/**
	 * <pre>
	 * 로그인 정보를 sBox에서 sessionVo로 세팅하는 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @param sBox
	 */
	private SessionVo sBoxToSessionVo(SBox sBox){
		SessionVo sessionVo = new SessionVo();
		Integer usrId = sBox.getInt("USR_ID");
		String compType = sBox.getString("COMP_TYPE");
		String compMngCd = sBox.getString("COMP_MNG_CD");
		sessionVo.setCurrent(true);
		sessionVo.setSessionUsrId(usrId);
		sessionVo.setSessionCompMngCd(compMngCd);
		sessionVo.setSessionLoginId(sBox.getString("LOGIN_ID"));
		sessionVo.setSessionUsrNm(sBox.getString("USR_NM"));
		sessionVo.setSessionEmail(sBox.getString("EMAIL"));
		sessionVo.setSessionTelNo(sBox.getString("TEL_NO"));
		sessionVo.setSessionMbNo(sBox.getString("MB_NO"));
		sessionVo.setSessionPostNo(sBox.getString("POST_NO"));
		sessionVo.setSessionAddr1(sBox.getString("ADDR_1"));
		sessionVo.setSessionAddr2(sBox.getString("ADDR_2"));
		sessionVo.setSessionUsrStat(sBox.getString("USR_STAT"));
		sessionVo.setSessionCompUsrId(sBox.getInt("COMP_USR_ID"));
		sessionVo.setSessionCompType(compType);
		sessionVo.setSessionCompUsrNm(sBox.getString("COMP_USR_NM"));
		sessionVo.setSessionUsrType(sBox.getString("USR_TYPE"));
		sessionVo.setSessionUsrNo(sBox.getString("USR_NO"));
		sessionVo.setSessionCompUsrStat(sBox.getString("COMP_STAT"));
		
		sessionVo.setSessionMbrId(sBox.getInt("MBR_ID"));
		sessionVo.setSessionMbrUsrNm( sBox.getString("MBR_USR_NM"));
		sessionVo.setSessionMbrStat(sBox.getString("MBR_STAT"));
		String erpUsrId = sBox.getString("ERP_USR_ID");
		sessionVo.setSessionErpUsrId(erpUsrId);
		//sessionVo.setSessionPayTermType(sBox.getString("PAY_TERM_TYPE"));
		//sessionVo.setSessionPayTermDay(sBox.getInt("PAY_TERM_DAY"));
		String admYn = sBox.getString("ADM_YN");
		sessionVo.setSessionAdmYn(admYn);
		
		sessionVo.setSessionSbCompUsrId(sBox.getString("SB_COMP_USR_ID"));
		sessionVo.setSessionSbLoginId(sBox.getString("SB_LOGIN_ID"));
		sessionVo.setSessionSbusrId(sBox.getString("SB_USR_ID"));
		
		String debtGrnt = sBox.getString("CRED1C");
		if("Y".equals(debtGrnt)){
			debtGrnt="CRED1C";
		}
		String custSearchGrnt = sBox.getString("SBDE1R");
		if("Y".equals(custSearchGrnt)){
			custSearchGrnt=EMemberUserGrantCode.CUST_SEARCH_GRN.getCode();
		}
		String custBrifGrnt = sBox.getString("SBDE2R");
		if("Y".equals(custBrifGrnt)){
			custBrifGrnt=EMemberUserGrantCode.CUST_BRIF_GRN.getCode();
		}
		String custEwDetail = sBox.getString("SBDE3R");
		if("Y".equals(custEwDetail)){
			custEwDetail=EMemberUserGrantCode.CUST_EW_DETAIL_GRN.getCode();
		}
		String custEwAdd = sBox.getString("SBDE4R");
		if("Y".equals(custEwAdd)){
			custEwAdd=EMemberUserGrantCode.CUST_EW_ADD_GRN.getCode();
		}
		
		if("Y".equals(admYn)){
			sessionVo.setSessionDebtGrnt(true);
			sessionVo.setSessionCustSearchGrnt(true);
			sessionVo.setSessionCustBrifGrnt(true);
			sessionVo.setSessionCustEwDetailGrnt(true);
			sessionVo.setSessionCustEwAddGrnt(true);
		}else{
			//개인회원 권한코드 설정
			if(EMemberUserGrantCode.DEBN_GRN.getCode().equals(debtGrnt)){
				sessionVo.setSessionDebtGrnt(true);
			}
			
			if(EMemberUserGrantCode.CUST_SEARCH_GRN.getCode().equals(custSearchGrnt)){
				sessionVo.setSessionCustSearchGrnt(true);
			}
			
			if(EMemberUserGrantCode.CUST_BRIF_GRN.getCode().equals(custBrifGrnt)){
				sessionVo.setSessionCustBrifGrnt(false);
			}
			
			if(EMemberUserGrantCode.CUST_EW_ADD_GRN.getCode().equals(custEwAdd)){
				sessionVo.setSessionCustEwAddGrnt(true);
			}
			
			if(EMemberUserGrantCode.CUST_EW_DETAIL_GRN.getCode().equals(custEwDetail)){
				sessionVo.setSessionCustEwDetailGrnt(true);
			}
			
		}
		String key = compType + "_" + usrId;
		sessionVo.setSessionKey(key);
		return sessionVo;
	}
	
	/**
	 * <pre>
	 * Guest 로그인 정보를 sBox에서 sessionVo로 세팅하는 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 18.
	 * @version 1.0
	 * @param sBox
	 */
	private SessionVo sBoxToSessionVoForGuest(SBox sBox){
		SessionVo sessionVo = new SessionVo();
		sessionVo.setCurrent(true);
		Integer tmpUsrId = sBox.getInt("TMP_USR_ID");
		sessionVo.setSessionUsrId(tmpUsrId);
		sessionVo.setSessionLoginId(sBox.getString("LOGIN_ID"));
		sessionVo.setSessionStLoginDt((Date) sBox.get("ST_LOGIN_DT"));
		String key = "GU_" + tmpUsrId;
		sessionVo.setSessionKey(key);
		return sessionVo;
	}
	
	/**
	 * <pre>
	 * sessionVo를 sBox에 세팅
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @param sessionVo
	 * @param sBox
	 * @return
	 */
	public SBox sessionVoToSBox(SessionVo sessionVo, SBox sBox){
		sBox.set(ESessionNameType.COMP_USR_ID.getName(), sessionVo.getSessionCompUsrId());
		sBox.set(ESessionNameType.MBR_ID.getName(), sessionVo.getSessionMbrId());
		sBox.set(ESessionNameType.COMP_MNG_CD.getName(), sessionVo.getSessionCompMngCd());
		sBox.set(ESessionNameType.USR_ID.getName(), sessionVo.getSessionUsrId());
		sBox.set(ESessionNameType.LOGIN_ID.getName(), sessionVo.getSessionLoginId());
		sBox.set(ESessionNameType.COMP_USR_NM.getName(), sessionVo.getSessionCompUsrNm());
		sBox.set(ESessionNameType.USR_NO.getName(), sessionVo.getSessionUsrNo());
		sBox.set(ESessionNameType.USR_NM.getName(), sessionVo.getSessionUsrNm());
		sBox.set(ESessionNameType.MBR_USR_NM.getName(), sessionVo.getSessionMbrUsrNm());
		sBox.set(ESessionNameType.ADM_YN.getName(), sessionVo.getSessionAdmYn());
		sBox.set(ESessionNameType.COMP_TYPE.getName(), sessionVo.getSessionCompType());
		//sBox.set(ESessionNameType.GRN_CD.getName(), sessionVo.getSessionGrnCd());
		
		sBox.set(ESessionNameType.COMP_USR_STAT.getName(), sessionVo.getSessionCompUsrStat());
		sBox.set(ESessionNameType.USR_STAT.getName(), sessionVo.getSessionUsrStat());
		sBox.set(ESessionNameType.MBR_STAT.getName(), sessionVo.getSessionMbrStat());
		sBox.set(ESessionNameType.EMAIL.getName(), sessionVo.getSessionEmail());
		sBox.set(ESessionNameType.TEL_NO.getName(), sessionVo.getSessionTelNo());
		sBox.set(ESessionNameType.MB_NO.getName(), sessionVo.getSessionMbNo());
		sBox.set(ESessionNameType.POST_NO.getName(), sessionVo.getSessionPostNo());
		sBox.set(ESessionNameType.ADDR1.getName(), sessionVo.getSessionAddr1());
		sBox.set(ESessionNameType.ADDR2.getName(), sessionVo.getSessionAddr2());
		sBox.set(ESessionNameType.USR_TYPE.getName(), sessionVo.getSessionUsrType());
		sBox.set(ESessionNameType.ERP_USR_ID.getName(), sessionVo.getSessionErpUsrId());
/*		sBox.set(ESessionNameType.PAY_TERM_TYPE.getName(), sessionVo.getSessionPayTermType());
		sBox.set(ESessionNameType.PAY_TERM_DAY.getName(), sessionVo.getSessionPayTermDay());*/
		sBox.set(ESessionNameType.IS_DEBT_GRN.getName(), sessionVo.isDebtGrnt());
		sBox.set(ESessionNameType.IS_CUST_SEARCH_GRN.getName(), sessionVo.isSessionCustSearchGrnt());
		sBox.set(ESessionNameType.IS_CUST_BRIF_GRN.getName(), sessionVo.isSessionCustBrifGrnt());
		sBox.set(ESessionNameType.IS_CUST_EW_DETAIL_GRN.getName(), sessionVo.isSessionCustEwDetailGrnt());
		sBox.set(ESessionNameType.IS_CUST_EW_ADD_GRN.getName(), sessionVo.isSessionCustEwAddGrnt());
	
		sBox.set(ESessionNameType.SB_COMP_USR_ID.getName(), sessionVo.getSessionSbCompUsrId());
		sBox.set(ESessionNameType.SB_LOGIN_ID.getName(), sessionVo.getSessionSbLoginId());
		sBox.set(ESessionNameType.SB_USR_ID.getName(), sessionVo.getSessionSbusrId());
		return sBox;
	}
	
	/*public SBox sessionVoToSBoxForGuest(SessionVo sessionVo, SBox sBox){
		sBox.set(ESessionNameType.USR_ID.getName(), sessionVo.getSessionUsrId());
		sBox.set(ESessionNameType.LOGIN_ID.getName(), sessionVo.getSessionLoginId());
		sBox.set(ESessionNameType.ST_LOGIN_DT.getName(), sessionVo.getSessionStLoginDt());
		return sBox;
	}*/
	
	/**
	 * <pre>
	 * 현재 SessionVo를 가져오는 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 13.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	public SessionVo getCurrentSessionVo(HttpServletRequest request){
		HttpSession session = request.getSession();
		LinkedHashMap<String, SessionVo> sessionObjects = getSessionObjects(session);
		if(sessionObjects != null){
			for (Iterator<SessionVo> i = sessionObjects.values().iterator(); i.hasNext();) {
				SessionVo sessionVo = (SessionVo) i.next();
				if(sessionVo.isCurrent()){
					return sessionVo;
				}
			}
		}
		return null;
	}
	
	/**
	 * <pre>
	 * 현재 로그인인지 여부
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	public boolean isLogined (HttpServletRequest request){
		boolean isLogined = false;
		SessionVo sessionVo = getCurrentSessionVo(request);
		if(sessionVo != null){
			isLogined = true;
		}
		return isLogined;
	}
	
	/**
	 * <pre>
	 * 현재 세션 설정 메소드(세션에 key가 있을 때 사용)
	 * </pre>
	 * @author Wontae
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 * @param key : 세션에 저장된 key
	 * @throws RuntimeException
	 */
	public void setCurrentSession(HttpServletRequest request, String key) throws RuntimeException{
		getCurrentSessionVo(request).setCurrent(false);
		HttpSession session = request.getSession();
		LinkedHashMap<String, SessionVo> sessionObjects = getSessionObjects(session);
		SessionVo sessionVo = sessionObjects.get(key);
		if(sessionVo == null){
			throw new RuntimeException("key is not exist.");
		}
		sessionVo.setCurrent(true);
	}
	
	/**
	 * <pre>
	 * 현재 세션 설정 메소드(세션에 key가 없을때나 갱신할 때 사용)
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 * @param sBox : 로그인 정보
	 */
	public SessionVo setCurrentSession(HttpServletRequest request, SBox sBox){
		//getCurrentSessionVo(request).setCurrent(false);
		HttpSession session = request.getSession();
		LinkedHashMap<String, SessionVo> sessionObjects = getSessionObjects(session);
		SessionVo sessionVo = sessionManager.sBoxToSessionVo(sBox);
		sessionObjects.put(sessionVo.getSessionKey(), sessionVo);
		session.setAttribute(LOGIN_NAME, sessionVo.getSessionUsrNm());
		return sessionVo;
	}
	
	/**
	 * <pre>
	 * 해당key의 세션이 존재하는지 여부 메소드
	 * </pre>
	 * @author Wontae
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 * @param key : 세션에 저장된 key
	 * @return
	 */
	public boolean isExist(HttpServletRequest request, String key){
		HttpSession session = request.getSession();
		LinkedHashMap<String, SessionVo> sessionObjects = getSessionObjects(session);
		SessionVo sessionObject = sessionObjects.get(key);
		return sessionObject == null ? false:true;
	}
	
	/**
	 * <pre>
	 * 로그아웃
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @param request
	 */
	public void logout(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.invalidate();
	}

	public String getSessionAttributeName() {
		return sessionAttributeName;
	}

	
	/**
	 * <pre>
	 * 현재 로그인한 사용자의 세션정보 생성
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 23.
	 * @version 1.0
	 * @return
	 */
	public SBox getCurrentSession(){
		SBox sBox = new SBox();
		RequestAttributes attribs = RequestContextHolder.getRequestAttributes();
		
		if (attribs instanceof NativeWebRequest) {
	        HttpServletRequest request = (HttpServletRequest) ((NativeWebRequest) attribs).getNativeRequest();
	        //return request.getRemoteAddress();
	        if (sessionManager == null){
				sessionManager =  new SessionManager();
			}
			SessionVo sessionVo = sessionManager.getCurrentSessionVo(request);
			sBox = sessionManager.sessionVoToSBox(sessionVo, sBox);
	    }
		
		return sBox;
	}
}
