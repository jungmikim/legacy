package com.spsb.common.enumtype;

import java.util.Map;
import java.util.TreeMap;
/**
 * <pre>
 * EW 등급 타입  enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 7. 22.
 * @version 1.0
 */
public enum EEwRatingType {

	EW0("00", "정상"),
	EW1("01", "관심"),
	EW2("02", "관찰1"),
	EW3("03", "관찰2"),
	EW4("04", "관찰3"),
	EW5("05", "휴업"),
	EW6("06", "부도"),
	EW7("07", "폐업");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 EW등급타입생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 */
	private EEwRatingType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는  EW등급타입 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @param code : 코드
	 * @return EEwRatingType : EW등급타입 enum
	 */
	public static EEwRatingType search(String code){
		for(EEwRatingType type : EEwRatingType.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
	
	
	/**
	 * <pre>
	 * 코드와 상태type으로된 Map Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public static Map<String, EEwRatingType> getEwStatMap(){
		TreeMap<String, EEwRatingType> ewStatMap = new TreeMap<String, EEwRatingType>();
		for(EEwRatingType type : EEwRatingType.values()){
			ewStatMap.put(type.getCode(), type);
		}
		return ewStatMap;
	}
	
}
