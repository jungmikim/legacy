package com.spsb.common.soap;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.spsb.vo.DebtStatRequestVo;

/**
 * <pre>
 * Soap 메시지로 HTTP request Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 7. 17.
 * @version 1.0
 */
public class SoapHttpClient {

//	@Value("#{common['SOAP_DEBT_URL'].trim()}") 
//	public static String soapDebtUrl;
	
	/**
	 * <pre>
	 * 채무불이행 상태 Soap 메시지로 HTTP request Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 17.
	 * @version 1.0
	 * @param url
	 * @param requestVo
	 * @return 
	 */
	public static SoapResultVo debtStatRequest(String url, String xml){
		int responseCode = 0;
		HttpURLConnection con = null;
		OutputStream reqStream = null;
		SoapResultVo resultVo = new SoapResultVo();
		
		try {
			//StringBuffer soapStr = new StringBuffer();
			/*soapStr.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			soapStr.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:deb=\"http://bizon.co.kr/debtService\" >");
			soapStr.append("<soapenv:Header/>");
			soapStr.append("<soapenv:Body>");
			soapStr.append("<deb:CreditNoticeUpdateCustomerLevel>");
			soapStr.append("<deb:userNo>" + requestVo.getUserNo() + "</deb:userNo>");
			soapStr.append("<deb:userNm>" + requestVo.getUserNm() + "</deb:userNm>");
			soapStr.append("<deb:startDate>" + requestVo.getStartData() + "</deb:startDate>");
			soapStr.append("<deb:endDate>" + requestVo.getEndData() + "</deb:endDate>");
			soapStr.append("</deb:CreditNoticeUpdateCustomerLevel>");
			soapStr.append("</soapenv:Body>");
			soapStr.append("</soapenv:Envelope>");*/
			
			String soapXml = xml;
//			System.out.println(soapXml);
			
			URL oURL = new URL(url);
			con = (HttpURLConnection) oURL.openConnection();
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
			con.setRequestProperty("SOAPAction", "http://bizon.co.kr/debtService/OutBoundService/CreditNoticeUpdateCustomerLevel");
			con.setRequestProperty("Content-Length", String.valueOf(soapXml.length()));
			con.setRequestProperty("User-Agent", "Mozilla/4.0(compatible; MSIE5.5; Windows NT 5.0)");
			con.connect();
			
			reqStream = con.getOutputStream();
			reqStream.write(soapXml.getBytes("UTF-8"));
			reqStream.flush();
			if(reqStream != null) reqStream.close();
			
			responseCode = con.getResponseCode();
			resultVo.setConnectionResult(responseCode);
			
			if(responseCode == HttpURLConnection.HTTP_OK){
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				StringBuffer buffer = new StringBuffer();
				String line = null;
				while ((line = reader.readLine()) != null) {
					buffer.append(line).append("\r\n");
				}
				reader.close();
//				System.out.println("buffer: " + buffer.toString());
				
				DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFac.newDocumentBuilder();
				InputSource insource = new InputSource();
				insource.setCharacterStream(new StringReader(buffer.toString()));
				Document doc = docBuilder.parse(insource);
				NodeList nodes = doc.getElementsByTagName("CreditNoticeUpdateCustomerLevelResult");
				/*String soapResult = nodes.item(0).getFirstChild().getTextContent();
				String[] soapResultArray = soapResult.split("@");
				if(soapResultArray.length >= 2){
					resultVo.setResultCode(soapResultArray[0]);
					resultVo.setResultMessage(soapResultArray[1]);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(con != null){
				con.disconnect();
			}
		}
		return resultVo;
	}
}
