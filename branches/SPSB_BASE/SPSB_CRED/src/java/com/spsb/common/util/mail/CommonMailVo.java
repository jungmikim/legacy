package com.spsb.common.util.mail;

/**
 * <pre>
 * mail Vo class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 1. 28.
 * @version 1.0
 */
public class CommonMailVo {

	private String to;	//수신자 Email
	private String from;	//발신자 Email
	private String fromName;	//발신자명
	private String subject;	//제목
	private String content;	//내용
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
