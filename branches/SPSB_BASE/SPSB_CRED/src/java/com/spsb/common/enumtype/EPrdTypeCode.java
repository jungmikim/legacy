package com.spsb.common.enumtype;


/**
 * <pre>
 * 상품 유형 코드 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 10. 1.
 * @version 1.0
 */
public enum EPrdTypeCode {

	COMP_SEARCH("C", "기업정보서비스"),
	DEBT_CHAG("D", "진행수수료"),
	EW("W", "조기경보서비스");
	
	private String code;	//상품유형코드
	private String desc;	//설명
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 10. 1.
	 * @version 1.0
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 10. 1.
	 * @version 1.0
	 * @return
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 상품 코드 생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 24.
	 * @version 1.0
	 * @param code
	 * @param desc
	 */
	private EPrdTypeCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는  상품 코드 조회 Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 10. 1.
	 * @version 1.0
	 * @return code : 코드
	 * @return EPrdTypeCode : 상품 유형 코드 enum
	 */
	public static EPrdTypeCode search(String code){
		for(EPrdTypeCode type : EPrdTypeCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
