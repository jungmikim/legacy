package com.spsb.common.util;

import java.io.StringReader;
import java.io.StringWriter;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class XMLUtil {
	 private static String header = "<?xml version=\"1.0\" encoding=\"EUC-KR\"?>\n\n";

	  private static OutputFormat format = new OutputFormat("    ", true, "EUC-KR");

	  public static Document toDocument(String str) throws Exception {
	    return new SAXReader().read(new StringReader(str));
	  }

	  public static Document toDocument(DocumentFactory factory, String str) throws Exception {
	    return new SAXReader(factory).read(new StringReader(str));
	  }

	  public static Element toElement(String str) throws Exception {
	    Document doc = toDocument(str);
	    return doc.getRootElement();
	  }

	  public static Element toElement(DocumentFactory factory, String str) throws Exception {
	    Document doc = toDocument(factory, str);
	    return doc.getRootElement();
	  }

	  public static String toString(Document doc) throws Exception {
	    StringWriter writer = new StringWriter();
	    new XMLWriter(writer, format).write(doc);
	    return writer.toString();
	  }

	  public static String toString(Document doc, String encoding) throws Exception {
	    StringWriter writer = new StringWriter();
	    new XMLWriter(writer, new OutputFormat("    ", true, encoding)).write(doc);
	    return writer.toString();
	  }

	  public static String toString(Element elem) throws Exception {
	    StringWriter writer = new StringWriter();
	    new XMLWriter(writer, format).write(elem);
	    return header + writer.toString();
	  }
}
