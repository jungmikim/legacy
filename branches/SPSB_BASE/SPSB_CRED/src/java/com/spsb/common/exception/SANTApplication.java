package com.spsb.common.exception;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 25.
 * @version 1.0
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SANTApplication
{
  public static final String MSG_STATUS_RECEIVED = "R";
  public static final String MSG_STATUS_PROCESSING = "P";
  public static final String MSG_STATUS_SEND = "S";
  public static final String MSG_STATUS_SENT = "T";
  public static final String MSG_STATUS_COMPLETE = "C";
  public static final String MSG_STATUS_ERROR = "E";
  public static final String MSG_STATUS_FAILED = "F";
  public static final String DOMAIN_ADAPTER = "ADT";
  public static final String DOMAIN_CONNECTOR = "CON";
  public static final String DOMAIN_SYSTEM = "SYS";
  public static final String DOMAIN_SERVICE = "UTL";
  public static final char SEVERITY_WARN = 'W';
  public static final char SEVERITY_ERROR = 'E';
  public static final String MSG_TYPE_REQUEST = "REQ";
  public static final String MSG_TYPE_RESPONSE = "RES";
  public static final String RESPONSE_PATTERN_TYPE_S = "S";
  public static final String RESPONSE_PATTERN_TYPE_A = "A";
  public static final String RESPONSE_PATTERN_TYPE_P = "P";
  public static final String RESULT_SUCC = "SCMN01";
  public static final String RESULT_FAIL = "FSYSP1";
  public static final String DIRECTION_INBOUND = "I";
  public static final String DIRECTION_OUTBOUND = "O";
  protected Logger logger = LoggerFactory.getLogger(getClass());

  public abstract String getDomain();

  public abstract String getActivity();


  protected String getClassName()
  {
    String fullname = getClass().getName();
    int index = fullname.lastIndexOf(".");
    return fullname.substring(index + 1);
  }

  protected void handleException(String messageTagId, SANTException se)
  {
    try
    {
      this.logger.info("insert ErrorLog");
      //getDaoHelper().insertErrorLog(messageTagId, se, getDomain(), getActivity());
    } catch (Exception e) {
      this.logger.error("Critical Error", e);
    }
  }

  public boolean isSuccessCode(String code)
  {
    if (code == null) {
      return false;
    }

    return (code.equals("SNTH00")) || (code.equals("SCMN01")) || (code.equals("SCMN02"));
  }

}