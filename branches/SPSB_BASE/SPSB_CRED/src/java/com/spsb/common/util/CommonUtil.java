package com.spsb.common.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

import javax.activation.DataHandler;

import org.springframework.web.multipart.MultipartFile;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * <pre>
 * System 공통 유틸 Class
 * </pre>
 * 
 * @author SungRangKong
 * @version v1.0
 * @since 2013.08.15
 * 
 */
public class CommonUtil {

	/**
	 * <pre>
	 * 현재 일자를 반환한다.(yyyy + flag + MM + flag + dd)
	 * </pre>
	 * 
	 * @author SungRangKong
	 * @since 2013.08.15
	 * 
	 * @return 현재일자(yyyy + flag + MM + flag + dd)
	 */
	public static String getDate(String flag) {

		String dateFormat = "yyyy" + flag + "MM" + flag + "dd";
		return new SimpleDateFormat(dateFormat).format(new Date());
	}

	/**
	 * <pre>
	 * 현재 일자를 반환한다.(yyyyMMdd)
	 * </pre>
	 * 
	 * @author SungRangKong
	 * @since 2013.08.15
	 * 
	 * @return 현재일자(yyyyMMdd)
	 */
	public static String getDate() {
		return new SimpleDateFormat("yyyyMMdd").format(new Date());
	}

	/**
	 * <pre>
	 * 현재 시각을 밀리세컨드 단위로 반환한다.(yyyyMMddHHmmssSSS)
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 24.
	 * @param
	 * @return
	 */
	public static String getTimeMiliSecond() {
		return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
	}

	/**
	 * <pre>
	 * 현재 시각을  반환한다.(yyyyMMddHHmmss)
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 23.
	 * @version 1.0
	 * @return
	 */
	public static String getTime() {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	}

	/**
	 * <pre>
	 * Date타입의 날짜를 yyyyMMdd 단위로 반환한다.
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param date
	 * @return
	 */
	public static String getFormatDateTimeMiliSecond(Date date){
		return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
	}
	
	/**
	 * <pre>
	 * Date타입의 날짜를 yyyyMMddhhmm:ss 단위로 반환한다.
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 9. 24.
	 * @version 1.0
	 * @param date
	 * @return
	 */
	public static String getFormatDate(Date date){
		return new SimpleDateFormat("yyyyMMdd").format(date);
	}
	
	/**
	 * <pre>
	 * 두 날짜간의 차이를 (일단위)Day 로 반환한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 26.
	 * @param stDate
	 *            : 시작일자(yyyy-MM-dd) , stDate : 종료일자(yyyy-MM-dd)
	 * @return 종료일자 - 시작일자
	 */
	public static long getDateDiff(String stDate, String edDate) {
		long returnDay = 0;
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date stObj = sf.parse(stDate);
			Date edObj = sf.parse(edDate);
			long diff = edObj.getTime() - stObj.getTime();
			returnDay = diff / (24 * 3600 * 1000);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnDay;
	}

	/**
	 * <pre>
	 * UTF-8 인코딩
	 * </pre>
	 * 
	 * @author SungRangKong
	 * @since 2013.08.15
	 * 
	 * @param msg
	 *            입력 메시지
	 * @return UTF-8 인코딩 후의 값
	 * @throws UnsupportedEncodingException
	 */
	public static String UTF8encode(String msg) throws UnsupportedEncodingException {
		return URLEncoder.encode(msg, "UTF-8").replaceAll("\\+", "\\ ");
	}

	/**
	 * <pre>
	 * UTF-8 디코딩
	 * </pre>
	 * 
	 * @author SungRangKong
	 * @since 2013.08.15
	 * 
	 * @param msg
	 *            입력 메시지
	 * 
	 * @return UTF-8 디코딩 후의 값
	 * @throws UnsupportedEncodingException
	 */
	public static String UTF8decode(String msg) throws UnsupportedEncodingException {
		return URLDecoder.decode(msg, "UTF-8");
	}

	/**
	 * <pre>
	 * 영문 숫자 포함 난수 발생후 유일한 Token값 생성 메소드
	 * </pre>
	 * 
	 * @author SungRangKong
	 * @since 2013.08.15
	 * 
	 * @param size
	 *            발생시킬 난수의 개수
	 * @return 입력받은 개수만큼 난수를 통해 생성된 숫자와 영문의 조합값
	 */
	public static String getRandomString(int size) {
		Random rand = new Random();
		String token = "";

		int i = 50;
		while (i-- > 0) {
			char ch = (char) (rand.nextInt(26) + 97);
			int in = rand.nextInt(9);
			token += Character.toString(ch) + in;
		}
		return token.substring(0, size);
	}

	/**
	 * <pre>
	 * Email 유효성 체크 xxxx.xxxx@xxxx.com 의 경우 유효성 검증함.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatEmail(String data) {
		return (data.trim().matches("[(\\w+\\.)-]*\\w+\\@[(\\w+\\.)-]+\\w+")) ? true : false;
	}

	/**
	 * <pre>
	 * 주민등록 번호 유효성 체크 xxxxxx-xxxxxxx 의 형태임.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatRegNum(String data) {

		data = data.trim().replaceAll("\\-", "");

		// [1] 주민번호의 개수 비교
		if (data.length() != 13) {
			return false;
		}

		// [2] 주민번호 가중치 계산
		int regNumWeight[] = { 2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5 }; // 주민등록 번호 가중치
		int sum = 0;
		for (int i = 0; i < 12; i++) {
			sum += (Character.getNumericValue(data.charAt(i)) * regNumWeight[i]);
		}

		// [3] 주민번호 최종 검증
		int result = (sum % 11);
		if (result == 0) {
			result = 1;
		} else if (result == 1) {
			result = 0;
		} else {
			result = 11 - result;
		}

		if (result == Character.getNumericValue(data.charAt(12))) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * <pre>
	 * 사업자 번호 유효성 체크 xxx-xx-xxxxx 의 형태임
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증할데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatBusinessNo(String data) {
		data = data.trim().replaceAll("\\-", "");

		// [1] 사업자 등록번호 문자열 개수 비교
		if (data.length() != 10) {
			return false;
		}

		// [2] 가중치 계산
		int sum = 0;
		int check[] = { 1, 3, 7, 1, 3, 7, 1, 3, 5 }; // 사업자번호 유효성 체크 필요한 수
		for (int i = 0; i < 9; i++) {
			sum = sum + (Character.getNumericValue(data.charAt(i)) * check[i]); // 검증식 적용
		}
		sum += (Character.getNumericValue(data.charAt(8)) * 5) / 10;

		// [3] 가중치에 의한 return 값 계산
		if ((10 - (sum % 10)) % 10 == Character.getNumericValue(data.charAt(9))) // 마지막 유효숫자와 검증식을 통한 값의 비교
			return true;
		else
			return false;
	}

	/**
	 * <pre>
	 * 법인 등록 번호 유효성 체크 xxx-xx-xxxxx 의 형태임
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증할데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatCorpNo(String data) {

		data = data.trim().replaceAll("\\-", "");

		// [1] 법인등록번호 문자열 개수 비교
		if (data.length() != 13) {
			return false;
		}

		// [2] 가중치 계산
		int weightNum[] = { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 };
		int sum = 0;
		for (int i = 0; i < weightNum.length; i++) {
			sum += (Character.getNumericValue(data.charAt(i)) * weightNum[i]);
		}

		// [3] 가중치에 의한 return 값 계산
		int result = (10 - (sum % 10));
		if (result == Character.getNumericValue(data.charAt(12))) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * <pre>
	 * 날짜 형식 xxxx-xx-xx 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatDate(String data) {
		return (data.trim().matches("(\\d{4})((0[1-9])|(1[0-2]))((0[1-9]|[1-2][0-9]|3[0-1]))")) ? true : false;
	}

	/**
	 * <pre>
	 * 일자 형식 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatDay(String data) {
		return (data.trim().matches("(([1-9])|(0[1-9])|([1-2][0-9])|([3][0-1]))")) ? true : false;
	}

	/**
	 * <pre>
	 * 우편번호 형식 xxx-xxx 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 1.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatZipcode(String data) {
		return (data.replaceAll("\\-", "").trim().matches("\\d{6}")) ? true : false;
	}

	/**
	 * <pre>
	 * 종사업장 번호 형식 xxxx 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatBusicode(String data) {
		return (data.matches("\\d{4}")) ? true : false;
	}

	/**
	 * <pre>
	 * 전화번호 형식 xxxxxx-xxx 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 08.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatTelCode(String data) {
		return (data.trim().matches("(02|031|032|033|041|042|043|051|052|053|054|055|062|061|063|064|070)-(\\d{3,4})-(\\d{4})")) ? true : false;
	}

	/**
	 * <pre>
	 * 핸드폰 번호 형식 xxx-xxxx-xxxx 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 11.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatPhoneCode(String data) {
		return (data.trim().matches("(010|011|016|017|018|019)-(\\d{3,4})-(\\d{4})")) ? true : false;
	}

	/**
	 * <pre>
	 * 아이디에 공백이 있거나  영문 대소문자, 숫자인지 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 6.
	 * @version 1.0
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatLoginId(String data) {
		String numEng = "[0-9]|[a-z]|[A-Z]";
		boolean ckeckResult = true;
		for (int i = 0; i < data.length(); i++) {
			String chr = data.substring(i, i + 1);
			if (chr.equals(" ")) {
				ckeckResult = false;
				break;
			}
			if (!chr.matches(numEng)) {
				ckeckResult = false;
				break;
			}
		}
		return ckeckResult;
	}
	
	/**
	 * <pre>
	 * 금액 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 11.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatMoney(String data) {
		return (data.replaceAll("\\,", "").trim().matches("^\\d+$")) ? true : false;
	}
	
	/**
	 * <pre>
	 * 숫자체크 검증
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 12. 24.
	 * @param data
	 *            : 검증할 데이터
	 * 
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkVaildNumeral(String data) {
		char ch;
		for (int i = 0 ; i < data.length(); i++) { 
			ch = data.charAt(i) ; 
			if (!Character.isDigit(ch)) {
				return false;
			}
		 }
		return true;
	}
	
	/**
	 * <pre>
	 * 특정길이의 숫자 포맷 유효성 검증
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 15.
	 * @param data
	 *            : 검증할 데이터, length: 검증할 길이 값
	 * 
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatInt(String data, int length) {
		if (length < 0) {
			return false;
		}
		return (data.replaceAll("\\,", "").trim().matches("^\\d{" + length + "}+$")) ? true : false;
	}

	/**
	 * <pre>
	 * 문자열 MaxLength 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 11.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatMaxStringLength(String data, int maxLen) {
		return (data.trim().length() < maxLen) ? true : false;
	}

	/**
	 * <pre>
	 * 문자열 Max Byte 유효성 검증
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 11.
	 * @param data
	 *            : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatMaxByteSize(String data, int maxLen) {
		return (data.trim().getBytes().length > maxLen) ? true : false;
	}

	/**
	 * <pre>
	 * 기술료관리번호 형식 유효성 검증
	 * </pre>
	 * @author YOUKYUNG HONG
	 * @since 2015. 3. 4.
	 * @param data : 검증할 데이터
	 * @return 검증결과 (true/false)
	 */
	public static boolean checkFormatMngNo(String data) {
		return (Pattern.matches("^[a-zA-Z0-9\\-]+$", data)) ? true : false;
	}
	
	/**
	 * <pre>
	 * 파일 복사 메소드
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 3.
	 * @param source
	 *            : 원본 파일 경로 , target : 목적 파일 경로
	 * @return
	 */
	public static void fileCopy(String source, String target) {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(source);
			fos = new FileOutputStream(target);

			int data = 0;
			byte[] outputByte = new byte[4096];
			while ((data = fis.read(outputByte, 0, 4096)) != -1) {
				fos.write(outputByte, 0, data);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * <pre>
	 * 파일 삭제 메소드
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 5. 2.
	 * @param source
	 *            : 삭제할 파일 경로
	 * @return
	 */
	public static boolean fileDelete(String source) {
		Boolean result = false;
		File deleteFile = null;
		try {
			deleteFile = new File(source);
			if (deleteFile.exists()) {
				deleteFile.delete();
			}
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	/**
	 * <pre>
	 * 파일 읽기 메소드
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 15.
	 * @param source
	 *            : 파일읽을 경로
	 * @return StringBuffer object
	 */
	@SuppressWarnings("resource")
	public static StringBuffer fileRead(String source) {

		StringBuffer sb = new StringBuffer();
		InputStream is = null;
		try {

			is = new FileInputStream(source);

			BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf8"), 4096);
			String temp;
			while ((temp = br.readLine()) != null) {
				sb.append(temp);
				sb.append("\r\n");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return sb;
	}

	/**
	 * <pre>
	 *   파일 파일명, 확장자 분리 메소드
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 23.
	 * @param oriFileName
	 *            : 원본파일 명
	 * @return
	 */
	public static String[] separateFileName(String oriFileName) {

		int file = oriFileName.lastIndexOf("/");
		int ext = oriFileName.lastIndexOf(".");
		String fileName = oriFileName.substring(file + 1, ext);
		String fileExt = oriFileName.substring(ext + 1);

		return new String[] { fileName, fileExt };
	}

	/**
	 * <pre>
	 *   유니코드 파일을 특수문자로 맵핑함.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014.06.19
	 * @param str
	 *            : unicode 문자열
	 * @return 특수문자 맵핑 문자열
	 */
	public static String mappingUnicode(String str) {

		// 파일명에 사용되는 특수문자
		char[] ch = { '~', '!', '@', '#', '$', '%', '&', '(', ')', '=', ';', '[', ']', '{', '}', '^', '-' };
		try {
			for (char c : ch) {
				String encodeData = URLEncoder.encode(c + "", "UTF-8");
				str = str.replaceAll(encodeData, "\\" + c);
			}
			str = str.replaceAll("%2B", "+"); // 띄워쓰기 의 경우 치환함
			str = str.replaceAll("%2C", "_"); // 콤마의 경우 언더바로 치환함
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * <pre>
	 * 날짜 유효성 체크
	 * </pre>
	 * 
	 * @author YouKyung Hong
	 * @since 2014. 7. 21.
	 * @param date
	 *            : 날짜, dateFormat : 날짜 형식
	 * @return
	 */
	public static boolean dateValidityCheck(String date, String dateFormat) {
		boolean result = false;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false);

			sdf.parse(date);

			result = true;
		} catch (ParseException pe) {
			result = false;
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 18.
	 * @version 1.0
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static String fileToString(MultipartFile file) throws IOException {
	    String fileString = new String();
	    FileInputStream inputStream =  null;
	    ByteArrayOutputStream byteOutStream = null;

	    try {
	    	 File convFile = new File( file.getOriginalFilename());
	    	 file.transferTo(convFile);
	         inputStream = new FileInputStream(convFile);
	         byteOutStream = new ByteArrayOutputStream();
	    
		int len = 0;
		byte[] buf = new byte[1024];
	        while ((len = inputStream.read(buf)) != -1) {
	             byteOutStream.write(buf, 0, len);
	        }

	        byte[] fileArray = byteOutStream.toByteArray();
	        fileString = new String(Base64.encode(fileArray));
	        
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	    	inputStream.close();
	        byteOutStream.close();
	    }
	    return fileString;
	}
	
	
	
	/**
	 * <pre>
	 * 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 18.
	 * @version 1.0
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static byte[] singleFileToString(File file) throws IOException {
	   //String fileString = new String();
	    FileInputStream inputStream =  null;
	    ByteArrayOutputStream byteOutStream = null;
	    byte[] fileArray =null;
	    try {
	   /* 	 File convFile = new File( file.getOriginalFilename());
	    	 file.transferTo(convFile);*/
	         inputStream = new FileInputStream(file);
	         byteOutStream = new ByteArrayOutputStream();
	    
		int len = 0;
		byte[] buf = new byte[1024];
	        while ((len = inputStream.read(buf)) != -1) {
	             byteOutStream.write(buf, 0, len);
	        }

	        fileArray = byteOutStream.toByteArray();
	       // fileString = new String(Base64.encode(fileArray));
	        
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	    	inputStream.close();
	        byteOutStream.close();
	    }
	    return fileArray;
	}
	
	
	/**
	 * InputStream의 데이터를 읽어 바이트 배열로 리턴한다.
	 * 
	 * @param dataHandler
	 * @return
	 * @throws IOException
	 */
	public static byte[] toByteArray(DataHandler dataHandler) throws IOException {
		InputStream in = null;
		try {
			in = dataHandler.getInputStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] bytes = new byte[1024];
			int count = 0;
			while((count = in.read(bytes)) != -1) {
				out.write(bytes, 0, count);
			}
			return out.toByteArray();
		} finally {
			if(in != null) {
				in.close();
			}
		}
	}


	
}
