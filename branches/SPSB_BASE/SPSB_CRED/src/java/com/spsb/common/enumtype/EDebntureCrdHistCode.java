package com.spsb.common.enumtype;

/**
 * <pre>
 * 거래처 신용정보 변동이력 휴폐업 코드
 * </pre>
 * @author sungrangkong
 * @since 2014.08.11
 * @version 1.0
 */
public enum EDebntureCrdHistCode {

	CLOSING_01("01", "휴업자"),
	CLOSING_02("02", "폐업자"),
	CLOSING_03("03", "부가가치세 일반과세자"),
	CLOSING_04("04", "부가가치세 간이과세자"),
	CLOSING_05("05", "부가가치세 면세사업자"),
	CLOSING_06("06", "비영리법인/단체/국가기관"),
	CLOSING_07("07", "고유번호가 부여된 단체"),
	CLOSING_99("99", "미등록된 사업자번호");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return해주는 getter 메소드
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.08.11
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return해주는 getter 메소드
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.08.11
	 * @return
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와 설명값을 setting 해주는 채무불이행기타상태타입생성자
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.08.11
	 * @param code : 코드값, desc : 설명값
	 */
	private EDebntureCrdHistCode(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 채무불이행기타상태 조회 Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.08.11
	 * @param code : 코드값
	 * @return
	 */
	public static EDebntureCrdHistCode search(String code) {
		for(EDebntureCrdHistCode type : EDebntureCrdHistCode.values()) {
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
