package com.spsb.common.soap;

/**
 * <pre>
 * Soap CreditRequestAmendDefault message 결과 Vo Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2014. 7. 17.
 * @version 1.0
 */
public class SoapResultVo {

	private int connectionResult;	//HttpURLConnection ResponseCode(200 이면 성공 , 그외에는 실패)
	private String resultCode;		//Soap message 결과코드 ( 성공 : SCMN01, 이외 실패 )
	private String resultMessage;	//Soap message 결과메시지 
	
	public int getConnectionResult() {
		return connectionResult;
	}
	public void setConnectionResult(int connectionResult) {
		this.connectionResult = connectionResult;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
}
