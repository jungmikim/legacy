package com.spsb.common.parent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SLog;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 *  <pre>
 *   Dao 전체 부모 Class
 *  </pre> 
 *  @author Jong Pil Kim
 *	@since 2013. 8. 21.
 *  @version 1.0
 */
@Repository
public class SuperDao extends EgovAbstractDAO {

	@Autowired
	protected SLog log;

}
