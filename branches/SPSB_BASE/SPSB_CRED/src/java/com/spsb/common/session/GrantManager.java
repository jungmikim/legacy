package com.spsb.common.session;

import org.springframework.beans.factory.annotation.Autowired;

import com.spsb.common.collection.SLog;



/**
 * <pre>
 * 권한관리 class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 13.
 * @version 1.0
 */
public class GrantManager {

	private static GrantManager grantManager;
	
	private static String[] needLoginUrl = {"/customer/", "/mypage/", "/help/",  "/debt/", "/config/"};//"/dgdebn/","/debenture/","/charge/"
	private static String[] notGrantCheckUrl = {"/main.do", "/login/", "/user/", "/mypage/", "/zip/", "/help/", "/mainHelp/" };
	private static String[] custAddGrantUrl = {"/customer/getCustomerForm.do", "/customer/getCustomerMultiForm.do"};
	private static String[] debnAddGrantUrl = {"/debenture/getDebentureForm.do", "/debenture/getDebentureMultiForm.do"};
	private static String[] needConfigGrantUrl = {"/config/"};
	//private static String[] needDebtGrantUrl = {"/debt/"};
	//private static String[] debtDocAddGrantUrl = {"/debt/getDebtForm.do"};
	
	//Guest회원
	//private static String[] guestNeedLoginUrl = {"/dgdebn/"};
	
	//SmartBill연계 URL
	/*private static String[] smartBillLinkUrl = {"/myPage/modifyCompInfoForSB.do", "/myPage/modifyUserInfoForSB.do", "/myPage/modifyAdmInfoForSB.do"
												, "/customer/addCustInfoForSB.do", "/customer/modifyCustInfoForSB.do", "/customer/deleteCustInfoForSB.do"
												, "/customer/creditChgRequest.do", "/customer/getCustEwReportPopupForSB.do"};*/
	@Autowired
	protected SLog log;
	
	/**
	 * <pre>
	 * 권한메니져 생성 or 기존 권한메니져 가져오는 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 16.
	 * @version 1.0
	 * @return
	 */
	public static GrantManager getInstance(){
		if (grantManager == null){
			grantManager =  new GrantManager();
		}
		return grantManager;
	}
	
	/**
	 * <pre>
	 * 스마트빌 연계 url check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 20.
	 * @version 1.0
	 * @param uri : request받은 url
	 * @return
	 */
	/*public boolean smartBillCheckUrl(String uri) {
		//스마트빌 연계 url check
		for (String checkUrl : smartBillLinkUrl) {
			if(uri.contains(checkUrl)) {
				return true;
			}
		}
		return false;
	}*/
	
	/**
	 * <pre>
	 * 로그인이 필요한 url check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	public boolean loginCheckUrl(SessionVo sessionVo, String uri) {
		boolean isNeedLogin = false;
		//로그인이 필요한 url check
		for (String checkUrl : needLoginUrl) {
			if(uri.contains(checkUrl)) {
				isNeedLogin = true;
				break;
			}
		}
		//로그인이 필요한 url은 로그인이 되어있는지 check
		if(isNeedLogin){
			if(sessionVo == null){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	/**
	 * <pre>
	 * 게스트 로그인 url check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	public boolean loginCheckUrlForGuest(SessionVo sessionVo, String uri) {
		boolean isNeedLogin = false;
		//로그인이 필요한 url check
		/*for (String checkUrl : guestNeedLoginUrl) {
			if(uri.contains(checkUrl)) {
				isNeedLogin = true;
				break;
			}
		}*/
		//로그인이 필요한 url은 로그인이 되어있는지 check
		if(isNeedLogin){
			if(sessionVo == null){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	/**
	 * <pre>
	 * 해당 url에 권한이 있는지  check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 5.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	public boolean grantCheckUrl(SessionVo sessionVo, String uri) {
		Integer compUsrId = sessionVo.getSessionCompUsrId();
		String admYn = sessionVo.getSessionAdmYn();
		String erpUsrId = sessionVo.getSessionErpUsrId();
		//Boolean isCustAddGrn = sessionVo.isSessionCustAddGrn();
		//Boolean isDebnAddGrn = sessionVo.isSessionDebnAddGrn();
		
		//권한이 필요없는 url
		for (String checkUrl : notGrantCheckUrl) {
			if(uri.contains(checkUrl)) {
				return true;
			}
		}
		
		String mbrStat = sessionVo.getSessionMbrStat();
		if(compUsrId == null || compUsrId <= 0 || mbrStat == null){  //개인회원이 기업에 속해있지 않은 경우
			return false;
		}else if(mbrStat != null && !("N".equals(mbrStat)) ){  //기업회원맴버상태가 정상이 아닌경우
			return false;
		}else if("Y".equals(admYn)){  //개인회원이 기업의 관리자인 경우 모든 권한 사용가능
			//if(erpUsrId != null && !erpUsrId.equals("")){	//erp관리자의 경우는 전체채권보기 권한만 가능하고 등록은 불가능.
				boolean custAddGrant = checkGrant(custAddGrantUrl, uri, false);
				boolean debnAddGrant = checkGrant(debnAddGrantUrl, uri, false);
				if(custAddGrant && debnAddGrant){
					return true;
				}else{
					return false;
				}
			/*}else{
				return true;
			}*/
		}else{ 
			return true;
			//일반 개인회원 권한 체크
			//boolean custAddGrant = checkGrant(custAddGrantUrl, uri, isCustAddGrn);
			//boolean debnAddGrant = checkGrant(debnAddGrantUrl, uri, isDebnAddGrn);
			/*if(custAddGrant && debnAddGrant){
				return true;
			}else{
				return false;
			}*/
		}
	}
	
	/**
	 * <pre>
	 * 해당 url에 채무불이행권한이 있는지  check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 5. 14.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	/*public boolean debtGrantCheckUrl(SessionVo sessionVo, String uri) {
		//url이 채무불이행 url이 아닌경우 
		for (String checkUrl : needDebtGrantUrl) {
			if(!uri.contains(checkUrl)) {
				return true;
			}
		}
		
		String admYn = sessionVo.getSessionAdmYn();
		//Boolean isDebtDocGrn = sessionVo.isSessionDebtDocGrn();
		//Boolean isDebtApplGrn = sessionVo.isSessionDebtApplGrn();
		if("Y".equals(admYn) || isDebtDocGrn){ 
			return true;
		}else{  
			if(isDebtApplGrn){
				boolean debtDocGrant = checkGrant(debtDocAddGrantUrl, uri, isDebtDocGrn);
				if(debtDocGrant ){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
	}*/
	
	/**
	 * <pre>
	 * 해당 url에  menu권한이 있는지  check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 4.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	/*public EMenuNoGrantType menuGrantCheckUrl(SessionVo sessionVo, String uri) {
		//권한이 필요없는 url
		for (String checkUrl : notGrantCheckUrl) {
			if(uri.contains(checkUrl)) {
				return null;
			}
		}
		String mbrStat = sessionVo.getSessionMbrStat();
		if(mbrStat != null && !("N".equals(mbrStat)) ){  //기업회원맴버상태가 정상이 아닌경우
			EMenuNoGrantType type = EMenuNoGrantType.search(uri);
			return type;
		}
		return null;
	}*/
	
	/**
	 * <pre>
	 * 임시회원이 해당 url에  menu권한이 있는지  check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 4.
	 * @version 1.0
	 * @param sessionVo : 로그인 정보
	 * @param uri : request받은 url
	 * @return
	 */
	/*public EMenuNoGrantType menuGrantCheckUrlForGuest(SessionVo sessionVo, String uri) {
		EMenuNoGrantType type = EMenuNoGrantType.search(uri);
		return type;
	}*/
	
	/**
	 * <pre>
	 * 권한이 있는지  check 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 5.
	 * @version 1.0
	 * @param grantUrl : 권한체크할 url
	 * @param uri : request받은 url
	 * @param isGrn : 권한이 있는지 여부
	 * @return
	 */
	private boolean checkGrant(String[] grantUrl, String uri, boolean isGrn){
		//권한이 필요한 url check
		boolean isNeedGrant = false;
		for (String checkUrl : grantUrl) {
			if(uri.contains(checkUrl)) {
				isNeedGrant = true;
				break;
			}
		}
		//권한이 필요한 url은 권한이 있는지 check
		if(isNeedGrant){
			if(isGrn){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
//	/**
//	 * <pre>
//	 * 로그인 상태인지 check 메소드
//	 * </pre>
//	 * @author JUNG EUN LIM
//	 * @since 2013. 8. 27.
//	 * @version 1.0
//	 * @param session : 세션
//	 * @return
//	 */
//	public boolean isLogin(HttpSession session){
//		try{
//			SBox sessionInfo  = new SBox();
//			Integer compUsrId = (Integer) session.getAttribute(ESessionNameType.COMP_USR_ID.getName());
//			Integer usrId = (Integer) session.getAttribute(ESessionNameType.USR_ID.getName());
//			if(!((compUsrId == null || compUsrId <= 0 ) &&( usrId == null || usrId <= 0 ))){
//				sessionInfo.set(ESessionNameType.COMP_USR_ID.getName(), compUsrId);
//				sessionInfo.set(ESessionNameType.USR_ID.getName(), usrId);
//				return true;
//			}else{
//				return false;
//			}
//		}catch(Exception e){
//			log.e("BizException EXCEPTION[로그인 Check error : " + e.getMessage() + "]");
//		}
//		return false;
//	}
	
	
}
