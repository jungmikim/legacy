package com.spsb.common.trans.credit;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.spsb.common.collection.SBox;
import com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType;
import com.spsb.schema.credit.request.amend.ReferencedDefaultAmendmentDocumentType;

/**
 * <pre>
 * XML CreditRequestAmendDefault message 결과 Vo Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2014. 7. 17.
 * @version 1.0
 */
public class CreditRequestAmendDefault {

	/**
	 * <pre>
	 * 채무불이행 상태 Soap 메시지로 HTTP request Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2015. 5. 18.
	 * @version 1.0
	 * @param 
	 * @param 
	 * @return 
	 */
	public String creditRequestAmendDefaultMessage(SBox sBox, String fileName){
		
		String xmlMessage = null;
		try {
			
			//marshelling 
        	String contextPath = CreditRequestAmendDefaultType.class.getPackage().getName();
        	JAXBContext context = JAXBContext.newInstance(contextPath); 
            Marshaller marshaller = context.createMarshaller(); 
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
            
            
            com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType crad = new com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType();
            com.spsb.schema.credit.request.amend.RequestDocumentType requestDocument = new com.spsb.schema.credit.request.amend.RequestDocumentType();
            com.spsb.schema.credit.request.amend.UserBusinessAccountType ubat = new  com.spsb.schema.credit.request.amend.UserBusinessAccountType();
            com.spsb.schema.credit.request.amend.DefaultAmendmentRequestDocumentType dard = new  com.spsb.schema.credit.request.amend.DefaultAmendmentRequestDocumentType();
            
            //요청 문서 정보 영역
            requestDocument.setID("ID");//"ID" sBox.getString("ID")
            requestDocument.setTypeCode("CRQAMD");//"CRQAMD"
            requestDocument.setVersionInformation("Version");//sBox.getString("Version")
            requestDocument.setDescription("DESC"); //sBox.getString("DESC")
            
            //계정 정보 영역
            ubat.setTypeCode("E"); //E: ERP
            ubat.setUserID(""); //sBox.getString("")
            ubat.setLoginID(sBox.getString("sessionLoginId"));
            ubat.setLoginPasswordID("");//sBox.getString("sessionLoginPW") PW는 확인해야함
            
            //요청 정보 영역
            dard.setID(""); //sBox.getString("DOC_CD") DE_DEBT_APPL.DOC_CD
            dard.setUpdateAssignedToRoleDateTime("20152523.00"); //DE_DEBT_APPL.UPD_DT
            dard.setUpdatedUserID(""); //sBox.getString("UPD_ID") DE_DEBT_APPL.UPD_ID
            dard.setTypeCode(sBox.getString("MOD_STAT")); //DE_DEBT_MOD_HIST.STAT 
            dard.setName(""); //sBox.getString("USR_NM") 필수값아니라 null표기 DE_DEBT_STAT_HIST.USR_NM
            
            //연체정보
            ReferencedDefaultAmendmentDocumentType rdad = new ReferencedDefaultAmendmentDocumentType();
            rdad.setDescription(sBox.getString("RMK_TXT")); //DE_DEBT_STAT_HIST.RMK_TXT
            rdad.setDefaultIncludedAmount(sBox.getInt("DEBT_AMT")); //DE_DEBT_MOD_HIST.DEBT_AMT
            dard.setReferencedDefaultAmendmentDocument(rdad); 
            
            crad.setRequestDocument(requestDocument);   
            crad.setUserBusinessAccount(ubat);
            crad.setDefaultAmendmentRequestDocument(dard);
            
         // Marshal Customer 
            // Write to System.out for debugging
            marshaller.marshal(crad, System.out); 
            // Write to File
            marshaller.marshal(crad, new File(fileName));
            
            // 만약 객체 직렬화가 안된다면 다시 XMl파일을 읽어와 String에 넣어줘야한다.
            FileInputStream xmlInputXml = new FileInputStream(fileName);
            InputStream is = xmlInputXml;
           
            xmlMessage = is.toString();
            
            xmlInputXml.close();
            is.close();
            
           
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return xmlMessage;
	}
	
}
