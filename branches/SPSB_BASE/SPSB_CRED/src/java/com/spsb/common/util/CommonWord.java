package com.spsb.common.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

/**
 * <pre>
 * POI 모듈을 이용한 워드파일 작성 API
 * (doc확장자만 가능)
 * </pre>
 * @author YouKyung Hong
 * @since 2014.06.16
 */
public class CommonWord {

	// 워드문서 객체
	private HWPFDocument hwpfDoc = null;
	
	// outputStream 객체
	private OutputStream out = null;

	/**
	 * <pre>
	 * CommonWord 생성자
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014.06.16
	 * @param filePath : 불러올 기존 워드 파일 경로
	 * @throws IOException
	 */
	public CommonWord(String filePath) throws IOException {
		hwpfDoc = new HWPFDocument(new FileInputStream(filePath));
	}
	
	/**
	 * <pre>
	 * 워드문서 객체 Getter 메서드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @return
	 */
	public HWPFDocument getHwpfDoc() {
		return hwpfDoc;
	}

	/**
	 * <pre>
	 * 워드문서 객체 Getter 메서드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @param hwpfDoc : 워드 객체
	 */
	public void setHwpfDoc(HWPFDocument hwpfDoc) {
		this.hwpfDoc = hwpfDoc;
	}

	/**
	 * <pre>
	 * outputStream 객체 Getter 메서드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @return
	 */
	public OutputStream getOut() {
		return out;
	}

	/**
	 * <pre>
	 * outputStream 객체 Getter 메서드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @param out : outputStream 객체
	 */
	public void setOut(OutputStream out) {
		this.out = out;
	}
	
	/**
	 * <pre>
	 * 저장할 워드 파일 생성 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @param filePath : 새로 생성할 파일 경로
	 * @throws IOException
	 */
	public void open(String filePath) throws IOException{
		out = new FileOutputStream(filePath);
	}
	
	/**
	 * <pre>
	 * 특정 범위 다음에 텍스트를 입력하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @param sIdx : 시작 인덱스
	 * @param eIdx : 끝 인덱스
	 * @param text : 입력할 텍스트
	 */
	public void insertAfter(int sIdx, int eIdx, String text) {
		Range range = new Range(sIdx, eIdx, this.hwpfDoc);
		range.insertAfter(text);
	}
	
	/**
	 * <pre>
	 * 문서 전체 범위에서 특정 단어를 변경하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 19.
	 * @param placeholder : 기존에 존재하는 단어
	 * @param newText : 새로 변경할 단어
	 */
	public void replaceAll(String placeholder, String newText) {
		Range range = this.hwpfDoc.getRange();
		range.replaceText(placeholder, newText);
	}
	
	/**
	 * <pre>
	 * 특정 범위 안에서 단어를 변경하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 19.
	 * @param sIdx : 범위 시작 인덱스
	 * @param eIdx : 범위 끝 인덱스
	 * @param placeholder : 기존에 존재하는 단어
	 * @param newText : 새로 변경할 단어
	 */
	public void replaceText(int sIdx, int eIdx, String placeholder, String newText) {
		Range range = new Range(sIdx, eIdx, this.hwpfDoc);
		range.replaceText(placeholder, newText);
	}
	
	/**
	 * <pre>
	 * 특정 범위 안에서 단어를 변경하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 19.
	 * @param range : 범위
	 * @param placeholder : 기존에 존재하는 단어
	 * @param newText : 새로 변경할 단어
	 */
	public void replaceText(Range range, String placeholder, String newText) {
		range.replaceText(placeholder, newText);
	}
	
	/**
	 * <pre>
	 * 설정한 내용을 워드 파일에 적용하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @throws IOException
	 */
	public void write() throws IOException {
		this.hwpfDoc.write(this.out);
		this.out.flush();
	}
	
	/**
	 * <pre>
	 * outputStream 객체를 Close하는 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 6. 16.
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (this.out != null) {
			this.out.close();
		}
	}
	
}
