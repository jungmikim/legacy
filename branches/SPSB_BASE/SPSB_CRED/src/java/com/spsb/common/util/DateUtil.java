package com.spsb.common.util;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 25.
 * @version 1.0
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil
{
  private static final DateFormat DF_TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmssSSS");

  private static final DateFormat DF_TODAY = new SimpleDateFormat("yyyyMMdd");

  private static final DateFormat DF_DATETIME = new SimpleDateFormat("yyyyMMddHHmmss");
  
  private static final DateFormat DF_YEAR = new SimpleDateFormat("yyyy");
  
  private static final DateFormat DF_YEARMONTH = new SimpleDateFormat("yyyyMM");
  

  public static String timestamp()
  {
    return DF_TIMESTAMP.format(new Date());
  }

  public static String datetime()
  {
    return DF_DATETIME.format(new Date());
  }

  public static String today()
  {
    return DF_TODAY.format(new Date());
  }
  
  public static String year()
  {
    return DF_YEAR.format(new Date());
  }
  
  public static String yearMonth()
  {
	  return DF_YEARMONTH.format(new Date());
  }
  
  
  
}