package com.spsb.common.util;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
import java.util.UUID;

public class SysUtil {

	  public static String uuid()
	  {
	    return UUID.randomUUID().toString();
	  }

	  public static String getUuid()
	  {
	    return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 17);
	  }

	  public static String getInstanceId(String serviceCode, String sender)//문서유형코드(6자리)+송신인사업자등록번호(13자리)+메시지생성일시(17자리) =36자리
	  {
		  String id =serviceCode+sender+UUID.randomUUID().toString().replaceAll("-", "").substring(0, 20);
		  return id;
	  }
}
