package com.spsb.common.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <pre>
 * 암호화 모듈 클래스
 * </pre>
 * 
 * @author SungRangKong
 * @since 2013.08.15
 * 
 */
public class CommonCrypto {

	/**
	 * <pre>
	 * SHA-1 암호화 메소드
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.15
	 * 
	 * @param input
	 *            암호화 대상 문자열
	 * @return 암호화 후 반환 문자열
	 * @throws NoSuchAlgorithmException
	 */
	public static String sha1(String input) throws NoSuchAlgorithmException {
		String result = input;
		if (input != null) {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			md.update(input.getBytes());
			BigInteger hash = new BigInteger(1, md.digest());
			result = hash.toString(16);
			if ((result.length() % 2) != 0) {
				result = "0" + result;
			}
		}
		return result;
	}
	
	/**
	 * <pre>
	 * SHA-1 방식으로 암호화 메소드
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 2. 5.
	 * @version 1.0
	 * @param strData : 암호화 대상 문자열
	 * @return 암호화 후 반환 문자열
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptSha1(String strData) throws NoSuchAlgorithmException {
		if (strData == null) {
			return strData;
		}
		StringBuffer strENCData = new StringBuffer();
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(strData.getBytes());
		byte[] digest = md.digest();
		for (int i = 0; i < digest.length; i++) {
			strENCData.append(String.format("%02x", ( digest[i] & 0xFF ) ).toLowerCase());
		}
		return strENCData.toString();
	}

	/**
	 * <pre>
	 * MD5 암호화 메소드
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.15
	 * 
	 * @param input
	 *            암호화 대상 문자열
	 * @return 암호화 후 반환 문자열
	 * @throws NoSuchAlgorithmException
	 */
	public static String md5(String input) throws NoSuchAlgorithmException {
		String result = input;
		if (input != null) {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			BigInteger hash = new BigInteger(1, md.digest());
			result = hash.toString(16);
			if ((result.length() % 2) != 0) {
				result = "0" + result;
			}
		}
		return result;
	}
}
