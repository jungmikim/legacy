package com.spsb.common.enumtype;

/**
 * <pre>
 * 세션에서 사용하는 이름 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 27.
 * @version 1.0
 */
public enum ESessionNameType {

	COMP_USR_ID("sessionCompUsrId", "기업회원순번"),
	COMP_MNG_CD("sessionCompMngCd", "기업관리순번"),
	MBR_ID("sessionMbrId", "기업회원멤버순번"),
	USR_ID("sessionUsrId", "개인회원순번"),
	LOGIN_ID("sessionLoginId", "로그인식별자"),
	COMP_USR_NM("sessionCompUsrNm", "회사이름"),
	USR_NO("sessionUsrNo", "사업자등록번호"),
	USR_NM("sessionUsrNm", "회원이름"),
	MBR_USR_NM("sessionMbrUsrNm", "회원멤버이름"),
	ADM_YN("sessionAdmYn", "관리자여부"),
	COMP_TYPE("sessionCompType", "개인사업자:I, 법인사업자:C"),
	GRN_CD("sessionGrnCd", "권한코드"),
	COMP_USR_STAT("sessionCompUsrStat", "기업회원상태"),
	USR_STAT("sessionUsrStat", "개인회원상태"),
	MBR_STAT("sessionMbrStat", "기업회원멤버상태"),
	EMAIL("sessionEmail", "회원이메일"),
	TEL_NO("sessionTelNo", "회원전화번호"),
	MB_NO("sessionMbNo", "회원휴대폰번호"),
	POST_NO("sessionPostNo", "우편번호"),
	ADDR1("sessionAddr1", "회원주소1"),
	ADDR2("sessionAddr2", "회원주소2"),
	USR_TYPE("sessionUsrType", "사용자 유형(0: 해당사항없음(Default), 1: SAP, 2: Oracle, 3: Legacy)"),
	ERP_USR_ID("sessionErpUsrId", "ERP개인회원순번"),
	IS_DEBT_GRN("isSessionDebtGrnt","채무불이행 작성/신청 권한" ),
	IS_CUST_SEARCH_GRN("isSessionCustSearchGrnt","기업정보검색 권한" ), 
	IS_CUST_BRIF_GRN("isSessionCustBrifGrnt","기업정보브리핑 권한" ),
	IS_CUST_EW_DETAIL_GRN("isSessionCustEwDetailGrnt","조기경보상세 권한"),
	IS_CUST_EW_ADD_GRN("isSessionCustEwAddGrnt","조기경보업체등록 권한"),
	ST_LOGIN_DT("sessionStLoginDt", "Guest회원을 위한 초기 로그인일시"),	//Guest회원
	SB_COMP_USR_ID("sessionSbCompUsrId", "스마트빌 회사순번"),	//SB연동정보
	SB_LOGIN_ID("sessionSbLoginId", "스마트빌 로그인아이디"),	//SB연동정보
	SB_USR_ID("sessionSbusrId", "스마트빌 회원순번");	//SB연동정보
	
	
	private String name;
	private String desc;
	
	/**
	 * <pre>
	 * 이름을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @return name : 이름
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 이름과 설명값을 setting 해주는 세션이름타입생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param name : 이름
	 * @param desc : 설명
	 */
	private ESessionNameType(String name, String desc){
		this.name = name;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 이름에 해당하는 세션이름 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param name : 이름
	 * @return ESessionNameType : 세션에서 사용하는 이름타입 enum
	 */
	public static ESessionNameType search(String name){
		for(ESessionNameType type : ESessionNameType.values()){
			if(type.getName().equals(name)){
				return type;
			}
		}
		return null;
	}
}
