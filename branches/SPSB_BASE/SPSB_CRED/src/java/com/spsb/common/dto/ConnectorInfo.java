package com.spsb.common.dto;

import com.spsb.common.util.SysUtil;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 17.
 * @version 1.0
 */
public class ConnectorInfo {
  String connectorId;
  String description;
  String connectionUrl;
  String endpointDomain;
  String endpointPort;
  String endpointPath;
  String userId;
  String userPasswd;
  String wsdlUrl;

  public ConnectorInfo()
  {
    this.connectorId = SysUtil.uuid();
  }
  public String getConnectorId() {
    return this.connectorId;
  }
  public void setConnectorId(String connectorId) {
    this.connectorId = connectorId;
  }
  public String getDescription() {
    return this.description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getConnectionUrl() {
    if (this.connectionUrl != null) {
      return this.connectionUrl;
    }
    return this.endpointDomain + ":" + this.endpointPort + "/" + this.endpointPath;
  }

  public void setConnectionUrl(String connectionUrl) {
    this.connectionUrl = connectionUrl;
  }
  public String getUserId() {
    return this.userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserPasswd() {
    return this.userPasswd;
  }
  public void setUserPasswd(String userPasswd) {
    this.userPasswd = userPasswd;
  }
  public String getWsdlUrl() {
    return this.wsdlUrl;
  }
  public void setWsdlUrl(String wsdlUrl) {
    this.wsdlUrl = wsdlUrl;
  }
}
