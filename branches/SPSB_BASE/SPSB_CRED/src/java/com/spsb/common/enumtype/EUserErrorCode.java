package com.spsb.common.enumtype;


/**
 * <pre>
 * 회원관련 error 코드 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 27.
 * @version 1.0
 */
public enum EUserErrorCode {
	// 정상 에러코드
	SUCCESS("00000", "성공하였습니다."),
	
	//로그인 에러코드
	LOGIN_NOT_EXIST("00001", "사용자가 존재하지 않습니다."),
	LOGIN_FAILED_PASSWD("00002", "비밀번호가 일치하지 않습니다."),
	LOGIN_RETIRED("00003", "탈퇴요청 상태입니다."),
	LOGIN_PENDING("00004", "승인대기 상태입니다."),
	LOGIN_CANCELED("00005", "승인요청이 거절된 상태입니다."),
	LOGIN_TERMINATE("00006", "회원권한이 해지된 상태입니다."),
	LOGIN_FAILED_CERT("00007", "인증서가 일치하지 않습니다."),
	
	//회원등록 에러코드
	USERJOIN_DEBN_ADD_FAIL("00101", "채권DB에서 개인회원,기업멤버등록 실패"),
	USERJOIN_DEBN_USR_ADD_FAIL("00102", "채권DB에서 개인회원등록 실패"),
	USERJOIN_DEBN_MBR_ADD_FAIL("00103", "채권DB에서 기업회원멤버등록 실패"),
	USERJOIN_DEBN_COMP_USR_ADD_FAIL("00104", "채권DB에서 기업회원 등록 실패"),
	USERJOIN_DEBN_COMP_USR_NOT_EXIST("00105", "채권DB에서 기업회원 존재하지 않음"),
	USERJOIN_GET_COMP_USR_FAIL("00111", "기업회원 조회 실패"),
	USERJOIN_DEBN_USR_NOT_EXIST("00112", "채권DB에서 개인회원 존재하지 않음"),
	USERJOIN_PARAM_DATA_FAIL("00113", "전달 된 Data가 정확하지 않습니다."),
	USERJOIN_DEBN_COMP_USR_UPDATE_FAIL("00114", "채권DB에서 기업회원 업데이트 실패"),
	USERJOIN_DEBN_USR_UPDATE_FAIL("00115", "채권DB에서 개인회원 업데이트 실패"),
	USERJOIN_DEBN_COMP_ADM_UPDATE_FAIL("00116", "채권DB에서 기업관리자  업데이트 실패"),
	
	DUPLICATION_LOGIN_ID("00201", "중복된 아이디가 존재합니다."),
	DUPLICATION_COMP_USR_NO("00202", "중복된 기업회원이 존재합니다."),
	
	//guest회원 로그인
	GUEST_INCORRECT_URL("00301", "잘못된 URL입니다."),
	GUEST_EXPIRED("00302", "만료된 게스트 회원입니다."),
	GUEST_DUPLICATION_EMAIL("00303", "이미 체험 계정을 사용하셨습니다."),
	GUEST_NOT_GRANT("00304", "사용할 수 있는 권한이 없습니다."),
	
	// 실패에러코드
	FAIL("99999", "실패하였습니다.");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 로그인결과코드생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param code : 코드
	 * @param desc : 설명
	 */
	private EUserErrorCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 로그인 결과 코드 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param code : 코드
	 * @return ELoginResultCode : 로그인 결과 코드 enum
	 */
	public static EUserErrorCode search(String code){
		for(EUserErrorCode type : EUserErrorCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
}
