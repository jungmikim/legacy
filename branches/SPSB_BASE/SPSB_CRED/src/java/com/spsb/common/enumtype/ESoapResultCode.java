package com.spsb.common.enumtype;


/**
 * <pre>
 * Soap결과 코드 enum Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 7. 17.
 * @version 1.0
 */
public enum ESoapResultCode {

	// 정상 에러코드
	SUCCESS("SCMN01", "정상 처리");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 17.
	 * @version 1.0
	 * @param code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 17.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	private ESoapResultCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 soap 결과 코드 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 17.
	 * @version 1.0
	 * @param code : 코드
	 * @return ESoapResultCode :  결과  enum
	 */
	public static ESoapResultCode search(String code){
		for(ESoapResultCode type : ESoapResultCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
