package com.spsb.common.util;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 25.
 * @version 1.0
 */

import java.util.StringTokenizer;

public class StringUtil
{
  public static boolean isNull(String text)
  {
    return text == null;
  }

  public static boolean isEmpty(String text)
  {
    if (isNull(text)) return true;

    return "".equals(text);
  }

  public static String getNullToString(String text, String defaultValue)
  {
    if (isNull(text)) return defaultValue;
    return text;
  }

  public static String getNullToEmpty(String text)
  {
    if (isNull(text)) return "";
    return text;
  }

  public static String getEmptyToString(String text, String defaultValue)
  {
    if (isEmpty(text)) return defaultValue;
    return text;
  }

  public static String getEmptyToNull(String text)
  {
    if (text == null) return text;
    if (text.equals("")) return null;
    return text;
  }

  public static int parseInt(String text, int defaultValue)
  {
    if (isEmpty(text)) return defaultValue; try
    {
      return Integer.parseInt(text); } catch (NumberFormatException e) {
    }
    return defaultValue;
  }

  public static long parseLong(String text, long defaultValue)
  {
    if (isEmpty(text)) return defaultValue; try
    {
      return Long.parseLong(text); } catch (NumberFormatException e) {
    }
    return defaultValue;
  }

  public static String[] getTokenizedStringsWithDelimiter(String str, String delim, boolean returnDelims)
  {
    if ((isNull(str)) || (isNull(delim)))
      throw new IllegalArgumentException();
    String[] as = null;
    StringTokenizer st = new StringTokenizer(str, delim, returnDelims);
    int count = st.countTokens();
    if (count > 0) {
      as = new String[count];
      for (int i = 0; i < count; i++) {
        as[i] = st.nextToken();
      }
    }
    return as;
  }

  public static String formatText(String text, String format)
  {
    if ((isEmpty(text)) || (isEmpty(format))) return "";

    int textIndex = 0;
    int formatIndex = 0;
    int textLen = text.length();
    int formatLen = format.length();

    StringBuffer sb = new StringBuffer();

    while ((textIndex < textLen) && (formatIndex < formatLen)) {
      char ch = format.charAt(formatIndex++);
      if (ch == '?')
        sb.append(text.charAt(textIndex++));
      else {
        sb.append(ch);
      }
    }

    return sb.toString();
  }

  public static String getShortName(String text, int delLength, boolean isForward)
  {
    String result = "";
    if (text.length() > delLength) {
      if (isForward)
        result = "..." + text.substring(text.length() - delLength, text.length());
      else {
        result = text.substring(0, delLength) + "...";
      }
      result = "<acronym title=\"" + text + "\">" + result + "</acronym>";
    } else {
      result = text;
    }
    return result;
  }

  public static String substringWithLen(String text, int len)
  {
    if ((isEmpty(text)) || (len < 0)) return text;
    if (text.length() > len) {
      return text.substring(0, len);
    }
    return text;
  }

  public static String replace(String text, String searchString, String replacement) {
    int max = -1;
    if ((isEmpty(text)) || (isEmpty(searchString)) || (replacement == null) || (max == 0)) {
      return text;
    }
    int start = 0;
    int end = text.indexOf(searchString, start);
    if (end == -1) {
      return text;
    }
    int replLength = searchString.length();
    int increase = replacement.length() - replLength;
    increase = increase < 0 ? 0 : increase;
    increase *= (max > 64 ? 64 : max < 0 ? 16 : max);
    StringBuffer buf = new StringBuffer(text.length() + increase);
    while (end != -1) {
      buf.append(text.substring(start, end)).append(replacement);
      start = end + replLength;
      max--; if (max == 0) {
        break;
      }
      end = text.indexOf(searchString, start);
    }
    buf.append(text.substring(start));
    return buf.toString();
  }
}
