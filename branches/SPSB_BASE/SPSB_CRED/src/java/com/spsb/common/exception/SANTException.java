package com.spsb.common.exception;

import com.spsb.common.util.DateUtil;


/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 25.
 * @version 1.0
 */

public class SANTException extends Exception
{
  private static final long serialVersionUID = 1L;
  public static final String DOMAIN_ADAPTER = "ADT";
  public static final String DOMAIN_CONNECTOR = "CON";
  public static final String DOMAIN_SYSTEM = "SYS";
  public static final char SEVERITY_WARN = 'W';
  public static final char SEVERITY_ERROR = 'E';
  public static final String WS_MSG_TAG_MAKE_ERROR = "53001";
  public static final String WS_REQUEST_PROCESS_ERROR = "53002";
  public static final String WS_STATUS_PROCESS_ERROR = "53003";
  public static final String WS_RESULT_PROCESS_ERROR = "53004";
  public static final String WS_MALFORMED_URL_ERROR = "53005";
  public static final String JCO_SEND_ERROR = "53201";
  public static final String JCO_REQUEST_SEND_ERROR = "53202";
  public static final String JCO_STATUS_SEND_ERROR = "53203";
  public static final String JCO_RESULT_SEND_ERROR = "53204";
  public static final String JCO_MSG_TAG_MAKE_ERROR = "53205";
  public static final String JCO_TABLE_READ_ERROR = "53206";
  public static final String JCO_TABLE_WRITE_ERROR = "53207";
  public static final String JCO_SEND_OK = "55000";
  public static final String JCO_DESTINATION_ERROR = "53208";
  public static final String JCO_FUNC_NAME_ERROR = "53209";
  public static final String JCO_CONVERSION_ERROR = "53209";
  public static final String JCO_ABAP_ERROR = "53210";
  public static final String JCO_JCO_ERROR = "53211";
  public static final String JCO_RUNTIME_ERROR = "53212";
  public static final String JCO_INFO_MISMATCH_ERROR = "53213";
  public static final String JCO_SERVER_ERROR = "53200";
  public static final String JCO_FILE_SAVE_ERROR = "53214";
  public static final String JCO_FILE_BINARY_NOT_FOUNT = "53215";
  public static final String JCO_SIGN_VERIFY_ERROR = "53216";
  public static final String WSC_REQUEST_SEND_ERROR = "53011";
  public static final String WSC_STATUS_SEND_ERROR = "53012";
  public static final String WSC_RESULT_SEND_ERROR = "53013";
  public static final String WSC_ACK_SEND_ERROR = "53014";
  public static final String WSC_COMMON_ERROR = "53015";
  public static final String WSC_MALFORMED_URL_ERROR = "53016";
  private String codeId;
  private char severity = 'E';
  private String className;
  private String methodName;
  private int lineNumber;
  private String eventTimestamp;

  public SANTException(String codeId, String message)
  {
    super(message);
    this.codeId = codeId;

    init(this);
  }

  public SANTException(String codeId, Exception e)
  {
    super(e.getMessage());
    this.codeId = codeId;

    init(e);
  }

  private void init(Exception e) {
    StackTraceElement element = extractStackTraceElement(e);
    this.className = element.getClassName();
    this.methodName = element.getMethodName();
    this.lineNumber = element.getLineNumber();

    this.eventTimestamp = DateUtil.timestamp();
  }

  private StackTraceElement extractStackTraceElement(Exception e) {
    StackTraceElement[] elements = e.getStackTrace();
    for (StackTraceElement element : elements) {
      String className = element.getClassName();
      if (className.startsWith("com.sant.platform")) {
        return element;
      }
    }
    return elements[0];
  }

  public String getCodeId()
  {
    return this.codeId;
  }

  public void setCodeId(String codeId)
  {
    this.codeId = codeId;
  }

  public char getSeverity()
  {
    return this.severity;
  }

  public String getClassName()
  {
    return this.className;
  }

  public String getMethodName()
  {
    return this.methodName;
  }

  public int getLineNumber()
  {
    return this.lineNumber;
  }

  public void setClassName(String className)
  {
    this.className = className;
  }

  public void setMethodName(String methodName)
  {
    this.methodName = methodName;
  }

  public void setLineNumber(int lineNumber)
  {
    this.lineNumber = lineNumber;
  }

  public String getEventTimestamp()
  {
    return this.eventTimestamp;
  }

  public void setEventTimestamp(String eventTimestamp)
  {
    this.eventTimestamp = eventTimestamp;
  }
}