package com.spsb.common.enumtype;

/**
 * <pre>
 * 사용자 유형(ERP회원유형) 코드 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 10. 01.
 * @version 1.0
 */
public enum EUserTypeCode {

	DEFAULT("0", "해당사항없음(Default)"),
	SAP("1", "SAP"),
	ORACLE("2", "Oracle"),
	LEGACY("3", "Legacy");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 멤버회원권한생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @param desc : 설명
	 */
	private EUserTypeCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 멤버회원권한 코드 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @return EUserTypeCode : 사용자 유형(ERP회원유형) 코드  enum
	 */
	public static EUserTypeCode search(String code){
		for(EUserTypeCode type : EUserTypeCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
