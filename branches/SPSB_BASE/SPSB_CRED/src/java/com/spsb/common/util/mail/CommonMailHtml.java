package com.spsb.common.util.mail;


/**
 * <pre>
 * 메일 html
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 1. 28.
 * @version 1.0
 */
public class CommonMailHtml {

//	@Value("#{mail['MAIL_IMAGE_URL'].trim()}") 
//	private String mailImageUrl;
	
	/**
	 * <pre>
	 * 체험계정받기 Mail HTML(사용안함-ajax html방법으로 수정됨)
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 28.
	 * @version 1.0
	 * @return
	 */
	public String getGuestUserIdHtml(String domain){
		String imageUrl  = domain + "/web/images/mail";
		StringBuffer sb = new StringBuffer();
		sb.append("<!doctype html>");
		sb.append("<html lang=\"ko\">");
//		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"lang=\"ko\"xml:lang=\"ko\">");
		
//		sb.append("<head><meta http-equiv=\"Content-Type\"content=\"text/html;charset=utf-8\"/><title>채권관리 서비스 체험 계정 메일</title>");
		sb.append("<head><meta charset=\"utf-8\" /><title>채권관리 서비스 체험 계정 메일</title>");
		sb.append("<style type=\"text/css\">");
		sb.append("body {color:#707070; font-family:dotum, arial; font-size:12px; margin:0 auto; line-height:1.7em;}");
		sb.append("h1.title01 {padding-left:20px; margin:0;}");
		sb.append("span.point01 {font-weight:bold; color:#fea700;}");
		sb.append("span.point02 {font-weight:bold;}");
		sb.append("p.txt01 {color:#959595; margin:0 auto;}");
		sb.append("footer {color:#707070; font-family:dotum, arial; font-size:11px; margin:0 auto; line-height:1.5em;}");
		sb.append(".footerWrap h3 {padding:16px 0 0 17px; text-align:left;}");
		sb.append(".footerWrap .relative_company {display:block; padding:20px 0 0; width:580px; overflow:hidden; margin:0 auto; list-style: none;}");
		sb.append(".footerWrap .relative_company li {float:left; padding:0 20px 0 0;}");
		sb.append(".footerWrap .footmenu {overflow:hidden; border-bottom:5px solid #ffa700; padding:0 0 0 7px;}");
		sb.append(".footerWrap .footmenu li {float:left; background:url('" + imageUrl + "/footer_bar.gif') no-repeat 0 12px; padding:11px 8px 14px 9px;}");
		sb.append(".footerWrap .footmenu li:first-child {background:none;}");
		sb.append(".footerWrap .footmenu li a {display:block;}");
		sb.append(".footerWrap .footBtm {padding-top:15px; text-align:center;}");
		sb.append(".footerWrap .footBtm address {font-style:normal; color:#9e9e9e;}");
		sb.append(".footerWrap .footBtm address em {display:block; font-style:normal; color:#9e9e9e;}");
		sb.append(".footerWrap .footBtm span {color:#535353; margin:0 5px;}");
		sb.append(".footerWrap .footBtm .customerService a {color:#00a0e9;}");
		sb.append(".footerWrap .footBtm .copyright {padding:15px 0 0;}");
		sb.append("</style></head>");
		
		sb.append("<body>");
		sb.append("<table width=\"770px\" border=\"0\"><tbody>");
		sb.append("<tr><td style=\"border:0px #c7c7c7; border-top:0; height:7px; background-color:#fea700;\"></tr>");
		sb.append("<tr><td><img src=\"" + imageUrl + "/space.gif\" height=\"30px\"></td></tr>");
		sb.append("</tbody></table>");
		
		sb.append("<h1 class=\"title01\"><img src=\"" + imageUrl + "/tit01.gif\" /></h1>");
		sb.append("<table width=\"770px\" border=\"0\"><tbody>");
		sb.append("<tr><td colspan=\"2\" style=\"border-bottom:1px solid #dedede;\"></td></tr>");
		sb.append("<tr><td colspan=\"2\"><img src=\"" + imageUrl + "/space.gif\" height=\"35px\"></td></tr>");
		sb.append("<tr>");
		sb.append("<td width=\"31\"></td><td>안녕하세요, 채권관리 서비스 스마트채권입니다.<br />");
		sb.append("게스트 회원으로 <span class=\"point01\">1일 체험 계정 받기</span>를 신청해주셔서 감사드립니다.<br /><br />");
		sb.append("1일 동안 채권관리 서비스 중  <span class=\"point01\">채권진단 서비스를 무료로 사용</span>하실 수 있으며, <span class=\"point02\">최고 5건까지 조회 가능</span>합니다.<br />");
		sb.append("비즈니스 온의 채권관리 서비스를 체험해보세요!<br /><br />아래 버튼을 누르시면 채권관리 서비스로 바로 넘어갑니다.<br /></td>");
		sb.append("</tr>");
		sb.append("<tr><td colspan=\"2\"><img src=\"" + imageUrl + "/space.gif\" height=\"35px\"></td></tr>");
		sb.append("<tr><td colspan=\"2\" style=\"border-bottom:1px solid #dedede;\"></td></tr>");
		sb.append("<tr><td colspan=\"2\"><img src=\"" + imageUrl + "/space.gif\" height=\"35px\"></td></tr>");
		sb.append("<tr><td colspan=\"2\" align=\"center\"><a href=\"#\"><img src=\"" + imageUrl + "/btn_go.gif\"></a></td></tr>");
		sb.append("<tr><td colspan=\"2\"><img src=\"" + imageUrl + "/space.gif\" height=\"10px\"></td></tr>");
		sb.append("<tr><td colspan=\"2\" align=\"center\"><p class=\"txt01\">※ 임시 계정의 유효시간은 계정을 확인한 시점에서 24시간입니다.<br />※ 계정을 받은 후 24시간 내에 사용하지 않으시면 자동 소멸됩니다.</p></td></tr>");
		sb.append("</tbody></table>");
		
		sb.append("<table width=\"770px\"><tbody>");
		sb.append("<tr><td><img src=\"" + imageUrl + "/space.gif\" height=\"30px\"></td></tr>");
		
		sb.append("<tr><td height=\"245px\" style=\"background:url('" + imageUrl + "/bg_footer.gif') repeat-x;\">");
		sb.append("<div class=\"footerWrap\"><footer id=\"footer\"><div class=\"footBtm\">");
		sb.append("<address>(주)비즈니스온커뮤니케이션 서울특별시 강남구 논현동 142 영풍빌딩 3F <span>|</span><em>대표자 : 박혜린<span>|</span>   사업자등록번호 220-87-58882 <span>|</span> 통신판매업 : 강남-16190</em></address>");
		sb.append("<p class=\"customerService\">고객센터 <a href=\"tel:1588-8064\">1588-8064</a> <span>|</span> 개인정보관리 : OOO (<a href=\"mailto:OOO@businesson.co.kr\">OOO@businesson.co.kr</a>)</p>");
		sb.append("<p class=\"copyright\">Copyright(C) 2012 (주)비즈니스온커뮤니케이션.All rights reserved.</p>");
		sb.append("<ul class=\"relative_company\">");
		sb.append("<li><img src=\"" + imageUrl + "/footer_relative01.gif\" alt=\"서울경제 브랜드대상\" /></li>");
		sb.append("<li><img src=\"" + imageUrl + "/footer_relative02.gif\" alt=\"KIBO 벤처기업인증\" /></li>");
		sb.append("<li><img src=\"" + imageUrl + "/footer_relative03.gif\" alt=\"표준전자세금 계산서 V3.0\" /></li>");
		sb.append("<li><img src=\"" + imageUrl + "/footer_relative04.gif\" alt=\"한국전자문서 산업협회\" /></li>");
		sb.append("<li><img src=\"" + imageUrl + "/footer_relative05.gif\" alt=\"국세청 대용량 연계사업자\" /></li>");
		sb.append("</ul></div></footer></div>");
		sb.append("</td></tr>");
		
		sb.append("</tbody></table>");
		sb.append("</body>");
		sb.append("</html>");
		return sb.toString();
	}
	
}

