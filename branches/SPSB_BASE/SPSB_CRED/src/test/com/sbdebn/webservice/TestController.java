package com.sbdebn.webservice;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spsb.common.parent.SuperController;
import com.spsb.service.customer.CustomerService;
import com.spsb.vo.JSONTest;

/**
 * <pre>
 *   거래처 단건 등록 관리 Controller Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2013. 8. 21.
 * @version 1.0
 */
@Controller
public class TestController extends SuperController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value="/test/jsonTest.do")
	public @ResponseBody JSONTest jsonTest() {
	    	//가상의 배열및 리스트에 데이터 add
		HashMap<String, String> map = new HashMap<String, String>();
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String,String>>();
	    	
        map.put("no", "3");
        map.put("title", "test3");
        list.add(map);
        map = new HashMap<String, String>();
        map.put("no", "1");
        map.put("title", "test1");
        list.add(map);
        map = new HashMap<String, String>();
        map.put("no", "2");
        map.put("title", "test2");
        list.add(map);
        
    	//VO객체에 SET한후 vo객체자체를 return
    	JSONTest test = new JSONTest();
    	test.setArryList(list);
	    	
		return test;
	}
	
	
	
}
