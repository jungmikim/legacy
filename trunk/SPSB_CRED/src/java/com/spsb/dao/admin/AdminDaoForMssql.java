package com.spsb.dao.admin;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 환경설정  Dao Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 06. 25.
 * @version 1.0
 */
@Repository
public class AdminDaoForMssql extends SuperDao{

	/**
	 * 연동업체 전체 리스트 조회
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public SBoxList<SBox> selectCompMngList() throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("admin.selectCompMngList" ));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 선택한 연동업체에 대한 사업장 정보 조회
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public SBoxList<SBox> selectCompUsrListAboutCompMng(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("admin.selectCompUsrListAboutCompMng" ,sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 선택한 사업장정보에 대한 회원정보 조회
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public SBoxList<SBox> selectMbrListAboutCompUsr(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("admin.selectMbrListAboutCompUsr" ,sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 권한 조회 
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public SBoxList<SBox> selectGrnListAboutMbr(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("admin.selectGrnListAboutMbr" ,sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 상품 조회 
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public SBoxList<SBox> selectPrdListAboutCompMng(SBox sBox) throws BizException {
		SBoxList<SBox> resultList;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("admin.selectPrdListAboutCompMng" ,sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * 
	 * 연동업체 등록
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertCompUser(SBox sBox) throws BizException {

		 int returnBox = 0;

		try {
			returnBox = ((SBox) super.getSqlMapClientTemplate().queryForObject("admin.insertCompUser", sBox)).getInt("TOTAL_CNT");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 연동업체별 상품 등록
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertCompProduct(SBox sBox) throws BizException {

		 int returnBox = 0;

		try {
			returnBox = ((SBox) super.getSqlMapClientTemplate().queryForObject("admin.insertCompProduct", sBox)).getInt("TOTAL_CNT");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 권한 등록
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertGrnInfo(SBox sBox) throws BizException {

		 int returnBox = 0;

		try {
			returnBox = ((SBox) super.getSqlMapClientTemplate().queryForObject("admin.insertGrnInfo", sBox)).getInt("TOTAL_CNT");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 서비스 코드 등록 여부 체크 
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer checkServiceCode(SBox sBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.checkServiceCode", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 중복된 연동업체 등록 여부 체크
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer checkCompMng(SBox sBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.checkCompMng", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 *  권한삭제
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer deleteGrnInfo(SBox sBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.deleteGrnInfo", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 *  상품삭제
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer deletePrdInfo(SBox sBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.deletePrdInfo", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	
	/**
	 * 
	 * 연동업체 등록
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertServiceCode(SBox serviceBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.insertServiceCode", serviceBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 연동업체별 사업장 등록 
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertCustForComp(SBox serviceBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.insertCustForComp", serviceBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
	/**
	 * 
	 * 연동업체별 사업장 등록 
	 * @author HWAJUNG SON
	 * @since 2016. 6. 16.
	 */
	public Integer insertMbrInfo(SBox serviceBox) throws BizException {

		int returnBox = 0;

		try {
			returnBox = (Integer) getSqlMapClientTemplate().queryForObject("admin.insertMbrInfo", serviceBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39903", EErrorCodeType.search("39903").getErrMsg());
		}

		return returnBox;
	}
	
}
