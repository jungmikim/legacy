/**
 * 
 */
package com.spsb.dao.login;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 로그인 Dao Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 26.
 * @version 1.0
 */
@Repository
public class LoginDaoForMssql extends SuperDao{

	/**
	 * <pre>
	 * 개인회원 로그인 조회 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param loginId : 로그인 식별자
	 * @return result : 개인회원 정보
	 */
	public SBox selectLoginMemberUser(String loginId){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("login.selectLoginMemberUser_SQL", loginId);
		return result;
	}
	
	/**
	 * <pre>
	 * 임시회원 로그인 조회 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 21.
	 * @version 1.0
	 * @param tmpUsrId : 임시회원순번
	 * @return result : 개인회원 정보
	 */
	public SBox selectLoginTempUser(Integer tmpUsrId){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("login.selectLoginTempUser_SQL", tmpUsrId);
		return result;
	}
	
	/**
	 * <pre>
	 * 임시회원  최초 로그인정보 변경 업데이트 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 06. 24.
	 * @version 1.0
	 * @param sBox
	 * 			tmpUsrId : 임시회원순번, stat : 상태
	 * @return result : 업데이트 결과
	 */
	public Integer updateTmpUsrStLoginInfo(SBox sBox){
		Integer result = (Integer) super.getSqlMapClientTemplate().queryForObject("login.updateTmpUsrStLoginInfo_SQL", sBox);
		return result;
	}
}
