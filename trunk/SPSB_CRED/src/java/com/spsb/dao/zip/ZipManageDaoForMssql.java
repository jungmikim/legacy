package com.spsb.dao.zip;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 우편번호 찾기 Dao Class
 * </pre>
 * @author KIM GA EUN
 * @since 2014. 1. 7.
 * @version 1.0
 */
@Repository
public class ZipManageDaoForMssql extends SuperDao{
	
	/**
	 * 
	 * <pre>
	 * 시도 List Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 1. 7.
	 * @version 1.0
	 * @return
	 */
	public SBoxList<SBox> selectSidoForWebList(){
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("zip.selectSidoForWebList_SQL"));
		return resultList;
	}
	
	/**
	 * 
	 * <pre>
	 * 군구 List Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 1. 7.
	 * @version 1.0
	 * @return
	 */
	public SBoxList<SBox> selectGunguForWebList(String sido){
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("zip.selectGunguForWebList_SQL", sido));
		return resultList;
	}
	
	/**
	 * 
	 * <pre>
	 * 도로명 주소 + 건물번호 우편번호 검색 List Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 1. 7.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, rdNm : 도로명 주소, mnNo : 건물번호 본번, subNo : 건물번호 부번
	 * @return
	 */
	public SBoxList<SBox> selectAddressByStreetSearchForWebList(SBox sBox){
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("zip.selectAddressByStreetSearchForWebList_SQL", sBox));
		return resultList;
	}
	
	/**
	 * 
	 * <pre>
	 * 동(읍/면) + 지번 우편번호 검색 List Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 1. 7.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, dong : 동(읍/면), mnNo : 지번 본번, subNo : 지번 부번
	 * @return
	 */
	public SBoxList<SBox> selectAddressByDongSearchForWebList(SBox sBox){
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("zip.selectAddressByDongSearchForWebList_SQL", sBox));
		return resultList;
	}
	
	/**
	 * 
	 * <pre>
	 * 건물명(아파트명)우편번호 검색 List Dao Method
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 1. 7.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, bldNm : 건물명(아파트명)
	 * @return
	 */
	public SBoxList<SBox> selectAddressByBuildingSearchForWebList(SBox sBox){
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("zip.selectAddressByBuildingSearchForWebList_SQL", sBox));
		return resultList;
	}

}
