/**
 * 
 */
package com.spsb.dao.mypage;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 마이페이지 - 정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Repository
public class MyPageBelongingDaoForOracle extends SuperDao{
	
	//개인회원 - 소속기업 검색 결과 가져오기
	public SBox selectCompanyUserByUsrNo(String usrNo){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.slectCompanyUserByUsrNo_SQL", usrNo);
		return result;
	}
	
	//개인회원 - 소속기업 기업회원멤버테이블에 Insert
	public SBox insertMbr(SBox sBox){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.insertMbr_SQL", sBox);
		return result;
	}
	
	//개인회원 - 마이페이지 메인 소속기업 현황
	public SBoxList<SBox> selectBelongingtoCompanyList(SBox sBox)throws BizException {
		SBoxList<SBox> resultList;
		
		try{
			resultList  = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("mypage.selectCompMemberList_SQL", sBox));
		}catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectOfficeList_SQL Dao ERROR");
		}
		return resultList;
	}
	
	//개인회원 - 마이페이지 소속기업현황 디폴트 사업자 변경
	public SBox updateMemberByDftYn(SBox sBox){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateMemberByDftYn_SQL", sBox);
		return result;
	}

	//개인회원 - 마이페이지 - 현재 존재하는 기업회원 순번 로직 점검
	public SBox selectUsedCompany(SBox sBox){
		SBox result = (SBox)super.getSqlMapClientTemplate().queryForObject("mypage.selectUsedCompany_SQL", sBox);
		return result;
	}
	
	//마이페이지 - 소식기업 현황 버튼으로 상태변경 
	public SBox updateBelongtoCompStat(SBox sBox){
		SBox result = (SBox) super.getSqlMapClientTemplate().queryForObject("mypage.updateBelongtoCompStat_SQL", sBox);
		return result;
	}
	
	
	
	
	
	
}
