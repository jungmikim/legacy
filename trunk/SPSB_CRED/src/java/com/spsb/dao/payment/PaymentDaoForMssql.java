package com.spsb.dao.payment;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 결제 관련 Dao Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 7. 23.
 * @version 1.0
 */
@Repository
public class PaymentDaoForMssql extends SuperDao{

	/**
	 * <pre>
	 * 결제이력테이블등록과 PG주문번호 생성 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 23.
	 * @version 1.0
	 * @param sBox
	 * 			compUsrId : 기업회원순번, usrId : 개인회원순번, prdId : 상품식별자, prdAmt : 상품가격, payType : 결제유형
	 * @return orderId : 주문번호
	 */
	public String insertPGAcctOrderID(SBox sBox){
		return (String) super.getSqlMapClientTemplate().queryForObject("payment.insertPGAcctOrderID_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 회원구매 테이블등록  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 4.
	 * @version 1.0
	 * @param sBox
	 * 			compUsrId : 기업회원순번, usrId : 개인회원순번, prdId : 상품식별자, prdIdKey : 상품식별자KEY, stat : 상태, prdAmt : 상품가격
	 * 			, payDt : 결제일시, stDt : 적용시작일, edDt : 적용종료일, reqId : 요청ID, isCompSearchPrd : 기업정보조회 상품여부
	 * @return usrPoId : 회원구매 순번
	 */
	public Integer insertUserPo(SBox sBox){
		return (Integer) super.getSqlMapClientTemplate().queryForObject("payment.insertUserPo_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 회원구매입금 테이블등록  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 9. 1.
	 * @version 1.0
	 * @param sBox
	 * 			usrPoId : 개인구매순번, payDt : 결제일시, orderId : 주문번호, payType : 결제유형, payAmt : 납입금액
	 * @return usrPoPayId : 회원구매입금 순번
	 */
	public Integer insertUserPoPay(SBox sBox){
		return (Integer) super.getSqlMapClientTemplate().queryForObject("payment.insertUserPoPay_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 결제연계 등록  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 27.
	 * @version 1.0
	 * @param sBox
	 * 			orderId : 주문번호, sndStat : 전송상태, statCd : 결과코드, rmkTxt : 결과설명
	 * @return polId : 폴링식별자
	 */
	public Integer insertAcctLnkPol(SBox sBox){
		return (Integer) super.getSqlMapClientTemplate().queryForObject("payment.insertAcctLnkPol_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 주문번호로 PG결제내역 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 4.
	 * @version 1.0
	 * @param orderId : 주문번호
	 * @return 
	 */
	public SBox selectPGAcctHistByOrderId(String orderId){
		return (SBox) super.getSqlMapClientTemplate().queryForObject("payment.selectPGAcctHistByOrderId_SQL", orderId);
	}
	
	/**
	 * <pre>
	 * 주문번호로 회원구매 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 7.
	 * @version 1.0
	 * @param orderId : 주문번호
	 * @return 
	 */
	public SBox selectUserPoByOrderId(String orderId){
		return (SBox) super.getSqlMapClientTemplate().queryForObject("payment.selectUserPoByOrderId_SQL", orderId);
	}
	
	/**
	 * <pre>
	 * 기업의 채무불이행순번에 의한 회원구매 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 27.
	 * @version 1.0
	 * @param sBox
	 * 			compUsrId : 기업회원순번, prdId : 상품식별자, prdIdKey : 상품식별자KEY(채무불이행신청순번)
	 * @return 
	 */
	public SBox selectUserPoForDebt(SBox sBox){
		return (SBox) super.getSqlMapClientTemplate().queryForObject("payment.selectUserPoForDebt_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * 연계를 위한 결제정보 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 13.
	 * @version 1.0
	 * @param orderId : 주문번호
	 * @return 
	 */
	public SBox selectPayInfoForLink(String orderId){
		return (SBox) super.getSqlMapClientTemplate().queryForObject("payment.selectPayInfoForLink_SQL", orderId);
	}
	
	/**
	 * <pre>
	 * 연계를 위한 조기경보 고객정보 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 27.
	 * @version 1.0
	 * @param orderId : 주문번호
	 * @return 
	 */
	public SBox selectEWCustInfoForLink(String orderId){
		return (SBox) super.getSqlMapClientTemplate().queryForObject("payment.selectEWCustInfoForLink_SQL", orderId);
	}
	
	/**
	 * <pre>
	 * 조기경보상품에 따른 회원의 구매check(주문번호 제외) Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 12. 3.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @param orderId : 주문번호
	 * @return 
	 */
	public Integer selectUserPoCheckByEWForLink(Integer compUsrId, String orderId){
		SBox sBox = new SBox();
		sBox.set("compUsrId", compUsrId);
		sBox.set("orderId", orderId);
		return (Integer) super.getSqlMapClientTemplate().queryForObject("payment.selectUserPoCheckByEWForLink_SQL", sBox);
	}
	
	/**
	 * <pre>
	 * PG결제내역 수정 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 30.
	 * @version 1.0
	 * @param sBox
	 * 			orderId : 주문번호, tid : 거래번호, bankCd : 은행코드, payType : 결제유형, respCd : 응답코드, respDt : 응답일자, payNm : 결제자명
	 * 			, payAcctNo : 결제계좌, cflag : 무통장입금플래그, bankDt : 입금또는취소일시, cseqno : 모통장입금시퀀스
	 * @return
	 */
	public Integer updatePGAcctHist(SBox sBox){
		Integer result = (Integer) super.getSqlMapClientTemplate().queryForObject("payment.updatePGAcctHist_SQL", sBox);
		return result;
	}
	
	/**
	 * <pre>
	 * 무통장 PG결제내역 수정 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 4.
	 * @version 1.0
	 * @param sBox
	 * 			orderId : 주문번호, cflag : 무통장입금플래그, tamount : 입금총액(무통장입금), camount : 현입금액(무통장입금), bankDt : 입금또는취소일시, cseqno : 모통장입금시퀀스
	 * @return
	 */
	public Integer updatePGAcctHistForCas(SBox sBox){
		Integer result = (Integer) super.getSqlMapClientTemplate().queryForObject("payment.updatePGAcctHistForCas_SQL", sBox);
		return result;
	}
	
	/**
	 * <pre>
	 * 회원구매 수정 Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 6.
	 * @version 1.0
	 * @param sBox
	 * 			orderId : 주문번호, usrId : 개인회원순번, stat : 상태, payDt : 결제일시, tamount : 입금총액(무통장입금), stDt : 적용시작일, edDt : 적용종료일
	 * @return
	 */
	public Integer updateUserPo(SBox sBox){
		Integer result = (Integer) super.getSqlMapClientTemplate().queryForObject("payment.updateUserPo_SQL", sBox);
		return result;
	}
}
