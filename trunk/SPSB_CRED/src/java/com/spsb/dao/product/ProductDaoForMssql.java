package com.spsb.dao.product;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 * 상품 관련 Dao Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 8. 12.
 * @version 1.0
 */
@Repository
public class ProductDaoForMssql extends SuperDao{

	/**
	 * <pre>
	 * 상품 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 12.
	 * @version 1.0
	 * @param prdId : 상품식별자
	 * @return 
	 */
	public SBox selectProduct(Integer prdId){
		return (SBox) getSqlMapClientTemplate().queryForObject("product.selectProduct_SQL", prdId);
	}
	
	/**
	 * <pre>
	 * 상품코드로 상품 조회  Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @param prdCd : 상품코드
	 * @return 
	 */
	public SBox selectProductByPrdCd(String prdCd){
		return (SBox) getSqlMapClientTemplate().queryForObject("product.selectProductByPrdCd_SQL", prdCd);
	}
	
	/**
	 * <pre>
	 * 상품 조회 Map(key: 상품코드) Dao Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 24.
	 * @version 1.0
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, SBox> selectProductMap(){
		SBox sBox = new SBox();
		return (Map<String, SBox>) getSqlMapClientTemplate().queryForMap("product.selectProductMap_SQL", sBox, "PRD_CD");
	}
}
