package com.spsb.dao.customer;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;

/**
 * <pre>
 *   CustomerManagement Dao Class
 * </pre>
 * 
 * @author Jong Pil Kim
 * @since 2013. 8. 19.
 * @version 1.0
 */
@Repository
public class CustomerDaoForMssql extends SuperDao {

	/**
	 * <pre>
	 *   거래처 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 16.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월 말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일],
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순, 내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지, searchType : 검색유형 [ L(리스트) , E(Excel)
	 *            ], crdDtType : 신용정보변동이력[전체, 오늘, 직접입력], stCrdDt : 신용정보 변동이력 시작일자, edCrdDt : 신용정보 변동이력 종료일자, sessionPayTermType:세션에 저장된 결제타입, sessionPayTermDay:세션에 저장된 결제일자
	 * 
	 * @return resultList : 거래처 검색 리스트
	 */
	public SBoxList<SBox> selectCustomerList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}
	
	/**
	 * <pre>
	 *   거래처검색 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 10.
	 * @version 1.0
	 * @param sBox
	 *            custType : 거래처 유형[법인,개인], custKwd : 거래처 검색키워드, sessionCompUsrId : 기업멤버 식별자
	 * 
	 * @return resultList : 거래처 검색 리스트
	 */
	public SBoxList<SBox> selectCustomerListForSearch(SBox sBox) throws BizException {
		
		SBoxList<SBox> resultList;
		
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerListForSearch_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		
		return resultList;
	}

	/**
	 * <pre>
	 *   거래처 Total Count Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월 말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일],
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순, 내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지, searchType : 검색유형 [ L(리스트) , E(Excel)
	 *            ], crdDtType : 신용정보변동이력[전체, 오늘, 직접입력], stCrdDt : 신용정보 변동이력 시작일자, edCrdDt : 신용정보 변동이력 종료일자
	 * 
	 * @return result : 거래처 검색 TOTAL COUNT
	 */
	public int selectCustomerTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}

	/**
	 * <pre>
	 *   I/F를 위한 대상 거래처 리스트 조회  Dao Method
	 * </pre>
	 * @author Jung mi Kim
	 * @since 2015. 06. 01.
	 * @version 1.0
	 * @param sBox  sessionCompUsrId : 기업멤버 식별자, compMngCd : 회사관리코드, trans: 전송상태
	 * @return resultList : 거래처 검색 리스트
	 */
	public SBoxList<SBox> selectCustomerListForIF(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerListForIF_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}
	
	
	/**
	 * <pre>
	 * 거래처 중복 체크 / 개인 주민번호 중복체크 로직 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 27.
	 * @param custNo
	 *            : 거래처 사업자 등록 번호, comUsrId : 기업 회원 순번, custType : 거래쳐 유형 P OR C
	 * @return
	 */
	public SBox selectDuplicationCheckCustomer(String custNo, int sessionCompUsrId, String custType) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custNo", custNo);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("custType", custType);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDuplicationCheckCustomer_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	} 
	/**
	 * <pre>
	 * 거래처 정보 단건 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번,isSessionDebnSearchGrnY:채권 조회 권한, sessionUsrId: 로그인 개인회원 순번
	 * @return
	 */
	public SBox selectCustomer(String custNo, String sessionUsrNo, int sessionUsrId) throws BizException {

		// Parameter Box Setting
		SBox sBox = new SBox();
		sBox.set("custNo", custNo);
		sBox.set("sessionUsrNo", sessionUsrNo);
		//sBox.set("isSessionDebnSearchGrnY", isSessionDebnSearchGrnY);
		sBox.set("sessionUsrId", sessionUsrId);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomer_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 거래처 신용변동이력 조회 (유료/무료)Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param EWCd
	 *            : EW 코드, historyRowSize : 출력될 목록 갯수, historyNum : 현제 페이지 순번
	 * @return
	 */
	public SBoxList<SBox> selectCustomerAllCreditHistoryList(SBox sBox) throws BizException {
		
		SBoxList<SBox> returnBoxList = null;
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerAllCreditHistoryList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBoxList;
	}

	
	/**
	 * <pre>
	 *  거래처 신용변동이력 토탈 카운트 조회 (유료/무료)Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 12. 2.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox selectCustomerAllCreditHistoryListCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerAllCreditHistoryListCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}
	
	
	/**
	 * <pre>
	 *   거래처 신용정보 변동이력 Total Count Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 10. 02.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번, searchType : 조회 유형(오늘, 최근)
	 * 
	 * @return result : 거래처 신용정보 변동이력 TOTAL COUNT
	 */
	public int selectCustomerCreditHistoryTotalCount(int sessionCompUsrId, String searchType, String sessionUsrNo) throws BizException {

		int result;
		SBox paramBox = new SBox();
		paramBox.set("sessionCompUsrId", sessionCompUsrId);
		paramBox.set("searchType", searchType);
		paramBox.set("sessionUsrNo", sessionUsrNo);

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerCreditHistoryTotalCount_SQL", paramBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}
	
	/**
	 * <pre>
	 * 거래처 신용변동 테이블에 해당 거래처가 존재하는지 확인 DAO METHOD
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 7. 24.
	 * @param custNo : 사업자등록번호
	 * @return BTF : 거래처 신용변동 테이블에 해당 거래처 존재 유무
	 * @throws BizException
	 */
	public boolean selectCustomerCreditCount(String custNo) throws BizException {
		boolean BTF = false;

		try {
			int cnt = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerCreditCount_SQL", custNo)).getInt("CNT");
			if (cnt > 0) {
				BTF = true; // 카운트가 1개 이상일 경우 true 반환
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return BTF;
	}

	
	
	/**
	 * <pre>
	 * 크레딧서비스 조기경보 대상 거래처 신규등록 응답 메시지 상태변경
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 2.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox updateCustResponseForIF(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustResponseForIF_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * 크레딧서비스  I/F 조기경보 등급 업데이트    Procedure 호
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 2.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox updateCustCrdForIF(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustCrdForIF_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}
	
	

	/**
	 * <pre>
	 * [KED연계페이지] EW(조기경보)상세
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 10. 12.
	 * @version 1.0
	 * @param custNo
	 * @return
	 * @throws BizException
	 */
	public  SBox selectEwReportForKED(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerForKED_SQL",sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	} 
	
	/**
	 * <pre>
	 *   배치 아이디로 거래처 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param batchId : 배치아이디
	 * 
	 * @return resultList : 거래처 검색 리스트
	 */
	public SBoxList<SBox> selectCustomerListForBatchId(String batchId) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerListForBatchId_SQL", batchId));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}	
	
	/**
	 * <pre>
	 * 본사, 거래처 사업자번호로 거래처순번(회사코드,회사순번)을 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 20.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox selectCustInfoForResponseIF(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.selectCustInfoForResponseIF_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	} 
	
	/**
	 * <pre>
	 * 거래처 신용정보 업데이트/이력추가(K-Link EW30데이터) DAO METHOD
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2016. 4. 29.
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox inertCustCrdHistKLinkEW30ForIF(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.inertCustCrdHistKLinkEW30ForIF_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return returnBox;
	}	
	
	/**
	 * <pre>
	 * 거래처 신용정보 업데이트/이력추가(K-Link EW10데이터) DAO METHOD
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2016. 4. 29.
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBox updateCustCrdKLinkEW10ForIF(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustCrdKLinkEW10ForIF_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}   

		return returnBox;
	}	
	
	
	/**
	 * <pre>
	 *  조기경보 거래처 관리페이지에서 거래처 삭제에따른 Legacy 거래처 동기화
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 20.
	 * @version 1.0
	 * @param custId
	 * @return
	 * @throws BizException
	 */
	public  SBox updateCustListSyncfromKed() throws BizException {

		SBox returnBox = null;

		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.updateCustListForIF_SQL");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("50012", EErrorCodeType.search("50012").getErrMsg());
		}
		return returnBox;
	} 
	
	/**
	 * <pre>
	 * 거래처 수정 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,ownNo : 주민등록번호 ,corpNo : 법인등록번호 ,eMail : 이메일 ,postNo : 우편번호 ,custAddr1 : 회사주소 앞자리 ,custAddr2 : 회사주소 뒷자리
	 *            ,bizType : 업종 ,bizCond : 업태 ,telNo : 전화번호 ,faxNo : 팩스번호 ,mbNo : 휴대폰번호 ,crdLtd : 여신한도
	 * @return
	 */
	public SBox updateCustomer(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.updateCustomer_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}
	
	/**
	 * <pre>
	 * [KED연계페이지] 라이센스키 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016.06.27
	 * @version 1.0
	 * @param 
	 * @return
	 * @throws BizException
	 */
	public  SBox selectLicenseKeyForKED() throws BizException {
		SBox returnBox = null;

		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.selectLicenseKeyForKED_SQL");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	} 
	
	/**
	 * <pre>
	 *   거래처 삭제 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 21.
	 * @version 1.0
	 * @param custIdList
	 *            : 거래처 순번 String (ex : 1,2,3,)
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @param sessionUsrId
	 *            : 로그인한 회원 순번 Data
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 
	public SBox deleteCustomer(String customerIdList, int sessionCompUsrId, int sessionUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("customerIdList", customerIdList);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("sessionUsrId", sessionUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteCustomer_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 거래처 정보 단건 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번,isSessionDebnSearchGrnY:채권 조회 권한, sessionUsrId: 로그인 개인회원 순번
	 * @return
	 *//*
	public SBox selectCustomer(int custId, int sessionCompUsrId, int sessionUsrId) throws BizException {

		// Parameter Box Setting
		SBox sBox = new SBox();
		sBox.set("custId", custId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		//sBox.set("isSessionDebnSearchGrnY", isSessionDebnSearchGrnY);
		sBox.set("sessionUsrId", sessionUsrId);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomer_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/
	/**
	 * <pre>
	 * 거래처 단건등록 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 26.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,corpNo : 법인등록번호 ,eMail : 이메일 ,postNo : 우편번호 ,custAddr1 : 회사주소 앞자리 ,custAddr2 : 회사주소 뒷자리 ,bizType : 업종
	 *            ,bizCond : 업태 ,telNo : 전화번호 ,faxNo : 팩스번호 ,mbNo : 휴대폰번호
	 * @return
	 */
	/*public SBox insertCustomer(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomer_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 단거 등록 결제조건 입력 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 26.
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custId : 거래처 순번,initPayTermType : 초기 결제 조건 유형 ,initPayTermDay : 초기 결제 조건 기준일 ,futurePayTermType : 차후 결제 조건 유형 ,futurePayTermDay : 차후 결제 조건 기준일 ,payTermDate : 결제 조건 적용 날짜
	 * @return
	 */
	/*public SBox insertPayTermByInsertCustomer(SBox sBox) throws BizException {

		SBox returnBox = null;

		SBox paramBox = (SBox) sBox.clone();
		paramBox.set("initPayTermDay", "".equals(sBox.getString("initPayTermDay")) ? null : sBox.getString("initPayTermDay"));
		paramBox.set("futurePayTermDay", "".equals(sBox.getString("futurePayTermDay")) ? null : sBox.getString("futurePayTermDay"));

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertPayTermByInsertCustomer_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 결제조건 리스트 토탈 카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번
	 * @return
	 */
	/*public SBox selectPayTermListTotalCount(int custId, int sessionCompUsrId) throws BizException {

		// Parameter Box Setting
		SBox sBox = new SBox();
		sBox.set("custId", custId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectPayTermListTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 결제조건 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번 , num : 현재 페이지 순번, rowSize : 출력될 데이터 개수
	 * @return
	 */
	/*public SBoxList<SBox> selectPayTermList(SBox sBox) throws BizException {

		SBoxList<SBox> returnBoxList = null;
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectPayTermList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBoxList;
	}*/

	
	/**
	 * <pre>
	 * 거래처 수정(여신한도) Dao Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,ownNo : 주민등록번호 ,corpNo : 법인등록번호 ,eMail : 이메일 ,postNo : 우편번호 ,custAddr1 : 회사주소 앞자리 ,custAddr2 : 회사주소 뒷자리
	 *            ,bizType : 업종 ,bizCond : 업태 ,telNo : 전화번호 ,faxNo : 팩스번호 ,mbNo : 휴대폰번호 ,crdLtd : 여신한도
	 * @return
	 */
	/*public Integer updateCreditInfo(SBox sBox) throws BizException {

		Integer result;
		try {
			result = (Integer) super.getSqlMapClientTemplate().queryForObject("customer.updateCreditInfo_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return result;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 등록 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermType : 결제조건 유형 , payTermDay : 결제 조건 기준일, payTermDate : 결제 조건 적용날짜
	 * @return
	 */
	/*public SBox insertPayTerm(SBox sBox) throws BizException {

		SBox returnBox = null;
		SBox paramBox = (SBox) sBox.clone();
		paramBox.set("payTermDay", "".equals(sBox.getString("payTermDay")) ? null : sBox.getString("payTermDay"));

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertPayTerm_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 삭제 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermSn : 결제조건순번, payTermStat : 결제조건 상태
	 * @return
	 */
	/*public SBox deletePayTerm(int custId, int sessionCompUsrId, int payTermSn, String payTermStat) throws BizException {

		SBox sBox = new SBox();
		sBox.set("custId", custId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("payTermSn", payTermSn);
		sBox.set("payTermStat", payTermStat);

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deletePayTerm_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 수정 페이지 내에서의 결제조건 수정 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 28.
	 * @param custId
	 *            : 거래처 순번, sessionCompUsrId : 기업회원 순번, payTermSn : 결제조건순번,payTermType : 결제 조건 유형, payTermDay : 결제 조건 기준일, payTermDate : 결제 조건 적용날짜
	 * @return
	 */
	/*public SBox updatePayTerm(SBox sBox) throws BizException {

		SBox returnBox = null;

		SBox paramBox = (SBox) sBox.clone();
		paramBox.set("payTermDay", "".equals(sBox.getString("payTermDay")) ? null : sBox.getString("payTermDay"));

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updatePayTerm_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 대량등록이력 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 01.
	 * @param num
	 *            : 현재 페이지 번호 , rowSize : 출력될 목록의 갯수, sessionCompUsrId : 기업회원 순번 , searchType : 검색 유형(리스트, 엑셀) , fileName : 파일이름, custType : 거래처 유형 , firstDate : 업로드 초기날짜 ,lastDate :업로드 마지막 날짜
	 * @return
	 */
	/*public SBoxList<SBox> selectMultiHistoryList(SBox sBox) throws BizException {

		SBoxList<SBox> returnBoxList = null;
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectMultiHistoryList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBoxList;
	}*/

	/**
	 * <pre>
	 * 거래처 대량등록이력 카운트 갯수 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 01.
	 * @param num
	 *            : 현재 페이지 번호 , rowSize : 출력될 목록의 갯수, sessionCompUsrId : 기업회원 순번 , searchType : 검색 유형(리스트, 엑셀) , fileName : 파일이름, custType : 거래처 유형 , firstDate : 업로드 초기날짜 ,lastDate :업로드 마지막 날짜
	 * @return
	 */
	/*public SBox selectMultiHistoryTotalCount(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectMultiHistoryTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   대량등록 체크 여부를 조회하는 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 01.
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @return : 대량등록유형 (C: 거래처) ,MLT_ID : 대량등록번호 순번
	 */
	/*public SBox selectMulti(String mltType, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("mltType", mltType);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}
*/
	

	/**
	 * <pre>
	 * 거래처 대량등록 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 03.
	 * @version 1.0
	 * @param fileName
	 *            : 파일이름 , mltType: 대량등록 유형, stat: 대량등록 상태 sessionCompUsrId : 기업회원순번, sessionUsrId : 개인회원순번
	 * @return
	 */
	/*public SBox insertMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 임시거래처 대량등록 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 03.
	 * @version 1.0
	 * @param cust_type
	 *            : 거래처 유형 , cust_nm : 거래처 명, cust_no : 사업자등록번호, own_nm : 대표자명, corp_no : 법인등록번호, post_no : 우편번호 , addr_1 :주소1 , addr_2 : 주소2 ,biz_type :업종 ,biz_cond : 업태, tel_no : 전화, mb_no : 휴대폰 번호,
	 *            fax_no : 팩스번호, crd_ltd : 여신한도, ini_unpd : 초기미수금, pay_term_type : 결제조건, pay_term_day :결제조건일자 ,email : 이메일 mltId : 대량등록번호, errCode : 에러코드
	 * @return
	 */
	/*public SBox insertTempCustomerMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertTempCustomerMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 임시저장 거래처 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 03.
	 * @param sessionCompUsrId
	 *            : 기업회원 순번 , stat : 상태 (Y: 오류있음,N: 오류없음)
	 * @return
	 */
	/*public SBoxList<SBox> selectCustomerTemporary(int sessionCompUsrId, String stat) throws BizException {

		SBoxList<SBox> returnBoxList = null;

		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("stat", stat);
		try {
			returnBoxList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerTemporary_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBoxList;
	}*/

	/**
	 * <pre>
	 *   오류난 거래처 대량등록 삭제 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원순번 , stat: 상태(Y:오류있음 N:오류없음) , mltId: 대량등록 유형(거래처, 채권)
	 */
	/*public SBox deleteTempCustomerMulti(String stat, int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("stat", stat);
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteTempCustomerMulti_SQL", sBox)).get(0);

		} catch (Exception ex) {
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 오류난 거래처 대량등록 수정 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param cust_type
	 *            : 거래처 유형 , cust_nm : 거래처 명, cust_no : 사업자등록번호, own_nm : 대표자명, own_no : 대표자 주민등록번호 corp_no : 법인등록번호, post_no : 우편번호 , addr_1 :주소1 , addr_2 : 주소2 ,biz_type :업종 ,biz_cond : 업태, tel_no :
	 *            전화, fax_no : 팩스번호, crd_ltd : 여신한도, ini_unpd : 초기미수금, pay_term_type : 결제조건, pay_term_day :결제조건일자 ,email : 이메일 mltId : 대량등록번호, errCode : 에러코드
	 * @return
	 */
	/*public SBox updateTempCustomerMulti(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateTempCustomerMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   중복된 거래처 내역 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 28.
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 ,cust_no : 사업자번호 cust_type : 거래처 유형
	 * @return : 중복된 거래처, 임시거래처 list
	 */
	/*public SBoxList<SBox> selectDuplicationCustomerMulti(String cust_no, int sessionCompUsrId) throws BizException {

		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("cust_no", cust_no);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDuplicationCustomerMulti_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   대량이력 삭제 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 */
	/*public SBox deleteMulti(int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteMulti_SQL", sBox)).get(0);

		} catch (Exception ex) {
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   성공한 임시거래처 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 * @return resultList : 성공한 임시거래처 리스트
	 */
	/*public SBoxList<SBox> selectTempCustomerList(int mltId, int sessionCompUsrId) throws BizException {

		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectTempCustomerList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 * 대량이력 수정 Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번 , mltType : 대량이력 유형(C:거래처 B:채권) totalCnt : 성공한 임시거래처 카운트수
	 * @return
	 */
	/*public SBox updateMulti(int mltId, int sessionCompUsrId, String stat, int sessionUsrId, String totalCnt) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("stat", stat);
		sBox.set("sessionUsrId", sessionUsrId);
		sBox.set("totalCnt", totalCnt);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateMulti_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 임시 거래처 카운트 조회   Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 10. 07.
	 * @version 1.0
	 * @param mltId
	 *            : 기업회원순번, sessionCompUsrId : 로그인한 기업회원 순번
	 * @return
	 */
	/*public SBox selectTempCustomerTotalCount(int mltId, int sessionCompUsrId) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("mltId", mltId);
		sBox.set("sessionCompUsrId", sessionCompUsrId);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectTempCustomerTotalCount_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 	만기일자 조회  Dao Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013. 11. 26.
	 * @version 1.0
	 * @param cust_no
	 *            : 사업자번호, sessionCompUsrId : 로그인한 기업회원 순번, bill_dt: 세금계산서 작성일자, sessionPayTermType : 결제조건유형, sessionPayTermDay : 결제조건기준일
	 * @return
	 */
	/*public SBox selectCustomerLtdDate(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerLtdDate_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 대량등록 상태값을 조회하는 Dao Method
	 * </pre>
	 * 
	 * @author YOUKYUNG HONG
	 * @since 2014. 03. 31.
	 * @param sessionCompUsrId
	 *            : 로그인한 기업회원 순번 Data
	 * @return STAT : 대량등록유형 (C: 거래처) ,MLT_ID : 대량등록번호 순번
	 */
	/*public SBoxList<SBox> selectMultiStat(String mltType, int sessionCompUsrId) throws BizException {

		SBoxList<SBox> resultList = null;
		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("mltType", mltType);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectMultiStat_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 16.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 식별자, sessionCompUsrId : 기업멤버 식별자, isSessionDebnSearchGrnY : 채권 조회 권한, sessionUsrId : 회원 식별자
	 * 
	 * @return resultList : 거래처 수금계획 리스트
	 */
	/*public int selectCustomerPlanListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerPlanListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 16.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 식별자, sessionCompUsrId : 기업멤버 식별자, isSessionDebnSearchGrnY : 채권 조회 권한, sessionUsrId : 회원 식별자, rowSize : 출력개수
	 * 
	 * @return resultList : 거래처 수금계획 리스트
	 */
	/*public SBoxList<SBox> selectCustomerPlanList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerPlanList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 마스터 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @param custPlnSn
	 *            : 거래처 수금 계획 순번
	 * @return
	 */
	/*public SBox selectCustomerPlan(int custPlnSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custPlnSn", custPlnSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerPlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획에서 채권 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 17.
	 * @version 1.0
	 * @param sBox
	 * 			custId : 거래처 순번, orderCondition : 정렬기준, orderType : 정렬순서, bondMbrId : 채권 담당자 순번, custPlnSn : 거래처 수금 계획, excludeFlag : 타 거래처 수금 계획 제외 여부
	 * @return resultList : 채권 리스트
	 */
	/*public SBoxList<SBox> selectDebentureListForCustomerPlan(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureListForCustomerPlan_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   채권 수금계획[연기]조회 Dao 메소드
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @version 1.0
	 * @param sBox
	 * 
	 * @return resultList : 채권 리스트
	 */
	/*public SBoxList<SBox> selectDebentureListForDelay(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureListForDelay_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 계획 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번, totUnpdAmt : 미수총금액, plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, rmkTxt : 비고, debnCnt : 채권수, sessionCompUsrId : 기업회원 순번, sessionUsrId : 회원 순번
	 * @return
	 */
	/*public SBox insertCustomerPlan(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomerPlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 채권 수금계획 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금 계획 순번, debnId : 채권 순번, plnColAmt : 수금예정금액
	 * @return
	 */
	/*public SBox updateDebenturePlan(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateDebenturePlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권 수금 계획 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 22.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금 계획 순번, debnId : 채권 순번, plnColAmt : 수금예정금액, delayFlag : 연기 로직에 의한 채권 수금계획 등록인지 여부
	 * @return
	 */
	/*public SBox insertDebenturePlan(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertDebenturePlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권 수금 계획 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 * @version 1.0
	 * @param debnId
	 *            : 채권 순번, custPlnSn : 거래처 수금 계획 순번
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	/*public SBox deleteDebenturePlan(String debnId, int custPlnSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("debnId", debnId);
		sBox.set("custPlnSn", custPlnSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteDebenturePlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금계획 순번, totUnpdAmt : 미수총금액, plnColDt : 수금 예정 일자, plnColAmt : 수금 예정 금액, rmkTxt : 비고, debnCnt : 채권수, sessionCompUsrId : 기업회원 순번, isSessionDebnSearchGrn : 채권조회 권한,
	 *            isSessionCustAddGrn : 거래처 등록 권한, sessionUsrId : 회원 순번
	 * @return
	 */
	/*public SBox updateCustomerPlan(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerPlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 연기 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금계획 순번, newCustPln : 새로운 거래처 수금계획 순번, updateType : 수금계획 상태 플래그, rmkTxt : 비고, totRmnAmt : 미수잔액, updId : 수정자 순번, dlyDt : 연기일자
	 * @return
	 */
	/*public SBox updateCustomerPlanStat(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerPlanStat_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 23.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금계획 순번, updateType : 수금계획 상태 플래그, selectedFlag : 선택해서 삭제된 것인지 판단하는 플래그
	 * @return
	 */
	/*public SBox deleteCustomerPlanStat(int custPlnSn, String updateType, int sessionUsrId, String selectedFlag) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custPlnSn", custPlnSn);
		sBox.set("updateType", updateType);
		sBox.set("updId", sessionUsrId);
		sBox.set("selectedFlag", selectedFlag);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerPlanStat_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 마스터 정보 Count Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 20.
	 * @version 1.0
	 * @param custPlnSn
	 *            : 거래처 수금계획 순번
	 * @return result : 거래처 수금 TOTAL COUNT
	 */
	/*public int selectCustomerColCount(int custPlnSn) throws BizException {

		int result;
		SBox sBox = new SBox();
		sBox.set("custPlnSn", custPlnSn);

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerColCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 마스터 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @param custPlnSn
	 *            : 거래처 수금 계획 순번
	 * @return
	 */
	/*public SBox selectCustomerCol(int custPlnSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custPlnSn", custPlnSn);

		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerCol_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권 수금계획[수금정보 포함]조회 Dao 메소드
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @version 1.0
	 * @param sBox
	 * 			custId : 거래처 순번 , orderCondition : 정렬기준 , orderType : 정렬순서 , custPlnSn : 거래처 수금 계획
	 * @return resultList : 채권 리스트
	 */
	/*public SBoxList<SBox> selectDebentureListWithColInfo(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureListWithColInfo_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @version 1.0
	 * @param sBox
	 *            insertParam : 채권 수금 예정 Insert 파라미터, custId : 거래처 순번, colDt : 수금일자 colAmt : 수금액, colType : 수금방식, debnCnt : 채권 건수, rmkTxt : 비고, sessionCompUsrId : 기업회원 순번, isSessionDebnSearchGrn :
	 *            채권조회 권한, isSessionCustAddGrn : 거래처 등록 권한, sessionUsrId : 회원 순번
	 * @return
	 */
	/*public SBox insertCustomerCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomerCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권 수금 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 25.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금 계획 순번, debnId : 채권 순번, colAmt : 수금금액
	 * @return
	 */
	/*public SBox insertDebentureCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 채권 미수금액 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 04. 28.
	 * @version 1.0
	 * @param sBox
	 *            debnId : 채권 순번, colAmt : 수금금액, colStatType : 거래처 수금 상태
	 * @return
	 */
	/*public SBox updateDebentureForColSum(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateDebentureForColSum_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번
	 * 
	 * @return result : 거래처 독촉 토탈카운트
	 */
	/*public int selectCustomerPrsListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerPrsListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 식별자, rowSize : 출력개수, customerPrsListTotalCount : 거래처 독촉 토탈카운트
	 * 
	 * @return resultList : 거래처 수금계획 리스트
	 */
	/*public SBoxList<SBox> selectCustomerPrsList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerPrsList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @param custPrsSn
	 *            : 독촉 순번
	 * @return
	 */
	/*public SBox selectCustomerPrs(int custPrsSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custPrsSn", custPrsSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerPrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 채권 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 * 			custId : 거래처 순번,orderCondition : 정렬기준,orderType : 정렬순서,bondMbrId : 채권 담당자 순번,custPrsSn : 거래처 독촉 순번
	 * @return resultList : 채권 리스트
	 */
	/*public SBoxList<SBox> selectDebentureListForPrs(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureListForPrs_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번, rmkTxt : 비고, debnCnt : 채권수, prsType : 독촉유형, prsDt : 독촉일자 sessionCompUsrId : 기업회원 순번, sessionUsrId : 회원 순번
	 * @return
	 */
	/*public SBox insertCustomerPrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomerPrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   채권 독촉 등록 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 *            custPrsSn : 거래처 독촉 순번, debnId : 채권 순번
	 * @return
	 */
	/*public SBox insertDebenturePrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertDebenturePrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   독촉 첨부파일 등록 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @param sBox
	 *            custPrsSn : 독촉 순번, fileNm : 파일명, filePath : 파일 수정 경로
	 * @return
	 * @throws BizException
	 */
	/*public SBox insertCustomerPrsFile(SBox sBox) throws BizException {
		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomerPrsFile_SQL", sBox)).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}

		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 파일 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 29.
	 * @version 1.0
	 * @param sBox
	 * 			custPrsSn : 독촉 순번
	 * @return resultList : 파일 리스트
	 */
	/*public SBoxList<SBox> selectFileListForPrs(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectFileListForPrs_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   독촉 증빙파일 삭제 대상 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 6. 18.
	 * @param deleteFileParam
	 *            : 독촉 파일 순번 리스트
	 * @return
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDeletePrsileListInfo(String deleteFileParam) throws BizException {
		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("deleteFileParam", deleteFileParam);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDeletePrsileListInfo_SQL", sBox));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 첨부파일 정보 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param deleteFileParam
	 *            : 삭제할 파일 정보 순번
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	/*public SBox deleteFilePrs(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteFilePrs_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 독촉 채권 정보 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param deleteDebnParam
	 *            : 삭제할 독촉 채권 정보 순번
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	/*public SBox deleteDebenturePrs(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteDebenturePrs_SQL", sBox)).get(0);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 거래처 독촉 마스터 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번, rmkTxt : 비고, debnCnt : 채권수, prsType : 독촉유형, prsDt : 독촉일자 sessionCompUsrId : 기업회원 순번, sessionUsrId : 회원 순번, custPrsSn : 독촉 순번
	 * @return
	 */
	/*public SBox updateCustomerPrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerPrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *    거래처 독촉 삭제[실제로는 수정] Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param sBox
	 *            deleteParam : 삭제 대상 독촉 순번
	 * @return
	 */
	/*public SBox deleteCustomerPrs(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteCustomerPrs_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금업무 마스터 정보 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @param custColSn
	 *            : 거래처 수금 순번
	 * @return
	 */
	/*public SBox selectCustomerColNoPlan(int custColSn) throws BizException {

		SBox returnBox = null;
		SBox sBox = new SBox();
		sBox.set("custColSn", custColSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerColNoPlan_SQL", sBox)).get(0);
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금에서 채권 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번, orderCondition : 정렬 조건, orderType : 정렬 순서, bondMbrId : 채권 담당자 순번, custColSn : 거래처 수금 순번
	 * @return resultList : 채권 리스트
	 */
	/*public SBoxList<SBox> selectDebentureListForColNoPlan(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureListForColNoPlan_SQL", sBox));
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 토탈카운트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 순번
	 * 
	 * @return result : 거래처 수금 토탈카운트
	 */
	/*public int selectCustomerColListTotalCount(SBox sBox) throws BizException {

		int result;

		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerColListTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return result;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 4. 30.
	 * @version 1.0
	 * @param sBox
	 *            custId : 거래처 식별자, rowSize : 출력개수, customerColListTotalCount : 거래처 수금 토탈카운트
	 * 
	 * @return resultList : 거래처 수금 리스트
	 */
	/*public SBoxList<SBox> selectCustomerColList(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCustomerColList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 채권 정보 삭제 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 02.
	 * @version 1.0
	 * @param deleteDebnParam
	 *            : 삭제할 수금 채권 정보 순번, custColSn : 수금 순번
	 * @return : 삭제 여부 를 boolean type으로 return 함 (true = 삭제성공, false = 삭제실패)
	 */
	/*public SBox deleteDebentureCol(String deleteDebnParam, int custColSn) throws BizException {

		SBox returnBox = null;

		SBox paramBox = new SBox();
		paramBox.set("deleteDebnParam", deleteDebnParam);
		paramBox.set("custColSn", custColSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteDebentureCol_SQL", paramBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금계획 순번 조회  Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 134.
	 * @param sBox
	 *            deleteParam : 거래처 수금 삭제 대상 파라미터
	 * @return
	 */
	/*public String selectCustomerPlnListSn(SBox sBox) throws BizException {

		String result = null;
		try {
			result = (String) super.getSqlMapClientTemplate().queryForObject("customer.selectCustomerPlnListSn_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return result;
	}*/

	/**
	 * <pre>
	 *   채권 수금 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 22.
	 * @version 1.0
	 * @param sBox
	 *            custColSn : 거래처 수금 계획 순번, debnId : 채권 순번, colAmt : 수금금액, colStatType : 거래처 수금 상태
	 * @return
	 */
	/*public SBox updateDebentureCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateDebentureCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *   거래처 수금 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 02.
	 * @version 1.0
	 * @param sBox
	 *            custColSn : 거래처 수금 순번, colDt : 수금 일자, colAmt : 수금 금액, rmkTxt : 비고, debnCnt : 채권수, colType : 수금방식, statType : 상태, sessionCompUsrId : 기업회원 순번, isSessionDebnSearchGrn : 채권조회 권한,
	 *            isSessionCustAddGrn : 거래처 등록 권한, sessionUsrId : 회원 순번
	 * @return
	
	public SBox updateCustomerCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	} */

	/**
	 * <pre>
	 *   거래처 수금계획 미수총금액 수정 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 05. 14.
	 * @version 1.0
	 * @param sBox
	 *            custPlnSn : 거래처 수금계획 순번, custColSn : 거래처 수금 순번, debnId : 채권번호, colAmt : 수금금액
	 * @return
	 */
	/*public SBox updateCustomerPlanForTotUnpdAmt(SBox sBox) throws BizException {

		SBox returnBox = null;

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.updateCustomerPlanForTotUnpdAmt_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 *    거래처 수금 삭제[실제로는 수정] Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 07.
	 * @version 1.0
	 * @param sBox
	 *            deleteParam : 삭제 대상 수금 순번
	 * @return
	 */
	/*public SBox deleteCustomerCol(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteCustomerCol_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19904", EErrorCodeType.search("19904").getErrMsg());
		}
		return returnBox;
	}*/

	/**
	 * <pre>
	 * 특정 조회조건에 따른 담당자 검색 DAO METHOD
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 08.
	 * @param sessionCompUsrId
	 *            : 기업회원순번, mbrStat : 회원상태
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectMbrEmployeeListByCondition(int sessionCompUsrId, String mbrStat) throws BizException {
		SBoxList<SBox> resultList;
		SBox sBox = new SBox();
		sBox.set("sessionCompUsrId", sessionCompUsrId);
		sBox.set("mbrStat", mbrStat);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectMbrEmployeeListByCondition_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 * 거래처 요약 채권가이드 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 06. 05.
	 * @param custId
	 *            : 거래처 순번
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDebentureGuideAbstract(int custId) throws BizException {
		SBoxList<SBox> resultList;
		SBox paramBox = new SBox();
		paramBox.set("custId", custId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureGuideAbstract_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}*/

	/**
	 * <pre>
	 * 거래처 요약 : 채권가이드 서브키워드 의미 조회 DAO METHOD
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 07. 17.
	 * @param dgsMnKwId
	 *            : 메인 키워드 순번
	 * @throws BizException
	 */
	/*public SBoxList<SBox> selectDebentureGuideKeywordMeanByMKeyword(int dgsMnKwId) throws BizException {
		SBoxList<SBox> resultList;
		SBox paramBox = new SBox();
		paramBox.set("dgsMnKwId", dgsMnKwId);

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectDebentureGuideKeywordMeanByMKeyword_SQL", paramBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("39901", EErrorCodeType.search("39901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 *   독촉 첨부파일 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 7. 23.
	 * @version 1.0
	 * @param fileSn
	 *            : 증빙자료 순번
	 * 
	 * @return result : 증빙자료 정보
	 */
	/*public SBox selectPrsFile(int fileSn) throws BizException {

		SBox returnBox = null;

		SBox sBox = new SBox();
		sBox.set("fileSn", fileSn);

		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectPrsFile_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}
		return returnBox;
	}*/

	
	
	/**
	 * <pre>
	 * 거래처 신용변동 테이블 등록 DAO METHOD
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 7. 24.
	 * @param custNm : 거래처명, custNo : 거래처 사업자등록번호, stat : 거래처 신용변동 상태
	 * @return REPL_CD : 응답코드
	 * @throws BizException
	 */
	/*public SBox insertCustomerCredit(SBox sBox) throws BizException {

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.insertCustomerCredit_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 조기경보 서비스를 결제한 사용자 인지 여부, EW듭급, 결제번호
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 10. 14.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	/*public SBox selectUsrPoInfo(SBox sBox){
		SBox returnBox = null;
		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.selectUsrPoInfo_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}*/
	
	/**
	 * <pre>
	 * 조기경보 서비스를 결제한 사용자 인지 여부
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 10. 14.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	public String selectUsrPoInfoCheck(int compUsrId){
		String resultYn = null;
		try {
			resultYn = (String)super.getSqlMapClientTemplate().queryForObject("customer.selectUsrPoInfoCheck_SQL", compUsrId);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultYn;
	} */
	
	/**
	 * <pre>
	 * 조기경보 서비스 무료회원 자체 팝업
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 10. 14.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	/*public SBox selectCustCrdHistForEW(SBox sBox){
		SBox returnBox = null;
		try {
			returnBox = (SBox)super.getSqlMapClientTemplate().queryForObject("customer.selectCustCrdHistForEW_SQL", sBox);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return returnBox;
	}*/
	
	
	/**
	 * <pre>
	 *  스마트빌  연계 신규 거래처 추가 변경 인터페이스
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param loginId:로그인아이디 , custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,corpNo : 법인등록번호 ,
	 * 			post: 우편번호  ,addr1 :회사주소, bizType : 업종 ,bizCond : 업태 ,telNo: 전화  ,faxNo : 팩스, mbNo : 휴대폰 ,email : 이메일 
	 * @return returnBox
	 
	public SBox insertCustomerForSB(SBox sBox)throws BizException {
		SBox returnBox = null;
		try {
			returnBox = (SBox) super.getSqlMapClientTemplate().queryForObject("customer.insertCustomerForSB_SQL", sBox);
		}  catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19902", EErrorCodeType.search("19902").getErrMsg());
		}
		return returnBox;
	}*/
	
	
	/**
	 * <pre>
	 * 스마트빌  연계 거래처 삭제 Dao Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param loginId:로그인아이디 ,custNo : 사업자 등록번호 
	 * @return returnBox
	 */
	/*public SBox deleteCustomerForSB(SBox sBox) throws BizException {
		
		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.deleteCustomerForSB_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19903", EErrorCodeType.search("19903").getErrMsg());
		}
		return returnBox;
	}*/
	
	
	/**
	 * <pre>
	 *   거래처 리스트 조회 Dao Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2013. 8. 16.
	 * @version 1.0
	 * @param sBox
	 *            sessionCompUsrId : 기업멤버 식별자, custType : 거래처 유형[법인,개인], custSearchType : 거래처 검색조건[번호,회사명], custKwd : 거래처 검색키워드, payTermType : 결제조건[당월 말, 당월 ()일, 익월 초, 익월 말, 익월 ()일, 작성일자 +()일],
	 *            orderCondition : 정렬조건[등록일자, 신용정보 확인일자] crdLtdType : 여신한도[초과, 미달], orderType : 정렬유형[오름차순, 내림차순], rowSize : 목록갯수[10, 30, 50, 100], num : 현재 페이지, searchType : 검색유형 [ L(리스트) , E(Excel)
	 *            ], crdDtType : 신용정보변동이력[전체, 오늘, 직접입력], stCrdDt : 신용정보 변동이력 시작일자, edCrdDt : 신용정보 변동이력 종료일자, sessionPayTermType:세션에 저장된 결제타입, sessionPayTermDay:세션에 저장된 결제일자
	 * 
	 * @return resultList : 거래처 검색 리스트
	 */
	/*public SBoxList<SBox> selectCreditChgListForSB(SBox sBox) throws BizException {

		SBoxList<SBox> resultList;

		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("customer.selectCreditChgListForSB_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultList;
	}*/
	
	/**
	 * <pre>
	 *  스마트빌 조기경보 무료회원 EW상세 조회  Dao Method
	 * </pre>
	 * 
	 * @author Jung Mi Kim
	 * @since 2014. 11. 24.
	 * @version 1.0
	 * @param sBox - custNo: 거래처 사업자번호, ewDt:기준일자
	 * @return resultSBox : EW무료 등급 조회
	 */
	/*public SBox selectCustCrdHistForSB(SBox sBox) throws BizException {
		SBox resultSBox;
		try {
			resultSBox = (SBox)(super.getSqlMapClientTemplate().queryForObject("customer.selectCustCrdHistForSB_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultSBox;
	}
	*/
	/**
	 * <pre>
	 * 거래처 조기경보 결제 여부에 따른 파라미터값 셋팅 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 12. 3.
	 * @version 1.0
	 * @param sBox compUsrId:회사순번,custNo:거래처사업자번호,poYn:결제여부
	 * @return 
	 * @throws BizException
	 */
/*	public SBox selectCustCreditUserInfo(SBox sBox) throws BizException {
		SBox resultSBox;
		try {
			resultSBox = (SBox)(super.getSqlMapClientTemplate().queryForObject("customer.selectCustCreditUserInfo_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("19901", EErrorCodeType.search("19901").getErrMsg());
		}

		return resultSBox;
	}*/
	
	
}
