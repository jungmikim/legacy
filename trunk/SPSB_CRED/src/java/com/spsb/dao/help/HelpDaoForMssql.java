package com.spsb.dao.help;

import org.springframework.stereotype.Repository;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperDao;


@Repository
public class HelpDaoForMssql extends SuperDao {

	/**
	 * <pre>
	 * 공지사항 - 세부 검색 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 9. 30.
	 * @version 1.0
	 * @param ntcId : 공지사항 순번, ntcType : 게시판 타입
	 * @return
	 * @throws BizException
	 */
	public SBox selectNoticeDetail(SBox sBox)throws BizException {

		SBox result = new SBox();
		try {
			result = (SBox) super.getSqlMapClientTemplate().queryForObject("help.selectNoticeDetail_SQL", sBox);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectNoticeDetail Dao ERROR");
		}
		return result;
	}


	/**
	 * <pre>
	 * 공지사항 - 공통 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 9. 30.
	 * @version 1.0
	 * @param sBox - num : 현재화면 ,ctnId : 컨텐츠 코드, rowSize: 화면 사이즈, searchText: 검색어
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectNoticeSearchList(SBox sBox) {
		SBoxList<SBox> resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectSearchNotice_SQL", sBox));
		return resultList;
	}
	
	
	
	/**
	 * <pre>
	 * 공지사항  - List 전체갯수
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 08.
	 * @version 1.0
	 * @param sBox - num : 현재화면 ,ctnId : 컨텐츠 코드, rowSize: 화면 사이즈, searchText: 검색어
	 * @return 
	 * @throws BizException
	 */
	public int selectNoticeTotalCount(SBox sBox)  { 
		int result = ((SBox) super.getSqlMapClientTemplate().queryForObject("help.selectNoticeTotalCount_SQL", sBox)).getInt("totalCnt");
		return result;
	}
	

	/**
	 * <pre>
	 * 채권 공지사항에서 뿌려주는 화면
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 11.
	 * @version 1.0
	 * @param sBox -ROWSIZE : 보여질 갯수 입력
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectNoticeNewList(SBox sBox)throws BizException {

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectNewNotice_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "getNoticeNewList Dao ERROR");
		}
		return resultList;
	}
	
	
	/**
	 * <pre>
	 * FAQ  - List 전체갯수
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 4. 08.
	 * @version 1.0
	 * @param sBox - num : 현재화면 ,ctnId : 컨텐츠 코드, rowSize: 화면 사이즈, searchText: 검색어
	 * @return 
	 * @throws BizException
	 */
	public int selectFaqTotalCount(SBox sBox)throws BizException  { 

		int result = 0;
		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("help.selectFaqTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectTotalCount Dao ERROR");
		}

		return result;
	}
	
	
	/**
	 * <pre>
	 * FAQ - 공통 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 08.
	 * @version 1.0
	 * @param sBox - num : 현재화면 ,ctnId : 컨텐츠 코드, rowSize: 화면 사이즈, searchText: 검색어
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectFaqSearchList(SBox sBox)throws BizException {

		SBoxList<SBox> resultList = null;
		try {
			/*만약 초기에 들어와서 검색어가 없을경우 전체를 뿌려주기 위한 해답*/
			if(sBox.getString("searchText").isEmpty()){
				sBox.set("searchText",null);
			}
			
			resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectSearchFaq_SQL", sBox));
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectFaqSearchList Dao ERROR");
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * 고객지원 - 질문 분류 명 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 08.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectQclNm(SBox sBox)  throws BizException{

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectQcl_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectQcl Dao ERROR");
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * FAQ - 세부 검색 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 9. 30.
	 * @version 1.0
	 * @param faqId : FAQ 순번
	 * @return
	 * @throws BizException
	 */
	public SBox selectFaqDetail(String faqId)throws BizException {

		SBox result = new SBox();
		try {
			result = (SBox) super.getSqlMapClientTemplate().queryForObject("help.selectFaqDetail_SQL", faqId);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectFaqDetail Dao ERROR");
		}
		return result;
	}
	
	
	/**
	 * <pre>
	 *  1:1문의 등록화면 5개 최신글 가지고오기
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 11.
	 * @version 1.0
	 * @param sBox - rowSize : 보여질 갯수 표기
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectInquiryPreViewList(SBox sBox)throws BizException {

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectInquiryPreViewList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectInquiryPreViewList_SQL Dao ERROR");
		}
		return resultList;
	}
	
	
	/**
	 * <pre>
	 * 일대일 등록화면 DB등록 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 09
	 * @version 1.0
	 * @param sBox - qstType:질문자 유형, qstId: 질문자 식별자, eMail:이메일, telNo:전화번호
	 *               qst:답변 질문내용, tlNm:질문 제목, userNm:사용자이름, reQst:재질문여부, qclId:질문분류식별자
	 *               smsYn:SMS수신여부, ansYn:답변완료여부, chkYn:질문확인여부
	 * @return
	 * @throws BizException
	 */
	public SBox insertInquiry(SBox sBox)  throws BizException{ 

		SBox returnBox = null;
		try {
			returnBox = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("help.insertInquiry_SQL", sBox)).get(0);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "insertInquiry_SQL Dao ERROR");
		}
		return returnBox;
	}
	
	
	
	/**
	 * <pre>
	 * 전체 List 갯수 가지고 오기
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 09
	 * @version 1.0
	 * @param sBox - num: 혅재화면 ,ctnId: 컨텐츠 순번, rowSize: 화면 갯수지정
	 * @return
	 * @throws BizException
	 */
	public int selectTotalCount(SBox sBox)  throws BizException{ 

		int result = 0;
		try {
			result = ((SBox) super.getSqlMapClientTemplate().queryForObject("help.selectInquiryTotalCount_SQL", sBox)).getInt("totalCnt");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectTotalCount Dao ERROR");
		}

		return result;
	}
	
	
	/**
	 * <pre>
	 * 1:1문의 List조회 화면
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 09
	 * @version 1.0
	 * @param sBox - num: 혅재화면 ,ctnId: 컨텐츠 순번, rowSize: 화면 갯수지정
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectInquiryList(SBox sBox)  throws BizException{ 

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("help.selectInquiryList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectInquiryList Dao ERROR");
		}

		return resultList;
	}
	
	
	/**
	 * <pre>
	 * 1:1문의 상세 화면 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 09
	 * @version 1.0
	 * @param qaId :회원문의 답변순번
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectInquiryDetail(SBox sBox)throws BizException {

		SBoxList<SBox>  resultList = null;
		try {
			resultList =new SBoxList<SBox> (super.getSqlMapClientTemplate().queryForList("help.selectInquiryDetail_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "getInquiryDetail Dao ERROR");
		}
		return resultList;
	}
	
	/**
	 * <pre>
	 * 3개의 리스트 기본으로 뿌려줌 1:1문의 List조회 화면 By Mypage(채권)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 30.
	 * @version 1.0
	 * @param sBox - ctnId: 컨텐츠 순번, rowSize: 화면 갯수지정
	 * @return
	 * @throws BizException
	 */
	public SBoxList<SBox> selectInquiryNewList(SBox sBox)  throws BizException{ 

		SBoxList<SBox> resultList = null;
		try {
			resultList = new SBoxList<SBox>(super.getSqlMapClientTemplate().queryForList("help.selectInquiryNewList_SQL", sBox));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new BizException("21", "selectInquiryNewList_SQL Dao ERROR");
		}

		return resultList;
	}
	
	
}
