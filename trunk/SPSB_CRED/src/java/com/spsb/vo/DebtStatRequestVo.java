package com.spsb.vo;

/**
 * <pre>
 * 채무불이행 상태 폴링 Soap 메시지 를 보낼 request Vo Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 7. 17.
 * @version 1.0
 */
public class DebtStatRequestVo {

	private String userNo;			//채무불이행신청테이블에서 ERP아이디(ERP_ID)
	private String userNm;			//채무불이행신청테이블에서 회사명
	private String startData;		//
	private String endData;		    //변경 사유
	private String reqID;		    //결제요청
									//ex)계약서사본.txt:3249/계약서사본_20140711174559912_1.JPG|호적등록.jpg:3251/호적등록_20140711175250846_1.JPG
									//파일이 존재하지않을 경우 ''값 사용
	
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getStartData() {
		return startData;
	}
	public void setStartData(String startData) {
		this.startData = startData;
	}
	public String getEndData() {
		return endData;
	}
	public void setEndDate(String endData) {
		this.endData = endData;
	}
	
	public void setReqId(String reqID) {
		this.reqID = reqID;
	}
	
	
}
