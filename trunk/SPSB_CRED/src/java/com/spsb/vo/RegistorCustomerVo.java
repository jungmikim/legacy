package com.spsb.vo;



/**
 * <pre>
 *  요청 공통메시지
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 29.
 * @version 1.0
 */

public class RegistorCustomerVo {

	private String responseType = "A";
	private String versionInformation = "1.0.0";
	private String userBusinessAccountTypeCode = "E";
	private String reqName = "크레딧서비스조기경보대상거래처등록요청서";
	private String typeCode = "REQ";
	
	private String senderId;
	private String senderName;
	private String[] custNoIFList;
	private String documentType;

	private String sbLoginId;
	private String sbUserId;
	
	public String getReqName(){
		return reqName;
	}

	public  String getResponseType() {
		return responseType;
	}
	
	public  String getVersionInformation() {
		return versionInformation;
	}
	
	public  String getUserBusinessAccountTypeCode() {
		return userBusinessAccountTypeCode;
	}
	
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String[] getCustNoIFList() {
		return custNoIFList;
	}
	public void setCustNoIFList(String[] custNoIFList) {
		this.custNoIFList = custNoIFList;
	}

	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getTypeCode() {
		return typeCode;
	}
	
	public String getSbLoginId() {
		return sbLoginId;
	}

	public void setSbLoginId(String sbLoginId) {
		this.sbLoginId = sbLoginId;
	}

	public String getSbUserId() {
		return sbUserId;
	}

	public void setSbUserId(String sbUserId) {
		this.sbUserId = sbUserId;
	}

	
	
	
	
}
