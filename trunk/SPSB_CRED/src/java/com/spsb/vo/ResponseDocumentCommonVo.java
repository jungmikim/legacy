package com.spsb.vo;

/**
 * <pre>
 * message 결과 Vo Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 2.
 * @version 1.0
 */
public class ResponseDocumentCommonVo {
	//응답 문서 정보 영역
	private String Id;						//메시지 고유의 식별 번호		
	private String ReferenceID;				//요청 메시지의 식별 번호
	private String RDocumentTypeCode;		//유형 코드. 고정값			
	private String VersionInformation;		//문서 버전. 고정값: 1.0.0
	private String RDDescription;			//문서 비고 내역
	
	//응답 결과 영역
	private String RRDTypeCode;				//처리 성공, 실패 여부. 0: 실패, 1: 성공
	private String ResponseTypeCode;		//message 처리 결과 코드 ( 성공 : SCMN01, 이외 실패 )
	private String RRDDescription;			//접수 결과 내용, 사유, 기타 정보 
	/**
	 * @return the id
	 */
	public String getId() {
		return Id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		Id = id;
	}
	/**
	 * @return the referenceID
	 */
	public String getReferenceID() {
		return ReferenceID;
	}
	/**
	 * @param referenceID the referenceID to set
	 */
	public void setReferenceID(String referenceID) {
		ReferenceID = referenceID;
	}
	/**
	 * @return the rDocumentTypeCode
	 */
	public String getRDocumentTypeCode() {
		return RDocumentTypeCode;
	}
	/**
	 * @param rDocumentTypeCode the rDocumentTypeCode to set
	 */
	public void setRDocumentTypeCode(String rDocumentTypeCode) {
		RDocumentTypeCode = rDocumentTypeCode;
	}
	/**
	 * @return the versionInformation
	 */
	public String getVersionInformation() {
		return VersionInformation;
	}
	/**
	 * @param versionInformation the versionInformation to set
	 */
	public void setVersionInformation(String versionInformation) {
		VersionInformation = versionInformation;
	}
	/**
	 * @return the rDDescription
	 */
	public String getRDDescription() {
		return RDDescription;
	}
	/**
	 * @param rDDescription the rDDescription to set
	 */
	public void setRDDescription(String rDDescription) {
		RDDescription = rDDescription;
	}
	/**
	 * @return the rRDTypeCode
	 */
	public String getRRDTypeCode() {
		return RRDTypeCode;
	}
	/**
	 * @param rRDTypeCode the rRDTypeCode to set
	 */
	public void setRRDTypeCode(String rRDTypeCode) {
		RRDTypeCode = rRDTypeCode;
	}
	/**
	 * @return the responseTypeCode
	 */
	public String getResponseTypeCode() {
		return ResponseTypeCode;
	}
	/**
	 * @param responseTypeCode the responseTypeCode to set
	 */
	public void setResponseTypeCode(String responseTypeCode) {
		ResponseTypeCode = responseTypeCode;
	}
	/**
	 * @return the rRDTDescription
	 */
	public String getRRDDescription() {
		return RRDDescription;
	}
	/**
	 * @param rRDTDescription the rRDTDescription to set
	 */
	public void setRRDDescription(String rRDDescription) {
		RRDDescription = rRDDescription;
	}
	
	

}
