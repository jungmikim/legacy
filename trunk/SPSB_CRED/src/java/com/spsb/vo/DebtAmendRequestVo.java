package com.spsb.vo;

/**
 * 
 * <pre>
 *  채무불이행  등록 변경 요청서  request Vo Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 12.
 * @version 1.0
 */
public class DebtAmendRequestVo {

	private String docCd;
	private String updDt;
	private String updId;
	private String stat;
	private String modStat;
	private String usrNm;
	private String rmkTxt;
	private String modDebtAmt;
	private String DebtAmt;
	
	public String getDocCd() {
		return docCd;
	}
	public void setDocCd(String docCd) {
		this.docCd = docCd;
	}
	public String getUpdDt() {
		return updDt;
	}
	public void setUpdDt(String updDt) {
		this.updDt = updDt;
	}
	public String getUpdId() {
		return updId;
	}
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	public String getStat() {
		return stat;
	}
	public void setStat(String stat) {
		this.stat = stat;
	}
	public String getModStat() {
		return modStat;
	}
	public void setModStat(String modStat) {
		this.modStat = modStat;
	}
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}
	public String getRmkTxt() {
		return rmkTxt;
	}
	public void setRmkTxt(String rmkTxt) {
		this.rmkTxt = rmkTxt;
	}
	public String getModDebtAmt() {
		return modDebtAmt;
	}
	public void setModDebtAmt(String modDebtAmt) {
		this.modDebtAmt = modDebtAmt;
	}
	public String getDebtAmt() {
		return DebtAmt;
	}
	public void setDebtAmt(String debtAmt) {
		DebtAmt = debtAmt;
	}
	

}
