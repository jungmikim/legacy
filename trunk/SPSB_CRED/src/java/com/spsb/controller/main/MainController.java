package com.spsb.controller.main;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.parent.SuperController;

/**
 * <pre>
 * main Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 21.
 * @version 1.0
 */
@Controller
public class MainController extends SuperController{

	
	/**
	 * <pre>
	 * main Controller method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 21.
	 * @version 1.0
	 * @param request
	 * @return mav
	 */
	@RequestMapping(value = "/main.do")
	public ModelAndView goMain(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/main/main");
		return mav;
	}
	
}
