/**
 * 
 */
package com.spsb.controller.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.parent.SuperController;
import com.spsb.common.session.SessionManager;
import com.spsb.common.util.CommonDesForSBUtil;
import com.spsb.service.login.LoginService;

/**
 * <pre>
 * 로그인 Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 8. 26.
 * @version 1.0
 */
@Controller
public class LoginController extends SuperController{

	@Autowired
	private LoginService loginService;
	
	
	/**
	 * <pre>
	 * 로그인 화면
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/login/loginForm.do")
	public ModelAndView loginForm(HttpServletRequest request,
			@RequestParam(value = "loginId", required = false, defaultValue = "") String loginId ){
		ModelAndView mav = new ModelAndView();
		SessionManager sessionManager = SessionManager.getInstance();
		boolean isLogined = sessionManager.isLogined(request);
		if(isLogined){
			mav.setView(new RedirectView(request.getContextPath() + "/main.do"));
		}else{
			mav.setViewName("login/loginForm");
			/*mav.setViewName("login/loginSBForm");*/ 
			mav.addObject("loginId", loginId);
		}
		return mav;
	}
	
	/**
	 * <pre>
	 * 로그인 화면
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/login/loginFormAdm.do")
	public ModelAndView loginFormAdm(HttpServletRequest request,
			@RequestParam(value = "loginId", required = false, defaultValue = "") String loginId ){
		ModelAndView mav = new ModelAndView();
		SessionManager sessionManager = SessionManager.getInstance();
		boolean isLogined = sessionManager.isLogined(request);
		if(isLogined){
			mav.setView(new RedirectView(request.getContextPath() + "/main.do"));
		}else{
			mav.setViewName("login/loginForm");
			mav.addObject("loginId", loginId);
		}
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인회원 로그인 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param request
	 * @param loginId : 로그인 식별자
	 * @param loginPw : 로그인 비밀번호
	 * @return
	 */
	@RequestMapping(value = "/login/loginUserAjax.do", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView loginUserAjax(HttpServletRequest request,
			@RequestParam(value = "loginId", required = true) String loginId,
			@RequestParam(value = "loginPw", required = true) String loginPw ){
		ModelAndView mav = new ModelAndView("jsonView");
		SBox result = loginService.getLoginMemberUser(loginId, loginPw);
		
		SBox jsonResult = new SBox();
		if(result != null && EUserErrorCode.SUCCESS.equals(result.get("result"))){
			SessionManager.getInstance(request, result, sessionMaxInactiveTime);
			jsonResult.set("isSuccess", true);
		}else{
			jsonResult.set("isSuccess", false);
		}
		EUserErrorCode resultCode = result.get("result");
		jsonResult.set("message", resultCode.getDesc());
		mav.addObject("result", jsonResult);
		return mav;
	}
	
	/**
	 * <pre>
	 * 로그아웃 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login/logout.do")
	public ModelAndView logout(HttpServletRequest request ){
		ModelAndView mav = new ModelAndView();
		SessionManager sessionManager = SessionManager.getInstance();
		sessionManager.logout(request);
		mav.setView(new RedirectView(request.getContextPath() + "/login/loginForm.do"));
		return mav;
	}
	
	/**
	 * <pre>
	 * 권한 없음  Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 6. 17.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/login/notGrant.do")
	public ModelAndView notGrant(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login/notGrant");
		return mav;
	}
	
	/**
	 * <pre>
	 * 채무불이행권한 없음  Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 6. 17.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/login/notDebtGrant.do")
	public ModelAndView notDebtGrant(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login/notDebtGrant");
		return mav;
	}
	
	/**
	 * <pre>
	 * 게스트회원 로그인
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 2. 7.
	 * @version 1.0
	 * @param key1
	 * @param key2
	 * @return
	 */
	/*@RequestMapping(value = "/login/loginForGuest.do")
	public ModelAndView loginForGuest(HttpServletRequest request,
			@RequestParam(value = "key1", required = true) String key1,
			@RequestParam(value = "key2", required = true) String key2 ){
		SessionManager sessionManager = SessionManager.getInstance();
		sessionManager.logout(request);
		EUserErrorCode message = EUserErrorCode.GUEST_INCORRECT_URL;
		String redirectUrl = "/login/notGrantForGuest.do?message=" + message.getCode();
		if(key2.equals("debn")){
			try {
				String key = CommonDesUtil.decrypt(key1);
				String[] keyArray = key.split(",");
				if(keyArray.length == 2){
					Integer tmpUsrId = Integer.parseInt(keyArray[0]);
					SBox result = loginService.getLoginTempUser(tmpUsrId, keyArray[1]);
					
					if(EUserErrorCode.SUCCESS.equals(result.get("result"))){
						//처음 Guest 로그인일경우
						if(result.get("ST_LOGIN_DT") == null && "I".equals(result.get("STAT"))){
							loginService.modifyTmpUsrStLoginInfo(tmpUsrId);
							//SBox tempUserInfo = dgDebentureService.getTmpUsr(tmpUsrId);
							//SessionManager.getInstanceForGuest(request, tempUserInfo, sessionMaxInactiveTime);
							redirectUrl = "/dgdebn/addDgDebentureForm.do";
						}else{
							Date loginDt = result.get("ST_LOGIN_DT"); //로그인 시간	
							
							Calendar loginDateAfter = Calendar.getInstance();
							loginDateAfter.setTime(loginDt);
							loginDateAfter.add(Calendar.DATE, 1);
					
							Calendar now = Calendar.getInstance();
							now.setTimeInMillis(System.currentTimeMillis());
						
							if(now.getTimeInMillis() < loginDateAfter.getTimeInMillis()){ // 만료체크
								SessionManager.getInstanceForGuest(request, result, sessionMaxInactiveTime);
								redirectUrl = "/dgdebn/addDgDebentureForm.do";
							}else{	// 만료시간이면
								//dgDebentureService.modifyTmpUsrStat(tmpUsrId, "E"); // 상태를 만료(E)로 변경
								redirectUrl = "/login/notGrantForGuest.do?message=" + (EUserErrorCode.GUEST_EXPIRED).getCode();
							}
						}
					}else{
						redirectUrl = "/login/notGrantForGuest.do?message=" + ((EUserErrorCode)result.get("result")).getCode();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		RedirectView rv = new RedirectView(request.getContextPath() + redirectUrl);
		rv.setExposeModelAttributes(false);
		ModelAndView mav = new ModelAndView(rv);
		return mav;
	}*/
	
	/**
	 * <pre>
	 * 게스트회원 권한없거나 key오류
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 18.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	/*@RequestMapping(value = "/login/notGrantForGuest.do")
	public ModelAndView notGrantForGuest(HttpServletRequest request,
			@ModelAttribute("initBoxs") SBox sBox){
		EUserErrorCode messageCode = EUserErrorCode.search(sBox.getString("message"));
		if(messageCode == null){
			messageCode = EUserErrorCode.FAIL;
		}
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login/notGrantForGuest");
		mav.addObject("message", messageCode.getDesc());
		return mav;
	}*/
	
	/**
	 * <pre>
	 * 스마트빌 개인회원 로그인 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 4. 18.
	 * @version 1.0
	 * @param request
	 * @param loginId : 로그인 식별자
	 * @return
	 */
	/*@RequestMapping(value = "/login/loginSBUser.do")
	public ModelAndView loginPortalUser(HttpServletRequest request,
			@RequestParam(value = "loginId", required = true) String loginId){
		ModelAndView mav = new ModelAndView();
		SBox result = loginService.getLoginMemberUser(loginId);
		if(result == null){
			mav.addObject("message", EUserErrorCode.FAIL.getDesc());
			mav.setViewName("/login/notLogin");
		}else{
			EUserErrorCode resultCode = result.get("result");
			if(EUserErrorCode.SUCCESS.equals(resultCode)){
				SessionManager.getInstance(request, result, sessionMaxInactiveTime);
				RedirectView rv = new RedirectView(request.getContextPath() + "/main.do");
				rv.setExposeModelAttributes(false);
				mav.setView(rv);
			}else if(EUserErrorCode.LOGIN_NOT_EXIST.equals(resultCode)){
				//회원가입
			}else{
				mav.addObject("message", resultCode == null ? EUserErrorCode.FAIL.getDesc() : resultCode.getDesc());
				mav.setViewName("/login/notLogin");
			}
		}
		return mav;
	}*/
	
	
	/**
	 * 
	 * <pre>
	 * 스바트빌 로그인  정보를 이용 스마트채권의 session 생성
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 13.
	 * @version 1.0
	 * @param request
	 * @param value=loginId 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/login/loginUserForLegacy.do")
	public ModelAndView loginUserForSB(HttpServletRequest request,
									   @RequestParam(value = "value", required = true) String value) throws Exception {
		ModelAndView mav = new ModelAndView();
		String resultMsg = "스마트비즈 for Legacy 자동로그인  실패";
		mav.setViewName("/user/interfaceLoginErrorurl");
		try{
			if( value==null||"".equals(value)){
				mav.addObject("resultMsg", resultMsg.toString());
				return mav;
			}
			//String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = value.trim().split("&");//encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++){
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			if(loginId==null||loginId.isEmpty()){
				RedirectView rv = new RedirectView(request.getContextPath() + "/main.do");
				rv.setExposeModelAttributes(false);
				mav.setView(rv);
				resultMsg = "스마트비즈 for Legacy 자동로그인  성공";
			}
			String redirectUrl  = paramSBox.getString("REDIRECT_URL");
			String param = loginId + "|" + "|" + redirectUrl;
			log.i(getLogMessage("redirectUrl", "param", param));
			
			SBox result = loginService.getLoginMemberUser(loginId);
			if(result == null){
				mav.addObject("message", EUserErrorCode.FAIL.getDesc());
				resultMsg = "스마트비즈 for Legacy 자동로그인  실패";
			}else{
				EUserErrorCode resultCode = result.get("result");
				if(EUserErrorCode.SUCCESS.equals(resultCode)){
					resultMsg = "스마트비즈 for Legacy 자동로그인  성공";
					SessionManager.getInstance(request, result, sessionMaxInactiveTime);
					RedirectView rv = new RedirectView(request.getContextPath() + redirectUrl);
					rv.setExposeModelAttributes(false);
					mav.setView(rv);
				}else if(EUserErrorCode.LOGIN_NOT_EXIST.equals(resultCode)){
					resultMsg = "스마트비즈 for Legacy 자동로그인  실패";
				}else{
					mav.addObject("message", resultCode == null ? EUserErrorCode.FAIL.getDesc() : resultCode.getDesc());
					//mav.setViewName("/login/notLogin");
					resultMsg = "스마트비즈 for Legacy 자동로그인  실패";
				}
			}
		}catch(Exception e) {
			mav.setViewName("/user/interfaceNoteurl");
			e.printStackTrace();
		};
		
		mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}
}

