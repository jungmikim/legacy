package com.spsb.controller.scheduling;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Controller;


/**
 * <pre>
 * 조기경보 거래처 관리페이지에서 거래처 삭제에따른 Legacy 거래처 동기화
 * </pre>
 * @author JUNG MI KIM
 * @since 2014. 7. 18.
 * @version 1.0
 */
@Controller
public class RunSchedulingJob extends QuartzJobBean {
	
	private CustomerSchedulingManager customerSchedulingManager;
	
	public void setCustomerSchedulingManager(CustomerSchedulingManager customerSchedulingManager) {
		this.customerSchedulingManager = customerSchedulingManager;
	}
	
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException {
		customerSchedulingManager.setFtpDownloadTask();

		 
	
		
		
	}
}



