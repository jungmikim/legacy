package com.spsb.controller.scheduling;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;

import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.parent.SuperService;
import com.spsb.service.customer.CustomerService;


/**
 * <pre>
 *   
 * </pre>
 * @author JUNG MI KIM
 * @since 2014. 8. 4.
 * @version 1.0
 */
public class CustomerSchedulingManager extends SuperService{
	
	
	@Autowired
	private CustomerService customerService;
	
	
	/**
	 * <pre>
	 * 조기경보 거래처 관리페이지에서 거래처 삭제에따른 Legacy 거래처 동기화
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 20.
	 * @version 1.0
	 */
	public void setFtpDownloadTask(){
		
		try {
			Calendar nowTime = Calendar.getInstance();
	        SimpleDateFormat sd = new SimpleDateFormat("[ yyyy-MM-dd HH:mm:ss ]");
	        String strNowTime = sd.format(nowTime.getTime());
	        log.i(getLogMessage("##############################", strNowTime , " Time to start customerSync Scheduling. ##############################"));
	        customerService.checkCustSyncForIF();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("50012").getErrMsg() + "]");
		}
		
	}
	
	
	
   
}
