package com.spsb.controller.config;


import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonCode;
import com.spsb.service.config.ConfigService;
import com.spsb.service.debt.DebtService;
import com.spsb.service.message.InterfaceMessageService;

/**
 * <pre>
 *   환경설정 관리 Controller Class
 * </pre>
 * 
 * @author KIM GA EUN
 * @since 2015. 06. 15.
 * @version 1.0
 */
@Controller
public class ConfigManagementController extends SuperController {
	
	@Autowired
	private ConfigService configService;
	
	@Autowired
	private DebtService debtService;
	
	@Autowired
	private InterfaceMessageService interfaceMessageService;

	/**
	 * <pre>
	 * 사용자권한 및 회원정보
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/config/getUserInfoList.do")
	public ModelAndView getUserInfoList(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("result", configService.getUserInfoList(sBox));
		mav.addObject("sBox", sBox);
		mav.setViewName("/config/getUserInfoList");
		return mav;
	}
	
	/**
	 * <pre>
	 * 사용자 추가 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/config/addUserPopup.do")
	public ModelAndView addUserPopup(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("sBox", sBox);
		mav.setViewName("/config/addUserPopup");
		return mav;
	}
	
	/**
	 * <pre>
	 *  사용자 추가 실행 로직
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param request
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/config/addJoinMemberUser.do")
	@ResponseBody
	public ModelAndView userAddJoinMemberUser(@ModelAttribute("initBoxs") SBox sBox){
		ModelAndView mav = new ModelAndView("jsonview");
		mav.addObject("result",configService.addJoinMemberUser(sBox));
		mav.addObject("sBox", sBox);
		return mav;
	}
	
	
	
	/**
	 * <pre>
	 * 스마트빌 아이디 가져오기 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param sBox
	 * @param usrId
	 * @return
	 */
	@RequestMapping(value = "/config/getBizNoSearchPopup.do")
	public ModelAndView getBizNoSearchPopup(@ModelAttribute("initBoxs") SBox sBox
										   ,@RequestParam(value = "mbrId", required = true) int mbrId) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("sBox", sBox);
		try {
			sBox.set("mbrId",mbrId);
			sBox.set("usrNoFromWeb", sBox.getString("sessionUsrNo"));
			mav.addObject("userResultList", interfaceMessageService.getSyncUserListFromMiddleServer(sBox));
			mav.setViewName("/debt/getBizNoSearchPopup");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mav;
	}
	
	
	/**
	 * <pre>
	 * 사용자 수정 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/config/modifyUserPopup.do")
	public ModelAndView modifyUserPopup(@ModelAttribute("initBoxs") SBox sBox
			                            ,@RequestParam(value = "mbrId", required = true) int mbrId) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("mbrId", mbrId);
		mav.addObject("phoneCodeList", CommonCode.phoneCodeList);
		mav.addObject("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		mav.addObject("result", configService.getUser(mbrId));
		mav.addObject("sBox", sBox);
		mav.setViewName("/config/modifyUserPopup");

		return mav;
	}
	
	/**
	 * <pre>
	 *   회원 삭제 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2015. 7. 07.
	 * @version 1.0
	 * @param sBox  mbrIdList : 삭제 대상 회원 순번, sessionCompUsrId : 기업회원 순번, sessionUsrId : 개인회원 순번
	 * @return
	 */
	//TODO
	@RequestMapping(value = "/config/removeUser.do")
	@ResponseBody
	public void removeUser(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = configService.removeUser(sBox.getString("mbrIdList"), sBox.getInt("sessionUsrId"));
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 *   회원  수정 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2015. 7. 07.
	 * @version 1.0
	 * @param sBox
	 *            mbrId : 수정 대상 회원 순번, sessionUsrId : 개인회원 순번, compPersonalNo : 사번, telNo : 전화번호, email : 이메일
	 * @return
	 */
	@RequestMapping(value = "/config/updateUser.do")
	@ResponseBody
	public ModelAndView updateUser(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView("jsonview");
		mav.addObject("result",configService.modifyUser(sBox));
		mav.addObject("sBox", sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 사용자 추가 회원아이디 중복확인 검사
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 27.
	 * @version 1.0
	 * @param loginId
	 * @return
	 */
	@RequestMapping(value = "/config/isDuplicationLoginIdAjax.do")
	@ResponseBody
	public ModelAndView isDuplicationLoginIdAjax(@RequestParam(value = "loginId", required = true) String loginId) {
		ModelAndView mav = new ModelAndView("jsonView");
		boolean result = configService.isDuplicationMemberUser(loginId);
		mav.addObject("result", result);
		return mav;
	}
	
	/**
	 * <pre>
	 * 스마트빌회원정보 이름으로 검색 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 31.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/config/getBizNoSearchPopupAjax.do")
	@ResponseBody
	public ModelAndView getCustomerAjax(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", configService.getBizNoSearchName(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 	스마트 회원 유저 삭제 여부 체크 controller
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015.09.03
	 */	
	@RequestMapping(value = "/config/checkAdminDeleteYN.do")
	@ResponseBody
	public void checkAdminDeleteYN(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = configService.checkAdminDeleteYN(sBox);
		System.out.println(resultBox.toString());
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 	스마트 회원 유저  여부 체크 controller
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015.09.03
	 */	
	@RequestMapping(value = "/config/checkAdminYN.do")
	@ResponseBody
	public void checkAdminYN(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = configService.checkAdminYN(sBox);
		System.out.println(resultBox.toString());
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	////////////////////////////////////TEST 확인후 지워야함////////////////////////////////////////////////////////////////////
	/**
	 * <pre>
	 * TEST 확인후 지워야함
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 29.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/test/loginForLegacyTest.do")
	public ModelAndView loginForLegacyTest(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("/test/loginForLegacyTest");
		//mav.addObject("result",configService.removeUser(sBox.getString("mbrIdList"), sBox.getInt("sessionUsrId")));
		mav.addObject("sBox", sBox);
		return mav;
	}
	////////////////////////////////////TEST 확인후 지워야함////////////////////////////////////////////////////////////////////
	
	/**
	 * <pre>
	 * 라이선스 관리
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/getLicenseList.do")
	public ModelAndView getLicenseList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/getLicenseList");
		mav.addObject("sBox", sBox);

		return mav;
	} */
	
	/**
	 * <pre>
	 * 서비스 코드 추가 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return

	@RequestMapping(value = "/config/addServiceCodePopup.do")
	public ModelAndView addServiceCodePopup(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/addServiceCodePopup");
		mav.addObject("sBox", sBox);

		return mav;
	}	 */
	
	/**
	 * <pre>
	 * 서비스 코드 수정 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/modifyServiceCodePopup.do")
	public ModelAndView modifyServiceCodePopup(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/modifyServiceCodePopup");
		mav.addObject("sBox", sBox);

		return mav;
	} */
	
	/**
	 * <pre>
	 * XML 메시지 관리
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/getXmlMessageList.do")
	public ModelAndView getXmlMessageList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/getXmlMessageList");
		mav.addObject("sBox", sBox);

		return mav;
	} */
	
	/**
	 * <pre>
	 * XML 메시지 추가 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/addXmlMessagePopup.do")
	public ModelAndView addXmlMessagePopup(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/addXmlMessagePopup");
		mav.addObject("sBox", sBox);

		return mav;
	} */
	
	/**
	 * <pre>
	 * XML 메시지 수정 팝업
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/modifyXmlMessagePopup.do")
	public ModelAndView modifyXmlMessagePopup(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/modifyXmlMessagePopup");
		mav.addObject("sBox", sBox);

		return mav;
	} */
	
	/**
	 * <pre>
	 * 설정파일 관리
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@RequestMapping(value = "/config/getSettingFileList.do")
	public ModelAndView getSettingFileList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("config/getSettingFileList");
		mav.addObject("sBox", sBox);

		return mav;
	} */

	
	
	
}
