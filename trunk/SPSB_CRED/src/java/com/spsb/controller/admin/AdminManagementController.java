package com.spsb.controller.admin;


import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonCode;
import com.spsb.service.admin.AdminService;

/**
 * <pre>
 *   환경설정 관리 Controller Class
 * </pre>
 * 
 * @author KIM GA EUN
 * @since 2015. 06. 15.
 * @version 1.0
 */
@Controller
public class AdminManagementController extends SuperController {
	
	@Autowired
	private AdminService adminService;


	/**
	 * <pre>
	 * 연동업체 리스트 조회 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getCompAndPrdInfoForm.do")
	public ModelAndView getCompAndPrdInfoForm(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		SBox compList = adminService.getCompMngList(sBox);
		mav.addObject("result", compList);
		mav.addObject("sBox", sBox);
		mav.setViewName("/admin/getCompAndPrdInfoForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 연동업체 등록
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/setCompUser.do")
	@ResponseBody
	public void setCompUser(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.addCompUser(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 연동업체 별 상품 등록 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/setCompProduct.do")
	@ResponseBody
	public void setCompProduct(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.addCompProduct(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 연동업체별 사업장 폼 조회  
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getCustForCompForm.do")
	public ModelAndView setCustForComp(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		
		//연동업체 리스트 조회
		mav.addObject("result", adminService.getCompMngList(sBox));
		mav.addObject("sBox", sBox);
		mav.setViewName("/admin/getCustForCompForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 연동업체 별 사업장 관리 등록
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/setCustForComp.do")
	@ResponseBody
	public void setCustForComp(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.addCustForComp(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 사업장 별 회원 등록
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/setMbrInfo.do")
	@ResponseBody
	public void setMbrInfo(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.addMbrInfo(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	
	/**
	 * <pre>
	 * 회원정보 폭 조회  
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getMbrForm.do")
	public ModelAndView getMbrForm(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		
		//연동업체 리스트 조회
		mav.addObject("result", adminService.getCompMngList(sBox));
		//mav.addObject("usr", adminService.getCompUsrList(sBox));
		mav.addObject("sBox", sBox);
		mav.setViewName("/admin/getMbrForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 선택한 연동업체에 대한 사업장 정보조회 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getCompUsrListAboutCompMng.do")
	@ResponseBody
	public void getCompUsrListAboutCompMng(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.getCompUsrListAboutCompMng(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 선택한 연동업체에 대한 사업장 정보조회 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getMbrListAboutCompUsr.do")
	@ResponseBody
	public void getMbrListAboutCompUsr(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.getMbrListAboutCompUsr(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 *  회원에 대한 권한 조회 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getGrnListAboutMbrList.do")
	@ResponseBody
	public void getGrnListAboutMbrList(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.getGrnListAboutMbrList(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 *  회원에 대한 권한 조회 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getPrdListAboutCompMng.do")
	@ResponseBody
	public void getPrdListAboutCompMng(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.getPrdListAboutCompMng(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 회원정보 폭 조회  
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/getGrnInfoForm.do")
	public ModelAndView getGrnInfoForm(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		
		//연동업체 리스트 조회
		mav.addObject("result", adminService.getCompMngList(sBox));
		//mav.addObject("usr", adminService.getCompUsrList(sBox));
		mav.addObject("sBox", sBox);
		mav.setViewName("/admin/getGrnInfoForm");
		return mav;
	}
	
	/**
	 * <pre>
	 * 권한등록 
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016. 6. 20.
	 */
	@RequestMapping(value = "/admin/setGrnInfo.do")
	@ResponseBody
	public void setGrnInfo(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {

		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();		
		SBox resultBox = adminService.addGrnInfo(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
}
