package com.spsb.controller.zip;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.parent.SuperController;
import com.spsb.service.zip.ZipManageService;

/**
 * <pre>
 * 우편번호 찾기 Controller Class
 * </pre>
 * @author KIM GA EUN
 * @since 2014. 01. 07.
 * @version 1.0
 */
@Controller
public class ZipManageController extends SuperController{
	
	@Autowired
	private ZipManageService zipManageService;
	
	/**
	 * <pre>
	 * 	우편번호 찾기 팝업 페이지 호출 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 *
	 * 	
	 * @return
	 */
	@RequestMapping(value = "/zip/zipSearchPopup.do")
	public ModelAndView getZipSearchPopup(HttpServletRequest request,
			@RequestParam(value = "otherFunction", required = false, defaultValue = "") String otherFunction){
		ModelAndView mav = new ModelAndView();
		SBoxList<SBox> SidoList =  zipManageService.getSidoList();
		mav.addObject("SidoList", SidoList);
		mav.addObject("otherFunction", otherFunction);
		mav.setViewName("/zip/zipSearchPopup");
		return mav;
	}
	
	/**
	 * <pre>
	 * 	우편번호 군구 리스트 호출 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도
	 * 	
	 * @return
	 */
	@RequestMapping(value = "/zip/getGunguForWebList.do")
	@ResponseBody
	public ModelAndView getGunguForWebList(@RequestParam(value = "sido", required = true) String sido
			, HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET,POST");
		response.setHeader("Access-Control-Max-Age", "360");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		SBoxList<SBox> GunguList =  zipManageService.getGunguForWebList(sido);
		ModelAndView mav = new ModelAndView("jsonview");
		mav.addObject("GunguList", GunguList);
		return mav;
	}
	
	/**
	 * <pre>
	 * 	도로명 주소 + 건물번호 우편번호 검색 리스트 호출 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, rdNm : 도로명 주소, mnNo : 건물번호 본번, subNo : 건물번호 부번
	 * 	
	 * @return
	 */
	@RequestMapping(value = "/zip/getAddressByStreetSearchForWebList.do")
	public ModelAndView getAddressByStreetSearchForWebList(@ModelAttribute("initBoxs") SBox sBox
			,@RequestParam(value = "sido", required = true) String sido
			,@RequestParam(value = "gungu", required = true) String gungu
			,@RequestParam(value = "rdNm", required = true) String rdNm
			,@RequestParam(value = "mnNo", required = true) String mnNo
			,@RequestParam(value = "subNo", required = true) String subNo){
		
		sBox.set("sido", sido);
		sBox.set("gungu", gungu);
		sBox.set("rdNm", rdNm);
		sBox.set("mnNo", mnNo);
		sBox.set("subNo", subNo);
		
		SBoxList<SBox> addressList = zipManageService.getAddressByStreetSearchForWebList(sBox);

		ModelAndView mav = new ModelAndView();

		int addressSize = addressList.size();
		mav.addObject("addressSize",addressSize);
		mav.addObject("addressList",addressList);
		mav.setViewName("/zip/getZipListAjax");
		return mav;
	}
	
	/**
	 * <pre>
	 * 	동(읍/면) + 지번 우편번호 검색 리스트 호출 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, dong : 동(읍/면), mnNo : 지번 본번, subNo : 지번 부번
	 * 	
	 * @return
	 */
	@RequestMapping(value = "/zip/getAddressByDongSearchForWebList.do")
	public ModelAndView getAddressByDongSearchForWebList(@ModelAttribute("initBoxs") SBox sBox
			,@RequestParam(value = "sido", required = true) String sido
			,@RequestParam(value = "gungu", required = true) String gungu
			,@RequestParam(value = "dong", required = true) String dong
			,@RequestParam(value = "mnNo", required = true) String mnNo
			,@RequestParam(value = "subNo", required = true) String subNo){
		
		sBox.set("sido", sido);
		sBox.set("gungu", gungu);
		sBox.set("dong", dong);
		sBox.set("mnNo", mnNo);
		sBox.set("subNo", subNo);
		
		SBoxList<SBox> addressList = zipManageService.getAddressByDongSearchForWebList(sBox);

		ModelAndView mav = new ModelAndView();

		int addressSize = addressList.size();
		mav.addObject("addressSize",addressSize);
		mav.addObject("addressList",addressList);
		mav.setViewName("/zip/getZipListAjax");
		return mav;
	}
	
	/**
	 * <pre>
	 * 	건물명(아파트명) 우편번호 검색 리스트 호출 Controller Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2014. 01. 09.
	 * @version 1.0
	 * @param sBox: sido : 시도, gungu : 군구, bldNm : 건물명(아파트명)
	 * 	
	 * @return
	 */
	@RequestMapping(value = "/zip/getAddressByBuildingSearchForWebList.do")
	public ModelAndView getAddressByBuildingSearchForWebList(@ModelAttribute("initBoxs") SBox sBox
			,@RequestParam(value = "sido", required = true) String sido
			,@RequestParam(value = "gungu", required = true) String gungu
			,@RequestParam(value = "bldNm", required = true) String bldNm){
		
		sBox.set("sido", sido);
		sBox.set("gungu", gungu);
		sBox.set("bldNm", bldNm);
		
		SBoxList<SBox> addressList = zipManageService.getAddressByBuildingSearchForWebList(sBox);

		ModelAndView mav = new ModelAndView();

		int addressSize = addressList.size();
		mav.addObject("addressSize",addressSize);
		mav.addObject("addressList",addressList);
		mav.setViewName("/zip/getZipListAjax");
		return mav;
	}
	
	

}
