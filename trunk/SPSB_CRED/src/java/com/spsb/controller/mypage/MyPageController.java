package com.spsb.controller.mypage;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.ESessionNameType;
import com.spsb.common.parent.SuperController;
import com.spsb.service.mypage.MyPageUserModifyService;


/**
 * <pre>
 * 마이페이지 Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
@Controller
public class MyPageController extends SuperController{

	@Autowired
	private MyPageUserModifyService myPageUserModifyService;
	
	//@Autowired
	//private MyPageEmployeeService myPageEmployeeService;
	
	//@Autowired
	//private MyPageHeadOfficeService myPageHeadOfficeService;
	
	//@Autowired
	//private MyPageBranchOfficeService myPageBranchOfficeService;
	
	/*@Autowired
	private MyPageBelongingService myPageBelongingService;*/
	
	/**
	 * <pre>
	 * 개인회원 마이페이지 - 메인화면 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 6.
	 * @version 1.0
	 * @param request
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/mypage/myPageMain.do")
	public ModelAndView goMyPageMain(HttpServletRequest request, @ModelAttribute("initBoxs") SBox sBox){
			ModelAndView mav = new ModelAndView();
			if ("Y".equals(sBox.get(ESessionNameType.ADM_YN.getName()))) {
				sBox.setIfEmpty("debnType", "debnAll"); // 사업자 검색 유형(전체채권)
				sBox.set("mbrUsrId", sBox.get(ESessionNameType.USR_ID.getName()));
			} else {
				/*if ((Boolean) sBox.get(ESessionNameType.IS_DEBN_SEARCH_GRN.getName())) {
					sBox.setIfEmpty("debnType", "debnAll"); 
				} else {
					sBox.setIfEmpty("debnType", "debnMine"); 
				}*/
			}
			/*if("Y".equals(sBox.get(ESessionNameType.ADM_YN.getName())) || ((Boolean)sBox.get(ESessionNameType.IS_DEBT_APPL_GRN.getName()))){
				//전체가 나와야함
			}else{
				if((Boolean)sBox.get(ESessionNameType.IS_DEBT_DOC_GRN.getName())){
					sBox.set("mbrList", sBox.get(ESessionNameType.USR_ID.getName()));
				}else{
					//전체가 나와야함
				}
			}*/
			mav.setViewName("/mypage/myPageMain");
			mav.addObject("resultCustCreditSummery", myPageUserModifyService.getCustCreditSummaryList(sBox));
			mav.addObject("resultCompanyList", myPageUserModifyService.getBelongingtoCompanyList(sBox));
			mav.addObject("summery",  myPageUserModifyService.getSummaryList(sBox));
			mav.addObject("sBox",sBox);	
			
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지 - 직원 관리(신규 요청 리스트 Ajax)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox -  sessionCompUsrId:기업회원 순번,sessionCustType:거래처 유형
	 * @return
	 */
	@RequestMapping(value = "/mypage/myPageMainByEmployeeAjax.do")
	public ModelAndView getEmployeeNewRequestList(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/myPageMainAjax");
		mav.addObject("viewType", "EmployeeInfo");
		String stat = "P"; //승인대기
		String mbrType ="J";
		sBox.set("num",1); //페이징 추
		sBox.set("stat",stat);
		sBox.set("mbrType",mbrType);
		mav.addObject("sBox",sBox);
		//mav.addObject("result", myPageEmployeeService.getEmployeeList(sBox));
		return mav;
	}
	
	
	/**
	 * <pre>
	 * 마이페이지  기업 - 본사관리 요청현황 (AJAX)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 28.
	 * @version 1.0
	 * @param sBox -  sessionCompUsrId:기업회원 순번,
	 * @return
	 */
	@RequestMapping(value = "/mypage/myPageMainByHeadAjax.do")
	public ModelAndView getHeadOfficeAjaxFromClose(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/myPageMainAjax");
		mav.addObject("viewType", "HeadOfficeInfo");
		String stat = "P"; //승인대기
		sBox.set("stat",stat);
		sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		sBox.set("num",1); //페이징 
		sBox.set("rowSize",100); //페이징 
		sBox.set("officeType","H"); //본지사 구분코드 
		//mav.addObject("result", myPageHeadOfficeService.getHeadOfficeList(sBox)); 
		return mav;
	}
	
	/**
	 * <pre>
	 * 마이페이지  기업 - 지사관리 요청현황 (AJAX)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 28.
	 * @version 1.0
	 * @param sBox -  sessionCompUsrId:기업회원 순번,
	 * @return
	 */
	@RequestMapping(value = "/mypage/myPageMainByBranchAjax.do")
	public ModelAndView getBranchOfficeAjaxFromClose(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/myPageMainAjax");
		mav.addObject("viewType", "BranchOfficeInfo");
		String stat = "P"; //승인대기
		sBox.set("stat",stat);
		sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
		sBox.set("num",1); //페이징 
		sBox.set("rowSize",100); //페이징 
		sBox.set("officeType","B"); //본지사 구분코드 
		//mav.addObject("result", myPageBranchOfficeService.getBranchOfficeList(sBox)); 
		return mav;
	}
	
	
	/**
	 * <pre>
	 * 개인회원  마이페이지 - 소속기업 현황  default 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 6.
	 * @version 1.0
	 * @param sBox , mbrId:기업회원멤버순번, preMbrId:기존 기업회원멤버순번
	 * @return 
	 */
	@RequestMapping(value = "/mypage/updateBelingingtoCompany.do")
	@ResponseBody
	public ModelAndView updateEmployeeJoin(HttpServletRequest request,
										   @ModelAttribute("initBoxs") SBox sBox,
			 							   @RequestParam(value = "mbrId", required = true) Integer mbrId,
			 							   @RequestParam(value = "preMbrId", required = true) Integer preMbrId) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox paramVO = new SBox();
		//이전 디폴트 사업자 N으로 변경
		sBox.set("mbrId", preMbrId);
		sBox.set("updId", sBox.get(ESessionNameType.USR_ID.getName()));
		sBox.set("deftYn","N");
		//mav.addObject("resultN",  myPageBelongingService.updateBelingingtoCompany(sBox));
		
		//현재 디폴트 사업자 Y으로 변경
		paramVO.set("mbrId", mbrId);
		paramVO.set("updId", sBox.get(ESessionNameType.USR_ID.getName()));
		paramVO.set("deftYn", "Y");
		//mav.addObject("resultY",  myPageBelongingService.updateBelingingtoCompany(paramVO));
		
		//현재 session 변경
		modifyUserCurrentSession(request, sBox);
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인 마이페이지 - 소속기업현황 페이지 Ajax 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 6.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/mypage/getBelongingtoCompanyAjax.do")
	public ModelAndView getBelongingtoCompanyAjax(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/mypage/myPageMainAjax");
		mav.addObject("viewType", "BelongingtoCompany");
		sBox.set("mbrUsrId", sBox.get(ESessionNameType.USR_ID.getName()));
		mav.addObject("result", myPageUserModifyService.getBelongingtoCompanyList(sBox));
		return mav;
	}
	
	
	
	/**
	 * <pre>
	 * EW Test 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 12. 2.
	 * @version 1.0
	 * @param sBox, mbrId,usrNm,telNo,email
	 * @return
	 */
	@RequestMapping(value = "/ewtest/ewtest.do")
	public ModelAndView modifyBelongtoCompanyListInfo(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		StringBuffer value = new StringBuffer();
		mav.setViewName("/ewtest/ewtest");
		
		value.append("CO2208758882");
		byte[] b = value.toString().getBytes();
		String encoderValue = new String(Base64.encodeBase64(b));
		try {
			encoderValue = URLEncoder.encode(encoderValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.e(e.getMessage());
		}
		System.out.println("================http://co.cretop.com/EW.do?val="+encoderValue);
		String ewUrl = "http://co.cretop.com/EW.do?val="+encoderValue;
		
		String ewUrl2 = "https://asp.cretop.com/ew/EWSTT17R0.pop?cmMenuId=0100000338&id=CO_CO2208758882&svCls=CO&baseDt=20141014";
		System.out.println("====ewUrl==========="+ewUrl);
		sBox.set("ewUrl", ewUrl);
		sBox.set("ewUrl2", ewUrl2);
		mav.addObject("sBox",sBox);	
		return mav;
	}
	
	
	@RequestMapping(value = "/ewtest/testPopup.do")
	public ModelAndView testPopup(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		StringBuffer value = new StringBuffer();
		mav.setViewName("/ewtest/testPopup");
		
		value.append("CO2208758882");
		byte[] b = value.toString().getBytes();
		String encoderValue = new String(Base64.encodeBase64(b));
		try {
			encoderValue = URLEncoder.encode(encoderValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.e(e.getMessage());
		}
		System.out.println("================http://co.cretop.com/EW.do?val="+encoderValue);
		String ewUrl = "http://co.cretop.com/EW.do?val="+encoderValue;
		
		String ewUrl2 = "https://asp.cretop.com/ew/EWSTT17R0.pop?cmMenuId=0100000338&id=CO_CO2208758882&svCls=CO&baseDt=20141014";
		System.out.println("====ewUrl==========="+ewUrl);
		sBox.set("ewUrl", ewUrl);
		sBox.set("ewUrl2", ewUrl2);
		mav.addObject("sBox",sBox);	
		return mav;
	}
	
	@RequestMapping(value = "/ewtest/samplePopup.do")
	public ModelAndView samplePopup(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView();
		StringBuffer value = new StringBuffer();
		mav.setViewName("/ewtest/samplePopup");
		
		value.append("CO2208758882");
		byte[] b = value.toString().getBytes();
		String encoderValue = new String(Base64.encodeBase64(b));
		try {
			encoderValue = URLEncoder.encode(encoderValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.e(e.getMessage());
		}
		System.out.println("================http://co.cretop.com/EW.do?val="+encoderValue);
		String ewUrl = "http://co.cretop.com/EW.do?val="+encoderValue;
		
		String ewUrl2 = "https://asp.cretop.com/ew/EWSTT17R0.pop?cmMenuId=0100000338&id=CO_CO2208758882&svCls=CO&baseDt=20141014";
		System.out.println("====ewUrl==========="+ewUrl);
		sBox.set("ewUrl", ewUrl);
		sBox.set("ewUrl2", ewUrl2);
		mav.addObject("sBox",sBox);	
		return mav;
	}
	
	
	/**
	 * 
	 * <pre>
	 * 스마트빌 연계 회사정보 변경
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param request
	 * @param loginId : 회원아이디, usrNm: 회사명,  ownNm : 대표자명, bizCond : 업태,  bizType : 업종, addr: 주소, postNo: 우편번호, telNo: 전화번호, corpNo: 법인등록번호 , 
	 * 		  email: 이메일,  mbNo: 전화번호, certInfo: 인증서정보
	 * @return
	 * @throws Exception 
	 */
	/*@RequestMapping(value = "/myPage/modifyCompInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView modifyCompInfoForSB(HttpServletRequest request,
											@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 채권회원 수정 완료";  
		log.i(getLogMessage("modifyCompInfoForSB", "value", value));
		try{
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("modifyCompInfoForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String compUsrNm  = paramSBox.getString("COMP_NM");
			String ownNm  = paramSBox.getString("OWN_NM");
			String bizCond  = paramSBox.getString("BIZ_COND");
			String bizType  = paramSBox.getString("BIZ_TYPE");
			String addr  = paramSBox.getString("ADDR_1");
			String postNo  = paramSBox.getString("POST_NO");
			String telNo  = paramSBox.getString("TEL_NO");
			String corpNo  = paramSBox.getString("CORP_NO");
			String email = paramSBox.getString("EMAIL");
			String mbNo  = paramSBox.getString("MB_NO");
			String certInfo  = paramSBox.getString("CERT_INFO");
			
			String param = loginId + "|" + "|" + compUsrNm + "|" + ownNm + "|" + bizCond + "|" + bizType + "|" +  addr + "|" 
						+ postNo + "|" + "|" + telNo + "|" + corpNo + "|" + email + "|" + mbNo + "|" + certInfo;
			log.i(getLogMessage("modifyCompInfoForSB", "param", param));
			
			boolean isAblePostNo = CommonUtil.checkFormatZipcode(postNo);
			
			boolean isAbleEmail = true;
			if(email != null && !email.equals("")){
				isAbleEmail = CommonUtil.checkFormatEmail(email);
			}
			boolean isAbleTelNo = true;
			if(telNo != null && !telNo.equals("")){
				isAbleTelNo = CommonUtil.checkFormatTelCode(telNo);
			}
			boolean isAbleMbNo = true;
			if(mbNo != null && !mbNo.equals("")){
				isAbleMbNo = CommonUtil.checkFormatPhoneCode(mbNo);
			}
			if(!(isAblePostNo &&  isAbleEmail && isAbleTelNo && isAbleMbNo)){
				log.i(getLogMessage("modifyCompInfoForSB", "isAblePostNo", String.valueOf(isAblePostNo)));
				log.i(getLogMessage("modifyCompInfoForSB", "isAbleEmail", String.valueOf(isAbleEmail)));
				log.i(getLogMessage("modifyCompInfoForSB", "isAbleTelNo", String.valueOf(isAbleTelNo)));
				log.i(getLogMessage("modifyCompInfoForSB", "isAbleMbNo", String.valueOf(isAbleMbNo)));
				EUserErrorCode result = EUserErrorCode.USERJOIN_PARAM_DATA_FAIL;
				//mav.setViewName("/error/msgError");
				mav.addObject("resultMsg", result.getDesc());
				return mav;
			}
			
			//LoginId를 통해서 조회 사용자정보를 가져와서 그걸로 업데이트 해야함
			
			//기업회원 정보
			SBox compSBox = new SBox();
			compSBox.set("loginId", loginId);
			compSBox.set("usrNm", compUsrNm);
			compSBox.set("ownNm", ownNm);
			compSBox.set("bizCond", bizCond);
			compSBox.set("bizType", bizType);
			compSBox.set("addr1", addr);
			compSBox.set("postNo", postNo);
			compSBox.set("telNo", telNo);
			compSBox.set("corpNo", corpNo);
			compSBox.set("email", email);
			compSBox.set("mbNo", mbNo);
			compSBox.set("certInfo", certInfo);
		
			myPageUserModifyService.modifyCompUserForSB(compSBox);
		}catch(Exception e) {
			resultMsg = "회사 정보 변경 실패하였습니다.";     
			e.printStackTrace();
		};
		 mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	
	
	/**
	 * 
	 * <pre>
	 * 스마트빌 연계 개인회원정보(개인회원, 기업회원멤버)변경
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param request
	 * @param loginId : 회원아이디, usrNm: 회사명,  deptNm : 부서명,  email: 이메일, telNo: 전화번호, mbNo: 전화번호, jobTlNm: 직위
	 * @return
	 * @throws Exception 
	 */
	/*@RequestMapping(value = "/myPage/modifyUserInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView modifyUserInfoForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 개인회원 수정 완료";
		log.i(getLogMessage("modifyUserInfoForSB", "value", value));
		
		try{
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("modifyUserInfoForSB", "encodeValue", encodeValue));
			
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String usrNm  = paramSBox.getString("USR_NM");
			String deptNm  = paramSBox.getString("DEPT_NM");
			String email = paramSBox.getString("EMAIL");
			String telNo  = paramSBox.getString("TEL_NO");
			String mbNo  = paramSBox.getString("MB_NO");
			String jobTlNm  = paramSBox.getString("JOB_TL_NM");
			
			String param = loginId + "|" + "|" + usrNm + "|" + deptNm + "|" + email + "|" + telNo + "|" + mbNo + "|" + jobTlNm;
			log.i(getLogMessage("modifyUserInfoForSB", "param", param));
			
			
			//기업회원 정보
			SBox compSBox = new SBox();
			compSBox.set("loginId", loginId);
			compSBox.set("usrNm", usrNm);
			compSBox.set("deptNm", deptNm);
			compSBox.set("telNo", telNo);
			compSBox.set("mbNo", mbNo);
			compSBox.set("email", email);
			compSBox.set("mbNo", mbNo);
			compSBox.set("jobTlNm", jobTlNm);
			
			myPageUserModifyService.modifyUserForSB(compSBox);
			
		}catch(Exception e) {
			resultMsg = "개인정보 변경 실패하였습니다.";     
			e.printStackTrace();
		};
		 mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	/**
	 * 
	 * <pre>
	 * 스마트빌 연계 개인회원정보(개인회원, 기업회원멤버)변경
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param request
	 * @param loginId : 회원아이디, usrNm: 회사명,  deptNm : 부서명,  email: 이메일, telNo: 전화번호, mbNo: 전화번호, jobTlNm: 직위
	 * @return
	 * @throws Exception 
	 */
	/*@RequestMapping(value = "/myPage/modifyAdmInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView modifyCompAdmInfoForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 기업관리자  수정 완료";  
		log.i(getLogMessage("modifyAdmInfoForSB", "value", value));
		try{
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("modifyAdmInfoForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String admId  = paramSBox.getString("ADM_ID");
			
			
			String param = loginId + "|" + admId ;
			log.i(getLogMessage("modifyAdmInfoForSB", "param", param));
			

			//기업회원 정보
			SBox compSBox = new SBox();
			compSBox.set("loginId", loginId);
			compSBox.set("admId", admId);
		
			myPageUserModifyService.modifyCompAdmInfoForSB(compSBox);
		}catch(Exception e) {
			resultMsg = "기업관리자 변경 실패하였습니다.";     
			e.printStackTrace();
		};
		 mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	/**
	 * <pre>
	 * 마이페이지 소속기업에 속한 사용자 정보변경 수정(이름/전화번호/이메일) 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 12. 2.
	 * @version 1.0
	 * @param sBox, mbrId,usrNm,telNo,email
	 * @return
	 */
	/*@RequestMapping(value = "/mypage/modifyBelongtoCompanyListInfo.do")
	@ResponseBody
	public ModelAndView modifyBelongtoCompanyListInfo(@ModelAttribute("initBoxs") SBox sBox,
													@RequestParam(value = "mbrId", required = true) int mbrId,
													@RequestParam(value = "usrNm", required = true) String usrNm,
													@RequestParam(value = "cellNo", required = true) String cellNo,
													@RequestParam(value = "email", required = true) String email) {
		ModelAndView mav = new ModelAndView("jsonview");
		SBox paramVo = new SBox();
		
		paramVo.set("mbrId", mbrId);
		paramVo.set("usrNm", usrNm);
		paramVo.set("cellNo", cellNo);
		paramVo.set("email", email);
		paramVo.set("updId", sBox.get(ESessionNameType.USR_ID.getName()));
		mav.addObject("result",  myPageUserModifyService.modifyBelongtoCompanyListInfo(paramVo));
		return mav;
	}*/
	
}
