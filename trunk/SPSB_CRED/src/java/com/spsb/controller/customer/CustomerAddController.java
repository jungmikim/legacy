package com.spsb.controller.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.spsb.common.parent.SuperController;
import com.spsb.service.customer.CustomerService;

/**
 * <pre>
 *   거래처 단건 등록 관리 Controller Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2013. 8. 21.
 * @version 1.0
 */
@Controller
public class CustomerAddController extends SuperController {

	@Autowired
	private CustomerService customerService;

	/**
	 * <pre>
	 * 거래처 단 건 등록 페이지 호출 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 26.
	 * @version 1.0
	 * @param
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCustomerForm.do")
	public ModelAndView getCustomerForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("customer/getCustomerForm");
		mav.addObject("result", customerService.getCustomerForm(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 단 건 등록 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 26.
	 * @version 1.0
	 * @param sessionCompUsrId
	 *            : 기업회원 순번,custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,ownNo1 : 대표자 주민등록번호 앞자리 ,ownNo2 : 대표자 주민등록번호 뒷자리 ,corpNo
	 *            : 법인등록번호 ,eMail1 : 이메일 앞자리 ,eMail2 : 이메일 뒷자리 ,postNo1 : 우편번호 앞자리 ,postNo2 : 우편번호 뒷자리 ,custAddr1 : 회사주소 앞자리 ,custAddr2 : 회사주소 뒷자리
	 *            ,bizType : 업종 ,bizCond : 업태 ,telNo1 : 전화 지역번호 ,telNo2 : 전화 앞자리 ,telNo3 : 전화 뒷자리 ,faxNo1 : 팩스 지역번호 ,faxNo2 : 팩스 앞자리 ,faxNo3 : 팩스 뒷자리
	 *            ,mbNo1 : 휴대폰 지역번호 ,mbNo2 : 휴대폰 앞자리 ,mbNo3 : 휴대폰 뒷자리 ,crdLtd : 여신한도 ,firstDebn : 초기미수금 ,initPayTermType : 초기 결제 조건 유형 
	 *            ,initPayTermDay : 초기 결제 조건 기준일 ,futurePayTermType : 차후 결제 조건 유형 ,futurePayTermDay : 차후 결제 조건 기준일 ,payTermDate : 결제 조건 적용 날짜
	 * @version 1.0
	 * @return mav 거래처 관리 리스트 JSP 페이지
	 */
	/*@RequestMapping(value = "/customer/addCustomer.do")
	public ModelAndView addCustomer(@ModelAttribute("initBoxs") SBox sBox) {
		
		SBox resultBox = customerService.addCustomer(sBox);
		ModelAndView mav = new ModelAndView("redirect:/customer/getCustomerList.do");
		mav.addObject("sBox", resultBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 거래쳐 중복 체크 페이지 호출 Controller Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 8. 27.
	 * @version 1.0
	 * @return
	 */
	/*@RequestMapping(value = "/customer/getCheckDuplicationCustomerForm.do")
	public ModelAndView getCheckDuplicationCustomerForm(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("customer/getCheckDuplicationCustomerForm");
		mav.addObject("sBox",sBox);
		return mav;
	}*/

	/**
	 * <pre>
	 * 거래처 중복 체크 / 개인 주민번호 중복체크 로직 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2015. 02. 10.
	 * @param custNo
	 *            : 거래처 사업자 등록증 or 개인 주민번호, sessionCompUsrId : 기업회원 순번, custType : 거래처유형(P or C)
	 * @return
	 */
	/*@RequestMapping(value = "/customer/checkDuplicationCustomer.do")
	@ResponseBody
	public ModelAndView checkDuplicationCustomer(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonview");

		SBox resultBox = customerService.checkDuplicationCustomer(sBox.getString("custNo"), sBox.getInt("sessionCompUsrId"), sBox.getString("custType"));

		mav.addObject("resultBox", resultBox);

		return mav;
	}*/

	/**
	 * <pre>
	 * 스마트빌 연계 채권 거래처 단 건 등록 Controller Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param : loginId:로그인아이디 , custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,corpNo : 법인등록번호 ,
	 * 			post: 우편번호  ,addr1 :회사주소, bizType : 업종 ,bizCond : 업태 ,telNo: 전화  ,faxNo : 팩스, mbNo : 휴대폰 ,email : 이메일 
	 * @version 1.0
	 * @return mav 
	 */
	
	/*@RequestMapping(value = "/customer/addCustInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView addCustomerForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 연계 신규 거래처 등록 완료";  
		log.i(getLogMessage("addCustinfoForSB", "value", value));
		try{
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("addCustinfoForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String custNm  = paramSBox.getString("CUST_NM");
			String custNo  = paramSBox.getString("CUST_NO");
			String ownNm  = paramSBox.getString("OWN_NM");
			String corpNo  = paramSBox.getString("CORP_NO");
			String postNo  = paramSBox.getString("POST_NO");
			String addr1  = paramSBox.getString("ADDR_1");
			String bizType  = paramSBox.getString("BIZ_TYPE");
			String bizCond  = paramSBox.getString("BIZ_COND");
			String telNo  = paramSBox.getString("TEL_NO");
			String faxNo  = paramSBox.getString("FAX_NO");
			String mbNo  = paramSBox.getString("MB_NO");
			String email  = paramSBox.getString("EMAIL");
			
			String param = loginId  +"|" + custNm + "|" + custNo + "|" + custNo + "|" + corpNo + "|" + postNo + "|" + addr1+ "|" + bizType+ "|" + bizCond+ "|" + telNo+ "|" + faxNo+ "|" + mbNo+ "|" + email;
			log.i(getLogMessage("addCustinfoForSB", "param", param));
			
			
			//거래처 정보 정보
			SBox custSBox = new SBox();
			custSBox.set("loginId", loginId);
			custSBox.set("custNm", custNm);
			custSBox.set("custNo", custNo);
			custSBox.set("ownNm", ownNm);
			custSBox.set("corpNo", corpNo);
			custSBox.set("postNo", postNo);
			custSBox.set("addr1", addr1);
			custSBox.set("bizType", bizType);
			custSBox.set("bizCond", bizCond);
			custSBox.set("telNo", telNo);
			custSBox.set("faxNo", faxNo);
			custSBox.set("mbNo", mbNo);
			custSBox.set("email", email);
			
			
			customerService.addCustomerForSB(custSBox);
			
		}catch(Exception e) {
			resultMsg = "신규 거래처 추가 실패하였습니다.";     
			StringWriter sw = new StringWriter();
    		e.printStackTrace(new PrintWriter(sw));
			log.e(e.toString());
			e.printStackTrace();
		};
		 mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	
	
	/**
	 * <pre>
	 * 스마트빌 연계 채권 거래처 수정 Controller Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param : loginId:로그인아이디 , custType : 거래처유형 ,custNm : 거래처명 ,custNo : 사업자 등록번호 ,ownNm : 대표자 ,corpNo : 법인등록번호 ,
	 * 			post: 우편번호  ,addr1 :회사주소, bizType : 업종 ,bizCond : 업태 ,telNo: 전화  ,faxNo : 팩스, mbNo : 휴대폰 ,email : 이메일 
	 * @version 1.0
	 * @return mav 
	 */
	
	/*@RequestMapping(value = "/customer/modifyCustInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView modifyCustomerForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 연계  거래처 수정 완료";  
		log.i(getLogMessage("modifyCustomerForSB", "value", value));
		try{
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("modifyCustomerForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
	
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String custNo  = paramSBox.getString("CUST_NO");
			String custNm  = paramSBox.getString("CUST_NM");
			String ownNm  = paramSBox.getString("OWN_NM");
			String corpNo  = paramSBox.getString("CORP_NO");
			String postNo  = paramSBox.getString("POST_NO");
			String addr1  = paramSBox.getString("ADDR_1");
			String bizType  = paramSBox.getString("BIZ_TYPE");
			String bizCond  = paramSBox.getString("BIZ_COND");
			String telNo  = paramSBox.getString("TEL_NO");
			String faxNo  = paramSBox.getString("FAX_NO");
			String mbNo  = paramSBox.getString("MB_NO");
			String email  = paramSBox.getString("EMAIL");
			
			
			String param = loginId  +"|" + custNm + "|" + custNo + "|" + custNo + "|" + corpNo + "|" + postNo + "|" + addr1+ "|" + bizType+ "|" + bizCond+ "|" + telNo+ "|" + faxNo+ "|" + mbNo+ "|" + email;
			log.i(getLogMessage("modifyCustomerForSB", "param", param));
			
			
			//거래처 정보 정보
			SBox custSBox = new SBox();
			custSBox.set("loginId", loginId);
			custSBox.set("custNo", custNo);
			custSBox.set("custNm", custNm);
			custSBox.set("ownNm", ownNm);
			custSBox.set("corpNo", corpNo);
			custSBox.set("postNo", postNo);
			custSBox.set("addr1", addr1);
			custSBox.set("bizType", bizType);
			custSBox.set("bizCond", bizCond);
			custSBox.set("telNo", telNo);
			custSBox.set("faxNo", faxNo);
			custSBox.set("mbNo", mbNo);
			custSBox.set("email", email);
		
			customerService.modifyCustomerForSB(custSBox);
			
		}catch(Exception e) {
			resultMsg = "거래처 수정 실패하였습니다.";   
			StringWriter sw = new StringWriter();
    		e.printStackTrace(new PrintWriter(sw));
			log.e(e.toString());
			e.printStackTrace();
		};
		 mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	/**
	 * <pre>
	 * 스마트빌 연계 채권 거래처 삭제 Controller Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param : loginId:로그인아이디 , custNo : 사업자 등록번호  
	 * @version 1.0
	 * @return mav 
	 */
	
	/*@RequestMapping(value = "/customer/deleteCustInfoForSB.do", method=RequestMethod.POST)
	public ModelAndView removeCustomerForSB(HttpServletRequest request,
			@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String resultMsg = "스마트빌 연계 거래처 삭제 완료";  
		log.i(getLogMessage("removeCustomerForSB", "value", value));
		try{
			
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("removeCustomerForSB", "encodeValue", encodeValue));
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
			
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String loginId = paramSBox.getString("LOGIN_ID");
			String custNo  = paramSBox.getString("CUST_NO");
			
			String param = loginId  +"|" + custNo;
			log.i(getLogMessage("removeCustomerForSB", "param", param));
			
			
			//거래처 정보 정보
			SBox custSBox = new SBox();
			custSBox.set("loginId", loginId);
			custSBox.set("custNo", custNo);
		
			//customerService.removeCustomerForSB(custSBox);
			
		}catch(Exception e) {
			resultMsg = "거래처 삭제 실패하였습니다.";     
			StringWriter sw = new StringWriter();
    		e.printStackTrace(new PrintWriter(sw));
			log.e(e.toString());
			e.printStackTrace();
		};
		mav.addObject("resultMsg", resultMsg.toString());
		return mav;
	}*/
	
	
	
	/**
	 * <pre>
	 * 스마트빌  신용정보 변동 내역  Controller Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 18.
	 * @version 1.0
	 * @param : st_dt:변동일자검색시작일 , ed_dt :변동일자 검색종료일 , loginId: SMARTBILL 하드코딩 
	 * @version 1.0
	 * @return mav 
	 */
	/*@RequestMapping(value = "/customer/creditChgRequest.do", method=RequestMethod.POST)
	public ModelAndView creditChgRequestForSB(HttpServletRequest request,
										@RequestParam(value = "value", required = true) String value) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/interfaceNoteurl");
		String responseValue = new String();
		log.i(getLogMessage("creditChgRequest", "value", value));
		try{
			
			String encodeValue = CommonDesForSBUtil.Decrypt(value.trim());
			log.i(getLogMessage("creditChgRequest", "encodeValue", encodeValue));
			
			//암호화한 정보 쪼개서 각 값에 셋팅
			String[] params = encodeValue.split("&");
			SBox paramSBox = new SBox();
			
			for(int i=0; i<params.length; i++)
			{
				String[] param = params[i].split("=");
				//파리미터값이 null일 경우
				if(param.length==2){
					paramSBox.set(param[0], param[1]);
				}else{
					paramSBox.set(param[0], "");
				}
			}
			
			String stDt = paramSBox.getString("ST_DT");
			String edDt  = paramSBox.getString("ED_DT");
			String loginId  = paramSBox.getString("LOGIN_ID");
			
			String param = stDt  +"|" + edDt  + "|" + loginId;
			log.i(getLogMessage("creditChgRequest", "param", param));
			
				if("SMARTBILL".equals(loginId)){
					
					//거래처 정보 정보
					SBox creditSBox = new SBox();
					creditSBox.set("loginId", loginId);
					creditSBox.set("stDt", stDt);
					creditSBox.set("edDt", edDt);
					
					//responseValue=  customerService.getCreditChgListForSB(creditSBox).get("strValue");
					
				}else{
					responseValue = "ERROR";
				}
		}catch(Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
    		e.printStackTrace(new PrintWriter(sw));
			log.e(e.toString());
			responseValue = "ERROR";
		}
		
		mav.addObject("resultMsg", responseValue.toString());
		return mav;
	}*/
	
	
}
