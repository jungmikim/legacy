package com.spsb.controller.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonDesForSBUtil;
import com.spsb.service.message.InterfaceMessageService;

/**
 * <pre>
 *  메시지 인터페이스 Controller Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
@Controller
public class MessageController extends SuperController{

	
	@Autowired
	private InterfaceMessageService interfaceMessageService;
	
	/**
	 * <pre>
	 *  LegacyForAPI 크레딧서비스의 조기경보 대상 거래처 사업자번호의 신규등록을 하기 위한 XML 메시지 생성 (CRQREC) 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param batchId : 배치 아이디
	 * @param SBId : 스마트빌 사업자번호
	 * @return
	 * /message/makeEwCustRequestMessage.do?batchId=111111111111111111111111111&SBId=smartbill
	 * 
	 */
	@RequestMapping(value="/message/makeEwCustRequestMessage.do")
	public ModelAndView makeEwCustRequestMessage(@RequestParam(value = "batchId", required = true) String batchId, 
	    		 								 @RequestParam(value = "SBId", required = true) String SBId) {
			ModelAndView mav = new ModelAndView();
			SBox paramBox = new SBox();
			SBox resultSBox = new SBox();
			try{
				String param = batchId+  "|" + SBId;
				log.i(getLogMessage("makeEwCustRequestMessage", "param", param));
				
				
				if(batchId.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","batchId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("batchId is empty");
					
				}else if(batchId.length() > 27){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","batchId 자릿수는 27자리 이하 입니다.");
					mav.addObject("resultSBox", resultSBox);
					log.e("batchId 자릿수는 27자리 이하 입니다.");
				
				}else if(SBId.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","SBId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("SBId is empty");
					
				}else{
					paramBox.set("LegacyType", "API");
					paramBox.set("batchId",batchId);
					paramBox.set("SBId",SBId);
					resultSBox = interfaceMessageService.setEwCustRequestMessage(paramBox);
					mav.addObject("resultSBox", resultSBox);
				}
				
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("call makeEwCustRequestMessage of MessageController Unknown Exception ");
			log.e(ex.getMessage());
		}
		return mav;
	}
	
	
	/**
	 * <pre>
	 *  LegacyForAPI 크레딧서비스 채무불이행 등록 요청서 등록을 하기 위한 XML 메시지 생성(CRQRDA)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param debtId : 채무불이행 순번
	 * @param SBId : 스마트빌 아이디
	 * @param tempFileSns : 첨부파일 순번
	 * @return
	 * /message/makeDebtRequestMessage.do?debtId=2208102810214866854420160510135427771&SBId=leeny39&tempFileSns=5,6
	 */
	@RequestMapping(value="/message/makeDebtRequestMessage.do")
	public ModelAndView makeDebtRequestMessage(@RequestParam(value = "debtId", required = true) String debtId, 
	    		 							   @RequestParam(value = "SBId", required = true) String SBId,
	    		 							   @RequestParam(value = "tempFileSns", required = true) String tempFileSns) {
			ModelAndView mav = new ModelAndView();
			SBox paramBox = new SBox();
			SBox resultSBox = new SBox();
			try{
				String param = debtId+  "|" + SBId +"|"+ tempFileSns;
				log.i(getLogMessage("makeDebtRequestMessage", "param", param));
				
				if(debtId.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","debtId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("debtId is empty");

				}else if(SBId.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","SBId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("SBId is empty");
					
				}else if(tempFileSns.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","tempFileSns is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("tempFileSns is empty");
					
				}else{
					paramBox.set("LegacyType", "API");
					paramBox.set("debtId",debtId);
					paramBox.set("SBId",SBId);
					paramBox.set("tempFileSns",tempFileSns);
					resultSBox = interfaceMessageService.setDebtRequestMessage(paramBox);
					mav.addObject("resultSBox", resultSBox);
				}
				
				
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("call makeDebtRequestMessage of MessageController Unknown Exception ");
			log.e(ex.getMessage());
		}
		return mav;
	}
	
	
	
	/**
	 * <pre>
	 *  LegacyForAPI 크레딧서비스 채무불이행 등록 변경 요청서  XML 메시지 생성(CRQAMD) TODO: 테스트 해야함 20160427
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param debtId : 채무불이행 순번
	 * @param SBId : 스마트빌 아이디
	 * @param modStat : 상태변경
	 * @param tempFileSns : 첨부파일 순번
	 * /message/makeDebtAmendRequestMessage.do?debtId=2208102810214866854420160510135427771&SBId=leeny39&modStat=FA&tempFileSns=5,6
	 * @return
	 */
	@RequestMapping(value="/message/makeDebtAmendRequestMessage.do")
	public ModelAndView makeDebtAmendRequestMessage(@RequestParam(value = "debtId", required = true) String debtId, 
	    		 								 	@RequestParam(value = "SBId", required = true) String SBId,
	    		 								 	@RequestParam(value = "modStat", required = true) String modStat,
	    		 								 	@RequestParam(value = "tempFileSns", required = true) String tempFileSns) {
			ModelAndView mav = new ModelAndView();
			SBox resultSBox = new SBox();
			SBox paramBox = new SBox();
			SBox validationResultBox = new SBox();
			boolean BTF;
			try{
				
				String param = debtId+  "|" + SBId + "|" + modStat+ "|" + tempFileSns;
				log.i(getLogMessage("makeDebtAmendRequestMessage", "param", param));
				
				
				if(debtId.isEmpty()){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","debtId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("debtId is empty");
					
				}else if(SBId.isEmpty()){
					//mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","SBId is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("SBId is empty");
					
				}else if(modStat.isEmpty()){
					//mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","modStat is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("modStat is empty");
					
				}else if(!(modStat.toUpperCase().matches("(?i)AC|FA|FB|RR|R"))){
//					mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","Legacy 변경가능 상태값은  AC(접수취소),FA(등록전민원발생 추가증빙),FB(등록후민원발생 추가증빙),RR(해제요청),R(채무금액 정정요청) 입니다.");
					mav.addObject("resultSBox", resultSBox);
					log.e("Legacy 변경가능 상태값은  AC(접수취소),FA(등록전민원발생 추가증빙),FB(등록후민원발생 추가증빙),RR(해제요청),R(채무금액 정정요청) 입니다.");
					
				}else if(tempFileSns.isEmpty()){
					//mav.setViewName("/error/error");
					resultSBox.set("code","FAIL");
					resultSBox.set("description","tempFileSns is empty");
					mav.addObject("resultSBox", resultSBox);
					log.e("tempFileSns is empty");
					
				}else{
					paramBox.set("LegacyType", "API");
					paramBox.set("debtId",debtId);
					paramBox.set("SBId",SBId);
					paramBox.set("modStat",modStat.toUpperCase());
					paramBox.set("tempFileSns",tempFileSns);
					
					//변경가능한 상태인 Check 하기
					validationResultBox = interfaceMessageService.ValidationFormForDebtStat(paramBox);
					BTF = validationResultBox.get("BTF");
					if (!BTF) {
						log.e("Validation Fail :"+ validationResultBox.getString("checkValue"));
						//throw new BizException("31006", EErrorCodeType.search("31006").getErrMsg());
						resultSBox.set("code", "FAIL");
						resultSBox.set("description", validationResultBox.getString("checkValue"));
					}else{
						resultSBox = interfaceMessageService.processMakeDebtAmendRequestMessageAPI(paramBox);
					}
					
					mav.addObject("resultSBox", resultSBox);
				}
				
			
		} catch (Exception ex) {
			mav.addObject("resultSBox", resultSBox);
			ex.printStackTrace();
			log.e("call makeDebtAmendRequestMessage of MessageController Unknown Exception ");
			log.e(ex.getMessage());
		}
		return mav;
	}
	
	/**
	 * <pre>
	 *  LegacyForAPI 크레딧서비스 채무불이행 등록 변경 요청서  XML 메시지 생성(CRQAMD) TODO: 테스트 해야함 20160427
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param debtId : 채무불이행 순번
	 * @param SBId : 스마트빌 아이디
	 * @param modStat : 상태변경
	 * @param tempFileSns : 첨부파일 순번
	 * /message/makeDebtAmendRequestMessage.do?debtId=2208102810214866854420160510135427771&SBId=leeny39&modStat=FA&tempFileSns=5,6
	 * @return
	 */
	@RequestMapping(value="/message/receiveLicenseKeyFromProxyServer.do")
	public ModelAndView receiveLicenseKeyFromProxyServer(@RequestParam(value = "licenseKey", required = true) String licenseKeyWithCrypto) {
		String replyCd = "";
		String replyMsg="";
		ModelAndView mav = new ModelAndView("message/makeLicenseForValidation");
		SBox sBox = new SBox();
		try {
			// [1] 라이센스 복호화
			licenseKeyWithCrypto = licenseKeyWithCrypto.replace(" ", "+");
			String licenseKey = CommonDesForSBUtil.Decrypt(licenseKeyWithCrypto);
			
			// [2] 라이센스 update 치기 
			int updateCnt = interfaceMessageService.setLicenseKeyForLegacy(licenseKey);
			if(updateCnt == 0){
				replyCd="FS001S";
				replyMsg="라이센스 업데이트 실패";
			}else{
				replyCd="SC001S";
				replyMsg="정상처리됨";
			}

			sBox.set("code",replyCd);
			sBox.set("description",replyMsg);
			mav.addObject("resultSBox",sBox);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	
}
