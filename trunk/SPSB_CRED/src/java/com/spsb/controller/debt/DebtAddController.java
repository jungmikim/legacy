package com.spsb.controller.debt;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.service.debt.DebtService;

/**
 * <pre>
 * 채무불이행신청 Controller Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.04.15
 */
@Controller
public class DebtAddController extends SuperController {
	
	@Autowired
	private DebtService debtService;
	
	/**
	 * <pre>
	 * 채무불이행신청 페이지 호출 Controller Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 4. 15.
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtForm.do")
	public ModelAndView getDebentureDefaultForm(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("debt/getDebtForm");
		mav.addObject("result", debtService.getDebtForm(sBox));
		mav.addObject("sBox", sBox);
		
		return mav;
	}
	
	
	/**
	 * <pre>
	 * 채무불이행신청 Controller Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 4. 28.
	 * @param mltRequest : 첨부파일
	 * @param deptApplSeq : 채무불이행신청서 순번, usrId : 채무불이행신청자 유저 순번, applyCustType : 신청자 회사 구분, applyCorpName : 신청자 회사명, custNo : 신청자 회사 사업자 등록번호, 
	 		applyCorpNum : 신청자 회사 법인등록번호, applyOwnName : 신청자 회사 대표자명, postNo : 신청자 회사 우편번호, applyAddr1 : 신청자 회사 주소1, applyAddr2 : 신청자 회사 주소2, 
	 		applyBizType : 신청자 회사 업종, managerName : 정보등록담당자 이름, managerPart : 정보등록담당자 부서, managerPosition : 정보등록담당자 직위, telNo : 정보등록담당자 전화번호, 
	 		mbNo : 정보등록담당자 핸드폰번호, faxNo : 정보등록담당자 팩스번호, email : 정보등록담당자 이메일, compUsrId : 기업회원순번, custId : 거래처 순번, custType : 거래처 회사구분, 
	 		custNm : 거래처 회사명, ownNm : 거래처 대표자명, corpNo : 거래처 법인등록번호, postNo : 거래처 우편번호, addr1 : 거래처 주소1, addr2 : 거래처 상세주소, debt_type : 거래처 채무구분, 
	 		totUnpdAmt : 미수총금액, stDebtAmt : 채무불이행금액, debnId : 채권 순번, existFileSn : 채권 증빙파일 순번
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/debt/addDeptApply.do")
	@ResponseBody
	public void addDeptApply(MultipartHttpServletRequest mltRequest, @ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {
		
		sBox.set("dataUploadA", mltRequest.getFiles("dataUploadA"));
		sBox.set("dataUploadB", mltRequest.getFiles("dataUploadB"));
		sBox.set("dataUploadC", mltRequest.getFiles("dataUploadC"));
		sBox.set("dataUploadD", mltRequest.getFiles("dataUploadD"));
		sBox.set("dataUploadE", mltRequest.getFiles("dataUploadE"));
		
		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = debtService.addDeptApply(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 특정 거래처의 채권 리스트 조회 Controller Method
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 4. 22.
	 * @param bondMbrId : 채권담당자멤버순번, custId : 거래처 순번, orderCondition : 정렬조건, orderType : 정렬순서
	 * @return
	 */
	/*@RequestMapping(value = "/debt/getDebentureListOfCustomer.do")
	@ResponseBody
	public ModelAndView getDebentureListOfCustomer(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("jsonview");
		SBox resultBox = debtService.getDebentureListOfCustomer(sBox);
		mav.addObject("sBox", resultBox);

		return mav;
	}*/

}
