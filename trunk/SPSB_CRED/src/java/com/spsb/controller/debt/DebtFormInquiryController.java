package com.spsb.controller.debt;

import java.io.FileInputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonUtil;
import com.spsb.service.debt.DebtService;
import com.spsb.service.message.InterfaceMessageService;

/**
 * <pre>
 *   채무불이행 등록 -> 조회(신청서 조회)  Controller Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.04.28.
 * @version 1.0
 */
@Controller
public class DebtFormInquiryController extends SuperController {

	@Autowired
	private DebtService debtService;

	@Autowired
	private InterfaceMessageService interfaceMessageService;
	
	@Value("#{common['TMPFILE'].trim()}")
	private String tmpFilePath;
	
	@Value("#{common['DEBTFILE'].trim()}")
	private String debtFilePath; // 채무불이행 첨부파일 경로
	
	/**
	 * <pre>
	 * 채무불이행 신청서 조회
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014.04.28.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재
	 *            페이지 순번, sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번
	 * @return mav : view Resolver
	 */
	@RequestMapping(value = "/debt/getDebtInquiryList.do")
	public ModelAndView getDebtInquiryList(@ModelAttribute("initBoxs") SBox sBox) {
		ModelAndView mav = new ModelAndView("debt/getDebtInquiryList");
		mav.addObject("result", debtService.getDebtInquiryList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 EXCEL 다운로드 조회
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014.04.28.
	 * @param periodStDt
	 *            : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusALL : 진행상태 전체검색조건 , processStatusSC : 진행상태 저장완료 검색조건 , processStatusTS : 진행상태 임시저장 검색조건 , custSearchType : 거래처 검색조건 , custKwd :
	 *            거래처 검색어 , debtMbrType : 등록담당자 타입(전체,여러개선택) , mbrList : 등록담당자 리스트(,구분자) , orderCondition : 정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재
	 *            페이지 순번, sessionCompUsrId : 기업회원순번 , grantCode : 권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번
	 */
	@RequestMapping(value = "/debt/getDebtInquiryListForExcel.do")
	public void getDebtInquiryListForExcel(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {

		String fileName = "채무불이행 신청서조회_" + CommonUtil.getDate() + "_" + CommonUtil.getRandomString(5) + ".xls";

		try {

			ServletOutputStream res_out = response.getOutputStream();
			response.setContentType("application/vnd.ms-excel");
			String docName = getDocNameByBrowser(fileName);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");


			debtService.getDebtInquiryListForExcel(sBox, fileName);

			FileInputStream fis = new FileInputStream(tmpFilePath + fileName);

			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					res_out.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			res_out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * <pre>
	 * 채무불이행신청 문서 증빙파일 다운 Controller Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 10.
	 * @param fileNm : 파일명, filePath : 파일경로
	 * @param response
	 */
	@RequestMapping(value = "/debt/getDebtPrfFileDownload.do")
	public void getDebtPrfFileDownload(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {
		
		try {
			
			ServletOutputStream os = response.getOutputStream();
			String docName = getDocNameByBrowser(sBox.getString("fileNm"));

			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Type", "application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			
			FileInputStream fis = new FileInputStream(debtFilePath + "A/" + sBox.getString("filePath"));
			
			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					os.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			os.close();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 삭제
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 1.
	 * @param debtIdList
	 *            : 채무불이행 신청서 삭제 대상 리스트(구분자 ,)
	 * @return mav : view Resolver
	 */
	@RequestMapping(value = "/debt/removeDebt.do")
	public ModelAndView removeDebt(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("redirect:/debt/getDebtInquiryList.do");
		mav.addObject("result", debtService.removeDebt(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}

	/**
	 * <pre>
	 * 채무불이행 신청서 상세페이지 조회
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 5. 1.
	 * @param debtApplId : 채무불이행 순번
	 * @return mav : view Resolver
	 */
	@RequestMapping(value = "/debt/getDebtInquiry.do")
	public ModelAndView getDebtInquiry(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtInquiry");
		mav.addObject("result", debtService.getDebtInquiry(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}

	/**
	 * <pre>
	 * 채무불이행 신청 페이지 조회
	 * </pre>
	 * @author Jungmi Kim
	 * @since 2015. 7. 10.
	 * @param debtApplId : 채무불이행 신청서 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtConfirmation.do")
	public ModelAndView getDebtConfirmation(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("redirect:/debt/getDebtInquiryList.do");
		sBox.set("LegacyType", "Legacy");
		mav.addObject("result",interfaceMessageService.setDebtRequestMessage(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}

	
	
	
	/**
	 * <pre>
	 * 채무불이행 신청 / 상태이력 등록 Controller Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 10.
	 * @param debtApplId
 	 *            : 채무불이행신청서 순번, overStDt : 연체개시일자, regDueDt : 등록예정일, sessionUsrId : 등록자 순번, sessionAdmYn : 등록자 회원 여부, sessionUsrNm : 등록자 이름
	 * @return
	 */
	@RequestMapping(value = "/debt/insertDebtApplication.do")
	@ResponseBody
	public ModelAndView insertDebtApplication(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("debt/getDebtConfirmation");
		mav.addObject("result", debtService.insertDebtApplication(sBox));
		mav.addObject("sBox", sBox);

		return mav;
		
	}
	
	/**
	 * <pre>
	 * 특정 거래처의 이미 진행중인 채무불이행이 있는지 조회하는 Service method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 13.
	 * @param sessionUsrId : 개인 회원 순번, sessionCompUsrId : 기업회원순번, npmCustId : 거래처 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getUnderWayDebtTotalCount.do")
	@ResponseBody
	public ModelAndView getUnderWayDebtTotalCount(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("jsonview");
		SBox resultBox = debtService.getUnderWayDebtTotalCount(sBox.getString("sessionUsrId"), sBox.getString("sessionCompUsrId"), sBox.getString("npmCustId"));
		mav.addObject("sBox", resultBox);
		
		return mav;
		
	}
	
	

	/**
	 * <pre>
	 * 채무불이행 신청서 복사 후 작성
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 5. 9.
	 * @param debtApplId : 채무불이행순번, custId : 거래처 순번
	 * @return 
	 */
	/*@RequestMapping(value = "/debt/getDebtCopyAndWrite.do")
	public ModelAndView getDebtCopyAndWrite(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getCopyDebtForm");
		mav.addObject("result", debtService.getDebtCopyAndWrite(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}*/
	
	
	/**
	 * <pre>
	 * 정보등록 담당자 가져오기
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2015.06.16.
	 * @param 
	 * @return 
	 */
	/*@RequestMapping(value = "/debt/getBizNoSearchPopup.do")
	public ModelAndView getBizNoSearchPopup(@ModelAttribute("initBoxs") SBox sBox, 
											@RequestParam(value = "usrId", required = true) String usrId) {
		ModelAndView mav = new ModelAndView("debt/getBizNoSearchPopup");
		mav.addObject("sBox", sBox);
		try {
			sBox.set("usrNoFromWeb", usrId);
			mav.addObject("userResultList", debtService.getSyncUserListFromMiddleServer(sBox));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mav;
	}*/

}
