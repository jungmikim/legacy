package com.spsb.controller.debt;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonUtil;
import com.spsb.service.debt.DebtService;

/**
 * <pre>
 *   채무불이행 등록 -> 조회(진행 중 접수문서)  Controller Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.05.01.
 * @version 1.0
 */
@Controller
public class DebtOngoingInquiryController extends SuperController {

	@Autowired
	private DebtService debtService;

	@Value("#{common['TMPFILE'].trim()}")
	private String tmpFilePath;
	

	@Value("#{common['DEBTFILE'].trim()}")
	private String debtFilePath; // 채무불이행 첨부파일 경로
	
	@Value("#{linkSB['SAMPLE_POPUP_DOMAIN'].trim()}") 
	protected String samplePopupDomain;	
	
	
	/**
	 * <pre>
	 * 진행중 접수문서 조회 Controller Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014. 5. 7.
	 * @param periodStDt : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusType : 진행상태 (구분자,) ,custSearchType : 거래처 검색조건 , custKwd : 거래처 검색어 , debtMbrType
	 *        : 등록담당자 타입(전체,여러개선택), registMbrType : 신청담당자(전체, 여러개 선택) , mbrList : 등록담당자 리스트(,구분자) , registMbrList : 신청담당자 리스트(,구분자) orderCondition :
	 *        정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지 순번, sessionCompUsrId : 기업회원순번 , grantCode :
	 *        권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번 , modStatR : 채무금액정정요청 , modStatC : 채무금액 정정완료
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtOngoingList.do")
	public ModelAndView getDebtOngoingInquiryList(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtOngoingInquiryList");
		//mav.addObject("result", debtService.getDebtOngoingList(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}

	/**
	 * <pre>
	 * 진행 중 접수문서조회 EXCEL 다운로드 조회 Controller Method
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2014.05.08.
	 * @param periodStDt : 작성일자 시작일자 , periodEdDt : 작성일자 종료일자 , processStatusType : 진행상태 (구분자,) ,custSearchType : 거래처 검색조건 , custKwd : 거래처 검색어 , debtMbrType
	 *        : 등록담당자 타입(전체,여러개선택), registMbrType : 신청담당자(전체, 여러개 선택) , mbrList : 등록담당자 리스트(,구분자) , registMbrList : 신청담당자 리스트(,구분자) orderCondition :
	 *        정렬대상컬럼, orderType : 정렬조건(오름,내림) , searchType : L(리스트)E(엑셀), rowSize : 출력물 갯수, num : 현재 페이지 순번, sessionCompUsrId : 기업회원순번 , grantCode :
	 *        권한값 , mbrStat : 회원상태 , usrId : 멤버회원순번 , fileName : 엑셀파일 이름, modStatR : 채무금액정정요청 , modStatC : 채무금액 정정완료
	 */
	@RequestMapping(value = "/debt/getDebtOngoingListForExcel.do")
	public void getDebtOngoingListForExcel(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {

		String fileName = "채무불이행 진행중 접수문서 조회_" + CommonUtil.getDate() + "_" + CommonUtil.getRandomString(5) + ".xls";

		try {

			ServletOutputStream res_out = response.getOutputStream();
			response.setContentType("application/vnd.ms-excel");
			String docName = getDocNameByBrowser(fileName);

			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			
			//debtService.getDebtOngoingListForExcel(sBox, fileName);

			FileInputStream fis = new FileInputStream(tmpFilePath + fileName);

			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					res_out.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			res_out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * <pre>
	 * 채무불이행 진행 중 접수문서 상세페이지 조회
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2014. 05 .09
	 * @param debtApplId : 채무불이행 순번
	 * @return mav : view Resolver
	 */
	@RequestMapping(value = "/debt/getDebtOngoingInquiry.do")
	public ModelAndView getDebtOngoingInquiry(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtOngoingInquiry");
		mav.addObject("result", debtService.getDebtOngoingInquiry(sBox));
		mav.addObject("samplePopupDomain", samplePopupDomain);
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 채권불이행 사유관리 리스트 페이징 Controller Method
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2014. 05. 13.
	 * @param num : 페이징 숫자 , debtApplId : 채무불이행코드 , custId : 거래처 코드 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/debt/getDebtOngoingStatHistoryList.do")
	@ResponseBody
	public void getDebtOngoingStatHistoryList(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {
		
		// IE 에서 Application/json 타입을 지원하지 않기 때문에 out.write를 사용함
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = debtService.getDebtOngoingStatHistoryList(sBox);
		
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}

	/**
	 * <pre>
	 * 채무 불이행 추가 증빙 서류 업로드 Controller Method
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2014. 05. 10.
	 * @param mltRequest : 첨부파일
	 * @param debtApplSeq : 채무불이행순번, sessionUsrId : 등록자 순번, write_explain : 비고, dataUpload : 채무불이행 증빙파일, chFileNm : 저장할 파일명
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/debt/addDeptOngoingAppl.do")
	@ResponseBody
	public void addDeptOngoingAppl(MultipartHttpServletRequest mltRequest, @ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) throws IOException {
		
		sBox.set("dataUpload", mltRequest.getFiles("dataUpload"));
		
		ServletOutputStream out = response.getOutputStream();
		SBox resultBox = debtService.addDeptOngoingAppl(sBox);
		out.write(resultBox.toJSON("UTF-8").getBytes());
		
	}
	
	/**
	 * <pre>
	 * 채무 불이행 정정요청 팝업 Controller Method
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *         debnApplId : 채무불이행 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtRequireModify.do")
	public ModelAndView getDebtRequireModify(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtRequireModify");
		mav.addObject("result", debtService.getDebtRequireModify(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 *   채무불이행 정정요청 Controller Method
	 * </pre>
	 * 
	 * @author Jung Mi Kim
	 * @since 2015. 7. 02.
	 * @version 1.0
	 * @param sBox
	 *         debtApplId : 채무불이행 순번, debtAmt : 정정 요청 금액, modStat : 채무금액정정상태, rmkTxt : 정정사유
	 * @return
	 */
	@RequestMapping(value = "/debt/modifyDebtRequire.do")
	@ResponseBody
	public ModelAndView modifyDebtRequire(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", debtService.modifyDebtRequire(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 채무불이행신청 문서 추가증빙파일 다운 Controller Method
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2014. 5. 10.
	 * @param fileNm : 파일명, filePath : 파일경로
	 * @param response
	 */
	@RequestMapping(value = "/debt/getDebtAddedPrfFileDownload.do")
	public void getDebtPrfFileDownload(@ModelAttribute("initBoxs") SBox sBox, HttpServletResponse response) {
		
		try {
			
			ServletOutputStream os = response.getOutputStream();
			String docName = getDocNameByBrowser(sBox.getString("fileNm"));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
			response.setHeader("Content-Type", "application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary;");
			response.setHeader("Pragma", "no-cache;");
			response.setHeader("Expires", "-1;");
			
			FileInputStream fis = new FileInputStream(debtFilePath +"B/" + sBox.getString("filePath"));
			
			byte[] outputByte = new byte[4096];
			while (fis.read(outputByte, 0, 4096) != -1) {
				if (outputByte != null) {
					os.write(outputByte, 0, 4096);
				}
			}

			fis.close();
			os.close();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

	/**
	 * <pre>
	 *   채무불이행 해제요청 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @version 1.0
	 * @param sBox
	 *         debtApplId : 채무불이행 순번, rmkTxt : 해제사유, debtStatSubCd : 채무불이행 상태 서브 코드, debtStatCd : 채무불이행 상태 코드
	 * @return
	 */
	@RequestMapping(value = "/debt/releaseDebtRequire.do")
	@ResponseBody
	public ModelAndView releaseDebtRequire(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", debtService.releaseDebtRequire(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 채무 불이행 해제요청 팝업 Controller Method
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *         debnApplId : 채무불이행 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtRequireRelease.do")
	public ModelAndView getDebtRequireRelease(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtRequireRelease");
		mav.addObject("result", debtService.getDebtRequireRelease(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 *   채무불이행 신청취소 Controller Method
	 * </pre>
	 * 
	 * @author Jong Pil Kim
	 * @since 2014. 5. 10.
	 * @version 1.0
	 * @param sBox
	 *         debtApplId : 채무불이행 순번, rmkTxt : 해제사유, debtStatSubCd : 채무불이행 상태 서브 코드, debtStatCd : 채무불이행 상태 코드
	 * @return
	 */
	@RequestMapping(value = "/debt/cancelDebtApply.do")
	@ResponseBody
	public ModelAndView cancelDebtApply(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", debtService.cancelDebtApply(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 채무 불이행 신청취소 팝업 Controller Method
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 10.
	 * @param sBox
	 *         debnApplId : 채무불이행 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtApplyCancel.do")
	public ModelAndView getDebtApplyCancel(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtApplyCancel");
		mav.addObject("result", debtService.getDebtApplyCancel(sBox));
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	/**
	 * <pre>
	 * 채무불이행 진행완료 접수문서 상세페이지 조회
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 8.
	 * @param debtApplId : 채무불이행신청서 순번
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtCompleteInquiry.do")
	public ModelAndView getDebtCompleteInquiry(@ModelAttribute("initBoxs") SBox sBox) {

		ModelAndView mav = new ModelAndView("debt/getDebtCompleteInquiry");
		mav.addObject("result", debtService.getDebtCompleteInquiry(sBox));
		mav.addObject("samplePopupDomain", samplePopupDomain);
		mav.addObject("sBox", sBox);

		return mav;
	}
	
	
	/**
	 * 
	 * <pre>
	 * 채무불이행 상태변경 Process For API
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 11.
	 * @version 1.0
	 * @param sBox
	 * @return
	 * /debt/getDebtStatusChgForAPI.do?debtId=2208102810214866854420160510135427771&SBId=leeny39
	 */
	@RequestMapping(value = "/debt/getDebtStatusChgForAPI.do")
	public ModelAndView getDebtInquiryListforAPI(@RequestParam(value = "debtId", required = true) String debtId,
												 @RequestParam(value = "SBId", required = true) String SBId) {
		ModelAndView mav = new ModelAndView("debt/getDebtStatusChgForAPI");
		
		SBox paramSBox = new SBox();
		paramSBox.set("debtApplId", debtId);
		paramSBox.set("SBId", SBId);
		paramSBox.set("LegacyType", "Legacy");
		
		mav.addObject("result", debtService.getDebtOngoingInquiryForAPI(paramSBox));
		mav.addObject("sBox", paramSBox);
		mav.addObject("samplePopupDomain", samplePopupDomain);

		return mav;
	}
	
	/**
	 * <pre>
	 *  채무불이행 진행완료 접수문서 상세페이지 조회 ForAPI
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 16.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@RequestMapping(value = "/debt/getDebtCompleteInquiryForAPI.do")
	public ModelAndView getDebtCompleteInquiryForAPI(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("debt/getDebtCompleteInquiryForAPI");
		mav.addObject("result", debtService.getDebtCompleteInquiry(sBox));
		mav.addObject("samplePopupDomain", samplePopupDomain);
		mav.addObject("sBox", sBox);
		
		return mav;
	}
	
	/**
	 * <pre>
	 * 결제 후 채무불이행 상태를 변경하는 Controller Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 28.
	 * @param sBox debtApplSeq : 채무불이행 순번
	 * @return
	
	@RequestMapping(value = "/debt/payDebtApplication.do")
	@ResponseBody
	public ModelAndView payDebtApplication(@ModelAttribute("initBoxs") SBox sBox) {
		
		ModelAndView mav = new ModelAndView("jsonView");
		mav.addObject("result", debtService.payDebtApplication(sBox));
		mav.addObject("sBox", sBox);
		
		return mav;
		
	} */
}
