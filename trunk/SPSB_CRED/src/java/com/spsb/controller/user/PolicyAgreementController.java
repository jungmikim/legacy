package com.spsb.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.parent.SuperController;

/**
 * <pre>
 * 정책관련 Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 12. 6.
 * @version 1.0
 */
@Controller
public class PolicyAgreementController extends SuperController{

	/**
	 * <pre>
	 * 이용약관 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 6.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/accessTerms.do")
	public ModelAndView accessTerms(){
		ModelAndView mav = new ModelAndView("/user/accessTerms");
		return mav;
	}
	
	/**
	 * <pre>
	 * 개인정보취급방침 Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 12. 6.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/privatePolicy.do")
	public ModelAndView privatePolicy(){
		ModelAndView mav = new ModelAndView("/user/privatePolicy");
		return mav;
	}
}
