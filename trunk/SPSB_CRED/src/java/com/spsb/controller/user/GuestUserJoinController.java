package com.spsb.controller.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.parent.SuperController;
import com.spsb.common.util.CommonCode;
import com.spsb.common.util.CommonCrypto;
import com.spsb.common.util.CommonDesUtil;
import com.spsb.service.user.UserService;

/**
 * <pre>
 * 게스트 회원 Controller class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 1. 29.
 * @version 1.0
 */
@Controller
public class GuestUserJoinController extends SuperController{

	@Autowired
	private UserService userService;
	
	/**
	 * <pre>
	 * 게스트회원 계정받기(회원가입) Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 20.
	 * @version 1.0
	 * @return
	 */
	@RequestMapping(value = "/user/guestUserJoin.do")
	public ModelAndView guestUserJoin(){
		ModelAndView mav = new ModelAndView("/user/guestUserJoin");
		mav.addObject("emailCodeList", CommonCode.emailCodeList);
		return mav;
	}
	
	/**
	 * <pre>
	 * 게스트회원 체험계정HTML Ajax Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 29.
	 * @version 1.0
	 * @param email
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 */
	@RequestMapping(value = "/user/addGuestUserAjax.do")
	@ResponseBody
	public ModelAndView addGuestUserAjax(
			@RequestParam(value = "email", required = true) String email) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		ModelAndView mav = new ModelAndView("jsonView");
		SBox result = userService.addGuestUser(email);
		SBox jsonResult = new SBox();
		if(EUserErrorCode.SUCCESS.equals(result.get("result"))){
			jsonResult.set("isSuccess", true);
			String passwordEncSha = CommonCrypto.encryptSha1(result.getString("loginPw"));
			String guestKey = result.getInt("tmpUsrId") + "," + passwordEncSha;
			String guestKeyEncode = CommonDesUtil.encrypt(guestKey);
			String guestKeyUrl = URLEncoder.encode(guestKeyEncode, "UTF-8");
			jsonResult.set("key", guestKeyUrl);
		}else{
			jsonResult.set("isSuccess", false);
		}
		EUserErrorCode resultCode = result.get("result");
		jsonResult.set("message", resultCode.getDesc());
		mav.addObject("result", jsonResult);
		return mav;
	}
	
	/**
	 * <pre>
	 * 게스트회원 체험계정HTML Ajax Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 29.
	 * @version 1.0
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/user/getGuestUserHtmlAjax.do")
	public ModelAndView getGuestUserHtmlAjax(
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "key", required = true) String key ){
		ModelAndView mav = new ModelAndView("/user/getGuestUserHtmlAjax");
		mav.addObject("key", key);
		return mav;
	}
	
	/**
	 * <pre>
	 * 게스트회원 체험계정 이메일보내기 Ajax Controller Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 29.
	 * @version 1.0
	 * @param emailHtml
	 * @return
	 * @throws MessagingException 
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value = "/user/sendEmailGuestUserAjax.do")
	@ResponseBody
	public ModelAndView sendEmailGuestUserAjax (
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "emailHtml", required = true) String emailHtml) throws MessagingException, UnsupportedEncodingException{
		ModelAndView mav = new ModelAndView("jsonView");
		userService.sendEmailGuestUser(email, emailHtml);
		return mav;
	}
	
}

