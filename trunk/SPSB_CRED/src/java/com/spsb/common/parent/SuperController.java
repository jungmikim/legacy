package com.spsb.common.parent;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SLog;
import com.spsb.common.enumtype.ESessionNameType;
import com.spsb.common.session.SessionManager;
import com.spsb.common.session.SessionVo;
import com.spsb.common.util.CommonUtil;
import com.spsb.service.login.LoginService;

/**
 * <pre>
 *   Controller 전체 부모 Class
 * </pre>
 * 
 * @author Jong Pil Kim
 * @since 2013. 8. 21.
 * @version 1.0
 */
@Controller
public class SuperController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private LoginService loginService;

	@Autowired
	protected SLog log;
	
	@Value("#{common['SESSION_MAX_INACTIVE_TIME'].trim()}") 
	protected Double sessionMaxInactiveTime;

	@ModelAttribute("initBoxs")
	public SBox initBoxs() {
		return (SBox) request.getAttribute("sBox");

	}

	/**
	 * <pre>
	 * 공통으로 사용하는 로그 메세지 형식
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageValue : 메세지값
	 * @return
	 */
	public String getLogMessage(String methodName, String messageName, String messageValue) {
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + " : " + messageValue + " )xxxxxxxxxx";
	}

	/**
	 * <pre>
	 * 결제에서 사용하는 로그 메세지 형식
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageCode : 메세지코드
	 * @param messageValue : 메세지값
	 * @return
	 */
	public String getLogMessageForPay(String methodName, String messageName, String messageCode, String messageValue) {
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + " [" + messageCode + ":" + messageValue + "] )xxxxxxxxxx";
	}

	/**
	 * <pre>
	 * 개인회원의 현재 session 을 변경
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 15.
	 * @version 1.0
	 * @param request
	 * @param sBox
	 */
	protected void modifyUserCurrentSession(HttpServletRequest request, SBox sBox) {
		SBox loginInfo = new SBox();
		String loginId = sBox.getString(ESessionNameType.LOGIN_ID.getName());
		loginInfo = loginService.getLoginMemberUser(loginId);
		SessionManager sessionManager = SessionManager.getInstance();
		SessionVo sessionVo = sessionManager.setCurrentSession(request, loginInfo);
		sessionManager.sessionVoToSBox(sessionVo, sBox);
	}

	/**
	 * <pre>
	 * 현재 브라우저 정보를 얻어온다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2014. 09. 23.
	 * @version 1.0
	 * @param request
	 * @param sBox
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("unchecked")
	protected String getDocNameByBrowser(String fileName) throws UnsupportedEncodingException {
		SBox header = new SBox();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			header.set(headerName, request.getHeader(headerName));
		}
		String browser = header.getString("user-agent");
		String docName = "";
		if (browser.contains("MSIE")) {
			docName = CommonUtil.mappingUnicode(CommonUtil.UTF8encode(fileName));
		} else if (browser.contains("Firefox")) {
			docName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		} else if (browser.contains("Opera")) {
			docName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		} else if (browser.contains("Chrome")) {
			docName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		} else if (browser.contains("Safari")) {
			docName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		} else{
			docName = CommonUtil.mappingUnicode(CommonUtil.UTF8encode(fileName));
		}
		return docName;
	}
}
