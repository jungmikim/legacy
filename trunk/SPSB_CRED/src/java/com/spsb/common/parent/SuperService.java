package com.spsb.common.parent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spsb.common.collection.SLog;
/**
 *  <pre>
 *   Service 전체 부모 Class
 *  </pre> 
 *  @author Jong Pil Kim
 *	@since 2013. 8. 21.
 *  @version 1.0
 */
@Service
public class SuperService {

	@Autowired
	protected SLog log;
	
	/**
	 * <pre>
	 * 공통으로 사용하는 로그 메세지 형식
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageValue : 메세지값
	 * @return
	 */
	public String getLogMessage(String methodName, String messageName, Object messageValue){
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + "  : " + messageValue + " )xxxxxxxxxx";
	}

	/**
	 * <pre>
	 * 결제에서 사용하는 로그 메세지 형식
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageValue : 메세지값
	 * @return
	 */
	public String getLogMessageForPay(String methodName, String messageName, String messageCode, String messageValue){
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + " [" + messageCode + ":" + messageValue + "] )xxxxxxxxxx";
	}
}
