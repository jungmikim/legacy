package com.spsb.common.session;

import java.util.Date;


/**
 * <pre>
 * session Vo class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 12.
 * @version 1.0
 */
public class SessionVo {
	private boolean current = false;
	private String sessionKey;
	private Integer sessionCompUsrId;
	private String sessionCompMngCd;
	private Integer sessionMbrId;
	private Integer sessionUsrId;
	private String sessionLoginId;
	private String sessionCompUsrNm;
	private String sessionUsrNo;
	private String sessionUsrNm;
	private String sessionMbrUsrNm;
	private String sessionAdmYn;
	private String sessionCompType;
	private String sessionCompUsrStat;
	private String sessionUsrStat;
	private String sessionMbrStat;
	private String sessionEmail;
	private String sessionTelNo;
	private String sessionMbNo;
	private String sessionPostNo;
	private String sessionAddr1;
	private String sessionAddr2;
	private String sessionUsrType;
	private String sessionErpUsrId;
	private boolean isSessionDebtGrnt; 
	private boolean isSessionCustSearchGrnt; 
	private boolean isSessionCustBrifGrnt; 
	private boolean isSessionCustEwDetailGrnt;
	private boolean isSessionCustEwAddGrnt;
	

	/**2015-07-14 added**/
	private String sessionSbCompUsrId;
	private String sessionSbLoginId;
	private String sessionSbusrId;
	
	private String sessionPrdType;
	
	
	
	
	public boolean isSessionCustEwAddGrnt() {
		return isSessionCustEwAddGrnt;
	}
	public void setSessionCustEwAddGrnt(boolean isSessionCustEwAddGrnt) {
		this.isSessionCustEwAddGrnt = isSessionCustEwAddGrnt;
	}
	
	public String getSessionSbCompUsrId() {
		return sessionSbCompUsrId;
	}
	public void setSessionSbCompUsrId(String sessionSbCompUsrId) {
		this.sessionSbCompUsrId = sessionSbCompUsrId;
	}
	public String getSessionSbLoginId() {
		return sessionSbLoginId;
	}
	public void setSessionSbLoginId(String sessionSbLoginId) {
		this.sessionSbLoginId = sessionSbLoginId;
	}
	public String getSessionSbusrId() {
		return sessionSbusrId;
	}
	public void setSessionSbusrId(String sessionSbusrId) {
		this.sessionSbusrId = sessionSbusrId;
	}
	
	//Guest회원
	private Date sessionStLoginDt;	//Guest회원을 위한 초기 로그인일시
	
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public Integer getSessionCompUsrId() {
		return sessionCompUsrId;
	}
	public String getSessionCompMngCd() {
		return sessionCompMngCd;
	}
	public void setSessionCompUsrId(Integer sessionCompUsrId) {
		this.sessionCompUsrId = sessionCompUsrId;
	}
	public void setSessionCompMngCd(String sessionCompMngCd) {
		this.sessionCompMngCd = sessionCompMngCd;
	}
	public Integer getSessionMbrId() {
		return sessionMbrId;
	}
	public void setSessionMbrId(Integer sessionMbrId) {
		this.sessionMbrId = sessionMbrId;
	}
	public Integer getSessionUsrId() {
		return sessionUsrId;
	}
	public void setSessionUsrId(Integer sessionUsrId) {
		this.sessionUsrId = sessionUsrId;
	}
	public String getSessionLoginId() {
		return sessionLoginId;
	}
	public void setSessionLoginId(String sessionLoginId) {
		this.sessionLoginId = sessionLoginId;
	}
	public String getSessionCompUsrNm() {
		return sessionCompUsrNm;
	}
	public void setSessionCompUsrNm(String sessionCompUsrNm) {
		this.sessionCompUsrNm = sessionCompUsrNm;
	}
	public String getSessionUsrNo() {
		return sessionUsrNo;
	}
	public void setSessionUsrNo(String sessionUsrNo) {
		this.sessionUsrNo = sessionUsrNo;
	}
	public String getSessionUsrNm() {
		return sessionUsrNm;
	}
	public void setSessionUsrNm(String sessionUsrNm) {
		this.sessionUsrNm = sessionUsrNm;
	}
	public String getSessionMbrUsrNm() {
		return sessionMbrUsrNm;
	}
	public void setSessionMbrUsrNm(String sessionMbrUsrNm) {
		this.sessionMbrUsrNm = sessionMbrUsrNm;
	}
	public String getSessionAdmYn() {
		return sessionAdmYn;
	}
	public void setSessionAdmYn(String sessionAdmYn) {
		this.sessionAdmYn = sessionAdmYn;
	}
	public String getSessionCompType() {
		return sessionCompType;
	}
	public void setSessionCompType(String sessionCompType) {
		this.sessionCompType = sessionCompType;
	}
	public boolean isDebtGrnt() {
		return isSessionDebtGrnt;
	}
	public void setSessionDebtGrnt(boolean isSessionDebtGrnt) {
		this.isSessionDebtGrnt = isSessionDebtGrnt;
	}
	public boolean isSessionCustSearchGrnt() {
		return isSessionCustSearchGrnt;
	}
	public void setSessionCustSearchGrnt(boolean isSessionCustSearchGrnt) {
		this.isSessionCustSearchGrnt = isSessionCustSearchGrnt;
	}
	public boolean isSessionCustBrifGrnt() {
		return isSessionCustBrifGrnt;
	}
	public void setSessionCustBrifGrnt(boolean isSessionCustBrifGrnt) {
		this.isSessionCustBrifGrnt = isSessionCustBrifGrnt;
	}
	public boolean isSessionCustEwDetailGrnt() {
		return isSessionCustEwDetailGrnt;
	}
	public void setSessionCustEwDetailGrnt(boolean isSessionCustEwDetailGrnt) {
		this.isSessionCustEwDetailGrnt = isSessionCustEwDetailGrnt;
	}
	public String getSessionCompUsrStat() {
		return sessionCompUsrStat;
	}
	public void setSessionCompUsrStat(String sessionCompUsrStat) {
		this.sessionCompUsrStat = sessionCompUsrStat;
	}
	public String getSessionUsrStat() {
		return sessionUsrStat;
	}
	public void setSessionUsrStat(String sessionUsrStat) {
		this.sessionUsrStat = sessionUsrStat;
	}
	public String getSessionMbrStat() {
		return sessionMbrStat;
	}
	public void setSessionMbrStat(String sessionMbrStat) {
		this.sessionMbrStat = sessionMbrStat;
	}
	public String getSessionEmail() {
		return sessionEmail;
	}
	public void setSessionEmail(String sessionEmail) {
		this.sessionEmail = sessionEmail;
	}
	public String getSessionTelNo() {
		return sessionTelNo;
	}
	public void setSessionTelNo(String sessionTelNo) {
		this.sessionTelNo = sessionTelNo;
	}
	public String getSessionMbNo() {
		return sessionMbNo;
	}
	public void setSessionMbNo(String sessionMbNo) {
		this.sessionMbNo = sessionMbNo;
	}
	public String getSessionPostNo() {
		return sessionPostNo;
	}
	public void setSessionPostNo(String sessionPostNo) {
		this.sessionPostNo = sessionPostNo;
	}
	public String getSessionAddr1() {
		return sessionAddr1;
	}
	public void setSessionAddr1(String sessionAddr1) {
		this.sessionAddr1 = sessionAddr1;
	}
	public String getSessionAddr2() {
		return sessionAddr2;
	}
	public void setSessionAddr2(String sessionAddr2) {
		this.sessionAddr2 = sessionAddr2;
	}
	public String getSessionUsrType() {
		return sessionUsrType;
	}
	public void setSessionUsrType(String sessionUsrType) {
		this.sessionUsrType = sessionUsrType;
	}
	public String getSessionErpUsrId() {
		return sessionErpUsrId;
	}
	public void setSessionErpUsrId(String sessionErpUsrId) {
		this.sessionErpUsrId = sessionErpUsrId;
	}
	public Date getSessionStLoginDt() {
		return sessionStLoginDt;
	}
	public void setSessionStLoginDt(Date sessionStLoginDt) {
		this.sessionStLoginDt = sessionStLoginDt;
	}
	
	public String getSessionPrdType() {
		return sessionPrdType;
	}
	public void setSessionPrdType(String sessionPrdType) {
		this.sessionPrdType = sessionPrdType;
	}	
	
}
