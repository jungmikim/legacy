package com.spsb.common.util.mail;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;

/**
 * <pre>
 * Mail보내기 class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 1. 28.
 * @version 1.0
 */
@Repository
public class CommonMailSender {

	@Resource(name = "javaMailSender")
	private JavaMailSender javaMailSender;
	
	/**
	 * <pre>
	 * 메일 보내기 method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 28.
	 * @version 1.0
	 * @param vo
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException 
	 */
	public void sendMail(CommonMailVo vo) throws MessagingException, UnsupportedEncodingException{
		if(vo == null){
			return;
		}
		//JavaMail(mail.jar)
		MimeMessage message = javaMailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
		helper.setTo(vo.getTo());
		helper.setFrom(vo.getFrom(), vo.getFromName());
		helper.setSubject(vo.getSubject());
		helper.setText(vo.getContent(), true);
		javaMailSender.send(message);
	}
	
}
