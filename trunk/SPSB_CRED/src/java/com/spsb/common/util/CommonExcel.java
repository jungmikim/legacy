package com.spsb.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * Poi 모듈을 이용한 엑셀파일 쓰기 Class API
 * </pre>
 * 
 * @author sungrangkong
 * @since 2013.08.23
 * 
 */
public class CommonExcel {

	// Workbook
	private Workbook wb = null;

	// File Write Stream
	private FileOutputStream out = null;

	// Sheet
	private Sheet sheet = null;

	// Cell BackGround ColorMap
	private static Map<String, Short> colorMap = new HashMap<String, Short>();

	// Font Bolder Type
	private static Map<String, Short> borderTypeMap = new HashMap<String, Short>();

	// Line Border type
	private static Map<String, Short> lineBorderTypeMap = new HashMap<String, Short>();

	// Align type
	private static Map<String, Short> alignTypeMap = new HashMap<String, Short>();

	// Row max Size
	private static int ROW_MAX_NUM = 65535;

	// Excel Font Object Declare
	private HSSFFont fontColor;

	// PoiWriter Constructor
	public CommonExcel(String filePath) throws IOException {
		File file = new File(filePath);
		POIFSFileSystem fs = null;
		try {
			fs = new POIFSFileSystem(new FileInputStream(file));
		} catch (Exception ex) {
		}
		out = new FileOutputStream(filePath);
		wb = (fs != null) ? new HSSFWorkbook(fs) : new HSSFWorkbook();
	}

	static {

		colorMap.put("RED", Short.parseShort("0"));
		colorMap.put("BLUE", Short.parseShort("0"));
		colorMap.put("YELLOW", Short.parseShort("0"));
		colorMap.put("LIGHT-YELLOW", HSSFColor.LIGHT_YELLOW.index);
		colorMap.put("WHITE", HSSFColor.WHITE.index);
		colorMap.put("ORANGE", HSSFColor.LIGHT_ORANGE.index);

		borderTypeMap.put("NORMAL", Font.BOLDWEIGHT_NORMAL);
		borderTypeMap.put("BOLD", Font.BOLDWEIGHT_BOLD);

		lineBorderTypeMap.put("THIN", HSSFCellStyle.BORDER_THIN);
		lineBorderTypeMap.put("THICK", HSSFCellStyle.BORDER_THICK);

		alignTypeMap.put("L", CellStyle.ALIGN_LEFT);
		alignTypeMap.put("C", CellStyle.ALIGN_CENTER);
		alignTypeMap.put("R", CellStyle.ALIGN_RIGHT);
	}

	/**
	 * <pre>
	 * s
	 * WorkBook Getter Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @return
	 */
	public Workbook getWb() {
		return wb;
	}

	/**
	 * <pre>
	 * WorkBook Setter Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @return
	 */
	public Sheet getSheet() {
		return sheet;
	}

	/**
	 * <pre>
	 * 새로운 스타일을 생성한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @return
	 */
	public CellStyle createNewStyle() {
		return wb.createCellStyle();
	}

	/**
	 * <pre>
	 * Excel Sheet 생성 Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param sheetsInfo
	 *            0:sheetName 1:sheetTitle
	 * @return
	 */
	public void createSheet(String sheetsInfo[]) {
		for (int i = 0; i < sheetsInfo.length; i++) {
			wb.createSheet(sheetsInfo[i]);
		}
		sheet = wb.getSheetAt(0);
	}

	/**
	 * <pre>
	 * 여러 생성된 Sheet들 중에서 Sheet를 선택한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param index
	 *            : sheet index number
	 */
	public void selectSheet(int index) {
		sheet = wb.getSheetAt(index);
	}

	/**
	 * <pre>
	 * 	max row 갯수를 가져온다
	 * </pre>
	 * 
	 * @author HWAJUNG SON
	 * @since 2013.10.23
	 * @return
	 */
	public static int getROW_MAX_NUM() {
		return ROW_MAX_NUM;
	}

	/**
	 * <pre>
	 * Excel File Data 셋팅 Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param row
	 *            : 가로 index
	 * @param col
	 *            : 세로 index
	 * @param data
	 */
	public void setData(CellStyle style, int row, int col, String data) {

		if (sheet.getRow(row) != null) {
			Row rowObj = sheet.getRow(row);
			if (rowObj.getCell(col) != null) {
				rowObj.getCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(style);
			} else {
				rowObj.createCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(style);
			}
		} else {
			Row rowObj = sheet.createRow(row);
			if (rowObj.getCell(col) != null) {
				rowObj.getCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(style);
			} else {
				rowObj.createCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(style);
			}
		}

		
		// 글자수에 따른 width 자동 늘림 ( autoSizeColumn 경우 속도가 현저히 떨어지기 때문에 다른 방법을 씀 )
		int cellWidth = 1;
		if (data != null && data.length() > 0) {
			for (int i = 0; i < data.length(); i++) {
				if (Character.getType(data.charAt(i)) == Character.OTHER_LETTER) {
					cellWidth += 590; // 한글의 경우 590을 매번 더함.
				} else {
					cellWidth += 256; // 알파뉴메릭은 256을 더함.
				}
			}
		}
		cellWidth = (sheet.getColumnWidth(col) < cellWidth) ? cellWidth : sheet.getColumnWidth(col);
		sheet.setColumnWidth(col, cellWidth);

	}

	/**
	 * <pre>
	 * HSSFRichTextString객체를 통해 Excel File Data 셋팅 Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param row
	 *            : 가로 index
	 * @param col
	 *            : 세로 index @ param data
	 */
	public void setData(CellStyle paramStyle, int row, int col, HSSFRichTextString data, String aligns) {

		if (sheet.getRow(row) != null) {
			Row rowObj = sheet.getRow(row);
			if (rowObj.getCell(col) != null) {
				rowObj.getCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(paramStyle);
			} else {
				rowObj.createCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(paramStyle);
			}
		} else {
			Row rowObj = sheet.createRow(row);
			if (rowObj.getCell(col) != null) {
				rowObj.getCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(paramStyle);
			} else {
				rowObj.createCell(col).setCellValue(data);
				rowObj.getCell(col).setCellStyle(paramStyle);
			}
		}

		// 글자수에 따른 width 자동 늘림 ( autoSizeColumn 경우 속도가 현저히 떨어지기 때문에 다른 방법을 씀 )
		int cellWidth = 1;
		if (data != null && data.length() > 0) {
			for (int i = 0; i < data.length(); i++) {
				if (Character.getType(data.toString().charAt(i)) == Character.OTHER_LETTER) {
					cellWidth += 590; // 한글의 경우 590을 매번 더함.
				} else {
					cellWidth += 256; // 알파뉴메릭은 256을 더함.
				}
			}
		}
		cellWidth = (sheet.getColumnWidth(col) < cellWidth) ? cellWidth : sheet.getColumnWidth(col);
		sheet.setColumnWidth(col, cellWidth);

	}

	/**
	 * <pre>
	 * StringArray 로 부터 데이터를 읽어와 엑셀에 출력한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @param row
	 *            : row index
	 * @param startCol
	 *            : 시작 col index
	 * @param data
	 *            : parameter가 포함된 StringArray 값들
	 * @since 2013.08.23
	 */
	public void setBulkColsDataFromStringArray(ArrayList<CellStyle> styles, int row, int startCol, String[] data) {
		for (int i = startCol, j = 0; i < startCol + data.length; i++, j++) {

			String alignData = this.setTextAlign(styles.get(j), data[j]);
			this.setData(styles.get(j), row, i, alignData);
		}
	}

	/**
	 * <pre>
	 * Cell 안의 Text 정렬 방식
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param style
	 *            : 적용할 style 변수
	 * @param data
	 *            : 적용할 데이터 (데이터$L) 의 형식이어야 함.
	 * @return
	 */
	public String setTextAlign(CellStyle style, String data) {
		String returnData = "";
		if (data.indexOf("$") > -1) {
			String temp = data.split("\\$")[1];
			returnData = data.split("\\$")[0];
			style.setAlignment(alignTypeMap.get(temp));
		} else {
			style.setAlignment(alignTypeMap.get("L"));
			returnData = data;
		}
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		return returnData;
	}

	/**
	 * <pre>
	 * SBox로 부터 데이터를 읽어와 엑셀에 출력한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param row
	 *            : row index
	 * @param startCol
	 *            : 시작 col index
	 * @param sBox
	 *            : Parameter가 포함된 sBox
	 * @param keys
	 *            : sBox내의 출력할 Key 값들
	 */
	public void setBulkColsDataFromSBox(ArrayList<CellStyle> styles, int row, int startCol, SBox sBox, String[] keys) {
		for (int i = startCol, j = 0; j < keys.length; i++, j++) {
			String alignData = this.setTextAlign(styles.get(j), sBox.getString(keys[j]));
			this.setData(styles.get(j), row, i, alignData);
		}
	}

	/**
	 * <pre>
	 * HSSFRichTextString타입의 데이터를 SBox로 부터 데이터를 읽어와 엑셀에 출력한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param row
	 *            : row index
	 * @param startCol
	 *            : 시작 col index
	 * @param sBox
	 *            : Parameter가 포함된 sBox
	 * @param keys
	 *            : sBox내의 출력할 Key 값들
	 */
	public void setBulkColsRichTextFromSBox(ArrayList<CellStyle> style, int row, int startCol, SBox sBox, String[] keys, String[] align) {

		for (int i = startCol, j = 0; j < keys.length; i++, j++) {
			if (sBox.get(keys[j]) instanceof HSSFRichTextString) {
				this.setTextAlign((CellStyle) style.get(j), align[j]);
				this.setData((CellStyle) style.get(j), row, i, (HSSFRichTextString) sBox.get(keys[j]), align[j]);
			}
		}
	}

	/**
	 * <pre>
	 * 현재 Cell의 Background 색을 변경한다.
	 * 참고 : http://ko.wikipedia.org/wiki/%EC%9B%B9_%EC%83%89%EC%83%81ㄴ
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param style
	 *            : 적용할 style 변수
	 * @param color
	 *            : 색상명
	 */
	public void setBackgroundColorSetting(CellStyle style, String color) {

		style.setFillForegroundColor(colorMap.get(color));
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

	}

	/**
	 * 현재 CELL의 LINE 두께를 변경한다.
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param style
	 *            : 적용할 style 변수
	 * @param borderWeight
	 *            : line의 두께
	 */
	public void setLineBorder(CellStyle style, String borderWeight) {
		style.setBorderBottom(lineBorderTypeMap.get(borderWeight));
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		style.setBorderLeft(lineBorderTypeMap.get(borderWeight));
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		style.setBorderRight(lineBorderTypeMap.get(borderWeight));
		style.setRightBorderColor(HSSFColor.BLACK.index);
		style.setBorderTop(lineBorderTypeMap.get(borderWeight));
		style.setTopBorderColor(HSSFColor.BLACK.index);
	}

	/**
	 * <pre>
	 * 현재 Cell의 폰트 속성을 정의한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @param style
	 *            : 적용할 style 변수
	 * @param fontName
	 *            : font이름
	 * @param fontHeight
	 *            : fontSize
	 * @param boldWeight
	 *            : font 굵기
	 * @param italicTF
	 *            : italic 여부
	 * @param strikeOutTF
	 *            : strike 여부
	 */
	public void setFontStyleSetting(CellStyle style, String fontName, short fontHeight, String boldWeight, boolean italicTF, boolean strikeOutTF) {

		Font font = wb.createFont();
		font.setFontHeightInPoints(fontHeight);
		font.setFontName(fontName);
		font.setItalic(italicTF);
		font.setStrikeout(strikeOutTF);
		font.setBoldweight(borderTypeMap.get(boldWeight));
		style.setFont(font);

	}

	/**
	 * <pre>
	 * 엑셀파일을 ByteStream 으로 생성한다.
	 * 
	 * <pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @throws IOException
	 */
	public void write() throws IOException {
		this.wb.write(this.out);
	}

	/**
	 * <pre>
	 * 엑셀파일 관련된 객체를 Close한다.
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013.08.23
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (this.out != null) {
			this.out.close();
		}
	}

	/**
	 * <pre>
	 * String Type의 엑셀 데이터를 HSSFRichTextString 타입의 데이터로 변환함
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10. 8.
	 * @param key
	 *            : sBox내의 변경할 데이터 키 , data : 변경할 sBox 객체, color : RBG칼라의 Text 색상
	 * @return 변경된 HSSFRichTextString 객체
	 */
	public HSSFRichTextString convertRichTextString(String key, SBox data, short color) {

		HSSFRichTextString rich = null;
		Pattern pattern = null;
		Matcher mc = null;
		if (this.fontColor == null || this.fontColor.getColor() != color) {
			this.fontColor = (HSSFFont) wb.createFont();
		}

		fontColor.setColor(color);
		if (!data.isEmpty(key)) {
			pattern = Pattern.compile(".*(\\[E(A|C|I)+(R|F)+\\d{2})+.*", Pattern.DOTALL);
			mc = pattern.matcher(data.getString(key));
			if (mc.matches()) {
				rich = new HSSFRichTextString(data.getString(key));
				rich.applyFont(data.getString(key).indexOf(mc.group(1)), data.getString(key).length(), fontColor);
			} else {
				rich = new HSSFRichTextString(data.getString(key));
			}
		} else {
			rich = new HSSFRichTextString("");
		}

		return rich;
	}
}
