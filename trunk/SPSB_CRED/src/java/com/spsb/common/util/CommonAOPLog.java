package com.spsb.common.util;

import org.apache.log4j.Logger;


/**
 * <pre>
 * AOP Log Sample 
 * AOP POINT CUT에 대한 공통 로그를 남기기 위한 클래스임
 * </pre>
 * 
 * @author Jong Pil Kim
 * @since 2013.08.16
 * @version 1.0
 * 
 */
public class CommonAOPLog {

	private Logger log = Logger.getLogger(this.getClass());
	
	/**
	 *  <pre>
	 *   Service 로직 수행 직전 출력 로그
	 *  </pre> 
	 *  @author Jong Pil Kim
	 *	@since 2013. 8. 16.
	 *  @version 1.0
	 */
	public void beforeServiceInfoLog() {
		System.out.println("Service BEFORE");
	}

	/**
	 *  <pre>
	 *   Service 로직 수행 직후 출력 로그
	 *  </pre> 
	 *  @author Jong Pil Kim
	 *	@since 2013. 8. 16.
	 *  @version 1.0
	 */
	public void afterServiceInfoLog() {
		System.out.println("Service AFTER");
	}

	/**
	 * <pre>
	 *  Service ThrowingLog
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2015. 1. 14.
	 * @version 1.0
	 * @param ex
	 */
	public void afterThrowingLog(RuntimeException ex){
		//System.out.println("AFTER Throwing" + ex.getMessage());
		log.error(getLogMessage("afterThrowingLog", "ERROR", ex.toString()));
//		StringBuffer stringbuf = new StringBuffer();
//		for (StackTraceElement element : ex.getStackTrace()) {
//			stringbuf.append(element.toString());
//		}
//		log.error(stringbuf.toString());
	}
	
	/**
	 * <pre>
	 * 공통으로 사용하는 로그 메세지 형식
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageValue : 메세지값
	 * @return
	 */
	private String getLogMessage(String methodName, String messageName, String messageValue) {
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + " : " + messageValue + " )xxxxxxxxxx";
	}
}
