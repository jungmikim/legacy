package com.spsb.common.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class JAXBUtil {
	 public static String marshall2String(Class xclass, Object obj)
	  {
	    String xmlString = null;
	    try {
	      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { xclass });
	      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	      jaxbMarshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));

	      StringWriter sw = new StringWriter();
	      jaxbMarshaller.marshal(obj, System.out);
	      jaxbMarshaller.marshal(obj, sw);
	      xmlString = sw.toString();
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    return xmlString;
	  }

	  public static Object unmarshallFromString(Class xclass, String xmlString) {
	    Object obj = null;
	    try {
	      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { xclass });
	      StringReader reader = new StringReader(xmlString);
	      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

	      obj = jaxbUnmarshaller.unmarshal(reader);

	      return obj;
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    return obj;
	  }
}
