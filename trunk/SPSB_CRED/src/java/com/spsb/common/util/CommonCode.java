package com.spsb.common.util;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;

/**
 * <pre>
 * 공통코드 클래스
 * CommonCode.[codeName] 으로 호출 함
 * 
 * 공통코드 종류 : 
 *  - 전화번호 국번 , 이메일 주소, 핸드폰 앞3자리
 *  
 * 기타 추가하고 싶은 코드를 아래와 같이 static final로 추가하면 됨.
 * </pre>
 * 
 * @author sungrangkong
 * @since 2013.08.20
 * 
 */
public class CommonCode {

	// 전화번호 국번 공통코드
	public static final SBoxList<String> phoneCodeList = new SBoxList<String>();

	// 이메일 주소 공통코드
	public static final SBoxList<String> emailCodeList = new SBoxList<String>();

	// 핸드폰번호 앞3자리 공통코드
	public static final SBoxList<String> cellPhoneCodeList = new SBoxList<String>();

	// 결제 조건 타입 공통코드 
	public static final SBoxList<SBox> payTermTypeList = new SBoxList<SBox>();
	
	// 정렬 조건 타입 공통코드 
	public static final SBoxList<SBox> orderConditionTypeList = new SBoxList<SBox>();
	
	// 정렬 순서 공통코드 
	public static final SBoxList<SBox> orderTypeList = new SBoxList<SBox>();
	
	// 목록 개수 공통코드 
	public static final SBoxList<SBox> rowSizeTypeList = new SBoxList<SBox>();
	
	// 정렬 순서 타입 공통코드
	public static final SBoxList<SBox> orderCollectTypeList = new SBoxList<SBox>();

	// 채권 정렬 조건 타입 공통코드
	public static final SBoxList<SBox> debnOrderConditionTypeList = new SBoxList<SBox>();
	
	// 조회 기간 공통코드
	public static final SBoxList<SBox> periodConditionTypeList = new SBoxList<SBox>();
	
	// 채권 조회 상태 공통코드
	public static final SBoxList<SBox> debnStatusTypeList = new SBoxList<SBox>();
	
	// 수금방식 공통코드
	public static final SBoxList<SBox> debnColTypeList = new SBoxList<SBox>();
	
	// 채권 별 수금계획 공통코드
	public static final SBoxList<SBox> debnPreOrderConditionTypeList = new SBoxList<SBox>();
	
	// 독촉유형 상태코드 
	public static final SBoxList<SBox> debnPrsTypeList = new SBoxList<SBox>();
	
	// 신용정보유형 공통코드
	public static final SBoxList<SBox> crdTypeList = new SBoxList<SBox>();
	
	// 채무불이행신청 정렬 조건 타입 공통코드
	public static final SBoxList<SBox> npmOrderConditionTypeList = new SBoxList<SBox>();
	
	// 수금 계획 정렬 타입 공통코드
	public static final SBoxList<SBox> planOrderConditionList = new SBoxList<SBox>();
	
	// 수금 정렬 타입 공통코드
	public static final SBoxList<SBox> colOrderConditionList = new SBoxList<SBox>();
	
	// 거래처 수금방식 공통코드 
	public static final SBoxList<SBox> custColTypeList = new SBoxList<SBox>();
	
	// 채무불이행 신청서 조회 정렬 조건 타입 공통코드
	public static final SBoxList<SBox> debtApplicationListOrderConditionTypeList = new SBoxList<SBox>();
	
	// 채무불이행 진행완료 접수문서 정렬 조건 타입 공통 코드
	public static final SBoxList<SBox> debtCompleteListOrderConditionTypeList = new SBoxList<SBox>();
	
	// 독촉 정렬 타입 공통코드
	public static final SBoxList<SBox> prsOrderConditionList = new SBoxList<SBox>();
	
	// 독촉 유형 공통코드
	public static final SBoxList<SBox> prsTypeList = new SBoxList<SBox>();
	
	// 수금 상태 공통코드
	public static final SBoxList<SBox> colStatTypeList = new SBoxList<SBox>();
	
	// 세금 유형 공통코드
	public static final SBoxList<SBox> taxTypeList = new SBoxList<SBox>();
	
	// 수금계획 상태 공통코드
	public static final SBoxList<SBox> planStatTypeList = new SBoxList<SBox>();
	
	// 작성일 정렬 조건 공통코드
	public static final SBoxList<SBox> registPeriodConditionTypeList = new SBoxList<SBox>();
	
	// 채무불이행 진행 중 접수문서 조회 정렬 조건 공통코드
	public static final SBoxList<SBox> debtOngoingListOrderConditionTypeList = new SBoxList<SBox>();
	
	// 기술료 관리 검색 기간 타입 공통코드
	public static final SBoxList<SBox> debnSearchTypeList = new SBoxList<SBox>();
	
	// 기술료 관리 정렬 조건 공통코드
	public static final SBoxList<SBox> debnConditionTypeList = new SBoxList<SBox>();
	
	// 기술료 수금 관리 검색 기간 타입 공통코드
	public static final SBoxList<SBox> debnColSearchTypeList = new SBoxList<SBox>();
	
	// 기술료 수금 관리 정렬 조건 공통코드
	public static final SBoxList<SBox> debnColConditionTypeList = new SBoxList<SBox>();
	
	// Ajax 호출후 View Page로 리턴과정에서의 에러코드 및 메시지
	public static final SBox ajaxReplCdMsg = new SBox();

	static {

		// 전화번호 국번 static data 설정
		phoneCodeList.set(new String[] { "02", "031", "032", "033", "041", "042", "043", "051", "052", "053", "054", "055", "062", "061", "063",
				"064", "070" });

		// 핸드폰 앞3자리 statid data 설정
		cellPhoneCodeList.set(new String[] { "010", "011", "016", "017", "018", "019" });

		// 이메일 주소 static data 설정
		emailCodeList.set(new String[] { "", "naver.com", "chol.com", "dreamwiz.com", "empal.com", "freechal.com", "gmail.com", "hanafos.com",
				"hanmail.net", "hanmir.com", "hitel.net", "hotmail.com", "korea.com", "lycos.co.kr", "nate.com", "netian.com", "paran.com",
				"yahoo.com", "yahoo.co.kr" });

		// 결제 수단 타입 공통코드 설정함
		payTermTypeList.setJson("[{\"KEY\":\"NONE\",\"VALUE\":\"선택\", \"DAYESSENTIAL\":\"false\"},{\"KEY\":\"A\",\"VALUE\":\"당월 말일\", \"DAYESSENTIAL\":\"false\"},{\"KEY\":\"B\",\"VALUE\":\"당월 \", \"DAYESSENTIAL\":\"true\"},"
				+ "{\"KEY\":\"C\",\"VALUE\":\"익월 1일\", \"DAYESSENTIAL\":\"false\"},{\"KEY\":\"D\",\"VALUE\":\"익월 말일\", \"DAYESSENTIAL\":\"false\"},"
				+ "{\"KEY\":\"E\",\"VALUE\":\"익월 \", \"DAYESSENTIAL\":\"true\"},{\"KEY\":\"F\",\"VALUE\":\"세금계산서작성일 +\", \"DAYESSENTIAL\":\"true\"}]");
		
		// 정렬 조건 타입 공통코드 설정함
		orderConditionTypeList.setJson("[{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"CUST_CRD_DT\",\"VALUE\":\"신용정보(EW)변동일\"}]");

		// 정렬 순서 타입 공통코드 설정함
		orderTypeList.setJson("[{\"KEY\":\"ASC\",\"VALUE\":\"오름차순\"},{\"KEY\":\"DESC\",\"VALUE\":\"내림차순\"}]");
		
		// 목록 개수 타입 공통코드 설정함
		rowSizeTypeList.setJson("[{\"KEY\":\"10\",\"VALUE\":\"10개\"},{\"KEY\":\"30\",\"VALUE\":\"30개\"},{\"KEY\":\"50\",\"VALUE\":\"50개\"},,{\"KEY\":\"100\",\"VALUE\":\"100개\"}]");

		// 정렬 조건 타입 공통코드 설정함 
		orderCollectTypeList.setJson("[{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"COL_DT\",\"VALUE\":\"수금일\"}]");
		
		// 채권 정렬 조건 타입 공통코드 설정함
		debnOrderConditionTypeList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"세금계산서작성일\"},{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"LTD_DT\",\"VALUE\":\"만기일\"},{\"KEY\":\"SUM_AMT\",\"VALUE\":\"채권합계금액\"}]"); //,{\"KEY\":\"COL_DT\",\"VALUE\":\"수금일\"},{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금계획일\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"잔액\"}]");
		
		// 조회 기간 조건 타입 공통코드 설정함
		periodConditionTypeList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"세금계산서작성일\"},{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"LTD_DT\",\"VALUE\":\"만기일\"}]");// {\"KEY\":\"ALL_DT\",\"VALUE\":\"전체\"},{\"KEY\":\"COL_DT\",\"VALUE\":\"수금일\"},{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금계획일\"}]");
				
		// 채권 조회 상태 타입 공통코드 설정함
		debnStatusTypeList.setJson("[{\"KEY\":\"debnSearch\",\"VALUE\":\"연체기간별조회\"},{\"KEY\":\"beforeDay\",\"VALUE\":\"미연체(6개월이전)\"},{\"KEY\":\"thirtyDay\",\"VALUE\":\"30일 연체\"},{\"KEY\":\"sixtyDay\",\"VALUE\":\"60일 연체\"},{\"KEY\":\"ninetyDay\",\"VALUE\":\"90일 연체\"},{\"KEY\":\"oneHundredTwentyDay\",\"VALUE\":\"120일 연체\"},{\"KEY\":\"oneHundredEightyDay\",\"VALUE\":\"180일 연체\"},{\"KEY\":\"oneHundredEightyOverDay\",\"VALUE\":\"180일 초과(6개월이후)\"}]");
	
		// 수금 방식 공통코드 설정함 
		debnColTypeList.setJson("[{\"KEY\":\"C\",\"VALUE\":\"현금\"},{\"KEY\":\"B\",\"VALUE\":\"어음\"},{\"KEY\":\"D\",\"VALUE\":\"기타할인\"}]");
		
		// 채권 별 수금계획 정렬방식 공통코드 설정함
		debnPreOrderConditionTypeList.setJson("[{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금계획일\"}]");
	
		// 독촉수금 공통코드 설정함
		debnPrsTypeList.setJson("[{\"KEY\":\"A\",\"VALUE\":\"독촉\"},{\"KEY\":\"B\",\"VALUE\":\"추심\"},{\"KEY\":\"C\",\"VALUE\":\"내용증명\"},{\"KEY\":\"D\",\"VALUE\":\"채무불이행등록\"},{\"KEY\":\"E\",\"VALUE\":\"소송\"}]");

		// 신용정보 타입 공통코드 설정함
		crdTypeList.setJson("[{\"KEY\":\"A\",\"VALUE\":\"휴폐업여부\"},{\"KEY\":\"B\",\"VALUE\":\"사업자 신용정보\"},{\"KEY\":\"C\",\"VALUE\":\"대표자 신용정보\"}]");

		// 채무불이행신청서 작성 정렬 조건 타입 공통코드 설정함
		npmOrderConditionTypeList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"세금계산서작성일\"},{\"KEY\":\"SUM_AMT\",\"VALUE\":\"채권합계금액\"},{\"KEY\":\"DELAY_DAY\",\"VALUE\":\"연체일수\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"미수금액\"}]");
		
		// 수금 계획 정렬 타입 공통코드 설정함
		planOrderConditionList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"작성일\"},{\"KEY\":\"SUM_AMT\",\"VALUE\":\"채권합계금액\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"미수금액\"}]");

		// 수금 정렬 타입 공통코드 설정함
		colOrderConditionList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"세금계산서작성일\"},{\"KEY\":\"SUM_AMT\",\"VALUE\":\"채권합계금액\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"미수금액\"}]");
		
		// 거래처 수금 방식 공통코드 설정함 
		custColTypeList.setJson("[{\"KEY\":\"H\",\"VALUE\":\"현금\"},{\"KEY\":\"B\",\"VALUE\":\"어음\"},{\"KEY\":\"C\",\"VALUE\":\"카드\"},{\"KEY\":\"T\",\"VALUE\":\"계좌이체\"},{\"KEY\":\"D\",\"VALUE\":\"기타할인\"},{\"KEY\":\"I\",\"VALUE\":\"대손처리\"}]");
		
		// 채무불이행 신청서 조회 정렬 조건 타입 공통코드 설정함
		debtApplicationListOrderConditionTypeList.setJson("[{\"KEY\":\"REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"CUST_NM\",\"VALUE\":\"거래처명\"},{\"KEY\":\"ST_DEBT_AMT\",\"VALUE\":\"채무금액\"},{\"KEY\":\"STAT\",\"VALUE\":\"진행상태\"},{\"KEY\":\"USR_NM\",\"VALUE\":\"담당자\"}]");
		
		// 채무불이행 진행완료 접수문서 조회 정렬 조건 타입 공통코드 설정함
		debtCompleteListOrderConditionTypeList.setJson("[{\"KEY\":\"ACPT_ID\",\"VALUE\":\"접수번호\"},{\"KEY\":\"APPL_DT\",\"VALUE\":\"신청일\"},{\"KEY\":\"CUST_NM\",\"VALUE\":\"거래처명\"},{\"KEY\":\"CUST_NO\",\"VALUE\":\"사업자번호\"},{\"KEY\":\"ST_DEBT_AMT\",\"VALUE\":\"채무금액\"},{\"KEY\":\"STAT\",\"VALUE\":\"진행상태\"},{\"KEY\":\"CH_STAT_DT\",\"VALUE\":\"진행상태변경일\"},{\"KEY\":\"USR_NM\",\"VALUE\":\"등록담당자\"},{\"KEY\":\"APPL_USR_NM\",\"VALUE\":\"신청자\"}]");
		
		// 독촉 정렬 타입 공통코드 설정함
		prsOrderConditionList.setJson("[{\"KEY\":\"BILL_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"SUM_AMT\",\"VALUE\":\"합계금액\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"미수금액\"}]");
		
		// 독촉 유형 공통코드 설정함
		prsTypeList.setJson("[{\"KEY\":\"A\",\"VALUE\":\"전화\"},{\"KEY\":\"B\",\"VALUE\":\"이메일\"},{\"KEY\":\"C\",\"VALUE\":\"방문\"},{\"KEY\":\"D\",\"VALUE\":\"#메일\"},{\"KEY\":\"E\",\"VALUE\":\"등기\"},{\"KEY\":\"F\",\"VALUE\":\"내용증명\"},{\"KEY\":\"G\",\"VALUE\":\"상거래채무불이행등록\"},{\"KEY\":\"H\",\"VALUE\":\"소송\"},{\"KEY\":\"I\",\"VALUE\":\"채무불이행등록\"}]");
		
		// 수금 상태 공통코드 설정함
		colStatTypeList.setJson("[{\"KEY\":\"C\",\"VALUE\":\"수금완료\"},{\"KEY\":\"I\",\"VALUE\":\"대손처리\"}]");
		
		// 세금 유형 공통코드 설정함
		taxTypeList.setJson("[{\"KEY\":\"T\",\"VALUE\":\"과세\"},{\"KEY\":\"F\",\"VALUE\":\"면세\"},{\"KEY\":\"Z\",\"VALUE\":\"영세\"}]");
		
		// 수금계획 상태 공통코드 설정함
		planStatTypeList.setJson("[{\"KEY\":\"P\",\"VALUE\":\"수금전\"},{\"KEY\":\"S\",\"VALUE\":\"부분수금\"},{\"KEY\":\"C\",\"VALUE\":\"수금완료\"},{\"KEY\":\"D\",\"VALUE\":\"연기\"}]");

		// 작성 일 공통코드 설정함
		registPeriodConditionTypeList.setJson("[{\"KEY\":\"ALL_DT\",\"VALUE\":\"전체\"},{\"KEY\":\"APPL_DT\",\"VALUE\":\"신청일\"},{\"KEY\":\"REG_DUE_DT\",\"VALUE\":\"등록예정일\"},{\"KEY\":\"CH_STAT_DT\",\"VALUE\":\"진행상태변경일\"},{\"KEY\":\"OVER_ST_DT\",\"VALUE\":\"연체개시일\"}]");
		
		// 채무불이행 진행 중 접수문서 조회 정렬 조건 타입 공통코드 설정함
		debtOngoingListOrderConditionTypeList.setJson("[{\"KEY\":\"ACPT_ID\",\"VALUE\":\"접수번호\"},{\"KEY\":\"APPL_DT\",\"VALUE\":\"신청일\"},{\"KEY\":\"CUST_NO\",\"VALUE\":\"사업자번호\"},{\"KEY\":\"CUST_NM\",\"VALUE\":\"거래처명\"},{\"KEY\":\"DEBT_AMT\",\"VALUE\":\"채무금액\"},{\"KEY\":\"OVER_ST_DT\",\"VALUE\":\"연체개시일\"},{\"KEY\":\"STAT\",\"VALUE\":\"진행상태\"},{\"KEY\":\"CH_STAT_DT\",\"VALUE\":\"진행상태변경일\"},{\"KEY\":\"REG_DUE_DT\",\"VALUE\":\"등록예정일\"},{\"KEY\":\"REG_USR_ID\",\"VALUE\":\"등록담당자\"},{\"KEY\":\"APPL_USR_ID\",\"VALUE\":\"신청자\"}]");
		
		// 기술료 관리 검색 기간 타입 공통코드
		debnSearchTypeList.setJson("[{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금예정일\"},{\"KEY\":\"CONT_DT\",\"VALUE\":\"계약일\"},{\"KEY\":\"CRD_UPD_DT\",\"VALUE\":\"기업신용변동일\"},{\"KEY\":\"NO_CONDITION\",\"VALUE\":\"전체\"}]");
		
		// 기술료 관리 정렬 조건 공통코드
		debnConditionTypeList.setJson("[{\"KEY\":\"DEBN_REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금예정일\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"잔액\"}]");
		
		// 기술료 수금 관리 검색 기간 타입 공통코드
		debnColSearchTypeList.setJson("[{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금계획일\"},{\"KEY\":\"DUE_COL_DT\",\"VALUE\":\"수금예정일\"},{\"KEY\":\"COL_DT\",\"VALUE\":\"수금일자\"},{\"KEY\":\"CONT_DT\",\"VALUE\":\"계약일\"},{\"KEY\":\"CRD_UPD_DT\",\"VALUE\":\"기업신용변동일\"}]");
		
		// 기술료 수금 관리 정렬 조건 공통코드
		debnColConditionTypeList.setJson("[{\"KEY\":\"COL_REG_DT\",\"VALUE\":\"등록일\"},{\"KEY\":\"DUE_COL_DT\",\"VALUE\":\"수금예정일\"},{\"KEY\":\"PLN_COL_DT\",\"VALUE\":\"수금계획일\"},{\"KEY\":\"RMN_AMT\",\"VALUE\":\"미수금액\"}]");
		
	}

}
