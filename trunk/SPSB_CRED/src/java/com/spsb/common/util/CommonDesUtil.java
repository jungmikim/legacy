package com.spsb.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * <pre>
 * 암호화, 복호화 
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 2. 5.
 * @version 1.0
 */
public class CommonDesUtil {

	/**
	 * Encryption Key
	 */
	private static Key key = null;
	private static final String DEFALT_SEED = "4d89g13j4j91j27c582ji69373y788r6";
	/**
	 * Initialize encryption key
	 */
	static {
		if( key == null ) {
			try {
				setKey( DEFALT_SEED );
            } catch( NoSuchAlgorithmException e ) {
            	e.printStackTrace();
            }
		}
	}
	
	public static void setKey(String seed) throws NoSuchAlgorithmException{
		byte[] keyB = new byte[24];
		for (int i = 0; i < seed.length() && i < keyB.length; i++) {
			keyB[i] = (byte) seed.charAt(i);
		}

		key = new SecretKeySpec(keyB, "TripleDES");
	}
	
	/**
	 * <pre>
	 * 암호화
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 2. 5.
	 * @version 1.0
	 * @param str
	 * @return
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static String encrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException
		, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
    	if(str == null){
    		return str;
    	}
		Cipher cipher = Cipher.getInstance("TripleDES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] plaintext = str.getBytes("UTF8");
		byte[] ciphertext = cipher.doFinal(plaintext);
		return new String(Base64.encodeBase64(ciphertext));
		
//		StringBuffer sb = new StringBuffer(ciphertext.length * 2);
//		for(int i = 0; i < ciphertext.length; i++) {
//			String hex = "0" + Integer.toHexString(0xff & ciphertext[i]);
//			sb.append(hex.substring(hex.length()-2));
//		}
//		return sb.toString();
    }
    
	/**
	 * <pre>
	 * 복호화
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 2. 5.
	 * @version 1.0
	 * @param str
	 * @return
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws IOException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static String decrypt(String str) throws NoSuchAlgorithmException, NoSuchPaddingException
		, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		if(str == null){
    		return str;
    	}
		// Decode base64 to get bytes
		byte[] dec = Base64.decodeBase64(str.getBytes());
		
		Cipher cipher = Cipher.getInstance("TripleDES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
        // Decrypt
        byte[] decryptedText = cipher.doFinal(dec);
        return new String(decryptedText, "UTF8");
    }
	
}
