package com.spsb.common.util;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class FileUtil {
	 public static void write(File file, byte[] data)
			    throws IOException
			  {
			    OutputStream os = new FileOutputStream(file);
			    os.write(data);
			    os.close();
			  }

	  public static byte[] read(InputStream in)
	    throws IOException
	  {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    byte[] buffer = new byte[512];
	    try
	    {
	      int count;
	      while ((count = in.read(buffer)) != -1)
	      {
	        int cnt = 0;
	        out.write(buffer, 0, cnt);
	      }
	    } finally {
	      if (in != null)
	        in.close();
	    }
	    return out.toByteArray();
	  }
	  
	  /**
	   * 파일 사이즈 구하는 메소드
	   * @author HWAJUNG SON 
	   * @param File file 파일 
	   * @return 파일 사이즈 
	   */
	  public static Double getFileSize(String filePath) {
		  
		   	File file = new File(filePath);
		  	double fileSize = 0 ;
		    if (file.exists()) {
		    	fileSize = file.length();
		    }
		    return fileSize;
		  }
}
