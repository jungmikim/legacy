package com.spsb.common.enumtype;


/**
 * <pre>
 * EW Summary 코드 ENUM CLASS
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 9.
 * @version 1.0
 */
public enum EEwSummaryCode {

	CR_KFB_YN("CR_KFB_YN", "금융권단기연체_은행연체"),
	CR_YN("CR_YN", "금융권단기연체_KED-일반"),
	CR_CARD_YN("CR_CARD_YN", "금융권단기연체_KED-법인카드"),
	COMP_DEBT_YN("COMP_DEBT_YN", "금융권 채무불이행정보_기업"),
	PER_DEBT_YN("PER_DEBT_YN", "금융권 채무불이행정보_개인"),
	NEXE_PURC_ENP_YN("NEXE_PURC_ENP_YN", "채무불이행_공공_기업"),
	NEXE_PURC_REPER_YN("NEXE_PURC_REPER_YN", "채무불이행_공공_대표자"),
	SUS_ACC_YN("SUS_ACC_YN", "당좌거래정지"),
	CLS_BIZ_YN("CLS_BIZ_YN", "휴폐업"),
	CTX_OV_YN("CTX_OV_YN", "상거래연체"),
	WORKOUT_YN("WORKOUT_YN", "법정관리"),
	ADMIN_MEASURE_YN("ADMIN_MEASURE_YN", "행정처분정보"),
	CRD_LV_YN("CRD_LV_YN", "신용등급"),
	FIN_YN("FIN_YN", "재무"),
	LWST_YN("LWST_YN", "소송"),
	SC_RCD_YN("SC_RCD_YN", "조회기록");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 멤버회원권한생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @param desc : 설명
	 */
	private EEwSummaryCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 멤버회원권한 코드 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @return EMemberUserGrantCode : 멤버회원권한 코드 enum
	 */
	public static EEwSummaryCode search(String code){
		for(EEwSummaryCode type : EEwSummaryCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
