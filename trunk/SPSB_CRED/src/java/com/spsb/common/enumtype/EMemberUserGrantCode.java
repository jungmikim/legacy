package com.spsb.common.enumtype;


/**
 * <pre>
 * 멤버회원 권한 코드 ENUM CLASS
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 9.
 * @version 1.0
 */
public enum EMemberUserGrantCode {

	DEBN_GRN("CRED1C", "채무불이행 작성/신청권한"),
	CUST_SEARCH_GRN("SBDE1R", "기업정보검색권한"),
	CUST_BRIF_GRN("SBDE2R", "기업정보브리핑권한"),
	CUST_EW_DETAIL_GRN("SBDE3R", "조기경보상세권한"),
	CUST_EW_ADD_GRN("SBDE4R", "조기경보업체등록권한");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 멤버회원권한생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @param desc : 설명
	 */
	private EMemberUserGrantCode(String code, String desc){
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 멤버회원권한 코드 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 4.
	 * @version 1.0
	 * @param code : 코드
	 * @return EMemberUserGrantCode : 멤버회원권한 코드 enum
	 */
	public static EMemberUserGrantCode search(String code){
		for(EMemberUserGrantCode type : EMemberUserGrantCode.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
