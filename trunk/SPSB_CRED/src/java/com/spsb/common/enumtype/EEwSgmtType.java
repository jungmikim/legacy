package com.spsb.common.enumtype;

import java.util.Map;
import java.util.TreeMap;
/**
 * <pre>
 * EW모형세그먼트구분 타입  enum class
 * </pre>
 * @author KIM GA EUN
 * @since 2016. 4. 21.
 * @version 1.0
 */
public enum EEwSgmtType {

	ES02("02", "금융기관-금융"),
	ES11("11", "대기업-제조"),
	ES12("12", "대기업-건설"),
	ES13("13", "대기업-도소매/서비스기타"),
	ES21("21", "외감-제조"),
	ES22("22", "외감-건설"),
	ES23("23", "외감-도소매/서비스기타"),
	ES31("31", "비외감-경공"),
	ES32("32", "비외감-중공"),
	ES33("33", "비외감-건설"),
	ES34("34", "비외감-도소매/서비스기타"),
	ES41("41", "소기업-제조"),
	ES42("42", "소기업-건설"),
	ES43("43", "소기업-도소매/서비스기타"),
	ES51("51", "기타법인-제조"),
	ES52("52", "기타법인-건설"),
	ES53("53", "기타법인-도소매/서비스기타");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 EW등급타입생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 */
	private EEwSgmtType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는  EW등급타입 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @param code : 코드
	 * @return EEwRatingType : EW등급타입 enum
	 */
	public static EEwSgmtType search(String code){
		for(EEwSgmtType type : EEwSgmtType.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
	
	
	/**
	 * <pre>
	 * 코드와 상태type으로된 Map Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public static Map<String, EEwSgmtType> getEwSgmtMap(){
		TreeMap<String, EEwSgmtType> ewSgmtMap = new TreeMap<String, EEwSgmtType>();
		for(EEwSgmtType type : EEwSgmtType.values()){
			ewSgmtMap.put(type.getCode(), type);
		}
		return ewSgmtMap;
	}
	
}
