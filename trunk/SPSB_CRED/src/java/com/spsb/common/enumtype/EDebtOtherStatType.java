package com.spsb.common.enumtype;

/**
 * <pre>
 * 채무불이행에서 사용하는 기타 상태 enum class
 * </pre>
 * @author youkyung
 * @since 2014.05.19
 * @version 1.0
 */
public enum EDebtOtherStatType {

	UNAVAIL_REASON("UR", "신청불가사유확인"),
	APPLICATION_REWRITE("AR", "신청서재작성"),
	OFFER_REGISTRATION("OR", "등록신청"),
	FAULT_OCCURRENCE("FO", "과실발생사유확인"),
	EVIDENCE_JUDGEMENT("EJ", "증빙심사완료"),
	REGISTER_AGAIN("RA", "재등록완료");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return해주는 getter 메소드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 19.
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return해주는 getter 메소드
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 19.
	 * @return
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와 설명값을 setting 해주는 채무불이행기타상태타입생성자
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 19
	 * @param code : 코드값, desc : 설명값
	 */
	private EDebtOtherStatType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 채무불이행기타상태 조회 Method
	 * </pre>
	 * @author YouKyung Hong
	 * @since 2014. 5. 19.
	 * @param code : 코드값
	 * @return
	 */
	public static EDebtOtherStatType search(String code) {
		for(EDebtOtherStatType type : EDebtOtherStatType.values()) {
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
}
