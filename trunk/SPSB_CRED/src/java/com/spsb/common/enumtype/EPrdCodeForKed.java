package com.spsb.common.enumtype;

import java.util.LinkedHashMap;
import java.util.Map;

import com.spsb.common.collection.SBox;


/**
 * <pre>
 * KED 상품 코드 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 8. 13.
 * @version 1.0
 */
public enum EPrdCodeForKed {

	COMP_SEARCH_12MONTH("CO12", "C", "기업정보검색"),
	DEBT_CHAG("DEBT", "D", "채무불이행정보 등록"),
	EW_3MONTH("EW03", "W", "조기경보 3개월"),
	EW_6MONTH("EW06", "W", "조기경보 6개월"),
	EW_12MONTH("EW12", "W", "조기경보 12개월"),
	EW_FREE_1MONTH("EWF1", "W", "조기경보 무료1개월");
	
	private String code;	//상품코드
	private String type;	//상품코드
	private String desc;	//설명
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 5.
	 * @version 1.0
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * type을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 5.
	 * @version 1.0
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 8. 5.
	 * @version 1.0
	 * @return
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 상품 코드 생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 24.
	 * @version 1.0
	 * @param code
	 * @param desc
	 */
	private EPrdCodeForKed(String code, String type, String desc){
		this.code = code;
		this.type = type;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는  상품 코드 조회 Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 24.
	 * @version 1.0
	 * @return code : 코드
	 * @return EPrdCodeForKed : 상품 코드 enum
	 */
	public static EPrdCodeForKed search(String code){
		for(EPrdCodeForKed type : EPrdCodeForKed.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
	/**
	 * <pre>
	 * 코드와 상품코드type으로된 Map Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 12. 1.
	 * @version 1.0
	 * @return
	 */
	public static Map<String, SBox> getPrdCodeMap(){
		Map<String, SBox> map = new LinkedHashMap<String, SBox>();
		for(EPrdCodeForKed type : EPrdCodeForKed.values()){
			SBox sBox = new SBox();
			sBox.set("CODE", type.code);
			sBox.set("TYPE", type.type);
			sBox.set("DESC", type.desc);
			map.put( "CD_" + type.code, sBox);
		}
		return map;
	}
}
