package com.spsb.common.enumtype;

import java.util.Map;
import java.util.TreeMap;
/**
 * <pre>
 * 거래처 구분 타입  enum class
 * </pre>
 * @author KIM GA EUN
 * @since 2016. 4. 21.
 * @version 1.0
 */
public enum ETxplClsType {

	TC110("110", "대기업"),
	TC120("120", "외감"),
	TC130("130", "비외감"),
	TC140("140", "소기업"),
	TC150("150", "기타법인"),
	TC160("160", "금융기관"),
	TC199("199", "미분류법인"),
	TC210("210", "개인기장"),
	TC220("220", "개인미기장"),
	TC299("299", "기타개인사업자"),
	TC310("310", "개인");
	
	private String code;
	private String desc;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return code : 코드
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @return desc : 설명
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 EW등급타입생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 */
	private ETxplClsType(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는  EW등급타입 조회 enum Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 7. 22.
	 * @version 1.0
	 * @param code : 코드
	 * @return EEwRatingType : EW등급타입 enum
	 */
	public static ETxplClsType search(String code){
		for(ETxplClsType type : ETxplClsType.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
	
	
	/**
	 * <pre>
	 * 코드와 상태type으로된 Map Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public static Map<String, ETxplClsType> getTxplClsMap(){
		TreeMap<String, ETxplClsType> txplClsMap = new TreeMap<String, ETxplClsType>();
		for(ETxplClsType type : ETxplClsType.values()){
			txplClsMap.put(type.getCode(), type);
		}
		return txplClsMap;
	}
	
}
