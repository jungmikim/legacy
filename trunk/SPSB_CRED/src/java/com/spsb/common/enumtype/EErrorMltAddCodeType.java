package com.spsb.common.enumtype;

/**
 * <pre>
 * 엑셀 대량등록 에러코드 enum class
 * </pre>
 * 
 * @author YouKyung Hong
 * @since 2015. 2. 26.
 * @version 1.0
 */
public enum EErrorMltAddCodeType {

	// 기술료 대량등록 에러코드
	DEBN_EAR01("EAR01","사업자등록번호는 반드시 입력되어야 합니다."),
	DEBN_EAR02("EAR02","기업명은 반드시 입력되어야 합니다."),
	DEBN_EAR03("EAR03","계약관리번호는 반드시 입력되어야 합니다."),
	DEBN_EAR04("EAR04","계약서상태는 반드시 입력되어야 합니다."),
	DEBN_EAR05("EAR05","계약일자는 반드시 입력되어야 합니다."),
	DEBN_EAR06("EAR06","계약금액은 반드시 입력되어야 합니다."),
	DEBN_EAR07("EAR07","기술료관리번호는 반드시 입력되어야 합니다."),
	DEBN_EAR08("EAR08","수금예정일은 반드시 입력되어야 합니다."),
	DEBN_EAR09("EAR09","수금예정금액은 반드시 입력되어야 합니다."),
	DEBN_EAF10("EAF10","사업자등록번호의 형식이 바르지 않습니다. (10자리 숫자)"),
	DEBN_EAF11("EAF11","기업명은 70자 이내로 입력 가능합니다."),
	DEBN_EAF12("EAF12","대표자명은 50자 이내로 입력 가능합니다."),
	DEBN_EAF13("EAF13","계약관리번호는 20자 이내로 입력 가능합니다."),
	DEBN_EAF14("EAF14","중복된 계약관리번호입니다."),
	DEBN_EAF15("EAF15","계약일자의 형식이 바르지 않습니다. (YYYYMMDD)"),
	DEBN_EAF16("EAF16","계약금액의 형식이 바르지 않습니다. (숫자 금액)"),
	DEBN_EAF17("EAF17","기술료관리번호는 20자 이내로 입력 가능합니다."),
	DEBN_EAF18("EAF18","중복된 기술료관리번호입니다."),
	DEBN_EAF19("EAF19","엑셀 파일로 업로드 된 기술료 정보 중에 중복된 기술료관리번호가 존재합니다."),
	DEBN_EAF20("EAF20","수금예정일의 형식이 바르지 않습니다. (YYYYMMDD)"),
	DEBN_EAF21("EAF21","수금예정금액의 형식이 바르지 않습니다. (숫자 금액)"),
	DEBN_EAF22("EAF22","수금일자의 형식이 바르지 않습니다. (YYYYMMDD)"),
	DEBN_EAF23("EAF23","수금금액의 형식이 바르지 않습니다. (숫자 금액)"),
	DEBN_EAF24("EAF24","계약관리번호의 형식이 바르지 않습니다. (영문, 숫자)"),
	DEBN_EAF25("EAF25","기술료관리번호의 형식이 바르지 않습니다. (영문, 숫자)"),
	DEBN_EAF26("EAF26","수금예정일은 계약일보다 이후여야 합니다."),
	DEBN_EAF27("EAF27","수금일은 계약일보다 이후여야 합니다."),
	DEBN_EAF28("EAF28","수금예정금액은 수금금액보다 커야 합니다.");
	
	private String errCode;
	private String errMsg;

	/**
	 * <pre>
	 * 에러코드을 return 해주는 getter Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10.03
	 * @version 1.0
	 * @return errCode : 에러코드
	 */
	public String getErrCode() {
		return errCode;
	}

	/**
	 * <pre>
	 * 에러메세지을 return 해주는 getter Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10.03
	 * @version 1.0
	 * @return errMsg : 에러메세지
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * <pre>
	 * 에러코드와 에러메세지를 setting 해주는 생성자
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10.03
	 * @version 1.0
	 * @return errCode: 에러코드, errMsg : 에러메세지
	 */
	private EErrorMltAddCodeType(String errCode, String errMsg) {
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	/**
	 * <pre>
	 * 에러코드에 해당하는 에러타입 조회 enum Method
	 * </pre>
	 * 
	 * @author sungrangkong
	 * @since 2013. 10.03
	 * @version 1.0
	 * @param errCode
	 *            : 에러코드
	 * @return EErrorCodeType : 에러코드타입 enum
	 */
	public static EErrorMltAddCodeType search(String errCode) {
		for (EErrorMltAddCodeType type : EErrorMltAddCodeType.values()) {
			if (type.getErrCode().equals(errCode)) {
				return type;
			}
		}
		return null;
	}
}
