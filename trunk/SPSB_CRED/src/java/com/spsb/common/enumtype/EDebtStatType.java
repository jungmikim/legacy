package com.spsb.common.enumtype;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * <pre>
 * 채무불이행에서 사용하는 상태 enum class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 3. 19.
 * @version 1.0
 */
public enum EDebtStatType {
	TEMP_SAVED("TS", "임시저장", "신청서 작성 중인 상태 입니다. 신청에 필요한 정보의 일부가 누락된 상태이므로 채무불이행등록 신청을 위하여 추가적인 입력이 필요합니다.", "U", 1),
	SAVED_COMPLETE("SC", "저장완료(신청가능)", "신청서 작성이 완료되어, 채무불이행등록 신청이 가능한 상태입니다.", "U", 1),
	ACCEPT_WAITING("AW", "심사대기", "신청서 접수가 완료되어, 신용정보업체의 접수를 기다리고 있는 상태입니다.", "U", 2),
	ACCEPT_CANCEL("AC", "신청취소", "신청이 완료되었으나, 고객께서 직접 접수를 취소하신 상태입니다.", "U", 3),
	EVALUATING("EV", "심사중", "신용정보업체에서 접수를 완료하여, 채무불이행등록을 하기 위해 서류 심사가 진행 중인 상태입니다. 서류 심사 기간은 최대 3일까지 예상될 수 있습니다.", "K", 2),
	ACCEPT_UNAVAIL("AU", "신청불가", "신청서류 등의 문제로 접수를 진행할 수 없는 상태입니다. 접수 불가 사유를 확인하시고, 고객센터에 전화 문의 하시기 바랍니다. 해당화면의 프로세스는 종료 되었으며, 신청불가 사유를 확인하시고, 신청서를 재작성하시면 다시 채무불이행 서비스를 진행하실 수 있습니다.", "K", 3),
	ACCEPT_AVAIL("AA", "결제대기", "신청서 심사가 완료되어, 고객님의 수수료 결제를 기다리고 있는 상태입니다. 결제를 완료하시면 채무불이행등록 절차가 즉시 진행됩니다.", "K", 2),
	PAYMENT("PY", "통보서준비", "고객님의 소중한 결제에 감사드립니다. 곧 채무불이행등록이 진행됩니다.", "U", 2),
	KED_FAIL("KF", "심사과실발생", "고객님 죄송합니다. 신용정보업체의 업무상 과실로 더 이상 채무불이행등록 절차를 진행할 수 없게 되었습니다. 고객센터와의 전화 문의를 통해 해결해 드리겠습니다.해당화면의 프로세스는 종료 되었으며, 과실발생 사유를 확인하시고, 신청서를 재작성하시면 다시 채무불이행 서비스를 진행하실 수 있습니다.", "K", 3),
	NOTICE("NT", "통보서발송", "채무불이행등록을 하기 위해 채무자에 해당 서류를 통보한 상태입니다. 채무 업체에서의 민원이 발생하지 않는다면 최대 30일 안에 채무불이행등록 절차가 진행될 예정입니다.", "K", 2),
	COMPLAINT_A("CA", "민원발생", "고객님께서 신청하신 채무불이행등록의 진행 중에 채무자께서 민원을 제시하였습니다. 민원에 대한 상세 사항을 확인하시고, 고객센터와의 전화 문의를 통해 추가 증빙 등의 절차를 진행하시면 됩니다.", "K", 2),
	COMPLAINT_FILE_A("FA", "추가증빙제출", "고객님이 채무자의 민원에 대해 증빙 서류를 추가로 제시하신 상태입니다. 추가로 증빙된 서류를 검토하여 채무불이행등록 절차를 계속 진행합니다.", "U", 2),
	REG_COMPLETE("RC", "등록완료", "고객님께서 신청하신 채무불이행등록이 완료되었습니다.", "K", 2),
	COMPLAINT_B("CB", "민원발생", "채무불이행등록된 채무자께서 민원을 제시하였습니다. 그로 이해 채무불이행등록이 잠시 대기된 상태가 되었습니다. 채무불이행의 민원에 대한 상세 사항을 확인하시고, 고객센터와의 전화 문의를 통해 추가 증빙 등의 절차를 진행하셔야 채무불이행등록 상태가 유지될 수 있습니다.", "K", 2),
	COMPLAINT_FILE_B("FB", "추가증빙제출", "고객님이 채무자의 민원에 대해 증빙 서류를 추가로 제시하신 상태입니다. 추가로 증빙된 서류를 검토하여 채무불이행등록 상태가 유지될 수 있도록 심사 중입니다.", "U", 2),
	REQ_REMOVE("RR", "해제요청", "고객님께서 채무불이행등록을 해제요청하신 상태입니다. 해제가 즉시 처리됩니다.", "U", 2),
	COMPLETE_REMOVE("CR", "해제완료", "채무불이행등록이 해제되었습니다. 이용해주셔서 감사합니다.", "K", 3),
	REG_UNAVAIL("RU", "등록불가", "채무불이행등록이 불가한 상태입니다. 불가 사유를 확인하시고, 고객센터에 전화 문의 하시기 바랍니다.", "K", 3);
	
	private String code;
	private String desc;
	private String text;
	private String actor;
	private int menu;
	
	/**
	 * <pre>
	 * 코드를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * <pre>
	 * 설명을 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public String getDesc() {
		return desc;
	}
	
	/**
	 * <pre>
	 * 상세설명를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 5. 12.
	 * @version 1.0
	 * @return
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * <pre>
	 * 행위자를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public String getActor() {
		return actor;
	}
	
	/**
	 * <pre>
	 * 출력하는 Menu번호를 return 해주는 getter Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public int getMenu() {
		return menu;
	}
	
	/**
	 * <pre>
	 * 코드와  설명값을 setting 해주는 채무불이행상태타입생성자
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return code : 코드
	 * @param desc : 설명
	 */
	private EDebtStatType(String code, String desc, String text, String actor, int menu){
		this.code = code;
		this.desc = desc;
		this.text = text;
		this.actor = actor;
		this.menu = menu;
	}
	
	/**
	 * <pre>
	 * 코드에 해당하는 채무불이행상태 조회 Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return code : 코드
	 * @return EDebtStatType : 채무불이행상태타입 enum
	 */
	public static EDebtStatType search(String code){
		for(EDebtStatType type : EDebtStatType.values()){
			if(type.getCode().equals(code)){
				return type;
			}
		}
		return null;
	}
	
	/**
	 * <pre>
	 * Menu별 나올수있는 채무불이행상태조회 Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @param menu
	 * @return
	 */
	public static List<EDebtStatType> getDebtStatList(int menu){
		List<EDebtStatType> debtStatList = new ArrayList<EDebtStatType>();
		for(EDebtStatType type : EDebtStatType.values()){
			if(type.getMenu() == menu){
				debtStatList.add(type);
			}
		}
		return debtStatList;
	}
	
	/**
	 * <pre>
	 * 코드와 상태type으로된 Map Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 19.
	 * @version 1.0
	 * @return
	 */
	public static Map<String, EDebtStatType> getDebtStatMap(){
		TreeMap<String, EDebtStatType> debtStatMap = new TreeMap<String, EDebtStatType>();
		for(EDebtStatType type : EDebtStatType.values()){
			debtStatMap.put(type.getCode(), type);
		}
		return debtStatMap;
	}
	
	
}
