package com.spsb.common.dto;

import com.spsb.common.util.SysUtil;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class MessageTag {
	 private String messageTagId;
	  private String instanceId;
	  private String referenceId;
	  private String groupId;
	  private String actionType;
	  private String senderId;
	  private String senderName;
	  private String receiveId;
	  private String receiveName;
	  private String serviceCode;
	  private String messageTypeCode;
	  private String responseType;
	  private String documentType;
	  private String documentData;
	  private String messageStatus;
	  private String direction;
	  private String docCreationDateTime;
	  private int fileCount;
	  private long totalSize;
	  private String resultCode;
	  private String resultMessage;
	  private String adapterKind;

	  public MessageTag()
	  {
	    this.messageTagId = SysUtil.uuid();
	  }

	  public MessageTag(String messageTagId) {
	    this.messageTagId = messageTagId;
	  }

	  public String getMessageTagId() {
	    return this.messageTagId;
	  }
	  public void setMessageTagId(String messageTagId) {
	    this.messageTagId = messageTagId;
	  }
	  public String getInstanceId() {
	    return this.instanceId;
	  }
	  public void setInstanceId(String instanceId) {
	    this.instanceId = instanceId;
	  }
	  public String getReferenceId() {
	    return this.referenceId;
	  }
	  public void setReferenceId(String referenceId) {
	    this.referenceId = referenceId;
	  }
	  public String getSenderId() {
	    return this.senderId;
	  }
	  public void setSenderId(String senderId) {
	    this.senderId = senderId;
	  }
	  public String getSenderName() {
	    return this.senderName;
	  }
	  public void setSenderName(String senderName) {
	    this.senderName = senderName;
	  }
	  public String getReceiveId() {
	    return this.receiveId;
	  }
	  public void setReceiveId(String receiveId) {
	    this.receiveId = receiveId;
	  }
	  public String getReceiveName() {
	    return this.receiveName;
	  }
	  public void setReceiveName(String receiveName) {
	    this.receiveName = receiveName;
	  }
	  public String getServiceCode() {
	    return this.serviceCode;
	  }
	  public void setServiceCode(String seviceCode) {
	    this.serviceCode = seviceCode;
	  }
	  public String getMessageTypeCode() {
	    return this.messageTypeCode;
	  }
	  public void setMessageTypeCode(String messageTypeCode) {
	    this.messageTypeCode = messageTypeCode;
	  }
	  public String getDocumentType() {
	    return this.documentType;
	  }
	  public void setDocumentType(String documentType) {
	    this.documentType = documentType;
	  }
	  public String getDocumentData() {
	    return this.documentData;
	  }
	  public void setDocumentData(String documentData) {
	    this.documentData = documentData;
	  }
	  public String getMessageStatus() {
	    return this.messageStatus;
	  }
	  public void setMessageStatus(String messageStatus) {
	    this.messageStatus = messageStatus;
	  }
	  public String getDirection() {
	    return this.direction;
	  }
	  public void setDirection(String direction) {
	    this.direction = direction;
	  }
	  public int getFileCount() {
	    return this.fileCount;
	  }
	  public void setFileCount(int fileCount) {
	    this.fileCount = fileCount;
	  }
	  public long getTotalSize() {
	    return this.totalSize;
	  }
	  public void setTotalSize(long totalSize) {
	    this.totalSize = totalSize;
	  }

	  public void setResponseType(String responseType) {
	    this.responseType = responseType;
	  }

	  public String toString()
	  {
	    return "MessageTag [messageTagId=" + this.messageTagId + ", instanceId=" + this.instanceId + ", referenceId=" + this.referenceId + ", senderId=" + this.senderId + ", senderName=" + this.senderName + 
	      ", receiveId=" + this.receiveId + ", receiveName=" + this.receiveName + ", serviceCode=" + this.serviceCode + ", messageTypeCode=" + this.messageTypeCode + ", documentType=" + 
	      this.documentType + ", documentData=" + this.documentData + ", messageStatus=" + this.messageStatus + ", direction=" + this.direction + ", fileCount=" + this.fileCount + ", totalSize=" + 
	      this.totalSize + "]";
	  }
	  public String getResponseType() {
	    return this.responseType;
	  }

	  public String getResultCode() {
	    return this.resultCode;
	  }

	  public void setResultCode(String resultCode) {
	    this.resultCode = resultCode;
	  }

	  public String getResultMessage() {
	    return this.resultMessage;
	  }

	  public void setResultMessage(String resultMessage) {
	    this.resultMessage = resultMessage;
	  }

	  public String getDocCreationDateTime() {
	    return this.docCreationDateTime;
	  }

	  public void setDocCreationDateTime(String docCreationDateTime) {
	    this.docCreationDateTime = docCreationDateTime;
	  }

	  public String getGroupId()
	  {
	    return this.groupId;
	  }

	  public void setGroupId(String groupId) {
	    this.groupId = groupId;
	  }

	  public String getActionType()
	  {
	    return this.actionType;
	  }

	  public void setActionType(String actionType) {
	    this.actionType = actionType;
	  }

	  public String getAdapterKind() {
	    return this.adapterKind;
	  }

	  public void setAdapterKind(String adapterKind) {
	    this.adapterKind = adapterKind;
	  }
}
