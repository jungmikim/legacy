package com.spsb.common.dto;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 18.
 * @version 1.0
 */

public class EncryptInfo
{
  boolean encrypted;
  String algorithm;
  byte[] securityToken;

  public String getAlgorithm()
  {
    return this.algorithm;
  }
  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }
  public byte[] getSecurityToken() {
    return this.securityToken;
  }

  public String getSecurityTokenString() {
    return null;
  }

  public void setSecurityToken(byte[] securityToken) {
    this.securityToken = securityToken;
  }

  boolean isEncrypted() {
    return this.encrypted;
  }

  void setEncrypted(boolean encInfo) {
    this.encrypted = encInfo;
  }
}