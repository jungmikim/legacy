package com.spsb.common.dto;

import com.spsb.common.util.SysUtil;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class FileData {

	  private String fileDataId;
	  private String messageTagId;
	  private int messageTagSeq;
	  private String fileName;
	  private byte[] fileData;
	  private long fileSize;
	  private String fileSeq;
	  private String fileDesc;

	  public FileData()
	  {
	    this.fileDataId = SysUtil.uuid();
	  }

	  public String getFileDataId() {
	    return this.fileDataId;
	  }

	  public void setFileDataId(String fileDataId) {
	    this.fileDataId = fileDataId;
	  }

	  public String getMessageTagId() {
	    return this.messageTagId;
	  }

	  public void setMessageTagId(String messageTagId) {
	    this.messageTagId = messageTagId;
	  }

	  public int getMessageTagSeq() {
	    return this.messageTagSeq;
	  }

	  public void setMessageTagSeq(int messageTagSeq) {
	    this.messageTagSeq = messageTagSeq;
	  }

	  public byte[] getFileData() {
	    return this.fileData;
	  }

	  public void setFileData(byte[] fileData) {
	    this.fileData = fileData;
	  }

	  public long getFileSize() {
	    return this.fileSize;
	  }

	  public void setFileSize(long fileSize) {
	    this.fileSize = fileSize;
	  }

	  public String getFileName() {
	    return this.fileName;
	  }

	  public void setFileName(String fileName) {
	    this.fileName = fileName;
	  }

	  public String getFileSeq() {
	    return this.fileSeq;
	  }

	  public void setFileSeq(String fileSeq) {
	    this.fileSeq = fileSeq;
	  }

	  public String getFileDesc() {
	    return this.fileDesc;
	  }

	  public void setFileDesc(String fileDesc) {
	    this.fileDesc = fileDesc;
	  }
}
