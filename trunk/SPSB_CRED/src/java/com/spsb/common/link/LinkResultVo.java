package com.spsb.common.link;

/**
 * <pre>
 * 연계 message 결과 Vo Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 9. 3.
 * @version 1.0
 */
public class LinkResultVo {

	private int connectionResult;	//HttpURLConnection ResponseCode(200 이면 성공 , 그외에는 실패)
	private String connectionMessage;	//HttpURLConnection ResponseMessage
	private String resultCode;		//message 결과코드 ( 성공 : SCMN01, 이외 실패 )
	private String resultMessage;	//message 결과메시지 
	
	public int getConnectionResult() {
		return connectionResult;
	}
	public void setConnectionResult(int connectionResult) {
		this.connectionResult = connectionResult;
	}
	public String getConnectionMessage() {
		return connectionMessage;
	}
	public void setConnectionMessage(String connectionMessage) {
		this.connectionMessage = connectionMessage;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
