package com.spsb.common.interceptor;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EPrdCodeForKed;
import com.spsb.common.session.GrantManager;
import com.spsb.common.session.SessionManager;
import com.spsb.common.session.SessionVo;
import com.spsb.service.login.LoginService;

/**
 * <pre>
 * Controller 호출 전 Handler를 통한 Filter Interceptor Class
 * </pre>
 */
@Controller
public class EventCheckInterceptor extends HandlerInterceptorAdapter {

	private String DEBN_ATTRIBUTE_NAME = SessionManager.DEBN_ATTRIBUTE_NAME;
	//private String GUEST_DEBN_ATTRIBUTE_NAME = SessionManager.GUEST_DEBN_ATTRIBUTE_NAME;
	
	
	
	@Autowired
	private LoginService loginService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	/**
	 * <pre>
	 * Controller 호출전 실행 메소드
	 * </pre>
	 * @param request
	 * @param response
	 * @param handler
	 * @return
	 * @throws Exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		SBox sBox = new SBox();
		SessionManager sessionManager = SessionManager.getInstance();
		String isSbDebnSession = null;
		String uri = request.getRequestURI();
		SessionVo sessionVo = sessionManager.getCurrentSessionVo(request);
		String sessionttributeName = sessionManager.getSessionAttributeName();
		GrantManager grantManager = GrantManager.getInstance();
		log.info(getLogMessage("interceptor", "uri", uri));
		
		//API 연계URL check
		/*if(grantManager.apiCheckUrl(uri)){
			return true;
		}*/
				
		// USER의 SESSION 정보를 가져와서 sBox에 Setting
		if(sessionVo != null){
			if(DEBN_ATTRIBUTE_NAME.equals(sessionttributeName)){
				sBox = sessionManager.sessionVoToSBox(sessionVo, sBox);
				isSbDebnSession = "Y";
			}/*else if(GUEST_DEBN_ATTRIBUTE_NAME.equals(sessionttributeName)){
				sBox = sessionManager.sessionVoToSBoxForGuest(sessionVo, sBox);
				isSbDebnSession = "N";
			}*/
		}
		if(DEBN_ATTRIBUTE_NAME.equals(sessionttributeName)){
			//로그인이 필요한 url check
			if(!grantManager.loginCheckUrl(sessionVo, uri)){
				response.sendRedirect(request.getContextPath() + "/login/loginForm.do");
				return false;
			}
			//menu권한이 필요한 url check
			/*if(sessionVo != null){
				log.info(getLogMessage("interceptor", "loginId", sessionVo.getSessionLoginId()));
				EMenuNoGrantType type = grantManager.menuGrantCheckUrl(sessionVo, uri);
				if(type != null){
					response.sendRedirect(request.getContextPath() + type.getReturnUrl());
					return false;
				}
			}else{
				response.sendRedirect(request.getContextPath() + "/login/loginForm.do");
				return false;
			}*/
			
			//권한이 필요한 url check
			/*if(sessionVo != null && !grantManager.grantCheckUrl(sessionVo, uri)){
				response.sendRedirect(request.getContextPath() + "/login/notGrant.do");
				return false;
			}*/
			
			//채무불이행 권한이 필요한 url check
			/*if(sessionVo != null && !grantManager.debtGrantCheckUrl(sessionVo, uri)){
				response.sendRedirect(request.getContextPath() + "/login/notDebtGrant.do");
				return false;
			}*/
		}/*else if(GUEST_DEBN_ATTRIBUTE_NAME.equals(sessionttributeName)){
			//url check
			if(!grantManager.loginCheckUrlForGuest(sessionVo, uri)){
				response.sendRedirect(request.getContextPath() + "/user/guestUserJoin.do");
				return false;
			}
			
			//menu권한이 필요한 url check
			if(sessionVo != null){
				EMenuNoGrantType type = grantManager.menuGrantCheckUrlForGuest(sessionVo, uri);
				if(type != null){
					String returnUrl = request.getContextPath() + type.getReturnGuestUrl() + "?message=" + (EUserErrorCode.GUEST_NOT_GRANT).getCode();
					response.sendRedirect(returnUrl);
					return false;
				}
			}else{
				response.sendRedirect(request.getContextPath() + "/user/guestUserJoin.do");
				return false;
			}
		}*/else{
			//로그인이 필요한 url check
			if(!grantManager.loginCheckUrl(sessionVo, uri)){
				response.sendRedirect(request.getContextPath() + "/login/loginForm.do");
				return false;
			}
		}
					
		Map<String, Object> requestMap = request.getParameterMap();
		Iterator<String> it = requestMap.keySet().iterator();

		// [1] Request 객체로 부터 Prameter 정보를 SBox로 셋팅함.
		while (it.hasNext()) {
			try {
				String key = (String) it.next();
				if ((requestMap.get(key) instanceof String[])) {
					String[] values = (String[]) requestMap.get(key);

					if (values != null && values.length == 1) {
						sBox.set(key, values[0]);
					} else {
						sBox.set(key, values);
					}
				} else {
					sBox.set(key, requestMap.get(key));
				}
			} catch (Exception ex) {
			}
		}

		// [2] 추가적인 정보를 SBox 에 셋팅함
		sBox.set("remoteHost", request.getRemoteHost());
		sBox.set("remoteAddr", request.getRemoteAddr());
		sBox.set("remoteURI", uri);
		sBox.set("productDebtCode", EPrdCodeForKed.DEBT_CHAG.getCode());
		sBox.set("isSbDebnSession", isSbDebnSession);

		request.setAttribute("sBox", sBox);
		return true;
	}
	
	/**
	 * <pre>
	 * Exception처리
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2015. 1. 13.
	 * @version 1.0
	 * @param request
	 * @param response
	 * @param handler
	 * @param ex
	 * @throws Exception
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response
			, Object handler, Exception ex) throws Exception {
		if(ex != null){
			String uri = request.getRequestURI();
			log.error(getLogMessage("afterCompletion", "ERROR_URI", uri));
//			log.error(getLogMessage("afterCompletion", "ERROR_HANDLER", handler.getClass().getName()));
    		log.error(getLogMessage("afterCompletion", "ERROR", ex.toString()));
    		StringBuffer stringbuf = new StringBuffer();
    		for (StackTraceElement element : ex.getStackTrace()) {
    			stringbuf.append("\tat" + element.toString());
    			stringbuf.append("\n");
    		}
    		log.error(stringbuf.toString());
		}
	}
	
	/**
	 * <pre>
	 * 공통으로 사용하는 로그 메세지 형식
	 * </pre>
	 * 
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 10.
	 * @version 1.0
	 * @param methodName : 메소드 이름
	 * @param messageName : 메세지명
	 * @param messageValue : 메세지값
	 * @return
	 */
	private String getLogMessage(String methodName, String messageName, String messageValue) {
		return "xxxxxxxxxx[ " + methodName + " ]xxxxxxxxxx( " + messageName + " : " + messageValue + " )xxxxxxxxxx";
	}
	
}
