package com.spsb.common.message;

import java.util.ArrayList;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.util.JAXBUtil;
import com.spsb.schema.credit.request.amend.ReferencedDefaultAmendmentDocumentType;
import com.spsb.schema.credit.request.register.AddressCommunicationType;
import com.spsb.schema.credit.request.register.AuthorizedDefaultApplicantPartyType;
import com.spsb.schema.credit.request.register.AuthorizedDefaultCustomerPartyType;
import com.spsb.schema.credit.request.register.CreditRequestRegisterDefaultApplicationType;
import com.spsb.schema.credit.request.register.DefaultApplicationRegistrationRequestDocumentType;
import com.spsb.schema.credit.request.register.DefaultMemberPersonType;
import com.spsb.schema.credit.request.register.MemberAddressType;
import com.spsb.schema.credit.request.register.NumberCommunicationType;
import com.spsb.schema.credit.request.register.OwnerPersonType;
import com.spsb.schema.credit.request.register.ReferencedDefaultDocumentType;
import com.spsb.schema.credit.response.CreditResponseRegisterDefaultApplicationType;
import com.spsb.schema.cust.ew.request.AuthorizedInsertEarlyWarningCustomerPartyType;
import com.spsb.schema.cust.ew.request.CreditRequestRegisterEarlyWarningCustomerType;
import com.spsb.schema.cust.ew.request.EarlyWarningCustomerRequestDocumentType;
import com.spsb.schema.cust.ew.request.ReferencedEarlyWarningCustomerInsertDocumentType;
import com.spsb.schema.cust.ew.request.RequestDocumentType;
import com.spsb.schema.cust.ew.request.UserBusinessAccountType;
import com.spsb.schema.cust.ew.request.user.CreditRequestUserListType;
import com.spsb.schema.cust.ew.request.user.UserListRequestDocumentType;
import com.spsb.schema.cust.ew.response.CreditResponseRegisterEarlyWarningCustomerType;
import com.spsb.schema.cust.ew.response.user.CreditResponseUserListType;
import com.spsb.schema.notice.conclusion.NoticeConclusionType;
import com.spsb.schema.notice.conclusion.ResponseDocumentType;
import com.spsb.schema.notice.conclusion.ResponseResultDocumentType;
import com.spsb.schema.notice.credit.updatestatus.CreditNoticeDefaultUpdateStatusType;
import com.spsb.schema.notice.receipt.NoticeReceiptType;
import com.spsb.vo.DebtAmendRequestVo;
import com.spsb.vo.DebtRegisterRequestVo;
import com.spsb.vo.EWCustRequestVo;
import com.spsb.vo.RequestCommonVo;
import com.spsb.vo.ResponseDocumentCommonVo;

/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
public class CommonMessageIF {
	
	/**
	 * <pre>
	 *  [거래처-request]조기경보 대상 거래처 신규등록  XML메시지 (CRQREC)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 29.
	 * @version 1.0
	 * @param commonVo
	 * @param requstVo
	 * @return
	 */
	public static String makeEwCustRequestMessageForXML(RequestCommonVo commonVo, EWCustRequestVo requstVo ) {
    	//가상의 배열및 리스트에 데이터 add
		String xmlString = null;
		
		CreditRequestRegisterEarlyWarningCustomerType  requestEwCust = new CreditRequestRegisterEarlyWarningCustomerType();
		
		//요청 문서 정보 영역
		RequestDocumentType requsetDoc = new RequestDocumentType();
		
		if(commonVo!=null){
			requsetDoc.setID(commonVo.getID());//메시지 교유의 식별 번호
			requsetDoc.setTypeCode(commonVo.getRequestDocumentTypeCode());//유형 코드. 고정값: CRQREC 
			requsetDoc.setVersionInformation(commonVo.getVersionInformation()); // 문서 버전. 고정값: 1.0.0
			requsetDoc.setDescription(commonVo.getDescription()); //문서 비고 내역
			requestEwCust.setRequestDocument(requsetDoc);
			
			//계정 정보 영역
			UserBusinessAccountType userBusinessAccount = new UserBusinessAccountType();
			userBusinessAccount.setTypeCode(commonVo.getUserBusinessAccountTypeCode()); //E: ERP회원
			userBusinessAccount.setUserID(commonVo.getUserID()); //회원 식별자  TODO(Legacy 회원식별자 아니면 스마트채권의 회원식별자 확인필요)
			userBusinessAccount.setLoginID(commonVo.getLoginID()); //로그인식별자 TODO(Legacy 로그인아이디 아니면 스마트채권의 아이디인지 확인필요)
			userBusinessAccount.setLoginPasswordID(commonVo.getLoginPasswordID());
			requestEwCust.setUserBusinessAccount(userBusinessAccount);
		}else{
			//CommonVo
		}
		
		//요청 정보 영역
		EarlyWarningCustomerRequestDocumentType ewCustRequestDoc = new EarlyWarningCustomerRequestDocumentType();
		//신규등록정보
		
		AuthorizedInsertEarlyWarningCustomerPartyType ewCustParty = new AuthorizedInsertEarlyWarningCustomerPartyType();
		ReferencedEarlyWarningCustomerInsertDocumentType ewCustInsertDoc = new ReferencedEarlyWarningCustomerInsertDocumentType();
		
		if(requstVo != null){
			ewCustParty.setID(requstVo.getId());
			ewCustParty.setFolderName(requstVo.getFolderName());
			ewCustInsertDoc.setAuthorizedInsertEarlyWarningCustomerParty(ewCustParty);
			ewCustRequestDoc.setReferencedEarlyWarningCustomerInsertDocument(ewCustInsertDoc);
			requestEwCust.setEarlyWarningCustomerRequestDocument(ewCustRequestDoc);
			xmlString = JAXBUtil.marshall2String(CreditRequestRegisterEarlyWarningCustomerType.class, requestEwCust);
		}else{
			//requstVo
		}
		
		return xmlString;
	}
	
	
	////////////////////////////test start//메시지 잘꺼내지나 테스트 TEST CODE 운영배포시 삭제 /////
	public static SBox getEwCustRequestMessageForXML(String xmlFile) {
		
		SBox sBox = new SBox();
		try{
			//정보 불러오는 영역
			Object obj = JAXBUtil.unmarshallFromString(CreditRequestRegisterEarlyWarningCustomerType.class, xmlFile);
			
			String id = ((CreditRequestRegisterEarlyWarningCustomerType) obj).getEarlyWarningCustomerRequestDocument().getReferencedEarlyWarningCustomerInsertDocument().getAuthorizedInsertEarlyWarningCustomerParty().getID();
			String folderName = ((CreditRequestRegisterEarlyWarningCustomerType) obj).getEarlyWarningCustomerRequestDocument().getReferencedEarlyWarningCustomerInsertDocument().getAuthorizedInsertEarlyWarningCustomerParty().getFolderName();
			
			sBox.set("id", id);
			sBox.set("folderName", folderName);
			System.out.println(sBox);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return sBox;
	}
	
	////////////////test start//메시지 잘꺼내지나 테스트 TEST CODE 운영배포시 삭제//////////////
	
	
	/**
	 * <pre>
	 *  [거래처-response]신규 거래처 등록 성공 메시지 (CRSREC)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 4.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static SBox getEwCustRegistorReponseMessageForXML(String xmlFile) {
		
		SBox sBox = new SBox();
		try{
			//정보 불러오는 영역
			Object obj = JAXBUtil.unmarshallFromString(CreditResponseRegisterEarlyWarningCustomerType.class, xmlFile);
			
			String id = ((CreditResponseRegisterEarlyWarningCustomerType) obj).getResponseResultDocument().getID();
			String typeCode = ((CreditResponseRegisterEarlyWarningCustomerType) obj).getResponseResultDocument().getTypeCode();
			String description = ((CreditResponseRegisterEarlyWarningCustomerType) obj).getResponseResultDocument().getDescription();
			
			sBox.set("id", id);
			sBox.set("typeCode", typeCode);
			sBox.set("description", description);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return sBox;
	}
	
	
	/**
	 * <pre>
	 *  [거래처]거래처 신용등급 변경 통지서: 비동기식 XML파싱(CNTUCL)
	 *  신용 등급 변경 통지서(Only 조기경보 - CNTUCL
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static SBoxList<SBox> getCreditNoticeUpdateCustomerLevelForXML(String xmlFile) {
		
		SBoxList<SBox>	responseList = new SBoxList<SBox>();		
		String strArrayList[] = null;
		String strArrayDatas[] = null;
		String strLabelPd[] ={"custNo","kedCd","crdUpdDt","ewCd","ewRating","E1","E2","E3","E4","E5","E6","E7","E8","E9","E10"};
		//ex) 거래처사업자번호|KED_CD|REG_DT|EW_CD|EW_RATING|Extend01|Extend02|Extend03|Extend04|Extend05|Extend06|Extend07|Extend08|Extend09|Extend10@
		
			if(!xmlFile.toString().isEmpty()){
				strArrayList=xmlFile.toString().split("@");
			}
		
			if((strArrayList!=null)){
				for (int i = 0; i < strArrayList.length; i++) {
					SBox sData = new SBox();
					strArrayDatas = strArrayList[i].split("\\|");
					for (int j = 0; j < strArrayDatas.length; j++) {
						sData.setIfEmpty(strLabelPd[j], strArrayDatas[j]); // Label과 sData값 매핑해줌.
					}
					responseList.add(i, sData); //List에 sBox 추가
				}
			}
		
		return responseList;
		
	}
	
	/**
	 * <pre>
	 * [거래처]거래처 신용등급 변경 통지서: 비동기식 XML파싱(CNTUCL)
	 * 신용 등급 변경 통지서(조기경보 + KLINK) - CNTKCL
	 *  '@' 단위로 잘라서 EW10 메시지와 EW30메시지를 구별.
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 26.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static ArrayList<String> getCreditNoticeByKLinkUpdateCustomerLevelForXML(String xmlFile) {
		
		ArrayList<String>	responseList = new ArrayList<String>();		
		String strArrayList[] = null;
			if(!xmlFile.toString().isEmpty()){
				strArrayList = xmlFile.toString().split("@");
			}
		
			if((strArrayList!=null) && (strArrayList.length==2)){
				for (int i = 0; i < strArrayList.length; i++) {
					String sData = strArrayList[i];
					responseList.add(i, sData); //List에 sBox 추가
				}
			}else{
				System.out.println("형식이 잘못되었습니다.");
			}
		return responseList;
	}
	
	/**
	 * <pre>
	 * 	[거래처]거래처 신용등급 변경 통지서: 비동기식 XML파싱(CNTUCL)
	 *  신용 등급 변경 통지서(조기경보 + KLINK) - CNTKCL
	 *  'EW10, W30메시지에 각각맞게 StrLabel로 매칭해서 Return해주는 메소드
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 27.
	 * @version 1.0
	 * @param strLabel
	 * @param message
	 * @return
	 */
	public static SBoxList<SBox> getCreditNoticeByKLinkUpdateCustomerLevelForFormat (String[] strLabel,  String message){
		SBoxList<SBox>	responseList = new SBoxList<SBox>();	
		String strArrayList[] = null;
		String strArrayDatas[] = null;
		
			if(!message.isEmpty()){
				strArrayList=message.split("#");
			}
		
			if((strArrayList!=null)){
				for (int i = 0; i < strArrayList.length; i++) {
					SBox sData = new SBox();
					strArrayDatas = strArrayList[i].split("\\|");
					for (int j = 0; j < strArrayDatas.length; j++) {
						sData.setIfEmpty(strLabel[j], strArrayDatas[j]); // Label과 sData값 매핑해줌.
					}
					responseList.add(i, sData); //List에 sBox 추가
				}
			}
		
		return responseList;
	}
	
	///////////////////////////////////////////////////////////////////////////////채무불이행 /////////////////////////////////////////////////////////////////////////////
	
	/**
	 * <pre>
	 * [채무불이행-request] 스마트채권 서비스 회원목록 요청  - 채무불이행 신청서 작성  전 처리작업 XML 메시지 (동기)(CRQUSL)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 2.
	 * @version 1.0
	 * @param commonVo
	 * @param requstVo
	 * @return
	 */
	public static String makeCreditRequestUserMessageForXML(RequestCommonVo commonVo, EWCustRequestVo requstVo ) {
    	//가상의 배열및 리스트에 데이터 add
		String xmlString = null;
		
		CreditRequestUserListType  requestUser = new CreditRequestUserListType();
		
		//요청 문서 정보 영역
		com.spsb.schema.cust.ew.request.user.RequestDocumentType requsetDoc = new com.spsb.schema.cust.ew.request.user.RequestDocumentType();
		
		if(commonVo!=null){
			requsetDoc.setID(commonVo.getID());//메시지 교유의 식별 번호
			requsetDoc.setTypeCode(commonVo.getRequestDocumentTypeCode());//유형 코드. 고정값: CRQREC 
			requsetDoc.setVersionInformation(commonVo.getVersionInformation()); // 문서 버전. 고정값: 1.0.0
			requsetDoc.setDescription(commonVo.getDescription()); //문서 비고 내역
			requestUser.setRequestDocument(requsetDoc);
			
		//계정 정보 영역
		com.spsb.schema.cust.ew.request.user.UserBusinessAccountType userBusinessAccount = new com.spsb.schema.cust.ew.request.user.UserBusinessAccountType();
			userBusinessAccount.setTypeCode(commonVo.getUserBusinessAccountTypeCode()); //E: ERP회원
			userBusinessAccount.setUserID(commonVo.getUserID()); //회원 식별자  TODO(Legacy 회원식별자 아니면 스마트채권의 회원식별자 확인필요)
			userBusinessAccount.setLoginID(commonVo.getLoginID()); //로그인식별자 TODO(Legacy 로그인아이디 아니면 스마트채권의 아이디인지 확인필요)
			userBusinessAccount.setLoginPasswordID(commonVo.getLoginPasswordID());
			requestUser.setUserBusinessAccount(userBusinessAccount);
		}else{
			//CommonVo
		}
		
		//요청 정보 영역
		UserListRequestDocumentType userRequestDoc = new UserListRequestDocumentType();
		
		//신규등록정보
		if(requstVo != null){
			userRequestDoc.setID(requstVo.getId());
			requestUser.setUserListRequestDocument(userRequestDoc);
			xmlString = JAXBUtil.marshall2String(CreditRequestUserListType.class, requestUser);
		}else{
			//requstVo
		}
		
		return xmlString;
	}
	
	
	
	/**
	 * <pre>
	 * [채무불이행-response] 스마트채권 서비스 회원목록 응답  - 크레딧서비스 회원목록응답서 /채무불이행 신청서 작성  전 처리작업 XML 메시지 (동기) (CRSUSL)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 15.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	/*public static SBoxList<SBox> getCrecditUsrListReponseMessage(StandardBusinessDocument response) {
		
		StandardBusinessDocumentHeader msgHeader = response.getStandardBusinessDocumentHeader().getValue();
		DocumentIdentification docIdentity = msgHeader.getDocumentIdentification();
		
		String senderId = msgHeader.getSender().getID();
		String senderName = msgHeader.getSender().getName();
		
		ArrayOfReceiver receivers = msgHeader.getReceiver();
		String receiverId = receivers.getReceiver().get(0).getID();
		String receiverName = receivers.getReceiver().get(0).getName();
		
		
		String xmlFile = response.getMessage().getValue();
		//String xmlFile = response.getMessage().getValue();
		
		SBoxList<SBox>	responseList = new SBoxList<SBox>();
		SBox sBox = new SBox();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(CreditResponseUserListType.class, xmlFile);

		int listSize = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().size();
		
		//xmlFile m개 값으로 Return 갯수만큼 
		for(int i=0; i<=listSize; i++){
			String id = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getID();
			String loginId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getLoginID();
			String compUserId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getCompanyUserID();
			String name = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getName();
			String localNumber = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getTelephoneNumberCommunication().getLocalNumber();
			String urlId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getEmailAddressCommunication().getURIID();
			
			sBox.set("id", id); //스마트 채권 개이 회원 식별자
			sBox.set("loginId", loginId); //스마트채권 로그인 아이디
			sBox.set("compUserId", compUserId); //기업회원 식별자
			sBox.set("name", name); //회원이름
			sBox.set("localNumber", localNumber); //전화번호
			sBox.set("urlId", urlId);//email
			
			responseList.add(sBox);
		}
		return responseList;
	}*/
	public static SBoxList<SBox> getCrecditUsrListReponseMessage(String xmlFile) {
		
		SBoxList<SBox>	responseList = new SBoxList<SBox>();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(CreditResponseUserListType.class, xmlFile);
		
		int listSize = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().size();
		
		//xmlFile m개 값으로 Return 갯수만큼 
		for(int i=0; i<listSize; i++){
			
			SBox sBox = new SBox();
			
			String id = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getID();
			String loginId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getLoginID();
			String compUserId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getCompanyUserID();
			String name = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getName();
			String localNumber = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getTelephoneNumberCommunication().getLocalNumber();
			String urlId = ((CreditResponseUserListType) obj).getResponseResultDocument().getReferencedUserListResponseDocument().get(i).getSpecifiedMemberPerson().getEmailAddressCommunication().getURIID();
			
			sBox.set("id", id); //스마트 채권 개이 회원 식별자
			sBox.set("loginId", loginId); //스마트채권 로그인 아이디
			sBox.set("compUserId", compUserId); //기업회원 식별자
			sBox.set("name", name); //회원이름
			sBox.set("localNumber", localNumber); //전화번호
			sBox.set("urlId", urlId);//email
			
			responseList.add(sBox);
		}
		return responseList;
	}

	
	
	/**
	 * <pre>
	 * [채무불이행 - Request ]채무불이행 신청서 등록 요청서  위한 XML 메시지 (CRQRDA) 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 2.
	 * @version 1.0
	 * @param commonVo
	 * @param requstVo
	 * @return
	 */
	public static String makeCreditRequestRegisterDefaultApplicationForXML(RequestCommonVo commonVo, DebtRegisterRequestVo requstVo ) {
    	//가상의 배열및 리스트에 데이터 add
		String xmlString = null;
		
		
		CreditRequestRegisterDefaultApplicationType  debtRegisterApplication = new CreditRequestRegisterDefaultApplicationType();
	
		
		if(commonVo!=null){
		//요청 문서 정보 영역	
		com.spsb.schema.credit.request.register.RequestDocumentType requsetDoc = new com.spsb.schema.credit.request.register.RequestDocumentType();
			requsetDoc.setID(commonVo.getID());//메시지 교유의 식별 번호
			requsetDoc.setTypeCode(commonVo.getRequestDocumentTypeCode());//유형 코드. 고정값: CRQREC 
			requsetDoc.setVersionInformation(commonVo.getVersionInformation()); // 문서 버전. 고정값: 1.0.0
			requsetDoc.setDescription(commonVo.getDescription()); //문서 비고 내역
			debtRegisterApplication.setRequestDocument(requsetDoc);
			
		//계정 정보 영역
		com.spsb.schema.credit.request.register.UserBusinessAccountType userBusinessAccount = new com.spsb.schema.credit.request.register.UserBusinessAccountType();
			userBusinessAccount.setTypeCode(commonVo.getUserBusinessAccountTypeCode()); //E: ERP회원
			userBusinessAccount.setUserID(commonVo.getUserID()); //회원 식별자  TODO(Legacy 회원식별자 아니면 스마트채권의 회원식별자 확인필요)
			userBusinessAccount.setLoginID(commonVo.getLoginID()); //로그인식별자 TODO(Legacy 로그인아이디 아니면 스마트채권의 아이디인지 확인필요)
			userBusinessAccount.setLoginPasswordID(commonVo.getLoginPasswordID());
			debtRegisterApplication.setUserBusinessAccount(userBusinessAccount);
			
		}else{
			//CommonVo
		}
		//요청 정보 영역
		DefaultApplicationRegistrationRequestDocumentType applicationRequestDoc = new DefaultApplicationRegistrationRequestDocumentType();
		//자사정보영역
		AuthorizedDefaultApplicantPartyType defaultParty = new AuthorizedDefaultApplicantPartyType();
		//거래저 정보 영역
		AuthorizedDefaultCustomerPartyType customerParty = new AuthorizedDefaultCustomerPartyType();
		//연체 정보 영역
		ReferencedDefaultDocumentType referenceDoc = new ReferencedDefaultDocumentType();
		
		//신규등록정보
		if(requstVo != null){
			
			OwnerPersonType ownPerson = new OwnerPersonType();
			DefaultMemberPersonType memberPerson = new DefaultMemberPersonType();
			NumberCommunicationType  numberCommunication = new NumberCommunicationType();
			AddressCommunicationType adressCommunication = new AddressCommunicationType();
			MemberAddressType memberAdress = new MemberAddressType();
			
			//자사 정보영역
			defaultParty.setCompanyUserID(requstVo.getCompUsrId());
			defaultParty.setRegisterAssignedToRoleDateTime(requstVo.getRegDt());//debtBox.getDouble("REG_DT")
			defaultParty.setRegisterUserID(requstVo.getRegId());
			defaultParty.setTypeCode(requstVo.getTypeCode());
			defaultParty.setName(requstVo.getCompNm());
			defaultParty.setCompanyID(requstVo.getCompNo());
			defaultParty.setCorporationID(requstVo.getCorpNo());
			defaultParty.setBusinessTypeDescription(requstVo.getBizType());
				ownPerson.setName(requstVo.getOwnName());
			defaultParty.setSpecifiedOwnerPerson(ownPerson);
				memberPerson.setName(requstVo.getUserNm());
				memberPerson.setDepartmentName(requstVo.getDeptNm());
				memberPerson.setJobTitleName(requstVo.getJobTlNm());
				numberCommunication.setLocalNumber(requstVo.getTelNo());
				memberPerson.setTelephoneNumberCommunication(numberCommunication);
				numberCommunication.setLocalNumber(requstVo.getMbNo());
				memberPerson.setMobileNumberCommunication(numberCommunication);
				numberCommunication.setLocalNumber(requstVo.getFaxNo());
				memberPerson.setFaxNumberCommunication(numberCommunication);
				adressCommunication.setURIID(requstVo.getEmail());
				memberPerson.setEmailAddressCommunication(adressCommunication);
				memberAdress.setPostCode(requstVo.getPostNo());
				memberAdress.setLineOne(requstVo.getAdressOne());
				memberAdress.setLineTwo(requstVo.getAdressTwo());
				memberPerson.setPostalCompanyAddress(memberAdress);
			defaultParty.setSpecifiedMemberPerson(memberPerson);
			
			applicationRequestDoc.setAuthorizedDefaultApplicantParty(defaultParty);
			
			//거래저 정보 영역
			customerParty.setTypeCode(requstVo.getCustType());
			customerParty.setName(requstVo.getCustNm());
			customerParty.setCompanyID(requstVo.getCustNo());
			customerParty.setCorporationID(requstVo.getCustCorpNo());
				ownPerson.setName(requstVo.getCustOwnNm());
    		customerParty.setSpecifiedOwnerPerson(ownPerson);
    			memberAdress.setPostCode(requstVo.getCustPostNo());
    			memberAdress.setLineOne(requstVo.getCustAddrOne());
    			memberAdress.setLineTwo(requstVo.getCustAddrTwo());
    		customerParty.setPostalCompanyAddress(memberAdress);
			applicationRequestDoc.setAuthorizedDefaultCustomerParty(customerParty);
			
			//연체정보
			referenceDoc.setTypeCode(requstVo.getDebtType());
			referenceDoc.setServiceTypeCode(requstVo.getDebtCompType());
			referenceDoc.setDefaultStartDateTime(requstVo.getOverStDt());//debtBox.getDouble("OVER_ST_DT")연체개시일자(yyyyMMdd)
			referenceDoc.setDefaultIncludedAmount(requstVo.getStDebtAmt());
			applicationRequestDoc.setReferencedDefaultDocument(referenceDoc);
			
			debtRegisterApplication.setDefaultApplicationRegistrationRequestDocument(applicationRequestDoc);
			xmlString = JAXBUtil.marshall2String(CreditRequestRegisterDefaultApplicationType.class, debtRegisterApplication);
		}else{
			//requstVo
		}
		
		return xmlString;
	}
	
	
	/**
	 * <pre>
	 *  [채무불이행 - response] 채무불이행등록 응답서 : 비동기식  XML파싱 (CRSRDA)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static SBox getCreditResponseRegisterDefaultApplicationForXML(String xmlFile) {
		
		SBox sBox = new SBox();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(CreditResponseRegisterDefaultApplicationType.class, xmlFile);
		
		String typeCode =  ((CreditResponseRegisterDefaultApplicationType) obj).getResponseResultDocument().getTypeCode(); //1: 성공 0: 실패
		String responseTypeCode =  ((CreditResponseRegisterDefaultApplicationType) obj).getResponseResultDocument().getResponseTypeCode();
		String id = ((CreditResponseRegisterDefaultApplicationType) obj).getResponseResultDocument().getID();
		String description = ((CreditResponseRegisterDefaultApplicationType) obj).getResponseResultDocument().getDescription();
		
		sBox.set("typeCode", typeCode);
		sBox.set("responseTypeCode", responseTypeCode);
		sBox.set("id", id);
		sBox.set("description", description);
		
		return sBox;
	}
	
	
	
	/**
	 * <pre>
	 * [채무불이행 - Request]채무불이행 등록 변경  위한 XML 메시지 (CRQAMD) 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 2.
	 * @version 1.0
	 * @param commonVo
	 * @param requstVo
	 * @return
	 */
	public static String makeCreditRequestAmendDefaultForXML(RequestCommonVo commonVo, DebtAmendRequestVo requstVo ) {
		String xmlString = null;
		
		 com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType amendDefault = new com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType();
         
         if(commonVo!=null){
     		//요청 문서 정보 영역	
        	 com.spsb.schema.credit.request.amend.RequestDocumentType requsetDoc = new com.spsb.schema.credit.request.amend.RequestDocumentType();
     			requsetDoc.setID(commonVo.getID());//메시지 교유의 식별 번호
     			requsetDoc.setTypeCode(commonVo.getRequestDocumentTypeCode());//유형 코드. 고정값: CRQAMD 
     			requsetDoc.setVersionInformation(commonVo.getVersionInformation()); // 문서 버전. 고정값: 1.0.0
     			requsetDoc.setDescription(commonVo.getDescription()); //문서 비고 내역
     			amendDefault.setRequestDocument(requsetDoc);
     			
     		//계정 정보 영역
     		com.spsb.schema.credit.request.amend.UserBusinessAccountType userBusinessAccount = new  com.spsb.schema.credit.request.amend.UserBusinessAccountType();
     			userBusinessAccount.setTypeCode(commonVo.getUserBusinessAccountTypeCode()); //E: ERP회원
     			userBusinessAccount.setUserID(commonVo.getUserID()); //회원 식별자  TODO(Legacy 회원식별자 아니면 스마트채권의 회원식별자 확인필요)
     			userBusinessAccount.setLoginID(commonVo.getLoginID()); //로그인식별자 TODO(Legacy 로그인아이디 아니면 스마트채권의 아이디인지 확인필요)
     			userBusinessAccount.setLoginPasswordID(commonVo.getLoginPasswordID());
     			amendDefault.setUserBusinessAccount(userBusinessAccount);
     			
     		}else{
     			//CommonVo
     		}
         
         com.spsb.schema.credit.request.amend.DefaultAmendmentRequestDocumentType amendmentRequest = new  com.spsb.schema.credit.request.amend.DefaultAmendmentRequestDocumentType();
       
        //신규등록정보
 		if(requstVo != null){
         
         //요청 정보 영역
         amendmentRequest.setID(requstVo.getDocCd()); //sBox.getString("DOC_CD") DE_DEBT_APPL.DOC_CD
         amendmentRequest.setUpdateAssignedToRoleDateTime(requstVo.getUpdDt()); //20152523.00 DE_DEBT_APPL.UPD_DT
         amendmentRequest.setUpdatedUserID(requstVo.getUpdId()); //sBox.getString("UPD_ID") DE_DEBT_APPL.UPD_ID
         amendmentRequest.setTypeCode(requstVo.getModStat()); //DE_DEBT_MOD_HIST.STAT 
         amendmentRequest.setName(requstVo.getUsrNm()); //sBox.getString("USR_NM") 필수값아니라 null표기 DE_DEBT_STAT_HIST.USR_NM
         
         //연체정보
         ReferencedDefaultAmendmentDocumentType referenceAmendment = new ReferencedDefaultAmendmentDocumentType();
         referenceAmendment.setDescription(requstVo.getRmkTxt()); //DE_DEBT_STAT_HIST.RMK_TXT
         referenceAmendment.setDefaultIncludedAmount(requstVo.getDebtAmt()); //DE_DEBT_MOD_HIST.DEBT_AMT
         amendmentRequest.setReferencedDefaultAmendmentDocument(referenceAmendment); 
         
         amendDefault.setDefaultAmendmentRequestDocument(amendmentRequest);
         xmlString = JAXBUtil.marshall2String(com.spsb.schema.credit.request.amend.CreditRequestAmendDefaultType.class, amendDefault);
         
		
		}else{
			//requstVo
		}
		
		return xmlString;
	}
	
	
	/**
	 * <pre>
	 *  [채무불이행 - Response]채무불이행 상태  변경  위한 XML 메시지 (CNTDUS) 
	 *  I/F  
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 26.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static SBox getCreditUpdateStatusForXml(String xmlFile) {
	
		SBox sBox = new SBox();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(CreditNoticeDefaultUpdateStatusType.class, xmlFile);
		
		String id = ((CreditNoticeDefaultUpdateStatusType) obj).getDefaultUpdateStatusNoticeDocument().getID();
		String typeCode =  ((CreditNoticeDefaultUpdateStatusType) obj).getDefaultUpdateStatusNoticeDocument().getTypeCode(); //1: 성공 0: 실패
		String description = ((CreditNoticeDefaultUpdateStatusType) obj).getDefaultUpdateStatusNoticeDocument().getDescription();
		
		sBox.set("id", id);
		sBox.set("typeCode", typeCode);
		sBox.set("description", description);
		
		return sBox;
	}
	
	
	
	///////////////////////////////////////////////////////////////////////////////Notice /////////////////////////////////////////////////////////////////////////////
	
	/**
	 * <pre>
	 *  [공통 - response ] 처리결과통보서: 비동기식  XML파싱(NTCCON)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	
	public static SBox getNoticeConclusionForXML(String xmlFile) {
		
		SBox sBox = new SBox();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(NoticeConclusionType.class, xmlFile);
		
		String typeCode =  ((NoticeConclusionType) obj).getResponseResultDocument().getTypeCode(); //1: 성공 0: 실패
		String responseTypeCode =  ((NoticeConclusionType) obj).getResponseResultDocument().getResponseTypeCode();
		String description = ((NoticeConclusionType) obj).getResponseResultDocument().getDescription();
		
		sBox.set("typeCode", typeCode);
		sBox.set("responseTypeCode", responseTypeCode);
		sBox.set("description", description);
		
		return sBox;
	}
	
	
	/**
	 * <pre>
	 * [공통 - requset ] 처리결과통보서: 비동기식   XML메시지 만들기  (NTCCON)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static String makeNoticeConclusionForXML(ResponseDocumentCommonVo responseVo) {
		
		String xmlString = null;
		
		NoticeConclusionType  noticeConclusion = new NoticeConclusionType();
		
		//응답 문서 정보 영역
		ResponseDocumentType responseDoc = new ResponseDocumentType();
		
		if(responseVo!=null){
			responseDoc.setID(responseVo.getId());									//메시지 고유의 식별 번호
			responseDoc.setReferenceID(responseVo.getReferenceID());				//요청 메시지의 식별 번호
			responseDoc.setTypeCode(responseVo.getRDocumentTypeCode()); 			//유형 코드. 고정값: NTCCON
			responseDoc.setVersionInformation(responseVo.getVersionInformation()); 	//문서 버전. 고정값: 1.0.0
			responseDoc.setDescription(responseVo.getRDDescription()); 				//문서 비고 내역
			
			noticeConclusion.setResponseDocument(responseDoc);
			
		//계정 정보 영역
		ResponseResultDocumentType resultDoc = new ResponseResultDocumentType();
			
			resultDoc.setTypeCode(responseVo.getRRDTypeCode());					 	//처리 성공, 실패 여부. 0: 실패, 1: 성공
			resultDoc.setResponseTypeCode(responseVo.getResponseTypeCode()); 		//처리 결과 코드. 처리결과코드표 참조
			resultDoc.setDescription(responseVo.getRRDDescription()); 				//처리 결과 내용, 사유, 기타 정보
			
			noticeConclusion.setResponseResultDocument(resultDoc);				
			
			xmlString = JAXBUtil.marshall2String(NoticeConclusionType.class, noticeConclusion);
		}else{
			//CommonVo
		}
		
		
		return xmlString;
	}
	
	
	
	/**
	 * <pre>
	 *  [공통  - response] 처리결과통보서: 비동기식   NTCRCT XML파싱
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public static SBox getNoticeReceiptForXML(String xmlFile) {
		
		SBox sBox = new SBox();
		
		//정보 불러오는 영역
		Object obj = JAXBUtil.unmarshallFromString(NoticeReceiptType.class, xmlFile);
		
		String typeCode =  ((NoticeReceiptType) obj).getResponseResultDocument().getTypeCode(); //1: 성공 0: 실패
		String responseTypeCode = ((NoticeReceiptType) obj).getResponseResultDocument().getResponseTypeCode();
		String description = ((NoticeReceiptType) obj).getResponseResultDocument().getDescription();
		
		sBox.set("typeCode", typeCode);
		sBox.set("responseTypeCode", responseTypeCode);
		sBox.set("description", description);
		
		return sBox;
	}
	
	/**
	 * <pre>
	 * [공통 - request] 접수통보서: 동기식   NTCRCT XML메시지 만들기  
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 3.
	 * @version 1.0
	 * @param xmlFile
	 * @return
	 */
	public String makeNoticeReceiptForXML(ResponseDocumentCommonVo responseVo) {
		
		String xmlString = null;
		
		NoticeReceiptType  noticeReceipt = new NoticeReceiptType();
		
		//응답 문서 정보 영역
		com.spsb.schema.notice.receipt.ResponseDocumentType responseDoc = new com.spsb.schema.notice.receipt.ResponseDocumentType();
		
		if(responseVo!=null){
			responseDoc.setID(responseVo.getId());									//메시지 고유의 식별 번호
			responseDoc.setTypeCode(responseVo.getReferenceID());					//요청 메시지의 식별 번호
			responseDoc.setVersionInformation(responseVo.getRDocumentTypeCode()); 	//유형 코드. 고정값: NTCCON
			responseDoc.setDescription(responseVo.getVersionInformation()); 		//문서 버전. 고정값: 1.0.0
			responseDoc.setDescription(responseVo.getRDDescription()); 				//문서 비고 내역
			
			noticeReceipt.setResponseDocument(responseDoc);
			
		//계정 정보 영역
			com.spsb.schema.notice.receipt.ResponseResultDocumentType resultDoc = new com.spsb.schema.notice.receipt.ResponseResultDocumentType();
			
			resultDoc.setTypeCode(responseVo.getRRDTypeCode());					 	//처리 성공, 실패 여부. 0: 실패, 1: 성공
			resultDoc.setResponseTypeCode(responseVo.getResponseTypeCode()); 		//처리 결과 코드. 처리결과코드표 참조
			resultDoc.setDescription(responseVo.getRRDDescription()); 				//처리 결과 내용, 사유, 기타 정보
			
			noticeReceipt.setResponseResultDocument(resultDoc);				
			
			xmlString = JAXBUtil.marshall2String(NoticeReceiptType.class, noticeReceipt);
		}else{
			//CommonVo
		}
		
		
		return xmlString;
	}
	
	
	
}
