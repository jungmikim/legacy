package com.spsb.common.event;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import com.spsb.common.dto.MessageTag;
import com.spsb.common.util.XMLUtil;
/**
 * <pre>
 * 
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class MessageEventFactory {

	  private static final String messageTagId_str = "MESSAGE_TAG_ID";
	  private static final String instanceId_str = "INSTANCE_ID";
	  private static final String referenceId_str = "REFERENCE_ID";
	  private static final String groupId_str = "GROUP_ID";
	  private static final String actionType_str = "ACTION_TYPE";
	  private static final String senderId_str = "SENDER_ID";
	  private static final String senderName_str = "SENDER_NAME";
	  private static final String receiveId_str = "RECEIVE_ID";
	  private static final String receiveName_str = "RECEIVE_NAME";
	  private static final String serviceCode_str = "SERVICE_CODE";
	  private static final String messageTypeCode_str = "MESSAGE_TYPE_CODE";
	  private static final String responseType_str = "RESPONSE_TYPE";
	  private static final String documentType_str = "DOCUMENT_TYPE";
	  private static final String documentData_str = "DOCUMENT_DATA";
	  private static final String messageStatus_str = "MESSAGE_STATUS";
	  private static final String fileCount_str = "FILE_COUNT";
	  private static final String direction_str = "DIRECTION";
	  private static final String resultCode_str = "RESULT_CODE";
	  private static final String resultMessage_str = "RESULT_MESSAGE";
	  private static final String docCreationDateTime_str = "CREATION_DATE_TIME";
	  private static final String ack_str = "IS_ACK";

	  public static MessageEvent toMessageEvent(String bizXML)
	    throws Exception
	  {
	    Element elem = XMLUtil.toElement(bizXML);

	    MessageEvent event = new MessageEvent();
	    event.setMessageTagId(elem.elementText("MESSAGE_TAG_ID"));
	    event.setInstanceId(elem.elementText("INSTANCE_ID"));

	    event.setReferenceId(elem.elementText("REFERENCE_ID"));
	    String groupId = elem.elementText("GROUP_ID");
	    if (groupId != null) {
	      event.setGroupId(groupId);
	    }
	    String actionType = elem.elementText("ACTION_TYPE");
	    if (actionType != null) {
	      event.setActionType(actionType);
	    }
	    event.setSenderId(elem.elementText("SENDER_ID"));
	    event.setSenderName(elem.elementText("SENDER_NAME"));
	    event.setReceiveId(elem.elementText("RECEIVE_ID"));
	    event.setReceiveName(elem.elementText("RECEIVE_NAME"));
	    event.setServiceCode(elem.elementText("SERVICE_CODE"));
	    event.setMessageTypeCode(elem.elementText("MESSAGE_TYPE_CODE"));
	    event.setResponseType(elem.elementText("RESPONSE_TYPE"));
	    event.setDocumentType(elem.elementText("DOCUMENT_TYPE"));
	    event.setDocumentData(elem.elementText("DOCUMENT_DATA"));
	    event.setDirection(elem.elementText("DIRECTION"));

	    String resultCode = elem.elementText("RESULT_CODE");
	    if (resultCode != null)
	      event.setResultCode(resultCode);
	    event.setResultMessage(elem.elementText("RESULT_MESSAGE"));
	    event.setDocCreationDateTime(elem.elementText("CREATION_DATE_TIME"));
	    event.setAck(Boolean.parseBoolean(elem.elementText("IS_ACK")));

	    String fileCount = elem.elementText("FILE_COUNT");

	    if (fileCount != null) {
	      event.setFileCount(Integer.parseInt(fileCount));
	    }
	    return event;
	  }

	  public static String toString(MessageEvent event) throws Exception
	  {
	    DocumentFactory factory = DocumentFactory.getInstance();
	    Document doc = factory.createDocument("MessageEvent");
	    Element message = doc.addElement("MessageEvent");

	    message.addElement("MESSAGE_TAG_ID").setText(event.getMessageTagId());
	    if (event.getInstanceId() != null) {
	      message.addElement("INSTANCE_ID").setText(event.getInstanceId());
	    }

	    if (event.getReferenceId() != null) {
	      message.addElement("REFERENCE_ID").setText(event.getReferenceId());
	    }
	    if (event.getGroupId() != null) {
	      message.addElement("GROUP_ID").setText(event.getGroupId());
	    }
	    if (event.getActionType() != null) {
	      message.addElement("ACTION_TYPE").setText(event.getActionType());
	    }
	    if (event.getSenderId() != null) {
	      message.addElement("SENDER_ID").setText(event.getSenderId());
	    }
	    if (event.getSenderName() != null) {
	      message.addElement("SENDER_NAME").setText(event.getSenderName());
	    }
	    if (event.getReceiveId() != null) {
	      message.addElement("RECEIVE_ID").setText(event.getReceiveId());
	    }
	    if (event.getReceiveName() != null) {
	      message.addElement("RECEIVE_NAME").setText(event.getReceiveName());
	    }
	    if (event.getServiceCode() != null) {
	      message.addElement("SERVICE_CODE").setText(event.getServiceCode());
	    }
	    if (event.getMessageTypeCode() != null) {
	      message.addElement("MESSAGE_TYPE_CODE").setText(event.getMessageTypeCode());
	    }
	    if (event.getResponseType() != null) {
	      message.addElement("RESPONSE_TYPE").setText(event.getResponseType());
	    }
	    if (event.getDocumentType() != null) {
	      message.addElement("DOCUMENT_TYPE").setText(event.getDocumentType());
	    }
	    if (event.getDocumentData() != null) {
	      message.addElement("DOCUMENT_DATA").setText(event.getDocumentData());
	    }
	    if (event.getDocCreationDateTime() != null) {
	      message.addElement("CREATION_DATE_TIME").setText(event.getDocCreationDateTime());
	    }
	    if (event.getDirection() != null) {
	      message.addElement("DIRECTION").setText(event.getDirection());
	    }
	    if (event.getResultCode() != null) {
	      message.addElement("RESULT_CODE").setText(event.getResultCode());
	    }
	    if (event.getResultMessage() != null) {
	      message.addElement("RESULT_MESSAGE").setText(event.getResultMessage());
	    }

	    boolean isAck = event.isAck();
	    if (isAck) {
	      message.addElement("IS_ACK").setText("true");
	    }

	    message.addElement("FILE_COUNT").setText(String.valueOf(event.getFileCount()));

	    String xmlString = XMLUtil.toString(doc);
	    return xmlString;
	  }

	  public static MessageEvent toMessageEvent(MessageTag msgTag) throws Exception {
	    MessageEvent event = new MessageEvent();

	    event.setMessageTagId(msgTag.getMessageTagId());
	    event.setInstanceId(msgTag.getInstanceId());
	    event.setReferenceId(msgTag.getReferenceId());
	    event.setGroupId(msgTag.getGroupId());
	    event.setActionType(msgTag.getActionType());
	    event.setSenderId(msgTag.getSenderId());
	    event.setSenderName(msgTag.getSenderName());
	    event.setReceiveId(msgTag.getReceiveId());
	    event.setReceiveName(msgTag.getReceiveName());
	    event.setServiceCode(msgTag.getServiceCode());
	    event.setMessageTypeCode(msgTag.getMessageTypeCode());
	    event.setResponseType(msgTag.getResponseType());
	    event.setDocumentType(msgTag.getDocumentType());
	    event.setDocumentData(msgTag.getDocumentData());
	    event.setDirection(msgTag.getDirection());
	    event.setResultCode(msgTag.getResultCode());
	    event.setResultMessage(msgTag.getResultMessage());
	    event.setDocCreationDateTime(msgTag.getDocCreationDateTime());
	    event.setFileCount(msgTag.getFileCount());

	    return event;
	  }
}
