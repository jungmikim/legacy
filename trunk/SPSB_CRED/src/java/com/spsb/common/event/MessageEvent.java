package com.spsb.common.event;

import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.FileData;

/**
 * <pre>
 * 라우팅 정보 메시지 영역
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 5. 21.
 * @version 1.0
 */
public class MessageEvent {

  private String senderId;
  private String senderName;
  private String receiveId;
  private String receiveName;	
//수신자 URL정보
  private String responseType;
  private String instanceId;
  private String referenceId;
  private String groupId;
  private String documentType; //비지니스 메시지 유형 코드  CRREQ
  private String actionType;
  private String docCreationDateTime;
  private String serviceCode;
  private String messageTypeCode;
  
  private String resultCode;
  private String resultMessage;
  private String messageTagId; 
  private String direction;
  private String typeCode;
  
  private String documentData;
  private int fileCount;
  private boolean isAck;
  private SBoxList<FileData> fileDatas;
  
  public String getTypeCode() {
	return typeCode;
  }
  public void setTypeCode(String typeCode) {
	this.typeCode = typeCode;
  }
  
  public SBoxList<FileData> getFileDatas() {
	return fileDatas;
  }
  public void setFileDatas(SBoxList<FileData> fileDatas) {
	this.fileDatas = fileDatas;
  }
  public String getMessageTagId()
  {
    return this.messageTagId;
  }
  public void setMessageTagId(String messageTagId) {
    this.messageTagId = messageTagId;
  }
  public String getInstanceId() {
    return this.instanceId;
  }
  public void setInstanceId(String instanceId) {
    this.instanceId = instanceId;
  }
  public String getReferenceId() {
    return this.referenceId;
  }
  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }
  public String getSenderId() {
    return this.senderId;
  }
  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }
  public String getSenderName() {
    return this.senderName;
  }
  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }
  public String getReceiveId() {
    return this.receiveId;
  }
  public void setReceiveId(String receiveId) {
    this.receiveId = receiveId;
  }
  public String getReceiveName() {
    return this.receiveName;
  }
  public void setReceiveName(String receiveName) {
    this.receiveName = receiveName;
  }
  public String getServiceCode() {
    return this.serviceCode;
  }
  public void setServiceCode(String serviceCode) {
    this.serviceCode = serviceCode;
  }

  public String getMessageTypeCode()
  {
    return this.messageTypeCode;
  }

  public void setMessageTypeCode(String messageTypeCode)
  {
    this.messageTypeCode = messageTypeCode;
  }
  public String getDocumentType() {
    return this.documentType;
  }
  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }
  public String getDocumentData() {
    return this.documentData;
  }
  public void setDocumentData(String documentData) {
    this.documentData = documentData;
  }
  public String getDirection() {
    return this.direction;
  }
  public void setDirection(String direction) {
    this.direction = direction;
  }
  public String getResultCode() {
    return this.resultCode;
  }
  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
  public String getResultMessage() {
    return this.resultMessage;
  }
  public void setResultMessage(String resultMessage) {
    this.resultMessage = resultMessage;
  }

  public void setResponseType(String responseType)
  {
    this.responseType = responseType;
  }

  public String getResponseType()
  {
    return this.responseType;
  }

  public String getDocCreationDateTime()
  {
    return this.docCreationDateTime;
  }

  public void setDocCreationDateTime(String docCreationDateTime)
  {
    this.docCreationDateTime = docCreationDateTime;
  }

  public boolean isAck()
  {
    return this.isAck;
  }

  public void setAck(boolean isAck)
  {
    this.isAck = isAck;
  }

  public int getFileCount()
  {
    return this.fileCount;
  }

  public void setFileCount(int fileCount)
  {
    this.fileCount = fileCount;
  }

  public String getGroupId()
  {
    return this.groupId;
  }

  public void setGroupId(String groupId)
  {
    this.groupId = groupId;
  }

  public String getActionType()
  {
    return this.actionType;
  }

  public void setActionType(String actionType)
  {
    this.actionType = actionType;
  }
}