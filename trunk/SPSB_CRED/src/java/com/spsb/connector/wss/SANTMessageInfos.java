package com.spsb.connector.wss;

import java.util.List;

import javax.xml.bind.JAXBElement;

import com.spsb.common.util.StringUtil;
import com.spsb.ws.ArrayOfReceiver;
import com.spsb.ws.BusinessScope;
import com.spsb.ws.DocumentIdentification;
import com.spsb.ws.Manifest;
import com.spsb.ws.ManifestItem;
import com.spsb.ws.Result;
import com.spsb.ws.StandardBusinessDocument;
import com.spsb.ws.StandardBusinessDocumentHeader;



// TODO : common pakage 로 이동 시키시오
public class SANTMessageInfos {
	private String senderId  ;
	private String senderName  ;
	private String receiveId;
	private String receiveName;
	private String responseType;	//  S,A,P
	private String instanceId;
	private String referenceId ;
	private String groupId; // added 2114.06.06 - groupId
	private String documentType ;	//MRQGU
	private String actionType;
	private String docCreationDateTime;
	private String serviceCode;
	private String messageType;	// RES/REQ typeCode
	private Boolean hasFile = false;
	
	  private String resultCode;
	  private String resultMessage;
	
	//Manifest
	private int fileCount;
	private long totalSize;
	
	//BusinessScope
	private boolean encrypted ;
	private String encAlrotirhm;
	private String securityToken;
	
	
	public SANTMessageInfos() {
		
	}
	
	public SANTMessageInfos(StandardBusinessDocument bizDoc){
		 this.extractValues(bizDoc);
	}

	public void extractValues(StandardBusinessDocument bizDoc) {
		StandardBusinessDocumentHeader msgHeader = bizDoc.getStandardBusinessDocumentHeader().getValue();
		
		
		DocumentIdentification docIdentity = msgHeader.getDocumentIdentification().getValue();
		
//		this.serviceCode = msgHeader.getServiceCode();
		this.serviceCode = msgHeader.getServiceCode().getValue();
		this.setMessageType(msgHeader.getTypeCode().getValue());
		
//		DocumentIdentification docIdentity = msgHeader.getDocumentIdentification();
		
		if(docIdentity != null ){
			this.instanceId = docIdentity.getInstanceID().getValue();
//			this.instanceId = docIdentity.getInstanceID();
			
			JAXBElement<String> jaxbRefId = docIdentity.getReferenceID();
			if(jaxbRefId != null) 
				this.referenceId = docIdentity.getReferenceID().getValue();
			
			this.documentType = docIdentity.getType().getValue();
//			this.documentType = docIdentity.getType();
			
			JAXBElement<String> jaxbResType =  docIdentity.getResponseType();
			if( jaxbResType != null) 
				this.responseType = jaxbResType.getValue();
			
			this.docCreationDateTime = docIdentity.getCreationDateAndTime().getValue();
//			this.docCreationDateTime = docIdentity.getCreationDateAndTime();
			
			this.groupId = docIdentity.getGroupID().getValue();
//			this.groupId = docIdentity.getGroupID();
			
			JAXBElement<String> jaxbActionType = docIdentity.getActionType();
//			Assert.isNull(jaxbActionType, "ActionType Element is null..");
			if(jaxbActionType != null)
				this.actionType = docIdentity.getActionType().getValue();
		} else {
//			TODO: exception
		}
		this.senderId = msgHeader.getSender().getValue().getID().getValue();
		this.senderName = msgHeader.getSender().getValue().getName().getValue();
//		this.senderId = msgHeader.getSender().getID();
//		this.senderName = msgHeader.getSender().getName();
				
		ArrayOfReceiver receivers = msgHeader.getReceiver().getValue();
//		ArrayOfReceiver receivers = msgHeader.getReceiver();
		
		if(receivers != null ){
			this.receiveId = receivers.getReceiver().get(0).getID().getValue();
			this.receiveName = receivers.getReceiver().get(0).getName().getValue();
//			this.receiveId = receivers.getReceiver().get(0).getID();
//			this.receiveName = receivers.getReceiver().get(0).getName();
		} else {
//			TODO : ecxeption
		}
		
		JAXBElement<BusinessScope> jaxbBizParam = msgHeader.getBusinessScope();
		if(jaxbBizParam != null ) {
			BusinessScope bizParam = jaxbBizParam.getValue();
			if(bizParam != null){
				if(bizParam.isIsEncrypted()){
					this.encrypted = true;
					this.encAlrotirhm = bizParam.getEncryptionAlgorithm().getValue();
					this.securityToken = bizParam.getSecurityToken().getValue();
				}
				else
					this.encrypted = false;
			}
		}
		
		if(msgHeader.isHasFile() != null) {
			hasFile = msgHeader.isHasFile();
			if(  hasFile.booleanValue()){
				Manifest manifest = msgHeader.getManifest().getValue();
				// 목록 valisation
				
				if(manifest != null){
					String itemNumbers = manifest.getNumberOfItems().getValue();
					if(itemNumbers != null ) {
						this.fileCount = Integer.parseInt(itemNumbers);
						long total = 0;
						List<ManifestItem> manifestItems =  manifest.getManifestItem().getValue().getManifestItem();
//						List<ManifestItem> manifestItems =  manifest.getManifestItem().getManifestItem();
						
						for(ManifestItem item : manifestItems){
							total += Integer.parseInt( item.getSize().getValue());
//							total += Integer.parseInt( item.getSize());
						}
						this.totalSize = total;
					}
				} 
			
			}
		} else {
			hasFile = false;
		}
		
		JAXBElement<Result> result = msgHeader.getResult();
		if(result != null ){
			this.resultCode = result.getValue().getTypeCode().getValue();
			this.resultMessage = result.getValue().getDescription().getValue();
		} else {
//			TODO : ecxeption
		}

		
	}
	
	
	public String getResultCode() {
		return resultCode;
	}
	
	public String getServiceCode() {
		return serviceCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public String getSenderId() {
		return senderId;
	}

	public String getSenderName() {
		return senderName;
	}


	public String getReferenceId() {
		return StringUtil.getNullToEmpty(referenceId);
	}

	public String getDocumenType() {
		return documentType;
	}

	public boolean isEncrypted(){
		return encrypted;
	}


	public String getEncAlrotirhm() {
		return encAlrotirhm;
	}


	public String getSecurityToken() {
		return securityToken;
	}
	
	public String getReceiveId() {
		return receiveId;
	}


	public String getReceiveName() {
		return receiveName;
	}


	public String getDocumentType() {
		return documentType;
	}


	public String getResponseType() {
		return responseType;
	}


	public String getDocCreationDateTime() {
		return docCreationDateTime;
	}


	public Boolean getHasFile() {
		if (hasFile == null)
			hasFile = false;
		return hasFile;
	}


	public int getFileCount() {
		return fileCount;
	}


	public long getTotalSize() {
		return totalSize;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getActionType() {
		return actionType;
	}

}
