package com.spsb.connector.wss;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.FileData;
import com.spsb.common.dto.MessageTag;
import com.spsb.common.event.MessageEvent;
import com.spsb.common.event.MessageEventFactory;
import com.spsb.common.exception.BizException;
import com.spsb.common.exception.SANTApplication;
import com.spsb.common.exception.SANTException;
import com.spsb.common.util.CommonUtil;
import com.spsb.common.util.FileUtil;
import com.spsb.dao.config.ConfigDaoForMssql;
import com.spsb.dao.config.ConfigDaoForOracle;
import com.spsb.dao.debt.DebtDaoForMssql;
import com.spsb.dao.debt.DebtDaoForOracle;
import com.spsb.service.message.InterfaceMessageService;
import com.spsb.ws.ArrayOfManifestItem;
import com.spsb.ws.ArrayOfReceiver;
import com.spsb.ws.BusinessScope;
import com.spsb.ws.DocumentIdentification;
import com.spsb.ws.IService;
import com.spsb.ws.Manifest;
import com.spsb.ws.ManifestItem;
import com.spsb.ws.ObjectFactory;
import com.spsb.ws.Receiver;
import com.spsb.ws.Result;
import com.spsb.ws.Sender;
import com.spsb.ws.StandardBusinessDocument;
import com.spsb.ws.StandardBusinessDocumentHeader;


public class SANTMessageInterfaceImpl  extends SANTApplication implements IService {
	
	SANTMessageInfos headInfo;
	SANTMessageConvert msgConvert ;
	ObjectFactory objFactory;
	
	@Autowired
	private InterfaceMessageService interfaceMessageService;
	// oracle dao version
	@Autowired
	private DebtDaoForOracle debtDaoForOracle;
	
	@Autowired  ConfigDaoForOracle configDaoForOracle;
	
	// mssql dao version
	@Autowired
	private DebtDaoForMssql debtDaoForMssql;
	
	@Autowired  ConfigDaoForMssql configDaoForMssql;
	
	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
		
	@Value("#{common['DEBTFILE'].trim()}")
	private String debtFilePath;
	
	 protected Logger logger = LoggerFactory.getLogger(getClass());

	@ResponseWrapper(localName = "RequestMessageResponse", targetNamespace = "http://serivce.host.sant.com", className = "com.spsb.ws.RequestMessageResponse")
	@Action(input = "http://serivce.host.sant.com/IService/RequestMessage", output = "http://serivce.host.sant.com/IService/RequestMessageResponse")
	@RequestWrapper(localName = "RequestMessage", targetNamespace = "http://serivce.host.sant.com", className = "com.spsb.ws.RequestMessage")
	@WebResult(name = "RequestMessageResult", targetNamespace = "http://serivce.host.sant.com")
	@WebMethod(operationName = "RequestMessage", action = "http://serivce.host.sant.com/IService/RequestMessage")
	public com.spsb.ws.StandardBusinessDocument requestMessage(
			@WebParam(name = "requestMessage", targetNamespace = "http://serivce.host.sant.com") com.spsb.ws.StandardBusinessDocument InStandardBusinessDocument) {
		// TODO Auto-generated method stub
		headInfo.extractValues(InStandardBusinessDocument);
		String serviceCode = headInfo.getServiceCode();
		String instanceId = headInfo.getInstanceId();
		String senderId  = headInfo.getSenderId();
		String senderName = headInfo.getSenderName();
		String referenceId = headInfo.getReferenceId();
		String documentType = headInfo.getDocumenType();
		String messageTagId = null;
		
		logger.debug("==========================================================================");
		logger.debug("RequestSantMessge call Sync:" );
		logger.debug("InstanceID : " + instanceId);
		logger.debug("serviceCode : " + serviceCode);
		logger.debug("ResponseType(S|P|A) : " + headInfo.getResponseType());
		logger.debug("senderName : " + senderName);
		logger.debug("documentType : " + documentType);
		logger.debug("==========================================================================");
		
		
		if(serviceCode.equalsIgnoreCase("TEST")) {
			JAXBElement<String> resMesaage = objFactory.createStandardBusinessDocumentMessage("Message responsed by Messasing jungmi");
			InStandardBusinessDocument.setMessage(resMesaage);
			
			return InStandardBusinessDocument;
		}
		// end of Test Message
		
		MessageTag messageTag = null;
		
		messageTag =  msgConvert.toMessageTag(InStandardBusinessDocument, headInfo);
		//messageTagId = messageTag.getMessageTagId();
		
		/*if(getDomain().equals("CON")) {
			messageTag.setDirection("I");
		} else {
			messageTag.setDirection("O");
		}
		messageTag.setResponseType("S");	// current method only serviced Sync Type
*/		
		// file 처리
		List<FileData> files = null;
		logger.debug("insert into FileData" + headInfo.getHasFile());
		if(headInfo.getHasFile()){
			try {
				JAXBElement<Manifest> jaxManifest = InStandardBusinessDocument.getStandardBusinessDocumentHeader().getValue().getManifest();
				if(jaxManifest != null) {
					Manifest manifest = jaxManifest.getValue();
					files = extractFiles(manifest);
				}else {
					//throw new SANTException(WSApplication.RESULT_FAIL,"Manifest element is null");
				}
				if(files != null && files.size() > 0) {
					//mapHelper.insertFileData(messageTag.getMessageTagId(), files);
				}
			} catch (SANTException e) {
				//handleException(messageTag.getMessageTagId(), new SANTException(WSApplication.RESULT_FAIL, "첨부파일 처리 실패" + e.getMessage()));
				
				StandardBusinessDocument response = null ; //= makeFaultResponseDocument(InStandardBusinessDocument, WSApplication.RESULT_FAIL, "첨부파일 처리  실패"+ e.getMessage());
				return response;
			}
			
			
		}
				
		
		//mapHelper.insertMessageTag(messageTag,getActivity(),getClassName());
		
		// request to Queue
		try {
			logger.debug("send to Adapter queeu " + messageTag.toString());
		} catch (Exception e) {
			// TODO : 
			// retry to que
			
			//handleException(messageTag.getMessageTagId(), new SANTException(WSApplication.RESULT_FAIL, "Queue 전송 실패"));
			
			StandardBusinessDocument response = null;// = makeFaultResponseDocument(InStandardBusinessDocument, WSApplication.RESULT_FAIL, "Queue 전송 실패"+ e.getMessage());
			return response;
		}
		
		// response from Queue
		//String correlation = queueHandler.getCorrelationID();
		String responseString = "";
		/*try {
			if(correlation != null)
				responseString= queueHandler.receive(correlation);
		} catch (JMSException e) {
			handleException(messageTag.getMessageTagId(), new SANTException(WSApplication.RESULT_FAIL, "Adapter로 부터 응답을 바들 수 없음" + e.getMessage()));
			// send  to public error response
			StandardBusinessDocument response = makeFaultResponseDocument(InStandardBusinessDocument, WSApplication.RESULT_FAIL, "Adapter로 부터 응답을 받을 수 없음" + e.getMessage());
			return response;
		}*/
		
		// send response Msg to public
		MessageEvent msgEvent = null;
		try {
			msgEvent = MessageEventFactory.toMessageEvent(responseString);
		} catch (Exception e) {
			//handleException(messageTag.getMessageTagId(),new SANTException(WSApplication.RESULT_FAIL, "Adapter로 부터 응답을 해석 할 수 없음" + e.getMessage()));
			// send  to public error response
			StandardBusinessDocument response = null ;//= makeFaultResponseDocument(InStandardBusinessDocument, WSApplication.RESULT_FAIL, "Adapter로 부터 응답을 해석 할 수 없음" + e.getMessage());
			return response;
		}
		
		logger.debug("update MessageTag as  a complate status");
		//mapHelper.updateMessageStatus(messageTagId, SANTApplication.MSG_STATUS_COMPLETE, getActivity());
		//mapHelper.setResult(messageTagId, msgEvent.getResultCode(),msgEvent.getResultMessage());
		
		String docMessage = msgEvent.getDocumentData();
		
		StandardBusinessDocument response = makeResponseBusinessDocument(docMessage, msgEvent);
		
//		StandardBusinessDocument response2 = makeResponseBusinessDocument(InStandardBusinessDocument, docMessage, msgEvent);
		logger.debug("send response to Public");
		return response;
	}

	

	private StandardBusinessDocument makeResponseBusinessDocument(StandardBusinessDocument response, String docMessage, MessageEvent msgEvent) {

//		StandardBusinessDocument response = objFactory.createStandardBusinessDocument();
		
		// TODO:
		//encryptions if required.
		boolean encrypted = false;
		JAXBElement<String> jaxbMessage = response.getMessage();
		jaxbMessage.setValue(docMessage);
//				objFactory.createStandardBusinessDocumentMessage(docMessage);
		
		response.setMessage(jaxbMessage);
		
		StandardBusinessDocumentHeader responseHeader = makeDocumentHeader(response.getStandardBusinessDocumentHeader().getValue(), msgEvent, encrypted);
		JAXBElement<StandardBusinessDocumentHeader> jaxbResponseHeader = response.getStandardBusinessDocumentHeader();
		jaxbResponseHeader.setValue(responseHeader);
//				objFactory.createStandardBusinessDocumentHeader(responseHeader);
		response.setStandardBusinessDocumentHeader(jaxbResponseHeader);
		
		return response;
	}


	private StandardBusinessDocumentHeader makeDocumentHeader(StandardBusinessDocumentHeader header, MessageEvent msgEvent, boolean encrypted) {
		
		if(msgEvent.getResultCode() != null) {
			// result code 값이 없으면 Node를 만들지 앟음.
			JAXBElement<Result > jaxbResult = header.getResult();
			if(jaxbResult != null){
				
//				Result result = jaxbResult.getValue();
//				
//				JAXBElement<String> jaxbCode = result.getTypeCode();
//				jaxbCode.setValue(msgEvent.getResultCode());
//				result.setTypeCode(jaxbCode);
//				
//				JAXBElement<String> jaxbString = result.getDescription();
//				jaxbString.setValue(msgEvent.getResultMessage());
//				result.setDescription(jaxbString);
				header.setResult(jaxbResult);
			}
			
			
			
		}
		if(header.getDocumentIdentification() != null ){
			JAXBElement<DocumentIdentification> docIdentity =  header.getDocumentIdentification();
			
			if(docIdentity != null ) {
				JAXBElement<String> resType = docIdentity.getValue().getResponseType();
				resType.setValue(SANTApplication.RESPONSE_PATTERN_TYPE_S);
			}
		}
		
		return header;
	}


	private StandardBusinessDocument makeFaultResponseDocument(StandardBusinessDocument inBizDoc, String code, String description) {
		StandardBusinessDocument response = objFactory.createStandardBusinessDocument();
		
		JAXBElement<StandardBusinessDocumentHeader> jaxbHeader = inBizDoc.getStandardBusinessDocumentHeader();
		StandardBusinessDocumentHeader header =jaxbHeader.getValue();
		
		// result code 값이 없으면 Node를 만들지 앟음.
		Result result = objFactory.createResult();
		JAXBElement<String> jaxbString = objFactory.createResultDescription(description);
		JAXBElement<String> jaxbResultCode = objFactory.createResultTypeCode(code);
	
		result.setTypeCode(jaxbResultCode);
		result.setDescription(jaxbString);
		
		JAXBElement<Result > jaxbResult = objFactory.createResult(result);
		header.setResult(jaxbResult);
		
		response.setStandardBusinessDocumentHeader(jaxbHeader);
		
		return response;
	}



	public StandardBusinessDocument makeResponseBusinessDocument(String message,MessageEvent msgEvent){
		
		StandardBusinessDocument response = objFactory.createStandardBusinessDocument();
		
		// TODO:
		//encryptions if required.
		boolean encrypted = false;
		JAXBElement<String> jaxbMessage = objFactory.createStandardBusinessDocumentMessage(message);
		
		response.setMessage(jaxbMessage);
		
		StandardBusinessDocumentHeader responseHeader = makeDocumentHeader(msgEvent, encrypted);
		JAXBElement<StandardBusinessDocumentHeader> jaxbResponseHeader = objFactory.createStandardBusinessDocumentHeader(responseHeader);
		response.setStandardBusinessDocumentHeader(jaxbResponseHeader);
		
		return response;
	}
	
	public StandardBusinessDocumentHeader makeDocumentHeader(MessageEvent msgEvent, boolean encrypted){
//		ObjectFactory objFactory = new ObjectFactory();
		
		StandardBusinessDocumentHeader header = objFactory.createStandardBusinessDocumentHeader();
		
		
		JAXBElement<String> jaxbServiceCode = objFactory.createStandardBusinessDocumentHeaderServiceCode(msgEvent.getServiceCode());
		header.setServiceCode(jaxbServiceCode);
		
		JAXBElement<String> jaxbTypeCode = objFactory.createStandardBusinessDocumentHeaderTypeCode(SANTApplication.MSG_TYPE_RESPONSE);
		header.setTypeCode(jaxbTypeCode);	// always "RES"
		
		Sender sender = objFactory.createSender();
		
		sender.setID(objFactory.createSenderID(msgEvent.getSenderId()));
		sender.setName(objFactory.createSenderName(msgEvent.getSenderName()));
		header.setSender(objFactory.createSender(sender));
		
//		sender.setID(msgEvent.getSenderId());
//		sender.setName(msgEvent.getSenderName());
//		header.setSender(sender);
		
		
		
		Receiver receiver = objFactory.createReceiver();
		receiver.setID(objFactory.createReceiverID(msgEvent.getReceiveId()));
		receiver.setName( objFactory.createReceiverName(msgEvent.getReceiveName() ));
//		receiver.setID(msgEvent.getReceiveId());
//		receiver.setName(msgEvent.getReceiveName());
		
		ArrayOfReceiver receiversArray = objFactory.createArrayOfReceiver();
		receiversArray.getReceiver().add(receiver);
		header.setReceiver(objFactory.createArrayOfReceiver(receiversArray));
//		header.setReceiver(receiversArray);
		
		
		DocumentIdentification docIdentity = objFactory.createDocumentIdentification();
		
		JAXBElement<String> jaxbInstanceId = objFactory.createDocumentIdentificationInstanceID(msgEvent.getInstanceId());
		docIdentity.setInstanceID(jaxbInstanceId);
//		docIdentity.setInstanceID(msgEvent.getInstanceId());
		
		JAXBElement<String> refrenceID = objFactory.createDocumentIdentificationReferenceID(msgEvent.getReferenceId());
		docIdentity.setReferenceID(refrenceID);
		
		JAXBElement<String> resType = objFactory.createDocumentIdentificationResponseType(SANTApplication.RESPONSE_PATTERN_TYPE_S); // always 'S'
		docIdentity.setResponseType(resType);
	
		JAXBElement<String> jaxbType = objFactory.createDocumentIdentificationType(msgEvent.getDocumentType());
		docIdentity.setType(jaxbType);
//		docIdentity.setType(msgEvent.getDocumentType());
		
		JAXBElement<String> jaxbCreationTime = objFactory.createDocumentIdentificationCreationDateAndTime(msgEvent.getDocCreationDateTime());
		docIdentity.setCreationDateAndTime(jaxbCreationTime);
//		docIdentity.setCreationDateAndTime(msgEvent.getDocCreationDateTime());
		
		JAXBElement<DocumentIdentification>  jaxbDocIdenty = objFactory.createDocumentIdentification(docIdentity);
		header.setDocumentIdentification(jaxbDocIdenty);
//		header.setDocumentIdentification(docIdentity);
		
		// TODO:
		// files 처리
//		if(msgEvent.)
		
		
		if(encrypted){
			//set BusinessScope
			BusinessScope bizScope = objFactory.createBusinessScope();
			bizScope.setIsEncrypted(encrypted);
		
			JAXBElement<BusinessScope> jaxbBizScope = objFactory.createBusinessScope(bizScope);
			header.setBusinessScope(jaxbBizScope);
		}
		
		if (msgEvent.getResultCode() != null) {
			// result code 값이 없으면 Node를 만들지 앟음.
			Result result = objFactory.createResult();
			JAXBElement<String> jaxbString = objFactory.createResultDescription(msgEvent.getResultMessage());
			result.setDescription(jaxbString);
			
			JAXBElement<String> jaxbResultCode = objFactory.createResultTypeCode(msgEvent.getResultCode());
			result.setTypeCode(jaxbResultCode);
//			result.setTypeCode(msgEvent.getResultCode());
			
			
			JAXBElement<Result > jaxbResult = objFactory.createResult(result);
			header.setResult(jaxbResult);
		}
		return header;
	}
	
	
	public String decryptDocument(SANTMessageInfos headerInfo, String encDocument){//throws SANTServiceException{
		
		return null;
	}
	
	public List<FileData> extractFiles(Manifest manifest) throws SANTException {
		// 목록 valisation
		List<FileData> files = new ArrayList<FileData>();
		
		if(manifest != null){
			int seq = 1;
			
			if(manifest.getManifestItem() != null) {
				ArrayOfManifestItem itemArray = manifest.getManifestItem().getValue();
//				ArrayOfManifestItem itemArray = manifest.getManifestItem();
				
				if(itemArray != null) {
					List<ManifestItem> itemList = itemArray.getManifestItem();
					for(ManifestItem fileItem : itemList ) {
						logger.debug("파일 이름 : " + fileItem.getUniformResourceID().getValue());
						FileData fileData = new FileData();
						
						fileData.setFileName(fileItem.getUniformResourceID().getValue());
//						fileData.setFileName(fileItem.getUniformResourceID());
						
						DataHandler binObj = fileItem.getBinaryData().getValue();
						byte[] fileBinary;
						try {
							fileBinary = CommonUtil.toByteArray(binObj);
							fileData.setFileData(fileBinary);
							fileData.setFileSize(fileBinary.length);
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						fileData.setFileSeq(fileItem.getSequenceNumber().getValue());
//						fileData.setFileSeq(fileItem.getSequenceNumber());
						fileData.setFileDesc(fileItem.getDescription().getValue());
						files.add(fileData);
						// TODO : test
						//File file = new File("E:/"+fileData.getFileName());
						//FileUtil.write(file, fileBinary);
					}
				} else {
					// TODO: exception 처리, hasFile attribute is true but no manifest infos.
				}
			}
		} else {
			// TODO: exception 처리, hasFile attribute is true but no manifest infos.
		}
		return files;
	}
	
	

	@Override
	public String getDomain() {
		// TODO Auto-generated method stub
		return SANTApplication.DOMAIN_CONNECTOR;
	}



	@Override
	public String getActivity() {
		// TODO Auto-generated method stub
		return "WSServer";
	}



	/*@Override
	protected MapperHelper getDaoHelper() {
		// TODO Auto-generated method stub
		return null;
	}
*/


	public void setObjFactory(ObjectFactory objFactory) {
		this.objFactory = objFactory;
	}


	/*public void setMapHelper(MapperHelper mapHelper) {
		this.mapHelper = mapHelper;
	}*/


	public void setHeadInfo(SANTMessageInfos headInfo) {
		this.headInfo = headInfo;
	}


	public void setMsgConvert(SANTMessageConvert msgConvert) {
		this.msgConvert = msgConvert;
	}


	@Oneway
	@Action(input = "http://serivce.host.sant.com/inboundservice/IService/RequestAsyncMessage")
	@RequestWrapper(localName = "RequestAsyncMessage", targetNamespace = "http://serivce.host.sant.com", className = "com.spsb.ws.RequestAsyncMessage")
	@WebMethod(operationName = "RequestAsyncMessage", action = "http://service.platform.sant.com/SANTMessageService")
	public void requestAsyncMessage(@WebParam(name = "StandardBusinessDocument", targetNamespace = "http://serivce.host.sant.com") StandardBusinessDocument standardBusinessDocument) {
		// TODO Auto-generated method stub
		headInfo.extractValues(standardBusinessDocument);
		String serviceCode = headInfo.getServiceCode();
		String instanceId = headInfo.getInstanceId();
		String senderId  = headInfo.getSenderId();
		String senderName = headInfo.getSenderName();
		String referenceId = headInfo.getReferenceId();
		String documentType = headInfo.getDocumenType();
		String groupId = headInfo.getGroupId();//채무불이행신청순번
		String responseXmlMessage = standardBusinessDocument.getMessage().getValue();
		String debtStatHistId="";
		String messageTagId = null;
		
		logger.debug("==========================================================================");
		logger.debug("RequestSantMessge call Async:" );
		logger.debug("InstanceID : " + instanceId);
		logger.debug("serviceCode : " + serviceCode);
		logger.debug("ResponseType(S|P|A) : " + headInfo.getResponseType());
		logger.debug("senderName : " + senderName);
		logger.debug("documentType : " + documentType);
		logger.debug("==========================================================================");
		
		
		if(serviceCode.equalsIgnoreCase("TEST")) {
			logger.debug("ASycn Interface call test  OK..");
			return;
		}
		// end of Test Message
		MessageTag messageTag =  msgConvert.toMessageTag(standardBusinessDocument, headInfo);
		messageTagId = messageTag.getMessageTagId();
		
		logger.debug("==========================================================================");
		logger.debug("messageTagId : " + messageTagId);
		logger.debug("groupId : " + groupId);
		logger.debug("==========================================================================");
		
		
		
		/*if(getDomain().equals("CON")) {
			messageTag.setDirection("I");
		} else {
			messageTag.setDirection("O");
		}*/
//		messageTag.setResponseType("A");	// current method only serviced ASync Type, duplicated
		
		//extra code from jungmi#########################################################################################//
		//logger.debug("connected  : "+documentType);
		
		//Session생성하여 현재 사용자의 정보를 가져온다
		//SessionManager sessionManager = SessionManager.getInstance();
		//SBox sBox = sessionManager.getCurrentSession();
		
		//INBOUND MESSAGE INSERT
		SBox ifmsghistSBox = new SBox();
		ifmsghistSBox.set("compMngCd",null);
		ifmsghistSBox.set("serviceCd", headInfo.getServiceCode());
		ifmsghistSBox.set("docTypeCd", headInfo.getDocumentType());
		ifmsghistSBox.set("typeCd","RES");
		ifmsghistSBox.set("instanceId", headInfo.getInstanceId());
		ifmsghistSBox.set("referenceId", headInfo.getReferenceId());
		ifmsghistSBox.set("groupId",headInfo.getGroupId() );
		ifmsghistSBox.set("senderId", headInfo.getSenderId());
		ifmsghistSBox.set("senderNm", headInfo.getSenderName());
		ifmsghistSBox.set("receiverId", headInfo.getReceiveId());
		ifmsghistSBox.set("receiverNm", headInfo.getReceiveName());
		ifmsghistSBox.set("manifestNo", headInfo.getFileCount());
		ifmsghistSBox.set("actionType", headInfo.getActionType());
		ifmsghistSBox.set("xmlMsg", null);//message
		ifmsghistSBox.set("rcode", headInfo.getResultCode());
		ifmsghistSBox.set("reason", headInfo.getResultMessage());
		ifmsghistSBox.set("xmlMsgRe",responseXmlMessage);
		ifmsghistSBox.set("regId",null);
		
		try {
			if("oracle".equals(dbmsType)){
				configDaoForOracle.insertIfMsgHist(ifmsghistSBox);
			}else{
				configDaoForMssql.insertIfMsgHist(ifmsghistSBox);
			}
			
		} catch (BizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if("CRSRDA".equals(documentType)){ //채무불이행 등록응답서			
			interfaceMessageService.getDebtReqisterIFResultForSucessResponse(messageTag);
		}else if("NTCCON".equals(documentType)){ //처리결과 통보서			
			interfaceMessageService.getIFResultForNoticeConclusionType(messageTag);
		}else if("CNTDUS".equals(documentType)){// 상태 변경 업데이트 
			debtStatHistId = interfaceMessageService.getDebtStatusIFUpdate(messageTag);
		}else if("CNTUCL".equals(documentType)){ //크레딧서비스 거래처 신용등급변경통지서
			interfaceMessageService.getCreditNoticeUpdateCustomerLevel(messageTag);
		}else if("CNTKCL".equals(documentType)){ //크레딧서비스 거래처 신용등급변경통지서(K-Link)
			interfaceMessageService.getCreditNoticeByKLinkUpdateCustomerLevel(messageTag);
		}/*else if("CRSREC".equals(documentType)){
			interfaceMessageService.getIFResultForCustRegister(messageTag);
		}*/
		//#################################################################################################################//
		
		// file 처리
		List<FileData> files = null;
		logger.debug("insert into FileData" + headInfo.getHasFile());
		if(headInfo.getHasFile()){
			try {
				JAXBElement<Manifest> jaxManifest = standardBusinessDocument.getStandardBusinessDocumentHeader().getValue().getManifest();
				if(jaxManifest != null) {
					Manifest manifest = jaxManifest.getValue();
					files = extractFiles(manifest);
					logger.debug("[1] 중계 첨부파일 추출  , 총 개수 [" + files.size() + "]");
				}else {
					logger.debug("Manifest element is null");
					//throw new SANTException(WSApplication.RESULT_FAIL,"Manifest element is null");
				}
				 SBoxList<SBox> debtFileList = new SBoxList<SBox>();
				 
				if(files != null && files.size() > 0) {
						for (FileData file : files) {
					      file.setMessageTagId(messageTagId);
					      	//파일 생성
					        //파일 경로에 저장
					      String debtApplId = messageTag.getGroupId();
					      debtApplId = debtApplId.trim();
					      /*SBox fileBox = new SBox();
					      fileBox.setIfEmpty("debtApplSeq", messageTag.getGroupId());
					      fileBox.setIfEmpty("ch_fileNm", file.getFileName());
					      fileBox.setIfEmpty("filePath", );
					      fileBox.setIfEmpty("debtStatHistId", );
					      SBox fileInsertResult = debtDao.insertDebtStatFile(fileBox);*/

					      SBox filesBox = new SBox();
						      filesBox.set("file", file.getFileData());
						      filesBox.set("debtApplSeq", debtApplId);
						      filesBox.set("typeCd", file.getFileDesc());
						      filesBox.set("fileNm", file.getFileName());
							
							if (!"".equals(file.getFileName())) {
								String[] fileNm = CommonUtil.separateFileName(file.getFileName());
								filesBox.set("filePath", debtApplId + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond()+ "_" + (file.getFileSeq()) + "." + fileNm[1]);
							}
							debtFileList.add(filesBox);
							logger.debug("[2] 첨부파일 파라미터 정제완료 , 총 개수 [" + debtFileList.size() + "]");
					    }
						
						// [3] 새롭게 등록한 채권 증빙파일 등록함
						Iterator<SBox> fileIT = debtFileList.iterator();
						while (fileIT.hasNext()) {
							SBox fileBox = (SBox) fileIT.next();
							if (!fileBox.isEmpty("fileNm")) {
								//fileBox.set("debnId", null); // 기존증빙파일은 채권첨부파일 테이블에서 가져오기 때문에 null 처리함
								fileBox.set("ch_fileNm", fileBox.getString("fileNm"));
								if(debtStatHistId.length()>0){
									fileBox.set("debtStatHistId", debtStatHistId);
								}
								SBox fileInsertResult = null;
								try {//
									fileBox.set("fileCol","B");
									if("oracle".equals(dbmsType)){
										fileInsertResult = debtDaoForOracle.insertDebtStatFile(fileBox);
									}else{
										fileInsertResult = debtDaoForMssql.insertDebtStatFile(fileBox);
									}
								} catch (BizException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								if (!fileBox.isEmpty("debtApplSeq")) {
									File debtFileDir = new File(debtFilePath + "B/" + fileBox.getString("debtApplSeq"));
									if (!debtFileDir.isDirectory()) {
										if(debtFileDir.mkdir()) {
											logger.debug("[3] 채무불이행신청서 증빙파일 디렉토리 생성[ 경로 : "+ debtFilePath  + "B/" + fileBox.getString("debtApplSeq") +" ]");
										}
									}
								}
								
								byte[] fileBinary = fileBox.get("file");
								if ("00000".equals(fileInsertResult.getString("REPL_CD"))) {
									File saveFile = new File(debtFilePath  + "B/" + fileBox.getString("filePath"));
									FileUtil.write(saveFile,fileBinary);
									
								} else {
									//throw new BizException(fileInsertResult.getString("REPL_CD"), EErrorCodeType.search(fileInsertResult.getString("REPL_CD")).getErrMsg());
								}
								logger.debug("[4] 채무불이행신청 상태변경 파일 INSERT 결과코드 : " + fileInsertResult.getString("REPL_CD") + " / 파일명 : " + fileBox.getString("fileNm") + " / 변경 파일명 : " + fileBox.getString("filePath"));	

							}
						}
				//	mapHelper.insertFileData(messageTag.getMessageTagId(), files);
				}
			} catch (SANTException e) {
				logger.debug("첨부파일 처리 실패" + e.getMessage());
				//handleException(messageTag.getMessageTagId(), new SANTException(WSApplication.RESULT_FAIL, "첨부파일 처리 실패" + e.getMessage()));
				
				//TODO
				//retry queue
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		// request to Queue
		try {
			//mapHelper.insertMessageTag(messageTag,getActivity(),getClassName());
			
			logger.debug("send to Adapter queue " + messageTag.toString());
		//	queueHandlerAsync.send(messageTag, null, null);
		
		
		} catch (Exception e) {
			//handleException(messageTag.getMessageTagId(), new SANTException(WSApplication.RESULT_FAIL, "Queue 전송 실패"));
			
			//TODO
			//retry queue
			return;
		}
		
	}
	

}
