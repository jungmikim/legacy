
package com.spsb.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardBusinessDocumentHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardBusinessDocumentHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessScope" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}BusinessScope" minOccurs="0"/>
 *         &lt;element name="DocumentIdentification" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}DocumentIdentification" minOccurs="0"/>
 *         &lt;element name="HasFile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Manifest" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}Manifest" minOccurs="0"/>
 *         &lt;element name="Receiver" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}ArrayOfReceiver" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}Result" minOccurs="0"/>
 *         &lt;element name="Sender" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}Sender" minOccurs="0"/>
 *         &lt;element name="ServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardBusinessDocumentHeader", propOrder = {
    "businessScope",
    "documentIdentification",
    "hasFile",
    "manifest",
    "receiver",
    "result",
    "sender",
    "serviceCode",
    "typeCode"
})
public class StandardBusinessDocumentHeader {

    @XmlElementRef(name = "BusinessScope", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<BusinessScope> businessScope;
    @XmlElementRef(name = "DocumentIdentification", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<DocumentIdentification> documentIdentification;
    @XmlElement(name = "HasFile")
    protected Boolean hasFile;
    @XmlElementRef(name = "Manifest", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<Manifest> manifest;
    @XmlElementRef(name = "Receiver", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<ArrayOfReceiver> receiver;
    @XmlElementRef(name = "Result", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<Result> result;
    @XmlElementRef(name = "Sender", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<Sender> sender;
    @XmlElementRef(name = "ServiceCode", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> serviceCode;
    @XmlElementRef(name = "TypeCode", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> typeCode;

    /**
     * Gets the value of the businessScope property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BusinessScope }{@code >}
     *     
     */
    public JAXBElement<BusinessScope> getBusinessScope() {
        return businessScope;
    }

    /**
     * Sets the value of the businessScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BusinessScope }{@code >}
     *     
     */
    public void setBusinessScope(JAXBElement<BusinessScope> value) {
        this.businessScope = ((JAXBElement<BusinessScope> ) value);
    }

    /**
     * Gets the value of the documentIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentIdentification }{@code >}
     *     
     */
    public JAXBElement<DocumentIdentification> getDocumentIdentification() {
        return documentIdentification;
    }

    /**
     * Sets the value of the documentIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentIdentification }{@code >}
     *     
     */
    public void setDocumentIdentification(JAXBElement<DocumentIdentification> value) {
        this.documentIdentification = ((JAXBElement<DocumentIdentification> ) value);
    }

    /**
     * Gets the value of the hasFile property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasFile() {
        return hasFile;
    }

    /**
     * Sets the value of the hasFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasFile(Boolean value) {
        this.hasFile = value;
    }

    /**
     * Gets the value of the manifest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Manifest }{@code >}
     *     
     */
    public JAXBElement<Manifest> getManifest() {
        return manifest;
    }

    /**
     * Sets the value of the manifest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Manifest }{@code >}
     *     
     */
    public void setManifest(JAXBElement<Manifest> value) {
        this.manifest = ((JAXBElement<Manifest> ) value);
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReceiver }{@code >}
     *     
     */
    public JAXBElement<ArrayOfReceiver> getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReceiver }{@code >}
     *     
     */
    public void setReceiver(JAXBElement<ArrayOfReceiver> value) {
        this.receiver = ((JAXBElement<ArrayOfReceiver> ) value);
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Result }{@code >}
     *     
     */
    public JAXBElement<Result> getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Result }{@code >}
     *     
     */
    public void setResult(JAXBElement<Result> value) {
        this.result = ((JAXBElement<Result> ) value);
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Sender }{@code >}
     *     
     */
    public JAXBElement<Sender> getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Sender }{@code >}
     *     
     */
    public void setSender(JAXBElement<Sender> value) {
        this.sender = ((JAXBElement<Sender> ) value);
    }

    /**
     * Gets the value of the serviceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceCode(JAXBElement<String> value) {
        this.serviceCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTypeCode(JAXBElement<String> value) {
        this.typeCode = ((JAXBElement<String> ) value);
    }

}
