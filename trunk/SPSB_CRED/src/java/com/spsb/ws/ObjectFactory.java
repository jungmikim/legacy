
package com.spsb.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.spsb.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Manifest_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Manifest");
    private final static QName _Sender_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Sender");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _StandardBusinessDocument_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "StandardBusinessDocument");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ManifestItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ManifestItem");
    private final static QName _ArrayOfManifestItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ManifestItem");
    private final static QName _DocumentIdentification_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "DocumentIdentification");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _StandardBusinessDocumentHeader_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "StandardBusinessDocumentHeader");
    private final static QName _Receiver_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Receiver");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _BusinessScope_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "BusinessScope");
    private final static QName _Result_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Result");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _ArrayOfReceiver_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Receiver");
    private final static QName _BusinessScopeEncryptionAlgorithm_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "EncryptionAlgorithm");
    private final static QName _BusinessScopeSecurityToken_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "SecurityToken");
    private final static QName _DocumentIdentificationResponseType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ResponseType");
    private final static QName _DocumentIdentificationInstanceID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "InstanceID");
    private final static QName _DocumentIdentificationCreationDateAndTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "CreationDateAndTime");
    private final static QName _DocumentIdentificationActionType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ActionType");
    private final static QName _DocumentIdentificationType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Type");
    private final static QName _DocumentIdentificationGroupID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "GroupID");
    private final static QName _DocumentIdentificationReferenceID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ReferenceID");
    private final static QName _SenderName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Name");
    private final static QName _SenderID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ID");
    private final static QName _RequestMessageStandardBusinessDocument_QNAME = new QName("http://serivce.host.sant.com/inboundservice", "StandardBusinessDocument");
    private final static QName _StandardBusinessDocumentHeaderTypeCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "TypeCode");
    private final static QName _StandardBusinessDocumentHeaderServiceCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "ServiceCode");
    private final static QName _ManifestNumberOfItems_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "NumberOfItems");
    private final static QName _ReceiverURL_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "URL");
    private final static QName _RequestMessageResponseRequestMessageResult_QNAME = new QName("http://serivce.host.sant.com/inboundservice", "RequestMessageResult");
    private final static QName _ManifestItemDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Description");
    private final static QName _ManifestItemUniformResourceID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "UniformResourceID");
    private final static QName _ManifestItemBinaryData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "BinaryData");
    private final static QName _ManifestItemSequenceNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "SequenceNumber");
    private final static QName _ManifestItemSize_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Size");
    private final static QName _StandardBusinessDocumentMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Sant.Interface.Message", "Message");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.spsb.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusinessScope }
     * 
     */
    public BusinessScope createBusinessScope() {
        return new BusinessScope();
    }

    /**
     * Create an instance of {@link Sender }
     * 
     */
    public Sender createSender() {
        return new Sender();
    }

    /**
     * Create an instance of {@link DocumentIdentification }
     * 
     */
    public DocumentIdentification createDocumentIdentification() {
        return new DocumentIdentification();
    }

    /**
     * Create an instance of {@link RequestMessage }
     * 
     */
    public RequestMessage createRequestMessage() {
        return new RequestMessage();
    }

    /**
     * Create an instance of {@link StandardBusinessDocumentHeader }
     * 
     */
    public StandardBusinessDocumentHeader createStandardBusinessDocumentHeader() {
        return new StandardBusinessDocumentHeader();
    }

    /**
     * Create an instance of {@link Manifest }
     * 
     */
    public Manifest createManifest() {
        return new Manifest();
    }

    /**
     * Create an instance of {@link ArrayOfManifestItem }
     * 
     */
    public ArrayOfManifestItem createArrayOfManifestItem() {
        return new ArrayOfManifestItem();
    }

    /**
     * Create an instance of {@link Receiver }
     * 
     */
    public Receiver createReceiver() {
        return new Receiver();
    }

    /**
     * Create an instance of {@link RequestMessageResponse }
     * 
     */
    public RequestMessageResponse createRequestMessageResponse() {
        return new RequestMessageResponse();
    }

    /**
     * Create an instance of {@link RequestAsyncMessage }
     * 
     */
    public RequestAsyncMessage createRequestAsyncMessage() {
        return new RequestAsyncMessage();
    }

    /**
     * Create an instance of {@link ManifestItem }
     * 
     */
    public ManifestItem createManifestItem() {
        return new ManifestItem();
    }

    /**
     * Create an instance of {@link StandardBusinessDocument }
     * 
     */
    public StandardBusinessDocument createStandardBusinessDocument() {
        return new StandardBusinessDocument();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link ArrayOfReceiver }
     * 
     */
    public ArrayOfReceiver createArrayOfReceiver() {
        return new ArrayOfReceiver();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Manifest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Manifest")
    public JAXBElement<Manifest> createManifest(Manifest value) {
        return new JAXBElement<Manifest>(_Manifest_QNAME, Manifest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Sender")
    public JAXBElement<Sender> createSender(Sender value) {
        return new JAXBElement<Sender>(_Sender_QNAME, Sender.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "StandardBusinessDocument")
    public JAXBElement<StandardBusinessDocument> createStandardBusinessDocument(StandardBusinessDocument value) {
        return new JAXBElement<StandardBusinessDocument>(_StandardBusinessDocument_QNAME, StandardBusinessDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManifestItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ManifestItem")
    public JAXBElement<ManifestItem> createManifestItem(ManifestItem value) {
        return new JAXBElement<ManifestItem>(_ManifestItem_QNAME, ManifestItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfManifestItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ArrayOfManifestItem")
    public JAXBElement<ArrayOfManifestItem> createArrayOfManifestItem(ArrayOfManifestItem value) {
        return new JAXBElement<ArrayOfManifestItem>(_ArrayOfManifestItem_QNAME, ArrayOfManifestItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentIdentification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "DocumentIdentification")
    public JAXBElement<DocumentIdentification> createDocumentIdentification(DocumentIdentification value) {
        return new JAXBElement<DocumentIdentification>(_DocumentIdentification_QNAME, DocumentIdentification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocumentHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "StandardBusinessDocumentHeader")
    public JAXBElement<StandardBusinessDocumentHeader> createStandardBusinessDocumentHeader(StandardBusinessDocumentHeader value) {
        return new JAXBElement<StandardBusinessDocumentHeader>(_StandardBusinessDocumentHeader_QNAME, StandardBusinessDocumentHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Receiver }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Receiver")
    public JAXBElement<Receiver> createReceiver(Receiver value) {
        return new JAXBElement<Receiver>(_Receiver_QNAME, Receiver.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessScope }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "BusinessScope")
    public JAXBElement<BusinessScope> createBusinessScope(BusinessScope value) {
        return new JAXBElement<BusinessScope>(_BusinessScope_QNAME, BusinessScope.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Result }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Result")
    public JAXBElement<Result> createResult(Result value) {
        return new JAXBElement<Result>(_Result_QNAME, Result.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReceiver }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ArrayOfReceiver")
    public JAXBElement<ArrayOfReceiver> createArrayOfReceiver(ArrayOfReceiver value) {
        return new JAXBElement<ArrayOfReceiver>(_ArrayOfReceiver_QNAME, ArrayOfReceiver.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "EncryptionAlgorithm", scope = BusinessScope.class)
    public JAXBElement<String> createBusinessScopeEncryptionAlgorithm(String value) {
        return new JAXBElement<String>(_BusinessScopeEncryptionAlgorithm_QNAME, String.class, BusinessScope.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "SecurityToken", scope = BusinessScope.class)
    public JAXBElement<String> createBusinessScopeSecurityToken(String value) {
        return new JAXBElement<String>(_BusinessScopeSecurityToken_QNAME, String.class, BusinessScope.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ResponseType", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationResponseType(String value) {
        return new JAXBElement<String>(_DocumentIdentificationResponseType_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "InstanceID", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationInstanceID(String value) {
        return new JAXBElement<String>(_DocumentIdentificationInstanceID_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "CreationDateAndTime", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationCreationDateAndTime(String value) {
        return new JAXBElement<String>(_DocumentIdentificationCreationDateAndTime_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ActionType", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationActionType(String value) {
        return new JAXBElement<String>(_DocumentIdentificationActionType_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Type", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationType(String value) {
        return new JAXBElement<String>(_DocumentIdentificationType_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "GroupID", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationGroupID(String value) {
        return new JAXBElement<String>(_DocumentIdentificationGroupID_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ReferenceID", scope = DocumentIdentification.class)
    public JAXBElement<String> createDocumentIdentificationReferenceID(String value) {
        return new JAXBElement<String>(_DocumentIdentificationReferenceID_QNAME, String.class, DocumentIdentification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Name", scope = Sender.class)
    public JAXBElement<String> createSenderName(String value) {
        return new JAXBElement<String>(_SenderName_QNAME, String.class, Sender.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ID", scope = Sender.class)
    public JAXBElement<String> createSenderID(String value) {
        return new JAXBElement<String>(_SenderID_QNAME, String.class, Sender.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serivce.host.sant.com/inboundservice", name = "StandardBusinessDocument", scope = RequestMessage.class)
    public JAXBElement<StandardBusinessDocument> createRequestMessageStandardBusinessDocument(StandardBusinessDocument value) {
        return new JAXBElement<StandardBusinessDocument>(_RequestMessageStandardBusinessDocument_QNAME, StandardBusinessDocument.class, RequestMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Result }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Result", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<Result> createStandardBusinessDocumentHeaderResult(Result value) {
        return new JAXBElement<Result>(_Result_QNAME, Result.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Manifest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Manifest", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<Manifest> createStandardBusinessDocumentHeaderManifest(Manifest value) {
        return new JAXBElement<Manifest>(_Manifest_QNAME, Manifest.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Sender", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<Sender> createStandardBusinessDocumentHeaderSender(Sender value) {
        return new JAXBElement<Sender>(_Sender_QNAME, Sender.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentIdentification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "DocumentIdentification", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<DocumentIdentification> createStandardBusinessDocumentHeaderDocumentIdentification(DocumentIdentification value) {
        return new JAXBElement<DocumentIdentification>(_DocumentIdentification_QNAME, DocumentIdentification.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "TypeCode", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<String> createStandardBusinessDocumentHeaderTypeCode(String value) {
        return new JAXBElement<String>(_StandardBusinessDocumentHeaderTypeCode_QNAME, String.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReceiver }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Receiver", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<ArrayOfReceiver> createStandardBusinessDocumentHeaderReceiver(ArrayOfReceiver value) {
        return new JAXBElement<ArrayOfReceiver>(_Receiver_QNAME, ArrayOfReceiver.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ServiceCode", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<String> createStandardBusinessDocumentHeaderServiceCode(String value) {
        return new JAXBElement<String>(_StandardBusinessDocumentHeaderServiceCode_QNAME, String.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessScope }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "BusinessScope", scope = StandardBusinessDocumentHeader.class)
    public JAXBElement<BusinessScope> createStandardBusinessDocumentHeaderBusinessScope(BusinessScope value) {
        return new JAXBElement<BusinessScope>(_BusinessScope_QNAME, BusinessScope.class, StandardBusinessDocumentHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "NumberOfItems", scope = Manifest.class)
    public JAXBElement<String> createManifestNumberOfItems(String value) {
        return new JAXBElement<String>(_ManifestNumberOfItems_QNAME, String.class, Manifest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfManifestItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ManifestItem", scope = Manifest.class)
    public JAXBElement<ArrayOfManifestItem> createManifestManifestItem(ArrayOfManifestItem value) {
        return new JAXBElement<ArrayOfManifestItem>(_ManifestItem_QNAME, ArrayOfManifestItem.class, Manifest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Name", scope = Receiver.class)
    public JAXBElement<String> createReceiverName(String value) {
        return new JAXBElement<String>(_SenderName_QNAME, String.class, Receiver.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "ID", scope = Receiver.class)
    public JAXBElement<String> createReceiverID(String value) {
        return new JAXBElement<String>(_SenderID_QNAME, String.class, Receiver.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "URL", scope = Receiver.class)
    public JAXBElement<String> createReceiverURL(String value) {
        return new JAXBElement<String>(_ReceiverURL_QNAME, String.class, Receiver.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serivce.host.sant.com/inboundservice", name = "RequestMessageResult", scope = RequestMessageResponse.class)
    public JAXBElement<StandardBusinessDocument> createRequestMessageResponseRequestMessageResult(StandardBusinessDocument value) {
        return new JAXBElement<StandardBusinessDocument>(_RequestMessageResponseRequestMessageResult_QNAME, StandardBusinessDocument.class, RequestMessageResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serivce.host.sant.com/inboundservice", name = "StandardBusinessDocument", scope = RequestAsyncMessage.class)
    public JAXBElement<StandardBusinessDocument> createRequestAsyncMessageStandardBusinessDocument(StandardBusinessDocument value) {
        return new JAXBElement<StandardBusinessDocument>(_RequestMessageStandardBusinessDocument_QNAME, StandardBusinessDocument.class, RequestAsyncMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Description", scope = ManifestItem.class)
    public JAXBElement<String> createManifestItemDescription(String value) {
        return new JAXBElement<String>(_ManifestItemDescription_QNAME, String.class, ManifestItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "UniformResourceID", scope = ManifestItem.class)
    public JAXBElement<String> createManifestItemUniformResourceID(String value) {
        return new JAXBElement<String>(_ManifestItemUniformResourceID_QNAME, String.class, ManifestItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataHandler }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "BinaryData", scope = ManifestItem.class)
    @XmlMimeType("application/octet-stream")
    public JAXBElement<DataHandler> createManifestItemBinaryData(DataHandler value) {
        return new JAXBElement<DataHandler>(_ManifestItemBinaryData_QNAME, DataHandler.class, ManifestItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "SequenceNumber", scope = ManifestItem.class)
    public JAXBElement<String> createManifestItemSequenceNumber(String value) {
        return new JAXBElement<String>(_ManifestItemSequenceNumber_QNAME, String.class, ManifestItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Size", scope = ManifestItem.class)
    public JAXBElement<String> createManifestItemSize(String value) {
        return new JAXBElement<String>(_ManifestItemSize_QNAME, String.class, ManifestItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Message", scope = StandardBusinessDocument.class)
    public JAXBElement<String> createStandardBusinessDocumentMessage(String value) {
        return new JAXBElement<String>(_StandardBusinessDocumentMessage_QNAME, String.class, StandardBusinessDocument.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StandardBusinessDocumentHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "StandardBusinessDocumentHeader", scope = StandardBusinessDocument.class)
    public JAXBElement<StandardBusinessDocumentHeader> createStandardBusinessDocumentStandardBusinessDocumentHeader(StandardBusinessDocumentHeader value) {
        return new JAXBElement<StandardBusinessDocumentHeader>(_StandardBusinessDocumentHeader_QNAME, StandardBusinessDocumentHeader.class, StandardBusinessDocument.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "Description", scope = Result.class)
    public JAXBElement<String> createResultDescription(String value) {
        return new JAXBElement<String>(_ManifestItemDescription_QNAME, String.class, Result.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", name = "TypeCode", scope = Result.class)
    public JAXBElement<String> createResultTypeCode(String value) {
        return new JAXBElement<String>(_StandardBusinessDocumentHeaderTypeCode_QNAME, String.class, Result.class, value);
    }

}
