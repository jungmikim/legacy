
package com.spsb.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Manifest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Manifest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ManifestItem" type="{http://schemas.datacontract.org/2004/07/Sant.Interface.Message}ArrayOfManifestItem" minOccurs="0"/>
 *         &lt;element name="NumberOfItems" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Manifest", propOrder = {
    "manifestItem",
    "numberOfItems"
})
public class Manifest {

    @XmlElementRef(name = "ManifestItem", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<ArrayOfManifestItem> manifestItem;
    @XmlElementRef(name = "NumberOfItems", namespace = "http://schemas.datacontract.org/2004/07/Sant.Interface.Message", type = JAXBElement.class)
    protected JAXBElement<String> numberOfItems;

    /**
     * Gets the value of the manifestItem property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfManifestItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfManifestItem> getManifestItem() {
        return manifestItem;
    }

    /**
     * Sets the value of the manifestItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfManifestItem }{@code >}
     *     
     */
    public void setManifestItem(JAXBElement<ArrayOfManifestItem> value) {
        this.manifestItem = ((JAXBElement<ArrayOfManifestItem> ) value);
    }

    /**
     * Gets the value of the numberOfItems property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumberOfItems() {
        return numberOfItems;
    }

    /**
     * Sets the value of the numberOfItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumberOfItems(JAXBElement<String> value) {
        this.numberOfItems = ((JAXBElement<String> ) value);
    }

}
