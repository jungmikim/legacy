//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.24 at 10:23:20 오전 KST 
//


package com.spsb.schema.cust.ew.response.user;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DefaultAmendmentRequestDocumentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultAmendmentRequestDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID">
 *           &lt;simpleType>
 *             &lt;restriction base="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:13}IDType">
 *               &lt;length value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UpdateAssignedToRoleDateTime" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:13}DateTimeType"/>
 *         &lt;element name="UpdatedUserID">
 *           &lt;simpleType>
 *             &lt;restriction base="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:13}IDType">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TypeCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:13}CodeType">
 *               &lt;maxLength value="2"/>
 *               &lt;enumeration value="AC"/>
 *               &lt;enumeration value="PY"/>
 *               &lt;enumeration value="FA"/>
 *               &lt;enumeration value="FB"/>
 *               &lt;enumeration value="RR"/>
 *               &lt;enumeration value="DE"/>
 *               &lt;enumeration value="R"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Name" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:13}TextType">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ReferencedDefaultAmendmentDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:12}ReferencedDefaultAmendmentDocumentType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultAmendmentRequestDocumentType", propOrder = {
    "id",
    "updateAssignedToRoleDateTime",
    "updatedUserID",
    "typeCode",
    "name",
    "referencedDefaultAmendmentDocument"
})
public class DefaultAmendmentRequestDocumentType {

    @XmlElement(name = "ID", required = true)
    protected String id;
    @XmlElement(name = "UpdateAssignedToRoleDateTime")
    protected double updateAssignedToRoleDateTime;
    @XmlElement(name = "UpdatedUserID", required = true)
    protected String updatedUserID;
    @XmlElement(name = "TypeCode", required = true)
    protected String typeCode;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "ReferencedDefaultAmendmentDocument", required = true)
    protected ReferencedDefaultAmendmentDocumentType referencedDefaultAmendmentDocument;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the updateAssignedToRoleDateTime property.
     * 
     */
    public double getUpdateAssignedToRoleDateTime() {
        return updateAssignedToRoleDateTime;
    }

    /**
     * Sets the value of the updateAssignedToRoleDateTime property.
     * 
     */
    public void setUpdateAssignedToRoleDateTime(double value) {
        this.updateAssignedToRoleDateTime = value;
    }

    /**
     * Gets the value of the updatedUserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdatedUserID() {
        return updatedUserID;
    }

    /**
     * Sets the value of the updatedUserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdatedUserID(String value) {
        this.updatedUserID = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the referencedDefaultAmendmentDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDefaultAmendmentDocumentType }
     *     
     */
    public ReferencedDefaultAmendmentDocumentType getReferencedDefaultAmendmentDocument() {
        return referencedDefaultAmendmentDocument;
    }

    /**
     * Sets the value of the referencedDefaultAmendmentDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDefaultAmendmentDocumentType }
     *     
     */
    public void setReferencedDefaultAmendmentDocument(ReferencedDefaultAmendmentDocumentType value) {
        this.referencedDefaultAmendmentDocument = value;
    }

}
