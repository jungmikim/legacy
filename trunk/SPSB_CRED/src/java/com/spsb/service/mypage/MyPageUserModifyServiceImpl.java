package com.spsb.service.mypage;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EMemberUserGrantCode;
import com.spsb.common.enumtype.ESessionNameType;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonCode;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.dao.mypage.MyPageBelongingDaoForMssql;
import com.spsb.dao.mypage.MyPageBelongingDaoForOracle;
import com.spsb.dao.mypage.MyPageEmployeeDaoForMssql;
import com.spsb.dao.mypage.MyPageEmployeeDaoForOracle;
import com.spsb.dao.mypage.MyPageUserModifyDaoForMssql;
import com.spsb.dao.mypage.MyPageUserModifyDaoForOracle;

/**
 * <pre>
 * 마이페이지 -정보조회 
 * </pre>
 * @author JUNG MI KIM
 * @since 2013. 10. 15.
 * @version 1.0
 */
public class MyPageUserModifyServiceImpl extends SuperService implements MyPageUserModifyService {

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private MyPageUserModifyDaoForOracle myPageUserModifyDaoForOracle;
	@Autowired
	private MyPageUserModifyDaoForMssql myPageUserModifyDaoForMssql;

	@Autowired
	private MyPageEmployeeDaoForOracle mypageEmployeeDaoForOracle;
	@Autowired
	private MyPageEmployeeDaoForMssql mypageEmployeeDaoForMssql;
	
	@Autowired
	private MyPageBelongingDaoForOracle myPageBelongingDaoForOracle;
	@Autowired
	private MyPageBelongingDaoForMssql myPageBelongingDaoForMssql;
	
	@Autowired
	private CustomerDaoForOracle customerDaoForOracle;
	@Autowired
	private CustomerDaoForMssql customerDaoForMssql;
	
	//@Autowired
	//private DebentureDao debentureDao;
	
	
	/**
	 * <pre>
	 * 기업회원 : 정보수정 - 마이페이지
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 03. 19.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId[1] : 기업회원 순번
	 * @return
	 */
	@Override
	public SBox getCompUserInfo(SBox sBox) {
		SBox result = new SBox();
		SBox commonCodeBox = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer compUsrId = 0;
			compUsrId = sBox.get(ESessionNameType.COMP_USR_ID.getName());
			
			//2. resultMsg 초기화
			result.set("resultMsg", "SUCCESS");
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);

			//3. 조회 / 결과 저장
			result.set("resultSbox", "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.selectCompUser(compUsrId) : 
																myPageUserModifyDaoForMssql.selectCompUser(compUsrId));
			result.set("commonCodeBox", commonCodeBox);

		}  catch (Exception ex) {
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION["  + resultMsg + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 거래처 신용정보 변동 현황 - 해당 회사별 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 7. 1.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getCustCreditSummaryList(SBox sBox) {
		SBox result = new SBox();
		try {
			
			result.set("resultBox", "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.selectCustomerEWRatingStat(sBox) : 
															   myPageUserModifyDaoForMssql.selectCustomerEWRatingStat(sBox));
			result.set("basicInfo", sBox);
			result.set("resultMsg", "SUCCESS");

		}  catch (Exception ex) {
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}
	
	
	
	/**
	 * <pre>
	 * 개인기업 : 정보수정 - 마이페이지 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 03. 19.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getUserInfo(SBox sBox) {
		SBox result = new SBox();
		SBox commonCodeBox = new SBox();
		SBox paramVO = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer mbrId = 0;
			mbrId = sBox.get(ESessionNameType.MBR_ID.getName()); //"sessionUsrId"
			int mbrAdmListCnt = 0;
			int mbrListCnt = 0;
			
			//멤버 테이블의 관리자 합계 조회 파라미터
			sBox.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName())); 
			sBox.set("stat", "N"); 
			sBox.set("mbrType", "J"); 
			sBox.set("mbrId", sBox.get(ESessionNameType.MBR_ID.getName())); 
			sBox.set("admYn", "Y"); 
			
			//멤버 테이블의 회원(비관리자) 합계 조회 파라미터
			paramVO.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName())); 
			paramVO.set("stat", "N"); 
			paramVO.set("mbrType", "J"); 
			paramVO.set("mbrId", sBox.get(ESessionNameType.MBR_ID.getName())); 
			paramVO.set("admYn", "N"); 
			
			//2. resultMsg 초기화 및 콤보박스 정의
			result.set("resultMsg", "SUCCESS");
			commonCodeBox.set("phoneCodeList", CommonCode.phoneCodeList);
			commonCodeBox.set("cellPhoneCodeList", CommonCode.cellPhoneCodeList);
			commonCodeBox.set("emailCodeList", CommonCode.emailCodeList);
			
			//3. 멤버수 수 조회
			//mbrAdmListCnt = myPageUserModifyDao.selectMbrListTotalCountForAdm(sBox);
			//mbrListCnt = myPageUserModifyDao.selectMbrListTotalCountForAdm(paramVO);
			
			//4. 조회 / 결과 저장
			result.set("resultSbox", "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.selectUser(mbrId) : 
															    myPageUserModifyDaoForMssql.selectUser(mbrId));
			//result.set("resultDeptSbox", myPageUserModifyDao.selectMbr(usrId));
			result.set("mbrAdmListCnt", mbrAdmListCnt);
			result.set("mbrListCnt", mbrListCnt);
			result.set("commonCodeBox", commonCodeBox);
			
		}  catch (Exception ex) {
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}
	
	/**
	 * <pre>
	 * 채무불이행 상태요약 - 해당 회사별 조회 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 7. 1.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getSummaryList(SBox sBox) {
		SBox result = new SBox();
		try {
			
			// [1-2] 기타 파라미터 초기화
			// PARAMETER 초기화를 위해 시작날짜, 종료날짜 세팅
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");

			// 종료 날짜 세팅
			String edDt = frm.format(cal.getTime());
			// 시작 날짜 세팅 ( 1달전 )
			cal.add(Calendar.MONTH, -1);
			String stDt = frm.format(cal.getTime());
			
			sBox.setIfEmpty("periodStDt", stDt); // 검색조건 : 조회기간 시작날짜(현재 월 기준 1일)
			sBox.setIfEmpty("periodEdDt", edDt); // 검색조건 : 조회기간 종료날짜(현재 월 기준 말일)
			result.set("resultBox", "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.selectSummaryList(sBox) : 
															   myPageUserModifyDaoForMssql.selectSummaryList(sBox));
			result.set("basicInfo", sBox);
			result.set("resultMsg", "SUCCESS");

		}  catch (Exception ex) {
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}
	
	
	/**
	 * <pre>
	 * 개인회원  정보 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 4.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public SBox modifyUserForPW(SBox sBox) {
		
		SBox resultUpdate = new SBox();
		//SBox resultBox = new SBox();
		SBox result = new SBox();
		//SBox resultDeptSbox = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer mbrId = 0;
			
			// 2. 개인 과 기업 파라미터 값 셋팅
			//** 전화번호 합치기(개인)
			String loginPw = sBox.get("psw1");
			mbrId = sBox.get(ESessionNameType.MBR_ID.getName());
			
			sBox.set("mbrId", mbrId);
			sBox.set("loginPw", loginPw);
			
			//3. resultMsg 초기화
			result.set("resultMsg", "SUCCESS");

			//4. 수정  및  조회 / 결과 저장
			//개인 정보 수정
			resultUpdate = "oracle".equals(dbmsType)? myPageUserModifyDaoForOracle.updateUserForPW(sBox)
													 :myPageUserModifyDaoForMssql.updateUserForPW(sBox);
			//resultBox = myPageUserModifyDao.selectUser(mbrId);
			/*resultDeptSbox = myPageUserModifyDao.selectMbr(usrId);*/
			
			result.set("resultUpdate",resultUpdate);
			//result.set("resultSbox",resultBox);
			//result.set("resultDeptSbox",resultDeptSbox);

		}  catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	} 
	
	
	/**
	 * <pre>
	 * 기업회원 정보수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 16.
	 * @version 1.0
	 * @param sBox - sessionCompUsrId:기업회원 순번, sessionCustType:거래처 유형			
	 * @return
	 
	@Override
	@Transactional
	public SBox modifyCompUser(SBox sBox) {
		
		SBox resultUpdate = new SBox();
		SBox resultBox = new SBox();
		SBox result = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer compUsrId = 0;
			
			//전화번호 합치기
			String telNo1 = sBox.get("telNo1");
			String telNo2 = sBox.get("telNo2");
			String telNo3 = sBox.get("telNo3");
			String telNo = telNo1+"-"+telNo2+"-"+telNo3;
			
			String cellNo1 = sBox.get("cellNo1");
			String cellNo2 = sBox.get("cellNo2");
			String cellNo3 = sBox.get("cellNo3");
			String cellNo = cellNo1+"-"+cellNo2+"-"+cellNo3;
			
			
			//대표자 주민번호 합치기
			
			//우편번호 합치기
			String postNo1 = sBox.get("postNo1");
			String postNo2 = sBox.get("postNo2");
			String postNo = postNo1+postNo2;
			
			compUsrId = sBox.get(ESessionNameType.COMP_USR_ID.getName());
			
			String corpNo = sBox.get("corpNo");
			
			if(corpNo.isEmpty()){
				sBox.set("corpNo", null);
			}
			sBox.setIfEmpty("corpNo", null);
			sBox.set("compUsrId", compUsrId);
			sBox.set("telNo", telNo);
			sBox.set("postNo", postNo);
			sBox.set("cellNo", cellNo);
			if(sBox.isEmpty("certInfo")){
				sBox.set("certInfo", null);
			}
			
			String payTermDay=sBox.get("payTermDay");
			if(payTermDay.isEmpty()){
				sBox.set("payTermDay", null);
			}
			
			sBox.setIfEmpty("payTermDay", null);
			
			//3. resultMsg 초기화
			result.set("resultMsg", "SUCCESS");

			//4. 조회 / 결과 저장
			resultUpdate = myPageUserModifyDao.updateCompUser(sBox);
			resultBox = myPageUserModifyDao.selectCompUser(compUsrId);
			result.set("resultUpdate",resultUpdate);
			result.set("resultSbox",resultBox);

		}  catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}
	*/
	
	/**
	 * <pre>
	 * 개인회원  정보 수정
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 4.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@Override
	@Transactional
	public SBox modifyUser(SBox sBox) {
		
		SBox sBoxDept = new SBox();
		SBox resultUpdate = new SBox();
		SBox resultDeptUpdate = new SBox();
		SBox resultBox = new SBox();
		SBox resultDeptSbox = new SBox();
		SBox result = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer usrId = 0;
			Integer mbrId = 0;
			
			// 2. 개인 과 기업 파라미터 값 셋팅
			//** 전화번호 합치기(개인)
			//String loginPw = sBox.get("psw1");
			usrId = sBox.get(ESessionNameType.USR_ID.getName());
			
			
			//우편번호 합치기(개인)
			String postNo1 = sBox.get("postNo1");
			String postNo2 = sBox.get("postNo2");
			String postNo = postNo1+postNo2;
			sBox.set("postNo", postNo);
			
			sBox.set("usrId", usrId);
			//sBox.set("loginPw", loginPw);
			
			//** 전화번호 합치기(기업)
			mbrId = sBox.get(ESessionNameType.MBR_ID.getName());
		
			String telNoDept1 = sBox.get("telNoDept1");
			String telNoDept2 = sBox.get("telNoDept2");
			String telNoDept3 = sBox.get("telNoDept3");
			String telNoDept = telNoDept1+"-"+telNoDept2+"-"+telNoDept3;
			
			String faxNo1 = sBox.get("faxNo1");
			String faxNo2 = sBox.get("faxNo2");
			String faxNo3 = sBox.get("faxNo3");
			String faxNo = faxNo1+"-"+faxNo2+"-"+faxNo3;
			sBoxDept.set("faxNo", faxNo);
			
			if(faxNo2.isEmpty()||faxNo3.isEmpty()){
				sBoxDept.set("faxNo", null);
			}
			
			
			String cellNoDept1 = sBox.get("cellNoDept1");
			String cellNoDept2 = sBox.get("cellNoDept2");
			String cellNoDept3 = sBox.get("cellNoDept3");
			String cellNoDept = cellNoDept1+"-"+cellNoDept2+"-"+cellNoDept3;
			
			sBox.setIfEmpty("postNo", null);
			sBox.setIfEmpty("custAddr1", null);
			sBox.setIfEmpty("custAddr2", null);
			sBox.set("cellNo", cellNoDept);//Member기준 변경 2014.04.21
			sBox.set("telNo", telNoDept);//Member기준 변경 2014.04.21
			sBox.set("email", sBox.get("emailTypeDept01")); //Member기준 변경 2014.04.21
			sBox.set("usrNm", sBox.get("usrNm")); //Member기준 변경 2014.04.21
			
			sBoxDept.setIfEmpty("faxNo", null);
			sBoxDept.set("mbrId", mbrId);
			sBoxDept.set("mbrUsrId", usrId); //update 아이디 
			sBoxDept.set("deptNm", sBox.get("deptNm"));
			sBoxDept.set("telNo", telNoDept);
			sBoxDept.set("cellNo", cellNoDept);
			sBoxDept.set("email", sBox.get("emailTypeDept01"));
			sBoxDept.set("usrNm", sBox.get("usrNm"));
			sBoxDept.set("jobTlNm", sBox.get("jobTlNm"));
			
			
			//3. resultMsg 초기화
			result.set("resultMsg", "SUCCESS");

			//4. 수정  및  조회 / 결과 저장
			//개인 정보 수정
			resultUpdate = myPageUserModifyDao.updateUser(sBox);
			resultBox = myPageUserModifyDao.selectUser(usrId);
			//기업 정보 수정
			resultDeptUpdate = myPageUserModifyDao.updateMbr(sBoxDept);
			resultDeptSbox = myPageUserModifyDao.selectMbr(usrId);
			
			result.set("resultUpdate",resultUpdate);
			result.set("resultSbox",resultBox);
			result.set("resultDeptUpdate",resultDeptUpdate);
			result.set("resultDeptSbox",resultDeptSbox);
			

		}  catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	} */
	
	
	
	

	/**
	 * <pre>
	 * 개인회원 - 회원 탈퇴  
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 10. 31.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@Override
	@Transactional
	public SBox modifyUsrSecession(SBox sBox) {
			SBox result = new SBox();
			SBox paramVO = new SBox();
			SBox paramVO2 = new SBox();
			try {
				result.set("resultCode", "00");
				result.set("resultMsg", "SUCCESS");
				
				sBox.set("usrId", sBox.get(ESessionNameType.USR_ID.getName()));
				sBox.set("stat", "R"); //R:탈퇴요청
				result.set("compResult", myPageUserModifyDao.updateUsrSecession(sBox));
				
				paramVO.set("usrId", sBox.get(ESessionNameType.USR_ID.getName()));
				paramVO.set("mbrId", sBox.get(ESessionNameType.MBR_ID.getName()));
				paramVO.set("stat", "D"); //탈퇴처리
				result.set("mbrResult", myPageUserModifyDao.updateMbrSecession(paramVO));
				
				//멤버가 없는 유일 관리자일경우 
				paramVO2.set("compUsrId", sBox.get(ESessionNameType.COMP_USR_ID.getName()));
				paramVO2.set("stat", "R"); //R:탈퇴요청
				if(sBox.getInt("mbrAdmListCnt")==0 &&sBox.getInt("mbrListCnt")==0){
					result.set("userCompResult", myPageUserModifyDao.updateCompUsrSecession(paramVO2));
				}
				
			}  catch (Exception ex) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				String resultMsg = ex.getMessage();
				log.e("Unknown EXCEPTION[" + resultMsg + "]");
			} 
			
			return result;
	} */
	
	
	/**
	 * <pre>
	 * 마이페이지 - 채권관리 현황 조회
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 14.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getDebentureListCountByMypage(SBox sBox) {
		SBox result = new SBox();
		
		try {
			sBox.set("admYn",sBox.get(ESessionNameType.ADM_YN.getName()));
			sBox.set("compUsrId",sBox.get(ESessionNameType.COMP_USR_ID.getName()));
			sBox.set("mbrUsrId",sBox.get(ESessionNameType.MBR_ID.getName()));
			sBox.set("grnCd","B"); //CD_MBR 테이블의 GRN_CD DEBN_SEARCH_GRN("B", "채권전체조회 권한")
			
			result.set("resultMsg", "SUCCESS");
			
			// PARAMETER 초기화를 위해 시작날짜, 종료날짜 세팅
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat frm = new SimpleDateFormat("yyyy/MM/dd");

			// 종료 날짜 세팅
			String edDt = frm.format(cal.getTime());
			// 시작 날짜 세팅
			cal.add(Calendar.MONTH, -1);
			String stDt = frm.format(cal.getTime());
			
			sBox.set("firstDate", stDt); // 수금조회 // 초기날짜
			sBox.set("lastDate", edDt); // 수금조회 마지막날짜

			//[1] 채권관리 현황 연체정보/부실정보 
			//result.set("resultBox", myPageUserModifyDao.selectDebentureListCountByMypage(sBox));
			//[2] 수금 계획 및 실적 정보조회
			//result.set("resultColMap", myPageUserModifyDao.selectColListForMypage(sBox));
			
			// [2] 신용정보 변동이력 오늘 건수 조회
			//int customerCrdHistoryTodayTotalCount = customerDao.selectCustomerCreditHistoryTotalCount(sBox.getInt(ESessionNameType.COMP_USR_ID.getName()), "WEEK");
			// [3] 신용정보 변동이력 최근90일 건수 조회
			//int customerCrdHistoryRecentDayTotalCount = customerDao.selectCustomerCreditHistoryTotalCount(sBox.getInt(ESessionNameType.COMP_USR_ID.getName()), "RECENT");
			
			//result.set("customerCrdHistoryTodayTotalCount", customerCrdHistoryTodayTotalCount);
			//result.set("customerCrdHistoryRecentDayTotalCount", customerCrdHistoryRecentDayTotalCount);
			//2014년 3월 27일 김정미 수정 산기대 쿼리 수정후 주석 제거해야함  result.set("debentureTotalCount", debentureTotalCount);
			//2014년 3월 27일 김정미 수정 산기대 쿼리 수정후 주석 제거해야함  result.set("debentureTotalCountByPlan", debentureTotalCountByPlan);
			
			
		}  catch (Exception ex) {
			ex.printStackTrace();
			//String resultMsg = ex.getMessage();
			//log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}
	
	/**
	 * <pre>
	 * 개인회원  - 마이페이지 메인 소속기업 현황 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 11. 5.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getBelongingtoCompanyList(SBox sBox) {
		SBoxList<SBox> belongingtoCompanyList; 
		SBox result = new SBox();
		
		try {
			belongingtoCompanyList = "oracle".equals(dbmsType)? myPageBelongingDaoForOracle.selectBelongingtoCompanyList(sBox):
																myPageBelongingDaoForMssql.selectBelongingtoCompanyList(sBox);
				String grnDesc ="";
				String admDesc ="";
				String debtGrnt ="";
				String custSearchGrnt =""; 
				String custBrifGrnt = "";
				String custEwDetailGrnt = "";
				String custEwAddGrnt = "";
				
				String StatDesc =""; //상태정보
				//상태가 N인것만 뿌려주기
				for(int i=0; i<belongingtoCompanyList.size(); i++){
					debtGrnt = belongingtoCompanyList.get(i).getString("CRED1C");
					custSearchGrnt = belongingtoCompanyList.get(i).getString("SBDE1R");
					custBrifGrnt = belongingtoCompanyList.get(i).getString("SBDE2R");
					custEwDetailGrnt = belongingtoCompanyList.get(i).getString("SBDE3R");
					custEwAddGrnt = belongingtoCompanyList.get(i).getString("SBDE4R");
					
					if("Y".equals(debtGrnt)){
						grnDesc+= EMemberUserGrantCode.DEBN_GRN.getDesc()+", ";	
					}else{
						grnDesc+="";
					}
					
					if("Y".equals(custSearchGrnt)){
						grnDesc+= EMemberUserGrantCode.CUST_SEARCH_GRN.getDesc()+", ";	
					}else{
						grnDesc+="";
					}
					
					if("Y".equals(custBrifGrnt)){
						grnDesc+= EMemberUserGrantCode.CUST_BRIF_GRN.getDesc()+", ";	
					}else{
						grnDesc+="";
					}
					
					if("Y".equals(custEwDetailGrnt)){
						grnDesc+= EMemberUserGrantCode.CUST_EW_DETAIL_GRN.getDesc()+", ";		
					}else{
						grnDesc+="";
					}
					if("Y".equals(custEwAddGrnt)){
						grnDesc+= EMemberUserGrantCode.CUST_EW_ADD_GRN.getDesc();	
					}else{
						grnDesc+="";
					}
					
					String admYn = belongingtoCompanyList.get(i).getString("ADM_YN");
					if("Y".equals(admYn)){
						admDesc+= "관리자,";	
					}else{
						admDesc+="";
					}
						
					if("N".equals(belongingtoCompanyList.get(i).getString("STAT"))){
						StatDesc = "정상";
					}
					else if("D".equals(belongingtoCompanyList.get(i).getString("STAT"))){
						StatDesc = "삭제";
					}
					else{ 
						StatDesc = "-";
					}
					belongingtoCompanyList.get(i).set("GRN_DESC", grnDesc);
					belongingtoCompanyList.get(i).set("ADM_DESC", admDesc);
					belongingtoCompanyList.get(i).set("STAT_DESC", StatDesc);
				}
				result.set("resultList",belongingtoCompanyList);
				result.set("resultListSize",belongingtoCompanyList.size());
				//result.set("admListSize",admSize);
				
			}  catch (Exception ex) {
				String resultMsg = ex.getMessage();
				log.e("Unknown EXCEPTION[" + resultMsg + "]");
			} 
			return result;
	}
	
	
	/**
	 * <pre>
	 *  마이페이지 소속기업에 속한 사용자 정보변경 수정(이름/전화번호/이메일)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2013. 12. 2.
	 * @version 1.0
	 * @param sBox
	 * @return
	
	@Override
	@Transactional
	public SBox modifyBelongtoCompanyListInfo(SBox sBox) {
		SBox result = new SBox();
		try {
			//resultCode, resultMsg 초기화
			result.set("resultMsg", "SUCCESS");
			//조회 / 결과 저장
			result.set("resultBox",myPageUserModifyDao.updateBelongtoCompanyListInfo(sBox));

		}  catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	} */
	
	/**
	 * <pre>
	 * 관리자 페이지 - 인증서 갱신(수정)
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 04. 3.
	 * @version 1.0
	 * @param sBox
	 * @return
	 
	@Override
	@Transactional
	public SBox modifyCertInfo(SBox sBox) {
		
		SBox resultUpdate = new SBox();
		SBox resultBox = new SBox();
		SBox result = new SBox();
		
		try {
			// 1. 변수 초기화
			Integer compUsrId = 0;
			
			compUsrId = sBox.get(ESessionNameType.COMP_USR_ID.getName());
			
			//2. resultMsg 초기화
			result.set("resultMsg", "SUCCESS");
			sBox.set("compUsrId", compUsrId);
			//3. 수정  및  조회 / 결과 저장
			//개인 정보 수정
			resultUpdate = myPageUserModifyDao.updateCertInfo(sBox);
			resultBox = myPageUserModifyDao.selectCompUser(compUsrId);
			
			result.set("resultUpdate",resultUpdate);
			result.set("resultSbox",resultBox);

		}  catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			String resultMsg = ex.getMessage();
			log.e("Unknown EXCEPTION[" + resultMsg + "]");
		} 
		return result;
	}*/
	

	
	
	/**
	 * <pre>
	 * 스마트빌연계 기업회원 정보 수정 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox - loginId : 회원아이디, usrNm: 회사명,  ownNm : 대표자명, bizCond : 업태,  bizType : 업종, addr: 주소, postNo: 우편번호, telNo: 전화번호, corpNo: 법인등록번호 ,
	 * 				 email: 이메일,  mbNo: 전화번호, certInfo: 인증서정보		
	 * @return
	
	@Override
	public SBox modifyCompUserForSB(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		
		sBox.set("stat", "N");	
		if (sBox.isEmpty("corpNo")) {
			sBox.set("corpNo", null);
		}
		if (sBox.isEmpty("bizCond")) {
			sBox.set("bizCond", null);
		}
		if(sBox.isEmpty("addr2")){
			sBox.set("addr2", null);
		}
		if(sBox.isEmpty("signInfo")){
			sBox.set("signInfo", null);
		}
		if(sBox.isEmpty("certInfo")){
			sBox.set("certInfo", null);
		}
		try {
			Integer updateResult = myPageUserModifyDao.modifyCompanyUserForSB(sBox);
			
			if(updateResult != null && updateResult > 0){
				sBox.set("result", EUserErrorCode.SUCCESS);
			}else{
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
				throw new UserException(EUserErrorCode.LOGIN_NOT_EXIST);
			}	
			
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_USR_UPDATE_FAIL);
			throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_USR_UPDATE_FAIL, e);
		}
		return sBox;
	} */
	
	
	/**
	 * <pre>
	 * 스마트빌연계 개인회원 정보 수정 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox - loginId : 회원아이디, usrNm: 회사명,  ownNm : 대표자명, bizCond : 업태,  bizType : 업종, addr: 주소, postNo: 우편번호, telNo: 전화번호, corpNo: 법인등록번호 ,
	 * 				 email: 이메일,  mbNo: 전화번호, certInfo: 인증서정보		
	 * @return
	 
	@Override
	public SBox modifyUserForSB(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		
		if (sBox.isEmpty("deptNm")) {
			sBox.set("deptNm", null);
		}
		
		if (sBox.isEmpty("jobTlNm")) {
			sBox.set("jobTlNm", null);
		}
		if (sBox.isEmpty("mbNo")) {
			sBox.set("mbNo", null);
		}
		try {
			Integer updateResult = myPageUserModifyDao.modifyUserForSB(sBox);
			
			if(updateResult != null && updateResult > 0){
				sBox.set("result", EUserErrorCode.SUCCESS);
			}else{
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
				throw new UserException(EUserErrorCode.LOGIN_NOT_EXIST);
			}	
			
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", EUserErrorCode.USERJOIN_DEBN_USR_UPDATE_FAIL);
			throw new UserException(EUserErrorCode.USERJOIN_DEBN_USR_UPDATE_FAIL, e);
		}
		return sBox;
	}*/
	
	
	/**
	 * <pre>
	 * 스마트빌연계 기업회원 관리자 변경 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2014. 11. 17.
	 * @version 1.0
	 * @param sBox - loginId : 회원아이디, admYn:관리자코드
	 * @return
	
	@Override
	public SBox modifyCompAdmInfoForSB(SBox sBox) throws UserException{
		sBox.set("result", EUserErrorCode.FAIL);
		
		try {
			Integer updateResult = myPageUserModifyDao.modifyCompAdmInfoForSB(sBox);
			
			if(updateResult != null && updateResult > 0){
				sBox.set("result", EUserErrorCode.SUCCESS);
			}else{
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				sBox.set("result", EUserErrorCode.LOGIN_NOT_EXIST);
				throw new UserException(EUserErrorCode.LOGIN_NOT_EXIST);
			}	
			
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			sBox.set("result", EUserErrorCode.USERJOIN_DEBN_COMP_ADM_UPDATE_FAIL);
			throw new UserException(EUserErrorCode.USERJOIN_DEBN_COMP_ADM_UPDATE_FAIL, e);
		}
		return sBox;
	} */
	
	
}