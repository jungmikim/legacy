package com.spsb.service.charge;

import java.util.Map;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;

/**
 * <pre>
 * 요금관리 Service Interface Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
public interface ChargeService {

	public Map<String, SBox> getUserPoCheckPerPrdTypeMap(Integer compUsrId);
	public SBoxList<SBox> getDebtRegChargeList();
	public SBox getUserPoList(SBox sBox);
	public boolean isUserPoCheckByPrdCd(int compUsrId, String prdCd);
}
