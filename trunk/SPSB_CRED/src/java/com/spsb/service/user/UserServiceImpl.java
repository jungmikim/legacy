package com.spsb.service.user;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.spsb.common.collection.SBox;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.mail.CommonMailSender;
import com.spsb.common.util.mail.CommonMailVo;
import com.spsb.dao.user.UserDaoForMssql;
import com.spsb.dao.user.UserDaoForOracle;

/**
 * <pre>
 * 회원(개인, 기업) Service Implements Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 9. 9.
 * @version 1.0
 */
public class UserServiceImpl extends SuperService implements UserService{

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired
	private UserDaoForOracle userDaoForOracle;
	@Autowired
	private UserDaoForMssql userDaoForMssql;
	
	@Autowired
	private CommonMailSender commonMailSender;
	@Value("#{mail['MAIL_SENDER_NAME'].trim()}") 
	private String mailSenderName;
	@Value("#{mail['MAIL_SENDER_EMAIL'].trim()}") 
	private String mailSenderEmail;
	
	/**
	 * <pre>
	 * 기업회원 정보 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 9. 30.
	 * @version 1.0
	 * @param compUsrId : 기업회원순번
	 * @return
	 */
	@Override
	public SBox getCompanyUser(int compUsrId) {
		return "oracle".equals(dbmsType)? userDaoForOracle.selectCompanyUser(compUsrId): userDaoForMssql.selectCompanyUser(compUsrId);
	}
	
	/**
	 * <pre>
	 * 사업자등록번호로 기업회원 정보  Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 10. 21.
	 * @version 1.0
	 * @param usrNo : 사업자등록번호
	 * @return
	 */
	@Override
	public SBox getCompanyUserByUsrNo(String usrNo) {
		return "oracle".equals(dbmsType)? userDaoForOracle.selectCompanyUserByUsrNo(usrNo):userDaoForMssql.selectCompanyUserByUsrNo(usrNo);
	}
	
	/**
	 * <pre>
	 * 중복된 개인회원아이디가 존재하는가 여부 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 28.
	 * @version 1.0
	 * @param loginId : 로그인식별자
	 * @return
	 */
	@Override
	public boolean isDuplicationMemberUser(String loginId) {
		SBox result = getUserByLoginId(loginId);
		return (result == null) ? false:true;
	}
	
	/**
	 * <pre>
	 * 로그인 회원 조회 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 3. 28.
	 * @version 1.0
	 * @param loginId : 로그인식별자
	 * @return
	 */
	@Override
	public SBox getUserByLoginId(String loginId) {
		return "oracle".equals(dbmsType)? userDaoForOracle.selectUserByLoginId(loginId):userDaoForMssql.selectUserByLoginId(loginId);
	}
	
	/**
	 * <pre>
	 * 중복된 사업자번호가 존재하는가 여부 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 25.
	 * @version 1.0
	 * @param usrNo : 사업자등록번호
	 * @return
	 */
	@Override
	public boolean isDuplicationCompany(String usrNo) {
		SBox result = getCompanyUserByUsrNo(usrNo);
		return (result == null) ? false:true;
	}
	
	/**
	 * <pre>
	 * 개인회원순번으로 디폴트 기업회원멤버 존재여부 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2013. 11. 28.
	 * @version 1.0
	 * @param usrId : 개인회원순번
	 * @return
	 */
	@Override
	public boolean isDefaultMember(Integer usrId) {
		Integer result = "oracle".equals(dbmsType)? userDaoForOracle.selectDefaultMbrTotalCountByUserId(usrId):
													userDaoForMssql.selectDefaultMbrTotalCountByUserId(usrId);
		return (result != null && result > 0) ? true:false;
	}
	
	/**
	 * <pre>
	 * 게스트회원 등록 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 2. 7.
	 * @version 1.0
	 * @param email
	 * @return
	 */
	@Override
	public SBox addGuestUser(String email) {
		SBox sBox = new SBox();
		sBox.set("loginId", email);
		sBox.set("loginPw", getGuestTempPassword());
		sBox.set("stat", "I");
		Integer tmpUsrId = "oracle".equals(dbmsType)? userDaoForOracle.insertTempUser(sBox):userDaoForMssql.insertTempUser(sBox);
		sBox.set("result", EUserErrorCode.FAIL);
		if(tmpUsrId != null && tmpUsrId > 0){
			sBox.set("result", EUserErrorCode.SUCCESS);
			sBox.set("tmpUsrId", tmpUsrId);
		}else if(tmpUsrId == 0){
			sBox.set("result", EUserErrorCode.GUEST_DUPLICATION_EMAIL);
		}
		return sBox;
	}
	
	/**
	 * <pre>
	 * 게스트회원 임시비밀번호  Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 6. 23.
	 * @version 1.0
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private String getGuestTempPassword(){
		UUID uuid = UUID.randomUUID();
		String password = uuid.toString().replace("-", "");
		return password;
	}
	
	/**
	 * <pre>
	 * 게스트회원 체험계정 메일보내기 Service Method
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 1. 29.
	 * @version 1.0
	 * @param email
	 * @param content
	 * @throws MessagingException 
	 * @throws UnsupportedEncodingException 
	 */
	@Override
	public void sendEmailGuestUser(String email, String content) throws MessagingException, UnsupportedEncodingException {
		String subject = "채권관리 서비스 체험 계정 Mail Test";
		
		CommonMailVo vo = new CommonMailVo();
		vo.setTo(email);
		vo.setFrom(mailSenderEmail);
		vo.setFromName(mailSenderName);
		vo.setSubject(subject);
		vo.setContent(content);
		commonMailSender.sendMail(vo);
	}
	
}
