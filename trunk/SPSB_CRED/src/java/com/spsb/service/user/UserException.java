package com.spsb.service.user;

import com.spsb.common.enumtype.EUserErrorCode;

/**
 * <pre>
 * 회원 Exception처리
 * </pre>
 * @author JUNG EUN LIM
 * @since 2013. 10. 2.
 * @version 1.0
 */
public class UserException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserException() {
		super();
	}
	
	public UserException(String message){
		super("[UserException]xxxxx( message : " + message + " )");
	}
	
	public UserException(EUserErrorCode errorCode) {
		super("[UserException]xxxxx( [" + errorCode + "] " + errorCode.getDesc() + " )");
	}
	
	public UserException(Throwable cause) {
		super(cause);
    }
	
	
	public UserException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UserException(EUserErrorCode errorCode, Throwable cause) {
		super("[UserException]xxxxx( [" + errorCode + "] " + errorCode.getDesc() + " )", cause);
	}
	
}
