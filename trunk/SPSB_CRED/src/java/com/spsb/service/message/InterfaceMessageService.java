package com.spsb.service.message;
import java.net.MalformedURLException;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.MessageTag;

/**
 * 
 * <pre>
 * 인테페이스 메시지 Service Interface Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
public interface InterfaceMessageService {
	
	
	//[거래처 등록 I/F] 크레딧 서비스 조기 경보 대상 거래처 등록 요청서 CRQREC
	public SBox setEwCustRequestMessage(SBox paramBox);
	
	//[채무불이행 등록 I/F] 크레딧서비스채무불이행등록요청서 CRQRDA
	public SBox setDebtRequestMessage(SBox paramBox);
	
	//[채무불이행 등록 변경  I/F] 크레딧서비스채무불이행등록변경요청서 CRQAMD
	public SBox setDebtAmendRequest(SBox paramBox);
	
	//[채무불이행 등록 변경  I/F] 채무불이행 변경시 상태별 분기 프로세스
	public SBox processMakeDebtAmendRequestMessageAPI(SBox sbox);
	
	//[공통 - 처리결과 통보서 ] NTCCON
	public SBox getIFResultForNoticeConclusionType(MessageTag messageTag);
	
	//[채무불이행 신규 신청 성공 메시지 I/F] CRSRDA (채무불이행 접수번호)
	public void getDebtReqisterIFResultForSucessResponse(MessageTag messageTag);

	
	//[채무불이행 등록 상태 변경 통지서 IF] CNTDUS	
	public String getDebtStatusIFUpdate(MessageTag messageTag);

	
	//[조기경보 대상 거래처 등록 응답서 ] CRSREC -- 사용여부 확인해야함 NTCCON으로 대체됨
	public void getIFResultForCustRegister(MessageTag messageTag) ; 
	

	//[조기경보 대상 거래처 등록업체(거래처) 신용 등급 변경 통지서(Only 조기경보] - CNTUCL
	public void getCreditNoticeUpdateCustomerLevel(MessageTag messageTag) ;
	
	
	//[조기경보 대상 거래처 등록업체(거래처) 신용 등급 변경 통지서(조기경보 + KLINK] - CNTKCL
	public void getCreditNoticeByKLinkUpdateCustomerLevel(MessageTag messageTag) ;
	
	
	////////////////SYNC
	public SBoxList<SBox> getSyncUserListFromMiddleServer(SBox sBox) throws MalformedURLException;
	
	//채무불이행 변경 가능한 상태 Validation
	public SBox ValidationFormForDebtStat(SBox paramBox);	
	
	public int setLicenseKeyForLegacy(String licenseKey);
	
	
}
