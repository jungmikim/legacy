package com.spsb.service.message;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.dto.FileData;
import com.spsb.common.dto.MessageTag;
import com.spsb.common.enumtype.EDebtStatType;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.event.MessageEvent;
import com.spsb.common.exception.BizException;
import com.spsb.common.message.CommonMessageIF;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonUtil;
import com.spsb.common.util.DateUtil;
import com.spsb.common.util.FileUtil;
import com.spsb.common.util.SysUtil;
import com.spsb.connector.wsc.WSClient;
import com.spsb.dao.config.ConfigDaoForMssql;
import com.spsb.dao.config.ConfigDaoForOracle;
import com.spsb.dao.customer.CustomerDaoForMssql;
import com.spsb.dao.customer.CustomerDaoForOracle;
import com.spsb.dao.debt.DebtDaoForMssql;
import com.spsb.dao.debt.DebtDaoForOracle;
import com.spsb.vo.DebtAmendRequestVo;
import com.spsb.vo.DebtRegisterRequestVo;
import com.spsb.vo.EWCustRequestVo;
import com.spsb.vo.MessageTagVo;
import com.spsb.vo.RegistorCustomerVo;
import com.spsb.vo.RequestCommonVo;
import com.spsb.vo.ResponseDocumentCommonVo;
/**
 * <pre>
 * 인터페이지 메시지 Service Implements Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 1.
 * @version 1.0
 */
public class InterfaceMessageServiceImpl extends SuperService implements InterfaceMessageService{

	// db type setting
		@Value("#{dbType['Globals.DbType'].trim()}")
		private String dbmsType;
		
		@Autowired
		private CustomerDaoForOracle customerDaoForOracle;
		@Autowired
		private CustomerDaoForMssql customerDaoForMssql;
		
		
		@Autowired
		private DebtDaoForOracle debtDaoForOracle;
		@Autowired
		private DebtDaoForMssql debtDaoForMssql;
		
		
		@Autowired 
		private ConfigDaoForOracle configDaoForOracle;
		@Autowired 
		private ConfigDaoForMssql configDaoForMssql;
		
		@Autowired
		WSClient wsClient;
		
		// 크레딧서비스의 조기경보 대상 거래처 신규 등록 인터페이스 서비스 코드
		@Value("#{service['CREDIT_REQUEST_EW_CUSTOMER'].trim()}")
		private String serviceCodeRequestEwCustomer;
		
		// 크레딧서비스 채무불이행 등록 요청서  인터페이스 서비스 코드
		@Value("#{service['CREDIT_REQUEST_APPLICATION'].trim()}")
		private String serviceCodeCreditRequestApplication;
		
		// 크레딧서비스 채무불이행 등록 변경요청서 인터페이스 서비스 코드
		@Value("#{service['CREDIT_REQUEST_AMEND'].trim()}")
		private String serviceCodeCreditRequestAmend;
		
		
		// 크레딧서비스회원목록요청서	  인터페이스 서비스 코드
		@Value("#{service['NOTICE_CONCLUSION'].trim()}")
		private String serviceCodeNoticeConclusion;
		
		
		// 크레딧서비스회원목록요청서	  인터페이스 서비스 코드
		@Value("#{service['CREDIT_REQUEST_USERLIST'].trim()}")
		private String serviceCodeRequestUserList;
		
		
		@Value("#{service['SERVICE_CODE_CRED'].trim()}")
		private String serviceCodeCred;

		@Value("#{service['RECEIVER_ID'].trim()}")
		private String receiveId;

		@Value("#{service['RECEIVER_NAME'].trim()}")
		private String receiveName;
		
		///////////////////////채무불이행 ////
		@Value("#{common['DEBTFILE'].trim()}")
		private String debtFilePath;
		//////////////////////////////////
	
		/**
		 * <pre>
		 * DB에 저장되어 있는 채무불이행신청서 데이터 VALIDATION  METHOD
		 * </pre>
		 * @author JUNG MI KIM
		 * @since 2016. 5. 20.
		 * @version 1.0
		 * @param paramBox : 정보가 담긴 SBox
		 * @param type : Validation 로직 Type  
		 * @return
		 */
		public SBox ValidationFormForDebt(SBox paramBox, String type) {

			SBox resultBox = new SBox();
			boolean BTF = true;
			String checkType="NON";
		
			if("DNR".equals(type)){ // Debt New Request
				// 필수 DATA CHECK
				if (paramBox.isEmpty("DEBT_APPL_ID")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.DEBT_APPL_ID";
				} else if (paramBox.isEmpty("COMP_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.COMP_NO"; 
				} else if (paramBox.isEmpty("REG_ID")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.REG_ID"; 
				}else if (paramBox.isEmpty("REG_DT")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.REG_DT"; 
				}else if (paramBox.isEmpty("COMP_TYPE")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.COMP_TYPE"; 
				}else if (paramBox.isEmpty("COMP_NM")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.COMP_NM"; 
				}else if (paramBox.isEmpty("OWN_NM")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.OWN_NM"; 
				}else if (paramBox.isEmpty("POST_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.POST_NO"; 
				}else if (paramBox.isEmpty("ADDR_1")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.ADDR_1"; 
				}else if (paramBox.isEmpty("BIZ_TYPE")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.BIZ_TYPE"; 
				}else if (paramBox.isEmpty("USR_NM")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.USR_NM"; 
				}else if (paramBox.isEmpty("TEL_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.TEL_NO"; 
				}else if (paramBox.isEmpty("MB_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.MB_NO"; 
				}else if (paramBox.isEmpty("EMAIL")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.EMAIL"; 
				}else if (paramBox.isEmpty("CUST_TYPE")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_TYPE"; 
				}else if (paramBox.isEmpty("CUST_NM")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_NM"; 
				}else if (paramBox.isEmpty("CUST_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_NO"; 
				}else if (paramBox.isEmpty("CUST_OWN_NM")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_OWN_NM"; 
				}else if (paramBox.isEmpty("CUST_POST_NO")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_POST_NO"; 
				}else if (paramBox.isEmpty("CUST_ADDR_1")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.CUST_ADDR_1"; 
				}else if (paramBox.isEmpty("DEBT_TYPE")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.DEBT_TYPE"; 
				}else if (paramBox.isEmpty("ST_DEBT_AMT")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.ST_DEBT_AMT"; 
				}else if (paramBox.isEmpty("OVER_ST_DT")) {
					BTF = false; 
					checkType ="DE_DEBT_APPL.OVER_ST_DT"; 
				}
			}else if("DNRF".equals(type)){  // Debt New Request File
				if (paramBox.isEmpty("FILE_SN")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.FILE_SN"; 
				} else if (paramBox.isEmpty("FILE_NM")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.FILE_NM"; 
				} else if (paramBox.isEmpty("FILE_PATH")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.FILE_PATH"; 
				}else if (paramBox.isEmpty("TYPE_CD")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.TYPE_CD"; 
				}
			}else if("DARTXT".equals(type)){  // Debt Amend Request RMK_TXT
				if (paramBox.isEmpty("RMK_TXT")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.RMK_TXT"; 
				} 
			}else if("DARAMT".equals(type)){	// Debt Amend Request DEBT_AMT
				if (paramBox.isEmpty("RMK_TXT")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.RMK_TXT"; 
				} else if (paramBox.isEmpty("DEBT_AMT")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.DEBT_AMT"; 
				} 
			}else if("DARFAB".equals(type)){  // Debt Amend Request FA,FB
				if (paramBox.isEmpty("FILE_NM")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.FILE_NM"; 
				} else if (paramBox.isEmpty("FILE_PATH")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.FILE_PATH"; 
				}  else if (paramBox.isEmpty("RMK_TXT")) {
					BTF = false; 
					checkType ="IF_DEBT_CHG_DATA.RMK_TXT"; 
				} 
			}

			resultBox.set("BTF", BTF);
			resultBox.set("checkType", checkType);
			
			return resultBox;
		}		
		
		
		/**
		 * <pre>
		 * 채무불이행 상태변경 Validation작업 
		 * </pre>
		 * @author JUNG MI KIM
		 * @since 2016. 5. 30.
		 * @version 1.0
		 * @param paramBox
		 * @param modStat
		 * @return
		 */
		@Override
		public SBox ValidationFormForDebtStat(SBox paramBox) {

			SBox resultBox = new SBox();
			boolean BTF = true;
			String currentStat = null;
			String checkValue = null;
			String modStat = null;
			try {
				modStat = paramBox.getString("modStat");
				
				resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtForApplId(paramBox.getString("debtId")) : debtDaoForMssql.selectDebtForApplId(paramBox.getString("debtId"));
				currentStat = resultBox.getString("STAT");
				
				 if("AC".equals(modStat)){ // 접수취소(AC)
					if( !(currentStat.toUpperCase().matches("(?i)AW|EV|AA|AC"))) { //--AW : 접수대기, EV :심사중, AA :결제대기, AC:접수취소
						BTF = false;
						checkValue ="AW : 접수대기, EV :심사중, AA :결제대기, AC:접수취소 상태일 경우만, AC : 접수취소가 가능합니다."; 
					}
				}else if("FA".equals(modStat)){ // 등록전민원발생(FA)
					if( !(currentStat.toUpperCase().matches("(?i)CA"))) { //--CA :등록전민원발생
						BTF = false;
						checkValue ="CA: 등록전민원발생 상태일 경우만, FA: 등록전민원발생 추가증빙이 가능합니다."; 
					}
				}else if("FB".equals(modStat)){ // 등록후민원발생(FB)
					if( !(currentStat.toUpperCase().matches("(?i)CB"))) { //--CA :등록후민원발생
						BTF = false;
						checkValue ="CB: 등록후민원발생 상태일 경우만, FB : 등록후민원발생 추가증빙이 가능합니다."; 
					}
				}else if("RR".equals(modStat)){ // RR :해제요청
					if( (currentStat.toUpperCase().matches("(?i)AW|AC|EV"))) { //--등록대기 이후 모두 가능
						BTF = false;
						checkValue ="등록대기 이후 상태일때, RR: 해제요청이 가능합니다."; 
					}
				}
				 
				resultBox.set("BTF", BTF);
				resultBox.set("checkValue", checkValue);
			
			} catch (BizException e) {
				e.printStackTrace();
				log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31006").getErrMsg() + "]");
				log.e(e.getMessage());
			}
			
			return resultBox;
		}	
		
	/////////////////REQ////////////////////////////////////////////////////////////////////
	/**
	 * <pre>
	 * 조기경보 신규 거래처  등록  I/F ServiceImple 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 1.
	 * @version 1.0
	 * @param paramBox
	 */
	
	@Override
	public SBox setEwCustRequestMessage(SBox paramBox){
		
		SBox resultBox = new SBox();
		SBoxList<SBox> resultList = new SBoxList<SBox>();
		MessageEvent msgEvent = new MessageEvent();
		RequestCommonVo commonVo = new RequestCommonVo();
		EWCustRequestVo requstVo = new EWCustRequestVo();
		RegistorCustomerVo reqCusVO = new RegistorCustomerVo();
		String usrId=null , sbdebnId = null;
		int intSuccessCnt = 0,intFailCnt = 0;
		
	try {
		//Only For API 호출
		if(paramBox.get("LegacyType").equals("API")){ 
			usrId = paramBox.getString(null); 	
			sbdebnId = paramBox.getString("SBId");
			resultList = "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustomerListForBatchId(paramBox.getString("batchId"))
					   									: customerDaoForMssql.selectCustomerListForBatchId(paramBox.getString("batchId"));
			//BATCH_ID VALIDATION.
			if(resultList.size()<=0||resultList.isEmpty()){
				log.i("batchId : "+ paramBox.getString("batchId")+"유효한 배치 아이디가 아닙니다.");
				
				resultBox.set("code", "FAIL");
				resultBox.set("description","batchId : "+ paramBox.getString("batchId")+ " 유효한 배치 아이디가 아닙니다. " );
				resultBox.set("intSuccessCnt", 0);
				resultBox.set("intFailCnt", 1);
				resultBox.set("cntTotal", 1);
				return resultBox;
			}
			
		}else{
			 
			//본사사업자 번호 SenderId로 등록
			reqCusVO.setSenderId(paramBox.getString("sessionUsrNo"));
			reqCusVO.setSenderName(paramBox.getString("sessionCompUsrNm"));
			
			usrId = paramBox.getString("sessionSbusrId"); 	
			sbdebnId = paramBox.getString("sessionSbLoginId");
			
			//1. I/F 보낼  대상 거래처 가져오기
			String[] custNoIfList = paramBox.getString("customerIdList").split(",");
			 int custNoIfListSize = custNoIfList.length;
			 
		
			 for(int i=0; i<custNoIfListSize; i++){
				 SBox inputSBox = new SBox();
				 inputSBox.put("CUST_NO", custNoIfList[i]);
				 paramBox.set("custNo", custNoIfList[i]);
				 paramBox.set("usrNo",paramBox.getString("sessionUsrNo"));
				 
				 resultBox =  "oracle".equals(dbmsType) ? customerDaoForOracle.selectCustInfoForResponseIF(paramBox)
						   : customerDaoForMssql.selectCustInfoForResponseIF(paramBox);

				 inputSBox.put("CUST_ID", resultBox.getString("CUST_ID"));
				 log.i("본사사업자 :" +  paramBox.getString("sessionUsrNo") + "| 거래처 사업자 :" +  inputSBox.getString("custNo") + "| 거래처 순번 :" +  resultBox.getString("CUST_ID"));
				 resultList.add(i,inputSBox);
			 }
		}
		
		//Start For문
		for(SBox sBox : resultList ){
			String instanceId = SysUtil.getInstanceId(serviceCodeRequestEwCustomer,sBox.getString("USR_NO")); 
			String strXml= null;
			//공통 메시지 (요청 문서 정보 영역, 계정 정보 영역)
			if(paramBox.get("LegacyType").equals("API")){
				msgEvent.setSenderId(sBox.getString("USR_NO")); //API사용 업체의 본사사업자번호
				msgEvent.setSenderName(sBox.getString("USR_NM")); //API사용 업체의  본사명
			}else{
				msgEvent.setSenderId(reqCusVO.getSenderId());
				msgEvent.setSenderName(reqCusVO.getSenderName());
			}
			
			msgEvent.setReceiveId(receiveId);
			msgEvent.setReceiveName(receiveName);
			msgEvent.setResponseType(reqCusVO.getResponseType());//ASync타입
			msgEvent.setInstanceId(instanceId);
			msgEvent.setGroupId(sBox.getString("CUST_NO")); //2016.04.18사업자번호자리
			msgEvent.setDocumentType(serviceCodeRequestEwCustomer);
			msgEvent.setDocCreationDateTime(DateUtil.datetime()); //고정값 
			msgEvent.setServiceCode(serviceCodeCred);
			//msgEvent.setTypeCode(reqCusVO.getTypeCode());
			msgEvent.setMessageTypeCode(reqCusVO.getTypeCode());
			
			msgEvent.setFileCount(0); //고정값
			 
			commonVo.setID(instanceId);
			commonVo.setRequestDocumentTypeCode(serviceCodeRequestEwCustomer);
			commonVo.setVersionInformation(reqCusVO.getVersionInformation());
			commonVo.setDescription(reqCusVO.getReqName());
			commonVo.setUserBusinessAccountTypeCode(reqCusVO.getUserBusinessAccountTypeCode());// ERP회원
			commonVo.setUserID(usrId); //스마트채권의 회원식별자 
			commonVo.setLoginID(sbdebnId); //스마트 채권의 아이디 
			commonVo.setLoginPasswordID(null);
			
			requstVo.setId(sBox.getString("CUST_NO"));
			requstVo.setFolderName(sBox.getString("POLDER_NM"));
			
			
			//2. 메시지생성 
			strXml =  CommonMessageIF.makeEwCustRequestMessageForXML(commonVo, requstVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent);
				
			//본사/ 거래처 사업자번호로 거래처 순번을 조회
			SBox dbParamBox = new SBox();
			dbParamBox.set("custId", sBox.getString("CUST_ID"));
			dbParamBox.set("trans", "2"); //처리 성공, 실패 여부. 0: 실패, 1: 성공 , 2: 전송중
			dbParamBox.set("reason", "Progress In Asyn Interface");
			dbParamBox.set("sessionUsrId", "0");
	        SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(dbParamBox)
	        										   : customerDaoForMssql.updateCustResponseForIF(dbParamBox);
				
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				log.e("크레딧서비스 조기경보 대상 거래처 신규등록 인터페이스 시 에러발생  : CUST_ID "+ sBox.getString("CUST_ID") +", REPL_CD ["+ updateBox.getString("REPL_CD") +",REPL_MSG[" + EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg() + "]");
				intFailCnt = intFailCnt + 1;
			}else{
				intSuccessCnt = intSuccessCnt + 1;
			}
			
			}//END FOR문
		
			resultBox.set("intSuccessCnt", intSuccessCnt);
			resultBox.set("intFailCnt", intFailCnt);
			resultBox.set("cntTotal", intFailCnt+intSuccessCnt);
		
			//count세서 return값에 넣어줘야함
		} catch (IOException ioe) {
			ioe.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10599").getErrMsg() +  "]");
			log.e(ioe.getMessage());
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10699").getErrMsg() + "]");
			log.e(e.getMessage());
		}
		return resultBox; 
	}
	
	
	
	/**
	 * <pre>
	 *  채무불이행 신규 등록 (CRQRDA) I/F ServiceImple - CreditRequestRegisterDefaultApplication 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 19.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox setDebtRequestMessage(SBox sBox) {
		SBox resultBox = new SBox();
		SBox debtBox = new SBox();
		SBox validationCheckBox = new SBox();
		String usrId=null, sbdebnId = null;
		String resultCode="30500";
		String resultMessage = null;
		SBox copyAPIfileResultSBox = new SBox();
		String fileCol = "A";
		boolean BTF;
		try {
			
			resultMessage =EErrorCodeType.search(resultCode).getErrMsg(); // 초기 Result Message Fail로 셋팅
					
			String chgStat = "AW"; // AW : 심사대기(Accept Waiting)
			//DateFormat regDtfromFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			DateFormat overStDtfromFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(sBox.get("LegacyType").equals("API")){
				usrId = "00"; 	
				sbdebnId = sBox.getString("SBId");
				
				//API일때, DEBT_APPL_ID만
				debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtForApplId(sBox.getString("debtId")) : debtDaoForMssql.selectDebtForApplId(sBox.getString("debtId"));
				resultBox.set("debtBox", debtBox);
				
				//Validation Form 채무불이행 신규 등록
				validationCheckBox  = ValidationFormForDebt(debtBox,"DNR");//Debt New Request
				BTF = validationCheckBox.get("BTF");
				if (!BTF) {
					log.e("Validation Fail :"+ validationCheckBox.getString("checkType"));
					throw new BizException("30417", EErrorCodeType.search("30417").getErrMsg());
				}
				
				//IF_DEBT_CHG_DATA에서 A대상 파일 찾기
				SBoxList<SBox> aFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectIFDebtChgDataForFile(sBox.getString("tempFileSns")):
																		debtDaoForMssql.selectIFDebtChgDataForFile(sBox.getString("tempFileSns"));
				
				if(aFileList.size()==0){
					log.e("존재하지 않는 파일 시퀀스 / " + sBox.getString("tempFileSns"));
					throw new BizException("70002", EErrorCodeType.search("70002").getErrMsg());
				}
				
				int fileTotalSize = 0;
				for(int i = 0; i< aFileList.size(); i++){
					// 파일 크기 체크 
					fileTotalSize += FileUtil.getFileSize(aFileList.get(i).getString("FILE_PATH")) ;
				}
				// 파일 크기가 50MB 클 경우 에러 발생 
				if(fileTotalSize >52428800 ){
					log.e("전체 파일크기가 너무 큽니다 ( 50MB 미만 ) / " + fileTotalSize);
					throw new BizException("70003", EErrorCodeType.search("70003").getErrMsg());
				}
				
				for(int i = 0; i< aFileList.size(); i++){
					
					//Validation Form 채무불이행 신규등록 첨부파일 
					validationCheckBox  = ValidationFormForDebt(aFileList.get(i),"DNRF");//Debt New Request File
					BTF = validationCheckBox.get("BTF");
					if (!BTF) {
						log.e("Validation Fail :"+ validationCheckBox.getString("checkType"));
						throw new BizException("30418", EErrorCodeType.search("30418").getErrMsg());
					}
					
					aFileList.get(i).set("debtApplSeq", sBox.getString("debtId")); 
					aFileList.get(i).set("REQ_TYPE", "NEW"); 
					copyAPIfileResultSBox = copyAPIFilePathToLegacyFilePath(aFileList.get(i),i,fileCol);
					if("FAIL".equals(copyAPIfileResultSBox.getString("RESULT"))){
						resultBox.set("code", "FAIL");
						resultBox.set("description","채무불이행 신규등록 시 증빙파일 복사 중 오류가 발생하였습니다.");
						throw new Exception(copyAPIfileResultSBox.getString("RESULT")+"파일 저장이 제대로 이루어 지지 않았습니다.");
					}
				}
			
				
				
			}else{
				usrId = sBox.getString("sessionSbusrId"); 	
				sbdebnId = sBox.getString("sessionSbLoginId");
				
				debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox) : debtDaoForMssql.selectDebt(sBox) ;
				debtBox.set("STAT_TXT", EDebtStatType.search(debtBox.getString("STAT")).getText());
				resultBox.set("debtBox", debtBox);
			}

			//증빙 파일 조회 
			SBox paramBox = new SBox();
			sBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));
			paramBox.set("debtApplId", debtBox.get("DEBT_APPL_ID"));
			paramBox.set("debtApplSeq", sBox.get("debtApplId"));
			paramBox.set("fileCol", fileCol); //A: 거래처 증빙자료
			SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(paramBox):
																	debtDaoForMssql.selectDebtFileByCondition(paramBox);
			resultBox.set("prfFileList", prfFileList);
			
			//요청 문서의 instanceId
			String senderId = debtBox.getString("COMP_NO");
			String senderName = debtBox.getString("COMP_NM");
			String instanceId = SysUtil.getInstanceId(serviceCodeCreditRequestApplication,senderId);
					 
			
			//첨부파일
			SBoxList<FileData>  manifestList = new SBoxList<FileData>();
			// 적하목록 정보 영역
			int numberOfItem = prfFileList.size();
				
			for(int i = 0; i< numberOfItem; i++){
				
				FileData manifestItem = new FileData();
				File singleFile = new File(debtFilePath + fileCol+"/" + prfFileList.get(i).getString("FILE_PATH"));
				FileInputStream fis = new FileInputStream(debtFilePath + fileCol+"/" + prfFileList.get(i).getString("FILE_PATH"));
				manifestItem.setFileSeq(String.valueOf(i+1));// 일련번호
				manifestItem.setFileName(prfFileList.get(i).getString("FILE_NM"));// 실제 파일명
				manifestItem.setFileSize(fis.available());// 실제 파일 크기
				manifestItem.setFileData(CommonUtil.singleFileToString(singleFile));//실제 파일 바이너리 정보
				manifestItem.setFileDesc(prfFileList.get(i).getString("TYPE_CD"));//적하 항목 설명
				manifestList.add(manifestItem);
				fis.close();
			}
				 
			//라우팅 VO 생성
				 MessageEvent msgEvent = new MessageEvent();
				 msgEvent.setSenderId(senderId);
				 msgEvent.setSenderName(senderName);
				 msgEvent.setReceiveId(receiveId);
				 msgEvent.setReceiveName(receiveName);
				 msgEvent.setResponseType("A");//ASync타입
				 msgEvent.setInstanceId(instanceId);
				 msgEvent.setGroupId(debtBox.getString("DEBT_APPL_ID"));//채무불이행순번
				 msgEvent.setDocumentType(serviceCodeCreditRequestApplication);//고정값
				 msgEvent.setActionType(chgStat); // AW : 심사대기(Accept Waiting)
				 msgEvent.setDocCreationDateTime(DateUtil.datetime());
				 msgEvent.setServiceCode(serviceCodeCred);//CRED 
				 msgEvent.setMessageTypeCode("REQ");
				 msgEvent.setFileCount(numberOfItem);
				 msgEvent.setFileDatas(manifestList);
		
				//1. 채무불이행 등록 메시지 파라미터 생성
				RequestCommonVo  requestVo = new RequestCommonVo();
				 requestVo.setID(instanceId);
				 requestVo.setRequestDocumentTypeCode(serviceCodeCreditRequestApplication);
				 requestVo.setVersionInformation("1.0.0");
				 requestVo.setDescription("크레딧서비스채무불이행등록요청서");
				 requestVo.setUserBusinessAccountTypeCode("E");// ERP회원
				 requestVo.setUserID(usrId); // sbUserId 스마트채권의 회원식별자
				 requestVo.setLoginID(sbdebnId); // sbdebnId 스마트 채권의 아이디
				 requestVo.setLoginPasswordID(null);
				DebtRegisterRequestVo debtReqisterRequstVo = new DebtRegisterRequestVo();
				
				if(sBox.get("LegacyType").equals("API")){ 
					debtReqisterRequstVo.setCompUsrId("11"); //필수값이라 임의값 생성
					debtReqisterRequstVo.setStDebtAmt(debtBox.getString("ST_DEBT_AMT").trim());
					debtReqisterRequstVo.setRegId("111111"); //필수값이라 임의값 생성
				}else{
					debtReqisterRequstVo.setCompUsrId(sBox.getString("sessionSbCompUsrId"));//sbCompUsrId 스마트 채권의 아이디
					debtReqisterRequstVo.setStDebtAmt(debtBox.getString("ST_DEBT_AMT_IF").trim());
					debtReqisterRequstVo.setRegId(sBox.getString("sessionSbusrId")); //debtBox.getString("REG_ID") 
				}
				//생성일자 날짜포멧 변경
				 String regdateStr = debtBox.getString("REG_DT");
				 
				 //Date regdate = regDtfromFormat.parse(regdateStr); // ORACLE
				 Date regdate = overStDtfromFormat.parse(regdateStr); //MSSQL 
				 debtReqisterRequstVo.setRegDt(CommonUtil.getFormatDateTimeMiliSecond(regdate));//debtBox.getDouble("REG_DT")
				
				 debtReqisterRequstVo.setTypeCode(debtBox.getString("COMP_TYPE"));
				 debtReqisterRequstVo.setCompNm(senderName);
				 debtReqisterRequstVo.setCompNo(senderId);
				 if(!debtBox.getString("CORP_NO").isEmpty()){
					 debtReqisterRequstVo.setCorpNo(debtBox.getString("CORP_NO"));
				 }
				 debtReqisterRequstVo.setBizType(debtBox.getString("BIZ_TYPE"));
				 debtReqisterRequstVo.setOwnName(debtBox.getString("OWN_NM"));
				 debtReqisterRequstVo.setUserNm(debtBox.getString("USR_NM"));
				 debtReqisterRequstVo.setDeptNm(debtBox.getString("DEPT_NM"));
				 debtReqisterRequstVo.setJobTlNm(debtBox.getString("JOB_TL_NM"));
				 debtReqisterRequstVo.setTelNo(debtBox.getString("TEL_NO"));
				 debtReqisterRequstVo.setMbNo(debtBox.getString("MB_NO"));
				 debtReqisterRequstVo.setFaxNo(debtBox.getString("FAX_NO"));
				 debtReqisterRequstVo.setEmail(debtBox.getString("EMAIL"));
				 debtReqisterRequstVo.setPostNo(debtBox.getString("POST_NO"));
				 debtReqisterRequstVo.setAdressOne(debtBox.getString("ADDR_1"));
				 debtReqisterRequstVo.setAdressTwo(debtBox.getString("ADDR_2"));
				 debtReqisterRequstVo.setCustType(debtBox.getString("CUST_TYPE"));
				 debtReqisterRequstVo.setCustNm(debtBox.getString("CUST_NM"));
				 debtReqisterRequstVo.setCustNo(debtBox.getString("CUST_NO"));
				 if(!debtBox.getString("CUST_CORP_NO").isEmpty()){
					 debtReqisterRequstVo.setCustCorpNo(debtBox.getString("CUST_CORP_NO"));
				 }
				 debtReqisterRequstVo.setCustOwnNm(debtBox.getString("CUST_OWN_NM"));
				 debtReqisterRequstVo.setCustPostNo(debtBox.getString("CUST_POST_NO"));
				 debtReqisterRequstVo.setCustAddrOne(debtBox.getString("CUST_ADDR_1"));
				 debtReqisterRequstVo.setCustAddrTwo(debtBox.getString("CUST_ADDR_2"));
				 debtReqisterRequstVo.setDebtType(debtBox.getString("DEBT_TYPE"));
				 debtReqisterRequstVo.setDebtCompType("E");
				//연체개시일자 날짜포멧 변경 
				 String overStdateStr = debtBox.getString("OVER_ST_DT");
				 Date overStdate = overStDtfromFormat.parse(overStdateStr);
					
				 debtReqisterRequstVo.setOverStDt(CommonUtil.getFormatDate(overStdate));//debtBox.getDouble("OVER_ST_DT")연체개시일자(yyyyMMdd)
				 
				//2. XML메시지 생성
				 String strXml = null;
				 strXml = CommonMessageIF.makeCreditRequestRegisterDefaultApplicationForXML(requestVo, debtReqisterRequstVo ); 
				 msgEvent.setDocumentData(strXml);
				 wsClient.remoteASyncMessageCall(msgEvent);
				
				
				SBox sendResultSBox = new SBox();
				sendResultSBox.set("debtApplId", debtBox.getString("DEBT_APPL_ID")); //GroupId
				sendResultSBox.set("chgStat", chgStat); //ActionTyp
				sendResultSBox.set("rmkTxt", null);
				sendResultSBox.set("trans", "2"); //0:실패/ 1:성공/ 2:전송중
				sendResultSBox.set("rcode",null);
				sendResultSBox.set("reason","인터페이스 전송중 상태입니다.");
				sendResultSBox.set("sessionUsrNm", "SYSTEM");
				sendResultSBox.set("sessionUsrId", "0");
				sendResultSBox.set("docYn", "N");
				sendResultSBox.set("docCd", null);
				sendResultSBox.set("debtStatSubCd", null);
	        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(sendResultSBox):
	        										 		debtDaoForMssql.updateDebtStatSendEndForWS(sendResultSBox);
				String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
				
				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					//throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
					resultMessage =EErrorCodeType.search(resultCode).getErrMsg();
					resultCode = updateBox.getString("REPL_CD");
					log.e("Unknown Exception : debtStatHistId ["+debtStatHistId+":" + resultCode+","+resultMessage+ "]");
				}
				
				resultCode = updateBox.getString("REPL_CD");
				resultMessage =  EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg();
				resultBox.set("code", resultCode);
				resultBox.set("description", resultMessage);
				 
		} catch (BizException biz) {
			biz.printStackTrace();
			log.e("BizException Exception[" + biz.getErrMsg() + "]");
			resultBox.set("code", "FAIL");
			resultBox.set("description",biz.getErrMsg());
			log.e(biz.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("30500").getErrMsg() + "]");
			log.e(e.getMessage());
			resultBox.set("code", "FAIL");
			resultBox.set("description", e.getMessage());
		}

		return resultBox;
	}
	
	
	
	/**
	 * <pre>
	 * 크레딧서비스채무불이행 등록 변경요청서(CRQAMD)  I/F ServiceImple - CreditRequestAmendDefault TODO: 테스트 진행 하면서 파라미터 체크 API
	 * 채무불이행신청서정보의 채무불이행금액, 상태 등의 수정을 요청하는 메시지
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 1.
	 * @version 1.0
	 * @param sBox
	 */
	@Override
	public SBox setDebtAmendRequest(SBox sBox){
		String debtStatHistId = null;
		SBox resultBox = new SBox();
		String usrId=null, sbdebnId = null;
		String resultCode="31002";
		String resultMessage = null;
		SBox debtbasicInfoSBox = new SBox();
		String fileCol = "B";
		
		SBoxList<FileData>  manifestList = new SBoxList<FileData>(); 
		int numberOfItem = 0;
		
		try {
			
			String	modStat = sBox.getString("MOD_STAT");
			resultMessage =EErrorCodeType.search(resultCode).getErrMsg(); // 초기 Result Message Fail로 셋팅
			
			if(sBox.get("LegacyType").equals("API")){
				usrId = "00"; 	
				sbdebnId = sBox.getString("SBId");
				debtbasicInfoSBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtForApplId(sBox.getString("debtId")) : debtDaoForMssql.selectDebtForApplId(sBox.getString("debtId"));
	
			
			}else{
				usrId = sBox.getString("sessionSbusrId"); 	
				sbdebnId = sBox.getString("sessionSbLoginId");
				
				debtbasicInfoSBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(sBox):debtDaoForMssql.selectDebt(sBox); 
				
				debtbasicInfoSBox.set("STAT_TXT", EDebtStatType.search(debtbasicInfoSBox.getString("STAT")).getText());
			}

			
			//전송대상 첨부파일 리스트 불러오기
			//sBox.set("fileCol","A"); // 거래증빙자료 FILE 정보 GET
			if("FA".equals(modStat)||"FB".equals(modStat)){
				// 적하목록 정보 영역
				sBox.set("fileCol",fileCol);
				SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebtFileByCondition(sBox):
																		debtDaoForMssql.selectDebtFileByCondition(sBox);
				numberOfItem = prfFileList.size();
				
				for(int i = 0; i< numberOfItem; i++){
					FileData manifestItem = new FileData();
					File singleFile = new File(debtFilePath + fileCol+"/" + prfFileList.get(i).getString("FILE_PATH"));
					FileInputStream fis = new FileInputStream(debtFilePath + fileCol+"/" + prfFileList.get(i).getString("FILE_PATH"));
					manifestItem.setFileSeq(String.valueOf(i+1));// 일련번호
					manifestItem.setFileName(prfFileList.get(i).getString("FILE_NM"));// 실제 파일명
					manifestItem.setFileSize(fis.available());// 실제 파일 크기
					manifestItem.setFileData(CommonUtil.singleFileToString(singleFile));//실제 파일 바이너리 정보
					manifestItem.setFileDesc(prfFileList.get(i).getString("TYPE_CD"));//적하 항목 설명
					manifestList.add(manifestItem);
					fis.close();
				}
			}
			
			 String senderId = debtbasicInfoSBox.getString("COMP_NO");
			 String senderName = debtbasicInfoSBox.getString("COMP_NM");
			 String instanceId = SysUtil.getInstanceId(serviceCodeCreditRequestAmend,senderId);
			
			 
		//라우팅 VO 생성
			 MessageEvent msgEvent = new MessageEvent();
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//ASync타입
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setGroupId(sBox.getString("debtApplSeq"));//sBox.getString("DEBT_APPL_ID"));//채무불이행순번
			 msgEvent.setDocumentType(serviceCodeCreditRequestAmend);
			 msgEvent.setActionType(modStat);
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED
			 msgEvent.setMessageTypeCode("REQ");
			 msgEvent.setFileCount(numberOfItem);
			 msgEvent.setFileDatas(manifestList);
			
			
			//1. 채무불이행 정정  메시지 파라미터 생성
			RequestCommonVo  commonVo = new RequestCommonVo();
			 commonVo.setID("크레딧서비스채무불이행등록변경요청서");
			 commonVo.setRequestDocumentTypeCode(serviceCodeCreditRequestAmend);
			 commonVo.setVersionInformation("1.0.0");
			 commonVo.setDescription(null);
			 commonVo.setUserBusinessAccountTypeCode("E");// ERP회원
			 commonVo.setUserID(usrId); //usrId 스마트채권의 회원식별자
			 commonVo.setLoginID(sbdebnId); //sbdebnId 스마트 채권의 아이디 
			 commonVo.setLoginPasswordID(null);
		
		
			DebtAmendRequestVo amendVo = new DebtAmendRequestVo();
			 amendVo.setDocCd(debtbasicInfoSBox.getString("DOC_CD"));// DE_DEBT_APPL.DOC_CD
			 amendVo.setUpdDt(DateUtil.datetime());//DE_DEBT_APPL.UPD_DT
			 amendVo.setUpdId(usrId); //DE_DEBT_APPL.UPD_ID
			 amendVo.setModStat(modStat); //DE_DEBT_MOD_HIST.STAT 
			 amendVo.setUsrNm(debtbasicInfoSBox.getString("USR_NM"));// 필수값아니라 null표기 DE_DEBT_STAT_HIST.USR_NM
			 amendVo.setRmkTxt(sBox.getString("RMK_TXT")); //DE_DEBT_STAT_HIST.RMK_TXT 
	        //채무불이행금액 변경
			 if("R".equals(modStat)){
				 sBox.set("debtAmtIf", !"0".equals(sBox.getString("debtAmt").replaceAll(",", "")) ? (sBox.getString("debtAmt").replaceAll(",", "")) : sBox.get("debtAmt"));
				 amendVo.setDebtAmt(sBox.getString("debtAmtIf").trim()); //DE_DEBT_MOD_HIST.DEBT_AMT
			 }
			 
			 String strXml = null;
			 strXml = CommonMessageIF.makeCreditRequestAmendDefaultForXML(commonVo,amendVo ); 
			 msgEvent.setDocumentData(strXml);
			 wsClient.remoteASyncMessageCall(msgEvent);
			 
			 
			 SBox snedResultSBox = new SBox();
			 snedResultSBox.set("debtApplId", sBox.getString("debtApplSeq")); //GroupId
			 snedResultSBox.set("chgStat", modStat); //ActionTyp I/F시  actionType이 null이 들어옴 문제해결해야함
			 
			 if("FA".equals(modStat) || "FB".equals(modStat)){
				 snedResultSBox.set("rmkTxt", sBox.getString("RMK_TXT"));
			 }else{
				 snedResultSBox.set("rmkTxt", null);
			 }
			 
			 if("RR".equals(modStat)||"AC".equals(modStat)){
				 snedResultSBox.set("debtStatSubCd", sBox.get("debtStatSubCd"));
			 }else{
				 snedResultSBox.set("debtStatSubCd", null);
			 }
			 
			 //IF전송을 위한 채무불이행 테이블  상태변경 및 히스톨 삽입
			if(!("R".equals(modStat))){
				snedResultSBox.set("trans", "2"); //0:실패/ 1:성공/ 2:전송중
				snedResultSBox.set("rcode",null);
				snedResultSBox.set("reason","인터페이스 전송중 상태입니다.");
				snedResultSBox.set("sessionUsrNm", "SYSTEM");
				snedResultSBox.set("sessionUsrId", "0");
				snedResultSBox.set("docYn", "N");
				snedResultSBox.set("docCd", null);
	        	 SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(snedResultSBox):
	        		 										 debtDaoForMssql.updateDebtStatSendEndForWS(snedResultSBox);
				 debtStatHistId = updateBox.getString("DEBT_STAT_HIST_ID");
				
				if (!"00000".equals(updateBox.getString("REPL_CD"))) {
					//throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
					resultMessage =EErrorCodeType.search(resultCode).getErrMsg();
					resultCode = updateBox.getString("REPL_CD");
					log.e("Unknown Exception : debtStatHistId ["+debtStatHistId+":" + resultCode+","+resultMessage+ "]");
				}
				
				resultCode = updateBox.getString("REPL_CD");
				resultMessage =  EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg();
			}else{
				if(sBox.get("LegacyType").equals("API") && "R".equals(modStat)){
					//이전 프로세스에서 modHistInsert시 저장된 result값을 넣어준
					resultMessage = sBox.getString("resultMessage");
					resultCode = sBox.getString("resultCode");
				}
			}
			
			resultBox.set("debtStatHistId",debtStatHistId);
			resultBox.set("code", resultCode);
			resultBox.set("description", resultMessage);
			 
		} catch (IOException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31002").getErrMsg() + "]");
			log.e(e.getMessage());
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31002").getErrMsg() + "]");
			log.e(e.getMessage());
		}
		return resultBox; 
	}
	

	
	
	
	/**
	 * <pre>
	 * 채무불이행 등록 변경  I/F 채무불이행 변경시 상태별 분기 프로세스
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 4.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox processMakeDebtAmendRequestMessageAPI(SBox sBox){
		
		
		SBox paramBox = new SBox();
		SBox ifChgData = new SBox();
		SBox resultSBox = new SBox();
		SBox statusChgAmtResult = new SBox();
		SBox moveAPIfileResultSBox = new SBox();
		SBox validationCheckBox = new SBox();
		String debtStatHistId = null;
		String fileCol = "B";
		boolean BTF=false;
		try{
			
			String debtId = sBox.getString("debtId");
			String SBId = sBox.getString("SBId");
			String modStat = sBox.getString("modStat");
			String tempFileSns = sBox.getString("tempFileSns");
			String resultCode = "31005";
			String resultMessage = EErrorCodeType.search("31005").getErrMsg();
			
			//한개인지 구분해보기
			String[] searchYn =  tempFileSns.split(",");
			if(searchYn.length == 1){
				ifChgData = debtDaoForMssql.selectIFDebtChgData(tempFileSns);
				
				if("AC".equals(modStat) || "RR".equals(modStat)){ // 신청취소(AC) : CancelDebtApply,  //채무불이행 해제요청(RR) : releaseDebtRequire 
					
					//Validation Form 상태 변경  신청취소, 채무불이행 해제요청시
					validationCheckBox  = ValidationFormForDebt(ifChgData,"DARTXT"); // Debt Amend Request RMK_TXT
					BTF = validationCheckBox.get("BTF");
					if (!BTF) {
						log.e("Validation Fail :"+ validationCheckBox.getString("checkType"));
						
						resultSBox.set("code","FAIL");
						resultSBox.set("description",validationCheckBox.getString("checkType")+" 필수값이 존재하지 않습니다.");
						return resultSBox;
					}
					
					paramBox.set("RMK_TXT", ifChgData.get("RMK_TXT"));
					
				}else if("R".equals(modStat)){ //채무불이행 금액 정정(R) : modifyDebtRequire  
					
					//Validation Form 상태 변경  채무불이행 정정금액 
					validationCheckBox  = ValidationFormForDebt(ifChgData,"DARAMT"); // Debt Amend Request DEBT_AMT
					BTF = validationCheckBox.get("BTF");
					if (!BTF) {
						log.e("Validation Fail :"+ validationCheckBox.getString("checkType"));
						
						resultSBox.set("code","FAIL");
						resultSBox.set("description",validationCheckBox.getString("checkType")+" 필수값이 존재하지 않습니다.");
						return resultSBox;
						
					}
					
					paramBox.set("RMK_TXT", ifChgData.get("RMK_TXT"));
					paramBox.set("rmkTxt", ifChgData.get("RMK_TXT"));
					paramBox.set("MOD_STAT", modStat);
					paramBox.set("modStat", modStat);
					paramBox.set("debtAmt", ifChgData.get("DEBT_AMT"));
					paramBox.set("debtApplId", debtId);
					paramBox.set("debtApplSeq", debtId);
					paramBox.set("sessionUsrId","0");
					statusChgAmtResult = modifyDebtRequireForAPI(paramBox);
					resultCode = statusChgAmtResult.getString("REPL_CD");
					resultMessage = statusChgAmtResult.getString("REPL_MSG");
					paramBox.set("resultCode",resultCode);
					paramBox.set("resultMessage",resultMessage);
					
					if(!"00000".equals(statusChgAmtResult.getString("REPL_CD"))){
						resultCode = statusChgAmtResult.getString("REPL_CD");
						resultMessage = statusChgAmtResult.getString("REPL_MSG");
						paramBox.set("resultCode",resultCode);
						paramBox.set("resultMessage",resultMessage);
						throw new Exception("채무불이행 순번 :" + paramBox.getString("debtApplId") +", REPL_CD : " + statusChgAmtResult.getString("REPL_CD") + ", REPL_MSG" + statusChgAmtResult.getString("REPL_MSG"));
					}
				}
			}else{
				if(!("FA".equals(modStat)||"FB".equals(modStat))){
					//등록전 민원발생(추가증빙) FA, 등록 후 민원발생(추가증빙)FB 을 제외한 상태변경에서는 한개의 첨부파일이 존재해야 합니다.
					resultSBox.set("code","30420");
					resultSBox.set("description", EErrorCodeType.search("30420").getErrMsg());
					return resultSBox;
					
				}
			}
			
			
			if("FA".equals(modStat)||"FB".equals(modStat)){
				// 적하목록 정보 영역
				paramBox.set("fileCol",fileCol);
				SBoxList<SBox> prfFileList = "oracle".equals(dbmsType)? debtDaoForOracle.selectIFDebtChgDataForFile(tempFileSns):
																		debtDaoForMssql.selectIFDebtChgDataForFile(tempFileSns);
				//Validation Form 채무불이행 상태변경 FA,FB일경우
				
				int fileTotalSize = 0;
				for(SBox validationBox : prfFileList ){
					validationCheckBox  = ValidationFormForDebt(validationBox,"DARFAB"); // Debt Amend Request FA,FB
					BTF = validationCheckBox.get("BTF");
					if (!BTF) {
						log.e("Validation Fail :"+ validationCheckBox.getString("checkType"));
						
						resultSBox.set("code","FAIL");
						resultSBox.set("description",validationCheckBox.getString("checkType")+" 필수값이 존재하지 않습니다.");
						return resultSBox;
					}
					fileTotalSize += FileUtil.getFileSize(validationCheckBox.getString("FILE_PATH"));
				}
				
				if(fileTotalSize > 52428800) {
					log.d("Total File size : " + fileTotalSize);
					throw new BizException("70003", EErrorCodeType.search("70003").getErrMsg());

				}
				
			 	SBox tBox = new SBox();
				tBox.set("debtApplId",debtId);
				tBox.set("sessionUsrNm","SYSTEM");
				tBox.set("sessionUsrId","0");
				tBox.set("debtStatCd","TM");		
				tBox.set("rmkTxt", prfFileList.get(0).getString("RMK_TXT")); //똑같은 값의 RMK_TXT를 업체에서 3개의 첨부파일을 넣는다면 동일한사유를 똑같이 3개의 파일에 매핑해서 넣어주기로 룰을 정함 따라서 그중에 하나를 읽어와서 넘기기로 정한다.
				paramBox.set("RMK_TXT", prfFileList.get(0).getString("RMK_TXT"));
				// 임시 테이블로 상태값 저장 									
				SBox sBoxHist = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtStatHist(tBox):debtDaoForMssql.insertDebtStatHist(tBox);
				debtStatHistId = sBoxHist.getString("DEBT_STAT_HIST_ID");
				for(int i = 0; i< prfFileList.size(); i++){
				prfFileList.get(i).set("debtApplSeq", debtId); 
				prfFileList.get(i).set("debtStatHistId", debtStatHistId);
				paramBox.setIfEmpty("debtStatHistId", debtStatHistId); //SOAP메시지 보낼때 이전 해당 변경 아이디만 보내야함.
				prfFileList.get(i).set("REQ_TYPE", "AMEND"); 
				moveAPIfileResultSBox = copyAPIFilePathToLegacyFilePath(prfFileList.get(i),i,fileCol);
					if("FAIL".equals(moveAPIfileResultSBox.getString("RESULT"))){
						resultSBox.set("resultCode","30421");
						resultSBox.set("resultMessage", EErrorCodeType.search("30421").getErrMsg());//"채무불이행  상태이력 증빙파일 디렉토리 생성중 오류가 발생하였습니다.");
						throw new Exception(moveAPIfileResultSBox.getString("RESULT")+"파일 저장이 제대로 이루어 지지 않았습니다.");
					}
				}
					
			}
			
			paramBox.set("tempFileSns",tempFileSns);
			paramBox.set("LegacyType", "API");
			paramBox.set("debtId",debtId);
			paramBox.set("debtApplId", debtId);
			paramBox.set("debtApplSeq",debtId);
			paramBox.set("SBId",SBId);
			paramBox.set("MOD_STAT",modStat);
			paramBox.set("modStat",modStat);
			paramBox.set("debtStatSubCd",null); 
			resultSBox = setDebtAmendRequest(paramBox); //CommonAmendRequest 기존 공통
			
			
			if(debtStatHistId!=null &&("FA".equals(modStat)||"FB".equals(modStat))){
				String ch_debtStatHistId = resultSBox.getString("debtStatHistId");
				System.out.println(debtStatHistId + "/ " + ch_debtStatHistId);
				// [9] temp값으로 넣었던 debt_stat_hist 테이블에 부모 값을 setting 해줌 
				SBox updateParentDebtStat = "oracle".equals(dbmsType)? debtDaoForOracle.updateParentDebtStatForTemp(debtStatHistId, ch_debtStatHistId):
					   debtDaoForMssql.updateParentDebtStatForTemp(debtStatHistId,ch_debtStatHistId);
				
				log.d("채무불이행 temp 파일 등록 성공 " + updateParentDebtStat.getString("REPL_CD"));
			}
			
			
		
		} catch (BizException biz) {
			log.e(biz.getMessage());
			resultSBox.set("resultCode", biz.getErrCode());
			resultSBox.set("resultMessage", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("call makeDebtAmendRequestMessage of MessageController Unknown Exception ");
			log.e(ex.getMessage());
			resultSBox.set("resultCode","30421");
			resultSBox.set("resultMessage",EErrorCodeType.search("30421").getErrMsg());
		}
		return resultSBox;
	}
	
	
	
	
	public SBox modifyDebtRequireForAPI(SBox sBox) {

		SBox resultBox = new SBox();
		String replCd = null;
		String replMsg = null;
		try {
			// [1] 채무불이행 정정 금액, 정정 상태 수정
			resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtApplForRequireModify(sBox):
												   debtDaoForMssql.updateDebtApplForRequireModify(sBox);

			replCd = resultBox.getString("REPL_CD");
			replMsg = EErrorCodeType.search(replCd).getErrMsg();
			if (!"00000".equals(replCd)) {
				log.e("채무불이행 순번 :" + sBox.getString("debtApplId") + ", 에러코드 : " + replCd + ", 에러메시지" + replMsg);
				resultBox.set("REPL_CD", replCd);
				resultBox.set("REPL_MSG", replMsg);
			}
			log.i("채무불이행 정정 요청 금액 수정 응답 결과 : REPL_CD[" + replCd + ", REPL_MSG[" + EErrorCodeType.search(replCd).getErrMsg() + "]");

			sBox.set("trans", "2"); //2:저장완료
			sBox.set("rcode",null);
			sBox.set("reason","인터페이스 전송중 상태입니다.");
			
			// [2] 채무불이행 정정 요청 이력 정보 추가
			resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtModHist(sBox):debtDaoForMssql.insertDebtModHist(sBox);
			replCd = resultBox.getString("REPL_CD");
			replMsg = EErrorCodeType.search(replCd).getErrMsg();
			
			if (!"00000".equals(replCd)) {
				log.e("채무불이행 순번 :" + sBox.getString("debtApplId") +"에러코드 : " + replCd + "에러메시지" + replMsg);
				resultBox.set("REPL_CD", replCd);
				resultBox.set("REPL_MSG", replMsg);
			}
			
			log.i("채무불이행 정정 요청 이력 추가 응답 결과 : REPL_CD[" + replCd + ", REPL_MSG[" + replMsg + "]");
			resultBox.set("REPL_CD", replCd);
			resultBox.set("REPL_MSG", replMsg);
			
            
		} catch (BizException biz) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultBox.set("REPL_CD", "31001");
			resultBox.set("REPL_MSG", EErrorCodeType.search("31001").getErrMsg());
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("31001").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	
	
	/**
	 * <pre>
	 * 채무불이행 민원발생 추가 증빙 시 API 첨부파일  Legacy Path 룰에 맞게 옮김 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 5. 3.
	 * @version 1.0
	 * @param fileBox
	 * @return
	 */
	@Transactional
	public SBox copyAPIFilePathToLegacyFilePath(SBox fileBox, int i, String fileCol) {
		SBox result = new SBox();
		try{
			String debtApplSeq = fileBox.getString("debtApplSeq");
	        String fileName = fileBox.getString("FILE_NM");//"ConnectorInfo.txt"; //바뀔 이름
	        String tempAPIFilePath = fileBox.getString("FILE_PATH");//API업체와 FUll Path넣기로 정함
	        String realFilePath = debtFilePath + fileCol; //Legacy Path
	        String path = realFilePath+"/"+debtApplSeq;
	        String filePath = null; //path+"/"+fileName; 
	        String reqType = fileBox.getString("REQ_TYPE");
	        
	        if (!fileName.isEmpty()) {
				String[] fileNm = CommonUtil.separateFileName(fileName);
				filePath = debtApplSeq + "/" + fileNm[0] + "_" + CommonUtil.getTimeMiliSecond() + "_" + (i+1) + "." + fileNm[1] ; // "_" + (i+1) +
			
				// [4] 채무불이행 상태 이력 증빙파일 디렉토리 생성
				File debtStatDir = new File(realFilePath);
				if (!debtStatDir.isDirectory()) {
					debtStatDir.mkdir();
				}
				
				File debtFileDir = new File(path);
				if (!debtFileDir.isDirectory()) {
					if(debtFileDir.mkdir()) {
						log.i("[API] 채무불이행  상태이력 증빙파일 디렉토리 생성 From API [ 경로 : "+ path +" ]");
					}
				}
				
			     File checkFileYN = new File(realFilePath+"/"+filePath);
			     //log.i(checkFileYN.isFile()+"");
			     if(!checkFileYN.isFile()){
			    	 boolean copyYn = CommonUtil.fileCopy(tempAPIFilePath,realFilePath+"/"+filePath );
			    	 if(true == copyYn){
				    	 result.set("RESULT", "SUCCESS");
				    	
							// 파일 업로드 등록
							String debtStatHistId = fileBox.getString("debtStatHistId");
							// 추가증빙파일 저장
							fileBox.set("debtStatHistId", debtStatHistId.trim().length()==0?null:debtStatHistId);
							fileBox.set("debtApplSeq", debtApplSeq);
							fileBox.set("fileCol",fileCol);
							fileBox.set("ch_fileNm",fileName);
							fileBox.set("filePath",filePath);
							if("NEW".equals(reqType)){ //채무불이행 신규등록일 경우 구분자가 필요하다  (증빙서류코드,A: 거래명세서,B: 세금계산서,C: 법원판결문,D: 계약서,E: 기타 증빙자료)
								fileBox.set("typeCd",fileBox.getString("TYPE_CD"));
							}
							SBox fileInsertResult = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtStatFile(fileBox):debtDaoForMssql.insertDebtStatFile(fileBox);
							if (!"00000".equals(fileInsertResult.getString("REPL_CD"))) {
								log.e("[API]채무불이행 진행 중 추가 증빙 서류 첨부  INSERT 결과코드 : " + fileInsertResult.getString("REPL_CD") + " / 파일명 : " + fileBox.getString("fileNm") + " / 변경 파일명 : " + fileBox.getString("filePath"));
							}

							log.i("[API]채무불이행 진행 중 추가 증빙 서류 첨부  INSERT 결과코드 : " + fileInsertResult.getString("REPL_CD") + " / 파일명 : " + fileBox.getString("fileNm") + " / 변경 파일명 : " + fileBox.getString("filePath"));
							result.setIfEmpty("REPL_CD", fileInsertResult.getString("REPL_CD"));
				    	 
				     }else{
				    	 result.set("RESULT", "FAIL"); 
				    	 log.e("채무불이행  상태이력 증빙파일 디렉토리 생성중 오류 : [API 파일경로: "+ tempAPIFilePath+", Legacy 파일 Move 경로: "+realFilePath + "]");
				     }
			     }else{
			    	 log.i("채무불이행  상태이력 증빙파일이 이미 Legacy에 존재합니다. : [API 파일경로: "+ tempAPIFilePath+", Legacy 파일 Move 경로: "+realFilePath + "]");
			     }
	        }else{
	        		log.e("채무불이행  상태이력 에러 : [debtApplSeq: "+ debtApplSeq+", 첨부파일 파일명이 존재하지 않습니다.]");
	        		throw new Exception("채무불이행  상태이력 에러 : [debtApplSeq: "+ debtApplSeq+", 첨부파일 파일명이 존재하지 않습니다.]");
	        }
			
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31000").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
		     
		return result;
	}
	
	
	
	
	/////////////////RES////////////////////////////////////////////////////////////////////
	

//RESPONSE MESSAGE START/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 
	 * <pre>
	 * 처리결과통보서(NTCCON) 조기경보 대상 거래처 등록, 채무불이행 요청에 대한 응답 메시지  (비동기) - NoticeConclusion
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 20.
	 * @version 1.0
	 * @param messageTag
	 * @return
	 */
	@Override
	@Transactional
	public SBox getIFResultForNoticeConclusionType(MessageTag messageTag) {
		SBox xmlResult = null;
		SBox paramBox = new SBox();
		SBox resultBox = new SBox();
		try{
			xmlResult = CommonMessageIF.getNoticeConclusionForXML(messageTag.getDocumentData());
            
			if(messageTag.getReferenceId()!=null){
		// 조기경보 대상 거래처 등록  IF
            	if(messageTag.getReferenceId().substring(0, 6).equals("CRQREC")){ // groupId가 존재하지 않고, ReferenceId가 조기경보대상 거래처등록이면
	                
	                xmlResult.set("custNo", messageTag.getGroupId()); 
	                xmlResult.set("rcode",xmlResult.get("typeCode"));//0:실패/ 1:성공
	            	xmlResult.set("reason",xmlResult.get("description"));		
	            	
	            	//본사/ 거래처 사업자번호로 거래처 순번을 조회
	            	paramBox.set("custNo", messageTag.getGroupId());
	            	paramBox.set("usrNo", messageTag.getReceiveId());
	            	resultBox = customerDaoForMssql.selectCustInfoForResponseIF(paramBox);
	            	
	            	log.i("본사사업자 :" +  xmlResult.getString("receiveId") + "| 거래처 사업자 :" +  xmlResult.getString("custNo") + "| 거래처 순번 :" +  resultBox.getString("CUST_ID"));
	            	
            		paramBox.set("custId",resultBox.getString("CUST_ID"));
	    			paramBox.set("trans", xmlResult.getString("rcode")); //처리 성공  실패 여부. [0: 실패, 1: 성공 , 2: 전송중]
	            	paramBox.set("reason", xmlResult.getString("reason"));
	            	paramBox.set("sessionUsrId", "0");
	            	SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(paramBox)
	            											   : customerDaoForMssql.updateCustResponseForIF(paramBox);
	    			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
	    				throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
	    			}
	    // 채무불이행 상태변경 IF		
            	}else{ 
                	 paramBox.set("debtApplId", messageTag.getGroupId()); //GroupId
                	 
	   				 if(!("0".equals(xmlResult.get("typeCode")))){ //IF성공
	   					paramBox.set("chgStat", messageTag.getActionType());
	   				}
   				 
   		        	paramBox.set("trans", xmlResult.get("typeCode")); //0:실패/ 1:성공
   		        	paramBox.set("rcode",xmlResult.get("responseTypeCode"));
   		        	paramBox.set("reason",xmlResult.get("description"));
   		        	paramBox.set("sessionUsrNm", "SYSTEM");
   		        	paramBox.set("sessionUsrId", "0");
   		        	paramBox.set("docYn", "N");
   				 
   				if("R".equals(messageTag.getActionType())){
   					
   					SBox sdebtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(paramBox):
   															  debtDaoForMssql.selectDebt(paramBox);
   					paramBox.set("modStat", messageTag.getActionType());
   					paramBox.set("debtAmt", !"0".equals(sdebtBox.getString("DEBT_AMT").replaceAll(",", "")) ? (sdebtBox.getString("DEBT_AMT").replaceAll(",", "")) : sdebtBox.get("DEBT_AMT"));
   					
   					SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtModHistForIF(paramBox):debtDaoForMssql.updateDebtModHistForIF(paramBox);
   					
   					
   					if (!"00000".equals(updateBox.getString("REPL_CD"))) {
   						throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
   					}
   					log.e("채무불이행 정정 요청 이력 추가 응답 결과 : REPL_CD[" + updateBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg() + "]");

   				}else{
   					
   		        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox):
   		        												debtDaoForMssql.updateDebtStatSendEndForWS(paramBox);
   					String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
   					
   					if (!"00000".equals(updateBox.getString("REPL_CD"))) {
   						log.e("debtStatHistId:"+debtStatHistId+","+ updateBox.getString("REPL_CD")+ EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
   					}
   					
   					// 파일 연결을 위하여 debt_stat_hist_f_id 추가 
   					SBox FileBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId):
							debtDaoForMssql.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId);
   					log.d("파일 연결을 위하여 debt_stat_hist_f_id 추가 ");
   					if (!"00000".equals(FileBox.getString("REPL_CD"))) {
   						throw new BizException("debtStatHistId:"+debtStatHistId+", "+FileBox.getString("REPL_CD"), EErrorCodeType.search(FileBox.getString("REPL_CD")).getErrMsg());
   					}
   				}
               }
        	}
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31000").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
		return resultBox;
	}
	
	
	
	/**
	 * 
	 * <pre>
	 * 크레딧서비스 채무불이행 등록응답서 (CRSRDA)  - CreditResponseRegisterDefaultApplication
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public void getDebtReqisterIFResultForSucessResponse(MessageTag messageTag){
		try{
	        
	        SBox xmlResult = CommonMessageIF.getCreditResponseRegisterDefaultApplicationForXML(messageTag.getDocumentData());
	        SBox paramBox = new SBox();
			paramBox.set("debtApplId", messageTag.getGroupId()); //GroupId
	        if("1".equals(xmlResult.getString("typeCode"))){ //0:실패 | 1: 성공일경우 
	        	paramBox.set("chgStat", "AW"); //messageTag.getActionType()
	        }else{
	        	log.e("채무불이행 상태변경 신청시 IF가 정상적으로 이루어지지 않았습니다 : CreditResponseRegisterDefaultApplication");
	        }
	    	paramBox.set("rmkTxt", null);
	    	paramBox.set("trans", xmlResult.getString("typeCode")); //2:저장완료
	    	paramBox.set("rcode", xmlResult.getString("responseTypeCode"));
	    	paramBox.set("reason",xmlResult.getString("description"));
	    	paramBox.set("docCd", xmlResult.getString("id"));
	    	paramBox.set("docYn", "Y");
	    	paramBox.set("sessionUsrNm", "SYSTEM");
	    	paramBox.set("sessionUsrId", "0");
	    	paramBox.set("debtStatSubCd", null);
	    	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox):
	    												debtDaoForMssql.updateDebtStatSendEndForWS(paramBox);
			String debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				throw new BizException("debtStatHistId:"+debtStatHistId+", "+updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
			}
		
			// 파일 연결을 위하여 debt_stat_hist_f_id 추가 
			SBox FileBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId):
				debtDaoForMssql.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId);
			log.d("파일 연결을 위하여 debt_stat_hist_f_id 추가 ");
			if (!"00000".equals(FileBox.getString("REPL_CD"))) {
				throw new BizException("debtStatHistId:"+debtStatHistId+", "+FileBox.getString("REPL_CD"), EErrorCodeType.search(FileBox.getString("REPL_CD")).getErrMsg());
			}
				
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31000").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
	}

	
	
	
	/**
	 * 
	 * <pre>
	 * 크레딧서비스 채무불이행 등록 상태 변경 통지서(CNTDUS)	- CreditNoticeDefaultUpdateStatus
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 5. 12.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public String getDebtStatusIFUpdate(MessageTag messageTag){
		String debtStatHistId ="";
		try{
			
	        SBox paramBox = new SBox();
			SBox xmlResult = CommonMessageIF.getCreditUpdateStatusForXml(messageTag.getDocumentData());
			String trans = "1";
			String rcode = "SCMN01";
			String reason = "정상 처리됨";

	        paramBox.set("debtApplId", messageTag.getGroupId());//xmlResult.getString("id"));//sBox.get("DEBT_APPL_ID"));
	        paramBox.set("chgStat", xmlResult.getString("typeCode"));//updateStatus.getDefaultUpdateStatusNoticeDocument().getTypeCode());
	    	paramBox.set("rmkTxt",  xmlResult.getString("description"));
	    	paramBox.set("docCd", null);
	    	paramBox.set("docYn", "N");
	    	paramBox.set("sessionUsrNm", "SYSTEM");
	    	paramBox.set("sessionUsrId", "0");
	    	paramBox.set("debtStatSubCd", null);
	    	
	    	SBox debtBox = "oracle".equals(dbmsType)? debtDaoForOracle.selectDebt(paramBox):debtDaoForMssql.selectDebt(paramBox);
	    	SBox resultBox = new SBox();
	    	
	    	if(!(debtBox == null)){
	    		//채무불이행 정정 ( 정정히스토리테이블에 insert/ 채무불이행테이블에 update) out
				//(정정히스토리테이블에 insert [이전 참조번호의 채무불이행 정정금액 조회해서] / 채무불이행테이블에 update) in
		    	//채무불이행 정정상태
		    	if("C".equals(paramBox.getString("chgStat"))){
		    		paramBox.set("modStat",  paramBox.getString("chgStat"));
		    		
		    		paramBox.set("debtAmt", !"0".equals(debtBox.getString("DEBT_AMT").replaceAll(",", "")) ? (debtBox.getString("DEBT_AMT").replaceAll(",", "")) : debtBox.get("DEBT_AMT"));
		    		resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtApplForRequireModify(paramBox)
		    											  :debtDaoForMssql.updateDebtApplForRequireModify(paramBox);
					// [2] 채무불이행 정정 요청 이력 정보 추가
		    		paramBox.set("stat", paramBox.getString("chgStat"));
		    		//resultBox = "oracle".equals(dbmsType)? debtDaoForOracle.insertDebtModHist(paramBox): debtDaoForMssql.insertDebtModHist(paramBox);
		    		
		    		resultBox= "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtModHistForIF(paramBox):debtDaoForMssql.updateDebtModHistForIF(paramBox);//TODO MSSQL쿼리작성후 테슽
		    		
		    		
		    		if (!"00000".equals(resultBox.getString("REPL_CD"))) { 
		    			 trans = "0";
		    			 rcode = "FSYS01";
		    			 reason = "처리 중 예외발생["+EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
		    			 log.e("Unknown Exception :" + resultBox.getString("REPL_CD")+","+ EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+ "]");
					}
		    	//채무불이행 상태변경	
		        }else{
		        	paramBox.set("trans", trans);
		        	paramBox.set("rcode", rcode);
		        	paramBox.set("reason", reason);
		        	
		        	SBox updateBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateDebtStatSendEndForWS(paramBox) :
		        											    debtDaoForMssql.updateDebtStatSendEndForWS(paramBox) ;
					debtStatHistId=updateBox.getString("DEBT_STAT_HIST_ID");
					if (!"00000".equals(updateBox.getString("REPL_CD"))) {
						 trans = "0";
		    			 rcode = "FSYS01";
		    			 reason = "debtStatHistId["+debtStatHistId+"] 채무불이행 상태변경 저장 중 예외발생";
		    			 log.e("Unknown Exception : debtStatHistId ["+debtStatHistId+":" + updateBox.getString("REPL_CD")+","+EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg()+ "]");
					}
					
					// 파일 연결을 위하여 debt_stat_hist_f_id 추가 
   					SBox FileBox = "oracle".equals(dbmsType)? debtDaoForOracle.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId):
							debtDaoForMssql.updateParentDebtStatForTemp(debtStatHistId, debtStatHistId);
   					log.d("파일 연결을 위하여 debt_stat_hist_f_id 추가 ");
   					if (!"00000".equals(FileBox.getString("REPL_CD"))) {
   						throw new BizException("debtStatHistId:"+debtStatHistId+", "+FileBox.getString("REPL_CD"), EErrorCodeType.search(FileBox.getString("REPL_CD")).getErrMsg());
   					}
		        }
	    	}else{
	    		 trans = "0";
    			 rcode = "FSYS01";
    			 reason = "처리 중 예외발생";
	    	}
	    	
	    	
	    	paramBox.set("trans", trans); //정상처리될때 에러처리될떄 들어가는 trans값과 rcode값 reason값을 확인해야함
	    	paramBox.set("rcode", rcode);
	    	paramBox.set("reason", reason);
	    	
	    	//NoticeConclusion 응답메시지 전송
			 String senderId = messageTag.getReceiveId();
			 String senderName = messageTag.getReceiveName();
			 String instanceId = SysUtil.getInstanceId(serviceCodeNoticeConclusion,senderId); //요청 문서의 instanceId
			//라우팅 메시지 VO 생성
			MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//S/A/P
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setReferenceId(messageTag.getInstanceId());
			 msgEvent.setGroupId(messageTag.getGroupId());//채무불이행순번 필수 
			 msgEvent.setDocumentType(serviceCodeNoticeConclusion);//NOTICE_CONCLUSION
			 msgEvent.setActionType(null); //채무불이행 신청 상태
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED 
			 msgEvent.setFileCount(0);
//			 msgEvent.setTypeCode("RES");
			 msgEvent.setMessageTypeCode("RES");
			 msgEvent.setResultCode(rcode);
			 msgEvent.setResultMessage(reason);
	    	
	    	ResponseDocumentCommonVo responseVo = new ResponseDocumentCommonVo();
			// 응답문서 정보 영역
			responseVo.setId(instanceId);
			responseVo.setReferenceID(messageTag.getInstanceId());
			responseVo.setRDocumentTypeCode(serviceCodeNoticeConclusion);
			responseVo.setVersionInformation("1.0.0");
			responseVo.setRDDescription("채무불이행 상태 변경 처리결과통보서");
			
			//응답 결과 영역
			responseVo.setRRDTypeCode(trans);//1정상처리
			responseVo.setResponseTypeCode(rcode);//정상 처리됨
			responseVo.setRRDDescription(reason);
			
			String strXml = CommonMessageIF.makeNoticeConclusionForXML(responseVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent); 
			
	
		}catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("31003").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
		return debtStatHistId;
	}

	
	
	
	
	
	
	/**
	 * <pre>
	 * //[조기경보 대상 거래처 등록업체(거래처) 신용 등급 변경 통지서(Only 조기경보] - CNTUCL
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 9. 25.
	 * @version 1.0
	 * @param messageTag
	 */
	@Override
	@Transactional
	public void getCreditNoticeUpdateCustomerLevel(MessageTag messageTag){
		SBoxList<SBox>	resultList = new SBoxList<SBox>();
		SBox resultBox = new SBox();
		try{

			// XML메시지 class 구조로 파싱
			resultList = CommonMessageIF.getCreditNoticeUpdateCustomerLevelForXML(messageTag.getDocumentData());
			String trans = "1";
			String rcode = "SCMN01";
			String reason = "정상 처리됨";
			
			String usrNo = messageTag.getReceiveId(); // 본사 사업자번호
			String usrNm = messageTag.getReceiveName(); //본사 사업자명
			
			//NoticeConclusion 응답메시지 전송
			 String senderId = messageTag.getReceiveId();
			 String senderName = messageTag.getReceiveName();
			 String instanceId = SysUtil.getInstanceId(serviceCodeNoticeConclusion,senderId); //요청 문서의 instanceId
			//라우팅 메시지 VO 생성
			MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//S/A/P
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setReferenceId(messageTag.getInstanceId());
			 msgEvent.setGroupId(messageTag.getGroupId());//채무불이행순번 필수 
			 msgEvent.setDocumentType(serviceCodeNoticeConclusion);//NOTICE_CONCLUSION
			 msgEvent.setActionType(null); //채무불이행 신청 상태
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED 
			 msgEvent.setFileCount(0);
			 //msgEvent.setTypeCode("RES");
			 msgEvent.setMessageTypeCode("RES");
			 msgEvent.setResultCode(rcode);
			 msgEvent.setResultMessage(trans);
	    	
	    	ResponseDocumentCommonVo responseVo = new ResponseDocumentCommonVo();
			// 응답문서 정보 영역
			responseVo.setId(instanceId);
			responseVo.setReferenceID(messageTag.getInstanceId());
			responseVo.setRDocumentTypeCode(serviceCodeNoticeConclusion);
			responseVo.setVersionInformation("1.0.0");
			responseVo.setRDDescription("크레딧서비스거래처신용등급변경통지서");
			
			//응답 결과 영역
			responseVo.setRRDTypeCode(trans);//1정상처리
			responseVo.setResponseTypeCode(rcode);//정상 처리됨
			responseVo.setRRDDescription(reason);
			
			for(SBox sBox : resultList ){
				sBox.set("usrNo", usrNo); // 본사 사업자번호
				sBox.set("usrNm", usrNm); // 본사 사업자명
				resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustCrdForIF(sBox):
													   customerDaoForMssql.updateCustCrdForIF(sBox);
				
				if (!"00000".equals(resultBox.getString("REPL_CD"))) {
					 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "처리 중 예외발생["+EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
					throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
				}
				log.i("조기경보 등록된 거래처 EW등급 받아오기 결과: REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			}
			
			String strXml = CommonMessageIF.makeNoticeConclusionForXML(responseVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent); 
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10801").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
	}
	
	/**
	 * <pre>
	 * [조기경보 대상 거래처 등록업체(거래처) 신용 등급 변경 통지서(조기경보 + KLINK] - CNTKCL 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2016. 4. 26.
	 * @version 1.0
	 * @param messageTag
	 */
	@Override
	@Transactional
	public void getCreditNoticeByKLinkUpdateCustomerLevel(MessageTag messageTag){
		SBoxList<SBox>	dataList10 = new SBoxList<SBox>();	
		SBoxList<SBox>	dataList30 = new SBoxList<SBox>();	
		SBox resultBox = new SBox();
		try{

			// XML메시지 10,30 으로 전달받음
			ArrayList<String> dataMessage = CommonMessageIF.getCreditNoticeByKLinkUpdateCustomerLevelForXML(messageTag.getDocumentData());
			
			//String strLabel10[] ={"EWCD","REG_ENP_NM","REG_ENP_REPER_NM","REG_DT","CHG_DT","BZNO","CONO","PID","TXPL_CLS","EW_MODL_SGMT_CLS","DEL_YN","UPD_DT","KED_CD"};
			String strLabel10[] ={"ewCd","custNm","ownNm","baseDt","ewchgDt","custNo","corpNo","ownNo","txplCls","ewModlSgmtCls","delYn","updDt","kedCd"};
			//EWCD,기준일자,EWRATING,RATING구분,CREDIT RISK,CARD CREDIT RISK,채무불이행_기업,채무불이행_대표자,신용등급,재무,소송,상거래연체,당좌거래정지,휴폐업,단기연체_전은연,행정처분정보,법정관리/workout,채무불이행_공공_기업,채무불이행_공공_개인,조회기록,휴폐업코드,일반 단기연체 진행중,법인카드단기연체 진행중,6개월이내일반단기연체경험,6개월이내법인단기연체경험,업데이트일자
			String strLabel30[] ={"ewCd","crdUpdDt","ewRating","ewRatingType","crYn","crCardYn","compDebtYn","perDebtYn","crdLvYn","finYn","lwstYn","ctxOvYn","susAccYn","clsBizYn","crKfbYn","adminMeasureYn","workoutYn","nexePurcEnpYn","nexePurcReperYn","crCurrentYn","crCardCurrentYn","crHisYn","crCardHisYn","scRcdYn","clsBizCd","kedUpdDt"};
			
			dataList10 = CommonMessageIF.getCreditNoticeByKLinkUpdateCustomerLevelForFormat(strLabel10, dataMessage.get(0).toString());
			dataList30 = CommonMessageIF.getCreditNoticeByKLinkUpdateCustomerLevelForFormat(strLabel30, dataMessage.get(1).toString());
			
			//다 update Insert 처리하고,
			//전송 메시지 발송
			
			String trans = "1";
			String rcode = "SCMN01";
			String reason = "정상 처리됨";
			
			String usrNo = messageTag.getReceiveId(); // 본사 사업자번호
			String usrNm = messageTag.getReceiveName(); //본사 사업자명
			
			
			//K-Link EW10정보 입력
			for(SBox sBox : dataList10 ){
				sBox.set("usrNo", usrNo); // 본사 사업자번호
				sBox.set("usrNm", usrNm); // 본사 사업자명
				
				String cutNo = sBox.getString("CUST_NO");
				String ewCd = sBox.getString("EW_CD");
				
				resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.updateCustCrdKLinkEW10ForIF(sBox):
													   customerDaoForMssql.updateCustCrdKLinkEW10ForIF(sBox);
				
				if (!"00000".equals(resultBox.getString("REPL_CD"))) {
					 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "처리 중 예외발생[ 거래처 사업자번호 : "+cutNo+", EW_CD: "+ewCd + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
					//throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
				}
				log.i("EW10 조기경보 등록된 거래처 EW등급 받아오기 결과: REPL_CD[거래처 사업자번호 :  "+cutNo+", EW_CD: "+ewCd + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			}
			
			//K-Link EW30정보 입력
			for(SBox sBox : dataList30 ){
				sBox.set("usrNo", usrNo); // 본사 사업자번호
				sBox.set("usrNm", usrNm); // 본사 사업자명
				resultBox = "oracle".equals(dbmsType)? customerDaoForOracle.inertCustCrdHistKLinkEW30ForIF(sBox):
													   customerDaoForMssql.inertCustCrdHistKLinkEW30ForIF(sBox); 
				
				if (!"00000".equals(resultBox.getString("REPL_CD"))) {
					 trans = "0";
	    			 rcode = "FSYS01";
	    			 reason = "처리 중 예외발생["+EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg()+"]";
					//throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
				}
				log.i("EW30 조기경보 등록된 거래처 EW등급 받아오기 결과: REPL_CD[" + resultBox.getString("REPL_CD") + ", REPL_MSG[" + EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg() + "]");
			}
			
			//NoticeConclusion 응답메시지 전송
			 String senderId = messageTag.getReceiveId();
			 String senderName = messageTag.getReceiveName();
			 String instanceId = SysUtil.getInstanceId(serviceCodeNoticeConclusion,senderId); //요청 문서의 instanceId
			//라우팅 메시지 VO 생성
			MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
			 msgEvent.setSenderId(senderId);
			 msgEvent.setSenderName(senderName);
			 msgEvent.setReceiveId(receiveId);
			 msgEvent.setReceiveName(receiveName);
			 msgEvent.setResponseType("A");//S/A/P
			 msgEvent.setInstanceId(instanceId);
			 msgEvent.setReferenceId(messageTag.getInstanceId());
			 msgEvent.setGroupId(messageTag.getGroupId());//채무불이행순번 필수 
			 msgEvent.setDocumentType(serviceCodeNoticeConclusion);//NOTICE_CONCLUSION
			 msgEvent.setActionType(null); //채무불이행 신청 상태
			 msgEvent.setDocCreationDateTime(DateUtil.datetime());
			 msgEvent.setServiceCode(serviceCodeCred);//CRED 
			 msgEvent.setFileCount(0);
			 //msgEvent.setTypeCode("RES");
			 msgEvent.setMessageTypeCode("RES");
			 msgEvent.setResultCode(rcode);
			 msgEvent.setResultMessage(trans);
	    	
	    	ResponseDocumentCommonVo responseVo = new ResponseDocumentCommonVo();
			// 응답문서 정보 영역
			responseVo.setId(instanceId);
			responseVo.setReferenceID(messageTag.getInstanceId());
			responseVo.setRDocumentTypeCode(serviceCodeNoticeConclusion);
			responseVo.setVersionInformation("1.0.0");
			responseVo.setRDDescription("크레딧서비스거래처신용등급변경통지서");
			
			//응답 결과 영역
			responseVo.setRRDTypeCode(trans);//1정상처리
			responseVo.setResponseTypeCode(rcode);//정상 처리됨
			responseVo.setRRDDescription(reason);
			
			String strXml = CommonMessageIF.makeNoticeConclusionForXML(responseVo);
			msgEvent.setDocumentData(strXml);
			wsClient.remoteASyncMessageCall(msgEvent); 
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10801").getErrMsg() + "]");
			log.e(ex.getMessage());
		}
	}
	
	
	
	/////////////////SYNC////////////////////////////////////////////////////////////////////
	
	/**
	 * <pre>
	 * 사용자정보 가져오기 I/F - CreditRequestUserList CRQUSL
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 6. 29.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBoxList<SBox> getSyncUserListFromMiddleServer(SBox sBox){
		 String senderId = sBox.getString("sessionUsrNo"); //송신자 정보
		 String senderName = sBox.getString("sessionCompUsrNm");
		 int mbrId = sBox.getInt("sessionMbrId");
		 String instanceId = SysUtil.getInstanceId(serviceCodeRequestUserList, senderId); //요청 문서의 instanceId
		
		 //라우팅 메시지 VO 생성
		MessageEvent msgEvent = new MessageEvent(); //라우팅 메시지 보내기 위해 
		 msgEvent.setSenderId(senderId);
		 msgEvent.setSenderName(senderName);
		 msgEvent.setReceiveId(receiveId);
		 msgEvent.setReceiveName(receiveName);
		 msgEvent.setResponseType("S");//S/A/P
		 msgEvent.setInstanceId(instanceId);
		 msgEvent.setGroupId(null);//채무불이행순번 필수 
		 msgEvent.setDocumentType(serviceCodeRequestUserList);
		 msgEvent.setActionType(null); //채무불이행 신청 상태
		 msgEvent.setDocCreationDateTime(DateUtil.datetime());
		 msgEvent.setServiceCode(serviceCodeCred);//CRED
		 msgEvent.setFileCount(0);
		 msgEvent.setTypeCode("REQ");
		
	//메시지 영역 VO 생성
		RequestCommonVo  requestVo = new RequestCommonVo();
		 requestVo.setID(instanceId); //InstanceId
		 requestVo.setRequestDocumentTypeCode(serviceCodeRequestUserList); 
		 requestVo.setVersionInformation("1.0.0");
		 requestVo.setDescription(null);//"크레딧서비스회원목록요청서");
		 requestVo.setUserBusinessAccountTypeCode("E");// ERP회원
		 requestVo.setUserID(sBox.getString("sessionSbusrId"));//"130952"); 	
		 requestVo.setLoginID(sBox.getString("sessionSbLoginId")); //스마트 채권의 아이디  
		 requestVo.setLoginPasswordID(null);//"123");
		EWCustRequestVo  ewCustRequstVo = new EWCustRequestVo();
		 ewCustRequstVo.setId(sBox.getString("usrNoFromWeb"));
		 
		//2. XML메시지 생성
		 String strXml = null;
		 strXml = CommonMessageIF.makeCreditRequestUserMessageForXML(requestVo,ewCustRequstVo );
		//메시지 영역 문서 라우팅에 추가
		 msgEvent.setDocumentData(strXml);
		
		 
		 SBoxList<SBox> userList = null;
		 System.out.println(msgEvent.toString());
		//3. 건당 라우팅 메시지 보내고
		String responseXml ="";
		try {
			MessageTagVo responseMessageVo = wsClient.remoteSyncMessageCall(msgEvent);
			
			if("SCMN01".equals(responseMessageVo.getResultCode().getValue())){//메시지가 정상이면
				
				responseXml = responseMessageVo.getXmlMessage();
				userList = CommonMessageIF.getCrecditUsrListReponseMessage(responseXml);
				 //DBO연결하여 Insert로직 추가
				 if(userList.size()!=0){
					 for(SBox insertSBox : userList){
						 insertSBox.set("mbrId", mbrId);
						 if("oracle".equals(dbmsType)){
							 configDaoForOracle.isertDeUsrTempForIF(insertSBox);
						 }else{
							 configDaoForMssql.isertDeUsrTempForIF(insertSBox);
						 }
					 }
				 }
				 
			}else{//메시지가 오류
				responseXml = responseMessageVo.getXmlMessage();
			}
			 
		} catch (IOException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("41000").getErrMsg() + "]");
			log.e(e.getMessage());
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("41000").getErrMsg() + "]");
			log.e(e.getMessage());
		} 
		return userList;
	}
	
	
	
	
	/**
	 * <pre>
	 * 조기경보업체 등록 인터페이스 응답결과
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 30.
	 * @version 1.0
	 * @param messageTag
	 */
	@Override
	@Transactional
	public void getIFResultForCustRegister(MessageTag messageTag){
		//SBox result = new SBox();
		try{
			
			
				
            // XML메시지 class 구조로 파싱
            SBox xmlResult = CommonMessageIF.getEwCustRegistorReponseMessageForXML(messageTag.getDocumentData());
            
            xmlResult.set("custNo", xmlResult.get("id")); 
            xmlResult.set("rcode",xmlResult.get("typeCode"));//0:실패/ 1:성공
        	xmlResult.set("reason",xmlResult.get("description"));		
        	
            // 파라미터 생성
            SBox paramBox = new SBox();
            paramBox.set("usrNo", messageTag.getReceiveId());
			paramBox.set("custNo", xmlResult.getString("custNo"));
			paramBox.set("sessionCompUsrId","5" );//sBox.getString("sessionCompUsrId") 추후에 조회해서 넘겨야함 TODO: 확인필요 20160427
			paramBox.set("compMngCd","ERP1" );//sBox.getString("compMngCd") TODO: 확인필요 20160427
			paramBox.set("trans", xmlResult.getString("rcode")); //처리 성공, 실패 여부. 0: 실패, 1: 성공 , 2: 전송중
        	paramBox.set("reason", xmlResult.getString("reason"));
        	paramBox.set("sessionUsrId", "0");
        	SBox updateBox = "oracle".equals(dbmsType) ? customerDaoForOracle.updateCustResponseForIF(paramBox)
        											   : customerDaoForMssql.updateCustResponseForIF(paramBox);
			
			if (!"00000".equals(updateBox.getString("REPL_CD"))) {
				throw new BizException(updateBox.getString("REPL_CD"), EErrorCodeType.search(updateBox.getString("REPL_CD")).getErrMsg());
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown Exception[" + EErrorCodeType.search("10800").getErrMsg() + "]");
		}
	}
	
	/**
	 * <pre>
	 * 라이센스 등록
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2016 . 6. 14
	 * @version 1.0
	 */
	@Override
	@Transactional
	public int setLicenseKeyForLegacy(String licenseKey){
		int updateCnt = 0;
		try {
			updateCnt=	"oracle".equals(dbmsType)? debtDaoForOracle.insertLicenseKeyForLegacy(licenseKey) : 
				  debtDaoForMssql.insertLicenseKeyForLegacy(licenseKey);
		} catch (BizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updateCnt;
	}
}
