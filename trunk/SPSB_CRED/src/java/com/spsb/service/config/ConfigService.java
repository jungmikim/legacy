package com.spsb.service.config;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 요금관리 Service Interface Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
public interface ConfigService {

	public SBox addJoinMemberUser(SBox sBox);
	public SBox getUserInfoList(SBox sBox);
	public SBox removeUser(String mbrIdList, int sessionUsrId);
	public SBox getUser(int mbrId);
	public SBox modifyUser(SBox sBox);
	boolean isDuplicationMemberUser(String loginId);
	public SBox getUserByLoginId(String loginId);
	public SBox getBizNoSearchName(SBox sBox);
	public SBox checkAdminDeleteYN(SBox sBox);
	public SBox checkAdminYN(SBox sBox);
	//public void addIfMsgHist(SBox sBox);
}
