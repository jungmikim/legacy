package com.spsb.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.enumtype.EUserErrorCode;
import com.spsb.common.exception.BizException;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonPage;
import com.spsb.dao.config.ConfigDaoForMssql;
import com.spsb.dao.config.ConfigDaoForOracle;

/**
 * <pre>
 * 요금관리 Service Implements Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
@Service
public class ConfigServiceImpl extends SuperService implements ConfigService{

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired  ConfigDaoForOracle configDaoForOracle;
	
	@Autowired  ConfigDaoForMssql configDaoForMssql;
	
	@Autowired
	private CommonPage commonPage;
	
	
	@Override
	public SBox getUserInfoList(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> userList = null;
		String pcPage = null;
		
		try {


			// [1-2] 기타 파라미터 초기화
			sBox.setIfEmpty("num", 1); // 현재 페이지
			sBox.setIfEmpty("rowSize", 10); // 목록 갯수

			// [5] 거래처 검색 리스트 TOTAL COUNT 조회
			int userTotalCount = "oracle".equals(dbmsType)? configDaoForOracle.selectUserTotalCount(sBox)
														  : configDaoForMssql.selectUserTotalCount(sBox) ;

			// [6] Paging 모듈 호출
			pcPage = commonPage.getPCPagingPrint(userTotalCount, sBox.getInt("num"), sBox.getInt("rowSize"), "getPage");
			// [7] 거래처 검색 리스트 조회
			userList = "oracle".equals(dbmsType)? configDaoForOracle.selectUserList(sBox)
												 :configDaoForMssql.selectUserList(sBox) ;

			// [11] 조회 결과 저장
			result.set("userList", userList);
			result.set("total", userTotalCount);
			result.set("pcPage", pcPage);

			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40502").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 팝업에서 사용자 추가 
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 6.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public SBox addJoinMemberUser(SBox sBox) {
		SBox resultBox = new SBox();
		try {

			sBox.setIfEmpty("admYn", "N");
			String telNo = sBox.getString("telNo1")+"-"+sBox.getString("telNo2")+"-"+sBox.getString("telNo3");
			sBox.set("mbrStat", "N");
			sBox.set("telNo", telNo);
			// [1] 결제조건 등록
			Integer usrId = "oracle".equals(dbmsType)? configDaoForOracle.insertMbr(sBox)
													  :configDaoForMssql.insertMbr(sBox) ;
			sBox.set("mbrId", usrId);
			
			if ("oracle".equals(dbmsType)) {
				configDaoForOracle.deleteMbrGrnt(usrId);
			}else {
				configDaoForMssql.deleteMbrGrnt(usrId);
			}
			
			if(sBox.get("grnCdArray")!= null){
				
				
				if(!("CRED1C".equals(sBox.get("grnCdArray")))&&!("SBDE1R".equals(sBox.get("grnCdArray")))&&!("SBDE2R".equals(sBox.get("grnCdArray")))&&!("SBDE3R".equals(sBox.get("grnCdArray")))&&!("SBDE4R".equals(sBox.get("grnCdArray")))){
					String[] grnCdArray = sBox.get("grnCdArray");
					for (int i = 0; i < grnCdArray.length; i++) {
						sBox.set("grnCd", grnCdArray[i]);
						if ("oracle".equals(dbmsType)) {
							configDaoForOracle.insertMbrGrnt(sBox);
						}else{
							configDaoForMssql.insertMbrGrnt(sBox);
						}
						
					}
				}else{
					sBox.set("grnCd", sBox.getString("grnCdArray"));
					if ("oracle".equals(dbmsType)) {
						configDaoForOracle.insertMbrGrnt(sBox);
					}else{
						configDaoForMssql.insertMbrGrnt(sBox);
					}
				}
				
			}
			
			sBox.set("result", EUserErrorCode.FAIL);
			if(usrId != null && usrId > 0){
				sBox.set("result", EUserErrorCode.SUCCESS);
				sBox.set("tmpUsrId", usrId);
			}
			
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40501").getErrMsg() + "]");
		}
		return resultBox;

	}
	
	/**
	 * 
	 * <pre>
	 * 회원 삭제 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2015. 7. 7.
	 * @version 1.0
	 * @param debentureIdList
	 *            : 회원 순번 String Array Data
	 * @param sessionUsrId
	 *            : 로그인한 개인회원 순번 Data
	 * @return
	 */
	@Override
	@Transactional
	public SBox removeUser(String mbrIdList, int sessionUsrId) {
		SBox resultBox = new SBox();
		try {
			// [1] 회원 삭제로직 시작(UPDATE 문으로 상태값만 변경함)
			resultBox = "oracle".equals(dbmsType) ? configDaoForOracle.deleteUser(mbrIdList, sessionUsrId)
												  : configDaoForMssql.deleteUser(mbrIdList, sessionUsrId);

			if (!"00000".equals(resultBox.getString("REPL_CD"))) {
				throw new BizException(resultBox.getString("REPL_CD"), EErrorCodeType.search(resultBox.getString("REPL_CD")).getErrMsg());
			}
		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40505").getErrMsg() + "]");
		}
		return resultBox;

	}
	
	/**
	 * <pre>
	 * 회원정보 조회 Service Method
	 * </pre>
	 * 
	 * @author KIM GA EUN
	 * @since 2015. 07. 07.
	 * @param mbrId : 기업회원 순번
	 */
	@Override
	public SBox getUser(int mbrId) {
		SBox resultBox = new SBox();
		
		try {
			// [1] 회원정보 조회
			SBox userInfo = "oracle".equals(dbmsType) ? configDaoForOracle.selectUser(mbrId)
													  : configDaoForMssql.selectUser(mbrId) ;
			// [2] 사용자 권한 리스트 조회
			SBoxList<SBox> userGnrList = "oracle".equals(dbmsType) ? configDaoForOracle.selectUserGrnList(mbrId)
																   : configDaoForMssql.selectUserGrnList(mbrId);

			resultBox.set("userInfo", userInfo); // 회원정보
			resultBox.set("userGnrList", userGnrList); // 사용자 권한 리스트

		} catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40502").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	/**
	 * <pre>
	 * 회원정보 수정
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2015. 07. 07.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	@Transactional
	public SBox modifyUser(SBox sBox) {
		SBox resultBox = new SBox();
		try {
			sBox.set("sessionUsrId", sBox.getInt("sessionUsrId"));
			sBox.set("mbrId", sBox.getInt("mbrId"));
			String telNo = sBox.get("telNo1")+"-"+sBox.get("telNo2")+"-"+sBox.get("telNo3");
			sBox.set("telNo", telNo);
			Integer mbrId = sBox.getInt("mbrId");
			if(sBox.isEmpty("admYn")){
				sBox.set("admYn","N");
			}
			if("oracle".equals(dbmsType)){
				configDaoForOracle.updateMbr(sBox);
				configDaoForOracle.deleteMbrGrnt(mbrId);
			}else{
				configDaoForMssql.updateMbr(sBox);
				configDaoForMssql.deleteMbrGrnt(mbrId);
			}

			if(sBox.get("grnCdArray")!= null){
				if(!("CRED1C".equals(sBox.get("grnCdArray")))&&!("SBDE1R".equals(sBox.get("grnCdArray")))&&!("SBDE2R".equals(sBox.get("grnCdArray")))&&!("SBDE3R".equals(sBox.get("grnCdArray")))&&!("SBDE4R".equals(sBox.get("grnCdArray")))){
					String[] grnCdArray = sBox.get("grnCdArray");
					for (int i = 0; i < grnCdArray.length; i++) {
						sBox.set("grnCd", grnCdArray[i]);
						if("oracle".equals(dbmsType)){
							configDaoForOracle.insertMbrGrnt(sBox);
						}else{
							configDaoForMssql.insertMbrGrnt(sBox);
						}
						
					}
				}else{
					sBox.set("grnCd", sBox.getString("grnCdArray"));
					if("oracle".equals(dbmsType)){
						configDaoForOracle.insertMbrGrnt(sBox);
					}else{
						configDaoForMssql.insertMbrGrnt(sBox);
					}
				}
				
			}
			
			sBox.set("result", EUserErrorCode.FAIL);
			if(mbrId != null && mbrId > 0){
				sBox.set("result", EUserErrorCode.SUCCESS);
				sBox.set("tmpUsrId", mbrId);
			}
			
		}catch (Exception ex) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40503").getErrMsg() + "]");
		}
		return resultBox;

	}
	
	
	/**
	 * <pre>
	 *  중복된 레가시아이디가 존재하는가 여부 Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 27.
	 * @version 1.0
	 * @param loginId
	 * @return
	 */
	@Override
	public boolean isDuplicationMemberUser(String loginId) {
		SBox result = getUserByLoginId(loginId);
		return (result == null) ? false:true;
	}
	
	/**
	 * <pre>
	 * Legacy 아이디  회원 조회 Service Method
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 27.
	 * @version 1.0
	 * @param loginId
	 * @return
	 */
	@Override
	public SBox getUserByLoginId(String loginId) {
		SBox result = null;
		try {
			result = "oracle".equals(dbmsType) ? configDaoForOracle.selectUserByLoginId(loginId)
											   : configDaoForMssql.selectUserByLoginId(loginId);
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40504").getErrMsg() + "]");
		}
		return result;
	}
	
	
	
	/**
	 * <pre>
	 *  
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015. 7. 31.
	 * @version 1.0
	 * @param sBox
	 * @return
	 */
	@Override
	public SBox getBizNoSearchName(SBox sBox) {
		SBox resultBox = new SBox();
		try {

			int mbrId = sBox.getInt("mbrId");
			String name = sBox.getString("name");
			sBox.set("mbrId", mbrId);	
			sBox.set("name", name);	
		
			// [3] 거래처 정보 조회
			SBoxList<SBox> searchUserNameList  = "oracle".equals(dbmsType) ? configDaoForOracle.selectSearchUserNameList(sBox)
																		   : configDaoForMssql.selectSearchUserNameList(sBox);
			/*if (customerBox == null) {
				throw new BizException("10113", EErrorCodeType.search("10113").getErrMsg());
			}
			sBox.set("custNo", customerBox.get("CUST_NO"));
			// 거래처가 이미 삭제상태(D)인 경우 검증
			if ("D".equals(customerBox.getString("STAT"))) {
				throw new BizException("10115", EErrorCodeType.search("10115").getErrMsg());
			}*/


			// [20] 결과값 셋팅
			resultBox.set("searchUserNameList", searchUserNameList); // 거래처 정보
		

			resultBox.set("REPL_CD", "00000");
			resultBox.set("REPL_MSG", EErrorCodeType.search("00000").getErrMsg());

		} catch (BizException biz) {
			resultBox.set("REPL_CD", biz.getErrCode());
			resultBox.set("REPL_MSG", biz.getErrMsg());
			log.e("BizException EXCEPTION[" + biz.getErrMsg() + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("10199").getErrMsg() + "]");
		}
		return resultBox;
	}
	
	/**
	 * <pre>
	 * 사용자 삭제 여부 유무 확인 service
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015. 09.03.
	 * @version 1.0
	 * @return
	 */
	@Override
	public SBox checkAdminDeleteYN(SBox sBox) {
		SBox result = null;
		try {
			// 해당 아이디가 관리자YN, 선택한 아이디를 제외한 관리자의 갯수, 관리자가 아닌 유저가 몇명 존재하는지  받아옴
			result = "oracle".equals(dbmsType) ? configDaoForOracle.checkAdminDeleteYN(sBox)
					   						   : configDaoForMssql.checkAdminDeleteYN(sBox);
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40504").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * <pre>
	 * 사용자 삭제 여부 유무 확인 service
	 * </pre>
	 * @author HWAJUNG SON
	 * @since 2015. 09.03.
	 * @version 1.0
	 * @return
	 */
	@Override
	public SBox checkAdminYN(SBox sBox) {
		SBox result = null;
		try {
			// 해당 아이디가 관리자YN, 선택한 아이디를 제외한 관리자의 갯수, 관리자가 아닌 유저가 몇명 존재하는지  받아옴
			result = "oracle".equals(dbmsType) ? configDaoForOracle.checkAdminYN(sBox)
					   						   : configDaoForMssql.checkAdminYN(sBox);
		} catch (BizException e) {
			e.printStackTrace();
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("40504").getErrMsg() + "]");
		}
		return result;
	}
	
	
}
