package com.spsb.service.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spsb.common.collection.SBox;
import com.spsb.common.collection.SBoxList;
import com.spsb.common.enumtype.EErrorCodeType;
import com.spsb.common.parent.SuperService;
import com.spsb.common.util.CommonPage;
import com.spsb.common.util.CommonUtil;
import com.spsb.dao.admin.AdminDaoForMssql;
import com.spsb.dao.admin.AdminDaoForOracle;

/**
 * <pre>
 * 요금관리 Service Implements Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
@Service
public class AdminServiceImpl extends SuperService implements AdminService{

	// db type setting
	@Value("#{dbType['Globals.DbType'].trim()}")
	private String dbmsType;
	
	@Autowired  AdminDaoForOracle adminDaoForOracle;
	
	@Autowired  AdminDaoForMssql adminDaoForMssql;
	
	@Autowired
	private CommonPage commonPage;
	
	
	/**
	 * 회원유저정보 선택  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Override
	public SBox getCompUsrListAboutCompMng(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> compUsrList = null;		
		SBoxList<SBox> mbrList = null;		
		SBoxList<SBox> grnList = null;
		try {
			compUsrList = "oracle".equals(dbmsType)? adminDaoForOracle.selectCompUsrListAboutCompMng(sBox) : adminDaoForMssql.selectCompUsrListAboutCompMng(sBox) ;

			// [11] 조회 결과 저장
			if(compUsrList.size() != 0){
				result.set("compUsrList", compUsrList);
				//compMng
				sBox.set("compUsr", compUsrList.get(0).getString("COMP_USR_ID"));
				mbrList = "oracle".equals(dbmsType)? adminDaoForOracle.selectMbrListAboutCompUsr(sBox) : adminDaoForMssql.selectMbrListAboutCompUsr(sBox) ;
				if(mbrList.size() !=0 ){
					result.set("mbrList", mbrList);
					sBox.set("mbrId", mbrList.get(0).getString("MBR_ID"));
					grnList = "oracle".equals(dbmsType)? adminDaoForOracle.selectGrnListAboutMbr(sBox) : adminDaoForMssql.selectGrnListAboutMbr(sBox) ;
					if(grnList.size() != 0){
						result.set("grnList", grnList);
						for(int i=0; i<grnList.size(); i++){
							result.set(grnList.get(i).getString("GRN_CD"),  grnList.get(i).getString("GRN_CD"));
						}
					}
					result.set("grnCount", grnList.size());
				}
				result.set("mbrCount", mbrList.size());
			}
			result.set("compUsrCount", compUsrList.size());
			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("39901").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * 회원유저정보 선택  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Override
	public SBox getMbrListAboutCompUsr(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> mbrList = null;		
		SBoxList<SBox> grnList = null;
		try {
			System.out.println(sBox.toString());
			mbrList = "oracle".equals(dbmsType)? adminDaoForOracle.selectMbrListAboutCompUsr(sBox) : adminDaoForMssql.selectMbrListAboutCompUsr(sBox) ;
			if(mbrList.size() !=0 ){
				result.set("mbrList", mbrList);
				
				sBox.set("mbrId", mbrList.get(0).getString("MBR_ID"));
				grnList = "oracle".equals(dbmsType)? adminDaoForOracle.selectGrnListAboutMbr(sBox) : adminDaoForMssql.selectGrnListAboutMbr(sBox) ;
				if(grnList.size() != 0){
					result.set("grnList", grnList);
					for(int i=0; i<grnList.size(); i++){
						result.set(grnList.get(i).getString("GRN_CD"),  grnList.get(i).getString("GRN_CD"));
					}
				}
				result.set("grnCount", grnList.size());
				
			}
			result.set("mbrCount", mbrList.size());
			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("39901").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * 회원유저정보 선택  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Override
	public SBox getGrnListAboutMbrList(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> grnList = null;
		try {
			grnList = "oracle".equals(dbmsType)? adminDaoForOracle.selectGrnListAboutMbr(sBox) : adminDaoForMssql.selectGrnListAboutMbr(sBox) ;
			if(grnList.size() != 0){
				result.set("grnList", grnList);
				for(int i=0; i<grnList.size(); i++){
					result.set(grnList.get(i).getString("GRN_CD"),  grnList.get(i).getString("GRN_CD"));
				}
			}
			result.set("grnCount", grnList.size());
			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("39901").getErrMsg() + "]");
		}
		return result;
	}
	
	/**
	 * 연동업체 페이지 조회  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Override
	public SBox getCompMngList(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> compMngList = null;		

		try {
			compMngList = "oracle".equals(dbmsType)? adminDaoForOracle.selectCompMngList() : adminDaoForMssql.selectCompMngList() ;

			// [11] 조회 결과 저장
			if(compMngList.size() > 0){
				result.set("compMngList", compMngList);
			}
			result.set("compMngCount", compMngList.size());
			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("39901").getErrMsg() + "]");
		}
		return result;
	}
	
	
	/**
	 * 회원유저정보 선택  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Override
	public SBox getPrdListAboutCompMng(SBox sBox){
		SBox result = new SBox();
		SBoxList<SBox> prdList = null;
		try {
			prdList = "oracle".equals(dbmsType)? adminDaoForOracle.selectPrdListAboutCompMng(sBox) : adminDaoForMssql.selectPrdListAboutCompMng(sBox) ;
			if(prdList.size() != 0){
				result.set("prdList", prdList);
				for(int i=0; i<prdList.size(); i++){
					result.set(prdList.get(i).getString("PRD_TYPE"),  prdList.get(i).getString("PRD_TYPE"));
				}
			}
			result.set("prdCount", prdList.size());
			// [11] resultCode, resultMsg 초기화
			result.set("resultCode", "00000");
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());

		} catch (Exception ex) {
			log.e("Unknown EXCEPTION[" + EErrorCodeType.search("39901").getErrMsg() + "]");
		}
		return result;
	}
	/**
	 * 연동업체 등록  
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Transactional
	public SBox addCompUser(SBox sBox){
		SBox result = new SBox();
		int insertBox = 0;		
		SBox temp = new SBox();
		int serviceInsertCnt = 0 ;
		try {
			// [1] 연동업체 중복여부 체크 
			System.out.println(sBox.toString());
			int checkComp_YN = "oracle".equals(dbmsType)? adminDaoForOracle.checkCompMng(sBox) : adminDaoForMssql.checkCompMng(sBox) ;
			if(checkComp_YN>0){
				result.set("REPL_CD", "00002");
				result.set("resultMsg", "연동업체 중복");
			}else{
				// [2] 서비스 코드 등록 
				int serviceCode_YN = "oracle".equals(dbmsType)? adminDaoForOracle.checkServiceCode(sBox) : adminDaoForMssql.checkServiceCode(sBox) ;
				
				if(serviceCode_YN == 0) {
					String [] service_codes = {"TMST","CRED","SBDE"};
					String [] service_nms = {"통합관제","채무불이행","거래처관리"};
					for(int i=0; i<service_codes.length; i++){
						temp.set("service_code",service_codes[i]);
						temp.set("service_nm",service_nms[i]);
						temp.set("version","1.1");
						serviceInsertCnt = "oracle".equals(dbmsType)? adminDaoForOracle.insertServiceCode(temp) : adminDaoForMssql.insertServiceCode(temp) ;	
					}
					if(serviceInsertCnt == 0 ) {
						throw new Exception();
					}
				}
				// [3] 연동업체 등록
				insertBox = "oracle".equals(dbmsType)? adminDaoForOracle.insertCompUser(sBox) : adminDaoForMssql.insertCompUser(sBox) ;
				
				if(insertBox > 0 ){
					result.set("REPL_CD", "00000");
					result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());
				}else{
					result.set("REPL_CD", "00001");
					result.set("resultMsg", "insert 등록에러");
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			result.set("REPL_CD","00001");
			log.e("Unknown 연동업체 등록 EXCEPTION[" + ex.getMessage() + "]");
		}

		return result;
	}
	
	/**
	 * 상품 등록 
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Transactional
	public SBox addCompProduct(SBox sBox){
		SBox result = new SBox();
		int insertBox = 0;		
		StringBuffer sb = new StringBuffer();
		int succCnt =0;
		int errCnt =0;

		try {
			sBox.set("compMng",sBox.getString("selectedComp"));
			SBoxList<SBox> checkPrd_YN = "oracle".equals(dbmsType)? adminDaoForOracle.selectPrdListAboutCompMng(sBox) : adminDaoForMssql.selectPrdListAboutCompMng(sBox) ;
			if(checkPrd_YN.size()>0){
				// 기존에 권한이 있을 경우 기존 권한 삭제 
				int deleteBox =  "oracle".equals(dbmsType)? adminDaoForOracle.deletePrdInfo(sBox) : adminDaoForMssql.deletePrdInfo(sBox) ;
				
				if(deleteBox >0){
					result.set("REPL_CD", "00000");
				}else{
					throw new Exception("등록되어있던 상품삭제 실패");
				}
				result.set("type","U");
			}else{
				result.set("type","I");
			}
			
			String prdList [] = sBox.getString("prdList").split(",");

			for(String prdCd : prdList ){
				sBox.set("prdCd",prdCd);
				insertBox = "oracle".equals(dbmsType)? adminDaoForOracle.insertCompProduct(sBox) : adminDaoForMssql.insertCompProduct(sBox) ;	
				if(insertBox == 0){
					errCnt++;
					if(sb.length() == 0){
						sb.append(prdCd);
					}else{
						sb.append(","+prdCd);
					}
				}else{
					succCnt ++ ;
				}
			}
			
			result.set("SUCC_CNT",succCnt);
			result.set("ERR_CNT", errCnt);
			result.set("ERR_PRD_CD", sb.toString());
			result.set("resultMsg", EErrorCodeType.search("00000").getErrMsg());
			
		} catch (Exception ex) {
			result.set("REPL_CD","00001");
			log.e("Unknown 연동업체별 상품 등록EXCEPTION[" + ex.getMessage() + "]");
		}
		return result;
	}
	
	/**
	 * 연동업체 별 사업장 등록
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Transactional
	public SBox addCustForComp(SBox sBox){
		SBox result = new SBox();
		int insertBox = 0;		

		try {
			
			sBox.set("selectedComp",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedComp")));
			sBox.set("usr_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("usr_no")));
			sBox.set("comp_mng_nm",CommonUtil.changeNullAboutEmptyValue(sBox.get("comp_mng_nm")));
			sBox.set("email",CommonUtil.changeNullAboutEmptyValue(sBox.get("email")));
			sBox.set("tel_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("tel_no")));
			sBox.set("mb_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("mb_no")));
			sBox.set("sign_info",CommonUtil.changeNullAboutEmptyValue(sBox.get("sign_info")));
			sBox.set("cert_info",CommonUtil.changeNullAboutEmptyValue(sBox.get("cert_info")));
			sBox.set("usr_type",CommonUtil.changeNullAboutEmptyValue(sBox.get("usr_type")));
			sBox.set("post_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("post_no")));
			sBox.set("addr_1",CommonUtil.changeNullAboutEmptyValue(sBox.get("addr_1")));
			sBox.set("addr_2",CommonUtil.changeNullAboutEmptyValue(sBox.get("addr_2")));
			sBox.set("own_nm",CommonUtil.changeNullAboutEmptyValue(sBox.get("own_nm")));
			sBox.set("corp_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("corp_no")));
			sBox.set("biz_type",CommonUtil.changeNullAboutEmptyValue(sBox.get("biz_type")));
			sBox.set("biz_cond",CommonUtil.changeNullAboutEmptyValue(sBox.get("biz_cond")));
			
			insertBox = "oracle".equals(dbmsType)? adminDaoForOracle.insertCustForComp(sBox) : adminDaoForMssql.insertCustForComp(sBox) ;
			
			if(insertBox >0){
				result.set("REPL_CD", "00000");
			}else{
				result.set("REPL_CD", "00001");
			}
			
		} catch (Exception ex) {
			result.set("REPL_CD","00001");
			log.e("Unknown 연동업체별 상품 등록EXCEPTION[" + ex.getMessage() + "]");
		}
		return result;
	}
	
	/**
	 * 사업장 별 회원정보 등록
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Transactional
	public SBox addMbrInfo(SBox sBox){
		SBox result = new SBox();
		int insertBox = 0;		

		try {
			
			sBox.set("selectedComp",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedComp")));
			sBox.set("selectedCompUsr",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedCompUsr")));
			sBox.set("login_id",CommonUtil.changeNullAboutEmptyValue(sBox.get("login_id")));
			sBox.set("login_pwd",CommonUtil.changeNullAboutEmptyValue(sBox.get("login_pwd")));
			sBox.set("adm_yn",CommonUtil.changeNullAboutEmptyValue(sBox.get("adm_yn")));
			sBox.set("usr_nm",CommonUtil.changeNullAboutEmptyValue(sBox.get("usr_nm")));
			sBox.set("email",CommonUtil.changeNullAboutEmptyValue(sBox.get("email")));
			sBox.set("tel_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("tel_no")));
			sBox.set("mb_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("mb_no")));
			sBox.set("fax_no",CommonUtil.changeNullAboutEmptyValue(sBox.get("fax_no")));
			sBox.set("debt_nm",CommonUtil.changeNullAboutEmptyValue(sBox.get("debt_nm")));
			sBox.set("job_tl_nm",CommonUtil.changeNullAboutEmptyValue(sBox.get("job_tl_nm")));
			sBox.set("comp_personal_id",CommonUtil.changeNullAboutEmptyValue(sBox.get("comp_personal_id")));
			sBox.set("sb_comp_usr_id",CommonUtil.changeNullAboutEmptyValue(sBox.get("sb_comp_usr_id")));
			sBox.set("sb_login_id",CommonUtil.changeNullAboutEmptyValue(sBox.get("sb_login_id")));
			sBox.set("sb_usr_id",CommonUtil.changeNullAboutEmptyValue(sBox.get("sb_usr_id")));
			
			insertBox = "oracle".equals(dbmsType)? adminDaoForOracle.insertMbrInfo(sBox) : adminDaoForMssql.insertMbrInfo(sBox) ;
			
			if(insertBox >0){
				result.set("REPL_CD", "00000");
			}else{
				result.set("REPL_CD", "00001");
			}
			
		} catch (Exception ex) {
			result.set("REPL_CD","00001");
			log.e("Unknown 연동업체별 상품 등록EXCEPTION[" + ex.getMessage() + "]");
		}
		return result;
	}
	
	/**
	 * 사업장 별 회원정보 등록
	 * @author HWAJUNG SON
	 * @since 2016.06.17
	 */
	@Transactional
	public SBox addGrnInfo(SBox sBox){
		SBox result = new SBox();
		int insertBox = 0;		

		try {
			
			sBox.set("selectedComp",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedComp")));
			sBox.set("selectedCompUsr",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedCompUsr")));
			sBox.set("selectedMbr",CommonUtil.changeNullAboutEmptyValue(sBox.get("selectedMbr")));
			sBox.set("prdList",CommonUtil.changeNullAboutEmptyValue(sBox.get("prdList")));
			
			// 권한여부 체크 
			SBoxList<SBox> checkGrn_YN = "oracle".equals(dbmsType)? adminDaoForOracle.selectGrnListAboutMbr(sBox) : adminDaoForMssql.selectGrnListAboutMbr(sBox) ;
			if(checkGrn_YN.size()>0){
				// 기존에 권한이 있을 경우 기존 권한 삭제 
				int deleteBox =  "oracle".equals(dbmsType)? adminDaoForOracle.deleteGrnInfo(sBox) : adminDaoForMssql.deleteGrnInfo(sBox) ;
				
				if(deleteBox >0){
					result.set("REPL_CD", "00000");
				}else{
					throw new Exception("등록되어있던 권한삭제 실패");
				}
				result.set("type","U");
			}else{
				result.set("type","I");
			}
			
			// 권한 등록
			if(!sBox.isEmpty("prdList")){
				String grants [] = sBox.getString("prdList").split(",");
				for(String grant : grants){
					sBox.set("grantVal",grant);
					insertBox = "oracle".equals(dbmsType)? adminDaoForOracle.insertGrnInfo(sBox) : adminDaoForMssql.insertGrnInfo(sBox) ;
				}
			}
	
			if(insertBox >0){
				result.set("REPL_CD", "00000");
			}else{
				result.set("REPL_CD", "00001");
			}
			
			
		} catch (Exception ex) {
			result.set("REPL_CD","00001");
			log.e("Unknown 연동업체별 상품 등록EXCEPTION[" + ex.getMessage() + "]");
		}
		return result;
	}
	
}
