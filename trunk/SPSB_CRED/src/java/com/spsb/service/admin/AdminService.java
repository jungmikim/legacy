package com.spsb.service.admin;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 요금관리 Service Interface Class
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 11. 24.
 * @version 1.0
 */
public interface AdminService {


	public SBox getCompMngList(SBox sBox);
	public SBox addCompUser(SBox sBox);
	public SBox addCompProduct(SBox sBox);
	public SBox addCustForComp(SBox sBox);
	public SBox getCompUsrListAboutCompMng(SBox sBox);
	public SBox addMbrInfo(SBox sBox);
	public SBox addGrnInfo(SBox sBox);
	public SBox getMbrListAboutCompUsr(SBox sBox);
	public SBox getGrnListAboutMbrList(SBox sBox);
	public SBox getPrdListAboutCompMng(SBox sBox);
	
	/*boolean isDuplicationMemberUser(String loginId);
	public SBox getUserByLoginId(String loginId);
	public SBox getBizNoSearchName(SBox sBox);
	public SBox checkAdminDeleteYN(SBox sBox);
	public SBox checkAdminYN(SBox sBox);*/
	//public void addIfMsgHist(SBox sBox);
}
