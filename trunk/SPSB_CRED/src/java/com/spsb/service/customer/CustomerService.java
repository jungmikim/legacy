package com.spsb.service.customer;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 거래처 Service Interface Class
 * </pre>
 * @author JUNG MI KIM
 * @since 2015. 6. 2.
 * @version 1.0
 */
public interface CustomerService {

	public SBox getCustomerList(SBox sBox);

	public SBox getCustomerListForExcel(SBox sBox, String fileName);

	public SBox getCustomerForm(SBox sBox);

	public SBox getCustomer(SBox sBox);

	public SBox checkDuplicationCustomer(String custNo, int sessionCompUsrId, String custType);
	
	public SBox getCustomerAllCreditHistoryList(SBox sBox);

	public SBox getCustomerSearch(SBox sBox);
	
	public SBox getEwReportForKED(SBox sBox);
	
	public SBox checkCustSyncForIF();//KED페이지에서 거래처를 삭제하게 된 경우, Legacy와 데이터 Sync.
	
	public SBox modifyCustomer(SBox sBox);
	
	public SBox getLicenseKeyForKED();
	//public void checkFormatValidation(SBoxList<SBox> data , SBox validationBox, int sessionCompUsrId, String cust_no);
		//public SBox removeCustomer(String customerIdList, int sessionCompUsrId, int sessionUsrId);
		//public SBox addCustomer(SBox sBox);
		//public SBox modifyCreditInfo(SBox sBox);
		//public SBox addPayTerm(SBox sBox);
		//public SBox removePayTerm(SBox sBox);
		//public SBox modifyPayTerm(SBox sBox);
		//public SBox getPayTermList(SBox sBox);
		//public SBox getCreditChgListForSB(SBox sBox);
		//public SBox getCustCrdHistForSB(SBox sBox);
		//public SBox getCustCreditUserInfo(SBox sBox);
		//public SBox getCustomerMultiHistoryList(SBox sBox);
		//public SBox getCustomerMultiHistoryListForExcel(SBox sBox, String fileName);
		//public SBox getCustomerMltAddFailListForExcel(int sessionCompUsrId, String fileName);
		//public SBox getCustomerMultiForm(int sessionCompUsrId);
		//public SBox addTempCustomerMulti(SBox sBox, MultipartFile custExcFile);
		//public SBox convertCustomerMulti(int sessionCompUsrId , int sessionUsrId, Row row);
		//public SBox modifyCustomerMulti(SBox sBox, MultipartFile custExcFile);
		//public SBox removeCustomerMulti(int mltId,int sessionCompUsrId );
		//public SBox addCustomerMulti(SBox sBox);
		//public void checkRequiredValidation(SBoxList<SBox> data);
		//public SBox addTempCustomerData(SBoxList<SBox> data, SBox sBox) ;
		//public SBox modifyTempCustomerData(SBoxList<SBox> data, SBox sBox);
		//public SBox getCustomerMultiStat(int sessionCompUsrId);
		//public SBox addFirstPayTerm(SBox sBox);
		//public SBox getCustomerPlanList(SBox sBox);
		//public SBox getCustomerPlanForm(SBox sBox);
		//public SBox addCustomerPlan(SBox sBox);
		//public SBox modifyCustomerPlan(SBox sBox);
		//public SBox getCustomerPlanDelayForm(SBox sBox);
		//public SBox getCustomerPlanDelayDetail(SBox sBox);
		//public SBox delayCustomerPlan(SBox sBox);
		//public SBox removeCustomerPlan(SBox sBox);
		//public SBox getCustomerColForm(SBox sBox);
		//public SBox addCustomerCol(SBox sBox);
		//public SBox getCustomerPrsForm(SBox sBox);
		//public SBox addCustomerPrs(SBox sBox);
		//public SBox getCustomerPrsList(SBox sBox);
		//public SBox modifyCustomerPrs(SBox sBox);
		//public SBox removeCustomerPrs(SBox sBox);
		//public SBox getCustomerColNoPlanForm(SBox sBox);
		//public SBox addCustomerColNoPlan(SBox sBox);
		//public SBox getCustomerColList(SBox sBox);
		//public SBox modifyCustomerColNoPlan(SBox sBox);
		//public SBox removeCustomerCol(SBox sBox);
		//public SBox getDebentureGuideAbstract(int custId);
		//public SBox getDebntureGuideFileDownload(SBox sBox);
		//public SBox getDebentureGuideKeywordMeanByMKeyword(int dgsMnKwId);
		//public SBox getPrsFileDown(int fileSn);
		//public SBox getUsrPoInfo(SBox sBox);
		//public SBox getCustCrdHistForEW(SBox sBox);
		//public String getUsrPoInfoCheck(int sessionCompUsrId);
		//public SBox addCustomerForSB(SBox sBox);
		//public SBox modifyCustomerForSB(SBox sBox);
		//public SBox removeCustomerForSB(SBox sBox);
	
}
