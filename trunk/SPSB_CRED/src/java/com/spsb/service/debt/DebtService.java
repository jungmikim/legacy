package com.spsb.service.debt;

import com.spsb.common.collection.SBox;

/**
 * <pre>
 * 채무불이행 Service Interface Class
 * </pre>
 * 
 * @author sungrangkong
 * @since 2014.04.15
 * 
 */
public interface DebtService {

	public SBox getDebtForm(SBox sBox);

	public SBox getDebentureListOfCustomer(SBox sBox);

	public SBox addDeptApply(SBox sBox);

	public SBox getDebtInquiryList(SBox sBox);

	//public SBox getDebtOngoingList(SBox sBox);

	//public SBox getDebtCompleteList(SBox sBox);

	public SBox getDebtInquiryListForExcel(SBox sBox, String fileName);

	public SBox removeDebt(SBox sBox);

	//public SBox getDebtOngoingListForExcel(SBox sBox, String fileName);

	//public SBox getDebtCompleteInquiryListForExcel(SBox sBox, String fileName);

	public SBox getDebtInquiry(SBox sBox);

	public SBox getDebtCompleteInquiry(SBox sBox);

	//public SBox getDebtCopyAndWrite(SBox sBox);

	public SBox getDebtOngoingInquiry(SBox sBox);

	public SBox addDeptOngoingAppl(SBox sBox);

	public SBox getDebtRequireModify(SBox sBox);

	public SBox modifyDebtRequire(SBox sBox);

	public SBox getDebtRequireRelease(SBox sBox);

	public SBox releaseDebtRequire(SBox sBox);

	public SBox getDebtApplyCancel(SBox sBox);

	public SBox cancelDebtApply(SBox sBox);

	public SBox insertDebtApplication(SBox sBox);

	public SBox getUnderWayDebtTotalCount(String sessionUsrId, String sessionCompUsrId, String npmCustId);

	public SBox getDebtOngoingStatHistoryList(SBox sBox);

	public SBox getDebtConfirmation(SBox sBox);
	
	public SBox getDebtStatProcess(String debtApplId);

	public SBox getDebtOngoingInquiryForAPI(SBox sBox); //API연동업체 채무불이행 상태변경 프로세스
	
	//public SBox payDebtApplication(SBox sBox);
	
	
}
