package com.sbdebn.common.util;

import java.net.URLEncoder;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.sbdebn.common.SuperTest;
import com.spsb.common.util.CommonCrypto;
import com.spsb.common.util.CommonDesUtil;

/**
 * <pre>
 * 암호화복호화 test
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 2. 5.
 * @version 1.0
 */
public class DesTest extends SuperTest{
	
//	@Test
	public void desTest() throws Exception{
		System.out.println("===[id]==================");
		int id = 0;
		String str = Integer.toString(id);
		String encode = CommonDesUtil.encrypt(str);
		System.out.println("encode:"+encode);
		
		String decode = CommonDesUtil.decrypt(encode);
		System.out.println("decode:"+decode);
		
		System.out.println("===[mail]==================");
		String str2 = "tempuser@biz.com";
		String encode2 = CommonDesUtil.encrypt(str2);
		System.out.println("encode:"+encode2);
		String str3 = "bluesalt55";
		String encode3 = CommonDesUtil.encrypt(str3);
		System.out.println("encode3:"+encode3);
		String str4 = "@hanmail.net";
		String encode4 = CommonDesUtil.encrypt(str4);
		System.out.println("encode4:"+encode4);
		
		String decode2 = CommonDesUtil.decrypt(encode2);
		System.out.println("decode:"+decode2);
		
		System.out.println("===[sha]==================");
		String strSha = "1";
		String sha = CommonCrypto.encryptSha1(strSha);
		System.out.println("sha:"+sha);
		String encodeSha = CommonDesUtil.encrypt(strSha);
		System.out.println("encodeSha:"+encodeSha);
		
		System.out.println("===[uuid]==================");
		UUID uuid = UUID.randomUUID();
		String password = uuid.toString().replace("-", "");
		System.out.println("password:"+password);
		String encodepw = CommonDesUtil.encrypt(password);
		System.out.println("encodepw:"+encodepw);
		String decodepw = CommonDesUtil.decrypt(encodepw);
		System.out.println("decodepw:"+decodepw);
		
		String enSha = CommonCrypto.encryptSha1(password);
		System.out.println("enSha:"+enSha);
		
		System.out.println("==guest key==================");
		System.out.println("password:" + password);
		String guestKey = strSha + "," +  enSha;
		System.out.println("guestKey: " + guestKey);
		String guestKeyEncode = CommonDesUtil.encrypt(guestKey);
		System.out.println("guestKeyEncode: " + guestKeyEncode);
		String guestKeyDecode = CommonDesUtil.decrypt(guestKeyEncode);
		System.out.println("guestKeyDecode: " + guestKeyDecode);
		
		System.out.println("==guest id test key==================");
		String passEncSha = CommonCrypto.encryptSha1("5a5c3497f9d94ef08e3473e6b7f31944");
		String passEnc = CommonDesUtil.encrypt("1," + passEncSha);
		System.out.println("passEnc: " + passEnc);
		System.out.println("passURLEnc: " + URLEncoder.encode(passEnc, "UTF-8"));
		
	}
	
	@Test
	public void encodeTest() throws Exception{
//		String value = "000000008720140926153550$(주)자산유리$1118105565$4$이용현$$제조업$122895$서울특별시 은평구 서오릉로 87 $$0233333333$$자산관리자$jasanadm@js.com$0233333333$0233333334$$$$5000$20140926$4$$20140926$20141225$tboca2014092615362239669$CS1118105565";
		String value = "CS1308677406$피스코코리아뉴매틱(주)$1308677406$$야마쟈키키요야스$20141222$20151231$100$정신윤$01088719652$piscobill@pisco.co.kr";
//		BASE64Encoder encoder = new BASE64Encoder();
		byte[] b = value.toString().getBytes();
//		String encoderValue = encoder.encode(b);
//		System.out.println("BASE64Encoder: " + encoderValue);
//
//		BASE64Decoder decoder = new BASE64Decoder();
//		byte[] decoderValue = decoder.decodeBuffer(encoderValue);
//		System.out.println("BASE64Decoder: " + new String(decoderValue));
		
		byte[] encode = Base64.encodeBase64(b);
		System.out.println("Base64: " + new String(encode));
		String enco = URLEncoder.encode(new String(encode), "UTF-8");
		System.out.println(enco);
		String encoK = URLEncoder.encode(new String(encode), "EUC-KR");
		System.out.println(encoK);
		byte[] decode = Base64.decodeBase64(encode);
		System.out.println("Base64: " + new String(decode));
	}
}
