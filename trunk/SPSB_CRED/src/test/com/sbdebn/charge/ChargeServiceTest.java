package com.sbdebn.charge;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sbdebn.common.SuperTest;
import com.spsb.common.collection.SBox;
import com.spsb.service.charge.ChargeService;

/**
 * <pre>
 * 요금관리 test
 * </pre>
 * @author JUNG EUN LIM
 * @since 2014. 5. 20.
 * @version 1.0
 */
public class ChargeServiceTest extends SuperTest{

	@Autowired
	private ChargeService chargeService;
	
	
	
	/**
	 * <pre>
	 * 상품 조회 Map TEST
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 25.
	 * @version 1.0
	 * @throws Exception
	 */
//	@Test
	public void getProductMapTest() throws Exception{
		/*Map<String, SBox> map = productService.getProductMap();
		System.out.println(map.get("DEBT"));*/
	}
	
	/**
	 * <pre>
	 * 상품타입별 결제 여부 확인  Map TEST
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 25.
	 * @version 1.0
	 * @throws Exception
	 */
//	@Test
	public void getUserPoCheckPerPrdTypeMapTest() throws Exception{
		Integer compUsrId = 2;
		/*Map<String, SBox> map = chargeService.getUserPoCheckPerPrdTypeMap(compUsrId);
		System.out.println(map.get("W").get("PO_YN"));*/
	}
	
	/**
	 * <pre>
	 * 기업의 채무불이행순번에 의한 회원구매 조회TEST
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @throws Exception
	 */
//	@Test
	public void getUserPoForDebtTest() throws Exception{
		Integer compUsrId = 2;
		Integer debtApplId = 7;
		//SBox sBox = paymentService.getUserPoForDebt(compUsrId, debtApplId);
		//System.out.println(sBox);
	}
	
	/**
	 * <pre>
	 * KED 채무불이행등록수수료 조회  TEST
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 26.
	 * @version 1.0
	 * @throws Exception
	 */
//	@Test
	public void getDebtRegChargeListTest() throws Exception{
		//SBoxList<SBox> debtRegChargeList = chargeService.getDebtRegChargeList();
		/*for (SBox sBox : debtRegChargeList) {
			System.out.println(sBox);
		}*/
	}
	
	/**
	 * <pre>
	 * 기업회원순번의 구매리스트 조회  TEST
	 * </pre>
	 * @author JUNG EUN LIM
	 * @since 2014. 11. 27.
	 * @version 1.0
	 * @throws Exception
	 */
	@Test
	public void getUserPoListTest() throws Exception{
		SBox sBox = new SBox();
		sBox.set("compUsrId", 2);
		sBox.set("num", 1);
		sBox.set("rowSize", 50);
		//SBox result = chargeService.getUserPoList(sBox);
		/*SBoxList<SBox> userPoList = result.get("userPoList");
		System.out.println(userPoList.size());
		System.out.println(result.get("getTotalCount"));
		System.out.println(result.get("pcPage"));*/
	}
	
}
