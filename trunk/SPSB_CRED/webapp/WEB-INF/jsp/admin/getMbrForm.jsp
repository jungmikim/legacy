<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="compMngList" value="${result.compMngList}" />
<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%>
<html class="no-js" lang="ko">
<%--<![endif]--%>
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtForm.css">
<%-- <link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css"> --%>

<style type="text/css">
.browsing {
	font-weight: bold;
}
a{cursor:pointer;}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript"
	src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		/**
		* 연동업체 별 사업장등록 
		* @author HWAJUNG SON
		*/
		$("#selectedComp").change(function(){
				$param = {compMng : $(this).val() };
				$.ajax({
					type : "POST", 
					url : "${HOME}/admin/getCompUsrListAboutCompMng.do",
					dataType : "json",
					data : $.param($param),
					async : false,
					success : function(msg) {
						
						// 기존 option 정보 삭제
						$("#selectedCompUsr").children().remove();
						// option 정보생성
						$result = msg.compUsrList;
						$resultCnt = msg.compUsrCount;
						var tempHtml = "";
						
						if($resultCnt > 0){
							for(var k=0;k<$result.length;k++){
								tempHtml+="<option  value='"+utfDecode($result[k].COMP_USR_ID)+"' ";
									
								tempHtml+=">"+utfDecode($result[k].USR_NM) + "["+ utfDecode($result[k].USR_NO)+"]</option>";	
							}          
						}else{
							tempHtml+="<option  value=''>[선택]</option>'";
						}
						
						$("#selectedCompUsr").append(tempHtml);
					},
					error : function(xmlHttpRequest, textStatus, errorThrown){
								alert("사업장정보 조회 에러발생 [" + textStatus + "]");
							}
				});
		});

		/**
		* 회원정보 등록
		* @author HWAJUNG SON
		*/
		$("#btn_usr_regist").click(function(){
		
			$("input, textarea").each(function(){
				   $(this).val(jQuery.trim($(this).val()));
			});
			
			if($("#selectedComp").val() == ""){
				alert("회사관리코드 항목은 필수 입력 사항입니다.");
				return false;
			}
			if($("#selectedCompUsr").val() == ""){
				alert("사업장정보 항목은 필수 입력 사항입니다.");
				return false;
			}
			if ($.fn.validate($('#apply_company_info'))) {
			
				if(confirm("회원을 등록하시겠습니까?")){

					$.ajax({
						type : "POST", 
						url : "${HOME}/admin/setMbrInfo.do",
						dataType : "json",
						data:$("#form_comp_usr").serialize(),
						async : false,
						success : function(msg) {
									if(msg.REPL_CD == '00000'){
										alert("등록이 성공하였습니다.");
									}else{
										alert("등록이 실패하였습니다.");
									}
									location.href="${HOME}/admin/getMbrForm.do";
								},
						error : function(xmlHttpRequest, textStatus, errorThrown){
									alert("회원 등록 에러발생 [" + textStatus + "]");
								}
					});
				}
			
			}
		});
		
		
		/**
		 * <pre>
		 * 전송 파라미터 검증 함수
		 * </pre>
		 * @author HWAJUNG SON
		 */
		 $.fn.validate = function($validationArea){
			
			$result = true;
			 
			$formEl = $validationArea.find('input');
			$.each($formEl, function(){
				
				// 필수 공백 검증
				if($(this).hasClass('required')){
					if($.trim($(this).val())=='') {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
						return $result; 
					}
				}
				
				if($.trim($(this).val()) != '') {

					// 숫자 검증
					if($(this).hasClass('numeric')){
						if(!typeCheck('numCheck',$.trim($(this).val().replace(/-/gi, '')))) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
							return $result;
						}
						
						if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
							return $result;
						}
					}
				}
				// 반복문 정지
				if(!$result) return $result;
			});
			
			// 스크립트 정지
			if(!$result) return $result;
		 
			return $result;
			
		 };
		 
			/**
			 * <pre>
			 * 폼 전송 파라미터 검증 중 경고창 함수
			 * </pre>
			 * @author HWAJUNG SON
			 */
			$.fn.validateResultAndAlert = function($el, $message) {
				alert($message);
				$el.focus();
				return false;
			};
	});
</script>
</head>
<style>
.admin_bar {
	 padding:10px 18px 18px 20px; font-weight:bold; color:#4c91ff; height:2px;
}
.wart1 {padding: 5px 0 0px 0;line-height:1.4em;font-size:11px;}
}
</style>
<body>

	<!-- contentArea -->
	<div id="containerWrap" style="padding-top:60px;">
		<!--  left -->
		<div id="leftWrap">
		<div class="leftGnb">
				<ul>
					<li class="left_tit ltit_admin_02"><em>관리자설정</em></li>
					<li><a href="${HOME}/admin/getCompAndPrdInfoForm.do" class="lgnb off">연동업체 및 상품 등록</a></li>
					<li><a href="${HOME}/admin/getCustForCompForm.do" class="lgnb off">업체별 사업장정보 등록</a></li>
					<li><a href="${HOME}/admin/getMbrForm.do" class="lgnb on">회원정보 등록</a></li>
					<li><a href="${HOME}/admin/getGrnInfoForm.do" class="lgnb off">권한 등록</a></li>
				</ul>
			</div>
		</div>
		<div id="rightWrap">
			<%-- menu별 이미지 class sub00 --%>
			<%-- HIDDEN AREA END --%>
			<div class="right_tit ltit_admin_01">
				<em>관리자페이지</em>
			</div>
			<p class="content_tit">회원정보 등록</p>
			<div id="defaultApplyWrap">
				<form id="form_comp_usr" autocomplete="off" enctype="multipart/form-data" method="post">
					<div class="default_Apply">
						<ul id="menu_slide">
							<li class="Apply_slide"><span class="slideSpan">
							<a class="admin_bar" id="" style="text-decoration: none;">회원정보 등록</a></span>
								<ul>
									<%-- STEP01 시작 --%>
									<li class="Apply_sub">
										<div id="step1" class="ApplysubWrap">
											<c:set var="companyInfoBox" value="${result.companyInfoBox}" />
											<p class="content_dot">회원정보</p>
											<table id="apply_company_info" class="defaultTbl01_01" summary="연동업체 등록 테이블">
												<caption>회사관리코드</caption>
												<colgroup>
													<col width="18%">
													<col width="32%">
												</colgroup>
												<tbody>
													<tr>
														<td class="title">회사관리코드<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedComp" name="selectedComp" class="required" title="연동업체">
																<c:choose>
																	<c:when test="${result.compMngCount eq 0 }">
																		<option value="">[선택]</option>
																	</c:when>
																	<c:otherwise>
																		<option value="">[선택]</option>
																		<c:forEach var="compList" items="${result.compMngList}">
																			<option value="${compList.COMP_MNG_CD}" >${compList.COMP_MNG_CD}</option>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">사업장정보<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedCompUsr" name="selectedCompUsr" title="연동업체 별 사업장정보" class="required" >
																<option value="">[선택]</option>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">로그인 아이디<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<input type="text" id="login_id" name="login_id" class="tblInput required" style="width: 196px;" title="로그인 아이디" value="" maxlength="100">
														</td>
													</tr>
													<tr>
														<td class="title">로그인 패스워드<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<input type="text" id="login_pwd" name="login_pwd" class="tblInput required" style="width:196px;" title="로그인 패스워드" value="" maxlength="10">
														</td>
													</tr>
													<tr>
														<td class="title">관리자여부</td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;" name="adm_yn" id="adm_yn">
																<option value="Y">관리자</option>
																<option value="N">일반</option>
															</select>		
														</td>
													</tr>
													<tr>
														<td class="title">회원이름</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="tel_no" name="usr_nm" class="tblInput" style="width:196px;" title="회원이름" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">이메일주소</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="email" name="email" class="tblInput" style="width: 196px;" title="이메일주소" value="" maxlength="100">
															<span class="em_po">ex: admin@businesson.co.kr</span>
														</td>
													</tr>
													<tr>
														<td class="title">전화번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="tel_no" name="tel_no" class="tblInput" style="width:196px;" title="전화번호" value="" maxlength="20">
															<span class="em_po">ex: 02-1234-5678</span>
														</td>
													</tr>
													<tr>
														<td class="title">휴대폰번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="mb_no" name="mb_no" class="tblInput" style="width: 196px;" title="휴대폰번호" value="" maxlength="20">
															<span class="em_po">ex: 010-1234-5678</span>
														</td>
													</tr>
													<tr>
														<td class="title">팩스번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="fax_no" name="fax_no" class="tblInput" style="width: 196px;" title="팩스번호" value="" maxlength="20">
															<span class="em_po">ex: 02-1234-5678</span>
														</td>
													</tr>
													<tr>
														<td class="title">부서명</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="debt_nm" name="debt_nm" class="tblInput" style="width: 196px;" title="부서명" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">직위</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="job_tl_nm" name="job_tl_nm" class="tblInput" style="width: 196px;" title="직위" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">사번</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="comp_personal_id" name="comp_personal_id" class="tblInput" style="width: 196px;" title="사번" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">스마트빌 회사순번</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="sb_comp_usr_id" readonly="readonly" name="sb_comp_usr_id" class="tblInput" style="width: 196px;" title="스마트빌 회사순번" value="1" maxlength="20">
															<span class="em_po">이 컬럼은 사용되지 않습니다.</span>
														</td>
													</tr>
													<tr>
														<td class="title">스마트빌<br>로그인아이디</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="sb_login_id" name="sb_login_id" class="tblInput" style="width: 196px;" title="스마트빌 로그인아이디" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">스마트빌 회원순번</td>
														<td class="default_note2" colspan="3">
															<input type="text" readonly="readonly" id="sb_usr_id" name="sb_usr_id" class="tblInput" style="width: 196px;" title="스마트빌 회원순번" value="1" maxlength="20">
															<span class="em_po">이 컬럼은 사용되지 않습니다.</span>
														</td>
													</tr>
												</tbody>
											</table>
											<div class="wart1">
											<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
											</div>
											<div class="ApplyBtn_Add" style="padding-bottom:12px;">
												<div class="ApplyBtn_Step">
													<a id="btn_usr_regist" style="float:right;">회원 등록</a>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<%--  footer Area Start --%>
		<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
		<%-- footer Area End --%>
		</div>
</body>
</html>