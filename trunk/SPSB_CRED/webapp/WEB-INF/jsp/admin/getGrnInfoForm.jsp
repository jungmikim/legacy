<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="compMngList" value="${result.compMngList}" />
<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%>
<html class="no-js" lang="ko">
<%--<![endif]--%>
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtForm.css">
<%-- <link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css"> --%>

<style type="text/css">
.browsing {
	font-weight: bold;
}
a{cursor:pointer;}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript"
	src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		/**
		* 연동업체 별 사업장등록 
		* @author HWAJUNG SON
		*/
		$(document).on('change','#selectedComp',function() { 
				$param = {compMng : $(this).val() };
				$.ajax({
					type : "POST", 
					url : "${HOME}/admin/getCompUsrListAboutCompMng.do",
					dataType : "json",
					data : $.param($param),
					async : false,
					success : function(msg) {
						// 기존 option 정보 삭제
						$("#selectedCompUsr").children().remove();
						$("#selectedMbr").children().remove();
						// option 정보생성
						$result = msg.compUsrList;
						$resultCnt = msg.compUsrCount;
						$resultMbr = msg.mbrList;
						$resultMbrCnt = msg.mbrCount;
						var tempHtml = "";
						var tempHtml_1 = "";
						if($resultCnt > 0){
							for(var k=0;k<$result.length;k++){
								tempHtml+="<option  value='"+utfDecode($result[k].COMP_USR_ID)+"' ";
									
								tempHtml+=">"+utfDecode($result[k].USR_NM) + "["+ utfDecode($result[k].USR_NO)+"]</option>";	
							}          
						}else{
							tempHtml+="<option  value=''>[선택]</option>'";
						}
						
						if($resultMbrCnt >0){
							for(var i=0;i<$resultMbr.length;i++){
								tempHtml_1+="<option  value='"+utfDecode($resultMbr[i].MBR_ID)+"' ";
									
								tempHtml_1+=">"+utfDecode($resultMbr[i].LOGIN_ID) + "["+ utfDecode($resultMbr[i].USR_NM)+"]</option>";	
							} 
						}else{
							tempHtml_1+="<option  value=''>[선택]</option>'";
						}
						
						$("#selectedCompUsr").append(tempHtml);
						$("#selectedMbr").append(tempHtml_1);
						
						// 체크박스 변경 
						$('.prdCd').attr('checked', false);
						
						if(typeof msg.CRED1C !='undefined'){
							$("#CRED1C").attr('checked', true);
						}
						if(typeof msg.SBDE1R !='undefined'){
							$("#SBDE1R").attr('checked', true);
						}
						if(typeof msg.SBDE2R !='undefined'){
							$("#SBDE2R").attr('checked', true);
						}
						if(typeof msg.SBDE3R !='undefined'){
							$("#SBDE3R").attr('checked', true);
						}
						if(typeof msg.SBDE4R !='undefined'){
							$("#SBDE4R").attr('checked', true);
						}
						
						
						$prdList ="";
						$parent = $(".prdCd").parent();
						if($parent.find('input:checked').length > 0){
							$parent.find('input:checked').each(function(idx){
								$prdList += $(this).val();
								if(idx+1 != $parent.find('input:checked').length){
									$prdList += ',';
								}
							});
						}
						$("#prdList").val($prdList);
						
						// 버튼 수정 
						$grnCount = msg.grnCount;
						if($grnCount >0){
							$("#btn_usr_regist").text("권한 수정");
						}else{
							$("#btn_usr_regist").text("권한 등록");
						}
					},
					error : function(xmlHttpRequest, textStatus, errorThrown){
								alert("사업장정보 조회 에러발생 [" + textStatus + "]");
					}
				});
		});

		/**
		* 연동업체 별 사업장등록 
		* @author HWAJUNG SON
		*/
		$(document).on('change','#selectedCompUsr',function() {
				
				$param = {compUsr : $(this).val() , compMng : $("#selectedComp").val()};
				$.ajax({
					type : "POST", 
					url : "${HOME}/admin/getMbrListAboutCompUsr.do",
					dataType : "json",
					data : $.param($param),
					async : false,
					success : function(msg) {
						
						// 기존 option 정보 삭제
						$("#selectedMbr").children().remove();
						// option 정보생성
						$resultMbr = msg.mbrList;
						$resultMbrCnt = msg.mbrCount;
						var tempHtml_1 = "";
							
						if($resultMbrCnt >0){
							for(var i=0;i<$resultMbr.length;i++){
								tempHtml_1+="<option  value='"+utfDecode($resultMbr[i].MBR_ID)+"' ";
									
								tempHtml_1+=">"+utfDecode($resultMbr[i].LOGIN_ID) + "["+ utfDecode($resultMbr[i].USR_NM)+"]</option>";	
							} 
						}else{
							tempHtml_1+="<option  value=''>[선택]</option>'";
						}
						
						$("#selectedMbr").append(tempHtml_1);
						
						$('.prdCd').attr('checked', false);
						if(typeof msg.CRED1C !='undefined'){
							$("#CRED1C").attr('checked', true);
						}
						if(typeof msg.SBDE1R !='undefined'){
							$("#SBDE1R").attr('checked', true);
						}
						if(typeof msg.SBDE2R !='undefined'){
							$("#SBDE2R").attr('checked', true);
						}
						if(typeof msg.SBDE3R !='undefined'){
							$("#SBDE3R").attr('checked', true);
						}
						if(typeof msg.SBDE4R !='undefined'){
							$("#SBDE4R").attr('checked', true);
						}
						
						
						$prdList ="";
						$parent = $(".prdCd").parent();
						if($parent.find('input:checked').length > 0){
							$parent.find('input:checked').each(function(idx){
								$prdList += $(this).val();
								if(idx+1 != $parent.find('input:checked').length){
									$prdList += ',';
								}
							});
						}
						$("#prdList").val($prdList);
						
						// 버튼 수정 
						$grnCount = msg.grnCount;
						if($grnCount >0){
							$("#btn_usr_regist").text("권한 수정");
						}else{
							$("#btn_usr_regist").text("권한 등록");
						}
						
					},
					error : function(xmlHttpRequest, textStatus, errorThrown){
								alert("사업장정보 조회 에러발생 [" + textStatus + "]");
					}
				});
		});
		
		/**
		* 회원정보에 대한 권한 정보조회
		* @author HWAJUNG SON
		*/
		$(document).on('change','#selectedMbr',function() {
				$param = {mbrId : $(this).val()};
				$.ajax({
					type : "POST", 
					url : "${HOME}/admin/getGrnListAboutMbrList.do",
					dataType : "json",
					data : $.param($param),
					async : false,
					success : function(msg) {

						$('.prdCd').attr('checked', false);
						
						if(typeof msg.CRED1C !='undefined'){
							$("#CRED1C").attr('checked', true);
						}
						if(typeof msg.SBDE1R !='undefined'){
							$("#SBDE1R").attr('checked', true);
						}
						if(typeof msg.SBDE2R !='undefined'){
							$("#SBDE2R").attr('checked', true);
						}
						if(typeof msg.SBDE3R !='undefined'){
							$("#SBDE3R").attr('checked', true);
						}
						if(typeof msg.SBDE4R !='undefined'){
							$("#SBDE4R").attr('checked', true);
						}

						$prdList ="";
						$parent = $(".prdCd").parent();
						if($parent.find('input:checked').length > 0){
							$parent.find('input:checked').each(function(idx){
								$prdList += $(this).val();
								if(idx+1 != $parent.find('input:checked').length){
									$prdList += ',';
								}
							});
						}
						$("#prdList").val($prdList);
						
						// 버튼 수정 
						$grnCount = msg.grnCount;
						if($grnCount >0){
							$("#btn_usr_regist").text("권한 수정");
						}else{
							$("#btn_usr_regist").text("권한 등록");
						}
						
					},
					error : function(xmlHttpRequest, textStatus, errorThrown){
								alert("사업장정보 조회 에러발생 [" + textStatus + "]");
					}
				});
		});
		
		/**
		* 회원정보 등록
		* @author HWAJUNG SON
		*/
		$("#btn_usr_regist").click(function(){
			
			if($("#selectedComp").val() == ""){
				alert("회사관리코드를 선택하시길 바랍니다.");
				return false;
			}
			if($("#selectedCompUsr").val() == ""){
				alert("사업장번호를 선택하시길 바랍니다.");
				return false;
			}
			if($("#selectedMbr").val() == ""){
				alert("회원정보를 선택하시길 바랍니다.");
				return false;
			}
			if($("#prdList").val() == ""){
				alert("권한을 선택하시길 바랍니다.");
				return false;
			}
			
			if ($.fn.validate($('#apply_company_info'))) {
				var content = ""; 
				// 수정일 경우 
				if($("#btn_usr_regist").text().indexOf("등록")!=-1){
					content = "권한을 등록하시겠습니까?";	
				}else{
					content = "권한을 수정하시겠습니까?";
				}
				if(confirm(content)){
					$param = {mbrId : $("#selectedMbr").val(), prdList :$("#prdList").val()};
					$.ajax({
						type : "POST", 
						url : "${HOME}/admin/setGrnInfo.do",
						dataType : "json",
						data : $.param($param),
						async : false,
						success : function(msg) {
								if(msg.type == 'I'){
									if(msg.REPL_CD == '00000'){
										alert("등록이 성공하였습니다.");
									}else{
										alert("등록이 실패하였습니다.");
									}
								}else{
									if(msg.REPL_CD == '00000'){
										alert("수정이 성공하였습니다.");
									}else{
										alert("수정이 실패하였습니다.");
									}
								}
									
									location.href="${HOME}/admin/getGrnInfoForm.do";
								},
						error : function(xmlHttpRequest, textStatus, errorThrown){
									alert("권한 등록 에러발생 [" + textStatus + "]");
								}
					});
				}
			
			}
		});
		
		/**
		* 체크박스 리스트 연결
		*@author HWAJUNG SON
		*/
		$(".prdCd").click(function(){
						
			$prdList = "";
			$parent = $(this).parent();
			if($parent.find('input:checked').length > 0){
				$parent.find('input:checked').each(function(idx){
					$prdList += $(this).val();
					if(idx+1 != $parent.find('input:checked').length){
						$prdList += ',';
					}
				});
			}
			$("#prdList").val($prdList);
		});
		
		/**
		 * <pre>
		 * 전송 파라미터 검증 함수
		 * </pre>
		 * @author HWAJUNG SON
		 */
		 $.fn.validate = function($validationArea){
			
			$result = true;
			 
			$formEl = $validationArea.find('input');
			$.each($formEl, function(){
				
				// 필수 공백 검증
				if($(this).hasClass('required')){
					if($.trim($(this).val())=='') {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
						return $result; 
					}
				}
				
				if($.trim($(this).val()) != '') {

					// 숫자 검증
					if($(this).hasClass('numeric')){
						if(!typeCheck('numCheck',$.trim($(this).val().replace(/-/gi, '')))) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
							return $result;
						}
						
						if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
							return $result;
						}
					}
				}
				// 반복문 정지
				if(!$result) return $result;
			});
			
			// 스크립트 정지
			if(!$result) return $result;
		 
			return $result;
			
		 };
		 
			/**
			 * <pre>
			 * 폼 전송 파라미터 검증 중 경고창 함수
			 * </pre>
			 * @author HWAJUNG SON
			 */
			$.fn.validateResultAndAlert = function($el, $message) {
				alert($message);
				$el.focus();
				return false;
			};
	});
</script>
</head>
<style>
.admin_bar {
	 padding:10px 18px 18px 20px; font-weight:bold; color:#4c91ff; height:2px;
}
.wart1 {padding: 5px 0 0px 0;line-height:1.4em;font-size:11px;}
}
</style>
<body>

	<!-- contentArea -->
	<div id="containerWrap" style="padding-top:60px;">
		<!--  left -->
		<div id="leftWrap">
		<div class="leftGnb">
				<ul>
					<li class="left_tit ltit_admin_02"><em>관리자설정</em></li>
					<li><a href="${HOME}/admin/getCompAndPrdInfoForm.do" class="lgnb off">연동업체 및 상품 등록</a></li>
					<li><a href="${HOME}/admin/getCustForCompForm.do" class="lgnb off">업체별 사업장정보 등록</a></li>
					<li><a href="${HOME}/admin/getMbrForm.do" class="lgnb off">회원정보 등록</a></li>
					<li><a href="${HOME}/admin/getGrnInfoForm.do" class="lgnb on">권한 등록</a></li>
				</ul>
			</div>
		</div>
		<div id="rightWrap">
			<%-- menu별 이미지 class sub00 --%>
			<%-- HIDDEN AREA END --%>
			<div class="right_tit ltit_admin_01">
				<em>관리자페이지</em>
			</div>
			<p class="content_tit">회원정보 등록</p>
			<div id="defaultApplyWrap">
				<form id="form_comp_usr" autocomplete="off" enctype="multipart/form-data" method="post">
					<input type="hidden" name="prdList" id="prdList" value=""/>
					<div class="default_Apply">
						<ul id="menu_slide">
							<li class="Apply_slide"><span class="slideSpan">
							<a class="admin_bar" id="" style="text-decoration: none;">권한 등록</a></span>
								<ul>
									<%-- STEP01 시작 --%>
									<li class="Apply_sub">
										<div id="step1" class="ApplysubWrap">
											<c:set var="companyInfoBox" value="${result.companyInfoBox}" />
											<p class="content_dot">권한정보</p>
											<table id="apply_company_info" class="defaultTbl01_01" summary="연동업체 등록 테이블">
												<caption>회사관리코드</caption>
												<colgroup>
													<col width="18%">
													<col width="32%">
												</colgroup>
												<tbody>
													<tr>
														<td class="title">회사관리코드<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedComp" name="selectedComp" class="required" title="연동업체">
																<c:choose>
																	<c:when test="${result.compMngCount eq 0 }">
																		<option value="" >[선택]</option>
																	</c:when>
																	<c:otherwise>
																		<option value="">[선택]</option>
																		<c:forEach var="compList" items="${result.compMngList}">
																			<option value="${compList.COMP_MNG_CD}" >${compList.COMP_MNG_CD}</option>
																		</c:forEach>
																	</c:otherwise>
																</c:choose>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">사업장정보<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedCompUsr" name="selectedCompUsr" title="연동업체 별 사업장정보" class="required" >
																<option value="">[선택]</option>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">회원정보<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedMbr" name="selectedMbr" title="회원정보" class="required" >
																<option value="">[선택]</option>
															</select>			
														</td>
													</tr>
													<tr>
														<td class="title">권한코드<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															채무불이행 작성/신청 권한(CRED1C) : <input type="checkbox" size="20"  id="CRED1C"  title="채무불이행 작성/신청 권한" value="CRED1C" class="prdCd"><br>
		    												기업정보검색 권한(SBDE1R) : <input type="checkbox"  size="20" id="SBDE1R" title="기업정보검색 권한" value="SBDE1R" class="prdCd"><br>
		    												기업정보브리핑 권한(SBDE2R) : <input type="checkbox" size="20" id="SBDE2R" title="기업정보브리핑 권한" value="SBDE2R" class="prdCd"><br>
		    												조기경보상세 권한(SBDE3R) : <input type="checkbox" size="20" id="SBDE3R" title="조기경보상세 권한" value="SBDE3R" class="prdCd"><br>
															조기경보업체 등록 권한(SBDE4R) : <input type="checkbox" id="SBDE4R" size="20" title="조기경보업체 등록 권한" value="SBDE4R" class="prdCd"><br>
														</td>
													</tr>
												</tbody>
											</table>
											<div class="wart1">
											<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
											</div>
											<div class="ApplyBtn_Add" style="padding-bottom:12px;">
												<div class="ApplyBtn_Step">
													<a id="btn_usr_regist" style="float:right;">권한 등록</a>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<%--  footer Area Start --%>
		<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
		<%-- footer Area End --%>
		</div>
</body>
</html>