<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$("#fnLogin").click(function(){
		$("#findForm").attr({"action":"${HOME}/login/loginForm.do"}).submit();
	});
});
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0704"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0704">비밀번호 찾기</h2>
			<p class="under_h2_sub0704">채권관리 서비스의 비밀번호를 찾기 위해서는 본인인증이 필요합니다</p>
		</div>
		
		<div class="findbox">
			<ul class="loginlist">
				<li>회원 가입 시 선택한 본인 인증 수단을 선택하여 아이디를 찾을 수 있습니다.</li>
				<li>회원 가입 후 실명인증을 한 회원에 한하여 본인인증 정보를 통한 아이디 검색이 가능합니다.</li>
				<li>본인 인증 시 제공되는 정보는 인증 이외의 용도로 이용 또는 저장하지 않습니다.</li>
			</ul>
			<h3 class="norm"><img src="${IMG }/common/h3_member_f01.gif" alt="본인인증" /></h3>
			<p class="notice2">
				<strong>${result.USR_NM }(${result.EMAIL })</strong> 님의 본인 확인이 완료되었습니다.<br />
				아래 메일 주소로 임시 비밀번호를 보내드렸으니, <em>확인하시고 다시 로그인 해주세요.</em>
			</p>
			<form id="findForm" name="findForm" method="post">
				<input type="hidden" name="loginId" value="${result.LOGIN_ID }">
			</form>
			<div class="special">※ 임시 비밀번호는 분실할 위험이 있으니 다시 로그인 하셨을 때 꼭 비밀번호를 재설정 해주시기 바랍니다.</div>
			<div class="infochk">
				<dl>
					<dd><strong>${result.EMAIL }</strong></dd>
				</dl>
			</div>
		</div>
		<div class="btnWrap btnMember">
			<div class="btnMiddle bottomBoxSpace">
				<a href="#" class="on btn_login" id="fnLogin">로그인 하기</a>
			</div>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>