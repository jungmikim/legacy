<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	<%--local storage에서 로그인 정보 저장 여부 가져와서 설정--%>
	/* var rememberYn = window.localStorage["rememberSbDebnYn"];
	var rememberId = window.localStorage["rememberSbDebnId"];
	if(rememberYn == "Y"){
		$("#loginId").val(rememberId);
		$("#remYn").attr("checked", true);
	} */
	
	<%--개인회원 로그인 함수--%>
	$("#btn_loginUser").click(function(){
		if(!fnUserValidate()) {
			return false;
		}
		<%--local storage에서 로그인 정보 저장 --%>
		/* if($("input[id='remYn']").is(":checked")){
			localStorage[ "rememberSbDebnYn" ] = "Y";
			localStorage[ "rememberSbDebnId" ] = $("#loginId").val();
		}else{
			localStorage[ "rememberSbDebnYn" ] = "N";
			localStorage[ "rememberSbDebnId" ] = "";
		} */
		$.ajax({
			url: "${HOME}/login/loginUserAjax.do",
			type: "post",
			dataType: "json",
			data:$("#loginUserForm").serialize(),
			async: false,
			error: function(result){
				alert("실패했습니다.");
			},
			success: function(data){
				if(data.model.result.isSuccess == true){
					location.href = "${HOME}/mypage/myPageMain.do";
				}else{
					alert(data.model.result.message);
				}
			}
		}); 
    });
	
	<%--아이디에서 enter클릭시 비밀번호로 focus--%>
	$("#loginId").keypress(function(event) {
		  if ( event.which == 13 ) {
			  $("#loginPw").focus();
		   }
	});
	
	<%--비빌번호에서 enter클릭시 개인회원 로그인 함수로 go--%>
	$("#loginPw").keypress(function(event) {
		  if ( event.which == 13 ) {
			  $("#btn_loginUser").click();
		   }
	});
	
	<%--개인회원 로그인 전송 파라미터 검증 함수--%>
	function fnUserValidate() {
		var loginId = $("#loginId");
		if( $.trim(loginId.val()).length == 0 ) {
			alert("아이디를 입력해주세요.");
			loginId.focus();
			return false;
		}
		var loginPw = $("#loginPw");
		if( $.trim(loginPw.val()).length == 0 ) {
			alert(" 비밀번호를 입력해주세요.");
			loginPw.focus();
			return false;
		}
		return true;
	}
	
	/* $("#btnConfirm").click(function(){
		location.href = "${HOME}/user/userJoinEachType.do";
	}); */
	
});
	
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<c:choose>
			<c:when test="${joinType eq 'COMP1' }">
				<h3 class="norm"><img src="${IMG }/common/h3_member_s01.gif" alt="기업회원가입1 | 개인회원이 아닌 경우" /></h3>
				<div class="stepbox">
					<ul>
						<li class="s1"><p><em>STEP 01. 약관동의</em></p></li>
						<li class="s2"><p><em>STEP 02. 정보입력 (다중 선택 가능)</em></p> <i></i></li>
						<li class="s3"><p><em>STEP 03. 기업담당자 등록</em></p> <i></i></li>
						<li class="s4 on"><p><em>STEP 04. 가입완료</em></p> <i></i></li>
					</ul>
				</div>
			</c:when>
			<c:otherwise>
				<h3 class="norm"><img src="${IMG }/common/h3_member_s03.gif" alt="개인회원가입" /></h3>
				<div class="stepbox2">
					<ul>
						<li class="s1"><p><em>STEP 01. 약관동의</em></p></li>
						<li class="s2"><p><em>STEP 02. 정보입력 </em></p> <i></i></li>
						<li class="s3 on"><p><em>STEP 03. 가입완료</em></p> <i></i></li>
					</ul>
				</div>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${result eq 'SUCCESS' }">
				<div class="log_joined">
					<%-- <h3 class="norm"><img src="${IMG }/common/h3_login02.gif" alt="개인회원 로그인" /></h3> --%>
					<div class="joinedbox">
						<div class="guide">
							<p>
								축하드립니다!
								회원가입이 성공적으로 이루어졌습니다!
								로그인하셔서 채권관리 서비스를 편리하게 활용해 보세요
							</p>
							<%-- <ul class="loginlist">
								<li>아이디를 잊어버리셨나요? <span class="btn"><a href="${HOME }/user/findLoginIdForm.do">바로 가기</a></span></li>
								<li>비밀번호를 잊어버리셨나요? <span class="btn"><a href="${HOME }/user/findLoginPwForm.do">바로 가기</a></span></li>
							</ul> --%>
						</div>
						<div class="infld2">
							<form id="loginUserForm" name="loginUserForm">
								<dl>
									<dd>
										<label for="loginId" class="label">아이디</label>
										<input type="text" class="txtInput" id="loginId" name="loginId" title="로그인아이디" />
									</dd>
									<dd>	
										<label for="loginPw" class="label">비밀번호</label>
										<input type="password" class="txtInput" id="loginPw" name="loginPw" title="비밀번호" />
									</dd>	
									<dd>
										<!-- <span class="chk">
											<input type="checkbox" id="remYn" name="remYn" value="Y"/><label for="remYn">ID 저장</label>
										</span> -->
										<a href="#" class="baseBtn loginBtn" id="btn_loginUser">로그인</a>
									</dd>
								</dl>
							</form>
						</div>
					</div>
				</div>
			</c:when>
			<c:when test="${result eq 'DUPLICATION_LOGIN_ID' }">
				<div class="erp_log_joined">
					<div class="erp_joinedbox">
						<div class="guide">
								<p>
									중복된 아이디가 존재합니다.<br/>
									고객센터에 문의하여 주시기 바랍니다.
								</p>
						</div>
						<!-- <div class="btnWrap btnMember">
							<div class="btnMiddle">
								<a href="#" id="btnConfirm">확인</a>
							</div>
						</div> -->
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="erp_log_joined">
					<div class="erp_joinedbox">
						<div class="guide">
								<p>
									회원가입이  실패하였습니다.<br/>
									고객센터에 문의하여 주시기 바랍니다.
								</p>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</section>

	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>