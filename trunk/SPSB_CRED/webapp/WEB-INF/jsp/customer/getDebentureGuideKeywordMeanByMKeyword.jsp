<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<%-- 변수 설정 영역 시작 --%>
<c:set var="keywordMeanList" value="${result.keywordMeanList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>채권진단 서브키워드 의미조회</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
</head>
<body>
	<ul class="GuideHowBox" style="width:630px;margin:10px 0 10px 10px;">
		<li class="guideRight">
			<ul>
				<li>
					<c:forEach var="i" items="${keywordMeanList}">
						<div class="KeyContentBox_detail">
							<div class="txt_cont_title">${i.TL_NM}</div>
							${i.RMK_TXT}
						</div>
					</c:forEach>
				</li>
			</ul>
		</li>
	</ul>

</body>
</html>