<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 거래처 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>
<c:set var="customerList" value="${result.customerList}"/>
<c:set var="prdTypeK" value="${result.prdTypeK}"/>
<input type="hidden" name="pcPage" id="pcPage" value="${result.pcPage}"/> 
<input type="hidden" name="totalAjax" id="totalAjax" value="${result.total}"/> 
<script type="text/javascript">
$(document).ready(function(){
		/**
		* <pre>
		*   거래처 전체 선택 토글 함수
		* </pre>
		* @author KIM GA EUN
		* @since 2015. 07. 03
		**/

    $('#btn_select_all').click(function(event) {
        if(this.checked) {
            $('.chk_remove_company').each(function() { 
                this.checked = true;              
            });
        }else{
            $('.chk_remove_company').each(function() { 
                this.checked = false;                        
            });         
        }
    });	
});
</script>
<div class="tbl02Wrap">

	
	<table>
	<tbody>
		<tr>
			<th width="3%" class="first_thl">
			<c:if test="${(fn:length(customerList) ne 0) and (customerList ne null) }">
				<input type="checkbox" class="chk_remove_debt" title="선택" value="" id="btn_select_all"/>
			</c:if>
			</th>
			<th width="28%">거래처명</th>
			<th width="11.5%">사업자번호</th>
			<th width="5.5%">EW<br>등급</th>
			<th width="8%">EW<br>변동일</th>
			<c:if test="${prdTypeK eq 'K'}">	
				<th width="10%">거래처구분</th>
				<th width="27%">EW Summary</th>
			</c:if>				
			<th width="7%">전송상태</th>
			<!-- <th width="8%">등록일</th> -->
		</tr>
		<c:if test="${(fn:length(customerList) eq 0) or (customerList eq null) }">
		<tr>
			<td colspan="9" align="center" style="border-left: none;">검색 결과가 없습니다.</td>
		</tr>
		</c:if>
	<c:forEach var="customerList" items="${customerList}">
		<tr>
			<c:if test="${prdTypeK eq 'K'}">
			<td class="first_tdl" align="center" style="padding-left:2px;">
			<input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_NO}"/>
			</td>
			</c:if>
			<c:if test="${prdTypeK ne 'K'}">
			<td class="first_tdl" align="center" style="padding-left:6px;">
			<input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_NO}"/>
			</td>
			</c:if>			
			<!-- <input type="checkbox" class="chk_remove_company" title="선택" value="${customerList.CUST_ID}"/> -->
			<td style="text-align:left; padding-left:5px;">
				<a onclick="getLnkCustomer('${customerList.CUST_NO}','${customerList.CUST_SEQ}')" href="#none">
					<c:if test="${!empty customerList.CUST_NM}">
						<strong>${customerList.CUST_NM}</strong>
					</c:if>
					<c:if test="${empty customerList.CUST_NM}">
						<span style="font-size:11px; color:red;">거래처명 등록이 필요합니다.</span>
					</c:if>
				</a>
			</td>
			<td>
				<c:if test="${(!empty fn:trim(customerList.KED_CD) and customerList.KED_CD ne null)}">
					<a href="#none" onclick="getBusinessBrifReport('${customerList.KED_CD}','${customerList.CUST_NO}')">  <img src="${IMG}/common/btn_cobriefing.gif" alt="기업정보 브리핑" /><br></a>
				</c:if>
					<a onclick="getLnkCustomer('${customerList.CUST_NO}','${customerList.CUST_SEQ}')" href="#none"><strong>${fn:substring(customerList.CUST_NO,0,3)}-${fn:substring(customerList.CUST_NO,3,5)}-${fn:substring(customerList.CUST_NO,5,10)}</strong></a>
			</td>
			<td style="font-size:11px;">
				<c:if test="${!empty customerList.EW_CD}">
					<c:if test="${!empty customerList.EW_RATING}"><a href="#none" onclick="getEwRateReport('${customerList.CUST_NO}','${customerList.EW_CD}')"> <img src="${IMG}/common/grade_${ewDescMap[customerList.EW_RATING].code}.gif" alt="상세" /> </a></c:if> 	  <!-- 자체 개발 팝업 출력  -->
				</c:if>
				<c:if test="${empty customerList.EW_RATING or empty customerList.EW_CD}">없음
				</c:if>
			</td>
			<td style="font-size:11px;">
				<c:if test="${(!empty fn:trim(customerList.ORI_CUST_CRD_DT_LU) and customerList.ORI_CUST_CRD_DT_LU ne null)}">
					<c:set var="DATE" value="${customerList.ORI_CUST_CRD_DT_LU}"/>
					${fn:substring(DATE, 0, 4)}-${fn:substring(DATE, 4, 6)}-${fn:substring(DATE, 6, 8)}
				</c:if>			
				<%-- <fmt:formatDate value="${customerList.ORI_CUST_CRD_DT_LU}" pattern="yyyy-MM-dd" /> --%>
			</td>
			<c:if test="${prdTypeK eq 'K'}">
				<td style="font-size:11px;">
					${txplClsMap[customerList.TXPL_CLS].desc}
				</td>
				<td style="text-align:left; padding-left:5px; font-size:11px;">
				<%-- ${customerList.EW_SUMMARY} --%>
				<c:if test="${customerList.CR_KFB_YN eq 'Y'}">
					- 금융권단기연체_은행연체 변동<br>
				</c:if>
				<c:if test="${customerList.CR_YN eq 'Y'}">
					- 금융권단기연체_KED-일반 변동<br>
				</c:if>
				<c:if test="${customerList.CR_CARD_YN eq 'Y'}">
					- 금융권단기연체_KED-법인카드 변동<br>
				</c:if>
				<c:if test="${customerList.COMP_DEBT_YN eq 'Y'}">
					- 금융권 채무불이행정보_기업 변동<br>
				</c:if>
				<c:if test="${customerList.PER_DEBT_YN eq 'Y'}">
					- 금융권 채무불이행정보_개인 변동<br>
				</c:if>			
				<c:if test="${customerList.NEXE_PURC_ENP_YN eq 'Y'}">
					- 채무불이행_공공_기업 변동<br>
				</c:if>		
				<c:if test="${customerList.NEXE_PURC_REPER_YN eq 'Y'}">
					- 채무불이행_공공_대표자 변동<br>
				</c:if>		
				<c:if test="${customerList.SUS_ACC_YN eq 'Y'}">
					- 당좌거래정지 변동<br>
				</c:if>		
				<c:if test="${customerList.CLS_BIZ_YN eq 'Y'}">
					- 휴폐업 변동<br>
				</c:if>									
				<c:if test="${customerList.CTX_OV_YN eq 'Y'}">
					- 상거래연채 변동<br>
				</c:if>		
				<c:if test="${customerList.WORKOUT_YN eq 'Y'}">
					- 당좌거래정지 변동<br>
				</c:if>		
				<c:if test="${customerList.CLS_BIZ_YN eq 'Y'}">
					- 법정관리 변동<br>
				</c:if>		
				<c:if test="${customerList.ADMIN_MEASURE_YN eq 'Y'}">
					- 행정처분정보 변동<br>
				</c:if>	
				<c:if test="${customerList.CRD_LV_YN eq 'Y'}">
					- 신용등급 변동<br>
				</c:if>	
				<c:if test="${customerList.FIN_YN eq 'Y'}">
					- 재무 변동<br>
				</c:if>	
				<c:if test="${customerList.LWST_YN eq 'Y'}">
					- 소송 변동<br>
				</c:if>		
				<c:if test="${customerList.SC_RCD_YN eq 'Y'}">
					- 조회기록 변동<br>
				</c:if>		
				</td>
				<%-- EW모형세그먼트구분 <td style="font-size:11px;">${ewSgmtMap[customerList.EW_MODL_SGMT_CLS].desc}</td> --%>
			</c:if>					
			<td style="font-size:11px;">
				<c:if test="${customerList.TRANS eq '0'}">전송실패</c:if>
				<c:if test="${customerList.TRANS eq '1'}">전송완료</c:if>
				<c:if test="${customerList.TRANS eq '2'}">전송중</c:if>
			</td>
			<%-- <td>${customerList.OWN_NM}</td> --%><!-- TODO:  대표자  -->
			<%-- <td style="font-size:11px;"><fmt:formatDate value="${customerList.REG_DT}" pattern="yyyy-MM-dd" /></td>	 --%>
		</tr>
	</c:forEach>
	</tbody>
	</table>
</div>
