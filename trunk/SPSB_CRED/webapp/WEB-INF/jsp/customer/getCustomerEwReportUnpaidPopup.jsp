<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/popup.css" />
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<style type="text/css">
	body {overflow-x:hidden}
</style>
<script type="text/javascript">
	//취소버튼
	function setCancel(){
		window.close();
	}
	
	
	//신용정보거래처 등록
	function getCustInfo(){
		var ewRegisterUrl = "${aspKedLinkDomain}${kedEwRegisterUrl}";
		var poYnW = "${resultW.PO_YN}"; //조기경보 결제
		var regYn="${resultW.EW_RATING}"; // 조기경보 등록 거래처
		 if(poYnW=='Y'){
			setCancel();
			if((regYn=='null') || (regYn=='')){
				 window.open(ewRegisterUrl,"ewRegisterUrl","scrollbars=yes,width=800,height=600"); 
			 } else {
				 var payType = $.trim("${custResult.PAY_TYPE}");
				 var ewCdPd=null;
				 var custNoParam = "${paramVo.custNo}";
				 if(payType=='P'){
					ewCdPd = $.trim("${custResult.EW_CD}");
				 }
				 var ewRateEwDetailUrl = "${aspKedLinkDomain}"+"/ew/EWSTT03R0.do?ewcd="+ewCdPd+"&bizno="+custNoParam; 
				 window.open(ewRateEwDetailUrl,"ewRateEwDetailUrl","scrollbars=yes,width=800,height=600");  
			 }
		 } else{
 //			alert("서비스 신청 페이지로 이동합니다.");
		    var f = document.createElement('form');
		    f.setAttribute('target', '_blank');
		    f.setAttribute('method', 'post');
		    f.setAttribute('action', '${HOME}/charge/getServiceApplyForm.do');
		    document.body.appendChild(f);
		    f.submit();
		 }
	}
	
	function fnDBGS_HelpPage() {
	    var w =  620;
	    var h =  492;
	    var f =  document.frmMain;
	    var a =  "${smartBillDomain}xDti/DBGS/POPUP/EW_Help.aspx";
	    var width;
	    var height;
	    width = (window.screen.width - w) / 2
	    if (width < 0) width = 0;
	
	    height = (window.screen.height - h) / 2;
	    if (height < 0) height = 0;
	    window.open(a,"", "resizable=no,  scrollbars=yes, left=" + width + ",top=" + height + ",screenX=" + width + ",screenY=" + height + ",width=" + w + "px, height=" + h + "px");
	}
	
</script>
</head>
<body>

<div class="ewboxWarp">
	<h1><img src="${IMG}/popup/h1_0102_01.gif" alt="거래처 신용 (EW등급 정보)" /></h1>
	<div class="blank"></div>
	<p class="info_co">${result.CUST_NM} ${fn:substring(result.CUST_NO,0,3)}-${fn:substring(result.CUST_NO,3,5)}-${fn:substring(result.CUST_NO,5,10)}</p>
	<table class="tbl2_ewtit" summary="ew등급정보" style="width:350px">
		<colgroup>
			<col width="65%">
			<col width="35%">
		</colgroup>
		<tbody>
			<tr>
				<td class="info_sitTit">${ewDescMap[result.EW_RATING].desc}</td>
				<td style="text-align:right"><div class="ewhelp_btnR"><a href="#" onclick="fnDBGS_HelpPage()"><img src="${IMG}/popup/btn_ewhelp.gif" /></a></div></td>
			</tr>
			<tr>
				<c:choose>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '정상'}">
						<td colspan="2" class="info_sitTxt">신용 상태에 문제 발생 없음</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '관심'}">
						<td colspan="2" class="info_sitTxt">신용 상태의 하락 가능성이 정상 기업에 비해 상대적으로 높아 변화 추이에 대한 지속적 관찰 필요</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰1'}">
						<td colspan="2" class="info_sitTxt">신용상태에 문제가 발생하여, 거래에 주의가 필요</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰2'}">
						<td colspan="2" class="info_sitTxt">신용상태가 심각한 수준에 근접하여 거래 위험이 높음</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰3'}">
						<td colspan="2" class="info_sitTxt">신용상태가 심각한 상태로 금융기관 거래 불가능 수준</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '부도'}">
						<td colspan="2" class="info_sitTxt">당좌거래 정지, 어음수표 부도</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '휴업'}">
						<td colspan="2" class="info_sitTxt">사업장이 휴업 상태인 경우</td>
					</c:when>
					<c:when test="${ewDescMap[result.EW_RATING].desc eq '폐업'}">
						<td colspan="2" class="info_sitTxt">사업장이 폐업 상태인 경우</td>
					</c:when>
					<c:otherwise>
						<td colspan="2" class="info_sitTxt">신용 상태에 문제 발생 없음</td>
					</c:otherwise>
				</c:choose>
			</tr>
		</tbody>
	</table>
	
	<!--// 등급표시 이미지 //-->
	<c:choose>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '정상'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv01.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '관심'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv02.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰1'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv03.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰2'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv04.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '관찰3'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv05.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '부도'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv06.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '휴업'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv07.gif" /></div>
		</c:when>
		<c:when test="${ewDescMap[result.EW_RATING].desc eq '폐업'}">
			<div id="ewlv"><img src="${IMG}/popup/ewlv08.gif" /></div>
		</c:when>
		<c:otherwise>
			<div id="ewlv"><img src="${IMG}/popup/ewlv01.gif" /></div>
		</c:otherwise>
	</c:choose>
	
	
	<table class="tbl2_box" summary="신규 요청 권한설정" style="width:350px">
	<colgroup>
		<col width="65%">
		<col width="35%">
	</colgroup>
	<tbody>

			
			<c:choose>
				<c:when test="${resultW.PO_YN eq 'Y'}">
					<tr>
						<td>금융권단기연체 (은행연)</td>
						<td rowspan="13"  style="text-align: center; vertical-align: middle">
							<c:choose>
								<c:when test="${(resultW.EW_RATING eq null) || (resultW.EW_RATING eq '')  }">
									<a href="javascript:getCustInfo()">변동내역(EW 정보) <br>상세보기 위하여 <br>EW 거래처 등록하기<br>[클릭]</a>
								</c:when>
								<c:otherwise>
									<a href="javascript:getCustInfo()">변동내역(EW 정보) <br>상세보기[클릭]</a>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<td>금융권단기연체 (일반)</td>
					</tr>
					<tr>
						<td>금융권단기연체 (법인카드)</td>
					</tr>
					<tr>
						<td>금융권 채무불이행</td>
					</tr>
					<tr>
						<td>당좌거래정지상태</td>
					</tr>
					<tr>
						<td>휴폐업상태</td>
					</tr>
					<tr>
						<td>상거래연체정보</td>
					</tr>
					<tr>
						<td>법정관리상태</td>
					</tr>
					<tr>
						<td>행정처분정보</td>
					</tr>
					<tr>
						<td>신용등급</td>
					</tr>
					<tr>
						<td>소송상태</td>
					</tr>
					<tr>
						<td>재무상태</td>
					</tr>
					<iframe src="${kedUrl}"  height="100%" style="display:none;"  width="100%" name="test" ></iframe>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			
			
						
		</tbody></table>
			<c:choose>
				<c:when test="${resultW.PO_YN eq 'Y'}">
					<c:choose>
						<c:when test="${(resultW.EW_RATING eq null) || (resultW.EW_RATING eq '')  }">
							<p class="info_warn">※ 상기 회사는 조기경보 거래처로 등록되지 않은 거래처입니다. 아래 바로가기 버튼 클릭 후, 거래처 등록 시, 신용도(EW등급)에 대한 상세정보를 확인하실 수 있습니다.</p>
							<div id="ewbox_go">
								<a href="javascript:getCustInfo()"><img src="${IMG}/popup/btn_ewgo2.gif" alt="바로가기"></a>
							</div>
						</c:when>
						<c:otherwise>
							<p class="info_warn">※ 이미 고객님의 조기경보 거래처로 등록된 업체입니다.</p>
							<div id="ewbox_go">
								<a href="javascript:getCustInfo()"><img src="${IMG}/popup/btn_ewgo1.gif" alt="바로가기"></a>
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<p class="info_ewinfo">조기경보서비스를 신청하시면, 아래와 같이 거래처의 신용등급 변동에 대한 상세한 내용을 확인하실 수 있습니다.</p>
						<!--// 예제 이미지 //-->
						<div id="ewlv"><img src="${IMG}/popup/ewex.gif" /></div>
						<p class="info_ewinfo">이 외에도 금융권 채무불이행정보 대표자, 당좌거래정지정보, 휴폐업정보, 비금융권 채무불이행 정보, 금융권 단기 연체(은행연합회/관련인, 행정처분정보, 소송정보, 신용등급 변동, 재무상황의 내용을 확인할 수 있습니다. </p>
						<div id="ewbox_go">
							<a href="javascript:getCustInfo()"><img src="${IMG}/popup/btn_ewgo3.gif" alt="바로가기"></a>
						</div>
				</c:otherwise>
			</c:choose>
</div>

</body>
</html>