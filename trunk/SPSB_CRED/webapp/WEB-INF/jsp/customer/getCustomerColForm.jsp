<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 수금 조회 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="custPln" value="${result.custPln}" />
<c:set var="custCol" value="${result.custCol}" />
<c:set var="debnList" value="${result.debnList}" />
<c:set var="commonCode" value="${result.commonCode}" />
<c:set var="bondMbrList" value="${result.bondMbrList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/popupCol.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerPlan.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	/**
	 * <pre>
	 *   체크박스 클릭시 수금 금액 활성화 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25 
	 */
	 $('.chkDebnId').live({
		 click : function(e){
			 if($(this).is(':checked')) {
				if($(this).parent().parent().find('td.last').find('input[type="text"]').length == 0) {
					$(this).parent().parent().find('td.last')
					.append(
						'<input type="text" id="col_amt_' + $(this).val() + '" name="colAmt" ' +
							    'class="txtInput colAmt numericMoney required" style="width:78px" title="수금 금액" ' + 
							    'value="' + $.trim($(this).parent().siblings('.debnPlnColAmt').text()) + '" maxlength="12">'		 
					); 
				}
				 
			 } else {
				 $(this).parent().parent().find('td.last').empty()
				 .append(
						'<input type="hidden" id="col_amt_' + $(this).val() + '" name="colAmt" ' +
							    'class="txtInput colAmt numericMoney required" ' + 
							    'value="0" maxlength="12">'		 
					); ;
			 }
			 
			<%-- 선택 채권 건수, 선택채권 미수금총액 --%>
		 	$.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T');
		 	$('#selected_debn').text($('.chkDebnId:checked').length + '건');
		 	
		 	<%-- 선택 채권 수금 예정 금액 --%>
		 	$('#tot_pln_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.debnPlnColAmt'), $('#selected_pln_col_amt'), '원', 'T'));
		 	
		 	<%-- 선택 채권 실수금 총액 --%>
		 	$('#tot_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), $('#selected_col_amt'), '원', 'V'));
		 	
		 	<%-- 선택 채권 미수 잔액 --%>
		 	$totColAmt = $.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), '', '', 'V');
		 	$totRmnAmt = $.fn.selectedTargetCalc($('.chkDebnId').parent().parent().find('.rmnAmt'), '', '', 'T');
			$('#tot_rmn_amt').val(parseInt($totRmnAmt, 10) - parseInt($totColAmt, 10));
			$('#selected_rmn_amt').text(addComma(parseInt($totRmnAmt, 10) - parseInt($totColAmt, 10)) + '원');
			
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		 }
	 });
	 
 	/**
	 * <pre>
	 *   수금 미수 금액과 비교하는 로직
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25 
	 */
	 $('.colAmt').live({
		focus: function(e) {
			$(this).val(replaceAll($(this).val(), ',', ''));
		},
		focusout: function(e) {
			
			<%-- 공백 검증 --%>
			if($.trim($(this).val()) == '') {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '를 입력해주세요.');
			}
			
			<%-- 숫자 검증 --%>
			if(!typeCheck('numCheck',$.trim($(this).val()))) {
				return $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			}
			
			<%-- 입력값 대소비교 --%>
			$colAmt = parseInt(($(this).val().replace(/[^0-9]/g, '')), 10);
			$plnColAmt = parseInt(($(this).parent().siblings('.debnPlnColAmt').text().replace(/[^0-9]/g, '')), 10);
			if(!$.fn.compareValue($colAmt, $plnColAmt, 'LE')) {
				return $.fn.validateResultAndAlert($(this), '수금금액은 수금예정금액이하로 입력되어야 합니다.');
			}
			
			<%-- 입력한 값 초기화 --%>
			$(this).val(addComma($colAmt));
			
			<%-- 선택 채권 건수, 선택채권 미수금총액 --%>
		 	$.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.rmnAmt'), $('#selected_tot_unpd_amt'), '원', 'T');
		 	$('#selected_debn').text($('.chkDebnId:checked').length + '건');
		 	
		 	<%-- 선택 채권 수금 예정 금액 --%>
		 	$('#tot_pln_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.debnPlnColAmt'), $('#selected_pln_col_amt'), '원', 'T'));
		 	
		 	<%-- 선택 채권 실수금 총액 --%>
		 	$('#tot_col_amt').val($.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), $('#selected_col_amt'), '원', 'V'));
		 	
		 	<%-- 선택 채권 미수 잔액 --%>
		 	$totColAmt = $.fn.selectedTargetCalc($('.chkDebnId:checked').parent().parent().find('.colAmt'), '', '', 'V');
		 	$totRmnAmt = $.fn.selectedTargetCalc($('.chkDebnId').parent().parent().find('.rmnAmt'), '', '', 'T');
			$('#tot_rmn_amt').val(parseInt($totRmnAmt, 10) - parseInt($totColAmt, 10));
			$('#selected_rmn_amt').text(addComma(parseInt($totRmnAmt, 10) - parseInt($totColAmt, 10)) + '원');
			
			<%-- 모두 선택 체크 상태 현재 선택에 따라 변경 --%>
			if($('.chkDebnId:not(:disabled):not(:checked)').length == 0) {
				$('#all_check').attr('checked', true);
			} else {
				$('#all_check').attr('checked', false);
			}
		}
	 });
	
	/**
	 * <pre>
	 *   체크박스 모두 선택 토글 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#all_check').live({
		click : function(){
			if($(this).is(':checked')) {
				$('.chkDebnId:not(:disabled)').attr('checked', false).trigger('click');
			} else {
				$('.chkDebnId:not(:disabled)').attr('checked', true).trigger('click');
			}
		}
	});
	 
	/**
	 * <pre>
	 *   검색 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_pln_search').live({
		click : function(e){
			$custId = $('#cust_id').val();
			$orderCondition = $('#order_condition').val();
			$orderType = $('#order_type').val();
			$bondMbrId = $('#bond_mbr_id').val();
			$custPlnSn = $('#cust_pln_sn').val();
			$.fn.getDebnListForCustomerCol($custId, $orderCondition, $orderType, $bondMbrId, $custPlnSn);
		}
	});
	 
	/**
	 * <pre>
	 *   부분수금 저장[추가]버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25 
	 */
	$('#btn_section_col_save').live({
		click : function(e){
			if($('.chkDebnId:not(:disabled):checked').length == 0) {
				alert('채권을 하나 이상 선택해주세요.');
				return false;
			}
			
			if(!$.fn.validate($('.chkDebnId:not(:disabled):checked').parent().parent().find('.colAmt'))) return false;
			
			$.fn.addCustomerCol('S');
		}
	});
	 
	/**
	 * <pre>
	 *   완전수금 저장[추가]버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25 
	 */
	$('#btn_completed_col_save').live({
		click : function(e){
			$.fn.addCustomerCol('C');
		}
	});
	 
	/**
	 * <pre>
	 *   취소 버튼 클릭시 팝업 닫기
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 25 
	 */
	$('#btn_customer_col_close').live({
		click : function(e){
			self.close();
		}
	});
	
	<%-- 부분 수금일 경우 페이지 로딩시 모두 선택 --%>
	if(('${sBox.pageType}' == 'S') && ('${result.customerColCount}' == 0)) {
		$('#all_check').trigger('click');
	}
	 
});

<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%> 

/**
 * <pre> 
 *   수금계획 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 19
 * @param $listEl : 검증할 엘리먼트 리스트
 *
 */
$.fn.validate = function($listEl) {
	
	$result = true;
	$zeroFlag = false;
	
	$.each($listEl, function(){
		
		$colAmt = $.trim(replaceAll($(this).val(), ',', ''));
		
		<%-- 공백 검증 --%>
		if($colAmt == '') {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '를 입력해주세요.');
			return $result;
		}
		
		<%-- 숫자 검증 --%>
		if(!typeCheck('numCheck',$colAmt)) {
			$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
			return $result;
		}
		
		<%-- 입력값 대소비교 --%>
		$plnColAmt = parseInt(($(this).parent().siblings('.debnPlnColAmt').text().replace(/[^0-9]/g, '')), 10);
		$colAmt = parseInt($colAmt);
		if(!$.fn.compareValue($colAmt, $plnColAmt, 'LE')) {
			$result = $.fn.validateResultAndAlert($(this), '수금예정금액은 수금예정금액보다 적어야 합니다.');
			return $result;
		}	
		
		<%-- 하나라도 0 이상의 값이 있으면 통과 --%>
		if(parseInt($colAmt, 10) > 0) {
			$zeroFlag = true;
		}
		
	});
	
	<%-- 0값 검증 --%>
	if(!$zeroFlag) {
		$result = $.fn.validateResultAndAlert('', '수금금액은 모두 0이 될 수 없습니다. ');
		return $result;
	}
	 
	return $result;
};

/**
 * <pre>
 *   경고창 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
 */
 $.fn.validateResultAndAlert = function($el, $message){
	alert($message);
	return false;
 }; 

/**
 * <pre> 
 *   <li> 태그 리스트 백그라운드 CSS 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 *
 */
 $.fn.setBackgroundEvenAndOdd = function($targetEl, $subEl, $evenColor, $oddColor) {
	 $targetEl.find($subEl + ':even').css('background-color', $evenColor);
	 $targetEl.find($subEl + ':odd').css('background-color', $oddColor);
 };
		
 /**
  * <pre> 
  *    대소 비교 함수
  * </pre>
  * @author Jong Pil Kim
  * @since 2014. 04. 18
  * @param $num1 : 비교할 값 1, $num2 : 비교할 값 2, $operand : 연산자[LE,GE,LT,GT]
  */
  $.fn.compareValue = function($num1, $num2, $operand) {
 	 if($operand == 'LE') {
 		 return ($num1 <= $num2);
 	 } else if($operand == 'GE') {
 		return ($num1 >= $num2);
 	 } else if($operand == 'LT') {
 		return ($num1 < $num2);
 	 } else if($operand == 'GT') {
 		return ($num1 > $num2);
 	 }  
  };

/**
 * <pre>
 *   선택한 값 자동 계산 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 18
 * @param $listEl : 계산할 엘리먼트, $targetEl : 결과 세팅 엘리먼트,  
 *        $suffix : 결과 뒤에 추가할 메세지, $valueType : 함수 사용 조건[V, T] 
 */
 $.fn.selectedTargetCalc = function($listEl, $targetEl, $suffix, $valueType){
	 $result = 0;
	 
	 $listEl.each(function() {
		$value = '';
		switch($valueType) {
			case 'V' :
				$value = $(this).val();
				break;
			case 'T' :
				$value = $(this).text();
				break;
		}  
		
		$result += parseInt(($value.replace(/[^0-9]/g, '')), 10);
	 });
	 
	 if($targetEl != '') {
		 $targetEl.html(addComma($result) + $suffix); 
	 }
	 
	 return $result;
 };
 
/**
 * <pre> 
 *   파라미터 구성 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 22
 *
 * @param $targetEl : 수집 대상 엘리먼트
 * @param $selector : 수집 대상 셀렉터(클래스)
 * @param $paramEl : 파라미터 수집 대상 객체
 * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
 * @param $dataSplitStr : 구분자[데이터]
 * @param $rowSplitStr : 구분자[row]
 */
$.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {
	
$result = '';

$.each($targetEl, function() {
	if($(this).hasClass($selector.toString())) {
		$tmpStr = '';
		for(var i = 0 ; i < $paramEl.length; i++) {
			if($tmpStr != '') {
				$tmpStr += $dataSplitStr;
			}
			$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
		}		
		
		if($result != '') {
			$result += $rowSplitStr;
		}
		$result += $tmpStr;
	}
});
	
return $result;
};

/**
 * <pre> 
 *   리스트가 없을 경우
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 17
 * @param $target : 리스트 구성 영역 Element
 * @param $noDataEl : 리스트를 구성할 Data
 */
$.fn.reloadNoDataList = function($target, $noDataEl) {
	 $htmlStr = '';
	 $target.empty().append($noDataEl);
	 $.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
};
 
<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>
 
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>
/**
 * <pre> 
 *  채권 리스트 재로딩 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 25
 *
 */
 $.fn.getDebnListForCustomerCol = function($custId, $orderCondition, $orderType, $bondMbrId, $custPlnSn) {
	 
	 $paramObj = {
			 custId : $custId,
			 orderCondition : $orderCondition,
			 orderType : $orderType,
			 bondMbrId : $bondMbrId,
			 custPlnSn : $custPlnSn
	 };
	 
	 $param = $.param($paramObj);
	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/getDebnListForCustomerCol.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	
			    	if($debnList.length > 0) {
			    		// 채권 조회 결과 리스트 조회
					    $.fn.reloadDebnList($('#debn_list_content tbody'), $debnList);	
			    	} else {			    		
			    		$htmlStr = (('${sBox.pageType}' == 'S') && ($result.customerColCount == 0)) ? 
			    				'<tr><td colspan="7"><div class="noSearch">결과가 없습니다.</div></td></tr>': 
			    				'<tr><td colspan="6"><div class="noSearch">결과가 없습니다.</div></td></tr>';
			    		
			    		$.fn.reloadNoDataList($('#debn_list_content tbody'), $htmlStr);
			    		$('#all_check').attr('checked', true).trigger('click');
			    	}
			    	
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("채권 조회 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
 
/**
 * <pre> 
 *   채권 리스트를 재구성 하는 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 25
 *
 * @param $target : 리스트 구성 영역 Element
 * @param $listEl : 리스트를 구성할 Data
 */
$.fn.reloadDebnList = function($target, $listEl) {
	$htmlStr = '';
 	
 	$.each($listEl, function() {
		$htmlStr +=
			'<tr>' + 
				'<td class="checkDebn">' + 
					'<input type="checkbox" class="chkDebnId" id="chk_' + this.DEBN_ID + '" value="' + this.DEBN_ID + '">' +
				'</td>' +
				'<td class="bondMbrNm">' +
					this.BOND_MBR_NM +
					'<input type="hidden" class="debnId" id="debn_id_' + this.DEBN_ID + '" value="' + this.DEBN_ID + '">' +
				'</td>' +
				'<td class="billDt">' + this.BILL_DT +'</td>' +
				'<td class="list_right sumAmt">' + this.SUM_AMT + '</td>' +
				'<td class="list_right rmnAmt">' + this.RMN_AMT + '</td>' +
				'<td class="list_right debnPlnColAmt">' + 
					this.PLN_COL_AMT_COMMA +
				'</td>' +
				'<td class="list_left last debnPlnColAmtInput">' +
					'<input type="text" id="col_amt_' + this.DEBN_ID + '" name="colAmt" class="txtInput colAmt numericMoney required" style="width:78px" title="수금 금액" value="' + this.PLN_COL_AMT_COMMA + '" maxlength="12">' +
				'</td>' +
			'</tr>';	
 	});
 	
 	$('#all_check').attr('checked', false);
 	$target.empty().append($htmlStr).find('.chkDebnId').trigger('click');
 	$.fn.setBackgroundEvenAndOdd($target, 'tr', '#f6f6f6', '#ffffff');
 	
 };
 
/**
 * <pre> 
 *   거래처 수금 입력 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 25
 * @param insertParam : 채권 수금 예정 Insert 파라미터, custId : 거래처 순번, colDt : 수금일자
 *		  colAmt : 수금액, colType : 수금방식, debnCnt : 채권 건수, rmkTxt : 비고, 
          custPlnSn : 수금예정순번, statType : 거래처 수금 계획 상태, colStatType : 거래처 수금 완료 상태
 *		  
 * $paramType[S, C] : S - 부분수금, C - 완전수금
 */
 $.fn.addCustomerCol = function($paramType) {

  	 $paramEl = new Array();
  	 $replaceEl = new Array();
  	 $paramEl.push('.debnId');
  	 $replaceEl.push('');
  	 $paramEl.push('.colAmt');
  	 $replaceEl.push(/[^0-9]/g);
  	 
  	 $insertParam = ($paramType == 'C') 
  	 				? $.fn.makeParam($('.debnId'), 'debnId', $paramEl, $replaceEl, ':', '|') 
  			 		: $.fn.makeParam($('.chkDebnId'), 'chkDebnId', $paramEl, $replaceEl, ':', '|');
  	 
  	 $paramObj = {
    			insertParam : $insertParam,
    			custId : $('#cust_id').val(),
    			colDt : $('#col_dt').val(),
    			colAmt : $('#tot_col_amt').val(),
    			rmkTxt : $('#rmk_txt').val(),
    			colType : $('#col_type').val(),
    			debnCnt : $('#debn_cnt').val(),
    			custPlnSn : $('#cust_pln_sn').val(),
    			plnStatType : ($('#tot_pln_col_amt').attr('data-pln-col-amt') == $('#tot_col_amt').val()) ? 'C' : 'S',
    			totRmnAmt : $('#tot_rmn_amt').val(),
    			colStatType : 'C'
  	 };
  	 
   	 $param = $.param($paramObj);
   	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/addCustomerCol.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('거래처 수금 등록이 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("거래처 수금 등록 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };

  
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
</script>
</head>
<body>

<div class="pWrap">
	<header>
		<c:choose>
			<c:when test="${sBox.pageType eq 'S' and result.customerColCount eq 0}">
				<h1><img src="${IMG}/customer/h3_section_complete.gif" alt="부분수금" /></h1>
			</c:when>
			<c:when test="${sBox.pageType eq 'C' and result.customerColCount eq 0}">
				<h1><img src="${IMG}/customer/h3_complete.gif" alt="완전수금" /></h1>
			</c:when>
			<c:otherwise>
				<h1><img src="${IMG}/customer/h3_col_plan_detail.gif" alt="수금 상세보기" /></h1>
			</c:otherwise>
		</c:choose>
	</header>
	
	<form name="customerColForm" id="customer_col_form" autocomplete="off">
		<%-- INPUT HIDDEN AREA --%>
		<input type="hidden" name="custColSn" id="cust_col_sn" value="${sBox.custColSn}" />
		<input type="hidden" name="custPlnSn" id="cust_pln_sn" value="${sBox.custPlnSn}" />
		<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}" />
		
		<c:if test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
			<div class="pContent">
				<div class="pSearch_list">
					<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자, 금액별로 정렬">
						<c:forEach var="orderConditionList" items="${commonCode.colOrderConditionList}">
							<option value="${orderConditionList.KEY}" <c:if test="${orderConditionList.KEY eq sBox.orderCondition}">selected="selected"</c:if> >${orderConditionList.VALUE}</option>
						</c:forEach>
					</select>
					<select id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
						<c:forEach var="orderTypeList" items="${commonCode.orderTypeList}">
							<option value="${orderTypeList.KEY}" <c:if test="${orderTypeList.KEY eq sBox.orderType}">selected="selected"</c:if> >${orderTypeList.VALUE}</option>
						</c:forEach>
					</select>
					<label for="listNo"></label>
					<select name="bondMbrId" id="bond_mbr_id" class="listOrderOption">
						<option value="" <c:if test="${sBox.bondMbrId eq null || sBox.bondMbrId eq '' }">selected="selected"</c:if> >전체</option>
						<c:forEach var="bondMbrList" items="${bondMbrList}">
							<option value="${bondMbrList.MBR_ID}" <c:if test="${bondMbrList.MBR_USR_ID eq null or bondMbrList.MBR_USR_ID eq ''}">class="mem_type_N"</c:if> <c:if test="${bondMbrList.MBR_ID eq sBox.bondMbrId}">selected="selected"</c:if> >${bondMbrList.USR_NM}</option>
						</c:forEach>
					</select>
					<a href="#none" class="pBtn_Search" id="btn_pln_search">검색</a>
				</div>
			</div>
		</c:if>

		<div class="pTableWrap">
			<div class="pTableTop">
				<table summary="수금 작성" class="pTableTitle debnListTitle" id="debn_list_title">
				<caption>수금 작성</caption>
				<colgroup>
					<c:choose>
						<c:when test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
							<col width="25px">
							<col width="74px">
						</c:when>
						<c:otherwise>
							<col width="99px">
						</c:otherwise>
					</c:choose>
					<col width="120px">
					<col width="105px">
					<col width="105px">
					<col width="105px">
					<col width="105px">
				</colgroup>
				<thead>
					<tr>
						<c:if test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
							<th scope="col">
								<input type="checkbox" id="all_check">
							</th>
						</c:if>
						<th scope="col">채권담당자</th>
						<th scope="col">세금계산서작성일</th>
						<th scope="col">합계금액</th>
						<th scope="col">미수금액</th>
						<th scope="col">수금예정금액</th>
						<th scope="col" class="last">수금금액</th>					
					</tr>
				</thead>
				</table>
			</div>
			<div class="pTableContent">
				<table summary="수금업무 작성" class="pTable debnListContent" id="debn_list_content">
				<caption>수금업무 작성</caption>
				<colgroup>
					<c:choose>
						<c:when test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
							<col width="25px">
							<col width="74px">
						</c:when>
						<c:otherwise>
							<col width="99px">
						</c:otherwise>
					</c:choose>
					<col width="120px">
					<col width="105px">
					<col width="105px">
					<col width="105px">
					<col width="105px">
				</colgroup>
				<tbody>
					<%-- 미수총금액, 실수금총액 합계 변수 --%>
					<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="0" groupingUsed="true"/>
					<fmt:formatNumber var="totColAmt" type="NUMBER" value="0" groupingUsed="true"/>
					
					<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
					<c:choose>
						<c:when test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
							<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
								<tr><td colspan="7"><div class="noSearch">결과가 없습니다.</div></td></tr>						
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
								<tr><td colspan="6"><div class="noSearch">결과가 없습니다.</div></td></tr>						
							</c:if>
						</c:otherwise>
					</c:choose>
					<c:forEach var="debnList" items="${debnList}">
						<tr>
							<c:if test="${sBox.pageType eq 'S' and result.customerColCount eq 0 }">
								<td class="checkDebn">
									<input type="checkbox" class="chkDebnId" id="chk_${debnList.DEBN_ID}" value="${debnList.DEBN_ID}">
								</td>
							</c:if>
							<td class="bondMbrNm">
								${debnList.BOND_MBR_NM}
								<input type="hidden" class="debnId" id="debn_id_${debnList.DEBN_ID}" value="${debnList.DEBN_ID}">
							</td>
							<td class="billDt">${debnList.BILL_DT}</td>
							<td class="list_right sumAmt">${debnList.SUM_AMT}</td>
							<td class="list_right rmnAmt">${debnList.RMN_AMT}</td>
							<td class="list_right debnPlnColAmt">
								<c:choose>
									<c:when test="${result.customerColCount eq 0}">
										${debnList.PLN_COL_AMT_COMMA}
									</c:when>
									<c:otherwise>
										${debnList.PLN_COL_AMT_COMMA}
									</c:otherwise>
								</c:choose>
							</td>
							<td class="list_left last debnPlnColAmtInput">
								<%-- 완전 수금을 요청했을 경우 --%>
								<c:if test="${sBox.pageType eq 'C' and result.customerColCount eq 0}">
									<p class="noBorder">${debnList.PLN_COL_AMT_COMMA}</p>
									<input type="hidden" id="col_amt_${debnList.DEBN_ID}" name="colAmt" class="txtInput colAmt numericMoney required" style="width:78px" title="수금 금액" value="${debnList.PLN_COL_AMT_COMMA}" maxlength="12">
								</c:if>
								<%-- 부분수금/완전수금을 된 수금계획을 조회할 경우 --%>
								<c:if test="${result.customerColCount ne 0}">
									<p class="noBorder">${debnList.COL_AMT}</p>
								</c:if>
							</td>
						</tr>
						<%-- 수금예정금액, 미수총금액, 실수금총액 합계 연산 --%>
						<fmt:formatNumber var="rmnAmt" type="NUMBER" value="${fn:replace(debnList.RMN_AMT, ',', '')}" pattern="#"/>
						<fmt:formatNumber var="totUnpdAmt" type="NUMBER" value="${totUnpdAmt + rmnAmt }" pattern="#"/>
						<fmt:formatNumber var="totColAmt" type="NUMBER" value="${totColAmt + debnList.PLN_COL_AMT }" pattern="#"/>
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		
		<div class="pSumWrap">
			<table class="pSumTable">
			<caption>통계</caption>
			<colgroup>
				<col width="50%">
				<col width="50%">
			</colgroup>
			<tbody>
				<tr>
					<td>
						선택채권 : 				
						<c:choose>
							<c:when test="${result.customerColCount eq 0}">
								<p id="selected_debn">
									${fn:length(result.debnList)} 건
								</p>
								<input type="hidden" name="debnCnt" id="debn_cnt" value="${fn:length(result.debnList)}" />
							</c:when>
							<c:otherwise>
								<p id="selected_debn">
								${custCol.DEBN_CNT}건
								</p>
								<input type="hidden" name="debnCnt" id="debn_cnt" value="${custCol.DEBN_CNT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						선택채권 미수금총액 : 
						<p id="selected_tot_unpd_amt">
							<fmt:formatNumber var="totUnpdAmtComma" type="NUMBER" value="${totUnpdAmt}" groupingUsed="true"/> 
							${totUnpdAmtComma}원
						</p>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		
		<div class="pInputWrap">
			<table class="pInputTable">
			<caption>수금업무 작성테이블</caption>
			<colgroup>
				<col width="105px">
				<col width="2px">
				<col width="120px">
				<col width="85px">
				<col width="2px">
				<col width="120px">
				<col width="85px">
				<col width="2px">
				<col width="120px">
			</colgroup>
			<tbody>
				<tr>
					<td><p>수금일</p></td>
					<td><p>:</p></td>
					<c:choose>
						<c:when test="${result.customerColCount eq 0}">
							<td colspan="7">
								<input type="text" name="colDt" id="col_dt" class="firstDate" title="수금일" value="${result.currentDt}" readonly="readonly">
							</td>
						</c:when>
						<c:otherwise>
							<td>
								${custCol.COL_DT}
							</td>
							<td><p>상태</p></td>
							<td><p>:</p></td>
							<td colspan="4">
								${custPln.STAT_NAME}
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="middle">
						<p>수금방식</p>
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_col_type" colspan="7">
						<c:choose>
							<c:when test="${(result.customerColCount eq 0 )}">
								<select name="colType" id="col_type">
									<c:forEach var="custColTypeList" items="${commonCode.custColTypeList}">
										<%-- 부분수금/완전수금에서는 수금방식이 대손처리는 보여주지 않아야함. --%>
										<c:if test="${custColTypeList.KEY ne 'I'}">
											<option value="${custColTypeList.KEY}" <c:if test="${custColTypeList.KEY eq sBox.custColType}">selected="selected"</c:if> >${custColTypeList.VALUE}</option>
										</c:if>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								${custCol.COL_TYPE_NAME}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td class="middle">
						<p>수금예정금액</p>
						<input type="hidden" name="totPlnColAmt" id="tot_pln_col_amt" value="${custPln.PLN_COL_AMT}" data-pln-col-amt="${custPln.PLN_COL_AMT}"/>
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_pln_col_amt">
						${custPln.PLN_COL_AMT_COMMA}원
					</td>
					<c:choose>
						<c:when test="${result.customerColCount eq 0}">
							<td class="middle">
								<p>실수금총액</p>
								<input type="hidden" name="totColAmt" id="tot_col_amt" value="${totColAmt}" />
								<fmt:formatNumber var="totColAmtComma" type="NUMBER" value="${totColAmt}" groupingUsed="true"/> 
							</td>
							<td class="middle"><p>:</p></td>
							<td class="middle" id="selected_col_amt">
								${totColAmtComma}원
							</td>
							<td class="middle">
								<p>미수잔액</p>
								<input type="hidden" name="totRmnAmt" id="tot_rmn_amt" value="${totUnpdAmt - totColAmt}" />
								<fmt:formatNumber var="totRmnAmtComma" type="NUMBER" value="${totUnpdAmt - totColAmt}" groupingUsed="true"/>
							</td>
							<td class="middle"><p>:</p></td>
							<td class="middle" id="selected_rmn_amt">
								${totRmnAmtComma}원
							</td>
						</c:when>
						<c:otherwise>
							<td class="middle" >
								<p>수금금액</p>
							</td>
							<td class="middle"><p>:</p></td>
							<td class="middle" colspan="4">${custCol.COL_AMT_COMMA}원</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="last"><p>비고</p></td>
					<td class="last"><p>:</p></td>
					<td class="last" colspan="7">
						<c:choose>
							<c:when test="${result.customerColCount eq 0}">
								<input type="text" id="rmk_txt" name="rmkTxt" class="txtInput" style="width:400px" title="비고" value="${custCol.RMK_TXT}" maxlength="200">
							</c:when>
							<c:otherwise>
								${custCol.RMK_TXT}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		<div class="pBtnWrap">
			<div class="pBtnMiddle">
				<c:choose>
					<c:when test="${sBox.pageType eq 'S' and result.customerColCount eq '0'}">
						<a href="#none" class="on" id="btn_section_col_save">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">취소</a>
					</c:when>
					<c:when test="${sBox.pageType eq 'C' and result.customerColCount eq '0'}">
						<a href="#none" class="on" id="btn_completed_col_save">저장</a>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">취소</a>
					</c:when>
					<c:otherwise>
						<a href="#none" class="btnCancel" id="btn_customer_col_close">닫기</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form>
</div>
</body>
</html>