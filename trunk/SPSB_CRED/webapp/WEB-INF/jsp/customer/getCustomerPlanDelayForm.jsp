<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 연기 상태가 아닐 경우, 경고창 띄우고 팝업 닫기 --%>
<c:if test="${result.custPln.STAT ne 'P' and result.custPln.STAT ne 'D'}">
	<script>
		alert('정상적인 접근이 아닙니다.\n페이지를 새로고침 해주세요.');
		window.open("about:blank","_self").close();
	</script>
</c:if>

<%-- 수금계획 조회 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="custPln" value="${result.custPln}" />
<c:set var="debnList" value="${result.debnList}" />
<c:set var="commonCode" value="${result.commonCode}" />
<c:set var="bondMbrList" value="${result.bondMbrList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/popupCol.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerPlan.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	/**
	 * <pre>
	 *   취소 버튼 클릭시 팝업 닫기
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 17 
	 */
	$('#btn_customer_pln_close').live({
		click : function(e){
			self.close();
		}
	});
	 
	/**
	 * <pre>
	 *   저장[추가] 버튼 클릭
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 18 
	 */
	$('#btn_customer_pln_delay').live({
		click : function(e){
			$.fn.delayCustomerPlan();
		}
	});
	 
	/**
     * <pre>
     *    연기 된 수금계획으로 이동
     * </pre>
     * @author Jong Pil Kim
     * @since 2014. 04. 23.
     */
  	 $('.lnkMove').live({
  		 click : function(e) {
  			  			
  			<%-- 파라미터 세팅 --%>
   			$custPlnSn = $(this).attr('id').replace(/[^0-9]/g, '');
   			$custId = $('#cust_id').val();
   			$url = '';
   			
   			// 계획중
   			if($(this).hasClass('STAT_P')) {
   				$url = '${HOME}/customer/getCustomerPlanForm.do?';
   			} 
   			// 연기
   			else if($(this).hasClass('STAT_D')) {
   				$url = '${HOME}/customer/getCustomerPlanDelayDetail.do?';
   			}
   			// 부분수금
   			else if($(this).hasClass('STAT_S')) {
   				$url = '${HOME}/customer/getCustomerColForm.do?';
   				$url += '&pageType=S';
   			}
   			// 완전수금
			else if($(this).hasClass('STAT_C')) {
				$url = '${HOME}/customer/getCustomerColForm.do?';
   				$url += '&pageType=C';
   			} 
			// 삭제상태
			else {
   				alert('삭제된 수금계획입니다.');
   				return false;
   			}
   			
			$url += '&custId='+$custId;
			$url += '&custPlnSn='+$custPlnSn;
			winOpenPopup($url,'getCustomerPlanPopup', 620,550,'no');
  		 }
  	 });
	 
     <%-- 연기일자를 수금계획 일자 이후로만 선택가능하도록 함 --%>
  	$('#dly_dt').datepicker('option', 'minDate', new Date(dateAdd($('#dly_dt').attr('data-pln-col-dt'),1)));
	 
});
 
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%> 
  
/**
 * <pre> 
 *   거래처 수금계획 연기 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 04. 22
 * @param dlyDt : 연기 일자, rmkTxt : 비고, custPlnSn : 거래처 수금 계획 순번 
 */
 $.fn.delayCustomerPlan = function() {

  	 $paramObj = {
			 custId : $("#cust_id").val(),
			 dlyDt : $('#dly_dt').val(),
			 rmkTxt : $('#rmk_txt').val(),
			 custPlnSn : $('#cust_pln_sn').val()
	 };
 	 
  	 $param = $.param($paramObj);
 	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/customer/delayCustomerPlan.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$debnList = $result.debnList;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('거래처 수금 계획 연기가 완료되었습니다.');
			    	if($('.customerSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} else if($('.debentureSubTab', window.opener.document).length > 0) {
			    		$(window.opener.location).attr('href', 'javascript:$.fn.pageReload()');
			    	} 
			    	window.open("about:blank","_self").close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("수금계획 연기 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };
  
<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>
</script>
</head>
<body>

<div class="pWrap">
	<header>
		<c:choose>
			<c:when test="${custPln.STAT eq 'P'}">
				<h1><img src="${IMG}/customer/h3_delay.gif" alt="연기" /></h1>
			</c:when>
			<c:when test="${custPln.STAT eq 'D'}">
				<h1><img src="${IMG}/customer/h3_col_plan_detail.gif" alt="수금계획 상세보기" /></h1>
			</c:when>
		</c:choose>
	</header>
	
	<form name="customerPlanForm" id="customer_plan_form" autocomplete="off">
		<%-- INPUT HIDDEN AREA --%>
		<input type="hidden" name="custPlnSn" id="cust_pln_sn" value="${sBox.custPlnSn}" />
		<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}" />
		<input type="hidden" id="exclude_flag" name="excludeFlag" value="Y" />
		
		<div class="pTableWrap">
			<div class="pTableTop">
				<table summary="연기 작성" class="pTableTitle debnListTitle" id="debn_list_title">
				<caption>연기 작성</caption>
				<colgroup>
					<col width="119px">
					<col width="135px">
					<col width="125px">
					<col width="125px">
					<col width="135px">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">채권담당자</th>
						<th scope="col">세금계산서작성일</th>
						<th scope="col">합계금액</th>
						<th scope="col">미수금액</th>
						<th scope="col" class="last">수금예정금액</th>					
					</tr>
				</thead>
				</table>
			</div>
			<div class="pTableContent">
				<table summary="수금업무 작성" class="pTable debnListContent" id="debn_list_content">
				<caption>수금업무 작성</caption>
				<colgroup>
					<col width="119px">
					<col width="135px">
					<col width="125px">
					<col width="125px">
					<col width="135px">
				</colgroup>
				<tbody>
					<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
					<c:if test="${fn:length(debnList) eq 0 or debnList eq null }">
						<tr><td colspan="5"><div class="noSearch">결과가 없습니다.</div></td></tr>						
					</c:if>
					<c:forEach var="debnList" items="${debnList}">
						<tr>
							<td class="bondMbrNm">
								${debnList.BOND_MBR_NM}
								<input type="hidden" id="input_${debnList.DEBN_ID}" class="debnId delayDebn" value="${debnList.DEBN_ID }" /> 
							</td>
							<td class="billDt">${debnList.BILL_DT}</td>
							<td class="list_right sumAmt">${debnList.SUM_AMT}</td>
							<td class="list_right rmnAmt">${debnList.RMN_AMT}</td>
							<td class="list_left last debnPlnColAmtInput">
								<p class="noBorder">
									${debnList.PLN_COL_AMT_COMMA}
								</p>
								<input type="hidden" id="pln_col_amt_${debnList.DEBN_ID}" name="plnColAmt" class="txtInput plnColAmt required" style="width:78px" title="수금예정 금액" value="${debnList.PLN_COL_AMT_COMMA}" maxlength="12" >
							</td>
						</tr>
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
		
		<div class="pSumWrap">
			<table class="pSumTable">
			<caption>통계</caption>
			<colgroup>
				<col width="50%">
				<col width="50%">
			</colgroup>
			<tbody>
				<tr>
					<td>
						선택채권 : 
						<c:choose>
							<c:when test="${(custPln.DEBN_CNT eq null) or (custPln.DEBN_CNT eq '')}">
							<p id="selected_debn">
								0건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="0" />
							</c:when>
							<c:otherwise>
							<p id="selected_debn">
							${custPln.DEBN_CNT}건
							</p>
							<input type="hidden" name="debnCnt" id="debn_cnt" value="${custPln.DEBN_CNT}" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						선택채권 미수금총액 : 
						<c:choose>
							<c:when test="${(custPln.TOT_UNPD_AMT eq null) or (custPln.TOT_UNPD_AMT eq '')}">
								<p id="selected_tot_unpd_amt">0원</p>
								<input type="hidden" name="totUnpdAmt" id="tot_unpd_amt" value="0" />
							</c:when>
							<c:otherwise>
								<p id="selected_tot_unpd_amt">${custPln.TOT_UNPD_AMT_COMMA}원</p>
								<input type="hidden" name="totUnpdAmt" id="tot_unpd_amt" value="${custPln.TOT_UNPD_AMT}" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		
		<div class="pInputWrap">
			<table class="pInputTable">
			<caption>수금업무 작성테이블</caption>
			<colgroup>
				<col width="120px">
				<col width="2px">
				<col width="198px">
				<col width="120px">
				<col width="2px">
				<col width="198px">
			</colgroup>
			<tbody>
				<tr>
					<td><p>연기일</p></td>
					<td><p>:</p></td>
					<c:choose>
						<c:when test="${(custPln.STAT eq 'P')}">
							<td colspan="4">
								<input type="text" name="dlyDt" id="dly_dt" class="firstDate" title="연기일" value="${custPln.PLN_COL_DT}" readonly="readonly" data-pln-col-dt="${custPln.PLN_COL_DT}">
							</td>
						</c:when>
						<c:otherwise>
							<td>
								${custPln.PLN_COL_DT}
							</td>
							<td><p>상태</p></td>
							<td><p>:</p></td>
							<td>
								${custPln.STAT_NAME}
								<c:if test="${custPln.DLY_CUST_PLN_SN ne null and custPln.DLY_CUST_PLN_SN ne '' }" >
									<a href="#none" class="textLink lnkMove STAT_${custPln.DLY_STAT}" id="new_cust_pln_${custPln.DLY_CUST_PLN_SN}">${custPln.DLY_DT}</a>
								</c:if>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td class="middle">
						<p>수금예정금액</p>
						<c:if test="${custPln.STAT eq 'P'}">
							<input type="hidden" name="totPlnColAmt" id="tot_pln_col_amt" value="${custPln.PLN_COL_AMT}" />
						</c:if>
					</td>
					<td class="middle"><p>:</p></td>
					<td class="middle" id="selected_pln_col_amt" colspan="4">
						${custPln.PLN_COL_AMT_COMMA}원
					</td>
				</tr>
				<tr>
					<td class="last"><p>비고</p></td>
					<td class="last"><p>:</p></td>
					<td class="last" colspan="4">
						<c:choose>
							<c:when test="${custPln.STAT eq 'P'}">
								<input type="text" id="rmk_txt" name="rmkTxt" class="txtInput" style="width:400px" title="비고" value="" maxlength="200">
							</c:when>
							<c:otherwise>
								${custPln.RMK_TXT}
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
			</table>	
		</div>
		<div class="pBtnWrap">
			<div class="pBtnMiddle">
				<c:choose>
					<c:when test="${custPln.STAT eq 'P'}">
						<a href="#none" class="on" id="btn_customer_pln_delay">연기</a>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">취소</a>
					</c:when>
					<c:otherwise>
						<a href="#none" class="btnCancel" id="btn_customer_pln_close">닫기</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form>
</div>
</body>
</html>