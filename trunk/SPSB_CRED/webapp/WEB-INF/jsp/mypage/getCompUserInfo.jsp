<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<c:set var="companyUsrInfoList" value="${result.resultSbox}"/>
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="${JS}/common/certX.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitConfig.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitObject.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	 <%--결제조건변경시 input box를 활성화 시키는 로직--%>
	function setPayTerm(){	
	 var isDay =  $('#payTermType').children("option:selected").hasClass('esentialSelect');
	 alert(isDay);
		 if(isDay==true){
			 $(".payTermDayDisplay").show();
		 }else{
			 $(".payTermDayDisplay").hide();
		 }
	 }
	 
	 setInfo();
});

function setInfo(){
	
	var payTermType="${result.resultSbox.PAY_TERM_TYPE}"; 
	$('#payTermType').val(payTermType);
	
	var payTermDay="${result.resultSbox.PAY_TERM_DAY}"; 
	$('#payTermDay').val(payTermDay);
	
	 var isDay = payTermDay;
	 if(isDay.length > 0){
		 $(".payTermDayDisplay").show();
	 }else{
		 $(".payTermDayDisplay").hide();
	 } 
	 
	 var compType="${result.resultSbox.COMP_TYPE}"; 
	 if(compType == "C"){
		$(".corpClass").show();
		$("#usrNo").attr("class", "half");
	 }else if(compType == "I"){
		$(".corpClass").hide();
		$("#usrNo").attr("class", "part");
	}
	//인증서 갱신 후  로그인시
	 if ("${isUpdatedCert}" == "true")
	        alert("인증서가 갱신되었습니다.");
	 
}

function getMyPageInfo(){
	location.href='${HOME}/mypage/myPageMain.do';
}
function getModify(){
	location.href='${HOME}/mypage/getCompUserModify.do';
}
	

function setCert(){
	var ssn = "${sBox.sessionUsrNo}";
	CheckIDN(ssn);
}

function getCertInfo(){
	if(confirm("스마트빌 인증서 신청 화면으로 이동하시겠습니까?")){
		location.href="${smartBillDomain}xMain/cert/app/app.aspx";
	}
}

function afterSucessCert(certInfo){
	$('#certInfo').val(certInfo);
	$('form').attr({'action':'${HOME}/mypage/modifyCertInfo.do', 'method':'POST'}).submit();
}

</script>

</head>
<body>
<div id="accessibility">
	<a href="#mainImg">본문바로가기</a>
</div>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0601"/>
</jsp:include>
<!-- Top Area End -->

<!-- contentArea -->
<div class="contentArea sub06"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		
		<div class="mypageCover">
			<dl class="myPic">
				<dt><em><a href="javascript:getMyPageInfo()">${sBox.sessionCompUsrNm}<br>${sBox.sessionUsrNm} 님 <c:if test="${sBox.sessionAdmYn eq 'Y'}"><br>(관리자)</c:if></a></em></dt>
				<dd><img src="${IMG}/common/bg_mypic.gif" alt="" /></dd>
			</dl>
			<div class="subTitBox">
				<h2 class="tit_sub06">마이페이지</h2>
				<p class="under_h2_sub06">회원의 채권 요약, 공지사항, 1:1문의, 환경 설정 등을 한 눈에 조회할 수 있습니다</p>
			</div>
		</div>
		
		 <div class="infoInputWrap">
			<h3 class="first2"><img src="${IMG}/common/h3_mypage_info.gif" alt="사업자 정보" /></h3>


			<div class="infoInput">
					<dl class="comMemInput">
<!-- 					
						<dt class="phone"><span><label for="compType">사업자 유형</label> </span></dt>
						<dd class="phone"><span>
							<c:if test="${result.resultSbox.COMP_TYPE eq 'C'}">법인 사업자</c:if>
							<c:if test="${result.resultSbox.COMP_TYPE eq 'I'}">개인 사업자</c:if>
						</span></dd>
 -->						
						<dt class="name"><span><label for="usrNo">사업자 등록번호 </label></span></dt>
						<dd id="usrNo" class="half"><span>
							<c:if test="${not empty companyUsrInfoList.USR_NO}"> 
							 ${fn:substring(companyUsrInfoList.USR_NO,0,3)}-${fn:substring(companyUsrInfoList.USR_NO,3,5)}-${fn:substring(companyUsrInfoList.USR_NO,5,10)} 
							</c:if>
						</span></dd>
						
						<dt class="name corpClass"><span><label for="corpNo">법인등록번호</label> </span></dt>
						<dd class="half corpClass"><span>
							<!-- <input type="text" class="txtInput" id="corpNo" name="corpNo" title="법인등록번호"  maxlength="13"/>  -->
							<c:if test="${not empty companyUsrInfoList.CORP_NO}">${fn:substring(companyUsrInfoList.CORP_NO,0,3)}-${fn:substring(companyUsrInfoList.CORP_NO,3,5)}-${fn:substring(companyUsrInfoList.CORP_NO,5,10)}</c:if>
						</span></dd>
						
						<dt class="name"><span><label for="usrNm">사업자 명</label> </span></dt>
						<dd class="half"><span>
							${result.resultSbox.USR_NM}
						</span></dd>
						<dt class="name"><span><label for="ownNm">대표자 명</label></span></dt>
						<dd class="half"><span>
							${result.resultSbox.OWN_NM} 
						</span></dd>
						<%--
						 <dt class="regno"><span><label for="corpNo">법인등록번호</label> </span></dt>
						<dd class="name"><span>
							${result.resultSbox.CORP_NO} 
						</span></dd>
						
						<dt class="regno"><span><label for="ownNo1">대표자주민번호</label></span></dt>
						<dd class="regno"><span>
							<c:if test="${not empty companyUsrInfoList.OWN_NO}">${fn:substring(companyUsrInfoList.OWN_NO,0,6)}-${fn:substring(companyUsrInfoList.OWN_NO,6,12)}</c:if> 
						</span></dd> --%>
						<dt class="name"><span><label for="bizCond">업태</label></span></dt>
						<dd class="half"><span>
							${result.resultSbox.BIZ_COND} 
						</span></dd>
						<dt class="name"><span><label for="bizType">업종</label> </span></dt>
						<dd class="half"><span>
							${result.resultSbox.BIZ_TYPE}
						</span></dd>
						
						<dt class="address first longer"><span>회사 주소</span></dt>
						<dd class="address first longer"><span><c:if test="${not empty companyUsrInfoList.POST_NO}">${companyUsrInfoList.POST_NO}</c:if></span>
						<span>${result.resultSbox.ADDR_1} ${result.resultSbox.ADDR_2}</span>
						</dd>
					</dl>
				</div>
				<%-- <div class="authenticWrap">
					<h3 class="second"><img src="${IMG}/common/h3_mypage_authentic.gif" alt="인증서 관리" /></h3>
					<div class="box">
						<p>기업인증서를 재발급하였거나 갱신한 경우, 인증서를 새로 등록하실 수 있습니다.</p>
						<a href="#none" onclick="javascript:setCert();">
							<img src="${IMG }/common/btn_certificate.gif" alt="기업인증서 등록" class="certImgBtn" />
						</a>	
						<br/>
						<br/>
						<a href="#none" onclick="javascript:getCertInfo();">
							<b>[인증서 신청 바로가기]</b>
						</a>
					</div>
				</div>
				
				<form name="updateCertInfoFrm" id="updateCertInfoFrm">
					<input type="hidden" id="certInfo" name="certInfo" />
				</form> --%>

		</div> 
	</section>

	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>