<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<c:set var="phoneCodeList" value="${result.commonCodeBox.phoneCodeList}" />
<c:set var="cellPhoneCodeList" value="${result.commonCodeBox.cellPhoneCodeList}" />
<c:set var="emailCodeList" value="${result.commonCodeBox.emailCodeList}" />
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 

<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;

$(document).ready(function(){
});


 function fnValidate(){
	 
	 if( $.trim($("#psw0").val()).length == 0 ) {
			alert("이전 비밀번호를  입력해주세요.");
			$("#psw0").focus();
			return false;
	}
	 
	var loginPw = $.trim($("#psw1").val());
	if( (loginPw.length < 8) || (loginPw.length > 12)) {
		alert("비밀번호는 8글자 이상 12자 이하로 입력하셔야 합니다.");
		return false;
	}
	var passwdCheckResult = true;
	var numCnt = 0;
	for( var i=0; i<loginPw.length; i++ ) {		
		var chr = loginPw.substr( i, 1 );
		if( onlyNum.test( chr ) ) {
			numCnt ++;
		}
		if( !numEng.test( chr ) ) {
			passwdCheckResult = false;
			break;
		}
	}
	if( numCnt == loginPw.length || numCnt == 0 || !passwdCheckResult) {
		alert("비밀번호는 영문과 숫자를 혼용하여 주시기 바랍니다.");
		return false;
	}
	
	//이전 비밀번호와 입력한 비밀번호가 동일한지 확인
	if( $.trim($("#psw0").val()) != $.trim($("#loginPw").val()) ) {
		alert("이전 비밀번호가 일치하지 않습니다.");
		$("#psw0").focus();
		return false;
	} 
	
	if( $("#psw1").val() != $("#psw2").val()) {
		alert("설정한 비밀번호가 같지 않습니다.");
		$("#psw1").focus();
		return false;
	} 
	
	
	return true;
}
 
function setUserInfo(){
	
	if(!fnValidate()) {
		return false;
	}
	if(confirm("비밀번호를 수정하시겠습니까?")){
		$('form').attr({'action':'${HOME}/mypage/modifyUserForPW.do', 'method':'POST'}).submit(); 
	}
} 

function setCancel(){
	 if(confirm('비밀번호 수정이 취소 됩니다.')){
		 location.href = '${HOME}/mypage/myPageMain.do';
		 //수정된 사항을 적용하지 않고 상세화면
	 }else{
		 //진행중이던 사업자 정보 수정화면
	 }
	
}
function getMyPageInfo(){
	location.href='${HOME}/mypage/myPageMain.do';
}
 

</script>
</head>
<body>

<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0001" />
</jsp:include>
<%-- Top Area End --%>

<div id="containerWrap">

<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0001"/>
</jsp:include>
<!-- Left Area End -->



<!-- rightWrap -->	
	<div id="rightWrap">
		<div class="right_tit ltit00_0101"><em>비밀번호 변경</em></div>
		<p class="content_tit">비밀번호 변경</p>
		<div class="tbl01Wrap">
			<input type="hidden" id="loginPw" name="loginPw" class="txtInput" value="${result.resultSbox.LOGIN_PW}" /> 
			<input type="hidden" id="loginId" name="loginId" class="txtInput" value="${result.resultSbox.LOGIN_ID}" /> 
			<form name="updateUserFrm" id="updateUserFrm">
			<div id="contentWrap">
				<div class="ptbl01Wrap">
					<table>
					<colgroup>
						<col width="30%">
						<col width="70%">								
					</colgroup>
					<tbody>
						<tr>
							<th>이전 비밀번호 <span class="em_po">*</span></th>
							<td><input type="password" id="psw0"  name="psw0" class="tblInput " style="width:200px"></td>
						</tr>   
						<tr>
							<th>비밀번호 <span class="em_po">*</span></th>
							<td><input type="password" id="psw1" name="psw1" class="tblInput" style="width:200px">
								<div class="wart1">
									<span class="warn_ico1">※ </span> 영문 + 숫자 8~12자 가능<br>
								</div>
								
							</td>
						</tr>
						<tr>
							<th>비밀번호 확인 <span class="em_po">*</span></th>
							<td><input type="password" id="psw2" name="psw2" class="tblInput" style="width:200px">
								<div class="wart1">
									<span class="warn_ico1">※ </span>비밀번호 확인을 위해 한 번 더 입력해 주세요.<br>
								</div>
							</td>
						</tr>
					</tbody>
					</table>
				</div>
			</div>
			</form>
			
			<div id="footWrap">
				
				<div class="wart1">
					<span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
				</div>
				<div class="ex">
					<div class="btn_cbbtn_01">
						<a href="#none" onclick="javascript:setUserInfo()" >저장</a> 
						<a href="#none" onclick="javascript:setCancel()">취소</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<%--  footer Area Start --%>
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<%-- footer Area End --%>
</body>
</html>