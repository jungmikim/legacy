<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</head>
<body>
<!-- contentArea -->
<div class="contentArea">
	<!-- content -->
	<section id="content">
		<div class="default_txt01">
			<img src="${IMG}/common/sorry.gif" alt="죄송합니다." />
				<ul style="font-size: 12px;">
				<c:choose>
					<c:when test="${errorType eq 'param'}">
						<li>죄송합니다.</li>
						<li>&nbsp;</li>	
							<c:choose>
								<c:when test="${errorRoot eq 'compIdEmpty'}">
									<li>필수 파라메터 중 본사 사업자번호가 존재하지 않습니다.</li>
								</c:when>
								<c:when test="${errorRoot eq 'compIdError'}">
									<li>필수 파라메터 중 본사 사업자번호가 유효하지 않습니다.</li>
								</c:when>	
								<c:when test="${errorRoot eq 'bizNoEmpty'}">
									<li>필수 파라메터 중 거래처 사업자번호가 존재하지 않습니다.</li>
								</c:when>
								<c:when test="${errorRoot eq 'bizNoError'}">
									<li>필수 파라메터 중 거래처 사업자번호가 유효하지 않습니다.</li>
								</c:when>
								<c:when test="${errorRoot eq 'keyIdError'}">
									<li>필수 파라메터 중 라이센스키가 존재하지 않습니다.</li>
								</c:when>															
							</c:choose>						
					</c:when>
					<c:when test="${errorType eq 'kedCd'}">
						<li>죄송합니다.</li>
						<li>&nbsp;</li>							
						<li>해당 거래처번호와 사업자번호로 조회된 KED코드가 존재하지 않습니다.</li>
						<li>&nbsp;</li>	
						<li>관리자에게 문의하세요.</li>
					</c:when>					
					<c:otherwise>
						<li>죄송합니다.</li>
						<li>&nbsp;</li>						
						<li>잠시 후에 다시 시도해 주세요.</li>					
					</c:otherwise>
				</c:choose>
				</ul>
		</div>
	</section>
</div>
</body>
</html>