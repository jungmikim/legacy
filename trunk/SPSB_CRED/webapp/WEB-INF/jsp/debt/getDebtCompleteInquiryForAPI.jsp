<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="result" value="${result}" />
<c:set var="debtBox" value="${result.debtBox}" />
<c:set var="debtStat" value="${result.debtStat}" />
<c:set var="debentureList" value="${result.debentureList}" />
<c:set var="prfFileList" value="${result.prfFileList}" />
<c:set var="debtProcess" value="${result.debtProcess}" />
<c:set var="histStatusList" value="${result.histStatusList}" />
<c:set var="pcPage" value="${result.pcPage}"/>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtCompleteInquiry.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		// STEP03 선택채권 건수 입력
		$('#selected_bond > p').text($('.debnList').find('tr').length+"건");
		
		// STEP03 채권 없을 시 띄울 화면
		if ($('.debnList').find('tr').length == 0) {
			$('.debnList').find('tbody').append('<tr><td colspan="4">검색결과가 존재하지 않습니다.</td></tr>');
		}
		
		// 채권이 열 개 이상일 시 height 조정 auto 풀기
		if ($('.debnList').find('tr').length >= 10) {
			$('.debnList').removeAttr('style');
		}
		
		// STEP04 맨 마지막 증빙자료 다음에는 콤마 삭제
		$('#prfArea').find('.default_note').each(function() {
			$(this).find('.comma').last().text('');
		});
		
		/*
		* <pre>
		* STEP TITLE CLICK EVENT
		* </pre>
		* @author 비즈온 디자인팀
		*/
		$('#menu_slide > li.Apply_slide > .slideSpan > a').click(function(){
			$checkElement = $(this).parent().next();
			if(($checkElement.is('ul')) && ($checkElement.is(':visible'))) {
				$checkElement.slideUp(300);
				$(this).attr("class", "down");
				return false;
			}
			if(($checkElement.is('ul')) && (!$checkElement.is(':visible'))) {
				$checkElement.slideDown(300);
				$(this).attr("class", "up");
				return false;
			}
		});
		
		/*
		* <pre>
		* 모두펼치기 버튼 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.09
		*/
		$('#open_all').click(function() {
			$('#menu_slide > li.Apply_slide > ul').slideDown(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class", "up");
			
			$('#open_all').hide();
			$('#close_all').removeAttr('style');
		});
		
		/*
		* <pre>
		* 모두닫기 버튼 CLICK EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.14
		*/
		$('#close_all').click(function() {
			$('#menu_slide > li.Apply_slide > ul').slideUp(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class", "down");
			
			$('#close_all').hide();
			$('#open_all').removeAttr('style');
		});
		
		/*
		* <pre>
		* 타이틀 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.14
		*/
		$('#menu_slide > li.Apply_slide > span > a').click(function() {
			
			if($(this).hasClass('down')) {
				$('#close_all').hide();
				$('#open_all').removeAttr('style');
			}else {
				if($('.down').length == 0){
					$('#open_all').hide();
					$('#close_all').removeAttr('style');
				}
			}
			
		});
		
		/*
		* <pre>
		* 인쇄 미리보기 페이지 띄우는 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.09
		*/
		$('#btn_print').click(function() {
			if( navigator.userAgent.indexOf("MSIE") > 0 ){
				window.print();
			}else{
				window.print();
			}
		});
		
		 /**
		 * <pre>
		 *   등록예정통보서 샘플 팝업
		 * </pre>
		 * @author KIM GA EUN
		 * @since 2014. 05. 01.
		 */	 
		 $('#btn_sample').click(function() {
			 $url = '${samplePopupDomain}';
				winOpenPopup($url, 'getSamplePopup', 550, 500, 'yes');
		 });			
		
		/**
		* <pre>
		* 복사 후 작성 CLICK EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.14
		*/
		$('#btn_copy_and_write').click(function(e){
			if(confirm("채무불이행 신청서를 복사 후 작성 하시겠습니까?")) {
				$copyfile = (confirm("STEP 04의 첨부파일을 함께 복사하시겠습니까?")) ? "Y" : "N";
				$param = {
					debtApplId : $("#debt_appl_id").val(),
					custId : $("#cust_id").val(),
					copyfile : $copyfile
				};
				location.href="${HOME}/debt/getDebtCopyAndWrite.do?" + $.param($param);
			}
		});
		
		/*
		* <pre>
		* 파일 다운로드 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.10
		*/
		$('.browsing').click(function() {
			
			$('#file_name').val($(this).text());
			$('#file_path').val($(this).attr('data-filePath'));
			
			if ($(this).hasClass('debtFile')) {
				$('form').attr({'action':'${HOME}/debt/getDebtPrfFileDownload.do', 'method':'POST'}).submit();
			} else {
				$('form').attr({'action':'${HOME}/debt/getDebtAddedPrfFileDownload.do', 'method':'POST'}).submit(); 
			}
		});
		
		/*
		* <pre>
		* 	페이징 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.13
		*/
		$(document).on('click', '.page', function(){
			 
			$param = "num=" + $(this).attr("pagenum") 
			 		+ "&debtApplId="+$("#debt_appl_id").val();
			
			$.ajax({
					type : "POST", 
					url : "${HOME}/debt/getDebtOngoingStatHistoryList.do",
					dataType : "json",
					data : $param,
					async : false,
					success : function(msg) {
							$result = msg;
							if($result.REPL_CD='00000'){
								// 페이징 된 사유관리 리스트 출력
		 						$("#statusBody").children().remove();
		 						$paramBox = '';
		 						$paramBox += ' <tr>' ;
 								$paramBox += '<th width="10%" class="first_thlwr4">날짜</th>' ;
								$paramBox += '<th width="12%">상태</th>' ;
								$paramBox += '<th width="38%">내용(사유)</th>' ;
								$paramBox += '<th width="30%">첨부파일</th>' ;
								$paramBox += '<th width="10%">작성자</th>' ;
								$paramBox += '</tr>' ;
		 						for(var i=0; i<$result.histStatusList.length; i++ ){
		 							$paramBox += '<tr>' ;
		 							$paramBox += '<td class="first_tdlwr4">' + utfDecode($result.histStatusList[i].REG_DT_TIME) + '</td>' ;
		 							$paramBox += '<td>' +utfDecode($result.histStatusList[i].CH_STAT) + '</td>' ;
		 							$paramBox += '<td style="padding-left:5px; text-align:left;"> ' +utfDecode($result.histStatusList[i].RMK_TXT) + '</td> '  ;
		 							$paramBox += '<td>' ;	 	
		 									 							
		 							if($result.histStatusList[i].FILE_NM != null){
		 								$arrayList = $result.histStatusList[i].FILE_NM;
		 								$.each(utfDecode($arrayList).split('|') , function(index , value) {
			 								$paramBox += '<a class="browsing" data-filePath=' ;
			 									$temp =utfDecode($result.histStatusList[i].FILE_PATH).split('|')[index];
			 									if($temp!=null){
			 										$paramBox += $temp ;
			 									}
			 									$paramBox += '>';
			 									if(utfDecode(value)!='null'){
					 								if(index==0){
					 									$paramBox+=utfDecode(value)+" ";
					 								}else{
					 									$paramBox+="," + utfDecode(value) +" ";
					 								}
				 								}	
			 								$paramBox += '</a> ' ;	  
			 							});
		 							}
 							
		 							$paramBox += '</td>' ;
		 							$paramBox += '<td>' + utfDecode($result.histStatusList[i].USR_NM) + '</td>' ;
		 							$paramBox += '</tr>' ;
		 							
		 						}

		 						$("#statusBody").append($paramBox);
							
		 						// 페이징 수정
		 						$(".paging").remove();
		 						$pageHtml = "<div class='page_num paging' id='getPageView'>" 
		 									+ utfDecode($result.pcPage)
		 									+"</div>";
		 						//$(".debtOngoingStatus").after($pageHtml);
		 						$(".hist_page").prepend($pageHtml);
		 								 						
							}
							
						},
						error : function(xmlHttpRequest, textStatus, errorThrown){
							alert("사유관리 페이징 실패 [" + textStatus + "]");
						}
				});
			 
		});
		
	});
	
</script>

</head>
<body>

 
<input type="hidden" id="debt_appl_id" value="${debtBox.DEBT_APPL_ID}" />
<input type="hidden" id="cust_id" value="${debtBox.CUST_ID}" />

<div style="display: none;">
<form>
	<input type="text" id="file_name" name="fileNm" />
	<input type="text" id="file_path" name="filePath" />
</form>
</div>

<%-- contentArea --%>
<%-- menu별 이미지 class sub00 --%>
<div id="containerWrap">
<div id="rightWrap">
		
		<p class="content_tit norm">진행완료 접수문서
			<a id="btn_sample" href="#none" class="ApplyBtn02" style="background:#4c91ff; solid #4e7ac2; color:#fff; text-shadow:0 0 1px #3f3f3f; border:1px solid #4e7ac2;">등록예정통보서 샘플</a>	
		</p>
		
	
		<%-- 채무불이행 진행상태 테이블 시작 --%>
		
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="9%" class="first_thlwr4">진행상태</th>
					<th width="82%">설명</th>
					<th width="9%">전송상태</th>
				</tr>
				<tr>	
					<td class="first_tdlwr4">
						<c:choose>
							<c:when test="${debtBox.STAT eq 'AU'}">신청불가</c:when>
							<c:when test="${debtBox.STAT eq 'AC'}">신청취소</c:when>
							<c:when test="${debtBox.STAT eq 'CR'}">해제완료</c:when>
							<c:when test="${debtBox.STAT eq 'RU'}">등록불가</c:when>
							<c:when test="${debtBox.STAT eq 'KF'}">심사과실발생</c:when>
						</c:choose>
					</td>
					<td style="text-align:left; padding-left:5px;">
						${debtBox.STAT_TXT}
					</td>
					<td>
						<c:if test="${debtBox.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${debtBox.TRANS eq '1'}">전송완료</c:if>
						<c:if test="${debtBox.TRANS eq '2'}">전송중</c:if>
						<c:if test="${debtBox.TRANS eq null}">-</c:if>
					</td>					
				</tr>						
			</tbody>
			</table>
		</div>	
		<%-- 채무불이행 진행상태 테이블 끝 --%>
		
		<%-- 채무불이행 사유 테이블 시작 --%>
		<c:if test="${not empty histStatusList}">
			<div class="linedot_u40"> </div>
			
			<p class="content_tit">사유관리</p>
			
			<div class="tbl04Wrap">
			<table class="debtOngoingStatus">
			<tbody id ="statusBody">
				<tr>
					<th width="10%" class="first_thlwr4">날짜</th>
					<th width="12%">상태</th>
					<th width="38%">내용(사유)</th>
					<th width="30%">첨부파일</th>
					<th width="10%">작성자</th>
				</tr>
				<c:forEach var="statHistoryList" items="${histStatusList}">
				<tr>
					<td class="first_tdlwr4">
						${statHistoryList.REG_DT_TIME}
					</td>
					<td>
						${statHistoryList.CH_STAT}
					</td>
					<td style="padding-left:5px; text-align:left;">
						${statHistoryList.RMK_TXT}
					</td>
					<td>
						<c:set var="FILE_NM" value="${fn:split(statHistoryList.FILE_NM,'|')}" />
						<c:set var="FILE_PATH" value="${fn:split(statHistoryList.FILE_PATH,'|')}" />
						<c:if test="${fn:trim(statHistoryList.FILE_NM) ne ''}">
                    		<c:forEach var="i" items="${FILE_NM}" varStatus="idx">
                    		<c:if test="${idx.index != 0}">,</c:if>
								<a class="browsing" data-filePath="${FILE_PATH[idx.index]}" >${FILE_NM[idx.index]}</a>									
							</c:forEach>		                    				
                    	</c:if>
					</td>
					<td>
						${statHistoryList.USR_NM}
					</td>
				</tr>
				</c:forEach>						
			</tbody>
			</table>
			<c:if test="${not empty histStatusList}" >	
			<div id="page_num_wrap" class="hist_page">
				<div class="page_num paging" id="getPageView">
					<c:out value="${pcPage}" escapeXml="false" />
				</div>
			</div>
			</c:if>
		</div>	
		</c:if>
		<%-- 채무불이행 사유 테이블 끝 --%>
		
		<div class="linedot_u40"> </div>
		
		<p class="content_tit">현재 진행상태</p>
		
		
		<%-- 현재 진행상태 테이블 시작 --%>
		<div id="defaultWrap">
			<div class="default_stepbox">
				<ul>
					<c:if test="${debtProcess.STEP1 ne ''}">
						<li class="s1">
						<p class="default_step_n">${debtProcess.STEP1 }</p>
						<p class="default_step_d">${debtProcess.STEP1_DT }</p>
						<em></em></li>
					</c:if>
					
					<c:if test="${debtProcess.STEP2 ne ''}">
						<li class="s2">
						<p class="default_step_n">${debtProcess.STEP2 }</p>
						<p class="default_step_d">${debtProcess.STEP2_DT }</p>
						<em></em></li>
					</c:if>
					
					<c:if test="${debtProcess.STEP3 ne ''}">
						<li class="s2">
						<p class="default_step_n">${debtProcess.STEP3 }</p>
						<p class="default_step_d">${debtProcess.STEP3_DT }</p>
						<em></em></li>
					</c:if>
					
					<c:if test="${debtProcess.STEP4 ne ''}">
						<li class="s2">
						<p class="default_step_n">${debtProcess.STEP4 }</p>
						<p class="default_step_d">${debtProcess.STEP4_DT }</p>
						<em></em></li>
					</c:if>
					
					<c:if test="${debtProcess.STEP5 ne ''}">
						<li class="s2">
						<p class="default_step_n">${debtProcess.STEP5 }</p>
						<p class="default_step_d">${debtProcess.STEP5_DT }</p>
						<em></em></li>
					</c:if>
					
					<c:if test="${debtProcess.STEP6 ne ''}">
						<li class="s2">
						<p class="default_step_n">${debtProcess.STEP6 }</p>
						<p class="default_step_d">${debtProcess.STEP6_DT }</p>
						<em></em></li>
					</c:if>
					
				</ul>
			</div>
		</div>		
		<%-- 현재 진행상태 테이블 끝 --%>
		
		<div class="linedot_u40"> </div>
		
		<p class="content_tit norm">채무불이행 신청서
			<a id="open_all" href="#none" class="ApplyBtn02">모두펼치기</a>		
			<a id="close_all" href="#none" class="ApplyBtn02" style="display: none;">모두닫기</a>
		</p>
		

		<%-- 채무불이행 신청서 테이블 시작 --%>
		<div id="defaultApplyWrap">
			<div class="default_Apply">
				<ul id="menu_slide">
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 01. 신청인 정보입력</a></span>
						<ul style="display: none;">
							
							<%-- STEP01 시작 --%>
							<li class="Apply_sub">							
								<div class="ApplysubWrap">
								<p class="content_dot">회사정보</p>									
									<table class="defaultTbl01_01" summary="회사정보 입력테이블">
									<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">구분</td>
												<td class="default_note" colspan="3">${debtBox.COMP_TYPE_STR}</td>
											</tr>
											<tr>
												<td class="title">회사명</td>
												<td>
													${debtBox.COMP_NM} 
												</td>
												<td class="title">사업자번호</td>
												<td>
													<c:set var="USR_NO" value="${fn:substring(debtBox.COMP_NO, 0, 3)}-${fn:substring(debtBox.COMP_NO, 3, 5)}-${fn:substring(debtBox.COMP_NO, 5, 13)}" />
													${USR_NO}
												</td>
											</tr>
											<c:choose>
												<c:when test="${debtBox.COMP_TYPE eq 'C'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">법인등록번호</td>
														<td>
														<c:if test="${!empty debtBox.CORP_NO}">
														${fn:substring(debtBox.CORP_NO,0,6)}-${fn:substring(debtBox.CORP_NO,6,13)}
														</c:if>														
														</td>
													</tr>
													<tr>
														<td class="title">업종</td>
														<td class="default_note" colspan="3">${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
												<c:when test="${debtBox.COMP_TYPE eq 'I'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">업종</td>
														<td>${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
											</c:choose>
											<tr>
												<td class="title">주소</td>
												<td class="default_note" colspan="3">
													<c:if test="${fn:trim(debtBox.POST_NO) ne ''}">
														(${debtBox.POST_NO})
													</c:if>
													<c:if test="${(debtBox.ADDR_1 ne null) or (debtBox.ADDR_1 ne '')}">
														 ${debtBox.ADDR_1} ${debtBox.ADDR_2}
													</c:if>
												</td>
											</tr>											
										</tbody>
									</table>
									
									<p class="content_dot2">정보등록 담당자</p>							
									<table class="defaultTbl01_01" summary="정보등록 담장자">
									<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">성명</td>
												<td>${debtBox.USR_NM}</td>
												<td class="title">부서</td>
												<td>${debtBox.DEPT_NM}</td>
											</tr>
											<tr>
												<td class="title">직위</td>
												<td class="default_note">${debtBox.JOB_TL_NM}</td>
												<td class="title">전화</td>
												<td class="default_note">
													 ${debtBox.TEL_NO}
												</td>
											</tr>
											<tr>
												<td class="title">휴대폰</td>
												<td class="default_note">
													${debtBox.MB_NO}
												</td>												
												<td class="title">팩스</td>
												<td class="default_note">
													${debtBox.FAX_NO}
												</td>
											</tr>
											<tr>
												<td class="title">이메일</td>
												<td class="default_note" colspan="3">
													${debtBox.EMAIL}
												</td>
											</tr>										
										</tbody>
									</table>
							
								</div>							
							</li>
							<%-- STEP01 끝 --%>
							
						</ul>
					</li>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 02. 채무자 정보입력</a></span>
						<ul style="display: none;">
						
							<%-- STEP02 시작 --%>
							<li class="Apply_sub">
								<div class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="회사정보 입력테이블">
									<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">구분</td>
												<td class="default_note" colspan="3">${debtBox.CUST_TYPE_STR}</td>
											</tr>
											<tr>
												<td class="title">회사명</td>
												<td>
													${debtBox.CUST_NM} 
												</td>
												<td class="title">사업자번호</td>
												<td>
													<c:set var="CUST_USR_NO" value="${fn:substring(debtBox.CUST_NO, 0, 3)}-${fn:substring(debtBox.CUST_NO, 3, 5)}-${fn:substring(debtBox.CUST_NO, 5, 13)}" />
													${CUST_USR_NO}
												</td>
											</tr>
											<tr>
												<td class="title">대표자명</td>
												<td <c:if test="${debtBox.CUST_TYPE eq 'I'}">colspan="3"</c:if>>${debtBox.CUST_OWN_NM}</td>
												<c:if test="${debtBox.CUST_TYPE eq 'C'}">
													<td class="title">법인등록번호</td>
													<td>
													<c:if test="${!empty debtBox.CUST_CORP_NO}">
														${fn:substring(debtBox.CUST_CORP_NO,0,6)}-${fn:substring(debtBox.CUST_CORP_NO,6,13)}
													</c:if>															
													</td>
												</c:if>
											</tr>											
											<tr>
												<td class="title">주소</td>
												<td class="default_note" colspan="3">
													<c:if test="${fn:trim(debtBox.CUST_POST_NO) ne ''}">
														(${debtBox.CUST_POST_NO})
													</c:if>
													<c:if test="${(debtBox.CUST_ADDR_1 ne null) or (debtBox.CUST_ADDR_1 ne '')}">
														 ${debtBox.CUST_ADDR_1} ${debtBox.CUST_ADDR_2}
													</c:if>
												</td>
											</tr>	
											<tr>
												<td class="title">채무구분</td>
												<td>${debtBox.DEBT_TYPE_STR}</td>
												<td class="title">채무불이행 금액</td>
												<td>${debtBox.ST_DEBT_AMT}원</td>
											</tr>
											<tr>
												<td class="title">연체개시일</td>
												<td>${debtBox.OVER_ST_DT}</td>
												<td class="title">등록사유발생일</td>
												<td>${debtStat.REG_DT}</td>
											</tr>
										</tbody>
									</table>
																	
								</div>
							</li>
							<%-- STEP02 끝 --%>
							
						</ul>
					</li>
<%-- 					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 03. 미수채권 불러오기 (선택사항)</a></span>
						<ul style="display: none;">
						
							STEP03 시작
							<li class="Apply_sub">
								<div class="ApplysubWrap">
									
									<table class="defaultTbl01_03" summary="미수채권 불러오기">
									<caption>미수채권 불러오기</caption>
										<colgroup>
											<col width="234px">
											<col width="236px">
											<col width="236px">
											<col width="236px">
										</colgroup>
										<thead>
										<tr>
											<th scope="col">세금계산서작성일</th>
											<th scope="col">채권합계금액</th>
											<th scope="col">연체일수</th>
											<th scope="col">미수금액</th>				
										</tr>
									</thead>
									</table>									
									<div class="debnList defaultTbl01_03_Content" style="height: auto;">
										<table summary="미수채권" class="defaultTbl01_03_list">
										<caption>미수채권</caption>
										<colgroup>
											<col width="236px">
											<col width="236px">
											<col width="236px">
											<col width="236px">
										</colgroup>
										<tbody>
										<c:forEach var="debentureList" items="${debentureList}">
											<c:if test="${debentureList.DEBT_APPL_ID ne null}">
												<tr>
													<td>${debentureList.BILL_DT}</td>
													<td class="list_right">${debentureList.SUM_AMT}원</td>
													<td>${debentureList.DELAY_DAY}일</td>
													<td class="list_right term last">${debentureList.RMN_AMT}원</td>
												</tr>
											</c:if>
										</c:forEach>
										</tbody>
										</table>									
									</div>
									<div class="defaultSumWrap">
										<table class="defaultSumTable">
										<caption>통계</caption>
										<colgroup>
											<col width="30%">
											<col width="30%">
											<col width="40%">
										</colgroup>
										<tbody>
											<tr>
												<td id="selected_bond" style="height: 25px;">선택채권 : <p></p></td>
												<td style="height: 25px;">선택채권 미수금총액 : <p>${debtBox.TOT_UNPD_AMT}원</p></td>
												<td style="height: 25px;">채무불이행 금액 : ${debtBox.ST_DEBT_AMT}원</td>
											</tr>
										</tbody>
										</table>	
									</div> 
								</div>
							</li>
							STEP03 끝
							
						</ul>
					</li> --%>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 03. 거래증빙자료 첨부 및 작성완료</a></span>
						<ul style="display: none;">
							
							<%-- STEP04 시작 --%>
							<li class="Apply_sub">
								<div class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블" style="margin-bottom:10px;">
									<caption>거래증빙자료</caption>
										<colgroup>
											<col width="18%">
											<col width="82%">
										</colgroup>
										<tbody id="prfArea">
										<tr>
											<td class="title">거래명세서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'A'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="debtFile browsing" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2; cursor: pointer;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">세금계산서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'B'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="debtFile browsing" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2; cursor: pointer;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">법원판결문</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'C'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="debtFile browsing" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2; cursor: pointer;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">계약서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'D'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="debtFile browsing" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2; cursor: pointer;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">기타증빙자료</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'E'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="debtFile browsing" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2; cursor: pointer;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										</tbody>
									</table>
															
								</div>
							</li>
							<%-- STEP04 끝 --%>
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<%-- 채무불이행 신청서 테이블 끝 --%>
		
		<div class="linedot_u40"> </div>
		
		<%-- 서비스 이용료 안내 테이블 시작 --%>
		<div class="tbl04Wrap">
		<table id="apply_company_info" class="defaultTbl01_01" summary="안내사항 테이블">
			<caption>안내사항</caption>
			<colgroup>
				<col width="18%">
				<col width="82%">
			</colgroup>
			<tbody>
				<tr >
				<td class="title" rowspan="5" style="height:150px;"><p align="center">안내사항</p></td>
				<td class="default_note2" style="height:150px;">
					<pre class="information_briefing">${result.informationBriefing}</pre>
					<div class="information_briefing_comment">
					<!-- 심사 및 신청취소, 해제 관련 문의 사항은 아래 전화번호로 문의해주시길 바랍니다.<br /> 
                    KED연체관리서비스 담당자: 02-3215-2432 -->
					</div>
				</td>

				
			</tbody>
		</table>
		</div>	
		<%-- 서비스 이용료 안내 테이블 끝 --%>
		
		<div class="empty_b7"> </div>
		
		
		<div id="tbl_numbtnWrap">
			<table class="tbl_numbtn">
			<tbody>
				<tr>
					<td class="left">
						<div id="btnWrap_l">
							<div class="btn_graybtn_01" id="btnWrap_l">
								<a id="btn_print" href="#none" class="on" id="btn_del">인쇄</a>
							</div>
						</div>
					</td>
					
					<td class="right">
					
					<%--   ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임 --%>
					<%-- <c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}">
						등록불가(RU) && 상태가 해제완료(CR)이고 해제요청 사유가 "수금완료" 인것은 복사후 작성 버튼을 보여주지 않음
						<c:if test="${debtBox.STAT ne 'RU'}">
						<div class="btn_whitebtn_01">
							<a id="btn_copy_and_write" class="ApplyBtn_Type on" <c:if test="${debtBox.STAT eq 'CR' and debtStat.DEBT_STAT_SUB_CD eq 'A'}">style="display: none;"</c:if> href="#none">복사 후 작성</a>
						</div>
						</c:if>
					</c:if> --%>
					</td>
            

				</tr>
			</tbody>
			</table>
		</div>	
		
</div>


</div>
</body>
</html>