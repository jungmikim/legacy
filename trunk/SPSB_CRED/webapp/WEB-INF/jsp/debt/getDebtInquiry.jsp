<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="result" value="${result}" />
<c:set var="commonCodeBox" value="${result.commonCodeBox}" />
<c:set var="phoneCodeList" value="${commonCodeBox.phoneCodeList}" />
<c:set var="faxCodeList" value="${commonCodeBox.phoneCodeList}" />
<c:set var="emailCodeList" value="${commonCodeBox.emailCodeList}" />
<c:set var="cellPhoneCodeList" value="${commonCodeBox.cellPhoneCodeList}" />

<%--  채무불이행 Master Table 정보 --%>
<c:set var="debtBox" value="${result.debtBox}" />

<%--  미수채권 리스트 정보 --%>
<c:set var="debentureList" value="${result.debentureList}" />

<%--  미수채권 파일 리스트 정보 --%>
<c:set var="debentureFileList" value="${result.debentureFileList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtInquiry.css" />

<style type="text/css">
	.browsing {font-weight: bold; line-height: 170%;}
	#ui-datepicker-div{ font-size: 13px; width: 210px; }
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript"
	src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		jQuery.support.cors = true;
		
		$.datepicker.setDefaults({
			dateFormat : 'yy-mm-dd',
			monthNames : [ '년 1월', '년 2월', '년 3월', '년 4월', '년 5월', '년 6월', '년 7월',
						   '년 8월', '년 9월', '년 10월', '년 11월', '년 12월' ],
			monthNamesShort : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월',
					            '10월', '11월', '12월' ],
			dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
			showMonthAfterYear : true,
			changeYear : true,
			changeMonth : true,
			maxDate : "-60D"
		});
		
		$('.step2_modify_toggle .applyDate').datepicker();

		/*
		 * <pre>
		 * 등록사유 발생일자
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.05.10
		 */
		$(document).on('change', '.step2_modify_toggle .applyDate',
			function() {
				$time = new Date($(this).val());
				$time.setDate($time.getDate() + 90);

				$year = $time.getFullYear();
				$month = ((eval($time.getMonth() + 1) < 10) ? "0" : "") + eval($time.getMonth() + 1);
				$day = (($time.getDate() < 10 ? "0" : "")) + $time.getDate();

				$('#reason_occur_date').text($year + "-" + $month + "-" + $day);
			});	
		
		// 전체 메뉴 보이도록 show 한다.
		$('#menu_slide > li.Apply_slide > ul').show();
		
		/*
		* <pre>
		* STEP01의 회사정보 구분을 변경했을 때의 form 변경
		* </pre>
		* @author sungrangkong
		* @since 2014.04.16
		*/
		$(document).on('change', '.applyCustType', function(e){
			$.changeCorpForm($('#apply_company_info'), $(this).val());
		});
		
		/*
		* <pre>
		* STEP01의 사업자번호 '-' 입력 여부
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#apply_cust_num').on({
			focusin: function() {
				$origin_value = $(this).val();
				$(this).val($origin_value.replace(/-/gi, ''));
			},
			focusout: function() {
				$origin_value = $(this).val().replace(/[^0-9]/gi, '');
				$(this).val($origin_value.substr(0,3)+'-'+$origin_value.substr(3,2)+'-'+$origin_value.substr(5,5));
			}
		});
		
		/*
		* <pre>
		* 이메일 SELECT BOX CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#select_email').change(function() {
			// 직접입력을 제외한 나머지의 경우 이메일 뒷부분 input창은 readonly 변경
			if($(this).val() == "") {
				$('#email2').val("");
				$('#email2').attr("readonly", false);
			} else {
				$('#email2').val($(this).val());
				$('#email2').attr("readonly", true);
			}
		});
		
		/*
		* <pre>
		* STEP01의 정보등록 담당자 수정 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#btn_memb_info_modify').click(function(e) {
			// 정보등록 담당자 수정 팝업페이지
			winOpenPopup("${HOME}/mypage/getUserModifyPopup.do",'getUserModifyPopup',1000,500,'yes');
		});
		
		/*
		* <pre>
		* STEP02의 회사정보 구분을 변경했을 때의 form 변경
		* </pre>
		* @author sungrangkong
		* @since 2014.04.17
		*/
		$('.deptorCustType').change(function() {
			$.changeCorpForm($('#deptor_company_info'), $(this).val());
		});

		
		/**
		* <pre>
		* 사업자 등록 중복확인 체크 Event 
		* </pre>
		* @author sungrangkong
		* @since 2013. 08. 27.
		*/
		$(document).on('click', '.customerPopup', function(e) {
			$custType = $('.deptorCustType:checked').val();
			winOpenPopup('${HOME}/debenture/getCustomerSearchForm.do?custType=' + $custType,'getCustomerSearchForm',340,120,'yes');
		});
		 
		 /*
		 * <pre>
		 * 전화번호 blur시 공백 지움
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.05.26
		 */
		 $('.phoneNo').blur(function() {
			$(this).val($(this).val().replace(/\s/gi, '')); 
		 });
		 
		 /**
	      * <pre>
	      * 여신한도, 초기미수금 클릭시 숫자 ,를 제거하는 함수 
	      * </pre>
	      * @author Jong Pil Kim
	      * @since 2013. 08. 27.
	      */ 
	      $('.numericMoney').on({
	    	  focus: function() {
		    	  $(this).val(replaceAll($(this).val(), ',', ''));
		      },
		      blur: function() {
		    	  $money = $(this).val().replace(/^0+/, '').replace(/\s/gi,'');
			      $(this).val(addComma($money));
		      }
	      });
	      
	     /**
		 * <pre>
		 * 증빙 파일 찾기 Click Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('click', '.ApplyBtn02', function() {
			 $(this).parent().find('input[type=file]').click();
		 });
		
		 /**
		 * <pre>
		 * 증빙 파일 선택되어지면 디자인 되어진 input box에 값을 넣는 Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('change', 'input[type=file]', function() {
			 $(this).parent().find('input[type=text]').val($(this).val());
		 });
		 
		 /**
		 * <pre>
		 * 파일 추가하기 (+) 버튼 Click Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('click', '.btn_plus', function(){

			 $parentEL = $(this).parent().parent();
			 $cloneEL = $parentEL.clone();

			 if($cloneEL.find('.btn_minus').length == 0){
			 	$cloneEL.find('.dataUpWrap02').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			 }
			 
			 $parentEL.find('.dataUpWrap02 > a').remove();
			 $parentEL.find('.dataUpWrap02').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			 
			 $parentEL.after("<div class='spliter'></div>");
			 $cloneEL.find('input[type=file]').val('');
			 $parentEL.next().after($cloneEL);
			 
		 });
		 
		 /**
		 * <pre>
		 * 파일 추가하기 (-) 버튼 Click Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('click', '.btn_minus', function(){
			 
			 $parentEL = $(this).parent().parent();
			 $tdEL = $parentEL.parent();
			 
			 // 이미 삭제 대상일 경우 로직을 수행하지 않음
			 if ($parentEL.find('span').hasClass('deleteFileSpan')) {
				 return false;
			 }
			 
			 // 마지막 데이터의 경우 input box 안의 내용만 비워준다.
			 if($tdEL.find('.dataUploadArea > input[type=file]').length != 1 && $parentEL.find('span').length == 0){
				 
				 if($parentEL.next().attr('class') == 'spliter'){
					 $parentEL.next().remove();	 
				 }else{
					 $parentEL.prev().remove();
				 }
				 $parentEL.remove();
				 
			 }else if($parentEL.find('span').length == 1){
				 
				 $parentEL.find('span').addClass('deleteFileSpan');
				 $parentEL.find('span').css('text-decoration','line-through');
				 
			 }else if($tdEL.find('.dataUploadArea > input[type=file]').length == 1){
				// Firefox 용 input file 초기화
				$tdEL.find('.dataUploadArea > input[type=file]').val('');
				
				// IE & Chrome 용 input file 초기화
				$tdEL.find('.dataUploadArea > input[type=file]').replaceWith( $tdEL.find('.dataUploadArea > input[type=file]').clone(true));
				
			 }
			 
			// 저장 후 변경시 title td에 changeEl 클래스 값 줌.
			 if ($('#step4_status').val() == 'complete') {
				 $tdEL.addClass('changeEl');
			 }
			 
			 $last = $tdEL.find('.dataUploadArea').last();
			 $last.find('.dataUpWrap02').remove();
			 $last.append('<div class="dataUpWrap02"><a class="btn_plus" href="#none"><img src="${IMG}/common/ico_data_p.gif"></a>&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a></div>');
			 
			 if ($parentEL.find('span > a').attr('data-deleteDebtFileSN') != null) {
				 $deleteDebtFileSN = $('#delete_debt_file_SN').val();
				 $deleteDebtFileSN += ($deleteDebtFileSN.length > 0 ? "," : "") + $parentEL.find('span > a').attr('data-deleteDebtFileSN');
				 $('#delete_debt_file_SN').val($deleteDebtFileSN);
			 }
			 
		 });
		 
		 /*
		 * <pre>
		 * 미수채권 전체선택 CHECKBOX CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.23
		 */
		 $('#check_all_bond').on({
			click : function() {
				if($('#check_all_bond').is(':checked')){
					 
					 if ($('.checkBond').length > 0) {
						// 체크박스 모두 체크되도록 속성 변경
						 $('#bond_list').find('input:checkbox').attr("checked", "checked");

						 // 채권리스트에 있는 모든 체크박스 개수
						 $('#selected_bond').find('p').html($('#bond_list').find('input:checkbox').length+"건");
						 
						 $sum = 0;
						 $('#bond_list').find('tr').each(function(){
							 // 채권리스트의 각 미수금액을 모두 더함
							 $sum += parseInt($(this).find('td:eq(4)').text().replace(/[^0-9]/gi, ''), 10);
						 });
						 
						 // 선택채권 미수금총액
						 $('#outstanding_sum').find('p').html(addComma(new String($sum))+'원');
						 $('#tot_unpd_amt').val($('#outstanding_sum').text().replace(/[^0-9]/gi, ''));
					 }
					 
				 }else{
					 // 체크박스 모두 해제되도록 속성 삭제
					 $('#bond_list').find('input:checkbox').removeAttr("checked");
					 
					 // 선택채권과 미수금총액 출력 html 변경
					 $('#selected_bond').find('p').html("0건");
					 $('#outstanding_sum').find('p').html("0원");
				 }
			} 
		 });

		 /*
		 * <pre>
		 * 미수채권 개별선택 CHECKBOX CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.23
		 */
		 $(document).on('click', '.checkBond', function() {

			// 체크해제된 값이 있다면, 전체체크선택 checkbox는 해제한다.
			if($('#bond_list').find("input:not(:checked)").length > 0){
				$("#check_all_bond").removeAttr("checked");
			}
			
			// 선택채권 개수
			$('#selected_bond').find('p').html($('#bond_list').find('input:checkbox:checked').length+"건");
				
			// 선택한 채권들의 미수금액을 모두 합하여 계산
			$bond_sum = parseInt($('#outstanding_sum').find('p').html().replace(/[^0-9]/gi, ''), 10);
			if($(this).is(':checked')){
				$add_mnt = parseInt($(this).parent().siblings('td').eq(3).text().replace(/[^0-9]/gi, ''), 10);
				$bond_sum += $add_mnt;
			}else{
				$add_mnt = parseInt($(this).parent().siblings('td').eq(3).text().replace(/[^0-9]/gi, ''), 10);
				$bond_sum -= $add_mnt;
			}
				
			// 선택채권 미수금총액
			$('#outstanding_sum').find('p').html(addComma(new String($bond_sum))+'원');
			$('#tot_unpd_amt').val($('#outstanding_sum').text().replace(/[^0-9]/gi, ''));
			 
		 });
		 
		 /*
		 * <pre>
		 * 채무불이행 가이드 건만 보이는 정렬 EVENT
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.06.11
		 */
		 $('#debt_guide_view').click(function() {
			 //$.getDebentureList();
		 });
		 
		 /*
		 * <pre>
		 * 미수채권리스트 정렬 BUTTON CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.24
		 */
		 $('#btn_search_bond').on({
			click : function(e) {

				 if($.trim($('#cust_id').val()) != ''){
					 //$.getDebentureList();
				 }else{
					alert("채무자 정보를 입력하신 후, 미수 채권을 조회해 주세요.");
					
					$('#btn_step_two_title').trigger("click");
				 }
			} 
		 });
		
		/*
		* <pre>
		* 증빙자료 업로드 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.25
		*/
		$('#btn_upload_attachment').on({
			click : function(e) {
				
				$('#step').val('step4'); // 현재 step 설정
				
				$result = false;
				
				$('.dataUploadArea').find('input').each(function() {
					if($.trim($(this).val()) != '') {
						$result = true;
						return false;
					}
				});
				
				// 파일 확장자 검증
				$fileRegistTF = true;
				$('.dataUploadArea').find('input:file').each(function(e){
					var result = checkUploadFile(this);
					if(!result){
						$fileRegistTF = false;
						return false;
					}
				});
				
				if(!$fileRegistTF){
					return false;
				}
				
				if (!$result) {
					if ($('.dataUploadArea').find('span').length > 0) {
						// 이미 증빙된 파일이 삭제된 파일인지 확인
						$('.dataUploadArea').find('span').each(function() {
							$cssArr = $(this).css('text-decoration').split(' ');
							if ($cssArr[0] != 'line-through') {
								$result = true;
								return false;
							}
						});
					} else {
						$result = false;
					}
				}
				
				if($result) {
					$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
					 .ajaxSubmit({
					 	dataType : "json",
						async : false,
						success : function(msg){
							$result = msg;
		 					if($result.REPL_CD == '00000') {
		 						alert("4단계의 정보를 저장 완료했습니다.");
		 						$('#file_area').find('.changeEl').removeClass('changeEl');
		 						$('#step4_status').val('complete');
		 						
		 						$('#delete_debt_file_SN').val('');
		 						
		 						$.getDebtPrfFileList($result.debtPrfFileList);
		 						
		 						// 진행상태 변경
		 						$('#currentStat').text(($result.STAT == 'SC') ? "저장완료(신청가능)" : "임시저장");
		 						if ($result.STAT == 'SC') {
			 						$('#btn_save_apply_form').hide();
			 						$('#btn_apply').show();								
								} else {
			 						$('#btn_save_apply_form').show();
			 						$('#btn_apply').hide();
								}
		 					} else {
		 						alert(utfDecode($result.REPL_MSG));
		 					}
						},
						error : function(xmlHttpRequest,textStatus,errorThrown) {
							alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
						}
					});
				} else {
					alert("반드시 1개 이상의 거래증빙자료를 첨부해야 합니다.");
					return false;
				}
			}
		});
		

		/**
		* <pre>
		* 복사 후 작성 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.09.
		*/
		$("#btn_copy_write").click(function(e){
			if(confirm("채무불이행 신청서를 복사 후 작성 하시겠습니까?")) {
				$copyfile = (confirm("STEP 04의 첨부파일을 함께 복사하시겠습니까?")) ? "Y" : "N";
				$param = {
					debtApplId : $("#debt_appl_id").val(),
					custId : $("#cust_id").val(),
					copyfile : $copyfile
				};
				
				location.href="${HOME}/debt/getDebtCopyAndWrite.do?" + $.param($param);
			}
		});
		
		/*
		* <pre>
		* STEP01단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/		
		$('#step1 input[type=text]').on('input', function() {
			if ($('#step1_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step1 select').on('change', function() {
			if ($('#step1_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // select box 변경시 상태 변경
		
		/*
		* <pre>
		* STEP02단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#step2 input[type=text]').on('input', function() {
			if ($('#step2_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step2 select').on('change', function() {
			if ($('#step2_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // select box 변경시 상태 변경
		
		/*
		* <pre>
		* STEP03단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#step3 input[type=text]').on('input', function() {
			if ($('#step3_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step3').on('change', '.checkBond', function() {
			if ($('#step3_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 체크박스 변경시 상태 변경
		
		/*
		* <pre>
		* STEP04단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#file_area').on('change', 'input[type=file]', function() {
			if ($('#step4_status').val() == 'complete') {
				$(this).parent().parent().addClass('changeEl');
			}
		}); // 파일 변경시 상태 변경
		
		/*
		* <pre>
		* 각 STEP 별 [수정] 버튼 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.07
		*/
		$(document).on('click','.on.btn_step1_step2_modify',function(e){
			
			$('#' + $(this).attr('data-step') + '_clone_area').html($(this).parent().parent().parent().clone());
			
			$parent = $(this).parent();
			
			$("." + $(this).attr('data-step') + '_modify_toggle').toggle();
			$("." + $(this).attr('data-step') + '_readOnly_toggle').toggle();
			
			$parent.find(".on.btn_step1_step2_modify , .on.btn_step1_step2_save , .on.btn_step1_step2_cancel").toggle();
	
			
		});
		
		/*
		* <pre>
		* 각 1,2,3,4 STEP 별 [저장] 버튼 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.07
		*/
		$(document).on('click','.on.btn_step1_step2_save,.btn_step3_save',function(e){
			$resultAjax = null;
			$thisEL = $(this);
			$step = $thisEL.attr('data-step');
			$('#step').val($step); // 현재 step 설정
			
			// 숫자값만 추출 (1,2,3,4단계)
			$digitStep = $step.replace(/[^0-9]/g, '');
			
			// Step1 저장
			if ($.fn.validate($('#' + $step))) {
				
				$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
				 .ajaxSubmit({
				 	dataType : "json",
					async : false,
					success : function(msg){
						$resultAjax = msg;
						if ($resultAjax.REPL_CD == '00000') {
							$('#debt_appl_seq').val($resultAjax.NPM_ID);
							alert($step+"단계의 정보를 저장 완료했습니다.");
							$('#' + $step).find('.changeEl').removeClass('changeEl');
							$('#' + $step + '_status').val('complete');
							
							var i = 0;
							switch($digitStep){
								case '1' :
									// 법인등록번호
									if ($('#apply_corp_num').val() != ""){
										$('.' + $step + '_readOnly_toggle').eq(i++).text(($('#apply_corp_num').val()).substring(0,6) + '-' + ($('#apply_corp_num').val()).substring(6,13));
									} else if ($('#apply_corp_num').val() == ""){
										$('.' + $step + '_readOnly_toggle').eq(i++).text($('#apply_corp_num').val());
									}
									// 법인등록번호
									/* $('.' + $step + '_readOnly_toggle').eq(i++).text($('#apply_corp_num').val()); */
									
									// 주소
									$addr = '(' + $('#apply_post_no1').val() + ') ' + $('#apply_addr_1').val() + ' ' + $('#apply_addr_2').val(); //'-' + $('#apply_post_no2').val() +
									$('.' + $step + '_readOnly_toggle').eq(i++).text($addr);
									// 부서
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#manager_part').val());
									// 직위
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#manager_position').val());
									// 전화
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#tel_no1').val() + ' - ' + $('#tel_no2').val() + ' - ' + $('#tel_no3').val());
									
									// 휴대폰
									$hp_no = ($('#mb_no2').val().length < 3 || $('#mb_no3').val().length != 4) ? "" : $('#mb_no1').val() + ' - ' + $('#mb_no2').val() + ' - ' + $('#mb_no3').val();
									$('.' + $step + '_readOnly_toggle').eq(i++).text($hp_no);
									
									// 팩스
									$fax_no = ($('#fax_no2').val().length < 3 || $('#fax_no3').val().length != 4) ? "" : $('#fax_no1').val() + ' - ' + $('#fax_no2').val() + ' - ' + $('#fax_no3').val();
									$('.' + $step + '_readOnly_toggle').eq(i++).text($fax_no);
									
									// 이메일
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#email1').val() + '@' + $('#email2').val());
									
									break;
								case '2' :
									// 법인등록번호
									if ($('#deptor_corp_num').val() != ""){
										$('.' + $step + '_readOnly_toggle').eq(i++).text(($('#deptor_corp_num').val()).substring(0,6) + '-' + ($('#deptor_corp_num').val()).substring(6,13));
									} else if ($('#deptor_corp_num').val() == ""){
										$('.' + $step + '_readOnly_toggle').eq(i++).text($('#deptor_corp_num').val());
									}
									// 법인등록번호
									/* $('.' + $step + '_readOnly_toggle').eq(i++).text($('#deptor_corp_num').val()); */
									
									// 주소
									$addr = '(' + $('#post_no1').val() + ')  ' + $('#cust_addr_1').val() + ' ' + $('#cust_addr_2').val(); //+ '-' + $('#post_no2').val()
									$('.' + $step + '_readOnly_toggle').eq(i++).text($addr);
									
									// 채무구분
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#dept_division option:selected').text());
									
									// STEP03 form reset
									$('#bond_list').html("");
									
									// STEP3 데이터 셋팅
									$('#bond_sum').val($('#nonpay_sum').val());
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#nonpay_sum').val()+'원'); // 채무불이행 금액
									
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#overdue_open_date').val());
									
									$('.' + $step + '_readOnly_toggle').eq(i++).text($('#reason_occur_date').html());
									
									$('#step2').find('.changeEl').removeClass('changeEl');
									$('#step2_status').val('complete');
									
									// 채권 증빙 파일 순번과 지울 채무불이행 증빙파일 순번 value reset
									$('#delete_debt_file_SN').val("");
									
									// 거래처의 미수 채권 가져오기
									//$.getDebentureList();
									
									break;
									
								case '3' :
									
									// 거래처의 미수 채권 가져오기
									//$.getDebentureList();
									
									// 채권 증빙 파일 순번과 지울 채무불이행 증빙파일 순번 value reset
									$('#delete_debt_file_SN').val("");
									
									$('#step3').find('.changeEl').removeClass('changeEl');
									$('#step3_status').val('complete');

									// STEP02의 채무불이행 금액 Setting
									$('#nonpay_sum').val($('#bond_sum').val());
									
									$('#deptor_company_info').find('td').last().text($('#bond_sum').val());
									
									if ($resultAjax.prfFileList != null && $resultAjax.prfFileList != '') {
										$.getDebtPrfFileList($resultAjax.prfFileList); // 증빙자료 조회하여 출력
									} else {
										// STEP04 form reset
										$('#file_area > table').remove();
										$('#file_area').prepend($('#hidden_file_area').html());
									}
									break;
							}
							
							// Step1 , Step2 공통영역
							if($digitStep == 1 || $digitStep == 2){
								// 새롭게 저장된 것을 clone 해서 임시저장영역에 복사한다.
								$('#' + $thisEL.attr('data-step') + '_clone_area').html($(this).parent().parent().parent().clone());
								
								// toggle 작업
								$parent = $thisEL.parent();
								$("." + $thisEL.attr('data-step') + '_modify_toggle').toggle();
								$("." + $thisEL.attr('data-step') + '_readOnly_toggle').toggle();
			
								$thisEL.parent().find(".on.btn_step1_step2_modify").toggle();
								$thisEL.parent().find(".on.btn_step1_step2_save").toggle();
								$thisEL.parent().find(".on.btn_step1_step2_cancel").toggle();
							}
							
							// 진행상태 변경
	 						$('#currentStat').text(($resultAjax.STAT == 'SC') ? "저장완료(신청가능)" : "임시저장");
							if ($resultAjax.STAT == 'SC') {
		 						$('#btn_save_apply_form').hide();
		 						$('#btn_apply').show();								
							} else {
		 						$('#btn_save_apply_form').show();
		 						$('#btn_apply').hide();
							}

						}
					},
					error : function(xmlHttpRequest,textStatus,errorThrown) {
						alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
						$resultAjax = null;
					}
				});

			}
			
		});
		
		/*
		* <pre>
		* 각 STEP 별 [취소] 버튼 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.07
		*/
		$(document).on('click','.on.btn_step1_step2_cancel',function(e){

			$(this).parent().parent().parent().html($('#' + $(this).attr('data-step') + '_clone_area > .Apply_slide').html());
			
			$parent = $(this).parent();

			$("." + $(this).attr('data-step') + '_modify_toggle').toggle();
			$("." + $(this).attr('data-step') + '_readOnly_toggle').toggle();
			
		    $(this).parent().find(".on.btn_step1_step2_modify").toggle();
			$(this).parent().find(".on.btn_step1_step2_save").toggle();
			$(this).parent().find(".on.btn_step1_step2_cancel").toggle();
			

		});
		
		/**
		* <pre>
		* 프린트 미리보기 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.09.
		*/
		$("#btn_print").click(function(e){
			if( navigator.userAgent.indexOf("MSIE") > 0 ){
				window.print();
			}else{
				window.print();
			}
		});
		
		/**
		* <pre>
		* 목록으로 바로가기 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.09.
		*/
		$("#btn_list").click(function(e){
			location.href="${HOME}/debt/getDebtInquiryList.do";
		});
		
		/**
		* <pre>
		* 채무불이행 삭제 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.09.
		*/
		$("#btn_delete").click(function(e){
			if(confirm("채무불이행 신청서를 삭제 하시겠습니까?")) {
				$param = {debtIdList : $("#debt_appl_id").val()};
				location.href="${HOME}/debt/removeDebt.do?" + $.param($param);	
			}			
		});

		/*
		* <pre>
		* 작성완료 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.27
		*/
		$('#btn_save_apply_form').click(function(e){

			$('#step').val('all'); // 현재 step 설정
			
			// validation이 제대로 되지 않으면 스크립트 정지
			if(!$.fn.validate($('form'))) {
				return false;
			}
			
			// FORM에서 변경된 요소가 1개 이상일 때 문구 추가
			if ($('form').find('.changeEl').length > 0) {
				if(!confirm('수정 사항이 있습니다. 저장하지 않고 계속 진행하시겠습니까?')) {
					return false;
				}
			}

			// ajaxSubmit을 통해 데이터 등록
			$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
			 .ajaxSubmit({
			 	dataType : "json",
				async : false,
				success : function(msg){
					$result = msg;
					if($result.REPL_CD == '00000') {
						alert("채무불이행신청서의 정보를 저장 완료했습니다.");
						
						$param = {
							custId : $('#cust_seq').val(),
							debtApplId : $('#debt_appl_seq').val()
						};
						
						// 신청서 수정 페이지로 이동함
						location.href="${HOME}/debt/getDebtInquiry.do?" + $.param($param);
					} else {
						alert(utfDecode($result.REPL_MSG));
					}
				},
				error : function(xmlHttpRequest,textStatus,errorThrown) {
					alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
				}
			});

		});
		
		/**
		* <pre>
		* 신청 CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.05.09.
		*/
		$("#btn_apply").click(function(e){
			
			// validation이 제대로 되지 않으면 스크립트 정지
			if(!$.fn.validate($('form'))) {
				return false;
			}
			
			// FORM에서 변경된 요소가 1개 이상일 때 문구 추가
			if ($('form').find('.changeEl').length > 0) {
				if(!confirm('수정 사항이 있습니다. 저장하지 않고 계속 진행하시겠습니까?')) {
					return false;
				}
			}
			
			// 현재 수정작업중인 STEP이 있는지 체크함.
			if($(".on.btn_step1_step2_save:visible").length != 0){
				alert("현재 수정 작업중인 STEP이 존재합니다.");
				return false;
			}
			
			if (confirm("심사는 최대 3일 소요됩니다. 신청하시겠습니까?")) {

				if ($('#cust_seq').val() != '') {
					
					$param = {npmCustId : $('#cust_seq').val()};
					
					$.ajax({
						type : "POST",
						url : "${HOME}/debt/getUnderWayDebtTotalCount.do",
						dataType : "json",
						data : $.param($param),
						async : false,
						success : function(msg) {
							$result = msg.model.sBox;
							
							if($result.REPL_CD == '00000') {
								
								$param = {debtApplId : $("#debt_appl_id").val()};
								location.href="${HOME}/debt/getDebtConfirmation.do?" + $.param($param);
								
							} else {
								alert($result.REPL_MSG);
							}
						},
						error : function(xmlHttpRequest, textStatus, errorThrown) {
							alert("채무불이행 신청 도중 에러 발생 [" + textStatus + "]");
						}
					
					});
					
				} else {
					
					$param = {debtApplId : $("#debt_appl_id").val()};
					location.href="${HOME}/debt/getDebtConfirmation.do?" + $.param($param);
					
				}
			}
			
		});
		
		/*
		* <pre>
		* 첨부파일 다운로드 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.07.04
		*/
		$(document).on('click','.browsing',function() {
			$("#file_path").val($(this).attr("data-filePath"));
			$("#file_nm").val($(this).text());
			
			$('form').attr({'action':'${HOME}/debt/getDebtPrfFileDownload.do', 'method':'POST'}).submit();
		});
		
	});
	
	/**
	* <pre>
	* 거래처 검색 팝업 종료 후 처리
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 06.
	* @param $custNo : 사업자번호, $custType : 거래처유형(P or C), $custId : 거래처 순번, $ltdDt : 만기일, $custOwnNm : 거래처 대표자명, $custCorpNo : 거래처 법인등록번호, 
	*        $custPost1 : 거래처 우편번호1, $custPost2 : 거래처 우편번호2 , $custAddr1 : 거래처 주소1, $custAddr2 : 거래처 주소2
	*/
	$.fn.exitPopup = function($custNm, $custNo, $custType, $custId, $ltdDt, $custOwnNm, $custCorpNo, $custPost1, $custAddr1, $custAddr2){ // $custPost2,
		
//		$msg = ($('.deptorCustType:checked').val() == 'C') ? "법인사업자" : "개인사업자"; 
		$msg = "C";
//		if( $('.deptorCustType:checked').val() != $custType ) {
//			alert("구분(" + $msg + ") 팝업을 이용해주세요.");	
//			return;
//		}
		
		// 팝업창 데이터 setting
		$("#cust_nm").val($.trim($custNm));
		$("#cust_no").val($.trim($custNo));
		$("#cust_id").val($.trim($custId));
		$('#deptor_name').val($.trim($custOwnNm));
		$('#deptor_corp_num').val($.trim($custCorpNo));
		$('#post_no1').val($.trim($custPost1));
		/* $('#post_no2').val($.trim($custPost2)); */
		$('#cust_addr_1').val($.trim($custAddr1));
		$('#cust_addr_2').val($.trim($custAddr2));
		
		if ($('#step2_status').val() == 'complete') {
			$("#cust_nm").addClass('changeEl');
			$("#cust_no").addClass('changeEl');
			$("#cust_id").addClass('changeEl');
		}
	};
	
	/*
	* <pre>
	* 회사 구분에 따른 form 변경
	* </pre>
	* @author sungrangkong
	* @since 2014.04.17
	*/
	$.changeCorpForm = function($table_form, $custType) {
		
		$table_form.find('input:text').val("");
		
		if($table_form.attr("id") == 'deptor_company_info') {
			// 법인등록번호 부분 html
			$corp_num_html = '<td class="title change">법인등록번호</td><td class="change">';
			$corp_num_html += '<span class="step1_modify_toggle">';
			$corp_num_html += '<input type="text" id="deptor_corp_num" name="deptorCorpNum" class="default_txtInput corp_no numeric" style="width: 200px;" title="법인등록번호" maxlength="13">';
			$corp_num_html += '</span>';
			$corp_num_html += '<span class="step1_readOnly_toggle">';
			$corp_num_html += '</span>';
			$corp_num_html += '</td>';
				
			// 회사구분이 개인사업자(I)일 때의 폼 변경
			if($custType == 'I') {
				$table_form.find('.change').remove();
				$table_form.find('tr:eq(2) > td:eq(1)').attr("colspan", "3");
			// 회사구분이 법인사업자(C)일 때의 폼 변경
			} else {
				$table_form.find('tr:eq(2) > td:eq(1)').removeAttr("colspan");
				$table_form.find('tr:eq(2)').append($corp_num_html);
				
			}
		} else {
			// 업종 부분 html
			$biz_type_html = '<td class="title change">업종<span class="em_po">*</span></td><td class="default_note change">';
			$biz_type_html += '<span class="step1_modify_toggle" style="display:block">';
			$biz_type_html += '<input type="text" id="apply_biz_type" name="applyBizType" class="default_txtInput required" style="width: 200px;" title="업종" maxlength="40">';
			$biz_type_html += '</span>';
			$biz_type_html += '<span class="step1_readOnly_toggle">';
			$biz_type_html += '</span>';
			$biz_type_html += '</td>';
		
			// 법인등록번호 부분 html
			$corp_num_html = '<td class="title change">법인등록번호</td><td class="change">';
			$corp_num_html += '<span class="step1_modify_toggle" style="display:block">';
			$corp_num_html += '<input type="text" id="apply_corp_num" name="applyCorpNum" class="default_txtInput corp_no numeric" style="width: 200px;" title="법인등록번호" maxlength="13">';
			$corp_num_html += '</span>';
			$corp_num_html += '<span class="step1_readOnly_toggle">';
			$corp_num_html += '</span>';
			$corp_num_html += '</td>';
			
			// 회사구분이 개인사업자(I)일 때의 폼 변경
			if($custType == 'I') {
				$table_form.find('.change').remove();
				$table_form.find('tr:eq(3) > td').remove();
				$table_form.find('tr:eq(2)').append($biz_type_html);
			// 회사구분이 법인사업자(C)일 때의 폼 변경
			} else {
				$table_form.find('.change').remove();
				$table_form.find('tr:eq(3)').append($biz_type_html);
				$table_form.find('tr:eq(2)').append($corp_num_html);
				$table_form.find('tr:eq(3) > td:eq(1)').attr("colspan", "3");
			}
		}
		
	};
	
	/*
	* <pre>
	* AJAX를 통해 form의 정보를 저장하는 FUNCTION
	* </pre>
	* @author sungrangkong
	* @since 2014.04.19
	* @param $step : 저장할 단계 , $next_title : 임시저장이 끝난 후 펼칠 단계
	*/
	$.saveInfoViaAjax = function($step){
		$result = null;
			$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
			 .ajaxSubmit({
			 	dataType : "json",
				async : false,
				success : function(msg){
					$result = msg;
					if($result.REPL_CD == '00000') {
						$('#debt_appl_seq').val($result.NPM_ID);
						alert($step+"단계의 정보를 저장 완료했습니다.");
					} else {
						alert(utfDecode($result.REPL_MSG));
					}
				},
				error : function(xmlHttpRequest,textStatus,errorThrown) {
					alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
					$result = null;
				}
			});

		return $result;
	};
	
	/**
	 * <pre>
	 * 폼 전송 파라미터 검증 중 경고창 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
	 */
	 $.fn.validateResultAndAlert = function($el, $message){
		 alert($message);
		 $el.focus();
		 return false;
	 }; 
	
	/**
	 * <pre>
	 * 전송 파라미터 검증 함수
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 04. 18
	 */
	 $.fn.validate = function($validationArea){
		
		$result = true;
		 
		$formEl = $validationArea.find('input');
		$.each($formEl, function(){
			
			// 필수 공백 검증
			if($(this).hasClass('required')){
				if($.trim($(this).val())=='') {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
					return $result; 
				}
			}
			
			if($.trim($(this).val()) != '') {

				// 사업자 번호 숫자 검증
				if($(this).hasClass('usrNo')){
					if(!typeCheck('numCheck',$.trim($(this).val().replace(/-/gi, '')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}
					
					if($(this).val().replace(/-/gi, '').length < 10) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 10자리여야 합니다.');
						return $result;
					}
				}
				
				// 숫자 검증
				if($(this).hasClass('numeric')){
					if(!typeCheck('numCheck',$.trim($(this).val()))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}
					
					if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
				
				//우편번호
				if($(this).hasClass('postNo')){
					if($(this).val().length != 5){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 5자리여야 합니다.');
						return $result;
					}
				}
				
				// 전화번호, 휴대폰번호, 팩스번호 자릿수 검증
				if($(this).hasClass('phoneNo')){
					if($(this).hasClass('midNo') && $(this).val().length < 3){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 3자리보다 커야 합니다.');
						return $result;
					}
					if($(this).hasClass('lastNo') && $(this).val().length != 4){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 4자리여야 합니다.');
						return $result;
					}
					if($(this).siblings('.phoneNo').val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this).siblings('.phoneNo'), $(this).siblings('.phoneNo').attr('title') + '를 확인해주세요.');
						return $result;
					}
				}
				
				// 화폐 검증
				if($(this).hasClass('numericMoney')){
					if(!(typeCheck('numCheck',replaceAll($.trim($(this).val()),',','')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
						return $result;
					}	
					
					if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
				
				if($(this).hasClass('nonpaySum')){
					if(replaceAll($.trim($(this).val()),',','') < 50000){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 50,000원 이상만 입력 가능합니다.');
						return $result;
					}
				}
			}
			// 반복문 정지
			if(!$result) return $result;
		});
		
		// 스크립트 정지
		if(!$result) return $result;
		
		// 사업자 번호 검증[문자열 형태 검증]
		$usrNoEl = $validationArea.find('.usrNo');
		if($usrNoEl != null && $.trim($usrNoEl.val()) != ''){
			$usrNo = $usrNoEl.val().replace(/-/gi, '');
			if($.trim($usrNo) != '') {
				if(!typeCheck('businessNoCheck', $usrNo)) {
					alert("사업자 번호 형식에 맞지 않습니다.");
					$result = false;
					return $result;
				}
			}
		}
		
		// 이메일 검증[문자열 형태 검증]
		$emailEl1 = $validationArea.find('#email1');
		$emailEl2 = $validationArea.find('#email2');
		if($.trim($emailEl1.val()) != '' || $.trim($emailEl2.val()) != ''){
			if(!typeCheck('emailCheck', $.trim($emailEl1.val()) + '@' + $.trim($emailEl2.val()))){
				alert('이메일을 확인해 주세요.');
				$result = false;
				return $result;
			}	
		}
		
		// 법인 등록번호 검증[문자열 형태 검증]
		$corpNoEl = $validationArea.find('.corpNo');
		if($.trim($corpNoEl.val()) != ''){
			if(!typeCheck('corpNoCheck',replaceAll($.trim($corpNoEl.val()),'-',''))){
				alert('법인등록번호 형식에 맞지 않습니다.');
				$result = false;
				return $result;
			} 
		}
		
		// Step3인 경우만 검증하도록 한다.
		/* if($validationArea.attr('id') == 'step3'){			
			if($.trim($('#bond_sum').val()).replace(/,/gi, '') != $.trim($('#tot_unpd_amt').val()).replace(/,/gi, '')){
				if (!confirm('채무불이행 금액과 선택채권미수총금액이 다릅니다. 계속 진행하시겠습니까?')) {
					return false;
				}
			}
		} */
		 
		return $result;
		
	 };
	 
	 /*
	 * <pre>
	 * 거래처에 따른 미수채권 조회 AJAX EVENT
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.04.22
	 */
	 $.getDebentureList = function() {
		 $param = {
				 custId : $('#cust_id').val(),
				 debtApplId : $("#debt_appl_id").val(),
				 orderCondition : $('#order_condition').val(),
				 orderType : $('#order_type').val(),
				 bondMbrId : $('#bond_mbr_id').val(),
				 debtGuideView : ($('#debt_guide_view').is(':checked') ? 'on' : 'off')
		 };
		 
		 $.ajax({
			type : "POST", 
			url : "${HOME}/debt/getDebentureListOfCustomer.do",
			dataType : "json",
			data : $.param($param),
			async : false,
			success : function(msg) {
						$result = msg.model.sBox;
						$bond_list = $result.debentureList;
						
						if($result.REPL_CD == '00000') {
							
							// 미수채권 리스트 HTML
							$bond_list_html = "";
							if($bond_list.length != 0){
								$.each($bond_list, function() {
									$contents = $(this).eq(0).get(0);
									$checked = ($contents.DEBT_APPL_ID != null) ? 'checked' : '';
									$bond_list_html += '<tr>';
									$bond_list_html += '<td><input type="checkbox" class="checkBond" name="debnId" value="'+$contents.DEBN_ID+'" ' + $checked +  ' ></td>';
									$bond_list_html += '<td>' + $contents.BILL_DT + '</td>';
									$bond_list_html += '<td class="list_right">'+$contents.SUM_AMT+'원</td>';
									$bond_list_html += '<td>'+$contents.DELAY_DAY+'일</td>';
									$bond_list_html += '<td class="list_right term last">'+$contents.RMN_AMT+'원</td>';
									$bond_list_html += '</tr>';
								});
							}else{
								$bond_list_html += '<tr>';
								$bond_list_html += '<td colspan="5">';
								$bond_list_html += '검색된 데이터가 존재하지 않습니다.';
								$bond_list_html += '</td>';
								$bond_list_html += '</tr>';
							}
							$('#bond_list').html("");
							$('#bond_list').append($bond_list_html);
							
						} else {
							alert($result.REPL_MSG);
						}
						
					},
			error : function(xmlHttpRequest, textStatus, errorThrown){
						alert("거래처 미수채권 조회 도중 에러발생 [" + textStatus + "]");
					}
		});
	 };
	
	/*
	* <pre>
	* 채무불이행신청 증빙파일 리스트 출력
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.02
	*/
	$.getDebtPrfFileList = function($debtPrfFileList) {

		// 기존 요소 삭제
		$('#file_area > table').remove();
		$('#file_area').prepend($('#hidden_file_area').html());
		
		$.each($debtPrfFileList, function(index) {
			$fileHtml = '';
			$fileEl = $(this).get(0);
			
			$appendField = ($fileEl.TYPE_CD =='A') ? "data_A" : (($fileEl.TYPE_CD =='B') ? "data_B" : (($fileEl.TYPE_CD =='C') ? "data_C" : (($fileEl.TYPE_CD =='D') ? "data_D" : "data_E"))) ;
			$fileHtml += '<div class="dataUploadArea">';
			$fileHtml += '<span style="display:inline-block; margin: 5px 0px 5px 0px;">';
			$fileHtml += '<a href="#none" class="browsing" data-filePath="'+utfDecode($fileEl.FILE_PATH)+'" data-deleteDebtFileSN="'+$fileEl.DEBT_FILE_SN+'" style="color:#4f7eb2; text-decoration:none;">'+utfDecode($fileEl.FILE_NM)+'</a></span>';
			$fileHtml += '<div class="dataUpWrap02">';
			$fileHtml += '<a href="#none" class="btn_minus"><img src="${IMG}/common/ico_data_m.gif"></a>';
			$fileHtml += '</div></div><div class="spliter"></div>';
			
			$('#'+$appendField).prepend($fileHtml);
		});
		
	};
	
	/*
	* <pre>
	* 회원정보 팝업 수정 콜백함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.23
	
	$.fn.modifyUserInfo = function($userNm, $deptNm, $jobTlNm, $telNo1, $telNo2, $telNo3, $mbNo1, $mbNo2, $mbNo3, $faxNo1, $faxNo2, $faxNo3, $email1, $email2, $selectEmail) {
		var i = 1; // index
		
		$('#manager_name').val($userNm);
		$('.step1_readOnly_toggle').eq(i++).text($userNm);
		$('#manager_part').val($deptNm);
		$('.step1_readOnly_toggle').eq(i++).text($deptNm);
		$('#manager_position').val($jobTlNm);
		$('.step1_readOnly_toggle').eq(i++).text($jobTlNm);
		$('#tel_no1').val($telNo1);
		$('#tel_no2').val($telNo2);
		$('#tel_no3').val($telNo3);
		$('.step1_readOnly_toggle').eq(i++).text($telNo1 + "-" + $telNo2 + "-" + $telNo3);
		$('#mb_no1').val($mbNo1);
		$('#mb_no2').val($mbNo2);
		$('#mb_no3').val($mbNo3);
		$('.step1_readOnly_toggle').eq(i++).text($mbNo1 + "-" + $mbNo2 + "-" + $mbNo3);
		$('#fax_no1').val($faxNo1);
		$('#fax_no2').val($faxNo2);
		$('#fax_no3').val($faxNo3);
		$('.step1_readOnly_toggle').eq(i++).text(faxNo1 + "-" + faxNo2 + "-" + faxNo3);
		$('#email1').val($email1);
		$('#email2').val($email2);
		$('.step1_readOnly_toggle').eq(i++).text(faxNo3 + "@" + faxNo3);
		$('#select_email').val($selectEmail);
	};*/
	
	/*
	* <pre>
	* 채무자 우편번호 검색 팝업창
	* </pre>
	* @author youkyung Hong
	* @since 2014.05.01
	
	function open_post(){ 
		open_win('${HOME}/zip/zipSearchPopup.do?otherFunction=deptorAddressResult');
	}*/
	
	/*
	* <pre>
	* 신청인 우편번호 검색 팝업창
	* </pre>
	* @author youkyung Hong
	* @since 2014.05.01
	
	function open_post_apply(){ 
		open_win('${HOME}/zip/zipSearchPopup.do?otherFunction=resultReceiveApply');
	}*/
	
	/*
	* <pre>
	* 채무자 우편번호 값 가져오기
	* </pre>
	* @author youkyung Hong
	* @since 2014.05.01
	
	function deptorAddressResult(zip, address) {
		$('#post_no1').val(zip.substring(0,3));
		$('#post_no2').val(zip.substring(3,6));
		$('#cust_addr_1').val(address);
		$('#cust_addr_2').val("");
		
		if($('#step2_status').val() == 'complete') {
			$('#post_no1').addClass('changeEl');
			$('#post_no2').addClass('changeEl');
			$('#cust_addr_1').addClass('changeEl');
			$('#cust_addr_2').addClass('changeEl');
		}
	}*/

	/*
	* <pre>
	* 신청인 우편번호 값 가져오기
	* </pre>
	* @author youkyung Hong
	* @since 2014.05.01
	
	function resultReceiveApply(zip, address){
		$('#apply_post_no1').val(zip.substring(0,3));
		$('#apply_post_no2').val(zip.substring(3,6));
		$('#apply_addr_1').val(address);
		$('#apply_addr_2').val("");
		
		if($('#step1_status').val() == 'complete') {
			$('#apply_post_no1').addClass('changeEl');
			$('#apply_post_no2').addClass('changeEl');
			$('#apply_addr_1').addClass('changeEl');
			$('#apply_addr_2').addClass('changeEl');
		}
	}*/

</script>
</head>

<body>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0201"/>
</jsp:include>
<%-- Top Area End --%>

<!-- contentArea -->
<div id="containerWrap">
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<!-- Left Area End -->

<div id="rightWrap">

	<%-- menu별 이미지 class sub00 --%>
	<%-- HIDDEN AREA START --%>
	<input type="hidden" id="step1_status" value="complete" />
	<input type="hidden" id="step2_status" value="complete" />
	<input type="hidden" id="step3_status" value="complete" />
	<input type="hidden" id="step4_status" value="complete" />
	
	<%-- 접속한 유저의 사업자 번호 --%>
	<input type="hidden" id="usrNo" value="${sBox.sessionUsrNo}" />
	
	<div class="right_tit ltit02_0101"><em>채무불이행관리</em></div>
				
		<p class="content_tit">채무불이행 신청서 조회</p>
		<div class="howtostep">
			<img style="margin-left:5px;" src="${IMG}/common/img_default01.gif" />
		</div>

		<div class="linedot_u40"></div>

		<p class="content_tit">채무불이행 신청서</p>
	
<%-- 	<c:if test="${debtBox.TRANS ne '0'}">
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="50%">진행상태</th>
					<th width="50%">전송상태</th>
				</tr>
				<tr>
					<td>
						<c:if test="${debtBox.STAT eq 'TS'}">임시저장</c:if>
						<c:if test="${debtBox.STAT eq 'SC'}">저장완료(신청가능)</c:if>
					</td>
					<td>
						<c:if test="${debtBox.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${debtBox.TRANS eq '1'}">전성완료</c:if>
						<c:if test="${debtBox.TRANS eq '2'}">전송중</c:if>
					</td>
				</tr>	
			</tbody>
			</table>
		</div>
	</c:if>
	
	<c:if test="${debtBox.TRANS eq '0'}">
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="20">진행상태</th>
					<th width="20%">전송상태</th>
					<th width="60%">전송실패 사유</th>
				</tr>
				<tr>
					<td>
						<c:if test="${debtBox.STAT eq 'TS'}">임시저장</c:if>
						<c:if test="${debtBox.STAT eq 'SC'}">저장완료(신청가능)</c:if>
					</td>
					<td>
						<c:if test="${debtBox.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${debtBox.TRANS eq '1'}">전성완료</c:if>
						<c:if test="${debtBox.TRANS eq '2'}">전송중</c:if>
					</td>
					<td>
						<c:if test="${debtBox.RCODE ne ''}">[${debtBox.RCODE}]</c:if> ${debtBox.REASON}
					</td>
				</tr>	
			</tbody>
			</table>
		</div>
	</c:if> --%>
	
	
	<!-- <div class="empty_b7"> </div> -->
	<div id="defaultApplyWrap">
		<form autocomplete="off" enctype="multipart/form-data" method="post">
		<%-- HIDDEN AREA START --%>
		<input type="hidden" id="step" name="step" value="" />
		
		<input type="hidden" id="delete_debt_file_SN" name="deleteDebtFileSN" value="" />
		
		<input type="hidden" id="debt_appl_seq" name="debtApplSeq" value="${sBox.debtApplId}"/>
		<input type="hidden" id="tot_unpd_amt" name="totUnpdAmt" />
		<input type="hidden" name="stat" value="${debtBox.STAT}" />
		
		<input type="hidden" id="file_path" name="filePath" />
		<input type="hidden" id="file_nm" name="fileNm" />
		<%-- HIDDEN AREA END --%>
		
				
						
						
			<div class="default_Apply">
				<ul id="menu_slide">
					<li class="Apply_slide">
						<span class="slideSpan">
							
							<a href="#none" id="btn_step_one_title" class="slideSpan nobtn" style="height:2px; padding:10px 18px 18px 20px;">STEP 01. 신청인 정보</a>
							
							 <!-- ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임 -->
							<%-- <c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}"> --%>
	                       	<span class="ApplyBtn01">
								 <a class="on btn_step1_step2_modify" href="#none" data-step="step1" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px;">수정</a>
								<a class="on btn_step1_step2_save" href="#none" data-step="step1" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px; right:90px;">저장</a>
								<a class="on btn_step1_step2_cancel" href="#none" data-step="step1" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px;">취소</a>
							</span>
							<%-- </c:if> --%>
						</span>
						<ul>

							<%-- STEP01 시작 --%>
							<li class="Apply_sub">
								<div id="step1" class="ApplysubWrap">
									<c:set var="companyInfoBox" value="${result.companyInfoBox}" />
									<p class="content_dot">회사정보</p>
									
									<%-- 신청인 회사정보 form 시작 --%>
									<table id="apply_company_info" class="defaultTbl01_01" summary="회사정보 입력테이블" data-oriCompType="${debtBox.COMP_TYPE}">
										<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
<!--  										
 -->											
											<tr>
												<c:set var="COMP_NO" value="${fn:substring(debtBox.COMP_NO, 0, 3)}-${fn:substring(debtBox.COMP_NO, 3, 5)}-${fn:substring(debtBox.COMP_NO, 5, 13)}" />
												<td class="title">회사명<span class="em_po">*</span></td>
												<td>
													${debtBox.COMP_NM}
												</td>
												<td class="title">사업자번호<span class="em_po">*</span></td>
												<td>
													${COMP_NO}
												</td>
											</tr>
											<tr>
												<td class="title">대표자명<span class="em_po">*</span></td>
												<td>
													${debtBox.OWN_NM}
												</td>
												<c:choose>
												<c:when test="${debtBox.COMP_TYPE eq 'C'}">
													<td class="title change">법인등록번호</td>
													<td class="change">
														<span class="step1_modify_toggle">
															<input type="text" id="apply_corp_num" name="applyCorpNum" class="default_txtInput corpNo numeric" style="width: 200px;" title="법인등록번호" value="${debtBox.CORP_NO}" maxlength="13">
														</span>
														<span class="step1_readOnly_toggle">
															<c:if test="${!empty debtBox.CORP_NO}">
															${fn:substring(debtBox.CORP_NO,0,6)}-${fn:substring(debtBox.CORP_NO,6,13)}
															</c:if>
														</span>
													</td>
												</c:when>
												<c:otherwise>
													<td class="title change">업종<span class="em_po">*</span></td>
													<td class="default_note change">
														${debtBox.BIZ_TYPE}
													</td>
												</c:otherwise>
												</c:choose>
											</tr>
											<c:if test="${debtBox.COMP_TYPE ne 'I'}">
												<tr>
													<td class="title">업종<span class="em_po">*</span></td>
													<td class="default_note" colspan="3">
														${debtBox.BIZ_TYPE}
													</td>
												</tr>
											</c:if>
											<tr>
												<td class="title">주소<span class="em_po">*</span></td>
												<td class="default_note2" colspan="3">
													<span class="step1_modify_toggle">
														<input type="text" class="default_txtInput required  numeric postNo" id="apply_post_no1" name="applyPostNo1" title="우편번호" maxlength="5" value="${fn:trim(debtBox.POST_NO)}"  style="width: 40px;"> 
														<%-- <input type="text" class="default_txtInput required" id="apply_post_no2" name="applyPostNo2" title="우편번호 뒷자리" onClick="javascript:open_post_apply(); return false;" maxlength="3" value="${fn:substring(debtBox.POST_NO, 3, 6)}" readonly="readonly" style="width: 40px;"> --%>
														<!-- <a href="#" class="ApplyBtn02" onClick="javascript:open_post_apply(); return false;">우편번호</a> -->&nbsp; 
														<input type="text" class="default_txtInput required" id="apply_addr_1" name="applyAddr1" title="주소" style="width: 237px;" value="${debtBox.ADDR_1}" maxlength="75" > 
														<input type="text" class="default_txtInput2" id="apply_addr_2" name="applyAddr2" title="상세주소" style="width: 255px; margin-top:0px;" value="${debtBox.ADDR_2}" maxlength="75">
													</span>
													<span class="step1_readOnly_toggle">
														<c:if test="${fn:trim(debtBox.POST_NO) ne ''}">
															(${debtBox.POST_NO})
														</c:if>
														<c:if test="${(debtBox.ADDR_1 ne null) or (debtBox.ADDR_1 ne '')}">
															 ${debtBox.ADDR_1} ${debtBox.ADDR_2}
														</c:if>
													</span>
												</td>
											</tr>
										</tbody>
									</table>
									<%-- 신청인 회사정보 form 끝 --%>
									
									
									
									<%-- HIDDEN AREA START --%>
									<input type="hidden" name="usrId" value="${debtBox.REG_USR_ID}" />
									<%-- HIDDEN AREA END --%>
									<p class="content_dot2">정보등록 담당자</p>
									<table id="apply_manager_info" class="defaultTbl01_01" summary="정보등록 담당자">
										<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">성명<span class="em_po">*</span></td>
												<td>
													${debtBox.USR_NM}
												</td>
												<td class="title">부서</td>
												<td>
													<span class="step1_modify_toggle">
														<input type="text" id="manager_part" name="managerPart" class="default_txtInput" style="width: 200px;" title="부서" value="${debtBox.DEPT_NM}" maxlength="30">
													</span>
													<span class="step1_readOnly_toggle">
														${debtBox.DEPT_NM}
													</span>
												</td>
											</tr>
											<tr>
												<td class="title">직위</td>
												<td class="default_note">
													<span class="step1_modify_toggle">
														<input type="text" id="manager_position" name="managerPosition" class="default_txtInput" style="width: 200px;" title="직위" maxlength="15" value="${debtBox.JOB_TL_NM}">
													</span>
													<span class="step1_readOnly_toggle">
														${debtBox.JOB_TL_NM}
													</span>
												</td>
												<c:set var="TEL_NO" value="${fn:split(debtBox.TEL_NO,'-')}" />
												<td class="title">전화</td>
												<td class="default_note">
													<span class="step1_modify_toggle">
														<select id="tel_no1" name="telNo1" title="전화번호 국번" class="tblSelect1">
															<c:forEach var="phoneCodeList" items="${phoneCodeList}">
																<option value="${phoneCodeList}" <c:if test="${phoneCodeList eq TEL_NO[0]}">selected</c:if>>${phoneCodeList}</option>
															</c:forEach>
														</select> - 
														<input type="text" class="default_txtInput numeric phoneNo midNo tblInput" id="tel_no2" name="telNo2" title="전화번호 앞자리" value="${TEL_NO[1]}" style="width: 50px;" maxlength="4"> - 
														<input type="text" class="default_txtInput numeric phoneNo lastNo tblInput" id="tel_no3" name="telNo3" title="전화번호 뒷자리" value="${TEL_NO[2]}" style="width: 50px;" maxlength="4">
													</span>
													<span class="step1_readOnly_toggle">
														<c:if test="${debtBox.TEL_NO ne ''}">
															${TEL_NO[0]} - ${TEL_NO[1]} - ${TEL_NO[2]}
														</c:if>
													</span>
												</td>
											</tr>
											<tr>
												<c:set var="MB_NO" value="${fn:split(debtBox.MB_NO,'-')}" />
												<td class="title">휴대폰<span class="em_po">*</span></td>
												<td class="default_note">
													<span class="step1_modify_toggle">
														<select class="required tblSelect1" id="mb_no1" name="mbNo1" title="휴대폰번호 국번">
															<c:forEach var="cellPhoneCodeList" items="${cellPhoneCodeList}">
																<option value="${cellPhoneCodeList}" <c:if test="${cellPhoneCodeList eq MB_NO[0]}">selected</c:if>>${cellPhoneCodeList}</option>
															</c:forEach>
														</select> - 
														<input type="text" class="default_txtInput numeric required phoneNo midNo tblInput" id="mb_no2" name="mbNo2" title="휴대폰번호 앞자리" value="${MB_NO[1]}" style="width: 50px;" maxlength="4"> - 
														<input type="text" class="default_txtInput numeric required phoneNo lastNo tblInput" id="mb_no3" name="mbNo3" title="휴대폰번호 뒷자리" value="${MB_NO[2]}" style="width: 50px;" maxlength="4">
													</span>
													<span class="step1_readOnly_toggle">
														<c:if test="${fn:trim(debtBox.MB_NO) ne ''}">
															${MB_NO[0]} - ${MB_NO[1]} - ${MB_NO[2]}
														</c:if>
													</span>
												</td>
												<c:set var="FAX_NO" value="${fn:split(debtBox.FAX_NO,'-')}" />
												<td class="title">팩스</td>
												<td class="default_note">
													<span class="step1_modify_toggle">
														<select id="fax_no1" name="faxNo1" title="팩스번호 국번" class="tblSelect1">
															<c:forEach var="faxCodeList" items="${faxCodeList}">
																<option value="${faxCodeList}" <c:if test="${faxCodeList eq FAX_NO[0]}"></c:if>>${faxCodeList}</option>
															</c:forEach>
														</select> - 
														<input type="text" class="default_txtInput numeric phoneNo midNo tblInput" id="fax_no2" name="faxNo2" title="팩스번호 앞자리" value="${FAX_NO[1]}" style="width: 50px;" maxlength="4"> - 
														<input type="text" class="default_txtInput numeric phoneNo lastNo tblInput" id="fax_no3" name="faxNo3" title="팩스번호 뒷자리" value="${FAX_NO[2]}" style="width: 50px;" maxlength="4">
													</span>
													<span class="step1_readOnly_toggle">
														<c:if test="${fn:trim(debtBox.FAX_NO) ne ''}">
															${FAX_NO[0]} - ${FAX_NO[1]} - ${FAX_NO[2]}
														</c:if>
													</span>
												</td>
											</tr>
											<tr>
												<c:set var="EMAIL" value="${fn:split(debtBox.EMAIL,'@')}" />
												<td class="title">이메일<span class="em_po">*</span></td>
												<td class="default_note" colspan="3">
													
													<span class="step1_modify_toggle">
														<input type="text" class="default_txtInput required" id="email1" name="email1" title="이메일 주소" value="${EMAIL[0]}" maxlength="30"> @ 
														<input type="text" class="default_txtInput required" readonly="readonly" id="email2" name="email2" title="이메일 도메인" value="${EMAIL[1]}" maxlength="30">
														<select id="select_email" title="이메일 도메인 선택" class="tblSelect1">
															<c:forEach var="emailCodeList" items="${emailCodeList}">
																<option value="${emailCodeList}" <c:if test="${emailCodeList eq EMAIL[1]}">selected</c:if>>
																	<c:choose>
																		<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
																		<c:otherwise>직접입력</c:otherwise>
																	</c:choose>
																</option>
															</c:forEach>
														</select>
													</span>
													<span class="step1_readOnly_toggle">
														${EMAIL[0]}@${EMAIL[1]}
													</span>
												</td>
											</tr>
										</tbody>
									</table>
									
									<div class="empty_b20"> </div>
									
									<div class="wart1 step1_readOnly_toggle" style="height:32px;">	
									</div>
									
									<div class="wart1 step1_modify_toggle">
									<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
									<span class="warn_ico1">ㆍ</span> 부서, 직위, 전화번호는 통보서에 기재되어 채무자에게 전달됩니다. 원활한 업무를 위해 입력해주세요.
									</div>
								</div>
							</li>
							<%-- STEP01 끝 --%>

						</ul></li>
					
					<li class="Apply_slide">
						<span class="slideSpan">
							<a href="#none" id="btn_step_two_title" class="down slideSpan nobtn_etc">STEP 02. 채무자 정보</a>
							
							<%--   ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임 --%>
							<%-- <c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}"> --%>
							<span class="ApplyBtn01">
								<a class="on btn_step1_step2_modify" href="#none" data-step="step2" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px;">수정</a>
								<a class="on btn_step1_step2_save" href="#none" data-step="step2" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px; right:90px;">저장</a>
								<a class="on btn_step1_step2_cancel" href="#none" data-step="step2" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-25px;">취소</a>
							</span>
							<%-- </c:if>	 --%>						
						</span>
						<ul style="display: none;">

							<%-- STEP02 시작 --%>
							<li class="Apply_sub">
								<div id="step2" class="ApplysubWrap">
								
								<%-- FORM HIDDEN AREA START --%>
								<input type="hidden" id ="cust_seq" name="custSeq" value="${debtBox.CUST_ID}" title="거래처순번"/>
								<input type="hidden" id ="cust_id" name="custId" value="${debtBox.CUST_NO}" title="거래처순번"/>
								<input type="hidden" id ="debt_appl_id" name="debtApplId" value="${sBox.debtApplId}" title="채무불이행 순번" />
								<%-- FORM HIDDEN AREA END --%>
								
									<table id="deptor_company_info" class="defaultTbl01_01" summary="회사정보 입력테이블">
										<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>

											<tr>
												<td class="title">회사명<span class="em_po">*</span></td>
												<c:set var="CUST_NO" value="${fn:substring(debtBox.CUST_NO, 0, 3)}-${fn:substring(debtBox.CUST_NO, 3, 5)}-${fn:substring(debtBox.CUST_NO, 5, 13)}" />
												<td>
													${debtBox.CUST_NM}
													<input type="hidden" id ="cust_nm" name="deptorCorpName" value="${debtBox.CUST_NM}" title="회사명"/>
												</td>
												<td class="title">사업자번호<span class="em_po">*</span></td>
												<td>
													${CUST_NO}
													<input type="hidden" id ="cust_no" name="deptorCustNum" value="${CUST_NO}" title="사업자번호"/>
												</td>
											</tr>
											<tr>
												<td class="title">대표자명<span class="em_po">*</span></td>
												<td<c:if test="${debtBox.CUST_TYPE eq 'I'}"> colspan="3"</c:if>>
													${debtBox.CUST_OWN_NM}
													<input type="hidden" id ="deptor_name" name="deptorName" value="${debtBox.CUST_OWN_NM}" title="대표자명"/>
												</td>
												<c:if test="${debtBox.CUST_TYPE ne 'I'}">
													<td class="title change">법인등록번호</td>
													<td class="change">
														<span class="step2_modify_toggle">
															<input type="text" id="deptor_corp_num" name="deptorCorpNum" class="default_txtInput corpNo numeric" style="width: 200px;" title="법인등록번호" value="${debtBox.CUST_CORP_NO}" maxlength="13">
														</span>
														<span class="step2_readOnly_toggle">
															<c:if test="${!empty debtBox.CUST_CORP_NO}">
															${fn:substring(debtBox.CUST_CORP_NO,0,6)}-${fn:substring(debtBox.CUST_CORP_NO,6,13)}
															</c:if>														
															<%-- ${debtBox.CUST_CORP_NO} --%>
														</span>
													</td>
												</c:if>
											</tr>
											<tr>
												<td class="title">통보주소<span class="em_po">*</span></td>
												<td class="default_note2" colspan="3">
													<span class="step2_modify_toggle">
														<input type="text" class="default_txtInput required numeric postNo" id="post_no1" name="postNo1" title="우편번호" maxlength="5" style="width: 40px;" value="${fn:trim(debtBox.CUST_POST_NO)}">
														<%-- <input type="text" class="default_txtInput required" id="post_no2" name="postNo2" title="우편번호 뒷자리" readonly="readonly" maxlength="3" style="width: 40px;" value="${fn:substring(debtBox.CUST_POST_NO, 3, 6)}"> --%>
														<!-- <a href="#" class="ApplyBtn02" onClick="javascript:open_post(); return false;">우편번호</a> -->&nbsp; 
														<input type="text" class="default_txtInput required" id="cust_addr_1" name="addr1" title="주소"  style="width: 237px;" maxlength="30" value="${debtBox.CUST_ADDR_1}"> 
														<input type="text" class="default_txtInput2" id="cust_addr_2" name="addr2" title="상세주소" style="width: 255px; margin-top:0px;" maxlength="50" value="${debtBox.CUST_ADDR_2}">
													</span>
													<span class="step2_readOnly_toggle">
														<c:if test="${fn:trim(debtBox.CUST_POST_NO) ne ''}">
														(${debtBox.CUST_POST_NO})
														</c:if>
														<c:if test="${(debtBox.CUST_ADDR_1 ne null) or (debtBox.CUST_ADDR_1 ne '')}">
														 ${debtBox.CUST_ADDR_1} ${debtBox.CUST_ADDR_2}
														</c:if>
													</span>
												</td>
											</tr>
											<tr>
												<td class="title">채무구분<span class="em_po">*</span></td>
												<td>
													<span class="step2_modify_toggle">
														<select id="dept_division" name="dept_division" class="tblSelect1" title="채무구분 선택" >
															<option value="delayPayment" <c:if test="${debtBox.DEBT_TYPE eq 'P'}">selected</c:if> >대금연체</option>
															<option value="jointSurety" <c:if test="${debtBox.DEBT_TYPE eq 'J'}">selected</c:if> >연대보증인</option>
														</select>
													</span>
													<span class="step2_readOnly_toggle">
														${debtBox.DEBT_TYPE_STR}
													</span>
												</td>
												<td class="title">채무불이행 금액<span class="em_po">*</span></td>
												<td>
													<span class="step2_modify_toggle">
														<input type="text" id="nonpay_sum" class="default_txtInput required numericMoney unsigned nonpaySum" name="nonpaySum" placeholder="50,000 이상 입력가능" maxlength="14" title="채무불이행 금액" style="width:190px;" value="${fn:trim(debtBox.ST_DEBT_AMT)}">원
													</span>
													<span class="step2_readOnly_toggle">
														<%-- <fmt:formatNumber type="NUMBER" value="${debtBox.ST_DEBT_AMT}" groupingUsed="true"/>원 --%>
														${fn:trim(debtBox.ST_DEBT_AMT)}원
													</span>
												</td>
											</tr>
										</tbody>
									</table>
									
									<div class="empty_b20"> </div>
									
<%--  									<table class="defaultTbl01_01" summary="정보등록 담장자">
										<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="50%">
											<col width="50%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">연체개시일<span class="em_po">*</span></td>
												<td>
													<span class="step2_modify_toggle">
														<input id="overdue_open_date" name="overStDt" type="text" class="applyDate required" readonly="readonly" title="연체개시일" value="${debtBox.OVER_ST_DT}"/>
													</span>
													<span class="step2_readOnly_toggle">
														${debtBox.OVER_ST_DT}
													</span>	
												</td>
											</tr>
										</tbody>
									</table> --%>
									
<%-- 									<table class="defaultTbl01_01 step2_readOnly_toggle" summary="정보등록 담장자">
										<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">연체개시일<span class="em_po">*</span></td>
												<td>${debtBox.OVER_ST_DT}</td>
												<td class="title">등록사유 발생일</td>
												<td>${debtBox.REG_DT}</td>
											</tr>
										</tbody>
									</table>	 --%>								

									<table class="defaultTbl01_01" summary="정보등록 담장자">
										<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">연체개시일<span class="em_po">*</span></td>
												<td class="default_note">
													<span class="step2_modify_toggle">
														<input id="overdue_open_date" name="overStDt" type="text" class="applyDate required" readonly="readonly" title="연체개시일" value="${debtBox.OVER_ST_DT}"/>
													</span>
													<span class="step2_readOnly_toggle">
														${debtBox.OVER_ST_DT}
													</span>												
												</td>
												<td class="title">등록사유 발생일</td>
												<td>
													<span class="step2_modify_toggle" id="reason_occur_date">
														${debtBox.REG_RES_DT_TIME}
													</span>
													<span class="step2_readOnly_toggle">
														${debtBox.REG_RES_DT_TIME}
													</span>												
												</td>
											</tr>
										</tbody>
									</table>

									<div class="wart1 step2_readOnly_toggle" style="height:16px;">
									</div>	
									
									<div class="wart1 step2_modify_toggle">
									<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
									</div>									
								</div>
							</li>
							<%-- STEP02 끝 --%>

						</ul></li>

					<li class="Apply_slide">
						<span class="slideSpan">
							<a href="#none" id="btn_step_four_title" class="down slideSpan nobtn_etc">STEP 03. 거래증빙자료 첨부 및 작성완료</a>
						</span>
						<ul style="display: none;">

							<%-- STEP04 시작 --%>
							<li class="Apply_sub">
								<div id="file_area" class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블">
									<caption>거래증빙자료 첨부 및 작성완료</caption>
										<colgroup>
											<col width="18%">
											<col width="82%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title stat_A">거래명세서<br/>(최대 20MB)</td>												
												<td id="data_A" class="default_note">
													<c:forEach var="i" items="${debentureFileList}" varStatus="idx">
														<c:if test="${debentureFileList[idx.index].TYPE_CD == 'A' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${debentureFileList[idx.index].FILE_PATH}" data-deleteDebtFileSN="${debentureFileList[idx.index].DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${debentureFileList[idx.index].FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_A" name="dataUploadA" class="default_txtFileInput" maxlength="100" style="width:500px;" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>										
											<tr>
												<td class="title stat_B">세금계산서<br/>(최대 20MB)</td>
												<td id="data_B" class="default_note">
													<c:forEach var="i" items="${debentureFileList}" varStatus="idx">
														<c:if test="${debentureFileList[idx.index].TYPE_CD == 'B' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${debentureFileList[idx.index].FILE_PATH}" data-deleteDebtFileSN="${debentureFileList[idx.index].DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${debentureFileList[idx.index].FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>	
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_B" name="dataUploadB" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_C">법원판결문<br/>(최대 20MB)</td>
												<td id="data_C" class="default_note">
													<c:forEach var="i" items="${debentureFileList}" varStatus="idx">
														<c:if test="${debentureFileList[idx.index].TYPE_CD == 'C' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${debentureFileList[idx.index].FILE_PATH}" data-deleteDebtFileSN="${debentureFileList[idx.index].DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${debentureFileList[idx.index].FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_C" name="dataUploadC" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_D">계약서<br/>(최대 20MB)</td>
												<td id="data_D" class="default_note">
													<c:forEach var="i" items="${debentureFileList}" varStatus="idx">
														<c:if test="${debentureFileList[idx.index].TYPE_CD == 'D' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${debentureFileList[idx.index].FILE_PATH}" data-deleteDebtFileSN="${debentureFileList[idx.index].DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${debentureFileList[idx.index].FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
												<div class="dataUploadArea">
													<input type="file" id="data_upload_D" name="dataUploadD" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
													<div class="dataUpWrap02">
													<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
													<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
													</div>
												</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_E">기타증빙자료<br/>(최대 20MB)</td>
												<td id="data_E" class="default_note">
													<c:forEach var="i" items="${debentureFileList}" varStatus="idx">
														<c:if test="${debentureFileList[idx.index].TYPE_CD == 'E' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${debentureFileList[idx.index].FILE_PATH}" data-deleteDebtFileSN="${debentureFileList[idx.index].DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${debentureFileList[idx.index].FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
												<div class="dataUploadArea">
													<input type="file" id="data_upload_E" name="dataUploadE" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
													<div class="dataUpWrap02">
													<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
													<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
													</div>
												</div>
												</td>
											</tr>
										</tbody>
									</table>

									<div class="wart1">
										<span class="warn_ico1">ㆍ</span> 원하는 증빙자료 종류를 선택하여 첨부해주세요. 종류 별로 20MB 까지 파일 첨부 가능합니다.<br>
										<span class="warn_ico2">ㆍ</span> 입력한 채무금액에 해당되는 증빙자료로 공급자정보, 채무자정보, 거래내역 등이 있어야 합니다.<br />
									</div>
									
									<p class="empty_b20"></p>
									
									<div class="ApplyBtn_Add">
										<div class="ApplyBtn_Step">
										<a href="#none" id="btn_upload_attachment" class="step4_info">증빙자료 업로드</a>
										<a id="btn_save_apply_form" class="step4_info" href="#none">작성완료</a>
										</div>
									</div>
									
									
								</div>
							</li>
							<%-- STEP04 끝 --%>

						</ul></li>
				</ul>
			</div>
			</form>
		</div>
		
		<div class="empty_b7"> </div>
		
		
		<div id="tbl_numbtnWrap">
			<table class="tbl_numbtn">
			<tbody>
				<tr>
					<td class="left">
						<div id="btnWrap_l">
							<div class="btn_graybtn_01">
								<a id="btn_apply" class="ApplyBtn_Type on" href="#none" <c:if test="${debtBox.STAT != 'SC'}">style="display:none;"</c:if> >신청</a>
							</div>
						</div>
						<c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}">
						<div id="btnWrap_l">
							<div class="btn_graybtn_01">
								<a id="btn_save_apply_form" class="ApplyBtn_Type on" href="#none" <c:if test="${debtBox.STAT != 'TS'}">style="display:none;"</c:if> >작성완료</a>
							</div>
						</div>
						<div id="btnWrap_l">
							<div class="btn_graybtn_01">
								<a id="btn_delete" href="#none" class="ApplyBtn_Type on" id="btn_02">삭제</a>
							</div>
						</div>
		            	</c:if>
						<div id="btnWrap_l">
							<div class="btn_whitebtn_01">
								<a id="btn_print" href="#none" class="on" id="btn_del">인쇄</a>
							</div>						
						</div>							
					</td>
            
					<td class="right">
						<div class="btn_graybtn_01">
							<a id="btn_list" class="ApplyBtn_Type on" href="#none">목록</a>
						</div>
					</td>
				</tr>
			</tbody>
			</table>
		</div>	

		
	
	

</div>

</div>

<%-- HIDDEN AREA START --%>
<div id="hidden_file_area" style="display: none;">
	<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블">
		<caption>거래증빙자료 첨부 및 작성완료</caption>
			<colgroup>
				<col width="17%">
				<col width="83%">
			</colgroup>
			<tbody>
				<tr>
					<td class="title stat_A">거래명세서</td>												
					<td id="data_A" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_A" name="dataUploadA" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>										
				<tr>
					<td class="title stat_B">세금계산서</td>
					<td id="data_B" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_B" name="dataUploadB" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>	
					</td>
				</tr>
				<tr>
					<td class="title stat_C">법원판결문</td>
					<td id="data_C" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_C" name="dataUploadC" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="title stat_D">계약서</td>
					<td id="data_D" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_D" name="dataUploadD" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="title stat_E">기타증빙자료</td>
					<td id="data_E" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_E" name="dataUploadE" class="default_txtFileInput" maxlength="100" style="width:500px;"/>
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>

<%-- Step 1 Clone 영역 --%>
<div id="step1_clone_area" style="display:none;"></div>

<%-- Step 2 Clone 영역 --%>
<div id="step2_clone_area" style="display:none;"></div>

<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>