<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>

<%-- 정렬관련 공통코드 --%>
<c:set var="orderTypeList" value="${result.orderTypeList}"/>

<%-- 목록갯수 공통코드 --%>
<c:set var="rowSizeTypeList" value="${result.rowSizeTypeList}"/>

<%-- 정렬 공통코드 --%>
<c:set var="debtApplicationListOrderConditionTypeList" value="${result.debtApplicationListOrderConditionTypeList}"/>

<%-- 채무불이행 등록 담당자 --%>
<c:set var="debtMbrList" value="${result.debtMbrList}"/>

<%-- 채무불이행 신청서 리스트 --%>
<c:set var="debtInquiryList" value="${result.debtInquiryList}"/>

<%-- 채무불이행 신청서 페이징 --%>
<c:set var="pcPage" value="${result.pcPage}"/>

<%-- 채무불이행 검색 총 갯수 --%>
<c:set var="total" value="${result.total}"/>

<%-- 클래스 타입 --%>
<c:set var="classType" value="${result.classType}"/>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/common.css" />
<%-- <link rel="stylesheet" href="${CSS}/debt/getDebtInquiryList.css" /> --%>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

	/*
	* <pre>
	* 거래처 검색조건 체인지 이벤트 함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014. 05. 22
	*/
	$('.custSearchType').change(function() {
		$('#cust_kwd').val(''); // 거래처 inputbox 초기화
		if($(this).val() == 'business_all') {
			$('#cust_kwd').attr("disabled", "disabled");
		}else{
			$('#cust_kwd').removeAttr("disabled");
		}
	});
	
	/**
	* <pre>
	*   채무불이행 전체 선택 토글 함수
	* </pre>
	* @author KIM GA EUN
	* @since 2015. 07. 03
	**/

	$('#btn_select_all').click(function(event) {
        if(this.checked) {
            $('.chk_remove_debt').each(function() { 
                this.checked = true;              
            });
        }else{
            $('.chk_remove_debt').each(function() { 
                this.checked = false;                        
            });         
        }
    });
 
	/**
	* <pre>
	*  채무불이행 선택삭제 버튼 클릭 이벤트 함수
	* </pre>
	* @author sungrangkong
	* @since 2014. 05. 01
	*/
	/* $('#btn_selected_debt_remove').click(function(){
		
		if(!$('.chk_remove_debt:checked').size()){
			alert('삭제하실 신청서를 선택해주세요.');
			return false;
		} else {
			$.fn.removeCustomer(
				$('.chk_remove_debt:checked').map(function(){
					return $(this).val();
				}).get().join(',')	   
			);
		}
	}); */
  	  
	/**
	* <pre>
	*   채무불이행 신청서 단일 항목 삭제
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	/* $('.close').click(function(){
		$.fn.removeCustomer(
			$(this).parent().find('.chk_remove_debt').val()
		);
	}); */
	  
	/**
	* <pre>
	*   채무불이행 검색 버튼 클릭 이벤트 함수
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	$('.search').click(function(){
		if(!$.fn.validate()) return false;
			$('#num').val(1);
			$('form').attr({'action':'${HOME}/debt/getDebtInquiryList.do', 'method':'POST'}).submit();
	});
	
/* 	var slide = '${sBox.slide}';

	if((slide == "T")){
		$('#searchType').text("검 색 조 건 접 기");
		$('#classType').val("up");
		$('#slideBar').attr('style',"display:block");
	} */
	
	/**
    * <pre>
    *	채무불이행 신청서 정보 Excel Download 함수
    * </pre>
    * @author sungrangkong
    * @since 2014. 04. 30
    */
	$('#btn_save_excel').click(function(){
		if(!$.fn.validate()) return false;
		$('form').attr({'action':'${HOME}/debt/getDebtInquiryListForExcel.do', 'method':'POST'}).submit();
	});
      
	/**
	* <pre>
	*	채무불이행 신청서 상세 정보 페이지 조회
	* </pre>
	* @author sungrangkong
	* @since 2013. 08. 27
	*/
	$('.linkDebt').click(function(){
       	$params = $(this).attr('data-debtApplId');
       	$stat = $(this).attr('data-stat');
       	$paramObj = {
       		 debtApplId : $(this).attr('data-debtApplId')
   	    };
      
       	if($stat == 'NT' || $stat == 'EV'|| $stat == 'CA' || $stat == 'CB'|| $stat == 'FA' || $stat == 'FB'||  $stat =='AW' ||  $stat =='RC' || $stat =='PY' || $stat =='RR'){
       		location.href ="${HOME}/debt/getDebtOngoingInquiry.do?" + $.param($paramObj);
       	}else if($stat =='AC' || $stat =='AU' || $stat =='CR' || $stat =='RU' || $stat =='KF'){
       		location.href ="${HOME}/debt/getDebtCompleteInquiry.do?" + $.param($paramObj);
       	} else if($stat =='TS' || $stat =='SC'){
       		location.href ="${HOME}/debt/getDebtInquiry.do?" + $.param($paramObj); 
       	}
	});
	
       
	/**
	* <pre>
	*	채무불이행 정렬 조건을 변경시키면 form을 전송
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	$('.listOrderOption').change(function(){
		if(!$.fn.validate()) return false;
		$('#num').val(1);
		$('form').attr({'action':'${HOME}/debt/getDebtInquiryList.do', 'method':'POST'}).submit();
	});

	/**
	* <pre>
	*	채무불이행 진행 상태 에 따른 checkbox CHANGE 이벤트
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 29
	*/
	//TODO
	$('.processStatusType').change(function(){        	
       	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $('.processStatusType:checked').not('[name=processStatusALL]').length;
		if($(this).val() == 'ALL' && checkedLen == 17){
       		$(this).attr('checked',true);
		}else if($(this).val() == 'ALL' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       	}else if($(this).val() != 'ALL' && checkedLen != 16){
       		$("#condition01").attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 16){
       		$("#condition01").attr('checked',true);
       	
       	}
	});
	
	
	/**
	*<pre>
	*	진행상태 클릭시  function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.07
	*/
	$(".processStatusType").click(function(){
		
		$processList = '';
		if($("input[class=processStatusType]:checkbox:checked").length > 0){
			$("input[class=processStatusType]:checkbox:checked").each(function(index){
				   $processList += $.trim($("input[class=processStatusType]:checkbox:checked").eq(index).val());
				  
				   if((index+1) != $("input[class=processStatusType]:checkbox:checked").length){
					   $processList += ',';
				   }

			   });
		}
		$("#process_status_type").val($processList);
				
		// 전체를 클릭하였을 경우 
		if($(this).attr("id")=='condition01'){
			if($(this).is(":checked")){
				$('.processStatusType').prop('checked', true);	
			}else{
				$('.processStatusType').prop('checked', false);
			}
		}
		
		// 진행상태가 전체, 통보서 준비, 통보서 발송, 민원발생, 민원발생(추가증빙), 등록완료 
		// 체크 되어있을 경우 채무금액 정정 검색가능 
		/* if($(".chkmodType:checked:not(#condition01)").length>0 || $(".processStatusType:checked").length==0 ){
			$("#mod_stat_type").show();
			
		}else{
			$("#mod_stat_type").hide();
			$(".modStatType").prop("checked",false);
		} */
			
	});
        
	/**
	* <pre>
	*	채무불이행 담당자 RADIO 박스 변경에 따른 CHANGE 이벤트
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 29
	*/
	$('.debtMbrType').change(function(){
		if($(this).val() == 'ALL'){
			$("#nopayment_mbr_select").attr('disabled','disabled');
			$("#mbr_spanList > span").remove();
			$("#mbr_list").val('');
			$("#mbr_name_list").val('');
		}else{
       		$("#nopayment_mbr_select").removeAttr('disabled');
       	}
	});
	
    /**
     * <pre>
     *	사업자번호 붙여넣기 할 때 '-' 삭제하기
     * </pre>
     * @author 백원태
     * @since 2014. 12. 15
     */
     $('#cust_kwd').change(function(){
    	 if(($('#business_no').attr('checked')) || ($('#company_corp_no').attr('checked'))){
    	 	$('#cust_kwd').attr('value', $('#cust_kwd').attr('value').replace(/-/g, '')); 
    	 }
     });
     
	/**
	* <pre>
	*	채무불이행 등록 담당자 선택 시 이벤트 함수
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	$('#nopayment_mbr_select').change(function(){
     	   
		// [1] 첫번째 option 값이 설정되었을 경우 검증
		if($(this).find('option').is(':first:selected')){
			return false;
		}
     	   
		// [2] 이미 선택된 담당자인지 체크함
		$mbrUsrId = $(this).find('option:selected').attr('data-mbrUsrId');
		$checkBTF = true;
		$("#mbr_spanList > span").each(function(e){
			if($(this).attr('data-mbrUsrId') == $mbrUsrId){
				alert("이미 선택된 담당자 입니다.");
				$checkBTF = false;
			}
		});
		if(!$checkBTF){
			return false;
		}
     	   
		// [3] 10명이상 담당자가 늘어난 경우 담당자 추가 제한함
		if($("#mbr_spanList").children().length + 1 > 10){
			alert("채무불이행 등록 담당자 검색은 10명 이상 할 수 없습니다.\n다수의 담당자를 검색하시고 싶은 고객은 [전체]선택을 통해 검색해주세요.");
			return false;
		}
     	   
		// [4] 검증 통과 후 , 담당자 명 설정 표기함
		$data = '<span class="mbr_name" data-mbrUsrId="' + $(this).find('option:selected').attr('data-mbrUsrId') + '">' + $(this).val() + '&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>';
		$('#mbr_spanList').append($data);
     	   
		// [5] HIDDEN TYPE 의 INPUT BOX에 데이터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			$("#mbr_spanList > span").each(function(index){
				$mbr_list += $.trim($(this).attr('data-mbrUsrId'));
				$mbr_name_list += $.trim($(this).text());
				if((index+1) != $("#mbr_spanList").children().length){
					$mbr_list += ',';
					$mbr_name_list += ',';
				}
			});
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
     	   
		// [6] 담당자 선택 SELECT TAG 초기화 실시함
		$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
	});
         
	/*
	* <pre>
	*   채무불이행 등록 담당자 X 표시 클릭 이벤트 함수
	* </pre>
	* @author sungrangkong 
	* @since 2014. 04. 30
	*/
	$(document).on('click','.btn_remove_mbr_name',function(e){
		// [1] 해당 담당자 ELEMENT를 삭제한다.
		$(this).parent().parent().remove();
		
		// [2] 모든 담당자 삭제시 자동 [전체] 메뉴가 선택된다.
		if($("#mbr_spanList").children().length == 0){
			$("#nopayment_mbr_select option:eq(0)").attr("selected", "selected");
			$('#nopayment_mbr_select').attr('disabled', true);
			$("#person_name").attr('checked',false);
			$("#person_all").attr('checked','checked');
			$("#mbr_list").val('');
		}
		
		//[3] FORM ACTION 파라미터 추가함
		$mbr_list = '';
		$mbr_name_list = '';
		if($("#mbr_spanList").children().length > 0){
			   $("#mbr_spanList > span").each(function(index){
				   $mbr_list += $.trim($(this).attr('data-mbrUsrId'));
				   $mbr_name_list += $.trim($(this).text());
				   if((index+1) != $("#mbr_spanList").children().length){
					   $mbr_list += ',';
					   $mbr_name_list += ',';
				   }
			   });
		}
		$("#mbr_list").val($mbr_list);
		$("#mbr_name_list").val($mbr_name_list);
	});
	
    /**
     * <pre>
     *   상세검색 열기/닫기 세팅
     * </pre>
     * @author KIM GA EUN
     * @since 2015. 08. 06.
     */
	  var slide = '${sBox.slide}';
	  if((slide == "Y")){
	  	$('#search_text').text("▲ 상세검색닫기");
		$('.searchDetail').attr('style',"display:table-row");
	  }
	  
    /**
     * <pre>
     *   상세검색 열기/닫기
     * </pre>
     * @author KIM GA EUN
     * @since 2015. 08. 06.
     */
    $('#btn_detail').click(function(){
		$('.searchDetail').toggle();
		changeText();
	  });	
	/*
	* <pre>
	* 검색조건 펼치기/접기
	* </pre>
	* @author KIM GA EUN
	* @since 2015. 06. 18
	*/
/* 	$('#menu_slide > li.Apply_slide > .slideSpan > a').click(function(){
		$checkElement = $(this).parent().next();
		if(($checkElement.is('ul')) && ($checkElement.is(':visible'))) {
			$checkElement.slideUp(300);
			$(this).attr("class", "down");
			$('#searchType').text("검 색 조 건 펼 치 기");
			$('#slide').val("F");
			$('#classType').val("down");
			return false;
		}
		if(($checkElement.is('ul')) && (!$checkElement.is(':visible'))) {
			$checkElement.slideDown(300);
			$(this).attr("class", "up");
			$('#searchType').text("검 색 조 건 접 기");
			$('#slide').val("T");
			$('#classType').val("up");
			return false;
		}
	});
});
 */
 });
/**
 * <pre>
 * 채무불이행 신청서 삭제 FORM 전송 함수
 * </pre>
 * @author sungrangkong
 * @since 2014. 04. 30
 * @param debtIdList : 채무불이행신청순번 리스트 문자열(구분자 : ,)

$.fn.removeCustomer = function(debtIdList) {
	 if(!confirm('선택하신 신청서가 삭제됩니다.\n정말 삭제하시겠습니까?')) return false; 
	 $('#debt_id_list').val(debtIdList);
	 $('form').attr({'action':'${HOME}/debt/removeDebt.do', 'method':'POST'}).submit();
}; */

/**
 * <pre>
 *   FORM 전송 직전 검증 함수
 * </pre>
 * @author sungrangkong
 * @since 2014. 04. 30 
 */
 $.fn.validate = function() {
	
	 $bTF = true; // 검증용 Flag 변수
	 
	 // 거래처 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	 if(!$("#business_all").is(':checked') && $.trim($("#cust_kwd").val()) == ''){
		 alert("[" + $(".custSearchType:checked").next().text() + "] 검색어를 입력해주세요");
		 $("#cust_kwd").focus();
		 return false;
	 }
	 
	// 거래처 사업자번호 선택할 경우 숫자를 검증
 	if($('.custSearchType:checked').attr('id') == 'business_no' || $('.custSearchType:checked').attr('id') == 'company_corp_no') {
 		 if(!typeCheck('numCheck',$.trim($('#cust_kwd').val()))){
 			 alert($('.custSearchType:checked').attr('title') + '는 숫자만 입력가능합니다.');
 			 $('#cust_kwd').focus();
 			return false;
 		 }
 	}
	// 진행상태 중 어떤 것도 선택되지 않았을 경우 
	if($('.processStatusType:checked').length == 0){
		alert('진행상태를 선택해주세요');
		return false;
	}
	
	if($('#person_name').is(':checked') && $('.mbr_name').length == 0){
		alert('담당자를 선택해주세요');
		return false;
	}
	 
	return $bTF;
 };
 /**
  * <pre>
  *  인터페이스 전송실패시 오류메시지 출력
  * </pre>
  * @author Jungmi Kim
  * @since 2015. 07. 27 
  */
 function getIFErrorMsg(code,msg){
	   if(code!=''&&code!=null){
	   	alert("전송실패 사유: ["+code+"],"+msg);   
	   }else{
	   	alert("전송실패 사유: "+msg);   
	   }
 }
  
  /**
   * <pre>
   *	상세검색 열기/닫기 버튼 텍스트
   * </pre>
   * @author KIM GA EUN
   * @since 2015. 08. 06.
   */
  function changeText(){
  	var display = $('.searchDetail').css("display");
  	if(display == 'table-row'){
  		$('#search_text').text("▲ 상세검색닫기");
  		$('#slide').val("Y");
  		
  	}else if(display == 'none'){
  		$('#search_text').text("▼ 상세검색열기");
  		$('#slide').val("N");
  	}
  }
 
</script>
</head>
<style>
	#ui-datepicker-div{ font-size: 13px; width: 210px; }
</style>
<body>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0201"/>
</jsp:include>
<%-- Top Area End --%>



<!-- contentArea -->
<div id="containerWrap">
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<!-- Left Area End -->

<div id="rightWrap">
	<div class="right_tit ltit02_0101"><em>채무불이행관리</em></div>
				
	<p class="content_tit">채무불이행 신청서 조회</p>
	
	<form name="debtInquiryListFrm" id="debtInquiryListFrm">
		
		<%-- FORM HIDDEN AREA START --%>
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/debt/getDebtInquiryList.do">
		
		<%-- 채무불이행 담당자 --%>
		<input type="hidden" name="mbrList" id="mbr_list" value="${sBox.mbrList}" />
		<input type="hidden" name="mbrNameList" id="mbr_name_list" value="${sBox.mbrNameList}" />
		
		<%-- 채무불이행 신청서 삭제 대상 리스트(구분자 ,) --%>
		<input type="hidden" name="debtIdList" id="debt_id_list" value="${sBox.debtIdList}" />
		
		<input type="hidden" name="classType" id="classType" value="${result.classType}" />
		<input type="hidden" name="slide" id="slide" value="${sBox.slide}"/> 
		<%-- FORM HIDDEN AREA END --%>
		
		<div class="tbl01Wrap">
			<table>
			<tbody>
				<tr>
					<th width="69px" colspan="2">일자</th>
					<td>
						<!-- <select class="tblSelect1">
							<option>작성일자</option>
							<option>신청일자</option>
							<option>등록예정일자</option>
							<option>진행상태 변경일자</option>
							<option>연체개시일자</option>
						</select>&nbsp; -->
						<input type="hidden" name="periodConditionType" id="periodConditionType" value="${sBox.periodConditionType}"/>
						<input type="text" name="periodStDt" id="period_st_dt" class="firstDate periodDate" title="등록일 시작조건" value="${sBox.periodStDt}" readonly="readonly" />
						
						~
						<input type="text" name="periodEdDt" id="period_ed_dt" class="firstDate periodDate" title="등록일 종료조건" value="${sBox.periodEdDt}" readonly="readonly" />
						
					</td>
				</tr>
				<tr>
					<th colspan="2">거래처</th>
					<td>
					<span class="empty_r10"><input type="radio" class="custSearchType" name="custSearchType" value="business_all" id="business_all" title="전체" <c:if test="${sBox.custSearchType eq 'business_all' }">checked</c:if>/> <label for="business_all">전체</label></span>
					<span class="empty_r10"><input type="radio" class="custSearchType" name="custSearchType" value="business_no" id="business_no" title="사업자번호" <c:if test="${sBox.custSearchType eq 'business_no' }">checked</c:if>/> <label for="business_no">사업자번호</label></span>
					<span class="empty_r10"><input type="radio" class="custSearchType" name="custSearchType" value="company_name" id="company_name" title="거래처명" <c:if test="${sBox.custSearchType eq 'company_name' }">checked</c:if>/> <label for="company_name">거래처명</label></span>
<%-- 					<span class="empty_r10"><input type="radio" class="custSearchType" name="custSearchType" value="company_owner_name" id="company_owner_name" title="대표자(채무자)" <c:if test="${sBox.custSearchType eq 'company_owner_name' }">checked</c:if>/> <label for="company_owner_name">대표자(채무자)</label></span>
					<span><input type="radio" class="custSearchType" name="custSearchType" value="company_corp_no" id="company_corp_no" title="법인등록번호" <c:if test="${sBox.custSearchType eq 'company_corp_no' }">checked</c:if>/> <label for="company_corp_no">법인등록번호</label> --%>
					<input type="text" id="cust_kwd" name="custKwd" class="txtInput tblInput" <c:if test="${sBox.custSearchType eq 'business_all'}">disabled="disabled"</c:if> style="width:110px" value="${sBox.custKwd}" maxlength="70" style="width:100px">
					</span>
					</td>
				</tr> 
				<tr class="searchDetail" style="display:none;">
					<th width="25px" rowspan="4" class="txt_valign_m">진<br />행<br />상<br />태</th>
					<th class="second_thl" style="width:84px;">전체선택</th>
					<td><input type="checkbox" class="processStatusType chkmodType" name="processStatusALL" value="ALL" id="condition01" <c:if test="${sBox.processStatusALL eq 'ALL'}">checked</c:if> /><label for="condition01"> 전체</label></td>
				</tr>
				<tr class="searchDetail" style="display:none;">
					<th>저장 중 <!-- <input type="checkbox" class="chk_remove_debt" title="선택" value=""> --></th>
					<td>
					<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="SC" id="condition14" <c:if test="${sBox.processStatusSC eq 'SC'}">checked</c:if> /> <label for="condition14">저장완료(신청가능)</label></span>
					<input type="checkbox" class="processStatusType" name="processStatusType" value="TS" id="condition13" <c:if test="${sBox.processStatusTS eq 'TS'}">checked</c:if> /> <label for="condition13">임시저장</label>
					</td>
				</tr>
				<tr class="searchDetail" style="display:none;">							
					<th>진행 중<!--  <input type="checkbox" class="chk_remove_debt" title="선택" value="" checked> --></th>
					<td class="padding_td1">
					<span style="padding-right:26px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="AW" id="condition02" <c:if test="${sBox.processStatusAW eq 'AW'}">checked</c:if> /> <label for="condition02">심사대기</label></span>
					<span style="padding-right:34px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="EV" id="condition03" <c:if test="${sBox.processStatusEV eq 'EV'}">checked</c:if>  /> <label for="condition03">심사 중</label></span>
					<input type="checkbox" class="processStatusType" name="processStatusType" value="AA" id="condition04" <c:if test="${sBox.processStatusAA eq 'AA'}">checked</c:if>  /> <label for="condition04">결제대기</label>
					<br />
					<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="PY" id="condition05" <c:if test="${sBox.processStatusPY eq 'PY'}">checked</c:if>  /> <label for="condition05">통보서 준비</label></span>
					<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="NT" id="condition06" <c:if test="${sBox.processStatusNT eq 'NT'}">checked</c:if>  /> <label for="condition06">통보서 발송</label></span>
					<span style="padding-right:23px;"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CA,CB" id="condition07" <c:if test="${sBox.processStatusCA eq 'CA,CB'}">checked</c:if>  /> <label for="condition07">민원발생</label></span>
					<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FA,FB" id="condition08" <c:if test="${sBox.processStatusFA eq 'FA,FB'}">checked</c:if>  /> <label for="condition08">추가증빙제출</label>
					<br />
					<span style="padding-right:26px;"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="RC" id="condition09" <c:if test="${sBox.processStatusRC eq 'RC'}">checked</c:if>  /> <label for="condition09">등록완료</label></span>
					<span style="padding-right:26px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="RR" id="condition10" <c:if test="${sBox.processStatusRR eq 'RR'}">checked</c:if>  /> <label for="condition10">해제요청</label></span>
<%-- 					<span style="padding-right:23px;"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CB" id="condition11" <c:if test="${sBox.processStatusCB eq 'CB'}">checked</c:if>  /> <label for="condition11">민원발생</label></span>
					<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FB" id="condition12" <c:if test="${sBox.processStatusFB eq 'FB'}">checked</c:if>  /> <label for="condition12">추가증빙제출</label> --%>
					</td>
				</tr>
				<tr class="searchDetail" style="display:none;">							
					<th>진행완료 <!-- <input type="checkbox" class="chk_remove_debt" title="선택" value=""> --></th>
					<td>
					<span style="padding-right:25px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="AU" id="condition17" <c:if test="${sBox.processStatusAU eq 'AU'}">checked</c:if> /> <label for="condition17">신청불가</label></span>
					<span style="padding-right:25px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="AC" id="condition18" <c:if test="${sBox.processStatusAC eq 'AC'}">checked</c:if> /> <label for="condition18">신청취소</label></span>
					<span style="padding-right:25px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="CR" id="condition15" <c:if test="${sBox.processStatusCR eq 'CR'}">checked</c:if> /> <label for="condition15">해제완료</label></span>
					<span style="padding-right:25px;"><input type="checkbox" class="processStatusType" name="processStatusType" value="RU" id="condition16" <c:if test="${sBox.processStatusRU eq 'RU'}">checked</c:if> /> <label for="condition16">등록불가</label></span>
					<input type="checkbox" class="processStatusType" name="processStatusType" value="KF" id="condition19" <c:if test="${sBox.processStatusKF eq 'KF'}">checked</c:if> /> <label for="condition19">심사과실발생</label>
					</td>
				</tr>
				<tr id="mod_stat_type" class="searchDetail" style="display:none;">							
					<th colspan="2">채무금액정정</th>
					<td>
					<span class="empty_r10"><input type="checkbox" class="modStatType" name="modStatR" value="R" id="mod_stat_r" <c:if test="${sBox.modStatR eq 'R'}">checked</c:if>  /> <label for="mod_stat_r">채무금액 정정요청</label></span>
					<input type="checkbox" class="modStatType" name="modStatC" value="C" id="mod_stat_c" <c:if test="${sBox.modStatC eq 'C'}">checked</c:if>  /> <label for="mod_stat_c">채무금액 정정완료</label>
					</td>
				</tr>
				<%-- 채무불이행 신청권한이 있는 유저만이 담당자 조건으로 검색가능함 --%>
				<c:if test="${sBox.isSessionDebtApplGrn}">
				<tr>							
					<th colspan="2">담당자</th>
					<td>
					<span class="empty_r10"><input type="radio" name="debtMbrType" class="debtMbrType" id="person_all" value="ALL" <c:if test="${sBox.debtMbrType eq 'ALL'}">checked</c:if> /> <label for="person_all">전체</label></span>
					<input type="radio" name="debtMbrType" class="debtMbrType" id="person_name" value="name" <c:if test="${sBox.debtMbrType eq 'name'}" >checked</c:if>/> <label for="person_name" class="hidden-accessible">등록 담당자</label>
					<select class="tblSelect1" id="nopayment_mbr_select" <c:if test="${sBox.debtMbrType ne 'name'}">disabled="disabled"</c:if> title="등록 담당자" >
					 <option>--선택--</option>
					 <c:forEach var="i" items="${debtMbrList}">
					  <option value="${i.USR_NM }"   data-mbrUsrId="${i.MBR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
					 </c:forEach>
					</select>
					
					<%-- 등록 담당자 리스트 출력 부분 --%>
					<span id="mbr_spanList">
						<c:set var="mbrNameListArray" value="${fn:split(sBox.mbrNameList,',')}"/> 
						<c:forEach var="i" items="${sBox.mbrList}" varStatus="idx">
							<span class="mbr_name" data-mbrUsrId="${i}">${mbrNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>
						</c:forEach>
					</span>

					</td>
				</tr>
			  </c:if>
			</tbody>
			</table>
		</div>
		
<%-- 		<div class="default_Apply">
		<ul id="menu_slide">
			<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="${classType}" style="height: 5px; padding: 7px 0px 14px 15px;">※ &nbsp;<label id="searchType">검 색 조 건 펼 치 기</label></a></span>
				<ul style="display: none;" id="slideBar">
				<li class="Apply_sub2">	
				<div class="tbl00Wrap">
					<table>
					<tbody>
						<tr>
							<th width="24px" rowspan="4" class="txt_valign_m">진<br />행<br />상<br />태</th>
							<th class="second_thl" style="width:84px;">전체선택</th>
							<td><input type="checkbox" class="processStatusType chkmodType" name="processStatusALL" value="ALL" id="condition01" <c:if test="${sBox.processStatusALL eq 'ALL'}">checked</c:if> /> 전체</td>
						</tr>
						<tr>
							<th>저장 중 <!-- <input type="checkbox" class="chk_remove_debt" title="선택" value=""> --></th>
							<td>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="SC" id="condition14" <c:if test="${sBox.processStatusSC eq 'SC'}">checked</c:if> /> <label for="condition14">저장완료(신청가능)</label></span>
							<input type="checkbox" class="processStatusType" name="processStatusType" value="TS" id="condition13" <c:if test="${sBox.processStatusTS eq 'TS'}">checked</c:if> /> <label for="condition13">임시저장</label>
							</td>
						</tr>
						<tr>							
							<th>진행 중<!--  <input type="checkbox" class="chk_remove_debt" title="선택" value="" checked> --></th>
							<td class="padding_td1">
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="AW" id="condition02" <c:if test="${sBox.processStatusAW eq 'AW'}">checked</c:if> /> <label for="condition02">심사대기</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="EV" id="condition03" <c:if test="${sBox.processStatusEV eq 'EV'}">checked</c:if>  /> <label for="condition03">심사 중</label></span>
							<input type="checkbox" class="processStatusType" name="processStatusType" value="AA" id="condition04" <c:if test="${sBox.processStatusAA eq 'AA'}">checked</c:if>  /> <label for="condition04">결제대기</label>
							<br />
							<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="PY" id="condition05" <c:if test="${sBox.processStatusPY eq 'PY'}">checked</c:if>  /> <label for="condition05">통보서 준비</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="NT" id="condition06" <c:if test="${sBox.processStatusNT eq 'NT'}">checked</c:if>  /> <label for="condition06">통보서 발송</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CA" id="condition07" <c:if test="${sBox.processStatusCA eq 'CA'}">checked</c:if>  /> <label for="condition07">민원발생</label></span>
							<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FA" id="condition08" <c:if test="${sBox.processStatusFA eq 'FA'}">checked</c:if>  /> <label for="condition08">추가증빙제출</label>
							<br />
							<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="RC" id="condition09" <c:if test="${sBox.processStatusRC eq 'RC'}">checked</c:if>  /> <label for="condition09">등록완료</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="RR" id="condition10" <c:if test="${sBox.processStatusRR eq 'RR'}">checked</c:if>  /> <label for="condition10">해제요청</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CB" id="condition11" <c:if test="${sBox.processStatusCB eq 'CB'}">checked</c:if>  /> <label for="condition11">민원발생</label></span>
							<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FB" id="condition12" <c:if test="${sBox.processStatusFB eq 'FB'}">checked</c:if>  /> <label for="condition12">추가증빙제출</label>
							</td>
						</tr>
						<tr>							
							<th>진행완료 <!-- <input type="checkbox" class="chk_remove_debt" title="선택" value=""> --></th>
							<td>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="AU" id="condition13" <c:if test="${sBox.processStatusAU eq 'AU'}">checked</c:if> /> <label for="condition02">신청불가</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="AC" id="condition14" <c:if test="${sBox.processStatusAC eq 'AC'}">checked</c:if> /> <label for="condition03">신청취소</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="CR" id="condition15" <c:if test="${sBox.processStatusCR eq 'CR'}">checked</c:if> /> <label for="condition04">해제완료</label></span>
							<span class="empty_r10"><input type="checkbox" class="processStatusType" name="processStatusType" value="RU" id="condition16" <c:if test="${sBox.processStatusRU eq 'RU'}">checked</c:if> /> <label for="condition05">등록불가</label></span>
							<input type="checkbox" class="processStatusType" name="processStatusType" value="KF" id="condition17" <c:if test="${sBox.processStatusKF eq 'KF'}">checked</c:if> /> <label for="condition06">심사과실발생</label>
							</td>
						</tr>
						<tr id="mod_stat_type">							
							<th colspan="2">채무금액정정</th>
							<td>
							<span class="empty_r10"><input type="checkbox" class="modStatType" name="modStatR" value="R" id="mod_stat_r" <c:if test="${sBox.modStatR eq 'R'}">checked</c:if>  /> <label for="mod_stat_r">채무금액 정정요청</label></span>
							<input type="checkbox" class="modStatType" name="modStatC" value="C" id="mod_stat_c" <c:if test="${sBox.modStatC eq 'C'}">checked</c:if>  /> <label for="mod_stat_c">채무금액 정정완료</label>
							</td>
						</tr>
						채무불이행 신청권한이 있는 유저만이 담당자 조건으로 검색가능함
						<c:if test="${sBox.isSessionDebtApplGrn}">
						<tr>							
							<th colspan="2">담당자</th>
							<td>
							<span class="empty_r10"><input type="radio" name="debtMbrType" class="debtMbrType" id="person_all" value="ALL" <c:if test="${sBox.debtMbrType eq 'ALL'}">checked</c:if> /> <label for="person_all">전체</label></span>
							<input type="radio" name="debtMbrType" class="debtMbrType" id="person_name" value="name" <c:if test="${sBox.debtMbrType eq 'name'}" >checked</c:if>/> <label for="person_name" class="hidden-accessible">등록 담당자</label>
							<select class="tblSelect1" id="nopayment_mbr_select" <c:if test="${sBox.debtMbrType ne 'name'}">disabled="disabled"</c:if> title="등록 담당자" >
							 <option>--선택--</option>
							 <c:forEach var="i" items="${debtMbrList}">
							  <option value="${i.USR_NM }" <c:if test="${i.MBR_TYPE eq 'N'}">class="mem_type_N"</c:if>  data-mbrUsrId="${i.MBR_ID}">${i.USR_NM}<c:if test="${i.MBR_TYPE eq 'N'}">(비회원)</c:if></option>
							 </c:forEach>
							</select>
							
							등록 담당자 리스트 출력 부분
							<span id="mbr_spanList">
								<c:set var="mbrNameListArray" value="${fn:split(sBox.mbrNameList,',')}"/> 
								<c:forEach var="i" items="${sBox.mbrList}" varStatus="idx">
									<span class="mbr_name" data-mbrUsrId="${i}">${mbrNameListArray[idx.index]}&nbsp;<a href="#none"><img class="btn_remove_mbr_name" src="${IMG}/common/btn_s_X.gif"></a></span>
								</c:forEach>
							</span>
		
							</td>
						</tr>
					  </c:if>	
					</tbody>
					</table>
				</div>
				</li>
				
				</ul>
			</li>
			<p class="line"></p>
		</ul>
		</div> --%>
		<ul class="tab_t2">
			<li><a href="#" id="btn_detail" class="debentureSubTab" ><label id="search_text" style="font-weight:normal; font-size:0.85em; font-family:NanumGothic, dotum, arial;">▼ 상세검색열기</label></a></li>
		</ul>		
		<div id="btnWrap_r" class="search_btn">
			<div class="btn_rbbtn_01"><a href="#none" class="on search" id="btn_search">검색</a></div>
		</div>
			
		<p class="linedot_b0"></p>
		
		<div id="content_tit_pr">
			<div class="content_tit pru40">검색결과  [<span id="total">${total}</span>개]</div>

			<%-- <p class="totalCnt">[${total}개]</p> --%>

			<ul class="select_r1">
				<li>
					<p>정렬
					<select id="order_condition" class="listOrderOption tblSelect1" name="orderCondition" title="일자별로 정렬">
					<c:forEach var="i" items="${debtApplicationListOrderConditionTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderCondition eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
					</select>
					</p>
				</li>
				<li>
					<p>
					<select  id="order_type" class="listOrderOption tblSelect1" name="orderType" title="조건별로 정렬">
					<c:forEach var="i" items="${orderTypeList}">
						<option value="${i.KEY}" <c:if test="${sBox.orderType eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
					</c:forEach>
					</select>
					</p>
				</li>
				<li>
					<p>목록갯수
					<select name="rowSize" id="listNo" class="listOrderOption tblSelect1">
						<c:forEach var="i" items="${rowSizeTypeList}">
							<option value="${i.KEY}" <c:if test="${sBox.rowSize eq i.KEY}">selected</c:if>>${i.VALUE}</option>	
						</c:forEach>
					</select>
					</p>
				</li>
			</ul>
		</div>
		</form>
		
		<div class="tbl02Wrap">
			<table>
			<tbody>
				<tr>
<%-- 					<th width="3%" class="first_thl">
					<c:when test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}">
					<input type="checkbox" title="선택" value="" id="btn_select_all"/>
					</c:when>
					</th> --%>
					<th width="27%" class="first_thl">거래처명</th>
					<th width="11%">사업자번호</th>
					<!-- <th width="14%">법인등록번호</th>
					<th width="7%">대표자<br />(채무자)</th> -->
					<th width="14%">채무금액</th>
					<th width="13%">진행상태</th>
					<th width="6%">담당자</th>
					<th width="9%">전송상태</th>
					<th width="8%">등록일</th>
				</tr>
				<%-- 검색결과가 존재하지 않을경우 문구 설정함 --%>
				<c:if test="${(fn:length(debtInquiryList) eq 0) or (debtInquiryList eq null) }">
				<tr>
					<td colspan="10" align="center" style="border-left: none;">검색 결과가 없습니다.</td>
				</tr>
				</c:if>
				<c:forEach var="i" items="${debtInquiryList}">
				<tr>
<%-- 					<td class="first_tdl_c">
					  ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임		
					<c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}">
					<input type="checkbox" class="chk_remove_debt" title="선택" value="${i.DEBT_APPL_ID}"/>
					</c:if>
					</td> --%>
					<%-- <td class="first_tdl_c"><a href="#" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"  data-custId="${i.CUST_NO}"><strong>${i.DOC_CD}<!-- </strong></a> --></td>	 --%>			
					<td style="text-align: left; padding-left: 5px;" class="first_tdl_c">
						<c:if test="${i.CUST_NM ne null}">
							<a href="#" class="linkDebt" data-debtApplId="${i.DEBT_APPL_ID}"  data-custId="${i.CUST_NO}" data-stat="<c:choose><c:when test="${i.STAT_CD eq null or i.STAT eq null}">TS</c:when><c:otherwise>${i.STAT_CD}</c:otherwise></c:choose>" ><strong>${i.CUST_NM}</strong></a> <em>|</em>
						</c:if>
					</td>
					<td>
						<c:if test="${i.CUST_NO ne null}">
							${fn:substring(i.CUST_NO,0,3)}-${fn:substring(i.CUST_NO,3,5)}-${fn:substring(i.CUST_NO,5,10)} <em>|</em>		
						</c:if>
					</td>
					<%-- <td>
					<c:if test="${fn:trim(i.CUST_CORP_NO) ne ''}">
						${i.CUST_CORP_NO} <em>|</em>		
					</c:if>
					</td>
					<td>
					<c:if test="${i.CUST_OWN_NM ne null}">
						${i.CUST_OWN_NM} <em>|</em>		
					</c:if>
					</td> --%>
					<td style="text-align: right; padding-right: 5px;">
					<c:if test="${i.ST_DEBT_AMT ne null}">	
						${i.ST_DEBT_AMT}원 <em>|</em>	
					</c:if>
					</td>
					<td><c:choose><c:when test="${i.STAT eq null}">임시저장</c:when><c:otherwise>${i.STAT}</c:otherwise></c:choose></td>
					<td>${i.REG_USR_ID}</td>
					<td>
						<c:if test="${i.TRANS eq '0'}"><a onclick="getIFErrorMsg('${i.RCODE}','${i.REASON}')" href="#none"><strong>전송실패</strong></a></c:if>
						<c:if test="${i.TRANS eq '1'}">전송완료</c:if>
						<c:if test="${i.TRANS eq '2'}">전송중</c:if>
						<c:if test="${i.TRANS eq null}">-</c:if>
					</td>
					<td>${i.REG_DT_CVT}</td>
				</tr>
				</c:forEach>
			</tbody>
			</table>
		</div>
		
		
		<!-- page_num_wrap -->
		<div id="page_num_wrap">
			<div class="page_num" id="getPageView">
				<c:out value="${pcPage}" escapeXml="false" />
			</div>
		</div>
		<!-- page_num_wrap -->
		
		<%-- <div id="tbl_numbtnWrap">
			<table class="tbl_numbtn">
			<tbody>
				<tr>
					<td class="left">
						  ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임
						<c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}">
						  <div class="btn_graybtn_01"><a href="#none" class="on" id="btn_selected_debt_remove">선택삭제</a></div>
						</c:if>
					</td>
					<td>
						
					</td>
					<!-- <td class="right">
						<div class="btn_whitebtn_01"><a href="#none" class="on" id="btn_save_excel">엑셀다운로드</a></div>
					</td> -->
				</tr>
			</tbody>
			</table>
		</div>
		 --%>
</div>
</div>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>