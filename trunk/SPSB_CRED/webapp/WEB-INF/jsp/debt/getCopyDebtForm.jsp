<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="result" value="${result}" />
<c:set var="debtBox" value="${result.debtBox.debtBox}" />
<c:set var="commonCodeBox" value="${result.debtBox.commonCodeBox}" />
<c:set var="phoneCodeList" value="${commonCodeBox.phoneCodeList}" />
<c:set var="faxCodeList" value="${commonCodeBox.phoneCodeList}" />
<c:set var="emailCodeList" value="${commonCodeBox.emailCodeList}" />
<c:set var="cellPhoneCodeList" value="${commonCodeBox.cellPhoneCodeList}" />

<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%>
<html class="no-js" lang="ko">
<%--<![endif]--%>
<head>
<title>채무불이행등록 &gt; 채무불이행 신청서 작성 | 스마트채권 - 채권관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtForm.css" />
<link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css">

<style type="text/css">
	.browsing {font-weight: bold;}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		jQuery.support.cors = true;
		
		/*
		* <pre>
		* STEP TITLE CLICK EVENT
		* </pre>
		* @author 비즈온 디자인팀
		*/
		$('#menu_slide > li.Apply_slide > .slideSpan > a').click(function(){
			$checkElement = $(this).parent().next();
			if(($checkElement.is('ul')) && ($checkElement.is(':visible'))) {
				$('#menu_slide > li.Apply_slide > ul:visible').slideUp(300);
				$(this).attr("class", "down");
				return false;
			}
			if(($checkElement.is('ul')) && (!$checkElement.is(':visible'))) {
				$('#menu_slide > li.Apply_slide > ul:visible').slideUp(300);
				$checkElement.slideDown(300);
				$('#menu_slide > li.Apply_slide > .slideSpan > a').attr("class", "down");
				$(this).attr("class", "up");
				return false;
			}
		});

		/*
		* <pre>
		* STEP01의 회사정보 구분을 변경했을 때의 form 변경
		* </pre>
		* @author sungrangkong
		* @since 2014.04.16
		*/
		$('.applyCustType').change(function() {
			if ($(this).val() == 'C') {
				$('#applicant_person').removeAttr('checked');
				$('#applicant_company').attr('checked', true);
			} else {
				$('#applicant_company').removeAttr('checked');
				$('#applicant_person').attr('checked', true);
			}
			
			$.changeCorpForm($('#apply_company_info'), $(this).val());
		});
		
		/*
		* <pre>
		* STEP01의 사업자번호 '-' 입력 여부
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#apply_cust_num').on({
			focusin: function() {
				$origin_value = $(this).val();
				$(this).val($origin_value.replace(/-/gi, ''));
			},
			focusout: function() {
				$origin_value = $(this).val().replace(/[^0-9]/gi, '');
				$(this).val($origin_value.substr(0,3)+'-'+$origin_value.substr(3,2)+'-'+$origin_value.substr(5,5));
			}
		});
		
		/*
		* <pre>
		* STEP01의 회사정보 수정 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#btn_corp_info_modify').click(function(e) {
			// 회사정보 수정 팝업 페이지
			winOpenPopup("${HOME}/mypage/getCompUserModifyPopup.do",'getCompUserModifyPopup',1000,500,'yes');
		});
		
		/*
		* <pre>
		* 이메일 SELECT BOX CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#select_email').change(function() {
			// 직접입력을 제외한 나머지의 경우 이메일 뒷부분 input창은 readonly 변경
			if($(this).val() == "") {
				$('#email2').val("");
				$('#email2').attr("readonly", false);
			} else {
				$('#email2').val($(this).val());
				$('#email2').attr("readonly", true);
			}
		});
		
		/*
		* <pre>
		* STEP01의 정보등록 담당자 수정 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.18
		*/
		$('#btn_memb_info_modify').click(function(e) {
			// 정보등록 담당자 수정 팝업페이지
			winOpenPopup("${HOME}/mypage/getUserModifyPopup.do",'getUserModifyPopup',1000,500,'yes');
		});
		
		/*
		* <pre>
		* STEP01의 다음단계 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @sine 2014.04.16
		*/
		$('#btn_one_to_two').click(function(e) {
			$('#step').val('step1'); // 현재 step 설정
			
			if ($.fn.validate($('#step1'))) {
				$.saveInfoViaAjax(1, $('#btn_step_two_title'));
			}
				
		});
		
		/*
		* <pre>
		* STEP02의 회사정보 구분을 변경했을 때의 form 변경
		* </pre>
		* @author sungrangkong
		* @since 2014.04.17
		*/
		$('.deptorCustType').change(function() {
			$.changeCorpForm($('#deptor_company_info'), $(this).val());
		});
		
		/*
		* <pre>
		* STEP02의 다음단계 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.17
		*/
		$('.step2_info').on({
			click : function(e) {
				$('#step').val('step2'); // 현재 step 설정
				
				// 버튼에 따라 어떤 타이틀로 진행할 지 초기화함.
				$click_title = $(this).attr('id') == 'btn_two_to_three' ? "btn_step_three_title" : "btn_step_four_title";
				
				// 2단계 정보 validation
				if ($.fn.validate($('#step2'))) {
					
					// 2단계 정보 저장
					$.saveInfoViaAjax(2, $('#'+$click_title));
				}
			}
		});
		
		 /**
		 * <pre>
		 * 사업자 등록 중복확인 체크 Event 
		 * </pre>
		 * @author sungrangkong
		 * @since 2013. 08. 27.
		 */
		 $('.customerPopup').on({
			click : function(e) {
				$custType = $('.deptorCustType:checked').val();
				winOpenPopup('${HOME}/debenture/getCustomerSearchForm.do?custType=' + $custType,'getCustomerSearchForm',340,120,'yes');
			} 
		 });
		 
		 /*
		 * <pre>
		 * 전화번호 blur시 공백 지움
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.05.26
		 */
		 $('.phoneNo').blur(function() {
			$(this).val($(this).val().replace(/\s/gi, '')); 
		 });
		 
		 /**
	      * <pre>
	      * 여신한도, 초기미수금 클릭시 숫자 ,를 제거하는 함수 
	      * </pre>
	      * @author Jong Pil Kim
	      * @since 2013. 08. 27.
	      */ 
	      $('.numericMoney').on({
	    	  focus: function() {
		    	  $(this).val(replaceAll($(this).val(), ',', ''));
		      },
		      blur: function() {
		    	  $money = $(this).val().replace(/^0+/, '').replace(/\s/gi,'');
			      $(this).val(addComma($money));
		      }
	      });
		
		 /**
		 * <pre>
		 * 증빙 파일 선택되어지면 디자인 되어진 input box에 값을 넣는 Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('change', 'input[type=file]', function() {
			 $(this).parent().find('input[type=text]').val($(this).val());
		 });
		 
		 /**
		 * <pre>
		 * 파일 추가하기 (+) 버튼 Click Event
		 * </pre>
		 * @author sungrangkong
		 * @since 2014. 04. 15.
		 */
		 $(document).on('click', '.btn_plus', function(){
			 
			 $parentEL = $(this).parent().parent();
			 $cloneEL = $parentEL.clone();

			 if($cloneEL.find('.btn_minus').length == 0){
			 	$cloneEL.find('.dataUpWrap02').append('<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			 }
			 
			 $parentEL.find('.dataUpWrap02 > a').remove();
			 $parentEL.find('.dataUpWrap02').append('<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			 
			 $parentEL.after("<div class='spliter'></div>");
			 $cloneEL.find('input[type=file]').val('');
			 $parentEL.next().after($cloneEL);
			 
		 });
		 
	 	/**
	 	* <pre>
	 	* 파일 추가하기 (-) 버튼 Click Event
	 	* </pre>
	 	* @author sungrangkong
	 	* @since 2014. 04. 15.
	 	*/
	 	$(document).on('click', '.btn_minus', function(){
			 
		 	$parentEL = $(this).parent().parent();
		 	$tdEL = $parentEL.parent();
			 
			// 이미 삭제 대상일 경우 로직을 수행하지 않음
			if ($parentEL.find('span').hasClass('deleteFileSpan')) {
				return false;
			}
			 
		 	// 마지막 데이터의 경우 input box 안의 내용만 비워준다.
	 		if($tdEL.find('.dataUploadArea > input[type=file]').length != 1 && $parentEL.find('span').length == 0){
				 
			 	if($parentEL.next().attr('class') == 'spliter'){
				 	$parentEL.next().remove();	 
			 	}else{
				 	$parentEL.prev().remove();
			 	}
			 	$parentEL.remove();
				 
			 	// 저장 후 변경시 title td에 changeEl 클래스 값 줌.
			 	if ($('#step4_status').val() == 'complete') {
				 	$tdEL.addClass('changeEl');
			 	}
				 
		 	}else if($parentEL.find('span').length == 1){
				
		 		$parentEL.find('span').addClass('deleteFileSpan');
			 	$parentEL.find('span').css('text-decoration','line-through');
				 
		 	}else if($tdEL.find('.dataUploadArea > input[type=file]').length == 1){
				// Firefox 용 input file 초기화
				$tdEL.find('.dataUploadArea > input[type=file]').val('');
					
				// IE & Chrome 용 input file 초기화
				$tdEL.find('.dataUploadArea > input[type=file]').replaceWith( $tdEL.find('.dataUploadArea > input[type=file]').clone(true) );
					
				// 저장 후 변경시 title td에 changeEl 클래스 값 줌.
				if ($('#step4_status').val() == 'complete') {
						$tdEL.addClass('changeEl');
				}
		 	}
			 
		 	$last = $tdEL.find('.dataUploadArea').last();
		 	$last.find('.dataUpWrap02').remove();
		 	$last.append('<div class="dataUpWrap02"><a class="btn_plus" href="#none"><img src="${IMG}/common/ico_data_p.gif"></a>&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a></div>');
			 
		 	if ($parentEL.find('span > a').attr('data-deleteDebtFileSN') != null) {
			 	$deleteDebtFileSN = $('#delete_debt_file_SN').val();
			 	$deleteDebtFileSN += ($deleteDebtFileSN.length > 0 ? "," : "") + $parentEL.find('span > a').attr('data-deleteDebtFileSN');
			 	$('#delete_debt_file_SN').val($deleteDebtFileSN);
		 	}
			
	 	});
		 
		 /*
		 * <pre>
		 * 미수채권 전체선택 CHECKBOX CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.23
		 */
		 $('#check_all_bond').on({
			click : function() {
				if($('#check_all_bond').is(':checked')){
					 
					 if($('.checkBond').length > 0){
						// 체크박스 모두 체크되도록 속성 변경
						 $('#bond_list').find('input:checkbox').attr("checked", "checked");

						 // 채권리스트에 있는 모든 체크박스 개수
						 $('#selected_bond').find('p').html($('#bond_list').find('input:checkbox').length+"건");
						 
						 $sum = 0;
						 $('#bond_list').find('tr').each(function(){
							 // 채권리스트의 각 미수금액을 모두 더함
							 $sum += parseInt($(this).find('td:eq(4)').text().replace(/[^0-9]/gi, ''), 10);
						 });
						 
						 // 선택채권 미수금총액
						 $('#outstanding_sum').find('p').html(addComma(new String($sum))+'원');
					 }
					 
				 }else{
					 // 체크박스 모두 해제되도록 속성 삭제
					 $('#bond_list').find('input:checkbox').removeAttr("checked");
					 
					 // 선택채권과 미수금총액 출력 html 변경
					 $('#selected_bond').find('p').html("0건");
					 $('#outstanding_sum').find('p').html("0원");
				 }
			} 
		 });

		 /*
		 * <pre>
		 * 미수채권 개별선택 CHECKBOX CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.23
		 */
		 $(document).on('click', '.checkBond', function() {
			 
			// 체크해제된 값이 있다면, 전체체크선택 checkbox는 해제한다.
			if($('#bond_list').find("input:not(:checked)").length > 0){
				$("#check_all_bond").removeAttr("checked");
			}else if($('.checkBond').length == $('#bond_list').find("input:checked").length){
				$("#check_all_bond").attr("checked", "checked");
			}
			 
			// 선택채권 개수
			$('#selected_bond').find('p').html($('#bond_list').find('input:checkbox:checked').length+"건");
				
			// 선택한 채권들의 미수금액을 모두 합하여 계산
			$bond_sum = parseInt($('#outstanding_sum').find('p').html().replace(/[^0-9]/gi, ''), 10);
			if($(this).is(':checked')){
				$add_mnt = parseInt($(this).parent().siblings('td').eq(3).text().replace(/[^0-9]/gi, ''), 10);
				$bond_sum += $add_mnt;
			}else{
				$add_mnt = parseInt($(this).parent().siblings('td').eq(3).text().replace(/[^0-9]/gi, ''), 10);
				$bond_sum -= $add_mnt;
			}
				
			// 선택채권 미수금총액
			$('#outstanding_sum').find('p').html(addComma(new String($bond_sum))+'원');
			 
		 });
		 
		 /*
		 * <pre>
		 * 채무불이행 가이드 건만 보이는 정렬 EVENT
		 * </pre>
		 * @author YouKyung Hong
		 * @since 2014.06.11
		 */
		 $('#debt_guide_view').click(function() {
			 $.getDebentureList();
		 });
		 
		 /*
		 * <pre>
		 * 미수채권리스트 정렬 BUTTON CLICK EVENT
		 * </pre>
		 * @author sungrangkong
		 * @since 2014.04.24
		 */
		 $('#btn_search_bond').on({
			click : function(e) {
				if($.trim($('#cust_id').val()) != ''){
					$.getDebentureList();
				}else{
					alert("채무자 정보를 입력하신 후, 미수 채권을 조회해 주세요.");
					
					$('#btn_step_two_title').trigger("click");
				}
			} 
		 });
		 
		/*
		* <pre>
		* 이전 메뉴로 돌아가기 
		* </pre>
		* @author YouKyung Hong
		* @since 2014.05.26
		*/
		$('.goToPrev').click(function() {
			$clickTitle = 'btn_step_one_title';
			
			// 1단계 정보 validation
			$.saveInfoViaAjax(1, $('#'+$clickTitle));
		});
		
		/*
		* <pre>
		* STEP03의 다음단계 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.24
		*/
		$('.step3_info').on({
			click : function(e) {
				
				$clickTitle = ($(this).attr('id') == 'btn_three_to_two') ? 'btn_step_two_title' : 'btn_step_four_title';
				
				$('#step').val('step3'); // 현재 step 설정
				
				$('#tot_unpd_amt').val($('#outstanding_sum').text().replace(/[^0-9]/gi, ''));
				
				if($.trim($('#bond_sum').val()).replace(/,/gi, '') != $.trim($('#tot_unpd_amt').val()).replace(/,/gi, '')){
					if (!confirm('채무불이행 금액과 선택채권미수총금액이 다릅니다. 계속 진행하시겠습니까?')) {
						return false;
					}
				}

				$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
				 .ajaxSubmit({
				 	dataType : "json",
					async : false,
					beforeSend: function() {
						// 1,2,3,4 단계의 임시 저장 버튼 숨기기
						$('#btn_one_to_two, .step2_info, .step3_info, .step4_info, .goToPrev').hide();
				    },
					success : function(msg){
						$result = msg;
						
						// 1,2,3,4 단계의 임시 저장 버튼 보이기
						$('#btn_one_to_two, .step2_info, .step3_info, .step4_info, .goToPrev').show();
						
						if($result.REPL_CD == '00000') {
							$('#debt_appl_seq').val($result.NPM_ID);
							alert("3단계의 정보를 저장 완료했습니다.\n다음 단계로 이동합니다.");
							
							// 채권 증빙 파일 순번과 지울 채무불이행 증빙파일 순번 value reset
							$('#delete_debt_file_SN').val("");
							
							$('#step3').find('.changeEl').removeClass('changeEl');
							$('#step3_status').val('complete');

							// STEP02의 채무불이행 금액 Setting
							$('#nonpay_sum').val($('#bond_sum').val());
							
							if ($result.prfFileList != null && $result.prfFileList != '') {
								$.getDebtPrfFileList($result.prfFileList); // 증빙자료 조회하여 출력
							} else {
								// STEP04 form reset
								$('#file_area > table').remove();
								$('#file_area').prepend($('#hidden_file_area').html());
							}
						} else {
							alert(utfDecode($result.REPL_MSG));
						}
						
						$('#'+$clickTitle).trigger("click");
					},
					error : function(xmlHttpRequest,textStatus,errorThrown) {
						alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
					}
				});
				
			}
		});
		
		/*
		* <pre>
		* 증빙자료 업로드 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.25
		*/
		$('#btn_upload_attachment').on({
			click : function(e) {
				
				$('#step').val('step4'); // 현재 step 설정
				
				$result = false;
				
				$('.dataUploadArea').find('input').each(function() {
					if($.trim($(this).val()) != '') {
						$result = true;
						return false;
					}
				});
				
				if (!$result) {
					if ($('.dataUploadArea').find('span').length > 0) {
						// 이미 증빙된 파일이 삭제된 파일인지 확인
						$('.dataUploadArea').find('span').each(function() {
							$cssArr = $(this).css('text-decoration').split(' ');
							if ($cssArr[0] != 'line-through') {
								$result = true;
								return false;
							}
						});
					} else {
						$result = false;
					}
				}
				
				// 파일  검증
				$fileRegistTF = true;
				$('.dataUploadArea').find('input:file').each(function(e){
					var result = checkUploadFile(this);
					if(!result){
						$fileRegistTF = false;
						return false;
					}
				});
				
				if(!$fileRegistTF){
					return false;
				}
				
				if($result) {
					$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
					 .ajaxSubmit({
					 	dataType : "json",
						async : false,
						beforeSend: function() {
							// 1,2,3,4 단계의 임시 저장 버튼 숨기기
							$('#btn_one_to_two, .step2_info, .step3_info, .step4_info, .goToPrev').hide();
					    },
						success : function(msg){
							$result = msg;
							
							// 1,2,3,4 단계의 임시 저장 버튼 숨기기
							$('#btn_one_to_two, .step2_info, .step3_info, .step4_info, .goToPrev').show();
							
		 					if($result.REPL_CD == '00000') {
		 						alert("4단계의 정보를 저장 완료했습니다.");
		 						$('#file_area').find('.changeEl').removeClass('changeEl');
		 						$('#step4_status').val('complete');
		 						
		 						$('#delete_debt_file_SN').val('');
		 						
		 						$.getDebtPrfFileList($result.debtPrfFileList);
		 					} else {
		 						alert(utfDecode($result.REPL_MSG));
		 					}
						},
						error : function(xmlHttpRequest,textStatus,errorThrown) {
							alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
						}
					});
				} else {
					alert("반드시 1개 이상의 거래증빙자료를 첨부해야 합니다.");
					return false;
				}
			}
		});
		
		/*
		* <pre>
		* 작성완료 BUTTON CLICK EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.27
		*/
		$('#btn_save_apply_form').click(function(e){

			$('#step').val('all'); // 현재 step 설정
			
			// validation이 제대로 되지 않으면 스크립트 정지
			if(!$.fn.validate($('form'))) {
				return false;
			}
			
			// FORM에서 변경된 요소가 1개 이상일 때 문구 추가
			if ($('form').find('.changeEl').length > 0) {
				if(!confirm('수정 사항이 있습니다. 저장하지 않고 계속 진행하시겠습니까?')) {
					return false;
				}
			}

			// ajaxSubmit을 통해 데이터 등록
			$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
			 .ajaxSubmit({
			 	dataType : "json",
				async : false,
				success : function(msg){
					$result = msg;
					if($result.REPL_CD == '00000') {
						alert("채무불이행신청서의 정보를 저장 완료했습니다.");
						
						$param = {
							custId : $('#cust_id').val(),
							debtApplId : $('#debt_appl_seq').val()
						};
						
						// 신청서 수정 페이지로 이동함
						location.href="${HOME}/debt/getDebtInquiry.do?" + $.param($param);
					} else {
						alert(utfDecode($result.REPL_MSG));
					}
				},
				error : function(xmlHttpRequest,textStatus,errorThrown) {
					alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
				}
			});

		});
		
		/*
		* <pre>
		* STEP01단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/		
		$('#step1 input[type=text]').on('input', function() {
			if ($('#step1_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step1 select').on('change', function() {
			if ($('#step1_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // select box 변경시 상태 변경
		
		/*
		* <pre>
		* STEP02단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#step2 input[type=text]').on('input', function() {
			if ($('#step2_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step2 select').on('change', function() {
			if ($('#step2_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // select box 변경시 상태 변경
		
		/*
		* <pre>
		* STEP03단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#step3 input[type=text]').on('input', function() {
			if ($('#step3_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 텍스트 변경시 상태 변경
		
		$('#step3').on('change', '.checkBond', function() {
			if ($('#step3_status').val() == 'complete') {
				$(this).addClass('changeEl');
			}
		}); // 체크박스 변경시 상태 변경
		
		/*
		* <pre>
		* STEP04단계 상태 VALUE CHANGE EVENT
		* </pre>
		* @author sungrangkong
		* @since 2014.04.28
		*/
		$('#file_area').on('change', 'input[type=file]', function() {
			if ($('#step4_status').val() == 'complete') {
				$(this).parent().parent().addClass('changeEl');
			}
		}); // 파일 변경시 상태 변경
		
		/*
		* <pre>
		* 첨부파일 다운로드 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.07.04
		*/
		$(document).on('click','.browsing',function() {
			$("#file_path").val($(this).attr("data-filePath"));
			$("#file_nm").val($(this).text());
			
			$('form').attr({'action':'${HOME}/debt/getDebtPrfFileDownload.do', 'method':'POST'}).submit();
		});
		
		/*
		* <pre>
		* 정보등록담당자 회원정보 수정 체크에 따른 EVENT
		* </pre>
		* @author YouKyung Hong
		* @since 2014.07.18
		*/
		$('#applyRemYn').change(function() {
			if ($(this).is(':checked')) {
				$('#manager_name').attr("type", "text");
				$('#apply_manager_info').find('tr:eq(0)').find('td:eq(1)').find('span').hide();
			} else {
				$('#manager_name').attr("type", "hidden");
				$('#apply_manager_info').find('tr:eq(0)').find('td:eq(1)').find('span').show();
			}
		});
	});
	
	/**
	* <pre>
	* 거래처 검색 팝업 종료 후 처리
	* </pre>
	* @author yeonjinyoon
	* @since 2013. 09. 06.
	* @param $custNo : 사업자번호, $custType : 거래처유형(P or C), $custId : 거래처 순번, $ltdDt : 만기일, $custOwnNm : 거래처 대표자명, $custCorpNo : 거래처 법인등록번호, 
	*        $custPost1 : 거래처 우편번호1, $custPost2 : 거래처 우편번호2 , $custAddr1 : 거래처 주소1, $custAddr2 : 거래처 주소2
	*/
	$.fn.exitPopup = function($custNm, $custNo, $custType, $custId, $ltdDt, $custOwnNm, $custCorpNo, $custPost1, $custAddr1, $custAddr2){ //$custPost2, 
		
//		$msg = ($('.deptorCustType:checked').val() == 'C') ? "법인사업자" : "개인사업자"; 
		$msg = "C";
//		if( $('.deptorCustType:checked').val() != $custType ) {
//			alert("구분(" + $msg + ") 팝업을 이용해주세요.");	
//			return;
//		}
		
		// 거래처에 따른 문구 수정
		if ($.trim($custId) == '') {
			$('#step2').find('.Apply_checkbox').find('span').text('위 채무자를 거래처로 등록합니다.');
		} else {
			$('#step2').find('.Apply_checkbox').find('span').text('위 채무자 정보로 거래처를 수정합니다.');
		}
		
		// 팝업창 데이터 setting
		$("#cust_nm").val($.trim($custNm));
		$("#cust_no").val($.trim($custNo));
		$("#cust_id").val($.trim($custId));
		$('#deptor_name').val($.trim($custOwnNm));
		$('#deptor_corp_num').val($.trim($custCorpNo));
		$('#post_no1').val($.trim($custPost1));
		/* $('#post_no2').val($.trim($custPost2)); */
		$('#cust_addr_1').val($.trim($custAddr1));
		$('#cust_addr_2').val($.trim($custAddr2));
		
		if ($('#step2_status').val() == 'complete') {
			$("#cust_nm").addClass('changeEl');
			$("#cust_no").addClass('changeEl');
			$("#cust_id").addClass('changeEl');
		}
	};
	
	/*
	* <pre>
	* 회사 구분에 따른 form 변경
	* </pre>
	* @author sungrangkong
	* @since 2014.04.17
	*/
	$.changeCorpForm = function($table_form, $custType) {
		
		if($table_form.attr("id") == 'deptor_company_info') {
			// 회사구분이 개인사업자(I)일 때의 폼 변경
			if($custType == 'I') {
				$table_form.find('.change').hide();
				$table_form.find('tr:eq(2) > td:eq(1)').attr("colspan", "3");
				//$('#deptor_corp_num').removeClass('required');
				//$('#deptor_corp_num').removeClass('corpNo');
			// 회사구분이 법인사업자(C)일 때의 폼 변경
			} else {
				$table_form.find('tr:eq(2) > td:eq(1)').removeAttr("colspan");
				$table_form.find('.change').show();
				//$('#deptor_corp_num').addClass('required');
				//$('#deptor_corp_num').addClass('corpNo');
			}
		} else {
			// 업종 부분 html
			$biz_type_html = '<td class="title change">업종<em>*</em></td><td class="default_note change">';
			$biz_type_html += '<input type="text" id="apply_biz_type" name="applyBizType" class="default_txtInput required" value="${debtBox.BIZ_TYPE}" style="width: 200px;" title="업종" maxlength="40">';
			$biz_type_html += '</td>';
			
			// 법인등록번호 부분 html
			$corp_num_html = '<td class="title change">법인등록번호</td><td class="change">';
			$corp_num_html += '<input type="text" id="apply_corp_num" name="applyCorpNum" class="default_txtInput corp_no numeric" value="${debtBox.CORP_NO}" style="width: 200px;" title="법인등록번호" maxlength="13">';
			$corp_num_html += '</td>';

			// 회사구분이 개인사업자(I)일 때의 폼 변경
			if($custType == 'I') {
				$table_form.find('.change').remove();
				$table_form.find('tr:eq(3) > td').remove();
				$table_form.find('tr:eq(2)').append($biz_type_html);
			// 회사구분이 법인사업자(C)일 때의 폼 변경
			} else {
				$table_form.find('.change').remove();
				$table_form.find('tr:eq(3)').append($biz_type_html);
				$table_form.find('tr:eq(2)').append($corp_num_html);
				$table_form.find('tr:eq(3) > td:eq(1)').attr("colspan", "3");
			}
		}
		
	};
	
	/*
	* <pre>
	* AJAX를 통해 form의 정보를 저장하는 FUNCTION
	* </pre>
	* @author sungrangkong
	* @since 2014.04.19
	* @param $step : 저장할 단계 , $next_title : 임시저장이 끝난 후 펼칠 단계
	*/
	$.saveInfoViaAjax = function($step, $next_title){
		
		$('form').attr({'action':'${HOME}/debt/addDeptApply.do', 'method':'POST'})
		 .ajaxSubmit({
		 	dataType : "json",
			async : false,
			success : function(msg){
				$resultMsg = msg;
				if($resultMsg.REPL_CD == '00000') {
					$('#debt_appl_seq').val($resultMsg.NPM_ID);

					switch($step){
					case 1:
						$('#step1').find('.changeEl').removeClass('changeEl');
						$('#step1_status').val('complete');
						
						// check 초기화
						$('#applyRemYn').attr("checked", false);
						// 담당자 이름 setting
						$('#apply_manager_info').find('tr:eq(0)').find('td:eq(1)').find('span').text($('#manager_name').val());
						
						$('#manager_name').attr("type", "hidden");
						$('#apply_manager_info').find('tr:eq(0)').find('td:eq(1)').find('span').show();
						
						$next_title.trigger('click');
						break;
						
					case 2:
						
						alert("2단계까지의 채무불이행 정보를 저장했습니다.");
						
						// STEP03 form reset
						$('#bond_list').html("");
						
						$('#bond_sum').val($('#nonpay_sum').val());
						$('#step2').find('.changeEl').removeClass('changeEl');
						$('#step2_status').val('complete');
						
						// 채권 증빙 파일 순번과 지울 채무불이행 증빙파일 순번 value reset
						$('#delete_debt_file_SN').val("");
						
						$('.defaultTbl01_02_Content').removeAttr('style');
						
						// 거래처의 미수 채권 가져오기
						$.getDebentureList();
						
						// 증빙 파일 가져오기
						if ($resultMsg.prfFileList != null && $resultMsg.prfFileList != '') {
							$.getDebtPrfFileList($resultMsg.prfFileList); // 증빙자료 조회하여 출력
						} else {
							// STEP04 form reset
							$('#file_area > table').remove();
							$('#file_area').prepend($('#hidden_file_area').html());
						}
						
						$next_title.trigger('click');
						
						break;
					}
					
				} else {
					alert(utfDecode($resultMsg.REPL_MSG));
				}
			},
			error : function(xmlHttpRequest,textStatus,errorThrown) {
				alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
			}
		});
	};
	
	/**
	 * <pre>
	 * 폼 전송 파라미터 검증 중 경고창 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
	 */
	 $.fn.validateResultAndAlert = function($el, $message){
		 alert($message);
		 $el.focus();
		 return false;
	 }; 
	
	/**
	 * <pre>
	 * 전송 파라미터 검증 함수
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 04. 18
	 */
	 $.fn.validate = function($validationArea){
		
		$result = true;
		 
		$formEl = $validationArea.find('input');
		$.each($formEl, function(){
			
			// 필수 공백 검증
			if($(this).hasClass('required')){
				if($.trim($(this).val())=='') {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
					return $result; 
				}
			}
			
			if($.trim($(this).val()) != '') {

				// 사업자 번호 숫자 검증
				if($(this).hasClass('usrNo')){
					if(!typeCheck('numCheck',$.trim($(this).val().replace(/-/gi, '')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}
					
					if($(this).val().replace(/-/gi, '').length < 10) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 10자리여야 합니다.');
						return $result;
					}
				}
				
				// 숫자 검증
				if($(this).hasClass('numeric')){
					if(!typeCheck('numCheck',$.trim($(this).val()))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}
					
					if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
				
				//우편번호
				if($(this).hasClass('postNo')){
					if($(this).val().length != 5){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 5자리여야 합니다.');
						return $result;
					}
				}
				
				// 전화번호, 휴대폰번호, 팩스번호 자릿수 검증
				if($(this).hasClass('phoneNo')){
					if($(this).hasClass('midNo') && $(this).val().length < 3){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 3자리보다 커야 합니다.');
						return $result;
					}
					if($(this).hasClass('lastNo') && $(this).val().length != 4){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 4자리여야 합니다.');
						return $result;
					}
					if($(this).siblings('.phoneNo').val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this).siblings('.phoneNo'), $(this).siblings('.phoneNo').attr('title') + '를 확인해주세요.');
						return $result;
					}
				}
				
				// 화폐 검증
				if($(this).hasClass('numericMoney')){
					if(!(typeCheck('numCheck',replaceAll($.trim($(this).val()),',','')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
						return $result;
					}	
					
					if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
				
				if($(this).hasClass('nonpaySum')){
					if(replaceAll($.trim($(this).val()),',','') < 50000){
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 50,000원 이상만 입력 가능합니다.');
						return $result;
					}
				}
			}
			// 반복문 정지
			if(!$result) return $result;
		});
		
		// 스크립트 정지
		if(!$result) return $result;
		
		// 사업자 번호 검증[문자열 형태 검증]
		$usrNoEl = $validationArea.find('.usrNo');
		$usrNo = $usrNoEl.val().replace(/-/gi, '');
		if($.trim($usrNo) != '') {
			if(!typeCheck('businessNoCheck', $usrNo)) {
				alert("사업자 번호 형식에 맞지 않습니다.");
				$result = false;
				return $result;
			}
		}
		
		// 이메일 검증[문자열 형태 검증]
		$emailEl1 = $validationArea.find('#email1');
		$emailEl2 = $validationArea.find('#email2');
		if($.trim($emailEl1.val()) != '' || $.trim($emailEl2.val()) != ''){
			if(!typeCheck('emailCheck', $.trim($emailEl1.val()) + '@' + $.trim($emailEl2.val()))){
				alert('이메일을 확인해 주세요.');
				$result = false;
				return $result;
			}	
		}
		
		// 법인 등록번호 검증[문자열 형태 검증]
		/* $corpNoEl = $validationArea.find('.corpNo');
		if($.trim($corpNoEl.val()) != ''){
			if(!typeCheck('corpNoCheck',replaceAll($.trim($corpNoEl.val()),'-',''))){
				alert('법인등록번호 형식에 맞지 않습니다.');
				$result = false;
				return $result;
			} 
		} */
		 
		return $result;
		
	 };
	 
	 /*
	 * <pre>
	 * 거래처에 따른 미수채권 조회 AJAX EVENT
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.04.22
	 */
	 $.getDebentureList = function() {
		 
		 $param = {
				 custId : $('#cust_id').val(),
				 orderCondition : $('#order_condition').val(),
				 orderType : $('#order_type').val(),
				 bondMbrId : $('#bond_mbr_id').val(),
				 debtApplId : $('#debt_appl_seq').val(),
				 debtGuideView : ($('#debt_guide_view').is(':checked') ? 'on' : 'off')
		 };
		 
		 $.ajax({
			type : "POST", 
			url : "${HOME}/debt/getDebentureListOfCustomer.do",
			dataType : "json",
			data : $.param($param),
			async : false,
			success : function(msg) {
						$result = msg.model.sBox;
						$bond_list = $result.debentureList;
						
						if($result.REPL_CD == '00000') {
							
							// 미수채권 리스트 HTML
							$bond_list_html = "";
							if($bond_list.length != 0){
								$.each($bond_list, function() {
									$contents = $(this).eq(0).get(0);

									$bond_list_html += '<tr>';
									$bond_list_html += '<td><input type="checkbox" class="checkBond" name="debnId" value="'+$contents.DEBN_ID+'" '+(($contents.DEBT_APPL_ID != null) ? 'checked="checked"' : '')+'"></td>';
									$bond_list_html += '<td>'+$contents.BILL_DT+'</td>';
									$bond_list_html += '<td class="list_right">'+$contents.SUM_AMT+'원</td>';
									$bond_list_html += '<td>'+$contents.DELAY_DAY+'일</td>';
									$bond_list_html += '<td class="list_right term last">'+$contents.RMN_AMT+'원</td>';
									$bond_list_html += '</tr>';
								});
							}else{
								$bond_list_html += '<tr>';
								$bond_list_html += '<td colspan="5">';
								$bond_list_html += '검색된 데이터가 존재하지 않습니다.';
								$bond_list_html += '</td>';
								$bond_list_html += '</tr>';
							}
							$('#bond_list').html("");
							$('#bond_list').append($bond_list_html);
							
							// 체크박스 및 선택채권, 선택채권 미수금총액 계산
							if (($('#bond_list').find('.checkBond').length != $('#bond_list').find('.checkBond:checked').length) || ($('#bond_list').find('.checkBond').length == 0)) {
								$('#check_all_bond').removeAttr('checked'); // 전체선택 체크박스 해제
								$('#selected_bond > p').text('0건');// 선택채권 0건
								$('#outstanding_sum > p').text('0원');// 선택채권 미수금총액 0원
							}
							
						} else {
							alert($result.REPL_MSG);
						}
						
					},
			error : function(xmlHttpRequest, textStatus, errorThrown){
						alert("거래처 미수채권 조회 도중 에러발생 [" + textStatus + "]");
					}
		});
	 };
	
	/*
	* <pre>
	* 채무불이행신청 증빙파일 리스트 출력
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.02
	*/
	$.getDebtPrfFileList = function($debtPrfFileList) {

		// 기존 요소 삭제
		$('#file_area > table').remove();
		$('#file_area').prepend($('#hidden_file_area').html());
		
		$.each($debtPrfFileList, function(index) {
			$fileHtml = '';
			$fileEl = $(this).get(0);
			
			$appendField = ($fileEl.TYPE_CD =='A') ? "data_A" : (($fileEl.TYPE_CD =='B') ? "data_B" : (($fileEl.TYPE_CD =='C') ? "data_C" : (($fileEl.TYPE_CD =='D') ? "data_D" : "data_E"))) ;
			$fileHtml += '<div class="dataUploadArea">';
			$fileHtml += '<span style="display:inline-block; margin: 5px 0px 5px 0px;">';
			$fileHtml += '<a href="#none" class="browsing" data-filePath="'+utfDecode($fileEl.FILE_PATH)+'" data-deleteDebtFileSN="'+$fileEl.DEBT_FILE_SN+'" style="color:#4f7eb2; text-decoration:none;">'+utfDecode($fileEl.FILE_NM)+'</a></span>';
			$fileHtml += '<div class="dataUpWrap02">';
			$fileHtml += '<a href="#none" class="btn_minus"><img src="${IMG}/common/ico_data_m.gif"></a>';
			$fileHtml += '</div></div><div class="spliter"></div>';
			
			$('#'+$appendField).prepend($fileHtml);
		});
		
	};
	
	/*
	* <pre>
	* 기업정보 팝업 수정 콜백함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.23
	*/
	$.fn.modifyCorpInfo = function($corpType, $corpNm, $corpNo, $ownNm, $bizType, $post1, $addr1, $addr2) {// $post2,

		if ($('.applyCustType:checked').val() != $corpType) {
			$.changeCorpForm($('#apply_company_info'), $corpType);
		}
		
		if ($corpType == 'C') {
			$('#applicant_person').removeAttr('checked');
			$('#applicant_company').attr('checked', 'checked');	
		} else if ($corpType == 'I') {
			$('#applicant_company').removeAttr('checked');
			$('#applicant_person').attr('checked', 'checked');
		}
		
		$('#apply_corp_name').val($corpNm);
		$('#apply_corp_num').val($corpNo);
		$('#apply_own_name').val($ownNm);
		$('#apply_biz_type').val($bizType);
		$('#apply_post_no1').val($post1);
		//$('#apply_post_no2').val($post2);
		$('#apply_addr_1').val($addr1);
		$('#apply_addr_2').val($addr2);
	};
	
	/*
	* <pre>
	* 회원정보 팝업 수정 콜백함수
	* </pre>
	* @author YouKyung Hong
	* @since 2014.05.23
	*/
	$.fn.modifyUserInfo = function($userNm, $deptNm, $jobTlNm, $telNo1, $telNo2, $telNo3, $mbNo1, $mbNo2, $mbNo3, $faxNo1, $faxNo2, $faxNo3, $email1, $email2, $selectEmail) {
		$('#manager_name').val($userNm);
		$('#manager_part').val($deptNm);
		$('#manager_position').val($jobTlNm);
		$('#tel_no1').val($telNo1);
		$('#tel_no2').val($telNo2);
		$('#tel_no3').val($telNo3);
		$('#mb_no1').val($mbNo1);
		$('#mb_no2').val($mbNo2);
		$('#mb_no3').val($mbNo3);
		$('#fax_no1').val($faxNo1);
		$('#fax_no2').val($faxNo2);
		$('#fax_no3').val($faxNo3);
		$('#email1').val($email1);
		$('#email2').val($email2);
		$('#select_email').val($selectEmail);
	};
	
	<%--채무자 우편번호 검색 팝업창
	function open_post(){ 
		open_win('${HOME}/zip/zipSearchPopup.do?otherFunction=deptorAddressResult');
	}--%>
	
	<%--신청인 우편번호 검색 팝업창
	function open_post_apply(){ 
		open_win('${HOME}/zip/zipSearchPopup.do?otherFunction=resultReceiveApply');
	}--%>
	
	/*
	* <pre>
	* 채무자 우편번호 값 가져오기
	* </pre>
	* @author youkyung Hong
	* @since 2014.05.01
	
	function deptorAddressResult(zip, address) {
		$('#post_no1').val(zip.substring(0,3));
		$('#post_no2').val(zip.substring(3,6));
		$('#cust_addr_1').val(address);
		$('#cust_addr_2').val("");
		
		if($('#step2_status').val() == 'complete') {
			$('#post_no1').addClass('changeEl');
			$('#post_no2').addClass('changeEl');
			$('#cust_addr_1').addClass('changeEl');
			$('#cust_addr_2').addClass('changeEl');
		}
	}*/
	
	<%--신청인 우편번호 값 가져오기
	function resultReceiveApply(zip, address){
		$('#apply_post_no1').val(zip.substring(0,3));
		$('#apply_post_no2').val(zip.substring(3,6));
		$('#apply_addr_1').val(address);
		$('#apply_addr_2').val("");
		
		if($('#step1_status').val() == 'complete') {
			$('#apply_post_no1').addClass('changeEl');
			$('#apply_post_no2').addClass('changeEl');
			$('#apply_addr_1').addClass('changeEl');
			$('#apply_addr_2').addClass('changeEl');
		}
	}--%>

		
</script>
</head>
<body>

<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0304" />
</jsp:include>
<%-- Top Area End --%>

<%-- contentArea --%>
<div class="contentArea sub07">
	<%-- menu별 이미지 class sub00 --%>
	<%-- HIDDEN AREA START --%>
	<input type="hidden" id="step1_status" value="check" />
	<input type="hidden" id="step2_status" value="check" />
	<input type="hidden" id="step3_status" value="complete" />
	<input type="hidden" id="step4_status" value="complete" />
	
	<%-- 접속한 유저의 사업자 번호 --%>
	<input type="hidden" id="usrNo" value="${sBox.sessionUsrNo}" />
	
	<%-- HIDDEN AREA END --%>
	<%-- content --%>
	<section id="content">

		<div class="titbox">
			<h2 class="tit_me03_02">채무불이행 신청서 작성</h2>
			<p class="tit_me03_02_under">채무불이행 등록을 위한 신청서를 작성하실 수 있습니다</p>
		</div>

		<h3 class="norm">
			<img src="${IMG}/common/h3_default_add04.gif" alt="채무불이행 신청서 작성 방법" />
		</h3>

		<%-- 채무불이행 신청서 작성 방법 테이블 시작 --%>
		<div class="default_howStep">
			<ul>
				<li class="s1 on"><p><em>STEP 01. 필수 : 신청인 정보입력</em></p></li>
				<li class="s2 on"><p><em>STEP 02. 필수 : 채무자 정보입력</em></p><i></i></li>
				<li class="s3 on"><p><em>STEP 03. 선택 : 채권 불러오기</em></p><i></i></li>
				<li class="s4 on"><p><em>STEP 04. 필수 : 거래 증빙자료 첨부 및 작성완료</em></p><i></i></li>
				<li class="s5 on"><p><em>STEP 05. 필수 : 채무불이행 신청하기</em></p><i></i></li>
			</ul>
		</div>
		<%-- 채무불이행 신청서 작성 방법 테이블 끝 --%>

		<h3 class="norm">
			<img src="${IMG}/common/h3_default_add03.gif" alt="채무불이행 신청서" />
		</h3>

		<%-- 채무불이행 신청서 테이블 시작 --%>
		<div id="defaultApplyWrap">
		<form autocomplete="off" enctype="multipart/form-data" method="post">
		<%-- HIDDEN AREA START --%>
		<input type="hidden" id="step" name="step" value="" />
		
		<input type="hidden" id="delete_debt_file_SN" name="deleteDebtFileSN" value="" />
		
		<input type="hidden" id="debt_appl_seq" name="debtApplSeq" value="${debtBox.DEBT_APPL_ID}" />
		<input type="hidden" id="tot_unpd_amt" name="totUnpdAmt" value="${fn:replace(debtBox.TOT_UNPD_AMT, ',', '')}" />
		
		<input type="hidden" id="file_path" name="filePath" />
		<input type="hidden" id="file_nm" name="fileNm" />
		<%-- HIDDEN AREA END --%>
			<div class="default_Apply">
				<ul id="menu_slide">
					<li class="Apply_slide"><span class="slideSpan">
					<a href="#" id="btn_step_one_title" class="up" style="text-decoration: none;">STEP 01. 신청인 정보입력</a></span>
						<ul>

							<%-- STEP01 시작 --%>
							<li class="Apply_sub">
								<div id="step1" class="ApplysubWrap">
									<h4 class="normh4">▶ 회사정보</h4>
									<%-- 신청인 회사정보 form 시작 --%>
									<table id="apply_company_info" class="defaultTbl01_01" summary="회사정보 입력테이블">
										<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">구분<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="radio" class="applyCustType" name="applyCustType" value="C" id="applicant_company" <c:if test="${debtBox.COMP_TYPE eq 'C'}">checked</c:if> /> 
													<label for="applicant_company">법인사업자</label>&nbsp;&nbsp;&nbsp;&nbsp; 
													<input type="radio" class="applyCustType" name="applyCustType" value="I" id="applicant_person" <c:if test="${debtBox.COMP_TYPE eq 'I'}">checked</c:if> /> 
													<label for="applicant_person">개인사업자</label>
												</td>
											</tr>
											<tr>
												<c:set var="COMP_NO" value="${fn:substring(debtBox.COMP_NO, 0, 3)}-${fn:substring(debtBox.COMP_NO, 3, 5)}-${fn:substring(debtBox.COMP_NO, 5, 13)}" />
												<td class="title">회사명<em>*</em></td>
												<td>
													<input type="text" id="apply_corp_name" name="applyCorpName" class="default_txtInput required" style="width: 200px;" title="회사명" value="${debtBox.COMP_NM}" maxlength="70"> 
												</td>
												<td class="title">사업자번호<em>*</em></td>
												<td>
													<input type="text" id="apply_cust_num" name="applyCustNum" class="default_txtInput required usrNo" style="width: 200px;" title="사업자번호" value="${COMP_NO}" maxlength="10"> 
												</td>
											</tr>
											<tr>
												<td class="title">대표자명<em>*</em></td>
												<td>
													<input type="text" id="apply_own_name" name="applyOwnName" class="default_txtInput required" style="width: 200px;" title="대표자명" value="${debtBox.OWN_NM}" maxlength="30">
												</td>
												<c:choose>
												<c:when test="${debtBox.COMP_TYPE ne 'I'}">
													<td class="title change">법인등록번호</td>
													<td class="change">
														<input type="text" id="apply_corp_num" name="applyCorpNum" class="default_txtInput corpNo numeric" style="width: 200px;" title="법인등록번호" value="${debtBox.CORP_NO}" maxlength="13">
													</td>
												</c:when>
												<c:otherwise>
													<td class="title change">업종<em>*</em></td>
													<td class="default_note change">
														<input type="text" id="apply_biz_type" name="applyBizType" class="default_txtInput required" style="width: 200px;" title="업종" value="${debtBox.BIZ_TYPE}" maxlength="40">
													</td>
												</c:otherwise>
												</c:choose>
											</tr>
											<tr <c:if test="${debtBox.COMP_TYPE eq 'I'}">style="display:none;"</c:if>>
												<td class="title">업종<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="text" id="apply_biz_type" name="applyBizType" class="default_txtInput required" style="width: 200px;" title="업종" value="${debtBox.BIZ_TYPE}" maxlength="40">
												</td>
											</tr>
											<tr>
												<td class="title">주소<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="text" class="default_txtInput required numeric postNo" id="apply_post_no1" name="applyPostNo1" title="우편번호" maxlength="5" value="${fn:trim(debtBox.POST_NO)}" style="width: 40px;">
													<%-- <input type="text" class="default_txtInput required" id="apply_post_no2" name="applyPostNo2" title="우편번호 뒷자리" maxlength="3" value="${fn:substring(debtBox.POST_NO, 3, 6)}" readonly="readonly" onClick="javascript:open_post_apply(); return false;" style="width: 40px;"> --%>
													<!-- <a href="#" class="ApplyBtn02" onClick="javascript:open_post_apply(); return false;">우편번호</a> -->&nbsp; 
													<input type="text" class="default_txtInput required" id="apply_addr_1" name="applyAddr1" title="주소" style="width: 237px;" value="${debtBox.ADDR_1}" readonly="readonly" maxlength="75" > 
													<input type="text" class="default_txtInput" id="apply_addr_2" name="applyAddr2" title="상세주소" style="width: 280px;" value="${debtBox.ADDR_2}" maxlength="75">
												</td>
											</tr>
										</tbody>
									</table>
									
									<%-- 신청인 회사정보 form 끝 --%>
									<%-- <div class="ApplyBtn">
										<div class="ApplyBtn01">
											<a id="btn_corp_info_modify" class="on" href="#none" <c:if test="${sBox.sessionAdmYn ne 'Y'}">style="display: none;"</c:if>>정보수정</a>
										</div>
									</div> --%>
									<%-- HIDDEN AREA START --%>
									<input type="hidden" name="usrId" value="${debtBox.REG_USR_ID}" />
									<%-- HIDDEN AREA END --%>
									<h4 class="normh4">▶ 정보등록 담당자</h4>
									<table id="apply_manager_info" class="defaultTbl01_01" summary="정보등록 담당자">
										<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">성명<em>*</em></td>
												<td>
													<span>${debtBox.USR_NM}</span>
													<input type="hidden" id="manager_name" name="managerName" class="default_txtInput required" style="width: 200px;" title="성명" value="${debtBox.USR_NM}" maxlength="30">
												</td>
												<td class="title">부서</td>
												<td>
													<input type="text" id="manager_part" name="managerPart" class="default_txtInput" style="width: 200px;" title="부서" value="${debtBox.DEPT_NM}" maxlength="30">
												</td>
											</tr>
											<tr>
												<td class="title">직위</td>
												<td class="default_note">
													<input type="text" id="manager_position" name="managerPosition" class="default_txtInput" value="${debtBox.JOB_TL_NM}" style="width: 200px;" title="직위" maxlength="15">
												</td>
												<c:set var="TEL_NO" value="${fn:split(debtBox.TEL_NO,'-')}" />
												<td class="title">전화</td>
												<td class="default_note">
													<select id="tel_no1" name="telNo1" title="전화번호 국번" style="height: 22px;">
														<c:forEach var="phoneCodeList" items="${phoneCodeList}">
															<option value="${phoneCodeList}" <c:if test="${phoneCodeList eq TEL_NO[0]}">selected</c:if>>${phoneCodeList}</option>
														</c:forEach>
													</select> - 
													<input type="text" class="default_txtInput numeric phoneNo midNo" id="tel_no2" name="telNo2" title="전화번호 앞자리" value="${TEL_NO[1]}" style="width: 60px;" maxlength="4"> - 
													<input type="text" class="default_txtInput numeric phoneNo lastNo" id="tel_no3" name="telNo3" title="전화번호 뒷자리" value="${TEL_NO[2]}" style="width: 60px;" maxlength="4">
												</td>
											</tr>
											<tr>
												<c:set var="MB_NO" value="${fn:split(debtBox.MB_NO,'-')}" />
												<td class="title">휴대폰<em>*</em></td>
												<td class="default_note">
													<select class="required" id="mb_no1" name="mbNo1" title="휴대폰번호 국번" style="height: 22px;">
														<c:forEach var="cellPhoneCodeList" items="${cellPhoneCodeList}">
															<option value="${cellPhoneCodeList}" <c:if test="${cellPhoneCodeList eq MB_NO[0]}">selected</c:if>>${cellPhoneCodeList}</option>
														</c:forEach>
													</select> - 
													<input type="text" class="default_txtInput numeric required phoneNo midNo" id="mb_no2" name="mbNo2" title="휴대폰번호 앞자리" value="${MB_NO[1]}" style="width: 60px;" maxlength="4"> - 
													<input type="text" class="default_txtInput numeric required phoneNo lastNo" id="mb_no3" name="mbNo3" title="휴대폰번호 뒷자리" value="${MB_NO[2]}" style="width: 60px;" maxlength="4">
												</td>
												<c:set var="FAX_NO" value="${fn:split(debtBox.FAX_NO,'-')}" />
												<td class="title">팩스</td>
												<td class="default_note">
													<select id="fax_no1" name="faxNo1" title="팩스번호 국번" style="height: 22px;">
														<c:forEach var="faxCodeList" items="${faxCodeList}">
															<option value="${faxCodeList}" <c:if test="${faxCodeList eq FAX_NO[0]}"></c:if>>${faxCodeList}</option>
														</c:forEach>
													</select> - 
													<input type="text" class="default_txtInput numeric phoneNo midNo" id="fax_no2" name="faxNo2" title="팩스번호 앞자리" value="${FAX_NO[1]}" style="width: 60px;" maxlength="4"> - 
													<input type="text" class="default_txtInput numeric phoneNo lastNo" id="fax_no3" name="faxNo3" title="팩스번호 뒷자리" value="${FAX_NO[2]}" style="width: 60px;" maxlength="4">
												</td>
											</tr>
											<tr>
												<c:set var="EMAIL" value="${fn:split(debtBox.EMAIL,'@')}" />
												<td class="title">이메일<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="text" class="default_txtInput required" id="email1" name="email1" title="이메일 주소" value="${EMAIL[0]}" style="width: 127px;" maxlength="30"> @ 
													<input type="text" class="default_txtInput required" id="email2" name="email2" title="이메일 도메인" value="${EMAIL[1]}" style="width: 130px;" maxlength="30">
													<select id="select_email" title="이메일 도메인 선택" style="height: 22px;">
														<c:forEach var="emailCodeList" items="${emailCodeList}">
															<option value="${emailCodeList}" <c:if test="${emailCodeList eq EMAIL[1]}">selected</c:if>>
																<c:choose>
																	<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
																	<c:otherwise>직접입력</c:otherwise>
																</c:choose>
															</option>
														</c:forEach>
													</select>
												</td>
											</tr>
										</tbody>
									</table>

									<div class="ApplyBtn">
										<em>*</em> 항목은 필수 입력 사항입니다.<br>
										<em>*</em> 부서, 직위, 전화번호는 통보서에 기재되어 채무자에게 전달됩니다. 원활한 업무를 위해 입력해주세요.<br>
										<!-- <p class="Apply_checkbox">
											<input type="checkbox" id="applyRemYn" class="remYn" name="applyRemYn">
											정보등록 담당자의 회원정보도 함께 수정합니다.
										</p> -->
										<!-- <div class="ApplyBtn01">
											<a id="btn_memb_info_modify" class="on" href="#none">정보수정</a>
										</div> -->
									</div>

									<p class="DotLine"></p>

									<div class="ApplyBtn_Add">
										<div class="ApplyBtn_Step">
											<a id="btn_one_to_two" href="#none">채무자 정보입력 가기 (Step 2)</a>
										</div>
									</div>
								</div>
							</li>
							<%-- STEP01 끝 --%>

						</ul></li>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan">
						<a href="#" id="btn_step_two_title" class="down" style="text-decoration: none;">STEP 02. 채무자 정보입력</a></span>
						<ul style="display: none;">

							<%-- STEP02 시작 --%>
							<li class="Apply_sub">
								<div id="step2" class="ApplysubWrap">
								<%-- FORM HIDDEN AREA START --%>
								<input type="hidden" id ="cust_id" name="custId" value="${debtBox.CUST_ID}" title="거래처순번"/>
								<%-- FORM HIDDEN AREA END --%>
								
									<table id="deptor_company_info" class="defaultTbl01_01" summary="회사정보 입력테이블">
										<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">구분<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="radio" class="deptorCustType" name="deptorCustType" value="C" id="deptor_company" <c:if test="${debtBox.CUST_TYPE eq 'C' or debtBox.CUST_TYPE eq null}">checked="checked"</c:if>/> 
													<label for="deptor_company">법인사업자</label>&nbsp;&nbsp;&nbsp;&nbsp; 
													<input type="radio" class="deptorCustType" name="deptorCustType" value="I" id="deptor_person" <c:if test="${debtBox.CUST_TYPE eq 'I'}">checked="checked"</c:if>/> 
													<label for="deptor_person">개인사업자</label>
												</td>
											</tr>
											<tr>
												<td class="title">회사명 + 사업자번호<em>*</em></td>
												<td colspan="3">
													<input type="text" id="cust_nm" name="deptorCorpName" class="default_txtInput customerPopup required" value="${debtBox.CUST_NM}" style="width: 200px;" title="회사명"  readonly="readonly" maxlength="20"> 
													<c:set var="CUST_USR_NO" value="${fn:substring(debtBox.CUST_NO, 0, 3)}-${fn:substring(debtBox.CUST_NO, 3, 5)}-${fn:substring(debtBox.CUST_NO, 5, 13)}" />
													<input type="text" id="cust_no" name="deptorCustNum" class="default_txtInput customerPopup required usrNo" <c:if test="${debtBox.CUST_NO ne null}" >value="${CUST_USR_NO}"</c:if> style="width: 200px;" title="사업자번호" readonly="readonly" maxlength="20"> 
													<a href="#none" class="ApplyBtn02 customerPopup">검색</a>
												</td>
											</tr>
											<tr>
												<td class="title">대표자명<em>*</em></td>
												<td<c:if test="${debtBox.CUST_TYPE eq 'I'}"> colspan="3"</c:if>>
													<input type="text" id="deptor_name" name="deptorName" class="default_txtInput required" value="${debtBox.CUST_OWN_NM}" style="width: 200px;" title="대표자명" maxlength="20">
												</td>
												<c:if test="${debtBox.CUST_TYPE ne 'I'}">
													<td class="title change">법인등록번호</td>
													<td class="change">
														<input type="text" id="deptor_corp_num" name="deptorCorpNum" class="default_txtInput numeric corpNo" value="<c:if test="${debtBox.CUST_CORP_NO ne 'null'}">${debtBox.CUST_CORP_NO}</c:if>" style="width: 200px;" title="법인등록번호" maxlength="20">
													</td>
												</c:if>
											</tr>
											<tr>
												<td class="title">통보주소<em>*</em></td>
												<td class="default_note" colspan="3">
													<input type="text" class="default_txtInput required numeric postNo" id="post_no1" name="postNo1" value="${fn:trim(debtBox.CUST_POST_NO)}" title="우편번호"  maxlength="5"  style="width: 40px;">
													<%-- <input type="text" class="default_txtInput required" id="post_no2" name="postNo2" value="${fn:substring(debtBox.CUST_POST_NO, 3, 6)}" title="우편번호 뒷자리"  maxlength="3" readonly="readonly" onClick="javascript:open_post(); return false;" style="width: 40px;"> --%>
													<!-- <a href="#" class="ApplyBtn02" onClick="javascript:open_post(); return false;">우편번호</a> -->&nbsp; 
													<input type="text" class="default_txtInput required" id="cust_addr_1" name="addr1" value="${debtBox.CUST_ADDR_1}" title="주소"  readonly="readonly" style="width: 237px;" maxlength="30" > 
													<input type="text" class="default_txtInput" id="cust_addr_2" name="addr2" value="${debtBox.CUST_ADDR_2}" title="상세주소" style="width: 280px;" maxlength="50">
												</td>
											</tr>
											<tr>
												<td class="title">채무구분<em>*</em></td>
												<td>
													<select id="dept_division" name="dept_division" title="채무구분 선택" style="height: 22px;">
														<option value="delayPayment" <c:if test="${debtBox.DEBT_TYPE eq 'P'}">selected="selected"</c:if>>대금연체</option>
														<option value="jointSurety" <c:if test="${debtBox.DEBT_TYPE eq 'J'}">selected="selected"</c:if>>연대보증인</option>
													</select>
												</td>
												<td class="title">채무불이행 금액<em>*</em></td>
												<td>
													<c:set var="ST_DEBT_AMT" value="${fn:replace(debtBox.ST_DEBT_AMT, ' ', '')}" />
													<input type="text" id="nonpay_sum" name="nonpaySum" class="default_txtInput required nonpaySum numericMoney unsigned" value="${ST_DEBT_AMT}" title="채무불이행 금액" placeholder="50,000 이상 입력가능" maxlength="14" style="width: 200px;">
												</td>
											</tr>
										</tbody>
									</table>
									
									
									<div class="ApplyBtn">
										<em>*</em> 항목은 필수 입력 사항입니다.<br>
										<p class="Apply_checkbox">
											<c:if test="${sBox.isSessionCustAddGrn eq true}">
											<%-- <input type="checkbox" class="remYn" name="remYn"> 
											<span>
												<c:choose>
													<c:when test="${fn:trim(debtBox.CUST_ID) ne ''}">
														위 채무자 정보로 거래처를 수정합니다.
													</c:when>
													<c:otherwise>
														위 채무자를 거래처로 등록합니다.
													</c:otherwise>	
												</c:choose>
											</span> --%>
											</c:if>
										</p>
									</div>

									<p class="DotLine"></p>

									<div class="ApplyBtn_Add">
										<div class="ApplyBtn_Step">
											<a href="#none" id="btn_two_to_one" class="goToPrev">신청인 정보로 돌아가기 (Step 1)</a>&nbsp; 
											<a href="#none" id="btn_two_to_three" class="step2_info">미수채권<font color="#ff0000">선택</font> 작성하기 (Step 3)</a>&nbsp; 
											<a href="#none" id="btn_two_to_four" class="step2_info">증빙자료 첨부하기 (Step 4)</a>
										</div>
									</div>
								</div>
							</li>
							<%-- STEP02 끝 --%>

						</ul></li>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan">
						<a href="#" id="btn_step_three_title" class="down" style="text-decoration: none;">STEP 03. 미수채권 불러오기 (선택사항)</a></span>
						<ul style="display: none;">

							<%-- STEP03 시작 --%>
							<li class="Apply_sub">
								<div id="step3" class="ApplysubWrap">

									<div class="ApplyListWrap">
										<div class="ApplyList_left">
											<input type="checkbox" id="debt_guide_view" name="debtGuideView" /> 세금계산서작성일 60일 이후 건만 보이기
										</div>
										<div class="ApplyList_right">
											<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자, 금액별로 정렬" style="height: 22px;">
												<c:forEach var="npmOrderConditionTypeList" items="${commonCodeBox.npmOrderConditionTypeList}">
													<c:choose>
														<c:when test="${sBox.orderCondition ne null}">
															<c:if test="${sBox.orderCondition eq npmOrderConditionTypeList.KEY}">
																<c:set var="init_selected" value="selected" />
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${npmOrderConditionTypeList.KEY == 'BILL_DT'}">
																<c:set var="init_selected" value="selected" />
															</c:if>
														</c:otherwise>
													</c:choose>
												
													<option value="${npmOrderConditionTypeList.KEY}" ${init_selected}>${npmOrderConditionTypeList.VALUE}</option>
													
													<c:set var="init_selected" value="" />
												</c:forEach>
											</select> 
											<select id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬" style="height: 22px;">
												<c:forEach var="orderTypeList" items="${commonCodeBox.orderTypeList}">
													<c:choose>
														<c:when test="${sBox.orderType ne null}">
															<c:if test="${sBox.orderType eq orderTypeList.KEY}">
																<c:set var="init_selected" value="selected" />
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${orderTypeList.KEY == 'DESC'}">
																<c:set var="init_selected" value="selected" />
															</c:if>
														</c:otherwise>
													</c:choose>
													
													<option value="${orderTypeList.KEY}" ${init_selected}>${orderTypeList.VALUE}</option>
													
													<c:set var="init_selected" value="" />
												</c:forEach>
											</select> 
											<label for="listNo"></label> 
											<select name="bondMbrId" id="bond_mbr_id" class="listOrderOption" style="height: 22px;">
											<option value="" selected="selected">전체</option>
												<c:forEach var="debentureContactList" items="${result.debtBox.debentureContactList}">
													<option value="${debentureContactList.MBR_ID}" <c:if test="${debentureContactList.MBR_USR_ID ne null && sBox.bondMbrId eq debentureContactList.MBR_USR_ID}">selected</c:if> <c:if test="${debentureContactList.MBR_USR_ID eq null}">class="mem_type_N"</c:if>>${debentureContactList.USR_NM}<c:if test="${debentureContactList.MBR_USR_ID eq null}">(비회원)</c:if></option>
												</c:forEach>
											</select> 
											<a href="#none" id="btn_search_bond" class="ApplyBtn02" style="font-size: 12px;">검색</a>
										</div>
									</div>

									<table class="defaultTbl01_02" summary="미수채권 불러오기">
										<caption>미수채권 불러오기</caption>
										<colgroup>
											<col width="30px">
											<col width="228px">
											<col width="228px">
											<col width="228px">
											<col width="228px">
										</colgroup>
										<thead>
											<tr>
												<th scope="col"><input type="checkbox" id="check_all_bond" /></th>
												<th scope="col">세금계산서작성일</th>
												<th scope="col">채권합계금액</th>
												<th scope="col">연체일수</th>
												<th scope="col">미수금액</th>
											</tr>
										</thead>
									</table>
									<div class="defaultTbl01_02_Content" style="height: auto;">
										<table summary="미수채권" class="defaultTbl01_02_list">
											<caption>미수채권</caption>
											<colgroup>
												<col width="32px">
												<col width="228px">
												<col width="228px">
												<col width="228px">
												<col width="228px">
											</colgroup>
											<tbody id="bond_list">	
												<c:set var="checkDebentureCnt" value="0"/>
												<c:set var="checkDebentureSumAmt" value="0"/>
												<c:if test="${fn:length(result.debtBox.debentureList) == 0}">
													<tr>
														<td colspan="5">검색결과가 존재하지 않습니다.</td>
													</tr>
												</c:if>
												<c:forEach var="i" items="${result.debtBox.debentureList}">
													<c:if test="${i.DEBT_APPL_ID ne null}">
														<c:set var="checkDebentureCnt" value="${checkDebentureCnt+1}"/>
														<c:set var="checkDebentureSumAmt" value="${fn:replace(i.SUM_AMT,',','') + checkDebentureSumAmt}"/>
													</c:if>
													<tr>
														<td><input type="checkbox" class="checkBond" name="debnId" value="${i.DEBN_ID}" <c:if test="${i.DEBT_APPL_ID ne null}">checked</c:if> ></td>
														<td>${i.BILL_DT}</td>
														<td class="list_right">${i.SUM_AMT}원</td>
														<td>${i.DELAY_DAY}일</td>
														<td class="list_right term last">${i.RMN_AMT}원</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
									<div class="defaultSumWrap">
										<table id="bond_stats" class="defaultSumTable">
											<caption>통계</caption>
											<colgroup>
												<col width="30%">
												<col width="30%">
												<col width="40%">
											</colgroup>
											<tbody>
												<tr>
													<td id="selected_bond">선택채권 :
														<p>${checkDebentureCnt}건</p>
													</td>
													<td id="outstanding_sum">선택채권 미수금총액 :
														<p><fmt:formatNumber pattern="#,##0" value="${checkDebentureSumAmt}"></fmt:formatNumber>원</p>
													</td>
													<td style="line-height: 27px">채무불이행 금액 : 
													<input type="text" id="bond_sum" name="bondSum" class="default_mInput numericMoney unsigned" value="${ST_DEBT_AMT}" style="width: 180px;" title="채무불이행 금액" maxlength="14">&nbsp;원
													</td>
												</tr>
											</tbody>
										</table>
									</div>

									<p class="DotLine"></p>

									<div class="ApplyBtn_Add">
										<div class="ApplyBtn_Step">
											<a id="btn_three_to_two" class="step3_info" href="#none" >채무자 정보로 돌아가기 (Step 2)</a>&nbsp; 
											<a id="btn_three_to_four" class="step3_info" href="#none" >증빙자료 첨부하기 (Step 4)</a>
										</div>
									</div>
								</div>
							</li>
							<%-- STEP03 끝 --%>

						</ul></li>
					<p class="line" ></p>
					<li class="Apply_slide"><span class="slideSpan">
						<a href="#" id="btn_step_four_title" class="down" style="text-decoration: none;">STEP 04. 거래증빙자료 첨부 및 작성완료</a></span>
						<ul style="display: none;">

							<%-- STEP04 시작 --%>
							<li class="Apply_sub">
								<div id="file_area" class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블">
									<caption>거래증빙자료 첨부 및 작성완료</caption>
										<colgroup>
											<col width="17%">
											<col width="83%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title stat_A">거래명세서<br/>(최대 20MB)</td>												
												<td id="data_A" class="default_note">
													<c:forEach var="i" items="${result.debtBox.debentureFileList}" varStatus="idx">
														<c:if test="${i.TYPE_CD == 'A' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${i.FILE_PATH}" data-deleteDebtFileSN="${i.DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${i.FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_E" name="dataUploadA" class="default_txtFileInput" maxlength="100" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>										
											<tr>
												<td class="title stat_B">세금계산서<br/>(최대 20MB)</td>
												<td id="data_B" class="default_note">
													<c:forEach var="i" items="${result.debtBox.debentureFileList}" varStatus="idx">
														<c:if test="${i.TYPE_CD == 'B' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${i.FILE_PATH}" data-deleteDebtFileSN="${i.DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${i.FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_E" name="dataUploadB" class="default_txtFileInput" maxlength="100" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
													</div>
												</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_C">법원판결문<br/>(최대 20MB)</td>
												<td id="data_C" class="default_note">
													<c:forEach var="i" items="${result.debtBox.debentureFileList}" varStatus="idx">
														<c:if test="${i.TYPE_CD == 'C' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${i.FILE_PATH}" data-deleteDebtFileSN="${i.DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${i.FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_E" name="dataUploadC" class="default_txtFileInput" maxlength="100" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_D">계약서<br/>(최대 20MB)</td>
												<td id="data_D" class="default_note">
													<c:forEach var="i" items="${result.debtBox.debentureFileList}" varStatus="idx">
														<c:if test="${i.TYPE_CD == 'D' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${i.FILE_PATH}" data-deleteDebtFileSN="${i.DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${i.FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_E" name="dataUploadD" class="default_txtFileInput" maxlength="100" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="title stat_E">기타증빙자료<br/>(최대 20MB)</td>
												<td id="data_E" class="default_note">
													<c:forEach var="i" items="${result.debtBox.debentureFileList}" varStatus="idx">
														<c:if test="${i.TYPE_CD == 'E' }">
															<div class="dataUploadArea">
																<span style="display:inline-block; margin: 5px 0px 5px 0px;">
																	<a href="#none" class="browsing" data-filePath="${i.FILE_PATH}" data-deleteDebtFileSN="${i.DEBT_FILE_SN}" style="color:#4f7eb2; text-decoration:none;">${i.FILE_NM}</a>
																</span>
																<div class="dataUpWrap02">
																	<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
																</div>
															</div>
															<div class="spliter"></div>
														</c:if>
													</c:forEach>
													<div class="dataUploadArea">
														<input type="file" id="data_upload_E" name="dataUploadE" class="default_txtFileInput" maxlength="100" />
														<div class="dataUpWrap02">
														<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
														<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>

									<div class="ApplyBtn">
										<font color="#ff0000">※ 주의하세요.</font><br />
										&nbsp;&nbsp;&nbsp;&nbsp;반드시 한 개 이상의 거래증빙 자료를 첨부하셔야 작성이 완료되어 채무불이행 신청을 하실 수 있습니다.<br /> 
										&nbsp;&nbsp;&nbsp;&nbsp;증빙자료 내용에는 공급자정보, 채무자정보, 거래내역, 서명(거래명세서)등의 내용이 포함되어 있어야 합니다.<br />
										<font color="4f7eb2">&nbsp;&nbsp;&nbsp;&nbsp;파일업로드는 20M bytes 이상 파일을 업로드 할 수 없습니다.</font><br />
										&nbsp;&nbsp;&nbsp;&nbsp;문서 (txt, rtf, xls, xlsx, doc, docx, ppt, pptx, pps, ppsx, hwp, pdf), 이미지 (gif,jpg, jpeg, png, bmp), 압축 (zip) 파일 첨부가능<br />
										
										<div class="ApplyBtn01">
											<a id="btn_upload_attachment" class="step4_info" href="#none" style="width: 110px;">증빙자료 업로드</a> 
											<a id="btn_save_apply_form" class="on step4_info" href="#none">작성완료</a>
										</div>
									</div>
								</div>
							</li>
							<%-- STEP04 끝 --%>

						</ul></li>
				</ul>
			</div>
			</form>
		</div>
		<%-- 채무불이행 신청서 테이블 끝 --%>

		<%-- //content --%>
	</section>
</div>

<%-- //contentArea --%>
<%-- HIDDEN AREA START --%>
<div id="hidden_file_area" style="display: none;">
	<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블">
		<caption>거래증빙자료 첨부 및 작성완료</caption>
			<colgroup>
				<col width="17%">
				<col width="83%">
			</colgroup>
			<tbody>
				<tr>
					<td class="title stat_A">거래명세서<br/>(최대 20MB)</td>												
					<td id="data_A" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_A" name="dataUploadA" class="default_txtFileInput" maxlength="100" />
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>										
				<tr>
					<td class="title stat_B">세금계산서<br/>(최대 20MB)</td>
					<td id="data_B" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_B" name="dataUploadB" class="default_txtFileInput" maxlength="100" />
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>	
					</td>
				</tr>
				<tr>
					<td class="title stat_C">법원판결문<br/>(최대 20MB)</td>
					<td id="data_C" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_C" name="dataUploadC" class="default_txtFileInput" maxlength="100" />
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="title stat_D">계약서<br/>(최대 20MB)</td>
					<td id="data_D" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_D" name="dataUploadD" class="default_txtFileInput" maxlength="100" />
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="title stat_E">기타증빙자료<br/>(최대 20MB)</td>
					<td id="data_E" class="default_note">
					<div class="dataUploadArea">
						<input type="file" id="data_upload_E" name="dataUploadE" class="default_txtFileInput" maxlength="100" />
						<div class="dataUpWrap02">
						<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
						<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>
						</div>
					</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<%-- HIDDEN AREA END --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>