<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행 Master Table 정보 --%>
<c:set var="debt" value="${result.debt}" />

<%-- 취소 사유 리스트 정보 --%>
<c:set var="rmkTxtList" value="${result.rmkTxtList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<%@ include file="../common/meta.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>스마트비즈 for Legacy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtRequireRelease.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	jQuery.support.cors = true;
	
	/*
	* <pre>
	*   취소 버튼 클릭 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('#btn_cancel').live({
		click : function() {
			self.close();
		}
	});
	
	/*
	* <pre>
	*   라디오 버튼 선택시 문구 자동 추가 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('input.radioRsnTxt, label.labelRsnTxt').live({
		click : function() {
			$('#rmk_txt').parent().empty().append(
				'<textarea id="rmk_txt" class="rmxTxt" maxlength="2000"></textarea>'		
			);
			$('#rmk_txt').val($('#' + $(this).attr('id')).attr('data-rmk-txt'));
		}
	});
	
	/*
	* <pre>
	*   신청취소 버튼 클릭 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('#btn_apply_cancel').live({
		click : function() {
			if(confirm('신청취소 하시겠습니까?')) {
				$.fn.cancelDebtApply();
			}
		}
	});
	
	<%-- 첫 라디오 버튼 선택 js 함수 --%>
	$('.radioRsnTxt:eq(0)').trigger('click');
	
});	

<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%>



<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>



<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>

/**
 * <pre> 
 *   신청취소 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 10
 * @param debtApplId : 채무불이행 순번, rmkTxt : 사유, debtStatSubCd : 채무불이행 상태 서브코드, debtStatCd : 채무불이행 상태
 *		  
 */
 $.fn.cancelDebtApply = function() {

  	 $paramObj = {
    	debtApplId : $('#debt_appl_id').val(),
    	rmkTxt : $('#rmk_txt').val(),
    	sessionSbLoginId : $('#sb_Login_id').val(),
    	debtStatSubCd : $('.radioRsnTxt:checked').val(),
    	debtStatCd : 'AC'
  	 };
  	 
   	 $param = $.param($paramObj);
   	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/debt/cancelDebtApply.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('신청취소가 완료되었습니다.');
			    	$(opener.location).attr("href", "javascript:$.fn.callBackDebtProcessStatChange('AC');");
			    	self.close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("신청취소 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };

<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>

</script>
</head>
<body>
<input type="hidden" id="sb_Login_id" name="sessionSbLoginId" value="${sBox.sessionSbLoginId}"/>
	<div id="defaultApplyWrap">
		<div class="ApplysubWrap">
			<p class="content_tit">채무불이행 신청취소</p>
			<!-- <h4 class="normh4">▶ 채무불이행 신청취소</h4> -->
			<div class="comment_1" style="font-size: 12px;">
				채무불이행 등록신청을 취소 하시겠습니까? <br/>
				신청 취소를 위해 취소 사유를 입력해 주세요.
			</div>
			
			<%-- 신청취소 테이블 시작 --%>
			<form name="applyCancelFrm" id="apply_cancel_frm" autocomplete="off">
				
				<%-- INPUT HIDDEN 영역 시작 --%>
				<input type="hidden" name="debtApplId" id="debt_appl_id" value="${sBox.debtApplId}"/>
				<input type="hidden" name="custId" id="cust_id" value="${sBox.custId}"/>
				<%-- INPUT HIDDEN 영역 종료 --%>
			
				<hr style="border-style: inset; border-width: 1px;"/>
				<div style="text-align:center; font-size: 12px;">
					<c:forEach var="rmkTxtList" items="${rmkTxtList }">
						<input id="radio_rsn_txt_${rmkTxtList.DEBT_RSN_TXT_ID}" type="radio" class="radioRsnTxt" name="debtStatSubCd" value="${rmkTxtList.DEBT_STAT_SUB_CD}" data-rmk-txt="${rmkTxtList.RMK_TXT }"/>  
						<label for="radio_rsn_txt_${rmkTxtList.DEBT_RSN_TXT_ID}" class="labelRsnTxt">${rmkTxtList.TL_NM}</label>
					</c:forEach>
				</div>
				<hr style="border-style: inset; border-width: 1px;"/>
				<div>
					<textarea class="rmxTxt" maxlength="2000" id="rmk_txt" form="apply_cancel_frm"></textarea>
				</div>
			</form>
			<%-- 신청취소 테이블 종료 --%>
		</div>
	</div>
	<br/><br/>
	<%-- 버튼 영역 시작 --%>
	<!-- <div id="tbl_numbtnWrap"> -->
	<table class="tbl_numbtn">
	<tbody>
		<tr>
			<td class="right">
				<div id="btnWrap_l">
					<div class="btn_graybtn_01">
						<a href="#none" class="ApplyBtn_Type on" id="btn_apply_cancel">확인</a>
					</div>
				</div>
	
				<div id="btnWrap_l">
					<div class="btn_graybtn_01">
						<a href="#none" class="ApplyBtn_Type on" id="btn_cancel">취소</a>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
	</table>
	<!-- </div> -->
	
<!-- 	
	<div class="pBtnWrap">
		<div class="pBtnMiddle">
			<a class="ApplyBtn_Type" href="#none" id="btn_apply_cancel">신청취소</a>
			<a class="ApplyBtn_Type" href="#none" id="btn_cancel">취소</a>
		</div>
	</div> -->
	<%-- 버튼 영역 종료 --%>
	
</body>
</html>