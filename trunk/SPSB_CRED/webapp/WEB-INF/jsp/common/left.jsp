<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- // 레프트 메뉴 시작 //-->	
<div id="leftWrap">
	<c:choose>
		<c:when test="${fn:substring(param.gnb, 0, 2) eq '00'}">
		<div class="leftGnb">
			<ul>
				<li class="left_tit ltit00_01"><em>마이페이지</em></li>
				<li><a href="${HOME}/mypage/myPageMain.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '01' ? 'on':'off'}">마이페이지</a></li>
				<li><a href="${HOME}/help/getNoticeList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '02' ? 'on':'off'}">공지사항</a></li>
<%-- 				<li><a href="${HOME}/help/getFaqList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '03' ? 'on':'off'}">FAQ</a></li>
				<li><a href="${HOME}/help/getInquiryList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '04' ? 'on':'off'}">1:1문의</a></li> --%>
			</ul>
		</div>
		</c:when>
		<c:when test="${fn:substring(param.gnb, 0, 2) eq '01'}">
		<div class="leftGnb">
			<ul>
				<li class="left_tit ltit01_01"><em>거래처관리</em></li>
				<li><a href="${HOME}/customer/getCustomerList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '01' ? 'on':'off'}">거래처조회</a></li>
			<c:if test="${sBox.isSessionCustSearchGrnt eq 'true'}">
				<li><a href="${HOME}/customer/getCustCreditInfoConnection.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '02' ? 'on':'off'}">기업정보검색</a></li>
			</c:if>
				<%-- <li><a href="${HOME}/customer/getEWConnection.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '03' ? 'on':'off'}">조기경보</a></li> --%>
			</ul>
		</div>
		</c:when>
		<c:when test="${fn:substring(param.gnb, 0, 2) eq '02'}">
		<div class="leftGnb">
			<ul>
			<c:if test="${sBox.isSessionDebtGrnt eq 'true'}">
				<li class="left_tit ltit02_01"><em>채무불이행관리</em></li>
				<li><a href="${HOME}/debt/getDebtInquiryList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '01' ? 'on':'off'}">채무불이행신청서조회</a></li>
				<li><a href="${HOME}/debt/getDebtForm.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '02' ? 'on':'off'}">채무불이행신청서작성</a></li>
			</c:if>
			</ul>
		</div>
		</c:when>
		<c:when test="${fn:substring(param.gnb, 0, 2) eq '03'}">
		<div class="leftGnb">
			<ul>
				<li class="left_tit ltit03_01"><em>환경설정</em></li>
				<li><a href="${HOME}/config/getUserInfoList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '01' ? 'on':'off'}">사용자권한 및 회원정보</a></li>
				<%-- <li><a href="${HOME}/config/getLicenseList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '02' ? 'on':'off'}">라이선스관리</a></li>
				<li><a href="${HOME}/config/getXmlMessageList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '03' ? 'on':'off'}">XML메시지관리</a></li>
				<li><a href="${HOME}/config/getSettingFileList.do" class="lgnb ${fn:substring(param.gnb, 2, 4) eq '04' ? 'on':'off'}">설정파일관리</a></li> --%>
	
			</ul>
		</div>
		</c:when>
	</c:choose>
</div>
<!-- // 레프트 메뉴  끝 //-->	