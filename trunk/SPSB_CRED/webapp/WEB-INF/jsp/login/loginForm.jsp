<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf" %>
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="${JS}/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="${JS}/PIE.js"></script><!--ie7포함-borderRadius-->
	
<script type="text/javascript">
$(document).ready(function(){
	<%--local storage에서 로그인 정보 저장 여부 가져와서 설정--%>
	var rememberYn = window.localStorage["rememberSbDebnYn"];
	var rememberId = window.localStorage["rememberSbDebnId"];
	if(rememberYn == "Y"){
		$("#loginId").val(rememberId);
		$("#remYn").attr("checked", true);
	}
	
	<%--개발하기 위해 입력한 test id 설정(추후 지움)--%>
	/* $("#loginId").val("sbuser"); */
	/* $("#loginId").val("cert");
	$("#loginPw").val("cert1234");
	 */
	<%--로그인 아이디가 넘어온경우--%>
	if("${loginId}" != ""){
		$("#loginId").val("${loginId}");
		$("#loginPw").val("");
	}
	
	//$("#loginId").val("user123");
	//$("#loginPw").val("asdf1234");
	
	<%--개인회원 로그인 함수--%>
	$("#btn_loginUser").click(function(){
		if(!fnUserValidate()) {
			return false;
		}
		<%--local storage에서 로그인 정보 저장 --%>
		if($("input[id='remYn']").is(":checked")){
			localStorage[ "rememberSbDebnYn" ] = "Y";
			localStorage[ "rememberSbDebnId" ] = $("#loginId").val();
		}else{
			localStorage[ "rememberSbDebnYn" ] = "N";
			localStorage[ "rememberSbDebnId" ] = "";
		}
		$.ajax({
			url: "${HOME}/login/loginUserAjax.do",
			type: "post",
			dataType: "json",
			data:$("#loginUserForm").serialize(),
			async: false,
			error: function(result){
				alert("로그인 실패했습니다.");
			},
			success: function(data){
				if(data.model.result.isSuccess == true){
					location.href = "${HOME}/main.do";
					//location.href = "${HOME}/mypage/myPageMain.do";
				}else{
					alert(data.model.result.message);
				}
			}
		}); 
    });
	
	<%--아이디에서 enter클릭시 비밀번호로 focus--%>
	$("#loginId").keypress(function(event) {
		  if ( event.which == 13 ) {
			  $("#loginPw").focus();
		   }
	});
	<%--비빌번호에서 enter클릭시 개인회원 로그인 함수로 go--%>
	$("#loginPw").keypress(function(event) {
		  if ( event.which == 13 ) {
			  $("#btn_loginUser").click();
		   }
	});
	
	<%--개인회원 로그인 전송 파라미터 검증 함수--%>
	function fnUserValidate() {
		var loginId = $("#loginId");
		if( $.trim(loginId.val()).length == 0 ) {
			alert("아이디를 입력해주세요.");
			loginId.focus();
			return false;
		}
		var loginPw = $("#loginPw");
		if( $.trim(loginPw.val()).length == 0 ) {
			alert(" 비밀번호를 입력해주세요.");
			loginPw.focus();
			return false;
		}
		return true;
	}
});
</script>
</head>
<body>
<div id="Wrapper">
	<div class="container">
			<div id="headerwrap_login">
				<h1><em>스마트비즈 for Legacy의 온라인 서비스를 이용하시려면 로그인해 주세요.</em></h1>
			</div>
			<form id="loginUserForm" name="loginUserForm">
				<div id="loginwrap">
					<div class="login_info"><img src="${IMG}/common/login_obj.gif" /></div>
					<div class="login_form">
						<p class="login_input">
							<input type="text" id="loginId" name="loginId" placeholder="아이디" />
						</p>
						<p class="login_input">
							<input type="password" id="loginPw" name="loginPw" placeholder="비밀번호" />
						</p>
						<p class="txtstyle1"><input type="checkbox" id="remYn" name="remYn" value="Y"> 아이디 저장</p>
						
						<br>
						<p><input type="submit" class="login"  id="btn_loginUser"><em>로그인</em></p>
						<!--<p class="login_left"><a href="#">아이디 찾기</a>
						<img src="${IMG}/common/login_txt_bar.gif" />
						<a href="#">비밀번호 찾기</a></p>
						
						 <div class="login_right"><a href="#">회원가입</a></div> -->
					</div>
					
					<div id="footerWrap_login">
						<div class="login_copyright"> COPYRIGHT(C)2009 (주)비즈니스온커뮤니케이션.ALL RIGHTS RESERVED.</div>
					</div>
					
				</div>

			</form>			
			<!-- <div id="footerWrap_login">
				<div class="login_copyright"> COPYRIGHT(C)2009 (주)비즈니스온커뮤니케이션.ALL RIGHTS RESERVED.</div>
			</div> -->
		</div>
</div><!--Wrapper//-->	
</body>
</html>