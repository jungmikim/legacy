
 /**
  * <pre>
  *   공인인증서 검증 함수 : 모든 Condition 설정, 사용자가 인증서를 선택, 휴대폰인증서사용
  * </pre>
  * @author JUNG MI KIM
  * @since 2014. 04. 10
  * @param type : 검증 유형, data : 검증 값
  */

  CheckIDN = function(args){
 	var nRet;
 	var ssn = args;
 	// 모든 Condition 설정. ('POLICIES' 대신 'FIRST_COMP_CERT_POLICIES'을 입력하면 법인)
 	nRet = TSToolkit.SetConfig("test", CA_LDAP_INFO, CTL_INFO, POLICIES, 
 							INC_CERT_SIGN, INC_SIGN_TIME_SIGN, INC_CRL_SIGN, INC_CONTENT_SIGN,
 							USING_CRL_CHECK, USING_ARL_CHECK);
 	if (nRet > 0){
         alert(nRet + " : " + TSToolkit.GetErrorMessage());
         return false;
     }
 	
 	 // 인증서 로딩시 KM 인증서 검증 옵션 켜기
    TSToolkit.SetEachConfig("LOAD_CERT_KM", "TRUE");
    // 사용자가 자신의 인증서를 선택. 
    nRet = TSToolkit.SelectCertificate(STORAGE_TYPE, 0, "");
    if (nRet > 0) {
        alert(nRet + " : " + TSToolkit.GetErrorMessage());
        return false;
    }
    nRet = TSToolkit.VerifyVID(ssn);
    if (nRet > 0) {
        alert(nRet + " : " + TSToolkit.GetErrorMessage());
        return false;
    }
    if (TSToolkit.OutData != "true") {
        alert("사업자번호가 일치하지 않습니다.");
        return false;
    }
    
    nRet = TSToolkit.GetCertificate(CERT_TYPE_SIGN, DATA_TYPE_PEM);
    if (nRet > 0) {
        alert("GetCertificate : " + TSToolkit.GetErrorMessage());
        return false;
    }
    var cert = TSToolkit.OutData;
    
    nRet = TSToolkit.CertificateValidation(cert);
	
    if (nRet > 0)
	{
    	//alert(nRet + " : " + TSToolkit.GetErrorMessage());
    	if (nRet == 136){
			alert("인증서가 만료되었습니다.");
			return false;
		}
		if (nRet == 141){
			//var revokedTime = TSToolkit.OutData;
			alert("폐지 또는 효력이 정지되었습니다.");
			return false;
		}
	}
           
    TSToolkit.GetCertificatePropertyFromID(cert, CERT_ATTR_SUBJECT_DN); //DN
    var dn = TSToolkit.OutData;
    afterSucessCert(dn); //jsp단으로 DN값 가지고 복귀
 };
 
	 
	 