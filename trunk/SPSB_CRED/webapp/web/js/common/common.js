/**
 * <pre>
 *   Page move function
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 20
 * @param movePage : Move Page Number
 */
 getPage = function(movePage) {
	var form = $('form');
	form.attr({'method':'POST','action':form.find('#target').val()});
	form.find('#num').val(movePage);
	form.submit();
 };
 
 /**
  * <pre>
  *   Type 별 Data 검증
  * </pre>
  * @author Jong Pil Kim
  * @since 2013. 08. 27
  * @param type : 검증 유형, data : 검증 값
  */
 typeCheck = function(type, data){
 	
 	var result = false;
 	switch (type) {
 		case 'emailCheck'	:	
 			//var emailReg = /(\w+\.)*\w+\@(\w+\.)+\w+/;
 			var emailReg = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
 			if(!emailReg.test(data)){
 				result =  false;
 			}else{
 				result =  true;
 			}
 		break;
 		
 		case 'koreanStrCheck' :
 			var hangulReg = /[^a-zA-Z0-9]/;
 			if(hangulReg.test(data)){
 				result = false;	
 			}else {
 				result = true;	
 			}
 		break;
 	
 		case 'regNumCheck' :
 			weight = new Array(2,3,4,5,6,7,8,9,2,3,4,5);
 			totalsum = 0;	
 			parity = 0;
 			genderNumber = parseInt(data.charAt(6));
 			if(data.length != 13){
 				return false;
 			}
 			for(var i=0; i<data.length-1; i++){
 				totalsum = totalsum + (parseInt(data.charAt(i)) * weight[i]);	
 			}
 			if(totalsum > 0){
 				modTotal = (totalsum % 11);
 				
 				if(modTotal == 0) {
 					parity = 1;
 				} else if (modTotal == 1) {
 					parity = 0;
 				} else {
 					parity = 11 - modTotal;	
 				}
 				
 				if(parity == data.charAt(12)){
 					return true;
 				}else{
 					return false;
 				}
 			}else{
 				return false;
 			}
 		break;
 		
 		case 'businessNoCheck' :
 			var sum = 0;
 	        var buninessNo =new Array(10);
 	        var checkValue =new Array(1,3,7,1,3,7,1,3,5);
 	        for(var i=0; i<10; i++) { buninessNo[i] = data.substring(i, i+1); }
 	        for(var i=0; i<9; i++) { sum += buninessNo[i]*checkValue[i]; }
 	        sum = sum + parseInt((buninessNo[8]*5)/10);
 	        sidliy = sum % 10;
 	        sidchk = 0;
 	        if(sidliy != 0) { sidchk = 10 - sidliy; }
 	        else { sidchk = 0; }
 	        if(sidchk != buninessNo[9]) { return false; }
 	        return true;
 		break;
 		
 		case 'corpNoCheck' :
 			data = replaceAll(data,'-','');

 			if (data.length != 13){
 			   return false;
 			}

 			var corpNo  = data.split("");
 			var checkValue   = new Array(1,2,1,2,1,2,1,2,1,2,1,2);
 			var iSumBusinessNo  = 0;
 			var iCheckDigit = 0;

 			for (i = 0; i < 12; i++){
 			  iSumBusinessNo +=  eval(corpNo[i]) * eval(checkValue[i]);
 			}

 			iCheckDigit = 10 - (iSumBusinessNo % 10);

 			iCheckDigit = iCheckDigit % 10;

 			if (iCheckDigit != corpNo[12]){
 			  return false;
 			}
 			return true;
 		break;
 		
 		case 'numCheck' :
 			var numReg = /^[0-9]+$/;
 			if(!numReg.test(data)){
 				return false;
 			}
 			return true;
 		break;
 	}
 	return result;
 };
 
 /**
  * <pre>
  *   숫자 입력 체크 함수 Function
  * </pre>
  * @param obj : Check 할 Object
  *	     maxsize : 입력되는 숫자의 max범위
  */
 checkNumber = function(obj, maxsize) {
 	result = false;
 	if(/[^0-9]/g.test(obj.val())) {
 		alert("숫자만 입력가능합니다.");
 		obj.val('');
 		obj.focus();
 		result = true;
 	}
 	if(maxsize>0){
 		if(obj.val() > maxsize)	{
 				alert(maxsize + " 이내의 숫자만 입력가능합니다.");
 				obj.val('');
 		 		obj.focus();
 				result = true;
 		}
 	}
 	return result;
 };
 
 /**
  * <pre>
  *   문자열 BYTE CHECK 함수
  * </pre>
  * @param str : 검증값
  */
 getByteLength = function(str){
 	
 	var len = 0;
 	
 	if(str == null || str == ''){
 		return 0;
 	}
 	
 	for(var i=0; i<str.length; i++){
 		var ch = escape(str.charAt(i));
 		if(ch.length == 1){
 			len ++;
 		}else if(ch.indexOf("%u") != -1){
 			len += 2;
 		}else if(ch.indexOf("%") != -1){
 			len += ch.length / 3;
 		}		
 	}
 	
 	return len;
 	
 };
 
 /**
  * <pre>
  *   문자열 BYTE CHECK 후 검증 로직 함수
  * </pre>
  * @param str : 검증값, maxbyte : 최대 byte 수
  */
 getMaxByteValidate = function(obj, maxbyte){
 	
 	var len = 0;
 	
 	if(obj.val() == null || obj.val() == ''){
 		return 0;
 	}
 	
 	for(var i=0; i<obj.val().length; i++){
 		var ch = escape(obj.val().charAt(i));
 		if(ch.length == 1){
 			len ++;
 		}else if(ch.indexOf("%u") != -1){
 			len += 2;
 		}else if(ch.indexOf("%") != -1){
 			len += ch.length / 3;
 		}		
 	}
 	
 	if(!(len<=maxbyte)){
 		alert(obj.attr('title') + ' 입력 값이 너무 깁니다.');
 		obj.focus();
 		return false;
 	} else {
 		return true;
 	}
 	
 };
 
 /**
  * 문자열 치환 함수
  * 자바의 replaceAll 치환 함수를 만듬.
  * 
  * @param input : 치환대상 문자열
  * @param search : target 문자열
  * @param replace : replace 문자열
  * @return 치환결과 문자열
  */
 replaceAll = function(input,search,replace){
 	while(input.indexOf(search) != -1){
 		input = input.replace(search,replace);
 	}
 	return input;
 };
 
 
 /**
 * PopUp Windows 함수 정의
 * @param url : 팝업 창 주소
 *        winName : 팝업 창 이름
 *        width : 팝업창 가로크기
 *        height : 팝업창 세로크기
 */
 winOpenPopup = function(url,winName,width,height,scrolling) {
 	
 	winLeft = screen.width/2 - width/2;
 	winTop = screen.height/2 - height/2;

 	attribute = "width=";
 	attribute += width;
 	attribute += ",height=";
 	attribute += height;
 	attribute += ",scrollbars=" + scrolling;
 	attribute += ",location=no";
 	attribute += ",menubar=no";
 	attribute += ",resizable=no";
 	attribute += ",status=no";
 	attribute += ",toolbar=no";
 	attribute += ",directories=no";
 	attribute += ",left=";
 	attribute += winLeft;
 	attribute += ",top=";
 	attribute += winTop;
 	
 	window.open(url,winName,attribute);
 };

 /**
  *	browser와 version
  *
  * @return {isIe : ie여부, ver : ie이면 version}
  */
checkIeBrowserVersion = function(){
 var ua = navigator.userAgent;
 var ver = "";
 var isIe = false;
	if(ua.indexOf('MSIE') > 0 || ua.indexOf('Trident') > 0){	//Explorer
		isIe = true;
		var verCheck = /(?:.*? rv:([\w.]+))?/.exec(ua)||[];
		var version = verCheck[1];
		if(version){
			ver = version.split(".");
			ver = ver[0];
		}else{
			var msie = ua.indexOf("MSIE");
			var comma = ua.indexOf(".", msie);
			ver = ua.substring(msie + 4, comma);
		}
	}
return {isIe: isIe, ver: ver};
};

 /**
 *	파일 확장자 걸러내기 함수
 *
 * @param fileName : 입력된 파일이름
 * @return extension : 파일 확장자명
 */
splitExtension = function(fileName){
	if(fileName.indexOf('.') != -1){
	 	fileName = fileName.split('.');
	 	extension = fileName[fileName.length-1];
	}else{
		extension = '';
	}
 	return extension;
 };
 
 /**
  *  첨부 파일 검증 함수
  *
  * @param uploadFile : 첨부된 파일
  * @return 검증 후 결과
  */
 checkUploadFile = function(uploadFile){
 	
	 var fileName = uploadFile.value;
		//alert("fileName:" + fileName);
		//alert("fileSize:" + $(this).files[0].fileSize);
		if($.trim(fileName) != ''){
			var extension = splitExtension(fileName);
			if(extension == ''){
				alert("[" + fileName + "] 이 올바르지 않는 파일명 입니다.");
				return false;
			}else{
				if(!checkFileExtension(extension)){
					return false;
				}else{
					var result = checkFileSize(uploadFile);
					if(!result){
						return false;
					}
				}
			}
		}
	return true;
 };
 
 /**
  *  파일 확장자 검증 함수
  *
  * @param extension 확장자
  * @return 확장자 체크 후 결과
  */
 checkFileExtension = function(extension){
 	
	var result= true;
	if( $.trim(extension) != "") {
		var fileReg =/(txt)|(xls)|(xlsx)|(doc)|(docx)|(ppt)|(pptx)|(pps)|(ppsx)|(hwp)|(pdf)|(gif)|(jpg)|(png)|(bmp)|(jpeg)|(zip)$/;
		extension = extension.toLowerCase();
		if(!fileReg.test(extension)){
			alert(extension + "는(은) 올바르지 않은 파일형식입니다.");
			result= false;	
		}
	}else{
		result= false;	
	}
	return result;
 };

 /**
  *  파일 용량 검증 함수
  *
  * @param fileObj : 파일 Object
  * @return 체크 후 결과
  */
 checkFileSize = function(fileObj){
	var ieBrowser = checkIeBrowserVersion();
	var fileSize = 0;
	try{
		//for IE
		if(ieBrowser.isIe){
			var ver = ieBrowser.ver;
			//alert("ver: " + ver);
			//alert("ActiveXObject: " + window.ActiveXObject);
//			var fileName = fileObj.value;
			if(ver == 11 || ver == 10){
				if(fileObj != null && fileObj.files != undefined){
					if(fileObj.files[0] != null && fileObj.files[0] != ""){
						fileSize = fileObj.files[0].size;
					}
				}
			}else if(ver == 9){//TODO 해결필요
				/* var sizeINIE = User_book.getFileSize(fileName);
				if(sizeINIE != null){
					alert(sizeINIE.value);
				} */
			}else if(ver == 7 || ver == 8){//TODO 해결필요
				/* var objFSO = new ActiveXObject("Scripting.FileSystemObject"); 
				var objFile = objFSO.getFile(fileName);
				fileSize = objFile.size; //size in kb */
			}else if(ver <= 6){//TODO 해결필요
				/* var file = new Image();
				file.src = document.getElementById(fileObj.id).value;
				fileSize = file.fileSize; */
			}
		}else{
			if(fileObj != null && fileObj.files[0] != null && fileObj.files[0] != ""){
				fileSize = fileObj.files[0].size;
			}
		}
		
		if(fileSize != null && fileSize > 0){
			//alert("size:" + size);
			//alert("fileSize: " + fileSize);
			fileSize = fileSize / 1048576; //size in mb
			if(fileSize >= 20){
				alert("파일업로드는 20MB 이상 파일을 업로드 할 수 없습니다.");
				return false;
			}
		}
		//alert("Uploaded File Size is " + fileSize + "MB");
		
	}catch (e){
		alert("Error is: " + e);
	}
	return true;
 };
 
 /**
  * UTF-8 문자열을 Decode 함.
  * 
  * @param msg Decode 할 대상 문자열
  * @return Decode 후 문자열
  */
 utfDecode = function(msg){
 	deCodeMsg = "";
 	
 	if(msg != null && msg != ""){
 		deCodeMsg = decodeURIComponent(msg);
 		deCodeMsg = replaceAll(deCodeMsg,"+"," ");

 	}else{
 		deCodeMsg = "";
 	}
 	
 	return deCodeMsg;
 };
 
 
 /**
  * <pre>
  *   숫자 ,를 추가하는 함수
  * </pre>
  * @param obj : Check 할 Object
  *	     maxsize : 입력되는 숫자의 max범위
  */
addComma  = function(number) {
	var reg = /(^[+-]?\d+)(\d{3})/;
    var str = "" + number;
    while (reg.test(str)) {
	   str = str.replace(reg, '$1' + ',' + '$2');
    }
 	return str;
 };
 
//우편번호 검색 팝업창
 function open_win (url){ 
	w = 550;    //팝업창의 너비
	h = 550;    //팝업창의 높이
	//중앙위치 구해오기
	LeftPosition=(screen.width-w)/2;
	TopPosition=(screen.height-h)/2;
	var popUp = window.open(url,'zipSearchPopup', "width="+w+",height="+h+",top="+TopPosition+",left="+LeftPosition+", scrollbars=no, resizable=no");
	    popUp.focus();
 }
	
//우편번호 값 가져오기
 function resultReceive (zip, address){
	 $("form").find('#post_no1').val(zip.substring(0,3));
	 $("form").find('#post_no2').val(zip.substring(3,6));
	 $("form").find('#cust_addr_1').val(address);
	 $("form").find('#cust_addr_2').val("");
 }
 
// 날짜에 일수 더하기/빼기
 function dateAdd(sDate, nDays) {
    var yy = parseInt(sDate.substr(0, 4), 10);
    var mm = parseInt(sDate.substr(5, 2), 10);
    var dd = parseInt(sDate.substr(8), 10);
 
    d = new Date(yy, mm - 1, dd + nDays);
 
    yy = d.getFullYear();
    mm = d.getMonth() + 1; mm = (mm < 10) ? '0' + mm : mm;
    dd = d.getDate(); dd = (dd < 10) ? '0' + dd : dd;
 
    return '' + yy + '-' +  mm  + '-' + dd;
}