
var isSuccessCert = false;	//기업인증서 등록성공여부
 /**
  * <pre>
  *   공인인증서 검증 함수 : 모든 Condition 설정, 사용자가 인증서를 선택, 휴대폰인증서사용
  * </pre>
  * @author Jung mi Kim
  * @since 2014. 04. 10
  * @param type : 검증 유형, data : 검증 값
  */

  CheckIDN = function(args){
	  
 	isSuccessCert = false;
 	var nRet;
 	var ssn = args;
 	// 모든 Condition 설정. ('POLICIES' 대신 'FIRST_COMP_CERT_POLICIES'을 입력하면 법인)
 	nRet = TSToolkit.SetConfig("test", CA_LDAP_INFO, CTL_INFO, POLICIES, 
 							INC_CERT_SIGN, INC_SIGN_TIME_SIGN, INC_CRL_SIGN, INC_CONTENT_SIGN,
 							USING_CRL_CHECK, USING_ARL_CHECK);
 	nRet = TSToolkit.SetPolicyNameMap(COMMON_CERT_POLICIES);
 	if (nRet > 0){
         alert(nRet + " : " + TSToolkit.GetErrorMessage());
         return false;
     }
 	
 	// 사용자가 자신의 인증서를 선택.
 	nRet = TSToolkit.SelectCertificate(STORAGE_TYPE, 0, "", SelectCerficateResultIDN, [ssn]);
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 	
 	nRet = TSToolkit.SetEachConfig("CHANNELNAME", "BOKJIRO");
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 	nRet = TSToolkit.SetEachConfig("CERT_COMPANY", "KTNET");
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 	nRet = TSToolkit.SetEachConfig("VIRTUALKEY_COMPANY", "NULL");
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 	nRet = TSToolkit.SetEachConfig("BROWSER_KEYTYPE", "NULL");
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 };
 
 /**
  * <pre>
  *  사용자가 인증서를 선택 : 인증서 꺼내기 후 비교
  * </pre>
  * @author Jung mi Kim
  * @since 2014. 04. 10
  * @param args : 검증값
  */
 SelectCerficateResultIDN = function (args){
	if(args.length<=0)
 	return false;
 	
 	var ssn = args[0]; 
 	var signcert, random;

 	// 인증서 정보 추출 (인증서 관리시 인증서 등록및 수정에서 사용
    var nRet = TSToolkit.GetCertificate(CERT_TYPE_SIGN, DATA_TYPE_PEM);
 	if (nRet > 0){
 		alert(nRet + " : " + TSToolkit.GetErrorMessage());
 		return false;
 	}
 	signcert = TSToolkit.OutData;
 	
 	nRet = TSToolkit.CertificateValidation(signcert);
 	if (nRet > 0){
		//alert(nRet + " : " + TSToolkit.GetErrorMessage());
		if (nRet == 136){
			alert("인증서가 만료되었습니다.");
		}
		if (nRet == 141){
			//var revokedTime = TSToolkit.OutData;
			alert("폐지 또는 효력이 정지되었습니다.");
		}
		return false;
	}
	
 	nRet = TSToolkit.GetRandomInKey();
	if (nRet > 0){
		alert(nRet + " : " + TSToolkit.GetErrorMessage());
		return false;
	}
	random = TSToolkit.OutData;

	nRet = TSToolkit.VerifyVID2(signcert, ssn, random);
	if (nRet > 0){
		alert(nRet + " : " + TSToolkit.GetErrorMessage());
		return false;
	}
	 
	if (TSToolkit.OutData != "true"){
		alert("사업자번호가 일치하지 않습니다.");
		return false;
	}
 	
 	TSToolkit.GetCertificatePropertyFromID(signcert, CERT_ATTR_SUBJECT_DN);
 	var certInfo = TSToolkit.OutData;
 	if(certInfo.length > 0){
 		isSuccessCert = true;
 		afterSucessCert(isSuccessCert,certInfo);
 		
 	}
 };
 
 
	 
	 