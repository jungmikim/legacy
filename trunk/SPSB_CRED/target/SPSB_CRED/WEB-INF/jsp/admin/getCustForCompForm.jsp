<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="compMngList" value="${result.compMngList}" />
<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%>
<html class="no-js" lang="ko">
<%--<![endif]--%>
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtForm.css">
<%-- <link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css"> --%>

<style type="text/css">
.browsing {
	font-weight: bold;
}
a{cursor:pointer;}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript"
	src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		/**
		* 연동업체 별 사업장등록 
		* @author HWAJUNG SON
		*/
		$("#btn_comp_usr_regist").click(function(){
			

			$("input, textarea").each(function(){
				   $(this).val(jQuery.trim($(this).val()));
			});
			if ($.fn.validate($('#apply_company_info'))) {			
				if(confirm("사업장을 등록하시겠습니까?")){
		
					$.ajax({
						type : "POST", 
						url : "${HOME}/admin/setCustForComp.do",
						dataType : "json",
						data:$("#form_comp_usr").serialize(),
						async : false,
						success : function(msg) {
									if(msg.REPL_CD == '00000'){
										alert("등록이 성공하였습니다.");
									}else{
										alert("등록이 실패하였습니다.");
									}
									location.href="${HOME}/admin/getCustForCompForm.do";
								},
						error : function(xmlHttpRequest, textStatus, errorThrown){
									alert("연동업체 등록 에러발생 [" + textStatus + "]");
								}
					});
				}
			
			}
		});
		/**
	     * <pre>
	     *	사업자등록번호 붙여넣기 할 때 '-' 삭제하기
	     * </pre>
	     * @author 백원태
	     * @since 2014. 12. 15
	     */
	     $('#usr_no').change(function(){
	    	 $('#usr_no').attr('value', $('#usr_no').attr('value').replace(/-/g, '')); 
	     });
		
		/**
		 * <pre>
		 * 전송 파라미터 검증 함수
		 * </pre>
		 * @author HWAJUNG SON
		 */
		 $.fn.validate = function($validationArea){
			
			$result = true;
			 
			$formEl = $validationArea.find('input');
			$.each($formEl, function(){
				
				// 필수 공백 검증
				if($(this).hasClass('required')){
					if($.trim($(this).val())=='') {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
						return $result; 
					}
				}
				
				if($.trim($(this).val()) != '') {

					// 숫자 검증
					if($(this).hasClass('numeric')){
						if(!typeCheck('numCheck',$.trim($(this).val().replace(/-/gi, '')))) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
							return $result;
						}
						
						if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
							$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
							return $result;
						}
					}
				}
				// 반복문 정지
				if(!$result) return $result;
			});
			
			// 스크립트 정지
			if(!$result) return $result;
		 
			return $result;
			
		 };
		 
			/**
			 * <pre>
			 * 폼 전송 파라미터 검증 중 경고창 함수
			 * </pre>
			 * @author HWAJUNG SON
			 */
			$.fn.validateResultAndAlert = function($el, $message) {
				alert($message);
				$el.focus();
				return false;
			};
	});
</script>
</head>
<style>
.admin_bar {
	 padding:10px 18px 18px 20px; font-weight:bold; color:#4c91ff; height:2px;
}
.wart1 {padding: 5px 0 0px 0;line-height:1.4em;font-size:11px;}
}
</style>
<body>

	<!-- contentArea -->
	<div id="containerWrap" style="padding-top:60px;">
		<!--  left -->
		<div id="leftWrap">
		<div class="leftGnb">
				<ul>
					<li class="left_tit ltit_admin_02"><em>관리자설정</em></li>
					<li><a href="${HOME}/admin/getCompAndPrdInfoForm.do" class="lgnb off">연동업체 및 상품 등록</a></li>
					<li><a href="${HOME}/admin/getCustForCompForm.do" class="lgnb on">업체별 사업장정보 등록</a></li>
					<li><a href="${HOME}/admin/getMbrForm.do" class="lgnb off">회원정보 등록</a></li>
					<li><a href="${HOME}/admin/getGrnInfoForm.do" class="lgnb off">권한 등록</a></li>
				</ul>
			</div>
		</div>
		<div id="rightWrap">
			<%-- menu별 이미지 class sub00 --%>
			<%-- HIDDEN AREA END --%>
			<div class="right_tit ltit_admin_01">
				<em>관리자페이지</em>
			</div>
			<p class="content_tit">연동업체별 사업장관리 등록</p>
			<div id="defaultApplyWrap">
				<form id="form_comp_usr" autocomplete="off" enctype="multipart/form-data" method="post">
					<div class="default_Apply">
						<ul id="menu_slide">
							<li class="Apply_slide"><span class="slideSpan">
							<a class="admin_bar" id="" style="text-decoration: none;">연동업체 별 사업장관리 등록</a></span>
								<ul>
									<%-- STEP01 시작 --%>
									<li class="Apply_sub">
										<div id="step1" class="ApplysubWrap">
											<c:set var="companyInfoBox" value="${result.companyInfoBox}" />
											<p class="content_dot">업체별 사업장관리 정보</p>
											<table id="apply_company_info" class="defaultTbl01_01" summary="연동업체 등록 테이블">
												<caption>회사관리코드</caption>
												<colgroup>
													<col width="18%">
													<col width="32%">
												</colgroup>
												<tbody>
													<tr>
														<td class="title">회사관리코드<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<select style="border:inherit;"id="selectedComp" name="selectedComp" title="연동업체">
																<c:choose>
																	<c:when test="${result.compMngCount eq 0 }">
																		<option>없음</option>
																	</c:when>
																	<c:otherwise>
																		<c:forEach var="compList" items="${result.compMngList}">
																	<option value="${compList.COMP_MNG_CD}" >${compList.COMP_MNG_CD}</option>
																</c:forEach>
																	</c:otherwise>
																</c:choose>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">회사명<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<input type="text" id="comp_mng_nm" name="comp_mng_nm" class="tblInput required" style="width: 196px;" title="회사명" value="" maxlength="100">
														</td>
													</tr>
													<tr>
														<td class="title">사업자번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="usr_no" name="usr_no" class="tblInput numeric" style="width:196px;" title="사업자번호" value="" maxlength="10">
														</td>
													</tr>
													<tr>
														<td class="title">이메일주소</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="email" name="email" class="tblInput" style="width: 196px;" title="이메일주소" value="" maxlength="100">
															<span class="em_po">ex: admin@businesson.co.kr</span>
														</td>
													</tr>
													<tr>
														<td class="title">전화번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="tel_no" name="tel_no" class="tblInput" style="width:196px;" title="전화번호" value="" maxlength="20">
															<span class="em_po">ex: 02-1234-5678</span>
														</td>
													</tr>
													<tr>
														<td class="title">휴대폰번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="mb_no" name="mb_no" class="tblInput" style="width: 196px;" title="휴대폰번호" value="" maxlength="20">
															<span class="em_po">ex: 010-1234-5678</span>
														</td>
													</tr>
													<tr>
														<td class="title">직인정보</td>
														<td class="default_note2" colspan="3">
															<textarea name="sign_info" style="width:200px;" class="sign_info" maxlength="200" id="sign_info"  title="직인정보" ></textarea>
														</td>
													</tr>
													<tr>
														<td class="title">인증서정보</td>
														<td class="default_note2" colspan="3">
															<textarea name="cert_info" style="width:200px;" class="cert_info" maxlength="200" id="cert_info"  title="인증서정보" ></textarea>
														</td>
													</tr>
													<tr>
														<td class="title">사용자유형</td>
														<td class="default_note2" colspan="3">
															<select name="usr_type" style="border:inherit;" id="usr_type">
																<option value="0">해당사항없음(Default)</option>
																<option value="1">SAP</option>
																<option value="2">Oracle</option>
																<option value="3">Legacy</option>
															</select>
														</td>
													</tr>
													<tr>
														<td class="title">우편번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="post_no" name="post_no" class="tblInput numeric" style="width: 80px;" title="우편번호" value="" maxlength="5">
														</td>
													</tr>
													<tr>
														<td class="title">주소</td>
														<td class="default_note2" colspan="3">
															<div style="padding-bottom:4px"><input type="text" id="addr_1" name="addr_1" class="tblInput" style="width: 300px;" title="주소1" value="" ></div>
															<div><input type="text" id="addr_2" name="addr_2" class="tblInput" style="width: 300px;" title="주소2" value="" ></div>															
														</td>
													</tr>
													<tr>
														<td class="title">대표자명</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="own_nm" name="own_nm" class="tblInput" style="width: 196px;" title="대표자명" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">법인등록번호</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="corp_no" name="corp_no" class="tblInput numeric" style="width: 196px;" title="법인등록번호" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">업종</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="biz_type" name="biz_type" class="tblInput" style="width: 196px;" title="업종" value="" maxlength="20">
														</td>
													</tr>
													<tr>
														<td class="title">업태</td>
														<td class="default_note2" colspan="3">
															<input type="text" id="biz_cond" name="biz_cond" class="tblInput" style="width: 196px;" title="업태" value="" maxlength="20">
														</td>
													</tr>
												</tbody>
											</table>
											<div class="wart1">
											<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
											</div>
											<div class="ApplyBtn_Add" style="padding-bottom:12px;">
												<div class="ApplyBtn_Step">
													<a id="btn_comp_usr_regist" style="float:right;">사업장 등록</a>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<%--  footer Area Start --%>
		<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
		<%-- footer Area End --%>
		</div>
</body>
</html>