<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행신청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="compMngList" value="${result.compMngList}" />
<!doctype html>
<%--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]--%>
<%--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]--%>
<%--[if (gt IE 9)|!(IE)]><%--%>
<html class="no-js" lang="ko">
<%--<![endif]--%>
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtForm.css">
<%-- <link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css"> --%>

<style type="text/css">
.browsing {
	font-weight: bold;
}
</style>

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript"
	src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		/**
		* 연동업체 등록
		*/
		$("#btn_comp_regist").click(function(){
			if(confirm("연동업체를 등록하시겠습니까?")){
				
				$param = {
						comp_mng_cd : $('#comp_mng_cd').val(),
						service_code : $('#service_code').val(),
						comp_mng_nm : $('#comp_mng_nm').val()
				 };
				
				$.ajax({
					type : "POST", 
					url : "${HOME}/admin/setCompUser.do",
					dataType : "json",
					data : $.param($param),
					async : false,
					success : function(msg) {
								if(msg.REPL_CD == '00000'){
									alert("등록이 성공하였습니다.");
								}else{
									alert("등록이 실패하였습니다.");
								}
								
							},
					error : function(xmlHttpRequest, textStatus, errorThrown){
								alert("거래처 미수기술료 조회 도중 에러발생 [" + textStatus + "]");
							}
				});
			}
		});
		
		/**
		* 연동업체 상품 중복체크
		*/
		$(".prdCd").click(function(){
			
			if($(this).val() == 'K'){
				if(($("#prdList").val()).indexOf("W") != -1 ){
					alert("같은 서비스를 중복해서 등록하실 수 없습니다.");
					return false;
				}
			}
			
			if($(this).val() == 'W'){
				if(($("#prdList").val()).indexOf("K") != -1){
					alert("같은 서비스를 중복해서 등록하실 수 없습니다.");
					return false;
				}
			}
			
			$prdList = "";
			$parent = $(this).parent();
			if($parent.find('input:checked').length > 0){
				$parent.find('input:checked').each(function(idx){
					$prdList += $(this).val();
					if(idx+1 != $parent.find('input:checked').length){
						$prdList += ',';
					}
				});
			}
			$("#prdList").val($prdList);
		});
	});
</script>
</head>
<style>
.admin_bar {
	 padding:10px 18px 18px 20px; font-weight:bold; color:#4c91ff; height:2px;
}
.wart1 {padding: 5px 0 0px 0;line-height:1.4em;font-size:11px;}
}
</style>
<body>

	<!-- contentArea -->
	<div id="containerWrap" style="padding-top:60px;">
		<!--  left -->
		<div id="leftWrap">
		<div class="leftGnb">
				<ul>
					<li class="left_tit ltit_admin_02"><em>관리자설정</em></li>
					<li><a href="${HOME}/admin/setCompAndPrdInfo.do" class="lgnb on">연동업체 및 상품 등록</a></li>
					<li><a href="${HOME}/admin/setCustForComp.do" class="lgnb off">업체별 사업장관리 등록</a></li>
					<li><a href="${HOME}/admin/setGrnInfo.do" class="lgnb off">권한생성</a></li>
					<li><a href="${HOME}/admin/set.do" class="lgnb off">관리자생성</a></li>
				</ul>
			</div>
		</div>
		<div id="rightWrap">
			<%-- menu별 이미지 class sub00 --%>

			<%-- 접속한 유저의 사업자 번호 --%>
			<input type="hidden" id="usrNo" value="${sBox.sessionUsrNo}" />

			<%-- HIDDEN AREA END --%>
			<div class="right_tit ltit_admin_01">
				<em>관리자페이지</em>
			</div>
			<p class="content_tit">연동업체 및 상품 등록</p>
			<div id="defaultApplyWrap">
				<form autocomplete="off" enctype="multipart/form-data" method="post">
				<input type="hidden" name="prdList" id="prdList" value=""/>
					<div class="default_Apply">
						<ul id="menu_slide">
							<li class="Apply_slide"><span class="slideSpan">
							<a class="admin_bar" id="" style="text-decoration: none;">연동업체 등록</a></span>
								<ul>
									<%-- STEP01 시작 --%>
									<li class="Apply_sub">
										<div id="step1" class="ApplysubWrap">
											<c:set var="companyInfoBox" value="${result.companyInfoBox}" />
											<p class="content_dot">연동업체 정보</p>
											<table id="apply_company_info" class="defaultTbl01_01" summary="연동업체 등록 테이블">
												<caption>회사정보</caption>
												<colgroup>
													<col width="18%">
													<col width="32%">
												</colgroup>
												<tbody>
													<tr>
														<td class="title">회사관리코드<span class="em_po">*</span></td>
														<td>
															<input type="text" id="comp_mng_cd" name="comp_mng_cd" class="tblInput required" style="width:196px;" title="회사관리코드" value="" maxlength="70">
														</td>
														<td class="title">서비스코드<span class="em_po">*</span></td>
														<td>
															<input type="text"  readonly="readonly" id="service_code" name="service_code" class="tblInput"  title="사업자번호" value="CRED">
														</td>
													</tr>
													<tr>
														<td class="title">회사명<span class="em_po">*</span></td>
														<td class="default_note2" colspan="3">
															<input type="text" id="comp_mng_nm" name="comp_mng_nm" class="tblInput required" style="width: 196px;" title="회사명" value="" maxlength="100">
														</td>
													</tr>
												</tbody>
											</table>
											<div class="wart1">
											<span class="warn_ico1">ㆍ</span> <span class="em_po">*</span> 항목은 필수 입력 사항입니다.<br>
											</div>
											<div class="ApplyBtn_Add" style="padding-bottom:12px;">
												<div class="ApplyBtn_Step">
													<a id="btn_comp_regist" style="float:right;">연동업체 등록</a>
												</div>
											</div>
										</div>
									</li>
								</ul></li>
							<p class="line"></p>
							
							<li class="Apply_slide">
							<span><a id="btn_step_two_title" class="admin_bar" style="text-decoration: none;">연동업체 상품등록</a></span>
								<ul>
									<li class="Apply_sub">
										<div id="step2" class="ApplysubWrap">
											<table id="deptor_company_info" class="defaultTbl01_01" summary="연동업체 상품등록">
												<caption>회사정보</caption>
												<colgroup>
													<col width="18%">
													<col width="32%">
													<col width="18%">
													<col width="32%">
												</colgroup>
												<tbody>
													<tr>
														<td class="title">연동업체<span class="em_po">*</span></td>
														<td colspan="3">
															<select style="border:inherit;"id="selectedComp" name="selectedComp" title="연동업체">
																<c:forEach var="compList" items="${result.compMngList}">
																	<option value="${compList.COMP_MNG_CD}" >${compList.COMP_MNG_CD}</option>
																</c:forEach>
															</select> 
														</td>
													</tr>
													<tr>
														<td class="title">상품<span class="em_po">*</span></td>
														<td colspan="3">
															기업정보검색(C) : <input type="checkbox" size="20" title="기업정보검색" value="C" class="prdCd"><br>
		    												채무불이행 수수료(D) : <input type="checkbox"  size="20" title="채무불이행" value="D" class="prdCd"><br>
		    												조기경보 서비스(W) : <input type="checkbox" size="20" title="조기경보" value="W" class="prdCd"><br>
		    												조기경보+K-Link 서비스(K) : <input type="checkbox" size="20" title="K-Link" value="K" class="prdCd"><br>
														</td>
													</tr>
												</tbody>
											</table>
											<p class="DotLine"></p>
											<div class="ApplyBtn_Add" class="ApplyBtn_Add" style="padding-bottom:12px;">
												<div class="ApplyBtn_Step">
													<a id="btn_product" class="step2_info" style="float:right;">상품등록</a>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<%--  footer Area Start --%>
		<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
		<%-- footer Area End --%>
		</div>
</body>
</html>