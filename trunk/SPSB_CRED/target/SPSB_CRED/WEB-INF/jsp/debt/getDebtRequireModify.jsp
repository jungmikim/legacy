<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 채무불이행 Master Table 정보 --%>
<c:set var="debt" value="${result.debt}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<%@ include file="../common/meta.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>스마트비즈 for Legacy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtRequireModify.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>

<%-- <link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css"> --%>
<script type="text/javascript" src="${JS}/common/certX.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitConfig.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitObject.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	jQuery.support.cors = true;
	
	
	/*
	* <pre>
	*   정정요청 버튼 클릭 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('#btn_require_modify').live({
		click : function() {
			// 정정 요청 검증 함수 호출
			if (!$.fn.validateRequireModify()) {
				return false;
			}
			
			if(confirm('채무불이행 정정요청을 하시겠습니까?')) {
				// 인증서 출력 호출
				afterSucessCert();
				/* CheckIDN('${sBox.sessionUsrNo}'); */
			} 
		}
	});
	
	/*
	* <pre>
	*   취소 버튼 클릭 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('#btn_cancel').live({
		click : function() {
			self.close();
		}
	});
	
	/*
	* <pre>
	*   정정 요청 금액 콤마 추가 / 삭제 함수
	* </pre>
	* @author Jong Pil Kim
	* @since 2014. 05. 10.
	*/
	$('.modifyDebtAmt').live({
		focus: function(e) {
			$(this).val(replaceAll($(this).val(), ',', ''));
		},
		focusout: function(e) {
			
			// 콤마 추가 
			$(this).val(addComma($(this).val()));
		}
	});
	
});	

<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%>

/**
 * <pre>
 *   경고창 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
 */
 $.fn.validateResultAndAlert = function($el, $message){
	alert($message);
	<%-- 파이어폭스 FOCUS() 버그때문에 이렇게 사용함. $(this).focus() 안먹힘 --%>
	setTimeout(function() {
		$el.focus();
	}, 0);
	return false;
 }; 

/*
* <pre>
*   정정 요청 검증 함수
* </pre>
* @author Jong Pil Kim
* @since 2014. 05. 10.
*/
$.fn.validateRequireModify = function() {
	
	$modifyDebtAmt = $.trim(replaceAll($('#modify_debt_amt').val(), ',',''));
	$priorDebtAmt = $.trim(replaceAll($('#prior_debt_amt').val(), ',',''));
	
	<%-- 필수 검증 --%>
	if($modifyDebtAmt == '') {
		return $.fn.validateResultAndAlert($('#modify_debt_amt'), '정정요청금액을 입력해주세요.');
	}
	
	<%-- 숫자 검증 --%>
	if($modifyDebtAmt.match(/[^0-9]+/)) {
		return $.fn.validateResultAndAlert($('#modify_debt_amt'), '정정요청금액은 숫자만 입력가능합니다.');
	}
	
	<%-- 정정요청금액 최소금액 검증 --%>
	if(parseInt($modifyDebtAmt, 10) < 50000) {
		return $.fn.validateResultAndAlert($('#modify_debt_amt'), '정정요청금액은 50,000원 보다 커야합니다.');
	}
	
	<%-- 정정요청금액 동일한 금액 검증 --%>
	if(parseInt($modifyDebtAmt, 10) == parseInt($priorDebtAmt,10)) {
		return $.fn.validateResultAndAlert($('#modify_debt_amt'), '정정요청금액이 현재 채무불이행 금액과 동일할 수 없습니다.');
	}
	
	<%-- 정정요청금액 최대금액 검증 --%>
	if(parseInt($modifyDebtAmt, 10) > parseInt($priorDebtAmt, 10)) {
		return $.fn.validateResultAndAlert($('#modify_debt_amt'), '정정요청금액은 ' + (addComma($priorDebtAmt)) + '원 보다 작아야 합니다.');
	}
	
	return true;
};

<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>



<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>

/**
 * <pre> 
 *   정정요청 Ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2014. 05. 10
 * @param debtApplId : 채무불이행 순번, debtAmt : 정정 요청 금액, modStat : 정정요청상태[R,C]
 *		  
 */
 $.fn.modifyDebtRequire = function() {

  	 $paramObj = {
    	debtApplId : $('#debt_appl_id').val(),
    	debtAmt : replaceAll($('#modify_debt_amt').val(), ',', ''),
    	sessionSbLoginId : $('#sb_Login_id').val(),
    	rmkTxt : $('#rmk_txt').val(),
    	modStat : 'R'
  	 };
  	 
   	 $param = $.param($paramObj);
   	 
	 $.ajax({
			type : "POST", 
			url : "${HOME}/debt/modifyDebtRequire.do",
			dataType : "json",
			data : $param,
		    success :  function(msg) {
			    
		    	$result = msg.model.result;
		    	$replCd = $result.REPL_CD;
			    $replMsg = $result.REPL_MSG;
			    
			    if('00000' == $replCd){
			    	alert('정정요청이 완료되었습니다.');
			    	$(opener.location).attr("href", "javascript:$.fn.callBackDebtProcessStatChange('RequireRelease');");
			    	self.close();
			    } else {
			    	alert(utfDecode($replMsg));
			    }
		    },
		    error : function(xmlHttpRequest, textStatus, errorThrown){
		    	 alert("정정요청 중 오류가 발생하였습니다. [" + textStatus + "]");
		    }
		});
 };

<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>


/**
* <pre>
* 인증서 등록 성공 CallBack 함수
* </pre>
* @author sungrangkong
* @since 2014. 05. 10.

function afterSucessCert(certInfo){
	$.fn.modifyDebtRequire();
};*/

/**
* <pre>
* 정정요청 버튼 클릭시 실행
* </pre>
* @author sungrangkong
* @since 2014. 05. 10.
*/
function afterSucessCert(){
	$.fn.modifyDebtRequire();
};

</script>
</head>
<body>
<input type="hidden" id="sb_Login_id" name="sessionSbLoginId" value="${sBox.sessionSbLoginId}"/>
	<div id="defaultApplyWrap">
		<div class="ApplysubWrap">
			<p class="content_tit">채무금액 정정요청</p>
<!-- 			<h4 class="normh4">▶ 채무금액 정정요청</h4> -->
			<div class="comment_1" style="font-size: 12px;">채무자의 채무불이행 금액 정정을 요청합니다.<br/>정정하고자 하는 금액을 입력하세요.</div>
			<div class="ApplyBtn" style="padding-top:10px; padding-bottom:10px;">
				채무불이행 금액 수정 후에는 <font color="#ff0000" style="padding-left:2px;">수정이 불가</font>합니다.<br/>정확하게 확인하시고 정정 요청하세요.
			</div>
	           
			<%-- 채무불이행 정정요청 테이블 시작 --%>
			<form name="debtRequireModifyFrm" id="non_payment_require_modify_frm" autocomplete="off">
				
				<%-- INPUT HIDDEN AREA START --%>
				<input type="hidden" name="debtApplId" id="debt_appl_id" value="${sBox.debtApplId}" />
				<input type="hidden" name="custId" id="cust_id" value="${sBox.custId}" />
				<%-- INPUT HIDDEN AREA END --%>			
			
				<table class="defaultTbl01_01" summary="채무불이행 정정 요청 테이블">
					<caption>채무불이행 정정요청</caption>
					<colgroup>
						<col width="45%">
						<col width="55%">						
					</colgroup>
					<tbody>			
						<tr>
							<td class="title">정정요청일</td>
							<td class="data-center">
								${result.currentDt}
							</td>
						</tr>
						<tr>
							<td class="title">현재 채무불이행 금액</td>
							<td class="data-right">
								<c:choose>
									<c:when test="${debt.DEBT_AMT ne null and debt.DEBT_AMT ne '' }">
										${debt.DEBT_AMT }원
										<input type="hidden" name="priorDebtAmt" id="prior_debt_amt" value="${fn:replace(debt.DEBT_AMT, ',','')}" />	
									</c:when>
									<c:otherwise>
										${debt.ST_DEBT_AMT }원
										<input type="hidden" name="priorDebtAmt" id="prior_debt_amt" value="${fn:replace(debt.ST_DEBT_AMT, ',','')}" />
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<td class="title">정정요청금액</td>
							<td class="default_note">
								<input type="text" style="width:158px;" class="default_txtInput numericMoney modifyDebtAmt required" id="modify_debt_amt" value="" maxLength="20" placeholder="50,000원 이상 입력" title="정정요청금액"/>원
							</td>
						</tr>
						<tr>
							<td class="title">정정사유</td>
							<td>
								<input type="text" class="fullTxt rmkTxt" name="rmkTxt" id="rmk_txt" value="일부상환(부분변제)" />
							</td>
						</tr>
					</tbody>
				</table>
			</form>
			<%-- 채무불이행 정정요청 테이블 종료 --%>
			
		</div>
	</div>
	<br/><br/>
	<%-- 버튼 영역 시작 --%>
	<table class="tbl_numbtn">
	<tbody>
		<tr>
			<td class="right">
				<div id="btnWrap_l">
					<div class="btn_graybtn_01">
						<a href="#none" class="ApplyBtn_Type on" id="btn_require_modify">확인</a>
					</div>
				</div>
	
				<div id="btnWrap_l">
					<div class="btn_graybtn_01">
						<a href="#none" class="ApplyBtn_Type on" id="btn_cancel">취소</a>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
	</table>	
<!-- 	
	<div class="pBtnWrap">
		<div class="pBtnMiddle">
			<a class="ApplyBtn_Type" href="#none" id="btn_require_modify">정정요청</a>
			<a class="ApplyBtn_Type" href="#none" id="btn_cancel">취소</a>
		</div>
	</div> -->
	<%-- 버튼 영역 종료 --%>
	
</body>
</html>