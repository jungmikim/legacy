<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- 채무불이행 Master Table 정보 --%>
<c:set var="debtBox" value="${result.debtBox}" />

<%-- 채무불이행 사유관리 리스트 정보  --%>
<c:set var="histStatusList" value="${result.histStatusList}" />

<%-- 채무불이행 정정관리  리스트 정보  --%>
<c:set var="modHistList" value="${result.modHistList}" />

<%-- 채무불이행 미수채권 리스트 정보 --%>
<c:set var="debentureList" value="${result.debentureList}" />

<%-- 채무불이행 증빙서류 리스트 정보 --%>
<c:set var="prfFileList" value="${result.prfFileList}" />

<%-- 채무불이행 신청서 페이징 --%>
<c:set var="pcPage" value="${result.pcPage}"/>

<%--채무불이행 프로세스 상태값 --%>
<c:set var="debtProcess" value="${result.debtProcess}" />

<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../common/meta.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>스마트비즈 for Legacy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache" />

<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtOngoinInquiry.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		// STAT이 AU(신청불가), AC(신청취소), CR(해제완료), RU(등록불가), KF(심사과실발생) 일 때는 진행완료 접수문서 상세보기 페이지로 볼 수 있도록 이동
		if ('${debtBox.STAT}' == 'AU' || '${debtBox.STAT}' == 'AC' || '${debtBox.STAT}' == 'CR' || '${debtBox.STAT}' == 'RU' || '${debtBox.STAT}' == 'KF') {
			$url = '${HOME}/debt/getDebtCompleteInquiry.do?';
	    	$url += 'debtApplId='+$('#debt_appl_seq').val();
			$(location).attr('href',$url);
		}
		
		// STEP03 선택채권 건수 입력
		$('#selected_bond > p').text($('.debnList').find('tr:not(.noSearch)').length+"건");
		
		// STEP03 결과 없을 시 검색 결과 없음으로 출력
		if ($('.debnList').find('tbody > tr').length < 1) {
			$html = '<tr class="noSearch">';
			$html += '<td colspan = "4"><div align="center" style="padding-top: 5px; padding-bottom: 5px;">검색 결과가 없습니다.</div></td>';
			$html += '</tr>';
			$('.debnList').find('tbody').html($html);
		}
		
		// 채권이 열 개 이상일 시 height 조정 auto 풀기
		if ($('.debnList').find('tr').length >= 10) {
			$('.debnList').removeAttr('style');
		}
		
		// STEP04 맨 마지막 증빙자료 다음에는 콤마 삭제
		$('#prfArea').find('.default_note').each(function() {
			$(this).find('.comma').last().text('');
		});
		
		/*
		* <pre>
		* STEP TITLE CLICK EVENT
		* </pre>
		* @author 비즈온 디자인팀
		*/
		$('#menu_slide > li.Apply_slide > .slideSpan > a').click(function(){
			$checkElement = $(this).parent().next();
			if(($checkElement.is('ul')) && ($checkElement.is(':visible'))) {
				$checkElement.slideUp(300);
				$(this).attr("class", "down");
				return false;
			}
			if(($checkElement.is('ul')) && (!$checkElement.is(':visible'))) {
				$checkElement.slideDown(300);
				$(this).attr("class", "up");
				return false;
			}
		});
		
		/*
		* <pre>
		* 모두펼치기 버튼 클릭 EVENT
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
		$(document).on('click', '#open_all', function(){		
			$('#menu_slide > li.Apply_slide > ul').slideDown(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class", "up");
			
			$('#open_all').hide();
			$('#close_all').removeAttr('style');
		});
		
		/*
		* <pre>
		* 모두닫기 버튼 클릭 EVENT
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.14
		*/
		$(document).on('click', '#close_all', function(){	
			$('#menu_slide > li.Apply_slide > ul').slideUp(300);
			$('#menu_slide > li.Apply_slide > span > a').attr("class","down");
			
			$('#close_all').hide();
			$('#open_all').removeAttr('style');
		});
		
		/*
		* <pre>
		* 타이틀 클릭 EVENT
		* </pre>
		* @author YouKyung Hong
		8 @since 2014.05.14
		*/
		$('#menu_slide > li.Apply_slide > span > a').click(function() {
			
			if($(this).hasClass('down')) {
				$('#close_all').hide();
				$('#open_all').removeAttr('style');
			}else {
				if($('.down').length == 0){
					$('#open_all').hide();
					$('#close_all').removeAttr('style');
				}
			}
			
		});
		
		/*
		* <pre>
		* 인쇄 미리보기 페이지 띄우는 EVENT
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
		$('#btn_print').click(function() {
			if( navigator.userAgent.indexOf("MSIE") > 0 ){
				window.print();
			}else{
				window.print();
			}
		});
		
		 /**
		 * <pre>
		 *   등록예정통보서 샘플 팝업
		 * </pre>
		 * @author KIM GA EUN
		 * @since 2014. 05. 01.
		 */	 
		 $('#btn_sample').click(function() {
				$url = '${samplePopupDomain}';
				winOpenPopup($url, 'getSamplePopup', 550, 500, 'yes');
		 });	 		
		
		/*
		* <pre>
		* 증빙 서류 파일명을 input 박스에 넣어주는 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
 		 $(document).on('change', '.default_txtFileInput', function(){
 			 $result =$(this).parent().parent();
 			 $result.find(".default_titleHiddenInput").val($(this).val());	
 			 $(this).parent().find(".dateUploadHiddenInput").val($(this).val());
		}); 
		
		/*
		* <pre>
		* 추가증빙서류를 추가할 때 input file 추가 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
		 $(document).on('click', '.btn_plus', function(){
			 
			 $parentEL = $(this).parent().parent().parent();			 
			 $cloneEL = $parentEL.clone();
			 if($cloneEL.find('.btn_minus').length == 0){
				 $cloneEL.find('.dataUpWrap01').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			 }
			 
			 $parentEL.find('.dataUpWrap01 > a').remove();
			 $parentEL.find('.dataUpWrap01').append('&nbsp;<a class="btn_minus" href="#none"><img src="${IMG}/common/ico_data_m.gif"></a>');
			  
			// $cloneEL.find('input[name=chg_dataUpload]').val('');
			 //$cloneEL.find('input[class=dateUploadHiddenInput]').val('');
			 //$cloneEL.find('input[type=file]').val('');
			 //$cloneEL.find("input[nane=chg_dataUploadType]").val('');
			 
			  $cloneEL.find('input[class=default_titleInput]').val('');
			 $cloneEL.find('input[class=dateUploadHiddenInput]').val('');
			 $cloneEL.find('input[type=file]').val('');
			 $cloneEL.find("input[class=default_titleHiddenInput]").val('');
			 $parentEL.parent().append($cloneEL);
			 
			 
		 });
		
		/*
		* <pre>
		* 추가증빙서류를 삭제할 때 input file 삭제 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
		 $(document).on('click', '.btn_minus', function(){

			 if($(".default_txtFileInput").length >=1 ){
				if($(this).parent().find(".btn_plus").length==1){
					$temp = $(this).parent().parent().parent().parent();
					if($temp.find(".addFile").length==1){
						$temp.parent().find(".default_txtFileInput").remove();
						$tempHtml='<input type="file" id="data_upload" name="dataUpload" class="default_txtFileInput"  maxlength="100" value="" style="width: 560px; height:22px;"/> ';
						$("#data_A").append($tempHtml);
						// 다시 input hidden 값 초기화
						$temp.parent().find(".dateUploadHiddenInput").val("");
						$(".default_titleInput").val("");
					
					}else{
						
						$parentEL = $(this).parent().parent().parent();			 
					 	$parentEL.remove();
						
						$paramprfBox = '<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>        '  ;
						$paramprfBox += '<a href="#none" class="btn_minus"><img src="${IMG}/common/ico_data_m.gif"></a>'  ;
						$temp.parent().find(".addFile").eq($temp.parent().find(".addFile").length-1).find(".dataUpWrap01").children().remove();
						$temp.parent().find(".addFile").eq($temp.parent().find(".addFile").length-1).find(".dataUpWrap01").append($paramprfBox);
					}
					
					
				}else{
				 	$parentEL = $(this).parent().parent().parent();			 
				 	$parentEL.remove();
				}					
			 }				 
		 });
		
		/*
		* <pre>
		*  증빙 자료 업로드 및 저장 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.09
		*/
		$("#Confirm").click(function(){

			// 파일이 한 개 이상 첨부되었는지 확인
			$result = false;
			$('.dateUploadHiddenInput').each(function() {
				if($.trim($(this).val()) != '') {
					$result = true;
					return false;
				}
			});		
			
			if($result) {
				//input box 값이 다 들어갔는지 확인 및 형태가 제대로 맞는지 확인 		
				$(".title").find('input:text').each(function() {
					if($(this).siblings('.default_titleHiddenInput').val() != ''){
						if($.trim($(this).val()) == '') {
							alert("변경할 파일명을 입력하셔야 합니다.");
							$(this).focus();
							$result = false;
							return false;
						}
						if(/[%\/\\\\:*?*<>|]/g.test($(this).val())) {
					 		alert("파일이름에는 다음과 같은 문자를 사용할수 없습니다. ( %\/\\:*?*<>| )");
					 		$(this).val('');
					 		$(this).focus();
					 		$result = false;
					 		return false;
					 	}
					}
				});
				
				if(!$result){return false;}
				
				// 사유 입력했는지 확인
				$('.write_explain').each(function() {
					if ($.trim($(this).val()) == '') {
						alert("증빙 서류에 대한 설명을 적어주셔야 합니다.");
						$(this).val('');
						$(this).focus();
						$result = false;
						return false;
					}
				});
				
				if(!$result){return false;}
				
			}else{
				alert("반드시 1개 이상의 거래증빙자료를 첨부해야 합니다.");
			}
			
			// 파일 확장자 검증
			$fileRegistTF = true;
			$('#debtDataBody').find('.default_txtFileInput').each(function(e){
				var result = checkUploadFile(this);
				if(!result){
					$fileRegistTF = false;
					return false;
				}
			});
			
			if(!$fileRegistTF){
				return false;
			}
			
			$titleInputs='';
			// 사용자가 직접 입력한 파일명에 확장자 명 넣어줌 
			$(".default_titleHiddenInput").each(function() {
				if($.trim($(this).val())!=""){
					$type = splitExtension($(this).val());
					
					if($type!=$(this).val()){
						$(this).val($(this).parent().find(".default_titleInput").val()+"." +$type);
						if($titleInputs!=''){
							$titleInputs+= ","+$(this).parent().find(".default_titleInput").val()+"." +$type;				
						}else{
							$titleInputs+=$(this).parent().find(".default_titleInput").val()+"." +$type;		
						}
					}else{
						if($titleInputs!=''){
							$titleInputs+= ","+$(this).parent().find(".default_titleInput").val();				
						}else{
							$titleInputs+=$(this).parent().find(".default_titleInput").val();		
						}
					}	
				}	
			});
			
			$("#ch_file_nm").val($titleInputs);

			if($result) {
				if(confirm("추가증빙자료를 등록하시겠습니까?")) {
					$('#debtOngoingInquiryFrm').attr({'action':'${HOME}/debt/addDeptOngoingAppl.do', 'method':'POST'})
					 .ajaxSubmit({
					 	dataType : "json",
						async : false,
						beforeSend: function() {
							// 추가증빙파일 저장 버튼 숨기기
							$('#Confirm').hide();
					    },
						success : function(msg){
							$result = msg;
							$debtStatHistId = msg.debtStatHistId;
							// 추가증빙파일 저장 버튼 보이기
							$('#Confirm').show();
							
		 					if($result.REPL_CD == '00000') {
		 						// [1] 진행완료 접수문서 상태값 수정
		 						$("#recentStatus").text("");
		 						$("#recentStatus").append(utfDecode($result.recentStatus));	 
		 						
		 						$('.debtOngoingStatus').css('display', '');
		 						
		 						// [2] 사유관리 리스트 update
		 						$("#statusBody").children().remove();
		 						$paramBox = '';
		 						$paramBox += ' <tr>' ;
 								$paramBox += '<th width="10%" class="first_thlwr4">날짜</th>' ;
								$paramBox += '<th width="15%">상태</th>' ;
								$paramBox += '<th width="35%">내용(사유)</th>' ;
								$paramBox += '<th width="30%">첨부파일</th>' ;
								$paramBox += '<th width="10%">작성자</th>' ;
								$paramBox += '</tr>' ;	
		 						if($result.histStatusList != null){
			 						for(var i=0; i<$result.histStatusList.length; i++ ){
			 							$paramBox += '<tr>' ;
			 							$paramBox += '<td class="first_tdlwr4">' + utfDecode($result.histStatusList[i].REG_DT_TIME) + '</td>' ;
			 							$paramBox += '<td>' +utfDecode($result.histStatusList[i].CH_STAT) + '</td>' ;
			 							$paramBox += '<td> ' +utfDecode($result.histStatusList[i].RMK_TXT) + '</td> '  ;
			 							$paramBox += '<td>' ;	 	
			 							
			 							if(($result.histStatusList[i].FILE_NM) != null){
			 								$arrayList = $result.histStatusList[i].FILE_NM;
			 								$.each(utfDecode($arrayList).split('|') , function(index , value) {
				 								$paramBox += '<a class="browsing" data-filePath=' ;
				 									$temp =utfDecode($result.histStatusList[i].FILE_PATH).split('|')[index];
				 									if($temp!=null){
				 										$paramBox += $temp ;
				 									}
				 									$paramBox += '>';
				 									if(utfDecode(value)!='null'){
						 								if(index==0){
						 									$paramBox+=utfDecode(value)+" ";
						 								}else if(index==1){
						 									$paramBox+=utfDecode(value)+" ";
						 								}else{
						 									$paramBox+="," + utfDecode(value) +" ";
						 								}
					 								}	
				 								$paramBox += '</a> ' ;	  
				 							});
			 							}
	 							
			 							$paramBox += '</td>' ;
			 							$paramBox += '<td>' + utfDecode($result.histStatusList[i].USR_NM) + '</td>' ;
			 							$paramBox += '</tr>' ;
			 						}
								}else{
									$paramBox += '<tr class="noSearch">' ;
									$paramBox += '<td colspan = "5"><div align="center" style="padding-top: 5px; padding-bottom: 5px;">검색 결과가 없습니다.</div></td>';
									$paramBox += '<tr>' ;
								}

	 							// [3] 현재 진행상태 수정함.
	 							$debtProcess = $result.debtProcess;
	 							$(".default_step_n").eq(0).text(utfDecode($debtProcess.STEP1));
	 							$(".default_step_n").eq(1).text(utfDecode($debtProcess.STEP2));
	 							$(".default_step_n").eq(2).text(utfDecode($debtProcess.STEP3));
	 							$(".default_step_n").eq(3).text(utfDecode($debtProcess.STEP4));
	 							$(".default_step_n").eq(4).text(utfDecode($debtProcess.STEP5));
	 							/* $(".default_step_n").eq(5).text(utfDecode($debtProcess.STEP6)); */
	 							
	 							$(".default_step_d").eq(0).text(utfDecode($debtProcess.STEP1_DT));
	 							$(".default_step_d").eq(1).text(utfDecode($debtProcess.STEP2_DT));
	 							$(".default_step_d").eq(2).text(utfDecode($debtProcess.STEP3_DT));
	 							$(".default_step_d").eq(3).text(utfDecode($debtProcess.STEP4_DT));
	 							$(".default_step_d").eq(4).text(utfDecode($debtProcess.STEP5_DT));
	 							/* $(".default_step_d").eq(5).text(utfDecode($debtProcess.STEP6_DT)); */
	 							
	 							// [4] 증빙파일 업로드 하는 폼 삭제함.
	 							$("#data_upload_area").remove();
	 							
	 							// [5] 현재 상태 Hidden값에 설정함.
	 							$("#current_stat").val($result.currentStat);
	

		 						$("#statusBody").append($paramBox);
		 						// [3] 추가 증빙 서류 reset
		 						$(".addFile").remove();
		 						$("#write_explain").val("");
		 						$paramprfBox = '<tr class="addFile">' ;
	 							$paramprfBox += '<td class="first_tdlwr4 title date_A" style="padding-left:5px;">' ;
	 							$paramprfBox += '<input type="hidden" class="default_titleHiddenInput default_txtInput" name="chg_dataUploadType" maxlength="100" style="border:1px;"> ' ;
	 							$paramprfBox += '<input type="text" class="default_titleInput default_txtInput" name="chg_dataUpload" maxlength="90" placeholder="파일명을 입력하세요" style="width: 120px;"> '  ;
	 							$paramprfBox += '</td>'  ;
	 							$paramprfBox += '<td class="default_note" id="data_A">'  ;
	 							$paramprfBox += '<input type="hidden" class="dateUploadHiddenInput" name="dateUploadHiddenInput" maxlength="100">';
	 							$paramprfBox += '<input type="file" id="data_upload" name="dataUpload" class="default_txtFileInput default_txtInput" maxlength="100" style="width: 560px; height:22px;"/> '  ;
	 							$paramprfBox += '<div class="dataUpWrap01">'  ;
	 							$paramprfBox += '<a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>   '  ;
	 							$paramprfBox += '<a href="#none" class="btn_minus"><img src="${IMG}/common/ico_data_m.gif"></a>   '  ;
	 							$paramprfBox += '</div>'  ;
	 							$paramprfBox += '</td></tr>'  ;
	 							
	 							
		 						$("#debtDataBody").append($paramprfBox);
		 						
		 						$(".paging").remove();
		 						$pageHtml = "<div class='page_num paging' id='getPageView'>" 
		 									+ utfDecode($result.pcPage)
		 									+"</div>";
		 						$("table.debtOngoingStatus").after($pageHtml);
								
		 						alert("증빙자료를 저장 완료했습니다.");
		 						
		 						location.href="${HOME}/debt/getDebtOngoingInquiry.do?debtApplId="+$('#debt_appl_seq').val();+"&debtStatHistId="+$debtStatHistId;
		 					} else {
		 						alert(utfDecode($result.REPL_MSG));
		 					}
						},
						error : function(xmlHttpRequest,textStatus,errorThrown) {
							alert("채무불이행신청서 저장 도중 에러발생 [" + textStatus + "]");
						}
					});
				}
			}
		});
		
		/*
		* <pre>
		*  채무불이행 상태에 따른 버튼 클릭 이벤트 함수
		* </pre>
		* @author Jong Pil Kim
		* @since 2014.05.10
		*/
		$(document).on('click', '.ApplyBtn_Type2 , .ApplyBtn_Type3', function(){
			// 채무불이행 정정 요청
			if($(this).hasClass('btnDebtReqCor')) {
				$url = '${HOME}/debt/getDebtRequireModify.do?';
				$url += 'debtApplId=' + $('#debt_appl_seq').val();
				$url += '&custId=' + $('#cust_id').val();
				window.open($url, 'getDebtRequireModifyPopup', 'width=400,height=420,scrollbars=no');
			}
			// 채무불이행 해제 요청
			else if($(this).hasClass('btnDebtReqRel')) {
				$url = '${HOME}/debt/getDebtRequireRelease.do?';
				$url += 'debtApplId=' + $('#debt_appl_seq').val();
				$url += '&custId=' + $('#cust_id').val();
				window.open($url, 'getDebtRequireReleasePopup', 'width=400,height=300,scrollbars=no');
			}
			// 신청취소
			else if($(this).hasClass('btnApplyCancel')) {
				$url = '${HOME}/debt/getDebtApplyCancel.do?';
				$url += 'debtApplId=' + $('#debt_appl_seq').val();
				$url += '&custId=' + $('#cust_id').val();
				window.open($url, 'getDebtApplyCancelPopup', 'width=400,height=270,scrollbars=no');
			}
			// 결제
			/*else if($(this).hasClass('btnPayment')) {
				if(!confirm('결제를 진행하시겠습니까?')) {
					return false;
				}
				var width = 400;
				var height = 330;
				$('#paymentForm').attr("target", "payRequestPopup");
				window.open("", 'payRequestPopup', 'width=' + width + ',height=' + height + ',scrollbars=no');
				$('#paymentForm').attr('action','${HOME}/payment/paymentFormPopup.do').submit();
			}*/
			// 목록
			else if($(this).hasClass('btnMoveList')) {
				location.href= "${HOME}/debt/getDebtInquiryList.do";
			}
		});
		
		/*
		* <pre>
		* 	첨부파일 다운로드 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.10
		*/
		 $(document).on('click', '.browsing', function(){

			 $("#file_path").val($(this).attr("data-filePath"));
			 $("#file_nm").val($(this).text());
			 
			 if ($(this).hasClass('debtFile')) {
				$('#debtOngoingInquiryFrm').attr({'action':'${HOME}/debt/getDebtPrfFileDownload.do', 'method':'POST'}).submit();
			 } else {
				$('#debtOngoingInquiryFrm').attr({'action':'${HOME}/debt/getDebtAddedPrfFileDownload.do', 'method':'POST'}).submit(); 
			 }
		 });
		
		/*
		* <pre>
		* 	페이징 function
		* </pre>
		* @author HWAJUNG SON
		* @since 2014.05.13
		*/
		 $(document).on('click', '.page', function(){
			 $param = "num=" + $(this).attr("pagenum")
			 		 + "&debtApplId="+$("#debt_appl_seq").val()
			 		 + "&custId="+$("#cust_id").val(); 
			 $.ajax({
					type : "POST", 
					url : "${HOME}/debt/getDebtOngoingStatHistoryList.do",
					dataType : "json",
					data : $param,
					async : false,
					success : function(msg) {
							$result = msg;
							if($result.REPL_CD='00000'){
								// 페이징 된 사유관리 리스트 출력
		 						$("#statusBody").children().remove();
		 						$paramBox = '';
		 						$paramBox += ' <tr>' ;
 								$paramBox += '<th width="10%" class="first_thlwr4">날짜</th>' ;
								$paramBox += '<th width="12%">상태</th>' ;
								$paramBox += '<th width="38%">내용(사유)</th>' ;
								$paramBox += '<th width="30%">첨부파일</th>' ;
								$paramBox += '<th width="10%">작성자</th>' ;
								$paramBox += '</tr>' ;
		 						for(var i=0; i<$result.histStatusList.length; i++ ){
		 							$paramBox += '<tr>' ;
		 							$paramBox += '<td class="first_tdlwr4">' + utfDecode($result.histStatusList[i].REG_DT_TIME) + '</td>' ;
		 							$paramBox += '<td>' +utfDecode($result.histStatusList[i].CH_STAT) + '</td>' ;
		 							$paramBox += '<td style="padding-left:5px; text-align:left;"> ' +utfDecode($result.histStatusList[i].RMK_TXT) + '</td> '  ;
		 							$paramBox += '<td>' ;	 	
		 							if(($result.histStatusList[i].FILE_NM) != null){
		 								$arrayList = $result.histStatusList[i].FILE_NM;
		 								$.each(utfDecode($arrayList).split('|') , function(index , value) {
			 								$paramBox += '<a class="browsing" data-filePath=' ;
			 									$temp =utfDecode($result.histStatusList[i].FILE_PATH).split('|')[index];
			 									if($temp!=null){
			 										$paramBox += $temp ;
			 									}
			 									$paramBox += '>';
			 									if(utfDecode(value)!='null'){
/* 			 										
					 								if(index==0){
					 									$paramBox+=utfDecode(value)+" ";
					 								}else{
					 									$paramBox+="," + utfDecode(value) +" ";
					 								}
 */
					 								if(index==0){
					 									$paramBox+=utfDecode(value)+" ";
					 								}else if(index==1){
					 									$paramBox+=utfDecode(value)+" ";
					 								}else{
					 									$paramBox+="," + utfDecode(value) +" ";
					 								}
				 								}	
			 								$paramBox += '</a> ' ;	  
			 							});
		 							}
 							
		 							$paramBox += '</td>' ;
		 							$paramBox += '<td>' + utfDecode($result.histStatusList[i].USR_NM) + '</td>' ;
		 							$paramBox += '</tr>' ;
		 							
		 						}

		 						$("#statusBody").append($paramBox);
							
		 						// 페이징 수정
		 						$(".paging").remove();
		 						$pageHtml = "<div class='page_num paging' id='getPageView'>" 
		 									+ utfDecode($result.pcPage)
		 									+"</div>";
		 									
		 						//$("table.debtOngoingStatus").after($pageHtml);
		 						$(".hist_page").prepend($pageHtml);
							}
							
						},
						error : function(xmlHttpRequest, textStatus, errorThrown){
							alert("사유관리 페이징 실패 [" + textStatus + "]");
						}
				});
			 
		 });
	});
	
	/*
	* <pre>
	* 	채무불이행 상태별 버튼 CallBack Function
	* </pre>
	* @author sungrangkong
	* @since 2014.05.21
	*/
	$.fn.callBackDebtProcessStatChange = function(stat){
		switch(stat){
			// [해제요청] callBack
			case "RR" :
				location.reload();
			break;
			// [채무불이행 정정요청] callBack
			case 'RequireRelease' :
				location.reload();
			break;
			// [신청취소] callBack
			case 'AC' :
				$url = '${HOME}/debt/getDebtCompleteInquiry.do?';
		    	$url += 'debtApplId='+$('#debt_appl_seq').val();
				$(location).attr('href',$url);
			break;
			// TODO [결제] 버튼 CallBack 작성해야함.
		}
	};

</script>

</head>
<body>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0201" />
</jsp:include>
<%-- Top Area End --%>


<div id="containerWrap">
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<!-- Left Area End -->

<div id="rightWrap">
<%--결제form Start--%>
<form id="paymentForm" name="paymentForm" method="post">
	<input type="hidden" name="prdCd" value="${sBox.productDebtCode }"/>
	<input type="hidden" name="regChag" value="${debtBox.REG_CHAG}"/>
	<input type="hidden" name="prdIdKey" value="${sBox.debtApplId}"/>
</form>
<%--결제form End--%>
<form autocomplete="off" enctype="multipart/form-data" method="post" id="debtOngoingInquiryFrm">
<%-- contentArea --%>
<%-- HIDDEN 영역 --%>
<input type="hidden" id="debt_appl_seq" name="debtApplSeq" value="${sBox.debtApplId}"/>
<input type="hidden" id="cust_id" name="custId" value="${sBox.custId}"/>
<input type="hidden" id="file_path" name="filePath" value=""/>
<input type="hidden" id="file_nm" name="fileNm" value=""/>
<input type="hidden" id="ch_file_nm" name="chFileNm" value=""/>
<input type="hidden" id="num" name="num" value="${sBox.num}"/> <%-- 페이지 번호 --%>
<input type="hidden" id="current_stat" name="currentStat" value="${debtBox.STAT}"/> <%-- 현재 상태 --%>
<%-- HIDDEN 영역 --%>
<%-- menu별 이미지 class sub00 --%>
	<div class="right_tit ltit02_0101"><em>채무불이행관리</em></div>
				
<%-- 		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="50%" class="first_thlwr4">신청서코드</th>
					<th width="50%">전송상태</th>
				</tr>
				<tr>
					<td class="first_tdlwr4">${debtBox.DOC_CD}</td>
					<td>
						<c:if test="${debtBox.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${debtBox.TRANS eq '1'}">전송완료</c:if>
						<c:if test="${debtBox.TRANS eq '2'}">전송중</c:if>
					</td>
				</tr>						
			</tbody>
			</table>
		</div>	 
			
		<div class="linedot_u40"> </div> --%>
		
		<p class="content_tit norm">진행 중 접수문서
			<a id="btn_sample" href="#none" class="ApplyBtn02" style="background:#4c91ff; solid #4e7ac2; color:#fff; text-shadow:0 0 1px #3f3f3f; border:1px solid #4e7ac2;">등록예정통보서 샘플</a>
		</p>
		
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="9%" class="first_thlwr4">진행상태</th>
					<th width="82%">설명</th>
					<th width="9%">전송상태</th>
				</tr>
				<tr>
					<td class="first_tdlwr4">
						<span <c:if test="${debtBox.STAT eq 'AA'}">class="red"</c:if> >${debtBox.CH_STAT}</span>
					</td>
					<td  style="text-align:left; padding-left:5px;">
						${debtBox.STAT_DESC}
					</td>
					<td>
						<c:if test="${debtBox.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${debtBox.TRANS eq '1'}">전송완료</c:if>
						<c:if test="${debtBox.TRANS eq '2'}">전송중</c:if>
						<c:if test="${debtBox.TRANS eq null}">-</c:if>
					</td>					
				</tr>						
			</tbody>
			</table>
		</div>	
		
		<div class="linedot_u40 debtOngoingStatus" <c:if test="${empty histStatusList}">style="display: none;"</c:if>> </div>
		
		<p class="content_tit debtOngoingStatus" <c:if test="${empty histStatusList}">style="display: none;"</c:if>>사유 관리</p>
		
		<div class="tbl04Wrap debtOngoingStatus" <c:if test="${empty histStatusList}">style="display: none;"</c:if>>
			<table class="debtOngoingStatus">
			<tbody id ="statusBody">
				<tr>
					<th width="10%" class="first_thlwr4">날짜</th>
					<th width="12%">상태</th>
					<th width="38%">내용(사유)</th>
					<th width="30%">첨부파일</th>
					<th width="10%">작성자</th>
				</tr>
				<c:forEach var="statHistoryList" items="${histStatusList}">
				<tr>
					<td class="first_tdlwr4">
						${statHistoryList.REG_DT_TIME}
					</td>
					<td>
						${statHistoryList.CH_STAT}
					</td>
					<td style="padding-left:5px; text-align:left;">
						${statHistoryList.RMK_TXT}
					</td>
					<td>
						<c:set var="FILE_NM" value="${fn:split(statHistoryList.FILE_NM,'|')}" />
						<c:set var="FILE_PATH" value="${fn:split(statHistoryList.FILE_PATH,'|')}" />
						<c:if test="${fn:trim(statHistoryList.FILE_NM) ne ''}">
                    		<c:forEach var="i" items="${FILE_NM}" varStatus="idx">
                    		<c:if test="${idx.index != 0}">,</c:if>
								<a class="browsing" data-filePath="${FILE_PATH[idx.index]}" >${FILE_NM[idx.index]}</a>									
							</c:forEach>		                    				
                    	</c:if>
					</td>
					<td>
						${statHistoryList.USR_NM}
					</td>
				</tr>
				</c:forEach>						
			</tbody>
			</table>
			<c:if test="${not empty histStatusList}" >	
			<div id="page_num_wrap" class="hist_page">
				<div class="page_num paging " id="getPageView">
					<c:out value="${pcPage}" escapeXml="false" />
				</div>
			</div>
			</c:if>
		</div>	
		
		
       <c:if test="${debtBox.STAT eq 'CA'  || debtBox.STAT eq 'CB'}">
		
		<div class="linedot_u40"> </div>
		
		<p class="content_tit">추가 증빙서류를 첨부하세요</p>
		
		<div class="tbl04Wrap">
		
		<table class="defaultTbl01_01" summary="파일 테이블">
			<caption>파일 테이블</caption>
			<colgroup>
				<col width="18%">
				<col width="82%">
			</colgroup>
			<tbody id="debtDataBody">								
				<tr class="addFile">
					<td class="first_tdlwr4 title date_A" style="padding-left:5px; height:15px;">
                       <input type="hidden" class="default_titleHiddenInput default_txtInput" name="chg_dataUploadType" maxlength="100" style="border:1px;">
                       <input type="text" class="default_titleInput default_txtInput" name="chg_dataUpload" maxlength="90" placeholder="파일명을 입력하세요" style="width: 120px;">
					</td>
					<td class="default_note" id="data_A">
						<input type="hidden" class="dateUploadHiddenInput" name="dateUploadHiddenInput" maxlength="100" value=''>
                        <input type="file" id="data_upload" name="dataUpload" class="default_txtFileInput default_txtInput"  maxlength="100" value="" style="width: 560px; height:22px;"/>              		
                         <div class="dataUpWrap01" style="margin-top:6px;">
                           <a href="#none" class="btn_plus"><img src="${IMG}/common/ico_data_p.gif"></a>
                           <a href="#none" class="btn_minus"><img src="${IMG}/common/ico_data_m.gif"></a>
                         </div>	
					</td>
				</tr>					
			</tbody>
		</table>
		
		<br/>

			<textarea class="write_explain" id="write_explain" name="write_explain" placeholder="추가된 서류 및 관련 내용 설명을 적어주세요." style="width:767px; height:80px;" ></textarea>
			
			<%--   ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임 --%>
		<%-- <c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtDocGrn eq true}"> --%>
			<div class="wart1">
				<span class="warn_ico1">※ </span> 문서 (txt, rtf, xls, xlsx, doc, docx, ppt, pptx, pps, ppsx, hwp, pdf), 이미지 (gif,jpg, jpeg, png, bmp), 압축 (zip) 파일 첨부가능<br />

			</div>
		        
		        <div class="ApplyBtn_Add">
					<div class="ApplyBtn_Step">
					<a href="#none" id="Confirm" class="step4_info on">증빙자료 업로드 및 저장</a>
					</div>
				</div>
	     <%-- </c:if> --%>
	        
		</div>	
	 </c:if>
	 	
	 	
 	<c:if test ="${debtProcess ne ''}">
	 	<div class="linedot_u40"> </div>
		 	<p class="content_tit">현재 진행상태</p>
		 	
		 	<%-- 현재 진행상태 테이블 시작 --%>
		 	
			<div id="defaultWrap">
				<div class="default_stepbox">
					<ul>
					
						<c:if test="${debtProcess.STEP1 ne ''}">
							<li class="s1">
							<p class="default_step_n">${debtProcess.STEP1 }</p>
							<p class="default_step_d">${debtProcess.STEP1_DT }</p>
							<em></em></li>
						</c:if>
						
						<c:if test="${debtProcess.STEP2 ne ''}">
							<li class="s2">
							<p class="default_step_n">${debtProcess.STEP2 }</p>
							<p class="default_step_d">${debtProcess.STEP2_DT }</p>
							<em></em></li>
						</c:if>
						
						<c:if test="${debtProcess.STEP3 ne ''}">
							<li class="s2">
							<p class="default_step_n">${debtProcess.STEP3 }</p>
							<p class="default_step_d">${debtProcess.STEP3_DT }</p>
							<em></em></li>
						</c:if> 
						
						<c:if test="${debtProcess.STEP4 ne ''}">
							<li class="s2">
							<p class="default_step_n">${debtProcess.STEP4 }</p>
							<p class="default_step_d">${debtProcess.STEP4_DT }</p>
							<em></em></li>
						</c:if>
						
						<c:if test="${debtProcess.STEP5 ne ''}">
							<li class="s2">
							<p class="default_step_n">${debtProcess.STEP5 }</p>
							<p class="default_step_d">${debtProcess.STEP5_DT }</p>
							<em></em></li>
						</c:if>
						<c:if test="${!empty debtProcess}">
							<c:if test="${debtProcess.STEP6 ne ''}">
								<li class="s2">
								<p class="default_step_n">${debtProcess.STEP6 }</p>
								<p class="default_step_d">${debtProcess.STEP6_DT }</p>
								<em></em></li>
							</c:if>
						</c:if>
						
					</ul>
				</div>
			</div>	
 	</c:if>
		
		<c:if test="${not empty modHistList}">
		
		<div class="linedot_u40"> </div>
		
		<p class="content_tit">채무불이행금액 정정 관리</p>
		
		<div class="tbl04Wrap">
			<table>
			<tbody id ="statusBody">
				<tr>
					<th width="15%" class="first_thlwr4">정정요청일</th>
					<th width="25%">정정요청 채무금액</th>
					<th width="45%">정정사유</th>
					<th width="15%">정정완료일</th>
					<!-- <th width="10%">IF상태</th> -->
				</tr>
				
				<c:forEach var="modList" items="${modHistList}">
				<tr>
					<td class="first_tdlwr4">
						${modList.REG_DT_TIME}
					</td>
					<td class="list_right" style="text-align: right; padding-right: 10px;">${modList.DEBT_AMT}원</td>
					<td  style="text-align:left; padding-left:5px;">${modList.RMK_TXT}</td>
					<td>${modList.FINISH_DT}</td>
					<%-- <td><c:if test="${modList.TRANS eq '0'}">전송실패</c:if>
						<c:if test="${modList.TRANS eq '1'}">전송완료</c:if>
						<c:if test="${modList.TRANS eq '2'}">전송중</c:if>
					</td> --%>
				</tr>
				</c:forEach>						
			</tbody>
			</table>

		</div>	
		
		
		</c:if>
		
	<%-- content --%>
	
	<div class="linedot_u40"> </div>

        
		<p class="content_tit norm">채무불이행 신청서
			<a id="open_all" href="#none" class="ApplyBtn02">모두펼치기</a>		
			<a id="close_all" href="#none" class="ApplyBtn02" style="display: none;">모두닫기</a>
		</p>	
		
<!-- 		<div id="tbl_numbtnWrap">
		  <table class="tbl_numbtn">
		  <tbody>
		    <tr>
		      <td class="right">
		        <div class="btn_whitebtn_01">
		          <a id="open_all" href="#none" class="ApplyBtn02 on">모두펼치기</a>		
			      <a id="close_all" href="#none" class="ApplyBtn02 on" style="display: none;">모두닫기</a>
			      
		        </div>
		      </td>
		      
		    </tr>
		  </tbody>
		  </table>
		</div>	 -->

		<%-- 채무불이행 신청서 테이블 시작 --%>
		<div id="defaultApplyWrap">
			<div class="default_Apply">
				<ul id="menu_slide">
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 01. 신청인 정보입력</a></span>
						<ul style="display: none;">
							
							<%-- STEP01 시작 --%>
							<li class="Apply_sub">							
								<div class="ApplysubWrap">
								<p class="content_dot">회사정보</p>							
									<table class="defaultTbl01_01" summary="회사정보 입력테이블" >
									<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>

											<tr>
												<td class="title">회사명</td>
												<td>
													${debtBox.COMP_NM} 
												</td>
												<td class="title">사업자번호</td>
												<td>
													<c:set var="USR_NO" value="${fn:substring(debtBox.COMP_NO, 0, 3)}-${fn:substring(debtBox.COMP_NO, 3, 5)}-${fn:substring(debtBox.COMP_NO, 5, 13)}" />
													${USR_NO}
												</td>
											</tr>
											<c:choose>
												<c:when test="${debtBox.COMP_TYPE eq 'C'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">법인등록번호</td>
														<td>
															<c:if test="${!empty debtBox.CORP_NO}">
																${fn:substring(debtBox.CORP_NO,0,6)}-${fn:substring(debtBox.CORP_NO,6,13)}
															</c:if>
														</td>
													</tr>
													<tr>
														<td class="title">업종</td>
														<td class="default_note" colspan="3">${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
												<c:when test="${debtBox.COMP_TYPE eq 'I'}">
													<tr>
														<td class="title">대표자명</td>
														<td>${debtBox.OWN_NM}</td>
														<td class="title">업종</td>
														<td>${debtBox.BIZ_TYPE}</td>
													</tr>
												</c:when>
											</c:choose>
											<tr>
												<td class="title">주소</td>
												<td class="default_note" colspan="3">
													<c:if test="${fn:trim(debtBox.POST_NO) ne ''}">
														(${debtBox.POST_NO})
													</c:if>
													<c:if test="${(debtBox.ADDR_1 ne null) or (debtBox.ADDR_1 ne '')}">
														 ${debtBox.ADDR_1} ${debtBox.ADDR_2}
													</c:if>
												</td>
											</tr>											
										</tbody>
									</table>
									
									<p class="content_dot2">정보등록 담당자</p>								
									<table class="defaultTbl01_01" summary="정보등록 담장자">
									<caption>정보등록 담당자</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>
											<tr>
												<td class="title">성명</td>
												<td>${debtBox.USR_NM}</td>
												<td class="title">부서</td>
												<td>${debtBox.DEPT_NM}</td>
											</tr>
											<tr>
												<td class="title">직위</td>
												<td class="default_note">${debtBox.JOB_TL_NM}</td>
												<td class="title">전화</td>
												<td class="default_note">
													 ${debtBox.TEL_NO}
												</td>
											</tr>
											<tr>
												<td class="title">휴대폰</td>
												<td class="default_note">
													${debtBox.MB_NO}
												</td>												
												<td class="title">팩스</td>
												<td class="default_note">
													${debtBox.FAX_NO}
												</td>
											</tr>
											<tr>
												<td class="title">이메일</td>
												<td class="default_note" colspan="3">
													${debtBox.EMAIL}
												</td>
											</tr>										
										</tbody>
									</table>
							
									<div class="empty_b20"> </div>
								</div>							
							</li>
							<%-- STEP01 끝 --%>
							
						</ul>
					</li>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 02. 채무자 정보입력</a></span>
						<ul style="display: none;">
						
							<%-- STEP02 시작 --%>
							<li class="Apply_sub">
								<div class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="회사정보 입력테이블">
									<caption>회사정보</caption>
										<colgroup>
											<col width="18%">
											<col width="32%">						
											<col width="18%">
											<col width="32%">
										</colgroup>
										<tbody>

											<tr>
												<td class="title">회사명</td>
												<td>
													${debtBox.CUST_NM} 
												</td>
												<td class="title">사업자번호</td>
												<td>
													<c:set var="CUST_USR_NO" value="${fn:substring(debtBox.CUST_NO, 0, 3)}-${fn:substring(debtBox.CUST_NO, 3, 5)}-${fn:substring(debtBox.CUST_NO, 5, 13)}" />
													${CUST_USR_NO}
												</td>
											</tr>
											<tr>
												<td class="title">대표자명</td>
												<td <c:if test="${debtBox.CUST_TYPE eq 'I'}">colspan="3"</c:if>>${debtBox.CUST_OWN_NM}</td>
												<c:if test="${debtBox.CUST_TYPE eq 'C'}">
													<td class="title">법인등록번호</td>
													<td>
														<c:if test="${!empty debtBox.CUST_CORP_NO}">
															${fn:substring(debtBox.CUST_CORP_NO,0,6)}-${fn:substring(debtBox.CUST_CORP_NO,6,13)}
														</c:if>													
													</td>
												</c:if>
											</tr>											
											<tr>
												<td class="title">주소</td>
												<td class="default_note" colspan="3">
													<c:if test="${fn:trim(debtBox.CUST_POST_NO) ne ''}">
														(${debtBox.CUST_POST_NO})
													</c:if>
													<c:if test="${(debtBox.CUST_ADDR_1 ne null) or (debtBox.CUST_ADDR_1 ne '')}">
														 ${debtBox.CUST_ADDR_1} ${debtBox.CUST_ADDR_2}
													</c:if>
												</td>
											</tr>	
											<tr>
												<td class="title">채무구분</td>
												<td>${debtBox.DEBT_TYPE_STR}</td>
												<td class="title">채무불이행 금액</td>
												<td>${debtBox.ST_DEBT_AMT}원</td><%-- <fmt:formatNumber type="NUMBER" value="${debtBox.ST_DEBT_AMT_IF}" groupingUsed="true"/> --%>
											</tr>
											<tr>
												<td class="title">연체개시일</td>
												<td>${debtBox.OVER_ST_DT}</td>
 												<td class="title">등록사유 발생일</td>
												<td>${debtBox.REG_RES_DT_TIME}</td>
											</tr>
										</tbody>
									</table>
									
									<div class="empty_b20"> </div>
																	
								</div>
							</li>
							<%-- STEP02 끝 --%>
							
						</ul>
					</li>
					<p class="line"></p>
					<li class="Apply_slide"><span class="slideSpan"><a href="#none" class="down">STEP 03. 거래증빙자료 첨부 및 작성완료</a></span>
						<ul style="display: none;">
							
							<%-- STEP04 시작 --%>
							<li class="Apply_sub">
								<div class="ApplysubWrap">
									<table class="defaultTbl01_01" summary="거래증빙자료 첨부 및 작성완료 테이블">
									<caption>거래증빙자료</caption>
										<colgroup>
											<col width="18%">
											<col width="82%">
										</colgroup>
										<tbody id="prfArea">
										<tr>
											<td class="title">거래명세서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'A'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="browsing debtFile" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">세금계산서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'B'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="browsing debtFile" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">법원판결문</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'C'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="browsing debtFile" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">계약서</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'D'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="browsing debtFile" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										<tr>
											<td class="title">기타증빙자료</td>
											<td class="default_note" style="padding: 10px 0 10px 9px;">
												<c:forEach var="prfFile" items="${prfFileList}" varStatus="idx">
													<c:if test="${prfFile.TYPE_CD eq 'E'}">
														<span style="display:inline-block; margin: 5px 0px 5px 0px;">
															<a href="#none" class="browsing debtFile" data-filePath="${prfFile.FILE_PATH}" style="color:#4f7eb2;">${prfFile.FILE_NM}</a>
														</span>
														<span class="comma">, </span>
													</c:if>
												</c:forEach>			
											</td>
										</tr>
										</tbody>
									</table>
															
								</div>
							</li>
							<%-- STEP04 끝 --%>
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<%-- 채무불이행 신청서 테이블 끝 --%>
		
		<div class="linedot_u40"> </div>
		
		<div class="tbl04Wrap">
		<table id="apply_company_info" class="defaultTbl01_01" summary="안내사항 테이블">
			<caption>안내사항</caption>
			<colgroup>
				<col width="18%">
				<col width="82%">
			</colgroup>
			<tbody>
				<tr >
				<td class="title" rowspan="5" style="height:150px;"><p align="center">안내사항</p></td>
				<td class="default_note2" style="height:150px;">
					<pre class="information_briefing">${result.informationBriefing}</pre>
					<div class="information_briefing_comment">
					심사 및 신청취소, 해제 관련 문의 사항은 아래 전화번호로 문의해주시길 바랍니다.<br /> 
                    KED연체관리서비스 담당자: 02-3215-2432
					</div>
				</td>

				
			</tbody>
		</table>
		</div>	
		
		<div class="empty_b7"> </div>
		
		<div id="tbl_numbtnWrap">
			<table class="tbl_numbtn">
			<tbody>
				<tr>
					<td class="left">
						<div id="btnWrap_l">
							<div class="btn_graybtn_01" id="btnWrap_l">
								<a id="btn_print" href="#none" class="on" id="btn_del">인쇄</a>
							</div>
						</div>
					</td>
					
					<td class="right">

						
						<%--   ERP 회원이 아니면서 && 채무불이행 신청서 작성,수정,삭제 권한이 있는경우만 삭제버튼이 보임 --%>
					<%-- <c:if test="${sBox.sessionErpUsrId eq '' && sBox.isSessionDebtApplGrn eq true}"> --%>
						<c:choose>
							<%-- 심사대기(AW), 심사중(EV) --%>
							<c:when test="${(debtBox.STAT eq 'AW') or (debtBox.STAT eq 'EV')}">
							<div id="btnWrap_l">
								<div class="btn_whitebtn_01">
									<a class="ApplyBtn_Type2 btnApplyCancel on" href="#none" data-stat="${debtBox.STAT}">신청취소</a>
								</div>
							</div>
							</c:when>
							<%-- 통보서 준비(PY) 통보서 발송(NT), 등록전민원발생(채무불이행등록전통보서발생시)(CA),등록완료(RC),등록전민원발생(추가증빙)(FA) 등록후민원발생(정보등록완료 후)(CB),등록후민원발생(추가증빙)(FB) --%>
							<c:when test="${(debtBox.STAT eq 'PY') or (debtBox.STAT eq 'NT') or (debtBox.STAT eq 'CA') or (debtBox.STAT eq 'RC') or (debtBox.STAT eq 'CB') or (debtBox.STAT eq 'FA') or (debtBox.STAT eq 'FB')}">
								<c:if test="${debtBox.MOD_STAT ne 'R'}">
								<div id="btnWrap_l">
									<div class="btn_whitebtn_01">
										<a class="ApplyBtn_Type2 btnDebtReqCor on" href="#none" data-stat="${debtBox.STAT}">채무금액 정정 요청</a>
									</div>
								</div>
								</c:if>
								<div id="btnWrap_l">
									<div class="btn_whitebtn_01">
									<a class="ApplyBtn_Type2 btnDebtReqRel on" href="#none" data-stat="${debtBox.STAT}">해제 요청</a>
									</div>
								</div>
							</c:when>
						</c:choose>
					<%-- </c:if> --%>
						
				<div class="btn_whitebtn_01">
					<a href="#none" class="ApplyBtn_Type2 btnMoveList on">목록</a>
				</div>
				
											
					</td>
            

				</tr>
			</tbody>
			</table>
		</div>	
		

	<%-- //content --%>
	

<%-- //contentArea --%>
</form>
</div>
</div>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>