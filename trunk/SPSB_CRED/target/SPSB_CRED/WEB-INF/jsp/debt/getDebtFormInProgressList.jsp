<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<%-- 채무불이행 검색 결과 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>

<%-- 정렬관련 공통코드 --%>
<c:set var="orderTypeList" value="${result.orderTypeList}"/>

<%-- 목록갯수 공통코드 --%>
<c:set var="rowSizeTypeList" value="${result.rowSizeTypeList}"/>

<%-- 작성일자 검색조건 공통코드 --%>
<c:set var="registPeriodConditionTypeList" value="${result.registPeriodConditionTypeList}"/>

<%-- 정렬 공통코드 --%>
<c:set var="noPaymentApplicationListOrderConditionTypeList" value="${result.noPaymentApplicationListOrderConditionTypeList}"/>

<%-- 채무불이행 신청서 리스트 --%>
<c:set var="debtList" value="${result.debtList}"/>

<%-- 채무불이행 신청서 페이징 --%>
<c:set var="pcPage" value="${result.pcPage}"/>

<%-- 채무불이행 검색 총 갯수 --%>
<c:set var="total" value="${result.total}"/>
 
<!-- 신청일자 검색조건 공통코드 -->
<c:set var="applyPeriodConditionTypeList" value="${result.applyPeriodConditionTypeList}"/>


<head>
<title>채무불이행등록 &gt; 신청서조회 | 스마트채권 - 채권관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/debt/getDebtList.css" />

<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	  
	/**
	* <pre>
	*   채무불이행 검색 버튼 클릭 이벤트 함수
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
	$('.search').click(function(){
		if(!$.fn.validate()) return false;
			$('#num').val(1);
			$('form').attr({'action':'${HOME}/debt/getDebtFormInProgressList.do', 'method':'POST'}).submit();
	});
  
      
	/**
    * <pre>
    * 채무불이행 신청서 정보 Excel Download 함수
    * </pre>
    * @author JUNG MI KIM
    * @since 2014. 06. 03
    */
	$('#btn_save_excel').click(function(){
		if(!$.fn.validate()) return false;
		$('#num').val(1);
		$('form').attr({'action':'${HOME}/debt/getDebtFormInProgressListForExcel.do', 'method':'POST'}).submit();
	}); 
      
	/**
	* <pre>
	*	채무불이행 신청서 상세 정보 페이지 조회
	* </pre>
	* @author sungrangkong
	* @since 2013. 08. 27
	*/
	$('.linkDebt').click(function(){
       	$params = $(this).attr('data-debtApplId');
       	$paramObj = {
       		 debtApplId : $(this).attr('data-debtApplId'),
   			 custId : $(this).attr('data-custId')
   	    };
       	location.href ="${HOME}/debt/getDebtFormInProgress.do?" + $.param($paramObj); 
	});
       
	/**
	* <pre>
	*  정렬 조건을 변경시키면 form을 전송
	* </pre>
	* @author sungrangkong
	* @since 2014. 04. 30
	*/
 	$('.listOrderOption').change(function(){
		if(!$.fn.validate()) return false;
		$('#num').val(1);
		$('form').attr({'action':'${HOME}/debt/getDebtFormInProgressList.do', 'method':'POST'}).submit();
	}); 

	/**
	* <pre>
	*	진행상태 CHECKBOX 전체선택 function
	* </pre>
	* @author HWAJUNG SON
	* @since 2014. 05. 7
	*/
	$('.processStatusType').change(function(){        	
       	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $(this).parent().find(':checked').not('[name=processStatusALL]').length;
		
		if($(this).val() == 'ALL' && checkedLen == 17){
       		$(this).attr('checked',true);
		}else if($(this).val() == 'ALL' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       	}else if($(this).val() != 'ALL' && checkedLen != 16){
       		$("#condition01").attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 16){
       		$("#condition01").attr('checked',true);
       	
       	}
	});
	 
	/**
	*<pre>
	*	진행상태 클릭시  function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.07
	*/
	$(".processStatusType").click(function(){
		
		$processList = '';
		if($("input[class=processStatusType]:checkbox:checked").length > 0){
			$("input[class=processStatusType]:checkbox:checked").each(function(index){
				   $processList += $.trim($("input[class=processStatusType]:checkbox:checked").eq(index).val());
				  
				   if((index+1) != $("input[class=processStatusType]:checkbox:checked").length){
					   $processList += ',';
				   }

			   });
		}
		$("#process_status_type").val($processList);
				
		// 전체를 클릭하였을 경우 
		if($(this).attr("id")=='condition01'){
			if($(this).is(":checked")){
				$('.processStatusType').prop('checked', true);	
			}else{
				$('.processStatusType').prop('checked', false);
			}
		}
		
		// 진행상태가 전체, 통보서 준비, 통보서 발송, 민원발생, 민원발생(추가증빙), 등록완료 
		// 체크 되어있을 경우 채무금액 정정 검색가능 
		/* if($(".chkmodType:checked:not(#condition01)").length>0 || $(".processStatusType:checked").length==0 ){
			$("#mod_stat_type").show();
			
		}else{
			$("#mod_stat_type").hide();
			$(".modStatType").prop("checked",false);
		} */
			
	});
	
	/**
	*<pre>
	*	작성기간 전체가 아닐 경우 날짜표시 function
	*</pre>
	*@author HWAJUNG SON
	*@since 2014.05.07
	*/
    $('#period_condition').change(function(){
   	 if('ALL_DT' == $(this).val()){
   		 $(this).parent().siblings().attr('disabled','disabled').hide(); 
   	 } else {
   		 $(this).parent().siblings().removeAttr('disabled').show();
   	 }
    });
	
	/**
	*<pre>
	* 신청서 코드 전체일 경우  신청코드 Disable function
	*</pre>
	*@author Jungmi Kim
	*@since 2014.05.16
	*/
	$('.applyCodeType').change(function(){
	   	 if('all_code' == $(this).val()){
	   		$("#applyCodeKwd").attr('disabled','disabled');
	   		$("#applyCodeKwd").val("");
	   	 } else {
	   		$('#applyCodeKwd').removeAttr('disabled');
	   	 }
	 });
    
	
	/**
	*<pre>
	* 신청자 코드 전체일 경우  신청코드 Disable function
	*</pre>
	*@author Jungmi Kim
	*@since 2014.05.16
	*/
	$('.applySearchType').change(function(){
	   	 if('apply_business_all' == $(this).val()){
	   		$("#applyKwd").attr('disabled','disabled');
	   		$("#applyKwd").val("");
	   	 } else {
	   		$('#applyKwd').removeAttr('disabled');
	   	 }
	 });
	
	/**
	*<pre>
	* 채무자  코드 전체일 경우  신청코드 Disable function
	*</pre>
	*@author Jungmi Kim
	*@since 2014.05.16
	*/
	$('.custSearchType').change(function(){
	   	 if('business_all' == $(this).val()){
	   		$("#custKwd").attr('disabled','disabled');
	   		$("#custKwd").val("");
	   	 } else {
	   		$('#custKwd').removeAttr('disabled');
	   	 }
	 });
	
        
});



/**
 * <pre>
 *   FORM 전송 직전 검증 함수
 * </pre>
 * @author sungrangkong
 * @since 2014. 04. 30 
 */
 $.fn.validate = function() {
	
	 $bTF = true; // 검증용 Flag 변수
	 
	 // 채무자 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	 if(!$("#business_all").is(':checked') && $.trim($("#custKwd").val()) == ''){
		 alert("[" + $(".custSearchType:checked").next().text() + "] 검색어를 입력해주세요");
		 $("#custKwd").focus();
		 $bTF = false;
	 }
	 
	// 신청자 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	 if(!$("#apply_business_all").is(':checked') && $.trim($("#applyKwd").val()) == ''){
		 alert("[" + $(".applySearchType:checked").next().text() + "] 검색어를 입력해주세요");
		 $("#applyKwd").focus();
		 $bTF = false;
	 }
	
	// 신청코드 검색조건이 '전체'가 아닌경우 검색어가 반드시 있어야 함.
	 if(!$("#all_code").is(':checked') && $.trim($("#applyCodeKwd").val()) == ''){
		 alert("[" + $(".applyCodeType:checked").next().text() + "] 검색어를 입력해주세요");
		 $("#applyCodeKwd").focus();
		 $bTF = false;
	 }
	
	 if($(".processStatusType:checked").length == 0){
		alert("진행상태를 선택 해주세요.");
		$bTF = false;
	 }
	 
	 return $bTF;
 };
</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0201"/>
</jsp:include>
<!-- Top Area End -->

<!-- contentArea -->
<div class="contentArea sub01"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
				<h2 class="tit_main0102">신청서 내역 조회</h2>
				<p class="tit_main0102_under">신청 완료 후, 접수 진행 중인 문서를 조회하고 확인합니다</p>
	    </div>	
		
		<form name="debtListFrm" id="debtListFrm">
		
		<!-- FORM HIDDEN AREA START -->
		<input type="hidden" name="num" id="num" value="${sBox.num}" />
		<input type="hidden" id="target" value="${HOME}/debt/getDebtFormInProgressList.do">
	
		<!-- FORM HIDDEN AREA END -->
		
		<fieldset class="searchBox">
			<legend>신청서 조회</legend>
			<table summary="채무불이행 신청서 조회 테이블로, 검색조건인 작성일자,거래처,진행상태,등록담당자로 구성되어있습니다.">
			<caption>신청서 조회 </caption>
				<colgroup>
					<col width="20%" />
					<col width="80%" />
				</colgroup>
				<tr>
						<th scope="row">작성기간</th>
						<td class="block">
							<span class="first">
								<select title="작성기간" name="periodConditionType" id="period_condition">
									<c:forEach var="registPeriodConditionTypeList" items="${registPeriodConditionTypeList}">
										<option value="${registPeriodConditionTypeList.KEY}" <c:if test="${sBox.periodConditionType eq registPeriodConditionTypeList.KEY}">selected</c:if>>${registPeriodConditionTypeList.VALUE}</option>	
									</c:forEach>
								</select>
							</span>
							<input type="text" name="periodStDt" id="period_st_dt" class="firstDate periodDate" title="작성기간 시작조건" value="${sBox.periodStDt}" readonly="readonly" <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>/><span <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>> ~ </span>
							<input type="text" name="periodEdDt" id="period_ed_dt" class="firstDate periodDate" title="작성기간 종료조건" value="${sBox.periodEdDt}" readonly="readonly" <c:if test="${sBox.periodConditionType eq 'ALL_DT' }">style="display:none;"</c:if>/>
						</td>
				</tr>
				<tr>
					<th scope="row">신청코드</th>
					<td class="block">
						<input type="radio" class="applyCodeType" name="applyCodeType" value="all_code" id="all_code" <c:if test="${sBox.applyCodeType eq 'all_code' }">checked</c:if>/> <label for="all_code">전체</label>
						<input type="radio" class="applyCodeType" name="applyCodeType" value="apply_code" id="apply_code" <c:if test="${sBox.applyCodeType eq 'apply_code' }">checked</c:if>/> <label for="apply_code">신청코드</label>
						<input type="text" id="applyCodeKwd" name="applyCodeKwd" class="txtInput" style="width:110px"  value="${sBox.applyCodeKwd}" maxlength="70"  <c:if test="${sBox.applyCodeType eq 'all_code' }">disabled="disabled"</c:if>/>
					</td>
				</tr>
				<tr>
					<th scope="row">신청자</th>
					<td class="block">
						<input type="radio" class="applySearchType" name="applySearchType" value="apply_business_all" id="apply_business_all" <c:if test="${sBox.applySearchType eq 'apply_business_all' }">checked</c:if>/> <label for="apply_business_all">전체</label>
						<input type="radio" class="applySearchType" name="applySearchType" value="apply_business_no" id="apply_business_no" <c:if test="${sBox.applySearchType eq 'apply_business_no' }">checked</c:if>/> <label for="apply_business_no">사업자번호</label>
						<input type="radio" class="applySearchType" name="applySearchType" value="apply_company_name" id="apply_company_name" <c:if test="${sBox.applySearchType eq 'apply_company_name' }">checked</c:if>/> <label for="apply_company_name">신청회사명</label>
						<input type="radio" class="applySearchType" name="applySearchType" value="apply_usr_id" id="apply_usr_id" <c:if test="${sBox.applySearchType eq 'apply_usr_id' }">checked</c:if>/> <label for="apply_company_owner_name">신청인</label>
						<input type="text" id="applyKwd" name="applyKwd" class="txtInput" style="width:110px" value="${sBox.applyKwd}" maxlength="70" <c:if test="${sBox.applySearchType eq 'apply_business_all' }">disabled="disabled"</c:if>/>
					</td>
				</tr>
				<tr>
					<th scope="row">채무자</th>
					<td class="block">
						<input type="radio" class="custSearchType" name="custSearchType" value="business_all" id="business_all" <c:if test="${sBox.custSearchType eq 'business_all' }">checked</c:if>/> <label for="business_all">전체</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="business_no" id="business_no" <c:if test="${sBox.custSearchType eq 'business_no' }">checked</c:if>/> <label for="business_no">사업자번호</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_name" id="company_name" <c:if test="${sBox.custSearchType eq 'company_name' }">checked</c:if>/> <label for="company_name">채무회사명</label>
						<input type="radio" class="custSearchType" name="custSearchType" value="company_owner_name" id="company_owner_name" <c:if test="${sBox.custSearchType eq 'company_owner_name' }">checked</c:if>/> <label for="company_owner_name">대표자(채무자)</label>
						<input type="text" id="custKwd" name="custKwd" class="txtInput" style="width:110px" value="${sBox.custKwd}" maxlength="70" <c:if test="${sBox.custSearchType eq 'business_all' }">disabled="disabled"</c:if>/>
					</td>
				</tr>
				<tr>
					<th scope="row" rowspan="3">진행상태 </th>
					<td class="block">
					<input type="checkbox" class="processStatusType chkmodType" name="processStatusALL" value="ALL" id="condition01" <c:if test="${sBox.processStatusALL eq 'ALL'}">checked</c:if> /> <label for="condition01">전체</label>
					</td>
				</tr>
				<tr>
					<td class="inlineBlock">
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AW" id="condition02" <c:if test="${sBox.processStatusAW eq 'AW'}">checked</c:if> /> <label for="condition02">심사대기</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="EV" id="condition03" <c:if test="${sBox.processStatusEV eq 'EV'}">checked</c:if>  /> <label for="condition03">심사중</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AA" id="condition04" <c:if test="${sBox.processStatusAA eq 'AA'}">checked</c:if>  /> <label for="condition04">결제대기</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="PY" id="condition05" <c:if test="${sBox.processStatusPY eq 'PY'}">checked</c:if>  /> <label for="condition05">통보서 준비</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="NT" id="condition06" <c:if test="${sBox.processStatusNT eq 'NT'}">checked</c:if>  /> <label for="condition06">통보서 발송</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CA" id="condition07" <c:if test="${sBox.processStatusCA eq 'CA'}">checked</c:if>  /> <label for="condition07">민원발생</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FA" id="condition08" <c:if test="${sBox.processStatusFA eq 'FA'}">checked</c:if>  /> <label for="condition08">추가증빙제출</label><br/>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="RC" id="condition09" <c:if test="${sBox.processStatusRC eq 'RC'}">checked</c:if>  /> <label for="condition09">등록완료</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="RR" id="condition10" <c:if test="${sBox.processStatusRR eq 'RR'}">checked</c:if>  /> <label for="condition10">해제요청</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="CB" id="condition11" <c:if test="${sBox.processStatusCB eq 'CB'}">checked</c:if>  /> <label for="condition11">민원발생</label>
						<input type="checkbox" class="processStatusType chkmodType" name="processStatusType" value="FB" id="condition12" <c:if test="${sBox.processStatusFB eq 'FB'}">checked</c:if>  /> <label for="condition12">추가증빙제출</label>
					</td>
				</tr>
				<tr>
					<td class="inlineBlock">
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AU" id="condition13" <c:if test="${sBox.processStatusAU eq 'AU'}">checked</c:if> /> <label for="condition02">신청불가</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="AC" id="condition14" <c:if test="${sBox.processStatusAC eq 'AC'}">checked</c:if> /> <label for="condition03">신청취소</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="CR" id="condition15" <c:if test="${sBox.processStatusCR eq 'CR'}">checked</c:if> /> <label for="condition04">해제완료</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="RU" id="condition16" <c:if test="${sBox.processStatusRU eq 'RU'}">checked</c:if> /> <label for="condition05">등록불가</label>
						<input type="checkbox" class="processStatusType" name="processStatusType" value="KF" id="condition17" <c:if test="${sBox.processStatusKF eq 'KF'}">checked</c:if> /> <label for="condition06">심사과실발생</label>
					</td>
				</tr>
				<tr id="mod_stat_type">
					<th scope="row">채무금액정정</th>
					<td>
						<input type="checkbox" class="modStatType" name="modStatR" value="R" id="mod_stat_r" <c:if test="${sBox.modStatR eq 'R'}">checked</c:if>  /> <label for="mod_stat_r">채무금액 정정요청</label>
						<input type="checkbox" class="modStatType" name="modStatC" value="C" id="mod_stat_c" <c:if test="${sBox.modStatC eq 'C'}">checked</c:if>  /> <label for="mod_stat_c">채무금액 정정완료</label>
						
					</td>
				</tr>
				<%-- <tr>
					<th scope="row">ERP 상태</th>
					<td class="inlineBlock">
						<input type="radio" class="processErpType" name="processErpType" value="erp_all" id="erp_all" <c:if test="${sBox.processErpType eq 'erp_all'}">checked</c:if> /> <label for="condition01">전체</label>
						<input type="radio" class="processErpType" name="processErpType" value="Y" id="erp_y" <c:if test="${sBox.processErpType eq 'Y'}">checked</c:if> /> <label for="condition02">ERP회원</label>
						<input type="radio" class="processErpType" name="processErpType" value="N" id="erp_n" <c:if test="${sBox.processErpType eq 'N'}">checked</c:if>  /> <label for="condition03">일반회원</label>
					</td>
				</tr> --%>
			</table>
			<div class="search_btn">
				<div>
					<a class="search" id="search" href="#">검색</a>
				</div>
			</div>
		</fieldset>
		
		<div class="tableTop" >
				<p class="totalCnt">검색결과 [${total}개]</p>
			<div class="rightTop" >
				정렬
				<select id="order_condition" class="listOrderOption" name="orderCondition" title="일자별로 정렬">
					<c:forEach var="orderC" items="${applyPeriodConditionTypeList}">
						<option value="${orderC.KEY}" <c:if test="${sBox.orderCondition eq orderC.KEY}">selected</c:if>>${orderC.VALUE}</option>	
					</c:forEach>
				</select>
				<select  id="order_type" class="listOrderOption" name="orderType" title="조건별로 정렬">
					<c:forEach var="orderT" items="${orderTypeList}">
						<option value="${orderT.KEY}" <c:if test="${sBox.orderType eq orderT.KEY}">selected</c:if>>${orderT.VALUE}</option>	
					</c:forEach>
				</select>
				<span class="noList">
					<label for="listNo">목록갯수</label>
					<select name="rowSize" id="listNo" class="listOrderOption">
						<c:forEach var="listO" items="${rowSizeTypeList}">
							<option value="${listO.KEY}" <c:if test="${sBox.rowSize eq listO.KEY}">selected</c:if>>${listO.VALUE}</option>	
						</c:forEach>
					</select>
				</span>
			</div>
		</div>
		</form>
			<div id="tableWrap" class=tabTable>
				<table class="basicTable">
				<caption>신청 내역 조회</caption><colgroup>
						<col width="8%" />
						<col width="7%" />
						<col width="7%" />
						<col width="7%" />
						<col width="7%" />
						<col width="12%" />
						<col width="10%" />
						<col width="7%" />
						<col width="12%" />
						<col width="7%" />
						<col width="8%" />
						<col width="8%" />
				</colgroup>
				<thead>
				<tr>
					<th scope="col">
						신청서코드
					</th>
					<th scope="col">
						신청일
					</th>
					<th scope="col">
						등록 예정일 
					</th>
					<th scope="col">
						등록사유 <br>발생일
					</th>
					<th scope="col">
						연체개시<br>일자
					</th>
					<th scope="col">
						신청회사명
					</th>
					<th scope="col">
						신청회사 <br>사업자번호
					</th>
					<th scope="col">
						신청인
					</th>
					<th scope="col">
						채무 회사명
					</th>
					<th scope="col">
						대표자 <br>(채무자)
					</th>
					<th scope="col">
						진행상태
					</th>
					<th scope="col">
						결제금액 <br>(원)
					</th>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="debt" items="${result.debtList}">
					<tr>
						<td>
							<a href="#none" class="linkDebt" data-debtApplId="${debt.DEBT_APPL_ID}"  data-custId="${debt.CUST_ID}"><strong>${debt.DOC_CD}</strong></a>
						</td>
						<td>
							${debt.APPL_DT_TIME}
						</td>
						<td>
							${debt.REG_DUE_DT_TIME}
						</td>
						<td>
							${debt.REG_RES_DT_TIME}
						</td>
						<td>
							${debt.OVER_ST_DT_TIME}
						</td>
						<td class="list_left">
							 <a href="#none" class="linkDebt" data-debtApplId="${debt.DEBT_APPL_ID}"  data-custId="${debt.CUST_ID}"><strong>${debt.COMP_NM}</strong></a>
						</td>
						<td>
							<a href="#none" class="linkDebt" data-debtApplId="${debt.DEBT_APPL_ID}"  data-custId="${debt.CUST_ID}"> <strong>${fn:substring(debt.COMP_NO,0,3)}-${fn:substring(debt.COMP_NO,3,5)}-${fn:substring(debt.COMP_NO,5,10)}</strong> </a>
						</td>
						<td>
							${debt.APPL_USR_NM}
						</td>
						<td class="list_left">
							<a href="#none" class="linkDebt" data-debtApplId="${debt.DEBT_APPL_ID}"  data-custId="${debt.CUST_ID}"><strong>${debt.CUST_NM}</strong></a>
						</td>
						<td>
							${debt.CUST_OWN_NM}
						</td>
						<td>
							${debtStatMap[debt.STAT].desc}
						</td>
						<td class="list_right">
							<c:if test="${debt.PAY_AMT eq null}">0</c:if>
							<fmt:formatNumber value="${debt.PAY_AMT}" /> 원
						</td>
					</tr>
				</c:forEach>	
				</tbody>
			</table>
		</div>
		<!-- 검색결과가 존재하지 않을경우 문구 설정함 -->
			<c:if test="${(fn:length(debtList) eq 0) or (debtList eq null) }">
				<div class="noSearch" align="center" style="padding-top: 5px; padding-bottom: 5px;">검색 결과가 없습니다.</div>
			</c:if>
		<div class="btmBtnWrap">
			<p class="moreList"><c:out value="${mobilePage}" escapeXml="false" /></p>
			<div class="paging">
				<c:out value="${pcPage}" escapeXml="false" />
			</div>
			<%-- <p class="rightBtn"><a id="btn_save_excel" href="#"><img src="${IMG}/common/btn_save.gif" alt="저장" /></a></p> --%>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->

</body>
</html>