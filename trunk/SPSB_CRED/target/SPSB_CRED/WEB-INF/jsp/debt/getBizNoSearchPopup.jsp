<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf" %>
<link rel="stylesheet" href="${CSS}/popup2.css"/>	
	
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript">
var isIng = false;
$(document).ready(function(){
	
	window.resizeTo(515, 505);
	
	//검색 결과값 로딩하는 함수
	$.fn.reloadUsrList = function($target, $listEl) {
		$htmlStr = '';
	 	$.each($listEl, function() {
			$htmlStr +=
				'<tr>' +
				'<td><input type="radio" name="mbrRadio" value="' + this.USR_ID + '"></td>' +
				'<td style="font-size: 12px;">' + this.USR_NM +
					'<input type="hidden" name="usrNm_' + this.USR_ID + '" id="usrNm_' + this.USR_ID + '" value="' + this.USR_NM + '"/>' +
				'</td>' +
				'<td style="font-size: 12px;">' + this.TEL_NO +
					'<input type="hidden" name="telNo_' + this.USR_ID + '" id="telNo_' + this.USR_ID+'" value="' + this.TEL_NO + '"/>' +
				'</td>' +
				'<td style="font-size: 12px;">' + this.EMAIL +
					'<input type="hidden" name="email_' + this.USR_ID + '" id="email_' + this.USR_ID + '" value="' + this.EMAIL + '"/>' +
					'<input type="hidden" name="loginId_' + this.USR_ID + '" id="loginId_' + this.USR_ID + '" value="' + this.LOGIN_USR_ID + '"/>' +
					'<input type="hidden" name="usrId_' + this.USR_ID + '" id="usrId_' + this.USR_ID + '" value="' + this.USR_ID+'"/>' +
					'<input type="hidden" name="compUsrId_' + this.USR_ID + '" id="compUsrId_' + this.USR_ID + '" value="' + this.COMP_USR_ID+'"/>' +
				'</td>';
				
	 	});
	 	
	 	$target.empty().append($htmlStr);
	 	
	 };
	 
	/*  $.fn.reloadNoDataList = function($target, $noDataEl) {
		 $htmlStr = '';
		 $target.empty().append($noDataEl);
	}; */
	
	//검색버튼 
	$("#btn_search").click(function(){
		if(isIng){
			alert("잠시만 기다려 주세요.");
			return false;
		}
		
		if($("#name").val().length == 0){
			alert('이름을 입력해 주세요');
			return false;
		}else{
			$.ajax({
				type : "POST",
				url : "${HOME}/config/getBizNoSearchPopupAjax.do",
				dataType : "json",
				data : $("#searchForm").serialize(),
				success : function(msg) {
					$result = msg.model.result;
					$searchUserNameList = $result.searchUserNameList;
					
					if($searchUserNameList.length > 0) {
					    $.fn.reloadUsrList($('#user_list_content tbody'), $searchUserNameList);	
			    	} else {	
			    		alert("일치하는 이름이 존재하지 않습니다.");
			    		/* $htmlStr = '<tr><td colspan="4"><div style="font-size: 12px;">결과가 없습니다.</div></td></tr>';
			    		$.fn.reloadNoDataList($('#user_list_content tbody'), $htmlStr); */
			    		
			    	}
					
				},
				error : function(xmlHttpRequest, textStatus, errorThrown) {
					alert("사용자 검색  중 오류가 발생하였습니다. [" + textStatus + "]");
				}
			});
		}
	});
	
	

});

function getCancel(){
	self.close();
}

function sendMember(){
	var radioSize = $("input[type='radio']:checked").length;;
	if(radioSize==0){
		alert("담당자를 선택해 주세요.");
	}else{
		var mbrId = $("input[name='mbrRadio']:checked").val();
		var usrNm = $('#usrNm_'+mbrId).val(); //고객명
		var usrId = $('#usrId_'+mbrId).val(); //스마트빌 개인순번
		var compUsrId = $('#compUsrId_'+mbrId).val(); //스마트빌 회사순번
		var loginId = $('#loginId_'+mbrId).val(); //로그인아이디
		var telNo = $('#telNo_'+mbrId).val(); //전화번호
		var telnos = telNo.split("-");
		var email = $('#email_'+mbrId).val(); //이메일주소
		var emails = email.split("@");
		
		opener.$.fn.modifyUserInfo(usrNm ,usrId, compUsrId, loginId, telnos[0], telnos[1], telnos[2], emails[0], emails[1], emails[1]);
		self.close();
	}
}


</script>
</head>
<body>
	<div class="popupWrap" >
		<h1><p class="content_tit">정보 등록 담당자 선택</p></h1>
		
		<div class="popupContents">
		<div id="viewInfo">
			<div class="noline_box">

				<table class="msr_pTbl01" summary="검색결과">
					<colgroup>
						<col width="30%">
						<col width="70%">
					</colgroup>
					<tbody>
					<tr>
						<th style="font-size: 12px; padding:19px 0 19px 0; font-weight:bold; background-color:#f9f9f9; text-align:center">이름</th>
						<td>
						<form id ="searchForm" name="searchForm">
							<input type="text" id="name" name="name" style="padding:4px 4px 3px 4px; border:1px solid #c4c4c4; width:300px;" placeholder="이름을 입력해주세요."  />
							<input type="hidden" id="mbr_id"  name="mbrId" class="ptblInput" value="${sBox.sessionMbrId}">
						</form>
						</td>			
					</tr>
				</tbody></table>
				
				<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_search">검색</a></div>
				</div>
				
				
				<div class="msr1_head" >			
					<div class="scroll_msr1">
					<table class="msr_pTbl02" summary="검색결과 고객선택" id="user_list_content">
						<colgroup>
							<col width="10%">
							<col width="20%">
							<col width="25%">
							<col width="45%">
						</colgroup>
						<tbody id="searchResult">
						<c:forEach var="list" items="${userResultList}">
							<tr>
								<td><input type="radio" name="mbrRadio" value="${list.id}"></td>
								<td style="font-size: 12px;">${list.name}
									<input type="hidden" name="usrNm_${list.id}" id="usrNm_${list.id}" value="${list.name}"/>
								</td>
								<td style="font-size: 12px;">${list.localNumber}
									<input type="hidden" name="telNo_${list.id}" id="telNo_${list.id}" value="${list.localNumber}"/>
								</td>
								<td style="font-size: 12px;">${list.urlId}
									<input type="hidden" name="email_${list.id}" id="email_${list.id}" value="${list.urlId}"/>
									<input type="hidden" name="loginId_${list.id}" id="loginId_${list.id}" value="${list.loginId}"/>
									<input type="hidden" name="usrId_${list.id}" id="usrId_${list.id}" value="${list.id}"/>
									<input type="hidden" name="compUsrId_${list.id}" id="compUsrId_${list.id}" value="${list.compUserId}"/>
								</td>				
								
							</tr>
						</c:forEach>
						</tbody>
					</table>
					</div>		
				</div>
				
				<!-- <div id="page_num_wrap">
					<div class="page_num">
						<ul>
							<li><input type="button" class="first" value="첫페이지"></li>
							<li><input type="button" class="pre" value="이전 페이지"></li>
							<li><a href="" class="on">1</a><span>l</span></li>
							<li><a href="">2</a><span>l</span></li>
							<li><a href="">3</a><span>l</span></li>
							<li><a href="">4</a><span>l</span></li>
							<li><a href="">5</a><span>l</span></li>
							<li><a href="">6</a><span>l</span></li>
							<li><a href="">7</a><span>l</span></li>
							<li><a href="">8</a><span>l</span></li>
							<li><a href="">9</a><span>l</span></li>
							<li><a href="">10</a></li>
							<li><input type="button" class="next" value="다음 페이지"></li>
							<li><input type="button" class="last" value="끝 페이지"></li>
						</ul>
					</div>
				</div> -->
				
				<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" onclick="javascript:getCancel();">취소</a></div>
				</div>
				<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" onclick="javascript:sendMember();">확인</a></div>
				</div>
				
			</div>
			
		</div>
		</div>
		
		
			
	</div>
</body>
</html>