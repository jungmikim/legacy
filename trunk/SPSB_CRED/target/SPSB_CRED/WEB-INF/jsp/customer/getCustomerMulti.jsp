<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 거래처 대량등록 파일 오류 수정 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}"/>
<c:set var="result" value="${result}"/>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>거래처 대량 등록 &gt; 거래처 관리 | 스마트채권 - 채권관리의 모든 것</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerMulti.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

	/**
	*	엑셀샘플파일 다운로드 함수
	*@author HWAJUNG SON
	*@since 2013.10.07
	*</pre>
	*/
	$("#btn_excel_down").click(function(){
		 $('#multiFrm').attr({'action':'${HOME}/customer/getCustomerMltAddFailListForExcel.do', 'method':'POST'}).submit();
	});
	
	/**
	*	엑셀샘플파일 다운로드 함수 (실패건수 클릭)
	*@author HWAJUNG SON
	*@since 2013.10.07
	*</pre>
	*/
	$(".bold").click(function(){
		$('#multiFrm').attr({'action':'${HOME}/customer/getCustomerMltAddFailListForExcel.do', 'method':'POST'}).submit();
	});
		
	/**
	*	파일 업로드 함수  
	*@author HWAJUNG SON
	*@since 2013.10.07
	*/	
	$("#cust_exc_file").change(function(){	
		$.fn.fileupload($('input[type=file]').val().toLowerCase());
	});
	

	
	
	/**
	*	거래처 삭제 함수
	*@author HWAJUNG SON
	*@since 2013.10.07
	*/
	$("#btn_delete").click(function(){
		if(confirm("등록한 거래처를 삭제하시겠습니까?")){
			$('#multiFrm').attr({'action':'${HOME}/customer/removeCustomerMulti.do', 'method':'POST'}).submit(); 
		}
	});
	
	/**
	*	거래처 등록 함수
	*@author HWAJUNG SON
	*@since 2013.10.07
	*/
	$("#btn_insert").click(function(){

		if($("#tempCountCheck").children().eq(1).html() == ("0건")){
			alert("등록할 거래처가 존재하지 않습니다.");
			return false;
		}
		
		var bTF = false;
		if($("#tempCountCheck").children().eq(2).find("a").length == 0){
			bTF = confirm("성공한 거래처를 등록하시겠습니까?");
		}else{
			bTF = confirm("실패한 건이 존재합니다.\n그래도 등록하시겠습니까?\n(등록하시면 성공한 건만 등록됩니다.)");
		}		
		
		if(bTF){
			$('#dvLoading').fadeIn(300);
			
			// 거래처 대량 등록 도중 "거래처 등록"버튼 또는 "삭제"버튼을 클릭할 수 있기 때문에 show / hide함
			$('#dvLoading').show();
			$("#btn_insert , #btn_delete").hide();
			
	 		setTimeout( function(){
				param = "mltId=" + $("#mlt_id").val() ;
				$.ajax({
					type : "POST", 
					url : "${HOME}/customer/addCustomerMulti.do",
					dataType : "json",
					data : param,
					async : false,
					success : function(json_result) {
							$('#dvLoading').fadeOut(3000);
							if(json_result.REPL_CD == "00000"){
								alert("총 "+json_result.totalCnt+"건의 거래처 등록이\n완료되었습니다.");
								location.href = '${HOME}/customer/getCustomerMultiHistoryList.do';
							}else {
								alert(utfDecode(json_result.REPL_MSG));
							}
							$("#btn_insert , #btn_delete").show();
						},
						error : function(xmlHttpRequest, textStatus, errorThrown){
							$('#dvLoading').fadeOut(3000);
							alert("거래처 대량등록 로직중 에러발생 [" + textStatus + "]");
							$("#btn_insert , #btn_delete").show();
						}
				});
		     }, 1000 );
		} 
	});
});
/**
 * 	엑셀 파일 검증 함수
 *@author HWAJUNG SON
 *@since 2013.10.04
 */
$.fn.validate = function($checkPrm){
	
	$result= true;

	 if( $.trim($checkPrm) != "") {
		 $fileReg =/(xls)$/;
			if(!$fileReg.test($checkPrm)){
				alert("엑셀업로드파일은 xls 형식만 가능합니다.");
				$result = false;	
			}
		}
	 return $result;
};
/**
 * 엑셀 업로드 함수
 *@author HWAJUNG SON
 *@since 2013 .10 .10
 */
 $.fn.fileupload = function($fileName){
	$result = true; 
	
	// 파일 형식 검증
	 if($.fn.validate(splitExtension($fileName))){
		// 10초 후 페이지 이동
 		setTimeout( function(){
			location.href = '${HOME}/customer/getCustomerMultiHistoryList.do';
	     }, 10000 );
 		// 엑셀 업로드
		 $('form').attr({'action':'${HOME}/customer/modifyCustomerMulti.do', 'method':'POST'})
			 .ajaxSubmit({
			 	dataType : "json",
				async : true,
				beforeSend: function() {
					$('#dvLoading').fadeIn(300);
			    },
			    success : function(msg){
					$('#dvLoading').fadeOut(3000);
					 if("00000" == msg.REPL_CD){
						location.href = '${HOME}/customer/getCustomerMultiForm.do';
					} else {
						alert(utfDecode(msg.REPL_MSG));
						location.href = '${HOME}/customer/getCustomerMultiForm.do';
					}	 				
				},
				error : function(xmlHttpRequest,textStatus,errorThrown) {
					$('#dvLoading').fadeOut(3000);
					location.href = '${HOME}/customer/getCustomerMultiForm.do';
				},
				uploadProgress: function(event, position, total, percentComplete) {
					$('#dvLoading').show();
				}
			}); 	
	 	}
	  return $result ;

};
</script>
</head>
<body>
<div id="accessibility">
	<a href="#content">본문바로가기</a>
</div>
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0103"/>
</jsp:include>
<%-- Top Area End --%>
 
<%-- contentArea --%>
<div class="contentArea sub02"><%-- menu별 이미지 class sub00 --%>
	<%-- content --%>
	<section id="content">
		<h2 class="tit_sub0103">거래처 대량 등록</h2>
		<p class="under_h2_sub0103">거래처 대량 등록 업로드 진행 상황에 대해 확인 및 관리를 할 수 있습니다.</p>
		<form id="multiFrm" name ="multiFrm" class="multiFrm" enctype="multipart/form-data" method="POST" action="${HOME}/customer/modifyCustomerMulti.do">
		<%-- hidden 값셋팅 --%>
		<input type="hidden" name="mltId" id="mlt_id" value="${result.MLT_ID}"/>
		<%-- loading img --%>
		<img src="${IMG}/common/loading.gif" id="dvLoading" />
		<div class="excel_down2">
			<dl>
				<dt>직접 다운로드 받아 리스트 파일 오류 수정하기</dt>
				<dd>오류사항이 표시된 엑셀을 다운로드 받으실 수 있습니다.</dd>
			</dl>
			<span><c:if test="${result.errCnt!=0}"> <a href="#" id="btn_excel_down">엑셀 다운로드</a></c:if></span>
			<p>
				<a href="#" id="btn_exc_upload">엑셀 업로드</a>
				<input type="file" name="custExcFile" id="cust_exc_file" class="custExcFile">
				
			</p>
		</div>

		<p class="totalNews">파일명 : ${result.FILE_NM}</p>

		<table class="table02" summary="거래처 등록건수를 보여주는 테이블로 총건수, 성공건수, 실패건수로 구성되어있습니다.">
			<caption>거래처 등록건수</caption>
			<colgroup>
				<col width="33%" />
				<col width="33%" />
				<col width="33%" />
			</colgroup>
			<tr>
				<th scope="col">총 건 수</th>
				<th scope="col">성공 건 수</th>
				<th scope="col">실패 건 수</th>
			</tr>
			<tr id ="tempCountCheck">
				<td>${result.totalCnt}건</td>
				<td>${result.sccCnt}건</td>
				<td>
					<c:choose>
					<c:when test="${result.errCnt!=0}">
						<a href="#" class="bold"> ${result.errCnt}건</a>
					</c:when>
					<c:otherwise>
						${result.errCnt}건
					</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
		</form>
        <div class="btmBtn">
          <div id="bottom">
              <a href="#" class="btnType01" id="btn_insert">거래처 등록</a>
              <a href="#" class="btnType01" id="btn_delete">삭제</a>
          </div>
        </div>	
               
	</section>
	<%-- //content --%>
</div>
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>

</body>
</html>