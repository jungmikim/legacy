<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 회사명 단건 등록 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />
<c:set var="result" value="${result}" />
<c:set var="commonCodeBox" value="${result.commonCodeBox}" />
<c:set var="phoneCodeList" value="${commonCodeBox.phoneCodeList}" />
<c:set var="emailCodeList" value="${commonCodeBox.emailCodeList}" />
<c:set var="cellPhoneCodeList" value="${commonCodeBox.cellPhoneCodeList}" />
<c:set var="payTermTypeList" value="${commonCodeBox.payTermTypeList}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>회사명 관리 &gt; 회사명 단 건 등록 | 기술료채권 - 기술료관리의 모든 것</title>
<%@ include file="../common/meta.jspf" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%-- TODO 운영에서는 삭제  --%>
<meta http-equiv="Cache-Control" content="No-Cache"/>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/customer/getCustomerForm.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/**
		 * <pre>
		 *   회사명 유형 체크박스 선택 함수
		 *    FORM 양식 전환 함수
		 * </pre>
		 * @author Jong Pil Kim
		 * @since 2013. 08. 26.
		 */
		 $('.chk_cust_type').change(function(){
			 if($(this).is('#type_company')) {
				 $('.infoView > dl').find('.corpFrm').show();
			 } else {
				 $('.infoView > dl').find('.corpFrm').hide();
			 }
			 $('#cust_type').val($(this).val());
		 });
		 
		 
		/**
		* <pre>
		*   우편번호 앞자리, 뒷자리 클릭 이벤트시 검색 팝업창을 띄움
		* </pre>
		* @author sungrangkong
		* @since 2014. 07. 21.
		*/
		$(document).on('click','#post_no1,#post_no2',function(e){
			open_post();
		});
		 
		/**
		 * <pre>
		 *   회사명 등록 함수
		 *     등록 버튼 클릭 이벤트 함수
		 * </pre>
		 * @author Jong Pil Kim
		 * @since 2013. 08. 26.
		 */
		 $('#btn_add_customer').click(function(){
			 if($.fn.validate() && confirm('입력하신 회사명 정보를 등록하시겠습니까?')) {
				 $('.numericMoney').each(function(){
					 $(this).val(replaceAll($(this).val(),',',''));
				 });
				 $('form').attr({'action':'${HOME}/customer/addCustomer.do', 'method':'POST'}).submit(); 
			 }
		 });
		 
		/**
		 * <pre>
		 *   회사명 등록 취소 함수
		 *     취소 버튼 클릭 이벤트 함수
		 * </pre>
		 * @author Jong Pil Kim
		 * @since 2013. 09. 02.
		 */
		 $('#btn_cancel_customer').click(function(){
			 if(confirm('회사명 등록을 취소하시겠습니까?'))
				 location.href = '${HOME}/customer/getCustomerList.do';
		 });
		 
		 /**
		 * <pre>
		 *   결제 유형 중 특정 option을 선택했을 경우
		 *    결제 기준일을 필수값으로 변경시키고 
		 *    아니면 결제 기준일을 hidden 시키는 함수
		 * </pre>
		 * @author Jong Pil Kim
		 * @since 2013. 08. 26.
		 */
		 $('.payTermType').live('change',function(e){
			 if($(this).find('option:selected').hasClass('esentialSelect')){
				 $(this).next().addClass('required termType').attr('readonly', false);
				 $(this).next().show();
				 $(this).next().next().show();
			 } else {
				 $(this).next().removeClass('required termType').attr('readonly', true).val('');
				 $(this).next().hide();
				 $(this).next().next().hide();
			 }
		 });
		 
		 /**
		 * <pre>
		 *   이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직
		 * </pre>
		 * @author Jong Pil Kim
		 * @since 2013. 08. 26.
		 */
		 $('#select_email').live('change',function(e){
			 if($.trim($(this).find('option:selected').val()) == ''){
				 $(this).prev().attr('readonly', false);
			 } else {
				 $(this).prev().attr('readonly', true);
			 }
			 $(this).prev().val($(this).val());
		 });
		 
		 /**
		 * <pre>
		 *   사업자 등록 중복확인 체크 Event 
		 * </pre>
		 * @author sungrangkong
		 * @since 2013. 08. 27.
		 */
		 $('#btn_checkCustNo').live('click',function(e){
			 url = '${HOME}/customer/getCheckDuplicationCustomerForm.do?custType=';
			 url += ($('#add_customer_frm').find('#companyCustomerArea').length>0)? 'C' : 'I';
			 winOpenPopup(url,'getCheckDuplicationCustomerForm',325,225,'no');
		 });
		 
		 /**
	      * <pre>
	      *   여신한도, 초기미수금 클릭시 숫자 ,를 제거하는 함수 
	      * </pre>
	      * @author Jong Pil Kim
	      * @since 2013. 08. 27.
	      */ 
	      $('.numericMoney').live({
		      focus: function() {
			      $(this).val(replaceAll($(this).val(), ',', ''));
		      },
		      blur: function() {
			      $(this).val(addComma($(this).val()));
		      }
	      });
	      
    /**
	 * <pre>
	 *   사업자등록  input box 클릭시 팝업 호출  
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 27.
	 */
	 $('#cust_no').live('click',function(e){
		 $('#btn_checkCustNo').trigger('click');
	 });

	});

/**
 * <pre>
 *   폼 전송 파라미터 검증 중 경고창 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
 */
 $.fn.validateResultAndAlert = function($el, $message){
	 alert($message);
	 $el.focus();
	 return false;
 }; 
	
/**
 * <pre>
 *    폼 전송 파라미터 검증 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 27
 */
 $.fn.validate = function(){
	
	$result = true;
	 
	$formEl = $('form').find('input');
	$.each($formEl, function(){
		
		// 필수 공백 검증
		if($(this).hasClass('required')){
			if($.trim($(this).val())=='') {
				$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + ' 항목은 필수 입력 사항 입니다.');
				return $result; 
			}
		}
		
		if($.trim($(this).val()) != '') {

			// 숫자 검증
			if($(this).hasClass('numeric')){
				if(!typeCheck('numCheck',$.trim($(this).val()))) {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
					return $result;
				}
				
				if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
					return $result;
				}
			}
			
			// 전화번호, 휴대폰번호, 팩스번호 자릿수 검증
			if($(this).hasClass('phoneNo')){
				if($(this).hasClass('midNo') && $(this).val().length < 3){
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 3자리보다 커야 합니다.');
					return $result;
				}
				if($(this).hasClass('lastNo') && $(this).val().length != 4){
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 4자리여야 합니다.');
					return $result;
				}
				if($(this).siblings('.phoneNo').val().length < 3) {
					$result = $.fn.validateResultAndAlert($(this).siblings('.phoneNo'), $(this).siblings('.phoneNo').attr('title') + '를 확인해주세요.');
					return $result;
				}
			}
			
			// 화폐 검증
			if($(this).hasClass('numericMoney')){
				if(!(typeCheck('numCheck',replaceAll($.trim($(this).val()),',','')))) {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
					return $result;
				}	
				
				if($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
					return $result;
				}
			}
		}
		// 반복문 정지
		if(!$result) return $result;
	});
	
	// 스크립트 정지
	if(!$result) return $result;
	
	// 이메일 검증[문자열 형태 검증]
	$emailEl1 = $('form').find('#email1');
	$emailEl2 = $('form').find('#email2');
	if($.trim($emailEl1.val()) != '' || $.trim($emailEl2.val()) != ''){
		if(!typeCheck('emailCheck', $.trim($emailEl1.val()) + '@' + $.trim($emailEl2.val()))){
			alert('이메일을 확인해 주세요.');
			$result = false;
			return $result;
		}	
	}
	
	// 법인 등록번호 검증[문자열 형태 검증]
	$corpNoEl = $('form').find('#corp_no');
	if($('#type_company').is(':checked') && $.trim($corpNoEl.val()) != ''){
		if(!typeCheck('corpNoCheck',replaceAll($.trim($corpNoEl.val()),'-',''))){
			$result = $.fn.validateResultAndAlert($corpNoEl,$corpNoEl.attr('title') + '을(를) 확인해주세요.');
			return $result;
		} 
	}
	
	// 결제 조건(일수) 검증 [대소비교]
	$initPayTermType = $('form').find('#init_pay_term_type');
	$futurePayTermType = $('form').find('#future_pay_term_type');
	if($initPayTermType.next().hasClass('termType')){
		if ($.trim($initPayTermType.next().val()) > 31 || $.trim($initPayTermType.next().val()) <= 0) {
			$result = $.fn.validateResultAndAlert($initPayTermType.next(),$initPayTermType.next().attr('title') + '은(는)  1~31 사이의 값이어야 합니다.');
			return $result;
		}
	}
	if($futurePayTermType.next().hasClass('termType')){
		if ($.trim($futurePayTermType.next().val()) > 31 || $.trim($futurePayTermType.next().val()) <= 0) {
			$result = $.fn.validateResultAndAlert($futurePayTermType.next(),$futurePayTermType.next().attr('title') + '은(는)  1~31 사이의 값이어야 합니다.');
			return $result;
		}
	}
	
	// 결제 조건은 선택 사항이므로 아예 입력하지 않거나 모두 입력해야 함
	if($initPayTermType.val() != 'NONE' || $futurePayTermType.val() != 'NONE' || $('#taxDate').val() != ''){
		if($initPayTermType.val() == 'NONE' || $futurePayTermType.val() == 'NONE' || $('#taxDate').val() == ''){
			alert('결제 조건을 설정하시려면 결제 조건 항목을 모두 채우셔야 합니다.');
			$result = false;
			return $result;
		}
	}
	 
	return $result;
	
 };
 
	// 우편번호 검색 팝업창
	function open_post(){ 
		open_win('${HOME}/zip/zipSearchPopup.do');
	 } 
	
</script>
</head>
<body>
	<div id="accessibility">
		<a href="#content">본문바로가기</a>
	</div>

	<%--  Top Area Start --%>
	<jsp:include page="../common/top.jsp" flush="true">
		<jsp:param name="gnb" value="0302" />
	</jsp:include>
	<%-- Top Area End --%>

	<%-- contentArea --%>
	<div class="contentArea sub01">
		<%-- menu별 이미지 class sub00 --%>
		<%-- content --%>
		<section id="content">
			<h2 class="tit_sub0102">회사명 단 건 등록</h2>
			<p class="under_h2_sub0102">회사명를 등록하셔야 채권관리를 보다 효율적으로 하실 수 있습니다.</p>

			<div class="customerTypeArea" style="height:30px;"></div>
			
			<form name="addCustomerFrm" id="add_customer_frm">
			
			<%-- FORM HIDDEN AREA START --%>
			<input type="hidden" name="custType" id="cust_type" value="${sBox.custType}" />
			<%-- FORM HIDDEN AREA END --%>
			
			<%-- 법인 사업자 회사명 영역 시작 --%>
			<div id="companyCustomerArea">
				<h3 class="first">
					<img src="${IMG}/common/h3_customer_info.gif" alt="회사명 정보" />
				</h3>
				<div class="infoView">
					<dl>
						<%-- <dt class="first">
							<span>회사명 유형<em>*</em></span>
						</dt>
						<dd class="first">
							<span>
								<input type="radio" class="chk_cust_type" name="chkCustType" id="type_company" value="C" <c:if test="${sBox.custType ne 'I' }">checked</c:if> style="width: auto;" /> 
								<label for="type_company">법인 사업자</label> 
								<input type="radio" class="chk_cust_type" name="chkCustType" id="type_personal" value="I" <c:if test="${sBox.custType eq 'I' }">checked</c:if> style="width: auto;" /> 
								<label for="type_personal">개인 사업자</label>
							</span>
						</dd> --%>
						<dt>
							<span>회사명<em>*</em></span>
						</dt>
						<dd>
							<span> 
								<input type="text" name="custNm" id="cust_nm" class="txtInput required" title="회사명" maxlength="70"/>
							</span>
						</dd>
						<dt>
							<span>사업자등록번호<em>*</em></span>
						</dt>
						<dd>
							<span> 
								<input type="text" name="custNo" id="cust_no" class="txtInput required numeric" title="사업자등록번호" readonly="readonly"/>
								<a id="btn_checkCustNo" href="#"><img src="${IMG}/popup/btn_overlapcheck.gif" class="imgBtn"></a>
							</span>
						</dd>
						<dt>
							<span>대표자</span>
						</dt>
						<dd>
							<span> 
								<input type="text" name="ownNm" id="own_nm" class="txtInput" title="대표자" maxlength="30"/>
							</span>
						</dd>
						<dt class="longer">
							<span>이메일</span>
						</dt>
						<dd class="longer">
							<span> 
								<input type="text" class="txtInput wno" name="eMail1" id="email1" title="이메일 주소" maxlength="20"/>@<input type="text" class="txtInput wno" name="eMail2" id="email2" title="이메일 도메인" maxlength="19"/> 
								<select id="select_email" title="이메일 도메인 선택">
									<c:forEach var="companyEmailCodeList" items="${emailCodeList}">
										<option value="${companyEmailCodeList}">
											<c:choose>
												<c:when test="${not empty companyEmailCodeList}">${companyEmailCodeList}</c:when>
												<c:otherwise>직접입력</c:otherwise>
											</c:choose>
										</option>
									</c:forEach>
								</select>
							</span>
						</dd>
						<dt class="first corpFrm">
							<span><label for="corp_no">법인등록번호</label></span>
						</dt>
						<dd class="first corpFrm">
							<span> 
								<input type="text" name="corpNo" id="corp_no" class="txtInput numeric" title="법인등록번호" maxlength="13"/>
							</span>
						</dd>
						<dt class="first address">
							<span>회사주소</span>
						</dt>
						<dd class="first address">
							<span> 
								<input type="text" name="postNo1" id="post_no1" class="txtInput wsmall numeric unsigned" title="우편번호[앞자리]" readonly="readonly" maxlength="3"/> - 
								<input type="text" name="postNo2" id="post_no2" class="txtInput wsmall numeric unsigned" title="우편번호[뒷자리]" readonly="readonly" maxlength="3"/> 

								<a id="btn_search_post_no" href="#"><img src="${IMG}/common/btn_s_zipcode.gif" alt="우편번호" class="imgBtn" onClick="javascript:open_post()"/></a>
								<%--  <a id="btn_search_new_post_no" href="#"><img src="${IMG}/common/btn_s_new_address.gif" alt="새주소 검색 바로가기" class="imgBtn" /></a> --%>
								<input type="text" name="custAddr1" id="cust_addr_1" class="txtInput address1" title="주소" readonly="readonly" maxlength="75"/> 
								<input type="text" name="custAddr2" id="cust_addr_2" class="txtInput address2" title="상세주소" maxlength="75"/>
							</span>
						</dd>
						<dt>
							<span><label for="biz_type">업종</label></span>
						</dt>
						<dd>
							<span> 
								<input type="text" name="bizType" id="biz_type" class="txtInput" title="업종" maxlength="40"/>
							</span>
						</dd>
						<dt>
							<span><label for="biz_cond">업태</label></span>
						</dt>
						<dd>
							<span> 
								<input type="text" name="bizCond" id="bizCond" class="txtInput" title="업태" maxlength="40"/>
							</span>
						</dd>
						<dt>
							<span>전화</span>
						</dt>
						<dd>
							<span> 
								<select id="selectedTelNo" name="telNo1" title="전화번호 국번">
									<c:forEach var="companyPhoneCodeList" items="${phoneCodeList}">
										<option value="${companyPhoneCodeList}">${companyPhoneCodeList}</option>
									</c:forEach>
								</select> - 
								<input type="text" name="telNo2" id="tel_no2" class="txtInput wsmall numeric unsigned phoneNo midNo" title="전화번호[앞자리]" maxlength="4"/> - 
								<input type="text" name="telNo3" id="tel_no3" class="txtInput wsmall numeric unsigned phoneNo lastNo" title="전화번호[뒷자리]" maxlength="4"/>
							</span>
						</dd>
						<dt>
							<span>팩스</span>
						</dt>
						<dd>
							<span> 
								<select id="selectedFaxNo" name="faxNo1" title="팩스번호 국번">
									<c:forEach var="companyFaxCodeList" items="${phoneCodeList}">
										<option value="${companyFaxCodeList}">${companyFaxCodeList}</option>
									</c:forEach>
								</select> - 
								<input type="text" name="faxNo2" id="fax_no2" class="txtInput wsmall numeric unsigned phoneNo midNo" title="팩스번호[앞자리]" maxlength="4"/> - 
								<input type="text" name="faxNo3" id="fax_no3" class="txtInput wsmall numeric unsigned phoneNo lastNo" title="팩스번호[뒷자리]" maxlength="4"/>
							</span>
						</dd>
						<dt class="first">
							<span>휴대폰</span>
						</dt>
						<dd class="first">
							<span> 
								<select id="selectedMbNo" name="mbNo1" title="휴대폰번호 국번">
									<c:forEach var="companyMobileCodeList" items="${cellPhoneCodeList}">
										<option value="${companyMobileCodeList}">${companyMobileCodeList}</option>
									</c:forEach>
								</select> - 
								<input type="text" name="mbNo2" id="mb_no2" class="txtInput wsmall numeric unsigned phoneNo midNo" title="휴대폰번호[앞자리]" maxlength="4"/> - 
								<input type="text" name="mbNo3" id="mb_no3" class="txtInput wsmall numeric unsigned phoneNo lastNo" title="휴대폰번호[뒷자리]" maxlength="4"/>
							</span>
						</dd>
					</dl>
				</div>
				<div class="btmBtn">
					<em>*</em> 항목은 필수 입력 사항입니다.
				</div>
				<%-- <h3 class="first">
					<img src="${IMG}/common/h3_settlement_condition.gif" alt="결제 조건" /><em></em>
				</h3>
	
				<div class="tableUlWrap">
					<ul>
						<li style="background:#FFFFFF !important;">
							<dl>
								<dd>
									초기 결제조건 : 
									<select id="init_pay_term_type" name="initPayTermType" class="payTermType termType" title="초기 결제조건">
										<c:forEach var="initPayTermTypeList" items="${payTermTypeList}">
											<option value="${initPayTermTypeList.KEY}" <c:if test="${initPayTermTypeList.DAYESSENTIAL eq 'true'}">class="esentialSelect"</c:if>>${initPayTermTypeList.VALUE}</option>
										</c:forEach>
									</select> 
									<input type="text" class="payTermDay numeric unsigned day" name="initPayTermDay" id="init_pay_term_day" title="초기 결제조건 일수" style="position: static;display:none;" size="6" readonly="readonly" maxlength="2"/><span style="display:none;">일</span> <em>|</em> 
									차후 결제조건 : 
									<select id="future_pay_term_type" name="futurePayTermType" class="payTermType termType" title="차후 결제조건">
										<c:forEach var="futurePayTermTypeList" items="${payTermTypeList}">
											<option value="${futurePayTermTypeList.KEY}" <c:if test="${futurePayTermTypeList.DAYESSENTIAL eq 'true'}">class="esentialSelect"</c:if>>${futurePayTermTypeList.VALUE}</option>
										</c:forEach>
									</select> 
									<input type="text" class="payTermDay numeric unsigned day" id="future_pay_term_day" name="futurePayTermDay" title="차후 결제조건 일수" style="position: static;display:none;" size="6" readonly="readonly"/><span style="display:none;">일</span> <em>|</em> 
									결제조건 적용 날짜 : <input type="text" id="taxDate" name="payTermDate" class="taxDate termType" title="결제조건 적용 날짜" readonly="readonly"/>
								</dd>
							</dl>
						</li>
					</ul>
				</div> --%>
			</div>
			<%-- 법인 사업자 회사명 영역 종료 --%>
			</form>
			
			<div class="btmBtn">
				<div id="bottom">
					<c:if test="${sBox.isSessionCustAddGrn eq true}">
						<a id="btn_add_customer" class="btnType01" href="#">등록</a>
					</c:if>
					<a id="btn_cancel_customer" class="btnType01" href="#">취소</a>
				</div>
			</div>
		</section>

		<%-- //content --%>
	</div>
	<%-- //contentArea --%>


	<%--  footer Area Start --%>
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<%-- footer Area End --%>
</body>
</html>