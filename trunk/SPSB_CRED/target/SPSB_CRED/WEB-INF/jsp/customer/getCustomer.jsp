<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${result.REPL_CD eq '10115' }">
	<script>
		alert('이미 삭제된 거래처 입니다.');
		location.href = '${HOME}/customer/getCustomerList.do';
	</script>
</c:if>

<%-- 거래처 단건 등록 및 요청 파라미터 변수 세팅 --%>
<c:set var="sBox" value="${sBox}" />

<c:set var="result" value="${result}" />
<c:set var="commonCode" value="${result.commonCodeBox}" />
<c:set var="customer" value="${result.customerBox}" />
<c:set var="pcPage" value="${result.pcPage}" />

<c:set var="payTermList" value="${result.payTermList}" />
<c:set var="customerCreditHistoryList"	value="${result.customerCreditHistoryList}" />
<c:set var="historyPcPage" value="${result.historyPcPage}" />

<c:set var="customerPlanListTotalCount"	value="${result.customerPlanListTotalCount}" />
<c:set var="customerPlanList" value="${result.customerPlanList}" />
<c:set var="customerPlanListPcPage"	value="${result.customerPlanListPcPage}" />

<c:set var="customerPrsListTotalCount"	value="${result.customerPrsListTotalCount}" />
<c:set var="customerPrsList" value="${result.customerPrsList}" />
<c:set var="customerPrsListPcPage" 	value="${result.customerPrsListPcPage}" />

<c:set var="customerColListTotalCount" 	value="${result.customerColListTotalCount}" />
<c:set var="customerColList" value="${result.customerColList}" />
<c:set var="customerColListPcPage" 	value="${result.customerColListPcPage}" />

<c:set var="faxCodeList" value="${commonCode.phoneCodeList}" />
<c:set var="phoneCodeList" value="${commonCode.phoneCodeList}" />
<c:set var="emailCodeList" value="${commonCode.emailCodeList}" />
<c:set var="cellPhoneCodeList" value="${commonCode.cellPhoneCodeList}" />
<c:set var="payTermTypeList" value="${commonCode.payTermTypeList}" />

<c:set var="debtProcess" value="${result.debtProcess}" />

<%-- 채무불이행 현재 상태 이력 --%>
<c:set var="debtBox" value="${result.debtBox}" />
<%-- 채무불이행 정보 --%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="ko">
<!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Cache-Control" content="No-Cache" />
<link rel="stylesheet" href="${CSS}/common.css" />
<%-- <link rel="stylesheet" href="${CSS}/customer/getCustomer.css" /> --%>
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script>
<script type="text/javascript" src="${JS}/jquery/jquery.word-break-keep-all.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	//신용정보 유료회원인 경우 신용정보 이력테이블 유료정보 출력
	if ("${poYn}" == "Y") {
		$('#custCrdPdHistTable').show();
	} else {
		$('#custCrdPdHistTable').hide();
	}
	<%-- 거래처 정보 수정 모드 숨김 --%>
	$('.modifyMode').hide();
	$('.modifyCreditMode').hide();
	
	$('.modify').hide();
	
	$("#btn_modify").click(function(e){
		$('.readOnly').hide();
		$('.modify').show();
	});	

	$("#btn_cancel").click(function(e){
		if (confirm("거래처 정보 수정을 취소하시겠습니까?")) {
			$('.modify').hide();
			$('.readOnly').show();
			location.href="${HOME}/customer/getCustomer.do?custNo="+$('#cust_no').val()+"&custId="+$('#cust_id').val();			
		}
	});	
	
	$("#btn_save").click(function(e){
		if ($.fn.validate() && confirm('입력하신 거래처 정보로 수정하시겠습니까?')) {
			$('form').attr({'action':'${HOME}/customer/modifyCustomer.do', 'method':'POST'}).submit();	
		}
	});		


	/**
	 * <pre>
	 *   결제 조건 추가 중 특정 option을 선택했을 경우
	 *    결제 기준일을 필수값으로 변경시키는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('.payTermType').live('change',function(e) {
		if ($(this).find('option:selected').hasClass('esentialSelect')) {
			$(this).next().addClass('required termType').attr('readonly', false);
			$(this).next().show();
			$(this).next().next().show();
		} else {
			$(this).next().removeClass('required termType').attr('readonly', true).val('');
			$(this).next().hide();
			$(this).next().next().hide();
		}
	});

	 /**
	 * <pre>
	 *   우편번호 앞자리, 뒷자리 클릭 이벤트시 검색 팝업창을 띄움
	 * </pre>
	 * @author sungrangkong
	 * @since 2014. 07. 21.
	 */
	/* $(document).on('click', '#post_no1,#post_no2',function(e) {
		open_post();
	}); */


	 /**
	 * <pre>
	 *   수정 완료 버튼 클릭 이벤트 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27.
	 */
	$('#btn_modify_complete').click(function(e) {
		if ($.fn.validate()&& confirm('입력하신 거래처 정보로 수정하시겠습니까?')) {
			$('#customer_temp_area').empty();
			$.fn.modifyCustomerInfo();
		}
	});



	 /**
	 * <pre>
	 *   거래처 리스트로 이동하는 함수 
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 29.
	 */
	$('#btn_get_list').click(function() {
		location.href = '${HOME}/customer/getCustomerList.do';
	});
	

	 /**
	 * <pre>
	 *   채무 불이행 등록 버튼 클릭 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 05. 01.
	 */
	$(document).on('click', '#btn_nonpayment_regist', function() {
		location.href = "${HOME}/debt/getDebtForm.do?custId=" + $("#cust_id_debt").val();
	});

	 /**
	 * <pre>
	 *   채무 불이행 상세보기 
	 * </pre>
	 * @author KIM GA EUN
	 * @since 2014. 05. 01.
	 */	 
	 $('#btnDebtOngoingDetail').click(function() {
		 location.href = "${HOME}/debt/getDebtOngoingInquiry.do?debtApplId=" + $('#debt_appl_id').val();
	 });	 
	 
	/*
	 * <pre>
	 *  신용정보 확인하기 버튼 클릭 함수
	 * </pre>
	 * @author JUNG MI KIM
	 * @since 2015.06.22
	 */
	$('.crdInfo').click(function() {
		var custId = $('#cust_no').val();
		var ewCd = $('#ew_cd').val();
		var errorMessage = "신용정보를 조회할수 있는 필수값이 존재 하지 않습니다.\n관리자에게 문의해주세요.";
		 if('${sBox.isSessionCustEwDetailGrnt}'== 'true'){
			if($.trim(custId).length != 0){
	  			if($.trim(ewCd).length != 0){
	  				var compId= "${sBox.compId}";
	  				
	  				window.open("${HOME}/customer/getEwReportForKED.do?compId="+compId+"&bizNo="+custId, "popup", "width=1000, height=700, resizable=no, scrollbars=yes, menubar=no, toobar=no, status=no, location=no, ");
	  			}else{
				 	alert(errorMessage);
				}
	  		}else{
			 	alert(errorMessage);
			}
		 }else{
			 alert("조기경보 상세 권한이 존재하지 않습니다.\n관리자에게 문의하시기 바랍니다.");
		 }
		  
	});

	 /*
	 * <pre>
	 *  거래처 정보 인쇄 버튼 Click Event
	 * </pre>
	 * @author sungrangkong
	 * @since 2014.06.08
	 */
	$(document).on('click', '.btn_custInfoPrint', function(e) {

		// [1] print 모드를 킴	
		$("#use_print").val("Y");

		// [2] 거래처 정보 탭으로 이동함
		$("#cust_info").trigger('click');

	});

	/*
	* <pre>
	* 이메일 SELECT BOX CHANGE EVENT
	* </pre>
	* @author sungrangkong
	* @since 2014.04.18
	*/
	$('#select_email').change(function() {
		// 직접입력을 제외한 나머지의 경우 이메일 뒷부분 input창은 readonly 변경
		if($(this).val() == "") {
			$('#email2').val("");
			$('#email2').attr("readonly", false);
		} else {
			$('#email2').val($(this).val());
			$('#email2').attr("readonly", true);
		}
	});	 

});
<%-- //////////////////////////////  함수 영역 시작  ////////////////////////////// --%>
	/**
	 * <pre> 
	 *   <li> 태그 리스트 백그라운드 CSS 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 28
	 *
	 */
	$.fn.setBackgroundEvenAndOdd = function($targetEl, $evenColor, $oddColor) {
		$targetEl.find('li:even').css('background-color', $evenColor);
		$targetEl.find('li:odd').css('background-color', $oddColor);
	};

	/**
	 * <pre>
	 *   modifyMode 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	$.fn.convertModifyMode = function() {
		$('.notModifyMode').hide();
		$('.modifyMode').show();

		// 필수값 * 표기함
		$('#companyCustomerArea .infoView em').show();
	};

	/**
	 * <pre>
	 *   notModify Mode 전환 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27 
	 */
	$.fn.convertNotModifyMode = function() {
		$('.notModifyMode').show();
		$('.modifyMode').hide();

		// 필수값 * 표기 숨김
		$('#companyCustomerArea .infoView em').hide();
	};

	/**
	 * <pre>
	 *   폼 전송 파라미터 검증 중 경고창 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 * @param $el : 검증 대상 엘리먼트, $message : 경고창에 띄울 메세지
	 */
	$.fn.validateResultAndAlert = function($el, $message) {
		alert($message);
		$el.focus();
		return false;
	};

	/**
	 * <pre>
	 *    폼 전송 파라미터 검증 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 08. 27
	 */
	$.fn.validate = function() {
		if($('#custNm').val()==""){
			alert("거래처명을 입력해주세요");
			$('#custNm').focus();
			return false;
		}
			
		return true;
/* 		$result = true;
		
 		$formEl = $('form').find('input');
		$.each($formEl, function() {

			// 필수 공백 검증
			if ($(this).hasClass('required')) {
				if ($.trim($(this).val()) == '') {
					$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '을(를) 입력해주세요.');
					return $result;
				}
			}

			if ($.trim($(this).val()) != '') {

				// 숫자 검증
				if ($(this).hasClass('numeric')) {
					if (!typeCheck('numCheck', $.trim($(this).val()))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자만 입력 가능합니다.');
						return $result;
					}

					if ($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}

				// 전화번호, 휴대폰번호, 팩스번호 자릿수 검증
				if ($(this).hasClass('phoneNo')) {
					if ($(this).hasClass('midNo') && $(this).val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 3자리보다 커야 합니다.');
						return $result;
					}
					if ($(this).hasClass('lastNo') && $(this).val().length != 4) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '의 자릿수는 4자리여야 합니다.');
						return $result;
					}
					if ($(this).siblings('.phoneNo').val().length < 3) {
						$result = $.fn.validateResultAndAlert($(this).siblings('.phoneNo'), $(this).siblings('.phoneNo').attr('title') + '를 확인해주세요.');
						return $result;
					}
				}

				// 화폐 검증
				if ($(this).hasClass('numericMoney')) {
					if (!(typeCheck('numCheck', replaceAll($.trim($(this).val()), ',', '')))) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 숫자(화폐)만 입력 가능합니다.');
						return $result;
					}

					if ($(this).hasClass('unsigned') && parseInt($.trim($(this).val()), 10) < 0) {
						$result = $.fn.validateResultAndAlert($(this), $(this).attr('title') + '은(는) 0보다 커야 합니다.');
						return $result;
					}
				}
			}
			// 반복문 정지
			if (!$result)
				return $result;
		});

		// 스크립트 정지
		if (!$result)
			return $result;
		
		// 법인 등록번호 검증[문자열 형태 검증]
		$corpNoEl = $('form').find('#corpNo');
		if ($.trim($corpNoEl.val()) != '') {
			if (!typeCheck('corpNoCheck', replaceAll($.trim($corpNoEl.val()), '-', ''))) {
				$result = $.fn.validateResultAndAlert($corpNoEl, $corpNoEl.attr('title') + '을(를) 확인해주세요.');
				return $result;
			}
		}
		
		// 이메일 검증[문자열 형태 검증]
		$emailEl1 = $('form').find('#email1');
		$emailEl2 = $('form').find('#email2');
		if ($.trim($emailEl1.val()) != '' || $.trim($emailEl2.val()) != '') {
			if (!typeCheck('emailCheck', $.trim($emailEl1.val()) + '@' + $.trim($emailEl2.val()))) {
				alert('이메일을 확인해 주세요.');
				$result = false;
				return $result;
			}
		}

		return $result; */
	};



	/**
	 * <pre> 
	 *   신용정보 이력 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 06
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCrdHistoryList = function($target, $listEl) {

		$htmlStr = '';
		$payName = '';

		$.each($listEl, function() {

			if (this.PAY_TYPE == 'F') {
				$payName = '무료';
			} else {
				$payName = '유료';
			}
			$htmlStr += 
						'<li>'
						+ '<dl>'
							+ '<dd class="border">'
								+ (this.PAY_TYPE != null ? ('구분　:　 ' + $payName): '')
								+ (this.EW_DT != null ? (' <em>|</em> 날짜　:　' + this.EW_DT.substring(0, 4) + "-"
										+ this.EW_DT.substring(4, 6) + "-" + this.EW_DT.substring(6, 10)): '')
								+ (this.CRD_TYPE_VALUE != null ? (' <em>|</em> EW등급 : ' + this.CRD_TYPE_VALUE): '');
								if (this.CUR_PAY_CNT > 0) {
									$htmlStr += 
										(this.KFB_ARR_YN != null ? (' <em>|</em> 금융권단기연체 은행연 : ' + this.KFB_ARR_YN): '')
										+ (this.KED_GEN_ARR_YN != null ? (' <em>|</em> 금융권단기연체 KED일반 : ' + this.KED_GEN_ARR_YN): '')
										+ (this.KED_CRD_ARR_YN != null ? (' <em>|</em> 금융권단기연체 KED법인카드 : ' + this.KED_CRD_ARR_YN): '')
										+ (this.COMP_DEBT_YN != null ? (' <em>|</em> 금융권 채무불이행정보 기업 : ' + this.COMP_DEBT_YN): '')
										+ (this.PER_DEBT_YN != null ? (' <em>|</em> 금융권 채무불이행정보 개인 : ' + this.PER_DEBT_YN): '')
										+ (this.COMP_PUB_YN != null ? (' <em>|</em> 공공정보 기업 : ' + this.COMP_PUB_YN): '')
										+ (this.OWN_PUB_YN != null ? (' <em>|</em> 공공정보 대표자 : ' + this.OWN_PUB_YN): '')
										+ (this.SUS_ACC_YN != null ? (' <em>|</em> 당좌거래정지 : ' + this.SUS_ACC_YN): '')
										+ (this.CLS_BIZ_YN != null ? (' <em>|</em> 휴폐업 : ' + this.CLS_BIZ_YN): '')
										+ (this.FIN_ARR_YN != null ? (' <em>|</em> 상거래연체 : ' + this.FIN_ARR_YN): '')
										+ (this.WO_YN != null ? (' <em>|</em> 법정관리/Workout : ' + this.WO_YN): '')
										+ (this.ADM_MSR_YN != null ? (' <em>|</em> 행정처분정보 : ' + this.ADM_MSR_YN): '')
										+ (this.CRD_LV_YN != null ? (' <em>|</em> 신용등급 : ' + this.CRD_LV_YN): '')
										+ (this.LWST_YN != null ? (' <em>|</em> 소송 : ' + this.LWST_YN): '')
										+ (this.FIN_YN != null ? (' <em>|</em> 재무 : ' + this.FIN_YN): '')
										+ (this.SRCH_REC_YN != null ? (' <em>|</em> 조회기록 : ' + this.SRCH_REC_YN): '')
										+ (this.CLS_BIZ_TYPE != null ? (' <em>|</em> ' + this.CLS_BIZ_TYPE): '');
								}
			$htmlStr += 	'</dd>' 
						+ '</dl>' 
					+ '</li>';
		});

		$target.append($htmlStr);
		$('.border').wordBreakKeepAll();
		$.fn.setBackgroundEvenAndOdd($target, 'li', '#f6f6f6', '#ffffff');
	};

	/**
	 * <pre> 
	 *   신용정보 이력 리스트를 재구성 하는 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2013. 09. 06
	 * @param $target : 리스트 구성 영역 Element
	 * @param $listEl : 리스트를 구성할 Data
	 */
	$.fn.reloadCrdPdHistoryList = function($target, $listEl) {

		$htmlStr = '';

		$.each($listEl, function() {

			$htmlStr += '<li>'
					+ '<dl>'
					+ '<dd class="border">'
					+ (this.REG_DT != null ? ('날짜　:　' + this.REG_DT): '')
					+ (this.CRD_TYPE_VALUE != null ? (' <em>|</em> EW등급 : ' + this.CRD_TYPE_VALUE): '') + '</dd>' + '</dl>' + '</li>';
		});

		$target.append($htmlStr);
		$('.border').wordBreakKeepAll();
		$.fn.setBackgroundEvenAndOdd($target, 'li', '#f6f6f6', '#ffffff');
	};

	// 우편번호 검색 팝업창
	/* function open_post() {
		open_win('${HOME}/zip/zipSearchPopup.do');
	}; */


	/**
	 * <pre> 
	 *   파라미터 구성 함수
	 * </pre>
	 * @author Jong Pil Kim
	 * @since 2014. 04. 22
	 *
	 * @param $targetEl : 수집 대상 엘리먼트
	 * @param $selector : 수집 대상 셀렉터(클래스)
	 * @param $paramEl : 파라미터 수집 대상 객체
	 * @param $replaceEl : 삭제대상 파라미터 정제 정규식 객체
	 * @param $dataSplitStr : 구분자[데이터]
	 * @param $rowSplitStr : 구분자[row]
	 */
	$.fn.makeParam = function($targetEl, $selector, $paramEl, $replaceEl, $dataSplitStr, $rowSplitStr) {

		$result = '';

		$.each($targetEl, function() {
			if ($(this).hasClass($selector.toString())) {
				$tmpStr = '';
				for ( var i = 0; i < $paramEl.length; i++) {
					if ($tmpStr != '') {
						$tmpStr += $dataSplitStr;
					}
					$tmpStr += (($(this).parent().parent().find($paramEl[i]).val()).replace($replaceEl[i], ''));
				}

				if ($result != '') {
					$result += $rowSplitStr;
				}
				$result += $tmpStr;
			}
		});

		return $result;
	};

	/**
	 * <pre>
	 *  페이지 reload 함수
	 * </pre>
	 */
	$.fn.pageReload = function() {
		$('.tab_on').trigger('click');
	};

<%-- //////////////////////////////  함수 영역 종료  ////////////////////////////// --%>
	
<%-- //////////////////////////////  AJAX 영역 시작  ////////////////////////////// --%>

/**
 * <pre> 
 *   거래처 정보 수정 ajax 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 08. 28
 */
$.fn.modifyCreditInfo = function() {

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/modifyCreditInfo.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			//$sBox = msg.model.sBox;
			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {
				$updatedCustomer = $result.updatedCustomerBox;

				// 수정 결과 세팅

				$('#tel_no_not_modify_area').html($updatedCustomer.TEL_NO);
				$('#crd_ltd_not_modify_area').html(
							(($updatedCustomer.CRD_LTD - $updatedCustomer.SUM_DEBN_AMT + $updatedCustomer.COL_SUM_DEBN_AMT) < 0) ? 
									'<a id="lnk_crd_ltd">' + addComma('' + $updatedCustomer.CRD_LTD) + '원' + '(초과)</a>'
									: addComma('' + $updatedCustomer.CRD_LTD) + '원');

				// not modify mode로 전화
				$.fn.convertNotModifyCreditMode();
				alert('거래처 여신한도 수정이 완료되었습니다.');
			} else {
				alert(utfDecode($replMsg));
			}

		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("거래처 정보 수정 중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};


/**
 * <pre>
 *    신용정보 조회 
 *       Ajax를 활용한 Paging 처리 함수
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 09. 26
 * @param num : 현재페이지
 */
getHistoryAjaxPage = function(num) {
	$param = "EW_CD=" + $('#ew_cd').val() + "&historyNum=" + num
			+ "&custId=" + $("#cust_id").val()
			+ "&custNo=" + $("#cust_no").val();

	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerAllCreditHistoryList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 신용정보 이력 리스트 재구성
				$('#crd_history_list ul').empty();
				$targetEl = $('#crd_history_list ul');

				$("#crd_ajax_num").val(num);

				// 결제 조건 추가 결과 리스트 조회
				if (parseInt($result.customerAllCreditHistoryListCount, 10) > 0) {
					$.fn.reloadCrdHistoryList($targetEl, $result.customerAllCreditHistoryList);
				} else {
					$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#history_pc_page"), $result.historyPcPage);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

/**
 * <pre>
 *    신용정보 조회 
 *       Ajax를 활용한 Paging 처리 함수(유료)
 * </pre>
 * @author Jong Pil Kim
 * @since 2013. 09. 26
 * @param num : 현재페이지
 */
getHistoryPdAjaxPage = function(num) {
	$param = "EW_CD=" + $('#ew_cd').val() + "&historyNum=" + num
			+ "&custId=" + $("#cust_id").val()
			+ "&custNo=" + $("#cust_no").val();
	$.ajax({
		type : "POST",
		url : "${HOME}/customer/getCustomerCreditPdHistoryList.do",
		dataType : "json",
		data : $param,
		success : function(msg) {

			$result = msg.model.result;
			$replCd = $result.REPL_CD;
			$replMsg = $result.REPL_MSG;

			if ('00000' == $replCd) {

				// 신용정보 이력 리스트 재구성
				$('#crd_pd_history_list ul').empty();
				$targetEl = $('#crd_pd_history_list ul');

				$("#crdPd_ajax_num").val(num);

				// 결제 조건 추가 결과 리스트 조회
				if (parseInt(
						$result.customerCreditHistoryPdListTotalCount, 10) > 0) {
					$.fn.reloadCrdPdHistoryList($targetEl, $result.customerCreditPdHistoryList);
				} else {
					$targetEl.append('<li><div class="noSearch">결과가 없습니다.</div></li>');
				}

				// PC Paging 모듈 연동
				$.fn.reloadPaging($("#historyPd_pc_page"), $result.historyPdPcPage);

			}
		},
		error : function(xmlHttpRequest, textStatus, errorThrown) {
			alert("Ajax Paging 모듈 요청중 오류가 발생하였습니다. [" + textStatus + "]");
		}
	});
};

<%-- //////////////////////////////  AJAX 영역 종료  ////////////////////////////// --%>


</script>
</head>
<body>
	<%--  Top Area Start --%>
	<jsp:include page="../common/top.jsp" flush="true">
		<jsp:param name="mgnb" value="0101" />
	</jsp:include>
	<%-- Top Area End --%>

<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0101"/>
</jsp:include>
<!-- Left Area End -->

<!-- rightWrap -->	
	<div id="rightWrap">
		<div class="right_tit ltit01_0101"><em>거래처조회</em></div>
		
		<p class="content_tit">
			거래처정보
      	<span class="ApplyBtn01" style="position:relative;">
			<a class="on readOnly" id="btn_modify" href="#none" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-5px; position:absolute; left:640px;">
				<c:if test="${!empty customer.CUST_NM}">
					수정
				</c:if>
				<c:if test="${empty customer.CUST_NM}">
					등록
				</c:if>				
			</a>
			<a class="on modify" id="btn_save" href="#none" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-5px; right:90px; left:570px; position:absolute;">저장</a>
			<a class="on modify" id="btn_cancel" href="#none" style="font-size:11px; padding:3px 13px 3px; width:30px; top:-5px; position:absolute; left:640px;">취소</a>
		</span>				
		
		
		</p>
		
	
		
		
		<div class="tbl01Wrap">
		<form name="modifyCustomerFrm" id="modify_customer_frm" autocomplete="off">
				<%-- FORM HIDDEN AREA START --%>
				<input type="hidden" name="EWCd" id="ew_cd"	value="${customer.EW_CD}" /> 
				<input type="hidden" name="custId"	id="cust_id" value="${customer.CUST_SEQ}" /> 
				<input type="hidden" name="custId_debt"	id="cust_id_debt" value="${customer.CUST_ID}" /> 
				<input type="hidden" name="debnSearchFlag" id="debn_search_flag" value="" /> 
				<input type="hidden" name="custNo" id="cust_no" value="${customer.CUST_NO}" /> 
				<input type="hidden" name="ewRatingPd" id="ew_rating_pd" value="${customer.EW_RATING_PD}" /> 
				<input type="hidden" name="debtApplId" id="debt_appl_id" value="${debtBox.DEBT_APPL_ID}" /> 
				<input type="hidden" id="current_tab" name="currentTab" value="${sBox.currentTab}" />
				<input type="hidden" id="use_print" name="usePrint" value="" />
				<input type="hidden" id="print_yn" name="printYn" value="${sBox.printYn }" /> 
				<input type="hidden" id="summary_flag" name="summaryFlag" value="" /> 
				<input type="hidden" id="dgs_mn_kw_id" name="dgsMnKwId" value="" /> 
				<input type="hidden" id="customer_plan_list_num" name="customerPlanListNum" value="${sBox.customerPlanListNum}" />
				<input type="hidden" id="customer_prs_list_num" name="customerPrsListNum" value="${sBox.customerPrsListNum}" />
				<input type="hidden" id="customer_col_list_num" name="customerColListNum" value="${sBox.customerColListNum}" />
				<input type="hidden" name="nonpaymentType" id="nonpaymentType" value="" />
				<input type="hidden" name="debnSearchType" id="debnSearchType" value="" />
				<input type="hidden" name="searchPeriodType" id="searchPeriodType" value="" />
				<input type="hidden" name="bizKwd" id="bizKwd" value="" />
				<%-- FORM HIDDEN AREA END --%>
		
					<table>
						<colgroup>
							<col width="150px">
							<col width="240px">						
							<col width="150px">
							<col width="240px">									
						</colgroup>
					<tbody>						
						<tr>
							<th>거래처</th>
							<td>
								<span class="readOnly">
									<c:if test="${!empty customer.CUST_NM}">
										${customer.CUST_NM}
									</c:if>
									<c:if test="${empty customer.CUST_NM}">
										<span style="font-size:11px; color:red;">거래처명 등록이 필요합니다.</span>
									</c:if>			
								</span>
								<span class="modify"><input type="text" class="tblInput" id="custNm" name="custNm" value="${customer.CUST_NM}"></span>
							</td>
							<th class="second_thl">사업자번호</th>
							<td>
								${fn:substring(customer.CUST_NO,0,3)}-${fn:substring(customer.CUST_NO,3,5)}-${fn:substring(customer.CUST_NO,5,10)}
							</td>
						</tr>
						<tr>
							<th>대표자</th>
							<td>
								<span class="readOnly">${customer.OWN_NM}</span>
								<span class="modify"><input type="text" class="tblInput" id="ownNm" name="ownNm" value="${customer.OWN_NM}"></span>
							</td>
							<th class="second_thl">법인등록번호</th>
							<td colspan="3">
								<span class="readOnly">
									<c:if test="${!empty customer.CORP_NO}">
									${fn:substring(customer.CORP_NO,0,6)}-${fn:substring(customer.CORP_NO,6,13)}
									</c:if>
								</span>
								<span class="modify"><input type="text" id="corpNo" name="corpNo" class="tblInput numeric" value="${customer.CORP_NO}" maxlength="13" title="법인등록번호"></span>
							</td>
						</tr>
						<tr>
							<th>이메일</th>
							<td colspan="3">
								<c:set var="EMAIL" value="${fn:split(customer.EMAIL,'@')}" />
								<span class="readOnly">${fn:join(EMAIL,'@')}</span>
								<span class="modify">
									<input type="text" class="tblInput required" id="email1" name="email1" title="이메일 주소" value="${EMAIL[0]}" maxlength="30"> @ 
									<input type="text" class="tblInput required" readonly="readonly" id="email2" name="email2" title="이메일 도메인" value="${EMAIL[1]}" maxlength="30">
									<select id="select_email" title="이메일 도메인 선택" class="tblSelect1">
										<c:forEach var="emailCodeList" items="${emailCodeList}">
											<option value="${emailCodeList}" <c:if test="${emailCodeList eq EMAIL[1]}">selected</c:if>>
												<c:choose>
													<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
													<c:otherwise>직접입력</c:otherwise>
												</c:choose>
											</option>
										</c:forEach>
									</select>
								</span>
							</td>
						</tr>
						<tr>
							<th>회사주소</th>
							<td colspan="3">
							<span class="readOnly"><c:if test="${!empty customer.POST_NO}">(${customer.POST_NO})</c:if>  ${fn:trim(customer.ADDR_1)} ${fn:trim(customer.ADDR_2)}</span>
							<span class="modify">
								<input type="text" class="tblInput required numeric postNo" id="postNo" name="postNo" title="우편번호" maxlength="5" value="${fn:trim(customer.POST_NO)}"  style="width: 40px;"> 
								<!-- <a href="#" class="ApplyBtn02" onClick="javascript:open_post_apply(); return false;">우편번호</a> -->&nbsp; 
								<input type="text" class="tblInput required" id="addr1" name="addr1" title="주소" style="width: 237px;" value="${customer.ADDR_1}" maxlength="75" > 
								<input type="text" id="addr2" class="tblInput" name="addr2" title="상세주소" style="width: 255px; margin-top:0px;" value="${customer.ADDR_2}" maxlength="75">							
							</span>
							</td>
						</tr>
						<tr>
							<th>업종</th>
							<td>
								<span class="readOnly">${customer.BIZ_TYPE}</span>
								<span class="modify"><input type="text" class="tblInput" id="bizType" name="bizType" value="${customer.BIZ_TYPE}"></span>
							</td>
							<th class="second_thl">업태</th>
							<td>
								<span class="readOnly">${customer.BIZ_COND}</span>
								<span class="modify"><input type="text" class="tblInput" id="bizCond" name="bizCond" value="${customer.BIZ_COND}"></span>
							</td>
						</tr>
						<tr>
							<th>전화</th>
							<td>
								<c:set var="TEL_NO" value="${fn:split(customer.TEL_NO,'-')}" />
								<span class="readOnly">${fn:join(TEL_NO,'-')}</span>
								<span class="modify">
									<select id="tel_no1" name="telNo1" title="전화번호 국번" class="tblSelect1">
										<c:forEach var="phoneCodeList" items="${phoneCodeList}">
											<option value="${phoneCodeList}" <c:if test="${phoneCodeList eq TEL_NO[0]}">selected</c:if>>${phoneCodeList}</option>
										</c:forEach>
									</select> - 
									<input type="text" class="default_txtInput numeric phoneNo midNo tblInput" id="tel_no2" name="telNo2" title="전화번호 앞자리" value="${TEL_NO[1]}" style="width: 50px;" maxlength="4"> - 
									<input type="text" class="default_txtInput numeric phoneNo lastNo tblInput" id="tel_no3" name="telNo3" title="전화번호 뒷자리" value="${TEL_NO[2]}" style="width: 50px;" maxlength="4">								
								</span>
							</td>
							<th class="second_thl">팩스</th>
							<td>
							<c:set var="FAX_NO" value="${fn:split(customer.FAX_NO,'-')}" />
							<span class="readOnly">${fn:join(FAX_NO,'-')}</span>
							<span class="modify">
								<select id="fax_no1" name="faxNo1" title="팩스번호 국번" class="tblSelect1">
									<c:forEach var="faxCodeList" items="${faxCodeList}">
										<option value="${faxCodeList}" <c:if test="${faxCodeList eq FAX_NO[0]}"></c:if>>${faxCodeList}</option>
									</c:forEach>
								</select> - 
								<input type="text" class="default_txtInput numeric phoneNo midNo tblInput" id="fax_no2" name="faxNo2" title="팩스번호 앞자리" value="${FAX_NO[1]}" style="width: 50px;" maxlength="4"> - 
								<input type="text" class="default_txtInput numeric phoneNo lastNo tblInput" id="fax_no3" name="faxNo3" title="팩스번호 뒷자리" value="${FAX_NO[2]}" style="width: 50px;" maxlength="4">							
							</span>
							</td>
						</tr>
						<tr>
							<th>휴대폰</th>
							<td colspan="3">
							<c:set var="MB_NO" value="${fn:split(customer.MB_NO,'-')}" />
							<span class="readOnly">${fn:join(MB_NO,'-')}</span>
							<span class="modify">
								<select class="required tblSelect1" id="mb_no1" name="mbNo1" title="휴대폰번호 국번">
									<c:forEach var="cellPhoneCodeList" items="${cellPhoneCodeList}">
										<option value="${cellPhoneCodeList}" <c:if test="${cellPhoneCodeList eq MB_NO[0]}">selected</c:if>>${cellPhoneCodeList}</option>
									</c:forEach>
								</select> - 
								<input type="text" class="default_txtInput numeric required phoneNo midNo tblInput" id="mb_no2" name="mbNo2" title="휴대폰번호 앞자리" value="${MB_NO[1]}" style="width: 50px;" maxlength="4"> - 
								<input type="text" class="default_txtInput numeric required phoneNo lastNo tblInput" id="mb_no3" name="mbNo3" title="휴대폰번호 뒷자리" value="${MB_NO[2]}" style="width: 50px;" maxlength="4">							
							</span>
							</td>
						</tr>
					</tbody>
					</table>
					</form>
				</div>
				
				<div id="btnWrap_r">
					<div class="btn_rbbtn_01"><a href="#none" class="search" id="btn_get_list">목록</a></div><!-- TODO: transIf 추가개발될 사항 -->
				</div>
				
				
			<c:if test="${sBox.isSessionDebtGrnt eq true}">
				<c:if test="${debtProcess.STEP1 eq '' || debtProcess.STEP1 eq null}">
					<p class="content_tit_u40">채무불이행등록
				</c:if>
				<c:if test="${debtProcess.STEP1 ne '' && debtProcess.STEP1 ne null}">
					<p class="content_tit_u40">채무불이행등록 &nbsp;&nbsp;<a href="#none" id="btnDebtOngoingDetail" style="background:#4c91ff; padding: 7px 10px 7px 11px; font-size:11px; border:1px solid #4e7ac2; text-align:center; font-weight:bold; color:#fff; text-shadow:0 0 1px #3f3f3f; border-radius: 3px;">상세보기</a></p>
				</c:if>
				
				<div class="tbl03Wrap">
				<c:if test="${debtProcess.STEP1 eq '' || debtProcess.STEP1 eq null}">
					<table>
					<tbody>
						<tr>
							<th>현재 진행중인 채무불이행 상태가 없습니다.</th>
						</tr>
						<tr>
							<td><div class="btn_cbbtn_01">
							<a href="#none" class="on" id="btn_nonpayment_regist">채무불이행등록</a></div></td>
						</tr>
					</tbody>
					</table>
				</c:if>
				<c:if test="${debtProcess.STEP1 ne '' && debtProcess.STEP1 ne null}">
					<%-- 현재 진행상태 테이블 시작 --%>
					<div id="defaultWrap">
						<div class="default_stepbox">
							<ul>
								<c:if test="${debtProcess.STEP1 ne ''}">
									<li class="s1">
									<p class="default_step_n">${debtProcess.STEP1 }</p>
									<p class="default_step_d">${debtProcess.STEP1_DT }</p>
									<em></em></li>
								</c:if>
								
								<c:if test="${debtProcess.STEP2 ne ''}">
									<li class="s2">
									<p class="default_step_n">${debtProcess.STEP2 }</p>
									<p class="default_step_d">${debtProcess.STEP2_DT }</p>
									<em></em></li>
								</c:if>
								
								<c:if test="${debtProcess.STEP3 ne ''}">
									<li class="s2">
									<p class="default_step_n">${debtProcess.STEP3 }</p>
									<p class="default_step_d">${debtProcess.STEP3_DT }</p>
									<em></em></li>
								</c:if>
								
								<c:if test="${debtProcess.STEP4 ne ''}">
									<li class="s2">
									<p class="default_step_n">${debtProcess.STEP4 }</p>
									<p class="default_step_d">${debtProcess.STEP4_DT }</p>
									<em></em></li>
								</c:if>
								
								<c:if test="${debtProcess.STEP5 ne ''}">
									<li class="s2">
									<p class="default_step_n">${debtProcess.STEP5 }</p>
									<p class="default_step_d">${debtProcess.STEP5_DT }</p>
									<em></em></li>
								</c:if>
								
								<c:if test="${debtProcess.STEP6 ne ''}">
									<li class="s2">
									<p class="default_step_n">${debtProcess.STEP6 }</p>
									<p class="default_step_d">${debtProcess.STEP6_DT }</p>
									<em></em></li>
								</c:if>
								
							</ul>
						</div>
					</div>		
					<%-- 현재 진행상태 테이블 끝 --%>				
				</c:if>
				</div> 
			</c:if>

				<div id="content_tit_pr">
					<div class="content_tit pru40">신용정보 변동이력 <p class="point_txt1"></p> &nbsp;    </div>					
					<div id="btnWrap_rpt">
						<c:choose>
							<c:when test="${fn:length(customerCreditHistoryList) eq 0 or customerCreditHistoryList eq null }">
								<div class="btn_rdgbtn_01"><a class="on" id="btnGetCrdList" href="javascript:alert('신용정보가 없습니다.')">신용정보 확인하기</a></div>
							</c:when>
							<c:otherwise>
								<div class="btn_rdgbtn_01"><a class="smallBtnType01 crdInfo" id="btnGetCrdList" href="#none">신용정보 확인하기</a></div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				
				<div class="tbl04Wrap" id="crd_history_list">
					<table>
					<tbody>
						<tr>
							<th width="50%" class="first_thlwr4">신용변동일</th>
							<th width="50%">신용정보(EW)등급</th>
						</tr>
						<c:if test="${fn:length(customerCreditHistoryList) eq 0 or customerCreditHistoryList eq null }">
							<td class="first_tdlwr4 noSearch" colspan="2">결과가 없습니다.</td>
						</c:if>
						<c:forEach var="customerCreditHistoryList" items="${customerCreditHistoryList}">
						<tr <c:if test="${customerCreditHistoryList.STAT == 'D'}">style="display: none;"</c:if>>
							<td class="first_tdlwr4">
							<c:if test="${not empty customerCreditHistoryList.EW_DT }">
								<%-- ${fn:substring(customerCreditHistoryList.EW_DT,0,4)}-${fn:substring(customerCreditHistoryList.EW_DT,5,7)}-${fn:substring(customerCreditHistoryList.EW_DT,8,10)} --%> 
								${customerCreditHistoryList.EW_DT}
							</c:if>
							</td>
							<td>
							<c:if test="${not empty customerCreditHistoryList.CRD_TYPE_VALUE }">
								${customerCreditHistoryList.CRD_TYPE_VALUE }
							</c:if>
							</td>
						</tr>						
						</c:forEach>
					</tbody>
					</table>
				</div>
				
				<!-- page_num_wrap -->
				<div id="page_num_wrap">
					<div class="page_num" id="getPageView">
						<c:out value="${historyPcPage}" escapeXml="false" />
					</div>
				</div>
				<!-- page_num_wrap -->
			</div>
		
</div>
	<%--  footer Area Start --%>
	<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
	<%-- footer Area End --%>
</body>
</html>