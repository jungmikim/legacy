<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<c:set var="companyUsrInfoList" value="${result.resultSbox}"/>
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">

$(document).ready(function(){
});

function getModify(){
	location.href='${HOME}/mypage/getUserModify.do';
}

function getModifyForPW(){
	location.href='${HOME}/mypage/getUserModifyForPW.do';
}

function getMyPageInfo(){
	location.href='${HOME}/mypage/myPageMain.do';
}


</script>

</head>
<body>
<div id="accessibility">
	<a href="#mainImg">본문바로가기</a>
</div>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0602"/>
</jsp:include>
<!-- Top Area End -->

<!-- contentArea -->
<div class="contentArea sub06"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		
		<div class="mypageCover">
			<dl class="myPic">
				<dt><em><a href="javascript:getMyPageInfo()">${sBox.sessionCompUsrNm}<br>${sBox.sessionUsrNm} 님 <c:if test="${sBox.sessionAdmYn eq 'Y'}"><br>(관리자)</c:if></a></em></dt>
				<dd><img src="${IMG}/common/bg_mypic.gif" alt="" /></dd>
			</dl>
			<div class="subTitBox">
				<h2 class="tit_sub06">마이페이지</h2>
				<p class="under_h2_sub06">회원의 채권 요약, 공지사항, 1:1문의, 환경 설정 등을 한 눈에 조회할 수 있습니다</p>
			</div>
		</div>
		
		<div class="infoInputWrap">
			<h3 class="first2 titB"><img src="${IMG}/common/h3_05_01.gif" alt="개인 정보" /></h3>
			
			<div class="infoInput"> 
				<dl class="comMemInput">
					<dt><span><label for="deptNm">이름</label></span></dt>
					<dd><span>
						${result.resultDeptSbox.USR_NM} 
					</span></dd>
					
					<dt class="first longer"><span>아이디</span></dt>
					<dd class="first longer"><span>
						${result.resultSbox.LOGIN_ID} 
					</span></dd>
					
					<dt><span><label for="deptNm">부서명</label></span></dt>
					<dd><span>
						${result.resultDeptSbox.DEPT_NM} 
					</span></dd>
					
					<dt><span><label for="deptNm">직위</label></span></dt>
					<dd><span>
						${result.resultDeptSbox.JOB_TL_NM} 
					</span></dd>
					<dt class="address first longer"><span>주소</span></dt>
					<dd class="address first longer"><span><c:if test="${not empty companyUsrInfoList.POST_NO}">${companyUsrInfoList.POST_NO}</c:if></span>
						<span>${result.resultSbox.ADDR_1} ${result.resultSbox.ADDR_2}</span>
					</dd>
					
					<dt class="first longer"><span>이메일</span></dt>
					<dd class="first longer"><span>
						${result.resultDeptSbox.EMAIL}  
					</span></dd>
					
					<dt class="first longer"><span>전화번호</span></dt>
					<dd class="first longer"><span>
						${result.resultDeptSbox.TEL_NO}  
					</span></dd>
					
					<dt class="first longer"><span>팩스번호</span></dt>
					<dd class="first longer"><span>
						${result.ltDeptSbox.FAX_NO}  
					</span></dd> 
					
					<dt class="first longer"><span>휴대폰번호</span></dt>
					<dd class="first longer"><span>
						${result.resultDeptSbox.MB_NO}  
					</span></dd>
				</dl>
			</div>	
		</div>
		
		<div class="btnWrap btnMember">
			<div class="btnMiddle">
				<a href="#" class="on" onclick="javascript:getModify()" >개인정보 수정</a>
				<a href="#" class="on" onclick="javascript:getModifyForPW()" >비밀번호 수정</a>
			</div>
		</div>
		
	</section>

	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>