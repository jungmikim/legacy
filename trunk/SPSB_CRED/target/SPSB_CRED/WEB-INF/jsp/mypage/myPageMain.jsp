<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	if("${sBox.sessionAdmYn}"=="Y" || ("${sBox.isSessionDebtGrnt}")=="true"){
		$('#debtDefaultView').show();
	}else{
		if(("${sBox.isSessionDebtGrnt}")=="true"){
			$('#debtDefaultView').show();
		}else{
			$('#debtDefaultView').hide();
		}
	}
});
//////////////////////////////////////////////////////////// FUNCTION  ////////////////////////////////////////////////////////////
function getMyPageInfo(){
	location.href='${HOME}/mypage/myPageMain.do';
}

//채권관리 현황 파라메터 함수

//채무불이행 진행상태 요약 파라메터 함수
function thirtyDay(paramId){
	   
	// 파라미터 값 셋팅
	if(paramId=='R'){
		$('#modStatR').val(paramId);
	}else{
		$('#modStatR').attr("disabled","disabled");
		$('#num').val(1);
		$('#processStatusType').val(paramId);
		
		var statusCode = "<input type='hidden' name='processStatus"+paramId+"' value='"+paramId+"'/>";
		$('#statusCode').html(statusCode);
		
	}
	$('#periodConditionType').val("ALL_DT");
	
	var usrId="";
	 if(paramId=='AU'|| paramId=='RU'||paramId=='KF'){
		usrId="getDebtCompleteInquiryList.do"; //진행상태 조회
	 }else {
		usrId="getDebtOngoingList.do"; // 완료상태 조회
	 }
	
	$('#summaryFrm').attr({'action':'${HOME}/debt/'+usrId, 'method':'POST'}).submit();
}

//채무불이행 진행상태 요약 파라메터 함수
function getParametersBySummery(paramId){
	   
	// 파라미터 값 셋팅
	if(paramId=='R'){
		$('#modStatR').val(paramId);
	}else{
		$('#modStatR').attr("disabled","disabled");
		$('#num').val(1);
		$('#processStatusType').val(paramId);
		
		var statusCode = "<input type='hidden' name='processStatus"+paramId+"' value='"+paramId+"'/>";
		$('#statusCode').html(statusCode);
		
		if(paramId=='CA'){
			var statusCode = "<input type='hidden' name='processStatusCA' value='CA,CB'/>";
			$('#statusCode').html(statusCode);
			$('#processStatusType').val('CA,CB');
		} 	
		
		if(paramId=='FA'){
			var statusCode = "<input type='hidden' name='processStatusFA' value='FA,FB'/>";
			$('#statusCode').html(statusCode);
			$('#processStatusType').val('FA,FB');
		} 			
		
		
	}
	$('#periodConditionType').val("ALL_DT");
	
	$('#summaryFrm').attr({'action':'${HOME}/debt/getDebtInquiryList.do', 'method':'POST'}).submit();
}


//거래처 신용정보 요약 파라메터 함수
function getCustCreditParametersBySummery(paramId){
	  
	if(paramId=='T1'){//T1 일주일 변동
		$('#crdDtType').val("Z");
		$('#crdDtDay').val(7);
		$('#ewRatingType').val("");
	}else if(paramId=='T9'){//T9 90일 변동
		$('#crdDtType').val("Y");
		$('#crdDtDay').val(90); 
		$('#ewRatingType').val("");
	}else{
		$('#crdDtType').val("");
		$('#crdDtDay').val("");
		$('#ewRatingType').val(paramId);
	}
	$('#num').val(1);
	usrId="getCustomerList.do"; // 완료상태 조회
	$('#custCreditListFrm').attr({'action':'${HOME}/customer/'+usrId, 'method':'POST'}).submit();
	
}

function modifyPW(){
	if(confirm("비밀번호를 변경하시겠습니까?")){
		location.href="${HOME}/mypage/getUserModifyForPW.do";
	}
}

</script>
</head>
<body>
<div id="Wrapper">
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0001"/>
</jsp:include>
<!-- Top Area End -->

<!-- contentArea -->
<div id="containerWrap">
<%-- <!--  Left Area Start -->
 <jsp:include page="../common/left_mypage.jsp" flush="true">
	<jsp:param name="gnb" value="0199"/>
</jsp:include>
<!-- Left Area End --> --%>

<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0001"/>
</jsp:include>
<!-- Left Area End -->

	<div id="rightWrap">
		<div class="right_tit ltit00_0101"><em>마이페이지</em></div>
		
		<p class="content_tit">소속기업 현황 
		<a href="#none" onclick="javascript:modifyPW();">
			<b style="font-size:12px;">[비밀번호 변경]</b>
		</a>
		</p>
		 
		
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<tr>
					<th width="25%" class="first_thlwr4">사업자명 <br />사업자등록번호</th>
					<th width="25%">정보</th>
					<th width="35%">권한</th>
					<th width="10%">상태</th>
				</tr>
				<c:forEach var="list" items="${resultCompanyList.resultList}" varStatus="index"> 
				<input type="hidden" value="${list.MBR_ID}"/>
				<tr>
					<td class="padding first_tdlwr4">
						<c:out value="${list.USR_NM}"/><br />( <c:if test="${not empty list.USR_NO}"> <c:out value="${fn:substring(list.USR_NO,0,3)}-${fn:substring(list.USR_NO,3,5)}-${fn:substring(list.USR_NO,5,10)}"/> </c:if> )
					</td>
					<td class="padding myPageBelongtoCompanyListInfo" id="myPageBelongtoCompanyListInfo_${list.MBR_ID}">
						${list.MBR_USR_NM}<c:if test="${not empty list.MBR_MB_NO}">
						<p class="ico_bar"><img src="${IMG}/common/ico_txt_bar.gif" /></p></c:if>
						${list.MBR_MB_NO}
						<c:if test="${not empty list.MBR_EMAIL}"><br></c:if>
						${list.MBR_EMAIL} 
					</td>
					<td class="padding">
						 <c:if test="${not empty list.GRN_DESC}">  ${list.ADM_DESC} ${list.GRN_DESC} </c:if>
						 <c:if test="${empty list.ADM_DESC && empty list.GRN_DESC}">
						 		 <c:if test="${list.STAT ne 'N'}"> 권한없음  </c:if> 
						 		 <c:if test="${list.STAT eq 'N'}"> - </c:if>
						 </c:if> 
					</td>
					<td class="padding"> <c:out value="${list.STAT_DESC}"/> </td>
				</tr>
				</c:forEach>
			</tbody>
			</table>
		</div>
		
		<p class="content_tit_u40">거래처 신용정보 변동현황</p>
		
		<form name="custCreditListFrm" id="custCreditListFrm">
			<!-- FORM HIDDEN AREA START 
			<input type="hidden" name="num" id="colNum" value="" />-->
			<input type="hidden" name="crdDtType" id="crdDtType" value="" />
			<input type="hidden" name="crdDtDay" id="crdDtDay" value="" />
			<input type="hidden" name="ewRatingType" id="ewRatingType" value="" />
			<!-- FORM HIDDEN AREA END -->
		</form>
		
		<div class="tbl04Wrap">
			<table>
			<tbody>
				<colgroup>
					<col width="14%" />
					<col width="14%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
					<col width="9%" />
				</colgroup>
				<tr>
					<th class="first_thlwr4">일주일 변동</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('T1')">
						<c:choose>
					    <c:when test="${not empty resultCustCreditSummery.resultBox['T1'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['T1'].EW_CNT}"/></c:when>
					    <c:otherwise>0</c:otherwise>
					    </c:choose>
					    </a>건
				    </td>
					<th>정상</th>
					<!-- <td><a href="#" class="txt_bar2_link">??</a> 건</td> -->
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('00')">
						<c:choose>
					    <c:when test="${not empty resultCustCreditSummery.resultBox['00'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['00'].EW_CNT}"/></c:when>
					    <c:otherwise>0</c:otherwise>
					    </c:choose>
						</a> 건
					</td>
					
					<th>관찰1</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('02')">
						<c:choose>
					    <c:when test="${not empty resultCustCreditSummery.resultBox['02'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['02'].EW_CNT}"/></c:when>
					    <c:otherwise>0</c:otherwise>
					    </c:choose>
						</a> 건
					</td>
					<th>관찰3</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('04')">
						<c:choose>
						<c:when test="${not empty resultCustCreditSummery.resultBox['04'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['04'].EW_CNT}"/></c:when>
						<c:otherwise>0</c:otherwise>
						</c:choose></a> 건
					</td>
					<th>휴업</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('05')">
						<c:choose>
						<c:when test="${not empty resultCustCreditSummery.resultBox['05'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['05'].EW_CNT}"/></c:when>
						<c:otherwise>0</c:otherwise>
						</c:choose>
						</a> 건
					</td>
				</tr>
				<tr>
					<th class="first_thlwr4">90일 변동</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('T9')">
						<c:choose>
				        <c:when test="${not empty resultCustCreditSummery.resultBox['T9'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['T9'].EW_CNT}"/></c:when>
				        <c:otherwise>0</c:otherwise>
				        </c:choose>
				        </a> 건
			        </td>
					<th>관심</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('01')">
						<c:choose>
				        <c:when test="${not empty resultCustCreditSummery.resultBox['01'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['01'].EW_CNT}"/></c:when>
				        <c:otherwise>0</c:otherwise>
				        </c:choose>
				        </a> 건
			        </td>
					<th>관찰2</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('03')">
						<c:choose>
				        <c:when test="${not empty resultCustCreditSummery.resultBox['03'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['03'].EW_CNT}"/></c:when>
				        <c:otherwise>0</c:otherwise>
				        </c:choose>
				        </a> 건
			        </td>
					<th>부도</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('06')">
						<c:choose>
						<c:when test="${not empty resultCustCreditSummery.resultBox['06'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['06'].EW_CNT}"/></c:when>
						<c:otherwise>0</c:otherwise>
						</c:choose></a> 건
					</td>
					<th>폐업</th>
					<td>
						<a href="#none" class="txt_bar2_link" onclick="getCustCreditParametersBySummery('07')">
						<c:choose>
						<c:when test="${not empty resultCustCreditSummery.resultBox['07'].EW_RATING}"><c:out value="${resultCustCreditSummery.resultBox['07'].EW_CNT}"/></c:when>
						<c:otherwise>0</c:otherwise>
						</c:choose></a> 건
					</td>
				</tr>
			</tbody>
			</table>
		</div>
		
		<div id="debtDefaultView">
			<p class="content_tit_u40">채무불이행 진행상태 현황</p>
			
			<c:set var="sBox" value="${sBox}"/>
			
			<!-- FORM HIDDEN AREA END -->
			<form name="summaryFrm" id="summaryFrm">
			
			<!-- FORM HIDDEN AREA START -->
			<input type="hidden" name="num" id="num" value="" />
			<input type="hidden" name="modStatR" id="modStatR" value=""/>
			<input type="hidden" name="processStatusType" id="processStatusType" value="" />
			<input type="hidden" name="periodConditionType" id="periodConditionType" value="ALL_DT" />
			<input type="hidden" name="mbrList" id="mbrList" value="" />
			<span id="statusCode"></span>
			
			<div class="tbl04Wrap">
				<table>
				<tbody>
					<tr>
						<th width="23%" class="first_thlwr4">진행상태</th>
						<th width="10%">건수</th>
						<th width="23%">진행상태</th>
						<th width="10%">건수</th>
						<th width="24%">진행상태</th>
						<th width="10%">건수</th>
					</tr>
					<tr>
						<td class="first_tdlwr4">채무금액 정정요청</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('R')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['R'].STAT}"><c:out value="${summery.resultBox['R'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>통보서 발송</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('NT')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['NT'].STAT}"><c:out value="${summery.resultBox['NT'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>심사대기</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('AW')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['AW'].STAT}"><c:out value="${summery.resultBox['AW'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
					</tr>
					<tr>
						<td class="first_tdlwr4">민원발생</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('CA')">
							<c:set var="CACB_CNT" value="${result.resultBox['CA'].CNT + result.resultBox['CB'].CNT}" /> <!-- 등록 전, 등록 후 민원발생 CNT -->
							<c:choose>
							<c:when test="${not empty summery.resultBox['CA'].STAT || not empty summery.resultBox['CB'].STAT}"><c:out value="${CACB_CNT}"/></c:when>
							<%-- <c:when test="${not empty summery.resultBox['CA'].STAT}"><c:out value="${summery.resultBox['CA'].CNT}"/></c:when> --%>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>심사 중</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('EV')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['EV'].STAT}"><c:out value="${summery.resultBox['EV'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
<%-- 						<td>민원발생(등록 후)</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('CB')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['CB'].STAT}"><c:out value="${summery.resultBox['CB'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td> --%>
						<td>추가증빙제출</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('FA')">
							<c:set var="FAFB_CNT" value="${result.resultBox['FA'].CNT + result.resultBox['FB'].CNT}" /> <!-- 추가증빙제출 등록 전, 등록 후 민원발생 CNT -->
							<c:choose>
							<c:when test="${not empty summery.resultBox['FA'].STAT || not empty summery.resultBox['FB'].STAT}"><c:out value="${FAFB_CNT}"/></c:when>
							<%-- <c:when test="${not empty summery.resultBox['FA'].STAT}"><c:out value="${summery.resultBox['FA'].CNT}"/></c:when> --%>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>						
					</tr>
					<tr>
						<td class="first_tdlwr4">통보서 준비</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('PY')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['PY'].STAT}"><c:out value="${summery.resultBox['PY'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>해체 요청</td>
						<td>
						<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('RR')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['RR'].STAT}"><c:out value="${summery.resultBox['RR'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
						</a> 건
						</td>
						<td>결제대기</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('AA')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['AA'].STAT}"><c:out value="${summery.resultBox['AA'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
					</tr>
					<tr>
<%-- 						<td class="first_tdlwr4">추가증빙제출</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('FA')">
							<c:set var="FAFB_CNT" value="${result.resultBox['FA'].CNT + result.resultBox['FB'].CNT}" /> <!-- 추가증빙제출 등록 전, 등록 후 민원발생 CNT -->
							<c:choose>
							<c:when test="${not empty summery.resultBox['FA'].STAT || not empty summery.resultBox['FB'].STAT}"><c:out value="${FAFB_CNT}"/></c:when>
							<c:when test="${not empty summery.resultBox['FA'].STAT}"><c:out value="${summery.resultBox['FA'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td> --%>
						<td class="first_tdlwr4">등록완료</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('RC')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['RC'].STAT}"><c:out value="${summery.resultBox['RC'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
<%-- 						<td>추가증빙제출(등록 후)</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('FB')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['FB'].STAT}"><c:out value="${summery.resultBox['FB'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건 
						</td> --%>
					</tr>
				</tbody>
				</table> 
			</div>
			
			<p class="content_tit_u40">채무불이행 완료상태 현황</p>
			
			<div class="tbl04Wrap">
				<table>
				<tbody>
					<tr>
						<th width="23%" class="first_thlwr4">진행상태</th>
						<th width="10%">건수</th>
						<th width="23%">진행상태</th>
						<th width="10%">건수</th>
						<th width="24%">진행상태</th>
						<th width="10%">건수</th>
					</tr>
					<tr>
						<td class="first_tdlwr4">신청불가</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('AU')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['AU'].STAT}"><c:out value="${summery.resultBox['AU'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
						<td>등록불가</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('RU')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['RU'].STAT}"><c:out value="${summery.resultBox['RU'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise> 
							</c:choose>
							</a> 건
						</td>
						<td>심사과실발생</td>
						<td>
							<a href="#none" class="txt_bar2_link" onclick="getParametersBySummery('KF')">
							<c:choose>
							<c:when test="${not empty summery.resultBox['KF'].STAT}"><c:out value="${summery.resultBox['KF'].CNT}"/></c:when>
							<c:otherwise>0</c:otherwise>
							</c:choose>
							</a> 건
						</td>
					</tr>
				</tbody>
				</table>
			</div>
		
			
			
			
			
			</form>
		
		</div>
		
	
	</div>

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</div>
</div>
</body>
</html>