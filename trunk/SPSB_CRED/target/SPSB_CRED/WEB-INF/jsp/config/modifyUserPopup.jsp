<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="userInfo" value="${result.userInfo}"/>
<c:set var="userGnrList" value="${result.userGnrList}"/>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/pop.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->
<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;
$(document).ready(function(){

	/**
	* <pre>
	*	권한선택 checkbox CHANGE 이벤트
	* </pre>
	* @author KIM GA EUN
	* @since 2015. 07. 07
	*/
	$('.grn').change(function(){        	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $(this).parent().find(':checked').not('[name=admYn]').length;
		var id = $(this).attr("id");
		if(id == "admYn"){
			if(this.checked) {
	            $('.grn').each(function() { 
	                this.checked = true;              
	            });
	        }else{
	            $('.grn').each(function() { 
	                this.checked = false;                        
	            });         
	        }
		}
		else if($(this).val() == 'Y' && checkedLen == 6){
       		$(this).attr('checked',true);
		}else if($(this).val() == 'Y' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       	}else if($(this).val() != 'ALL' && checkedLen != 5){
       		$("#admYn").attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 5){
       		$("#admYn").attr('checked',true);
       	}
	});
	
	 <%--이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직--%>
	 $('#select_email').live('change',function(e){
		 if($.trim($(this).find('option:selected').val()) == ''){
			 $(this).prev().attr('readonly', false);
			 $("#emailType01").val("");
		 } else {
			 $(this).prev().attr('readonly', true);
		 }
		 $(this).prev().val($(this).val());
	 });
	 
	 
	 
	 $("#btn_save").click(function(){
			if(!fnValidate()) {
				return false;
			}
			
			$paramObj = {
					 compUsrId : $("#compUsrId").val()
					 ,mbrId : $("#mbrId").val()
			 }; 
			$param = $.param($paramObj);
			$.ajax({
				url: "${HOME}/config/checkAdminYN.do",
				type: "POST",
				data: $param,
				dataType: "json",
				async: true,
				error:function(result){
		 		},
				success: function(result){

					var CHK_Y_CNT = result.CHK_Y_CNT;
					var U_ADM_YN = result.U_ADM_YN;
					if(U_ADM_YN == 'Y' && CHK_Y_CNT == 1){
						alert("최소 1명의 관리자는 권한 수정이 불가능합니다.");
					}else{
						if(confirm("수정하시겠습니까?")){

							$.ajax({
								url: "${HOME}/config/updateUser.do",
								type: "POST",
								data:$("#userModifyForm").serialize(),
								dataType: "json",
								async: true,
								error:function(result){
						 			//alert("fail");	
						 			},
								success: function(result){
									 $(opener.location).attr("href", "javascript:getNewRequestList()"); 					 
									 window.close();
								}
							}); 
						}
					}
				}
			});
		 });
	 
});

<%--회원가입 전송 파라미터 검증 함수--%>
function fnValidate() {

   /* if( $.trim($("#compPersonalNo").val()).length == 0 ) {
		alert("사번을 입력해주세요.");
		$("#compPersonalNo").focus();
		return false;
	} */
	 
	if( ($.trim($("#email1").val()).length == 0 || $.trim($("#email2").val()).length == 0) 
			&& ($.trim($("#emailType01").val()).length == 0 )) {
		alert("이메일을 입력해주세요.");
		return false;
	}
	var emailAddr = $.trim($("#emailType01").val());
	if( $.trim($("#email1").val()).length > 0 && $.trim($("#email2").val()).length > 0){
		emailAddr = $.trim($("#email1").val()) + "@" + $.trim($("#email2").val());
	}
	if( !typeCheck('emailCheck', emailAddr) ) {
		alert('이메일을 형식을 확인해 주세요.');
		return false;
	}
	$("#emailType01").val(emailAddr);
	
	if( $.trim($("#telNo2").val()).length < 3 || $.trim($("#telNo3").val()).length < 4 ) {
		alert("전화번호를 입력해주세요.");
		$("#telNo2").focus();
		return false;
	}
	if(!typeCheck('numCheck', $.trim($("#telNo2").val()) + $.trim($("#telNo3").val()))){
		alert("전화번호는 숫자만 입력 가능합니다.");
		return false;
	}

	if(!$('.grn:checked').size()){
	   alert('권한을 선택해주세요.');
	   return fale;
  }
	
	return true;
}


</script>
</head>
<body>
<div id="popWrap">
	<div id="headerWrap">
		<h1><img src="${IMG}/popup/tit_0301_02.gif" /></h1>
	</div>
	<form id="userModifyForm" name="userModifyForm" method="post">
	<input type="hidden" id="mbrId" name="mbrId" class="ptblInput" style="width:150px" value="${mbrId}">
	<input type="hidden" id="compUsrId" name="compUsrId" value="${sBox.sessionCompUsrId}"/>
		<div id="contentWrap">
		<div class="ptbl01Wrap">
			<table>
			<colgroup>
				<col width="27%">
				<col width="73%">								
			</colgroup>
			<tbody>
				<tr>
					<th>ID</th>
					<td>${userInfo.LOGIN_ID}</td>
				</tr>
				<tr>
					<th>이름</th>
					<td>${userInfo.USR_NM}</td>
				</tr>
				<tr>
					<th>사번</th>
					<td>
					<input type="text" id="compPersonalNo" name="compPersonalNo" class="ptblInput" value="${fn:trim(userInfo.COMP_PERSONAL_ID)}" style="width:150px">
					</td>
				</tr>
				<c:set var="TEL_NO" value="${fn:split(userInfo.TEL_NO,'-')}" />
				<tr>
					<th>전화번호 <span class="em_po">*</span> </th>
					<td>
						<select id="tel_no1" name="telNo1" title="전화번호 국번" class="ptblSelect1">
							<c:forEach var="phoneCodeList" items="${phoneCodeList}">
								<option value="${phoneCodeList}" <c:if test="${phoneCodeList eq TEL_NO[0]}">selected</c:if>>${phoneCodeList}</option>
							</c:forEach>
						</select> - 
						<input type="text" name="telNo2" class="ptblInput" id="telNo2" name="telNo2" title="전화번호 앞자리" value="${TEL_NO[1]}" style="width:50px"> -
						<input type="text" name="telNo3" class="ptblInput" id="telNo3" name="telNo3" title="전화번호 뒷자리" value="${TEL_NO[2]}" style="width:50px">
					</td>
				</tr>
				<c:set var="EMAIL" value="${fn:split(userInfo.EMAIL,'@')}" />
				<tr>
					<th>이메일 <span class="em_po">*</span></th>
					<td>
						<input type="text" id="emailType01" class="txtInput" name="emailType01" title="이메일" style="display:none" >
					    <input type="text" name="" class="ptblInput" id="email1" name="email1" title="이메일 주소" value="${EMAIL[0]}" style="width:60px"> @
					    <input type="text" name="" class="ptblInput" id="email2" name="email2" title="이메일 도메인" value="${EMAIL[1]}" style="width:80px">
						<select id="select_email" title="이메일 도메인 선택" class="ptblSelect1">
							<c:forEach var="emailCodeList" items="${emailCodeList}">
								<option value="${emailCodeList}" <c:if test="${emailCodeList eq EMAIL[1]}">selected</c:if>>
									<c:choose>
										<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
										<c:otherwise>직접입력</c:otherwise>
									</c:choose>
								</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>스마트빌 ID</th>
					<td><label>${userInfo.SB_LOGIN_ID}</label></td>
				</tr>
				<c:forEach var="userGnrList" items="${userGnrList}">
					<c:if test="${userGnrList.GRN_CD eq 'CRED1C'}">
						<c:set var="CRED1C" value="Y" />
					</c:if>
					<c:if test="${userGnrList.GRN_CD eq 'SBDE1R'}">
						<c:set var="SBDE1R" value="Y" />
					</c:if>
					<c:if test="${userGnrList.GRN_CD eq 'SBDE2R'}">
						<c:set var="SBDE2R" value="Y" />
					</c:if>
					<c:if test="${userGnrList.GRN_CD eq 'SBDE3R'}">
						<c:set var="SBDE3R" value="Y" />
					</c:if>
					<c:if test="${userGnrList.GRN_CD eq 'SBDE4R'}">
						<c:set var="SBDE4R" value="Y" />
					</c:if>
				</c:forEach>
				<tr>
					<th>권한 <span class="em_po">*</span></th>
					<td>			
					관리자 : <input type="checkbox" id="admYn" name="admYn" size="20" title="관리자" value="Y" class="grn" <c:if test="${userInfo.ADM_YN eq 'Y'}">checked</c:if>><br>
	    			채무불이행 작성/신청 권한 : <input type="checkbox" name="grnCdArray" size="20" title="채무불이행 작성/신청 권한" value="CRED1C" class="grn" <c:if test="${CRED1C eq 'Y'}">checked</c:if>><br>
	    			기업정보검색 권한 : <input type="checkbox"  name="grnCdArray" size="20" title="기업정보검색 권한" value="SBDE1R" class="grn" <c:if test="${SBDE1R eq 'Y'}">checked</c:if>><br>
	    			기업정보브리핑 권한 : <input type="checkbox" name="grnCdArray" size="20" title="기업정보브리핑 권한" value="SBDE2R" class="grn" <c:if test="${SBDE2R eq 'Y'}">checked</c:if>><br>
	    			조기경보상세 권한 : <input type="checkbox" name="grnCdArray" size="20" title="조기경보상세 권한" value="SBDE3R" class="grn" <c:if test="${SBDE3R eq 'Y'}">checked</c:if>><br>
					조기경보업체등록 권한 : <input type="checkbox" name="grnCdArray" size="20" title="조기경보업체등록 권한" value="SBDE4R" class="grn" <c:if test="${SBDE4R eq 'Y'}">checked</c:if>><br>
					</td><!-- 체크박스로 -->
				</tr>
			</tbody>
			</table>
		</div>
	</div>
	</form>
	
	<div id="footWrap">
		<div class="ex">
			<div class="btn_cbbtn_01"><a href="#none" id="btn_save">저장</a></div>
			<div class="btn_cgbtn_01"><a href="#none" id="btn_cancle" onclick="self.close()">취소</a></div>
		</div>
	</div>
		
</div>

</body>
</html>