<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/pop.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->
<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;
var loginIdCheckFlag = false;	// 로그인 아이디 확인 Flag 변수
var isIng = false;
$(document).ready(function(){
	
	<%--아이디 중복 확인--%>
	$("#btnLoginIdCheck").click(function(){
		fnLoginIdCheck();
	});
	
 	$(".grn").click(function(){
		if($(this).attr("id")=='admYn'){
			if($(this).is(":checked")){
				$('.grn').each(function() { 
	                this.checked = true;              
	            });
				
				//$('.grn').prop('checked', true);	
			}else{
				$('.grn').each(function() { 
	                this.checked = false;              
	            });
				//$('.grn').prop('checked', false);
			}
		}
	}); 
	

	/**
	* <pre>
	*	권한선택 checkbox CHANGE 이벤트
	* </pre>
	* @author KIM GA EUN
	* @since 2015. 07. 07
	*/
	$('.grn').change(function(){        	
		// [전체] 를 제외한 checked 수량 구하기
		var checkedLen = $(this).parent().find(':checked').not('[name=admYn]').length;
		var id = $(this).attr("id");
		if(id == "admYn"){
			if(this.checked) {
	            $('.grn').each(function() { 
	                this.checked = true;              
	            });
	        }else{
	            $('.grn').each(function() { 
	                this.checked = false;                        
	            });         
	        }
		}
		else if($(this).val() == 'Y' && checkedLen == 6){
       		$(this).attr('checked',true);
		}else if($(this).val() == 'Y' && $(this).is(':checked')){
       		$(this).siblings().attr('checked',true);
       	}else if($(this).val() != 'ALL' && checkedLen != 5){
       		$("#admYn").attr('checked',false);
       	}else if($(this).val() != 'ALL' && checkedLen == 5){
       		$("#admYn").attr('checked',true);
       	}
	});
	
	
	
	
	 <%--이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직--%>
	 $('#select_email').live('change',function(e){
		 if($.trim($(this).find('option:selected').val()) == ''){
			 $(this).prev().attr('readonly', false);
			 $("#emailType01").val("");
		 } else {
			 $(this).prev().attr('readonly', true);
		 }
		 $(this).prev().val($(this).val());
	 });
	 
	 
	 <%--회원가입 전송 파라미터 검증 함수--%>
	 function fnValidate() {
	 	
		 if( $.trim($("#legacyId").val()).length == 0 ) {
	 		alert("아이디를 입력해주세요.");
	 		$("#legacyId").focus();
	 		return false;
	 	}
		 
		if(!loginIdCheckFlag){
				alert("아이디 중복확인을 클릭 해주세요.");
				return;
		}
	 	
	 	if( $.trim($("#usrNm").val()).length == 0 ) {
	 		alert("이름을 입력해주세요.");
	 		$("#usrNm").focus();
	 		return false;
	 	}
	 	
	  /*   if( $.trim($("#compPersonalNo").val()).length == 0 ) {
	 		alert("사번을 입력해주세요.");
	 		$("#compPersonalNo").focus();
	 		return false;
	 	} */
	 	 
	 	
	 	if( ($.trim($("#email1").val()).length == 0 || $.trim($("#email2").val()).length == 0) 
	 			&& ($.trim($("#emailType01").val()).length == 0 )) {
	 		alert("이메일을 입력해주세요.");
	 		return false;
	 	}
	 	var emailAddr = $.trim($("#emailType01").val());
	 	if( $.trim($("#email1").val()).length > 0 && $.trim($("#email2").val()).length > 0){
	 		emailAddr = $.trim($("#email1").val()) + "@" + $.trim($("#email2").val());
	 	}
	 	if( !typeCheck('emailCheck', emailAddr) ) {
	 		alert('이메일을 형식을 확인해 주세요.');
	 		return false;
	 	}
	 	$("#emailType01").val(emailAddr);
	 	
	 	if( $.trim($("#telNo2").val()).length < 3 || $.trim($("#telNo3").val()).length < 4 ) {
	 		alert("전화번호를 입력해주세요.");
	 		$("#telNo2").focus();
	 		return false;
	 	}
	 	if(!typeCheck('numCheck', $.trim($("#telNo2").val()) + $.trim($("#telNo3").val()))){
	 		alert("전화번호는 숫자만 입력 가능합니다.");
	 		return false;
	 	}
	 	//스마트빌ID
	 	//권한
	 	if( $.trim($("#sb_loginId").val()).length == 0 ) {
	 		alert("스마트빌 ID를 매핑해주세요.");
	 		return false;
	 	}
	 	
	 	if(!$('.grn:checked').size()){
		   alert('권한을 선택해주세요.');
		   return false;
	   }
	 	
	 	return true;
	 }
	 
	 $("#btn_save").click(function(){
			
			if(!fnValidate()) {
				return false;
			}
			if(isIng){
				alert("잠시만 기다려 주세요.");
				return false;
			}
			if(confirm("사용자 추가하시겠습니까?")){
				isIng = true;
				
				 $.ajax({
						url: "${HOME}/config/addJoinMemberUser.do",
						type: "POST",
						data:$("#userJoinForm").serialize(),
						dataType: "json",
						async: true,
						error:function(result){
				 			//alert("fail");	
				 			},
						success: function(result){
							 $(opener.location).attr("href", "javascript:getNewRequestList()"); 					 
							 window.close();
						}
					}); 
				
				
				//$('#userJoinForm').attr({'action':'${HOME}/config/addJoinMemberUser.do', 'method':'POST'}).submit(); 
				//window.close();
			}
		});
	 
	 

	 
});

function getSmartbillIdList(){
	var mbrId = $('#mbr_id').val();
	window.open('${HOME}/config/getBizNoSearchPopup.do?mbrId='+mbrId, "popup1", "width=501, height=435, resizable=no, scrollbars=no, menubar=no, toobar=no, status=no, location=no ");
}


$.fn.modifyUserInfo = function($userNm, $usrId, $compUsrId, $loginId, $telNo1, $telNo2, $telNo3, $email1, $email2, $selectEmail) {
	$('#usrNm').val($userNm); // 담당자 이름
	$('#sb_usrId').val($usrId); // 스마트빌 회원순번
	$('#sb_compUsrId').val($compUsrId); // 스마트빌 회사순번
	$('#sb_loginId').val($loginId); // 스마트빌 로그인아이디
	$('#tel_no1').val($telNo1); // 담당자 전화번호1
	$('#tel_no2').val($telNo2); // 담당자 전화번호2
	$('#tel_no3').val($telNo3); // 담당자 전화번호4
	$('#email1').val($email1); // 담당자 이메일1
	$('#email2').val($email2); // 담당자 이메일2
	$('#select_email').val($selectEmail); // 담당자 이메일 셀렉트 박스
};



<%--로그인아이디 중복체크--%>
function fnLoginIdCheck(){
	var loginId = $.trim($("#legacyId").val());
	if( loginId.length == 0 ) {
		alert("아이디를 입력해주세요.");
		$("#loginId").focus();
		loginIdCheckFlag = false;
		return false;
	}
	var ckeckResult = true;
	var hasBlankYn = 'N';
	for( var i=0; i<loginId.length; i++ ) {				
		var chr = loginId.substr( i, 1 );
		if( chr == ' ' ) {
			ckeckResult = false;					
			loginIdCheckFlag = false;
			hasBlankYn = 'Y';
			break;
		}
		if( !numEng.test( chr ) ) {
			ckeckResult = false;
			loginIdCheckFlag = false;
			break;
		}
	} 
	if( !ckeckResult ) {
		if( hasBlankYn == 'Y' ) {
			alert( "아이디에 공백은 허용되지 않습니다." );
		} else {
			alert("아이디는 영문 대소문자, 숫자로만 입력해 주시기 바랍니다.");					
		}
		return false;
	}
	$.ajax({
		url: "${HOME}/config/isDuplicationLoginIdAjax.do",
		type: "post",
		dataType: "json",
		data:{ loginId : loginId },
		async: false,
		error: function(result){
			alert("실패했습니다.");
		},
		success: function(data){
			if(data.model.result){
				alert("이미 사용중인  아이디입니다.");
				loginIdCheckFlag = false;
			}else{
				alert("사용할 수 있는 아이디입니다.");
				loginIdCheckFlag = true;
			}
		}
	});
}
</script>
</head>
<body>
<div id="popWrap">
	<div id="headerWrap">
		<h1><img src="${IMG}/popup/tit_0301_01.gif" /></h1>
	</div>
	<form id="userJoinForm" name="userJoinForm" method="post">
		<input type="hidden" id="mbr_id"  name="mbrId" class="ptblInput" value="${sBox.sessionMbrId}">
		<input type="hidden" id="sb_usrId" value="130952" name="sbUsrId" class="ptblInput" style="width:150px">
		<input type="hidden" id="sb_compUsrId" value="29544" name="sbCompUsrId" class="ptblInput" style="width:150px">
		<div id="contentWrap">
			<div class="ptbl01Wrap">
				<table>
				<colgroup>
					<col width="27%">
					<col width="73%">								
				</colgroup>
				<tbody>
					<tr>
						<th>ID <span class="em_po">*</span></th>
						<td><input type="text" id="legacyId"  name="legacyId" class="ptblInput" style="width:150px">
							<img src="${IMG}/common/btn_s_dupcheck.gif" id="btnLoginIdCheck" alt="중복확인" class="imgBtn" />
						</td>
					</tr>
					<tr>
						<th>이름 <span class="em_po">*</span></th>
						<td><input type="text" id="usrNm" name="usrNm" class="ptblInput" style="width:150px"></td>
					</tr>
					<tr>
						<th>사번</th>
						<td><input type="text" id="compPersonalNo" name="compPersonalNo" class="ptblInput" style="width:150px"></td>
					</tr>
					<tr>
						<th>전화번호 <span class="em_po">*</span></th>
						<td><select class="ptblSelect1" id="telNo1" name="telNo1" title="전화번호 국번" style="width:50px">
								<c:forEach var="phoneCodeList" items="${phoneCodeList}">
									<option value="${phoneCodeList}">${phoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" id="telNo2"  name="telNo2" class="ptblInput"  maxlength="4" style="width:50px"> -
							<input type="text" id="telNo3" name="telNo3" class="ptblInput" maxlength="4" style="width:50px">
						</td>
					</tr>
					<tr>
						<th>이메일 <span class="em_po">*</span></th>
						<td>
							<input type="text" id="emailType01" class="txtInput" name="emailType01" title="이메일" style="display:none" >
							<input type="text" id="email1" name="email1" class="ptblInput" style="width:60px"> @
							<input type="text" id="email2" name="email2" class="ptblInput" style="width:80px">
							<select class="ptblSelect1" id="select_email" title="이메일 도메인 선택" >
								<c:forEach var="emailCodeList" items="${emailCodeList}">
									<option value="${emailCodeList}">
										<c:choose>
											<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
											<c:otherwise>직접입력</c:otherwise>
										</c:choose>
									</option>
								</c:forEach>
							</select>
							
						</td>
					</tr>
					<tr>
						<th>스마트빌 ID <span class="em_po">*</span></th>
						<td><input type="text" value="" name="sbLoginId" id="sb_loginId" class="ptblInput" readonly="readonly"  style="width:150px" > <!-- disabled="disabled" -->
						</td>
						<!-- 회원정보 가져오기 인터페이스 팝업이 떠서 담당자 선택하면  스마트빌 아이디가 자동으로 들어오는 구조로 변경 -->
					</tr>
					<tr>
						<th>권한 <span class="em_po">*</span></th>
						<td><!-- <input type="text" name="grant" class="ptblInput" style="width:150px"> -->
						관리자 : <input type="checkbox" id="admYn" name="admYn" size="20" title="관리자" value="Y" class="grn" ><br>
		    			채무불이행 작성/신청 권한 : <input type="checkbox" name="grnCdArray" size="20" title="채무불이행 작성/신청 권한" value="CRED1C" class="grn"><br>
		    			기업정보검색 권한 : <input type="checkbox"  name="grnCdArray" size="20" title="기업정보검색 권한" value="SBDE1R" class="grn"><br>
		    			기업정보브리핑 권한 : <input type="checkbox" name="grnCdArray" size="20" title="기업정보브리핑 권한" value="SBDE2R" class="grn"><br>
		    			조기경보상세 권한 : <input type="checkbox" name="grnCdArray" size="20" title="조기경보상세 권한" value="SBDE3R" class="grn"><br>
		    			조기경보업체등록 권한 : <input type="checkbox" name="grnCdArray" size="20" title="조기경보업체등록 권한" value="SBDE4R" class="grn"><br>
						</td><!-- 체크박스로 -->
					</tr>
				</tbody>
				</table>
			</div>
		</div>
	</form>
	<div id="footWrap">
		<div class="ex">
			<div class="btn_cdgbtn_01"><a href="#none" id="btn_mapping" onclick="javascript:getSmartbillIdList()">스마트빌 아이디매핑</a></div>
			<div class="btn_cbbtn_01"><a href="#none" id="btn_save">저장</a></div>
			<div class="btn_cgbtn_01"><a href="#none" id="btn_cancle" onclick="self.close()">취소</a></div>
		</div>
	</div>
		
</div>

</body>
</html>