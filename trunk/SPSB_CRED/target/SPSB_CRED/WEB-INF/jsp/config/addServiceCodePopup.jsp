<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/pop.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
});


</script>
</head>
<body>
<div id="popWrap">
	<div id="headerWrap">
		<h1><img src="${IMG}/popup/tit_0302_01.gif" /></h1>
	</div>

	<div id="contentWrap">
		<div class="ptbl01Wrap">
			<table>
			<colgroup>
				<col width="35%">
				<col width="65%">								
			</colgroup>
			<tbody>
				<tr>
					<th>서비스 코드</th>
					<td><input type="text" name="" class="ptblInput" style="width:130px"></td>
				</tr>
				<tr>
					<th>서비스명</th>
					<td><input type="text" name="" class="ptblInput" style="width:200px"></td>
				</tr>
				<tr>
					<th>서비스 회사명</th>
					<td><input type="text" name="" class="ptblInput" style="width:200px"></td>
				</tr>
				<tr>
					<th>서비스 버전</th>
					<td><input type="text" name="" class="ptblInput" style="width:130px"></td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
	
	<div id="footWrap">
		<div class="ex">
			<div class="btn_cbbtn_01"><a href="#none" id="btn_save">저장</a></div>
			<div class="btn_cgbtn_01"><a href="#none" id="btn_cancle" onclick="self.close()">취소</a></div>
		</div>
	</div>
		
</div>

</body>
</html>