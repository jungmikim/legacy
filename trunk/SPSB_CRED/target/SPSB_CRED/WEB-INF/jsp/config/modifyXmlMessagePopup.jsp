<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/pop.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
});


</script>
</head>
<body>
<div id="popWrap">
	<div id="headerWrap">
		<h1><img src="${IMG}/popup/tit_0303_02.gif" /></h1>
	</div>

	<div id="contentWrap">
		<div class="content_tit pru40">XML메시지 정보</div>
		<div class="ptbl01Wrap">
			<table>
			<colgroup>
				<col width="35%">
				<col width="65%">								
			</colgroup>
			<tbody>
				<tr>
					<th>메시지 코드</th>
					<td>DRQPUS</td>
				</tr>
				<tr>
					<th>메시지 내역</th>
					<td><input type="text" name="" class="ptblInput" style="width:200px"></td>
				</tr>
				<tr>
					<th>메시지 버전</th>
					<td><input type="text" name="" class="ptblInput" style="width:130px"></td>
				</tr>
				<tr>
					<th>Transformation</th>
					<td><input type="text" name="" class="ptblInput" style="width:200px"></td>
				</tr>
			</tbody>
			</table>
		</div>
		
		<p class="empty_b15"> </p>
		
		<div class="content_tit pru40">XML파일 다운로드 설정</div>
		<div class="ptbl01Wrap">
			<table>
			<colgroup>
				<col width="35%">
				<col width="65%">								
			</colgroup>
			<tbody>
				<tr>
					<td colspan="2" class="none_line"><strong>로컬PC 다운로드 여부</strong></th>
				</tr>
				<tr>
					<th>다운로드 경로</th>
					<td><input type="text" name="" class="ptblInput" style="width:200px"></td>
				</tr>
			</tbody>
			</table>
			
			<div class="wart1">
			<span class="warn_ico1">※ </span> 파일명은 자동생성<br />
			<span class="empty_word">.....</span>'생성일자' - '생성시간(ms포함)' - '메시지코드'.xml<br />
			<span class="empty_word">.....</span>예) 20140212-011256717-DRQPUS.xml<br />
			</div>
			
		</div>
	</div>
	
	<div id="footWrap">
		<div class="ex">
			<div class="btn_cbbtn_01"><a href="#none" id="btn_save">저장</a></div>
			<div class="btn_cgbtn_01"><a href="#none" id="btn_cancle" onclick="self.close()">취소</a></div>
		</div>
	</div>
		
</div>

</body>
</html>