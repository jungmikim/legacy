<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="sBox" value="${sBox}" />

<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->

<head>
<title>스마트비즈 for Legacy</title>
<%@ include file="../common/meta.jspf"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- TODO 운영에서는 삭제  -->
<meta http-equiv="Cache-Control" content="No-Cache"/>

<link rel="stylesheet" href="${CSS}/common.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="js/placeholders.min.js"></script><!--ie7포함-->
<script type="text/javascript" src="js/PIE.js"></script><!--ie7포함-borderRadius-->


<script type="text/javascript">
$(document).ready(function(){
});
function addXmlMessage(){
	window.open('${HOME}/config/addXmlMessagePopup.do', "popup", "width=480, height=572, resizable=no, scrollbars=no, menubar=no, toobar=no, status=no, location=no, ");

}

function modifyXmlMessage(){
window.open('${HOME}/config/modifyXmlMessagePopup.do', "popup", "width=480, height=572, resizable=no, scrollbars=no, menubar=no, toobar=no, status=no, location=no, ");

}
</script>
</head>
<body>
<div id="Wrapper">
<%--  Top Area Start --%>
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="mgnb" value="0301"/>
</jsp:include>
<%-- Top Area End --%>



<div id="containerWrap">
	
	
<!--  Left Area Start -->
 <jsp:include page="../common/left.jsp" flush="true">
	<jsp:param name="gnb" value="0303"/>
</jsp:include>
<!-- Left Area End -->
	
	<!-- rightWrap -->	
	<div id="rightWrap">
				<div class="right_tit ltit03_0301"><em>XML메시지관리</em></div>
				
				<div id="content_tit_pr_di1">
					<div class="content_tit pru40">XML메시지 관리</div>					
					<div id="btnWrap_duo">
						<div class="btn_bluebtn_01">
						<a href="#none" class="on" id="btn_aduser" onclick="javascript:addXmlMessage();">XML메시지 추가</a>
						</div>
					</div>
				</div>
				
				<div class="tbl04Wrap">
					<table>
					<tbody>
						<tr>
							<th width="5%" class="first_thlwr4">선택</th>
							<th width="10%">메시지 코드</th>
							<th width="18%">메시지 내역</th>
							<th width="10%">메시지 버전</th>
							<th width="15%">Transformation </th>
							<th width="8%">Down</th>
							<th width="12%">다운로드 경로</th>
							<th width="11%">생성시간</th>
							<th width="11%">변경시간</th>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td>DRQPUS</td>
							<td>채권개인회원정보요청서</td>
							<td>1.0.0</td>
							<td>ZT_AR_DRQPUS</td>
							<td>X</td>
							<td>D\temp\</td>
							<td>2014-01-01<br/>13:10:22</td>
							<td>2014-01-01<br/>22:10:08</td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="first_tdlwr4"><input type="checkbox" class="choice" title="선택"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
					</table>
				</div>
				
				<div id="tbl_numbtnWrap">
					<table class="tbl_numbtn">
					<tbody>
						<tr>
							<td class="left">
								<div id="btnWrap_l">
									<div class="btn_whitebtn_01"><a href="#none" class="on" id="btn_modify" onclick="javascript:modifyXmlMessage();">수정</a></div>
								</div>
								<div class="btn_graybtn_01"><a href="#none" class="on" id="btn_del">삭제</a></div>								
							</td>
							<td>
								<div class="page_num">
									<ul>
										<li><input type="button" class="first" value="첫페이지"></li>
										<li><input type="button" class="pre" value="이전 페이지"></li>
										<li><a href="" class="on">1</a><span>l</span></li>
										<li><a href="">2</a><span>l</span></li>
										<li><a href="">3</a><span>l</span></li>
										<li><a href="">4</a><span>l</span></li>
										<li><a href="">5</a><span>l</span></li>
										<li><a href="">6</a><span>l</span></li>
										<li><a href="">7</a><span>l</span></li>
										<li><a href="">8</a><span>l</span></li>
										<li><a href="">9</a><span>l</span></li>
										<li><a href="">10</a></li>
										<li><input type="button" class="next" value="다음 페이지"></li>
										<li><input type="button" class="last" value="끝 페이지"></li>
									</ul>
								</div>
							</td>
							<td class="right">
								<div id="btnWrap_l">
									<div class="btn_graybtn_01"><a href="#none" class="on" id="btn_modify">저장</a></div>
								</div>
								<div class="btn_whitebtn_01"><a href="#none" class="on" id="btn_del">인쇄</a></div>
							</td>
						</tr>
					</tbody>
					</table>
				</div>
				
				
			</div>
		</div>
	


		  
<%-- //contentArea --%>
<%--  footer Area Start --%>
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<%-- footer Area End --%>
</div> <!-- containerWrap -->

</body>
</html>