<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;
var isIng = false;
$(document).ready(function(){
	setSBInfo();
	
	 <%--이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직--%>
	 $('#select_email').live('change',function(e){
		 if($.trim($(this).find('option:selected').val()) == ''){
			 $(this).prev().attr('readonly', false);
			 $("#emailType01").val("");
		 } else {
			 $(this).prev().attr('readonly', true);
		 }
		 $(this).prev().val($(this).val());
	 });
	
	<%--회원가입 확인--%>
	$("#btnConfirm").click(function(){
		if(!fnValidate()) {
			return false;
		}
		if(isIng){
			alert("잠시만 기다려 주세요.");
			return false;
		}
		if(confirm("회원을 가입하시겠습니까?")){
			isIng = true;
			$("#userJoinForm").attr({"action":"${HOME}/user/userAddJoinMemberUser.do"}).submit();
		}
	});
	
	<%--회원가입 취소--%>
	$("#btnCancel").click(function(){
		history.back();
		return false;
		/* if(confirm("회원 가입을 취소하시겠습니까?")){
			location.href = "${HOME}/user/sbUserJoinEachType.do";
		} */
	});
	
});
	<%--스마트빌 정보 set--%>
	function setSBInfo(){
		$("#usrNm").val("${sbInfo.usrNm}");
		$("#loginId").val("${sbInfo.loginId}");
		if("${sbInfo.email}" != ""){
			var emailArray = "${sbInfo.email}".split("@");
			$("#email1").val(emailArray[0]);
			$("#email2").val(emailArray[1]);
		}
		if("${sbInfo.telNo}" != ""){
			var telNoArray = "${sbInfo.telNo}".split("-");
			$("#telNo1").val(telNoArray[0]);
			$("#telNo2").val(telNoArray[1]);
			$("#telNo3").val(telNoArray[2]);
		}
		if("${sbInfo.mbNo}" != ""){
			var mbNoArray = "${sbInfo.mbNo}".split("-");
			$("#mbNo1").val(mbNoArray[0]);
			$("#mbNo2").val(mbNoArray[1]);
			$("#mbNo3").val(mbNoArray[2]);
		}
	}

	<%--회원가입 전송 파라미터 검증 함수--%>
	function fnValidate() {
		if( $.trim($("#usrNm").val()).length == 0 ) {
			alert("이름을 입력해주세요.");
			$("#usrNm").focus();
			return false;
		}
		
		var loginPw = $.trim($("#loginPw").val());
		if( (loginPw.length < 8) || (loginPw.length > 12)) {
			alert("비밀번호는 8글자 이상 12자 이하로 입력하셔야 합니다.");
			return false;
		}
		var passwdCheckResult = true;
		var numCnt = 0;
		for( var i=0; i<loginPw.length; i++ ) {				
			var chr = loginPw.substr( i, 1 );
			if( onlyNum.test( chr ) ) {
				numCnt ++;
			}
			if( !numEng.test( chr ) ) {
				passwdCheckResult = false;
				break;
			}
		}
		if( numCnt == loginPw.length || numCnt == 0 || !passwdCheckResult) {
			alert("비밀번호는 영문과 숫자를 혼용하여 주시기 바랍니다.");
			return false;
		}
		if( $("#loginPw").val() != $("#loginPwConfirm").val() ) {
			alert("설정한 비밀번호가 같지 않습니다.");
			$("#loginPwConfirm").focus();
			return false;
		}
		if( $.trim($("#deptNm").val()).length == 0 ) {
			alert("부서명을 입력해주세요.");
			$("#deptNm").focus();
			return false;
		}
		if( ($.trim($("#email1").val()).length == 0 || $.trim($("#email2").val()).length == 0) 
				&& ($.trim($("#emailType01").val()).length == 0 )) {
			alert("이메일을 입력해주세요.");
			return false;
		}
		var emailAddr = $.trim($("#emailType01").val());
		if( $.trim($("#email1").val()).length > 0 && $.trim($("#email2").val()).length > 0){
			emailAddr = $.trim($("#email1").val()) + "@" + $.trim($("#email2").val());
		}
		if( !typeCheck('emailCheck', emailAddr) ) {
			alert('이메일을 형식을 확인해 주세요.');
			return false;
		}
		$("#emailType01").val(emailAddr);
		
		if( $.trim($("#telNo2").val()).length < 3 || $.trim($("#telNo3").val()).length < 4 ) {
			alert("전화번호를 입력해주세요.");
			$("#telNo2").focus();
			return false;
		}
		if(!typeCheck('numCheck', $.trim($("#telNo2").val()) + $.trim($("#telNo3").val()))){
			alert("전화번호는 숫자만 입력 가능합니다.");
			return false;
		}
		if($.trim($("#faxNo2").val()).length > 0 || $.trim($("#faxNo3").val()).length > 0){
			if(!typeCheck('numCheck', $.trim($("#faxNo2").val()) + $.trim($("#faxNo3").val()))){
				alert("팩스번호는 숫자만 입력 가능합니다.");
				return false;
			}
			if( $.trim($("#faxNo2").val()).length < 3 || $.trim($("#faxNo3").val()).length < 4 ) {
				alert("팩스번호를 정확하게 입력해주세요.");
				$("#faxNo2").focus();
				return false;
			}
		}
		if( $.trim($("#mbNo2").val()).length < 3 || $.trim($("#mbNo3").val()).length < 4 ) {
			alert("휴대폰번호를 입력해주세요.");
			$("#mbNo2").focus();
			return false;
		}
		if(!typeCheck('numCheck', $.trim($("#mbNo2").val()) + $.trim($("#mbNo3").val()))){
			alert("휴대폰번호는 숫자만 입력 가능합니다.");
			return false;
		}
		return true;
	}
	
	<%--우편번호 검색 팝업창--%>
	function open_post(){ 
		open_win('${HOME}/zip/zipSearchPopup.do');
	} 
	
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<h3 class="norm"><img src="${IMG}/common/h3_member_s03.gif" alt="개인회원가입" /></h3>
		<div class="stepbox2">
			<ul>
				<li class="s1"><p><em>STEP 01. 약관동의</em></p></li>
				<li class="s2 on"><p><em>STEP 02. 정보입력</em></p> <i></i></li>
				<li class="s3"><p><em>STEP 03. 가입완료</em></p> <i></i></li>
			</ul>
		</div>
		
		<div id="joinFormDiv">
			<h3 class="norm"><img src="${IMG}/common/h3_member_s032.gif" alt="개인회원 정보입력" /></h3>
			<form id="userJoinForm" name="userJoinForm" method="post">
				<input type="hidden" name="compUsrId" value="${sbInfo.compUsrId }"/>
				<input type="hidden" name="usrNo" value="${sbInfo.usrNo }"/>
				<div class="infoInput">
					<dl class="comMemInput">
						<dt class="name"><span><label for="usrNm">이름</label> <em>*</em></span></dt>
						<dd class="part"><span>
							<input type="text" id="usrNm" name="usrNm" class="txtInput" /> 
						</span></dd>
						
						<dt class="id"><span><label for="loginId">아이디</label> <em>*</em></span></dt>
						<dd class="id"><span>
							${sbInfo.loginId}
							<input type="hidden" id="loginId" name="loginId"/>
						</span></dd>
						
						<dt class="pwd"><span><label for="loginPw">비밀번호</label> <em>*</em></span></dt>
						<dd class="pwd"><span>
							<input type="password" id="loginPw" name="loginPw" class="txtInput" />
							<em>영문 + 숫자 8~12자 가능</em>
						</span></dd>
						
						<dt class="pwdck"><span><label for="loginPwConfirm">비밀번호 확인</label> <em>*</em></span></dt>
						<dd class="pwdck"><span>
							<input type="password" id="loginPwConfirm" class="txtInput" />
							<em>비밀번호 확인을 위해 한 번 더 입력해 주세요.</em>
						</span></dd>
						
						<dt class="part"><span><label for="deptNm">부서명</label> <em>*</em></span></dt>
						<dd class="part"><span>
							<input type="text" id="deptNm" name="deptNm" class="txtInput" />
						</span></dd>
						
						<dt class="part"><span><label for="jobTlNm">직위</label></span></dt>
						<dd class="part"><span>
							<input type="text" id="jobTlNm" name="jobTlNm" class="txtInput" />
						</span></dd>
						
						<dt class="address"><span>주소</span></dt>
						<dd class="address"><span>
							<input type="text" class="txtInput wsmall" id="post_no1" name="postNo1" title="우편번호 앞자리" maxlength="3"/> -
							<input type="text" class="txtInput wsmall" id="post_no2" name="postNo2" title="우편번호 뒷자리" maxlength="3"/> 
							<a href="#none"><img src="${IMG}/common/btn_s_zipcode02.gif" alt="우편번호" class="imgBtn" onClick="javascript:open_post()"/></a></span>
							<span><input type="text" class="txtInput address2" id="cust_addr_1" name="addr1" title="주소" /></span>
							<span><input type="text" class="txtInput address1" id="cust_addr_2" name="addr2" title="상세주소" /> 
						</span></dd>
						
						<dt class="mail"><span>이메일 <em>*</em></span></dt>
						<dd class="mail"><span>
							<input type="text" id="emailType01" class="txtInput" name="email" title="이메일" />
							<span id="emailType02">
								<input type="text" class="txtInput" id="email1" name="email1" title="이메일 주소" />
								@ <input type="text" class="txtInput" id="email2" name="email2" title="이메일 도메인" /> 
								<select id="select_email" title="이메일 도메인 선택" >
									<c:forEach var="emailCodeList" items="${emailCodeList}">
										<option value="${emailCodeList}">
											<c:choose>
												<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
												<c:otherwise>직접입력</c:otherwise>
											</c:choose>
										</option>
									</c:forEach>
								</select>
							</span>
						</span></dd>
						
						<dt class="phone"><span><label for="telNo1">전화번호</label> <em>*</em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="telNo1" name="telNo1" title="전화번호 국번">
								<c:forEach var="phoneCodeList" items="${phoneCodeList}">
									<option value="${phoneCodeList}">${phoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="telNo2" name="telNo2" title="전화번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="telNo3" name="telNo3" title="전화번호 뒷자리" maxlength="4"/> 
						</span></dd>
						
						<dt class="phone"><span><label for="faxNo1">팩스번호</label> <em></em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="faxNo1" name="faxNo1" title="팩스번호 국번">
								<c:forEach var="phoneCodeList" items="${phoneCodeList}">
									<option value="${phoneCodeList}">${phoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="faxNo2" name="faxNo2" title="팩스번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="faxNo3" name="faxNo3" title="팩스번호 뒷자리" maxlength="4"/> 
						</span></dd>
						
						<dt class="phone"><span><label for="mbNo1">휴대폰번호</label> <em>*</em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="mbNo1" name="mbNo1" title="휴대폰번호 국번">
								<c:forEach var="cellPhoneCodeList" items="${cellPhoneCodeList}">
									<option value="${cellPhoneCodeList}">${cellPhoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="mbNo2" name="mbNo2" title="휴대폰번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="mbNo3" name="mbNo3" title="휴대폰번호 뒷자리" maxlength="4"/> 
						</span></dd>
					</dl>
				</div>
			</form>
			<div class="btnWrap btnMember">
				<p><em>*</em> 항목은 필수 입력 사항입니다.</p>
				<div class="btnMiddle">
					<a href="#" class="on" id="btnConfirm">확인</a>
					<a href="#" id="btnCancel">뒤로</a>
				</div>
			</div>
		</div>

	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>