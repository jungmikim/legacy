<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>채권관리 서비스 체험 계정 메일</title>
	<style type="text/css">
		body {color:#707070; font-family:dotum, arial; font-size:12px; margin:0 auto; line-height:1.7em;}
	</style>
</head>

<body style="color:#707070; font-family:dotum, arial; font-size:12px; margin:0 auto; line-height:1.7em;">
	<!-- header -->
	<table style="width:700px; border:0;" cellspacing="0">
		<tbody>
			<tr>
				<td style="border:0px #c7c7c7; border-top:0; height:8px; background-color:#fea700;"></td>
			</tr>
			<tr>
				<td><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="30px"></td>
			</tr>
		</tbody>
	</table>
	<h1 style="margin:5px 20px 3px;"><img src="${sBox.sbDebnDomain}/web/images/mail/tit01.gif" /></h1>
	<!-- //header -->
	
	<!-- contents -->
	<table style="width:700px; border:0;">
		<tbody>
			<tr>
				<td colspan="2" style="border-bottom:1px solid #dedede;"></td>
			</tr>
			<tr>
				<td colspan="2"><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="35px"></td>
			</tr>
			<tr>
				<td width="31"></td>
				<td>안녕하세요, 채권관리 서비스 스마트채권입니다.<br />
				게스트 회원으로 <span style="font-weight:bold; color:#fea700;">1일 체험 계정 받기</span>를 신청해주셔서 감사드립니다.<br /><br />
				1일 동안 채권관리 서비스 중  <span style="font-weight:bold; color:#fea700;">채권진단 서비스를 무료로 사용</span>하실 수 있으며, 
				<span style="font-weight:bold;">최대 5건까지 조회 가능</span>합니다.<br />
				비즈니스온커뮤니케이션의 채권관리 서비스를 체험해보세요!<br /><br />
				아래 버튼을 누르시면 채권관리 서비스로 바로 넘어갑니다.<br /> 
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="35px"></td>
			</tr>
			<tr>
				<td colspan="2" style="border-bottom:1px solid #dedede;"></td>
			</tr>
			<tr>
				<td colspan="2"><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="35px"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<a href="${sBox.sbDebnDomain}/login/loginForGuest.do?key1=${key}&key2=debn">
						<img src="${sBox.sbDebnDomain}/web/images/mail/btn_go.gif">
					</a>
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="10px"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><p style="color:#959595; margin:0 auto;">
				※ 임시 계정의 유효시간은 계정을 확인한 시점에서 24시간입니다.<br />
				※ 계정을 받은 후 24시간 후에 자동 소멸됩니다.
				</p></td>
			</tr>
		</tbody>
	</table>
	<!-- //contents -->
	
	<!-- footer -->
	<table style="width:700px;" cellspacing=0>
		<tbody>
			<tr>
				<td><img src="${sBox.sbDebnDomain}/web/images/mail/space.gif" height="30px"></td>
			</tr>
			<tr>
				<td style="height:4px; background-color:#fea700;"></td>
			</tr>
			<tr>
				<td style="height:245px;background-color:#3e3e3e">
					<div id="footer" style="color:#707070; font-family:dotum, arial; font-size:11px; margin:0 auto; line-height:1.5em;">
						<div style="margin-top:15px; text-align:center;">
							<div style="font-style:normal; color:#9e9e9e;">
								(주)가나다라  서울특별시 강남구 역삼동 ABC빌딩 5층 <span style="color:#535353; margin:0 5px;">|</span>
							</div>
							<div style="font-style:normal; color:#9e9e9e;">
								<span style="display:block; font-style:normal; color:#9e9e9e;">대표자 : 홍길동<span style="color:#535353; margin:0 5px;">|</span>   사업자등록번호 111-22-33567 <span style="color:#535353; margin:0 5px;">|</span> 통신판매업 : 강남-16190</span>
							</div>
							<div style="margin-top:10px;">고객센터 <a href="#" style="color:#00a0e9;">1234-5678</a> <span style="color:#535353; margin:0 5px;">|</span> 개인정보관리 : OOO (<a href="mailto:OOO@businesson.co.kr" style="color:#00a0e9;">OOO@businesson.co.kr</a>)</div>
							<div style="margin:25px 0 0;">Copyright(C) 2012 (주)가나다라.All rights reserved.</div>
							<table style="margin:35px auto 0; width:580px; overflow:hidden; text-align:center;">
								<tr>
									<td>
										<img src="${sBox.sbDebnDomain}/web/images/mail/footer_relative01.gif" alt="서울경제 브랜드대상" />
									</td>
									<td>
										<img src="${sBox.sbDebnDomain}/web/images/mail/footer_relative02.gif" alt="KIBO 벤처기업인증" />
									</td>
									<td>
										<img src="${sBox.sbDebnDomain}/web/images/mail/footer_relative03.gif" alt="표준전자세금 계산서 V3.0" />
									</td>
									<td>
										<img src="${sBox.sbDebnDomain}/web/images/mail/footer_relative04.gif" alt="한국전자문서 산업협회" />
									</td>
									<td>
										<img src="${sBox.sbDebnDomain}/web/images/mail/footer_relative05.gif" alt="국세청 대용량 연계사업자" />
									</td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<!-- //footer -->
</body>
</html>