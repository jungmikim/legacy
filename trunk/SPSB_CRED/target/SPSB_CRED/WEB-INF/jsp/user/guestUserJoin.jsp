<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript">
var isSendEmail = false;
var isIng = false;
$(document).ready(function(){
	<%--이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직--%>
	 $('#select_email').live('change',function(e){
		 if($.trim($(this).find('option:selected').val()) == ''){
			 $(this).prev().attr('readonly', false);
			 $("#email").val("");
		 } else {
			 $(this).prev().attr('readonly', true);
		 }
		 $(this).prev().val($(this).val());
	 });
	 
	 <%--체험계정받기--%>
	 $("#tempLoginIdBtn").click(function(){
		 fnTempLoginId();
	 });
	 
});
	<%--체험계정받기--%>
	function fnTempLoginId(){
		if(!fnGuestValidate()) {
			return false;
		}
		if(isIng){
			alert("잠시만 기다려 주세요.");
			return false;
		}
		if(confirm("체험 계정을 받으시겠습니까?")){
			isIng = true;
			var email = $("#email").val();
			$.ajax({
				url: "${HOME}/user/addGuestUserAjax.do",
				data:{email : email},
				type: "post",
				dataType: "json",
				async: false,
				error: function(result){
					alert("실패하였습니다.");
					isIng = false;
				},
				success: function(data){
					if(data.model.result.isSuccess == true){
						fnGetHtml(data.model.result.key);
					}else{
						alert(data.model.result.message);
						isIng = false;
					}
					
				}
			});
		}
	}

	<%--체험계정Html--%>
	function fnGetHtml(key){
		var email = $("#email").val();
		$.ajax({
			url: "${HOME}/user/getGuestUserHtmlAjax.do",
			data:{email : email
				, key : key},
			type: "post",
			dataType: "html",
			async: false,
			error: function(result){
				alert("실패하였습니다.");
				isIng = false;
			},
			success: function(result){
				fnSendMailGuestUser(result);
			}
		});
	}
	
	<%--체험계정메일보내기--%>
	function fnSendMailGuestUser(emailHtml){
		var email = $("#email").val();
		$.ajax({
			url: "${HOME}/user/sendEmailGuestUserAjax.do",
			data:{email : email
				, emailHtml : emailHtml},
			type: "post",
			dataType: "json",
			async: true,
			error: function(result){
				alert("실패하였습니다.");
				isIng = false;
			},
			success: function(result){
				$("#email1").val("");
				$("#email2").val("");
				$("#email").val("");
				$("#select_email").val("");
				alert("메일이 전송되었습니다.");
				isSendEmail = true;
				isIng = false;
			}
		});
	}
	
	<%--게스트회원 로그인 체크--%>
	function fnGuestValidate(){
		if( ($.trim($("#email1").val()).length == 0 || $.trim($("#email2").val()).length == 0) 
				&& ($.trim($("#email").val()).length == 0 )) {
			alert("이메일을 입력해주세요.");
			return false;
		}
		var emailAddr = $.trim($("#email").val());
		if( $.trim($("#email1").val()).length > 0 && $.trim($("#email2").val()).length > 0){
			emailAddr = $.trim($("#email1").val()) + "@" + $.trim($("#email2").val());
		}
		if( !typeCheck('emailCheck', emailAddr) ) {
			alert('이메일을 형식을 확인해 주세요.');
			return false;
		}
		$("#email").val(emailAddr);
		return true;
	}
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<div class="log_guest">
			<h3 class="norm"><img src="${IMG }/common/h3_login03.gif" alt="게스트회원 로그인" /></h3>
			
			<div class="loginbox">
				<p class="notice">
					<span><em>1일 동안</em> 채권관리 서비스 중 <strong>채권진단 서비스를 무료로 사용</strong>하실 수 있으며,</span>
					<span><em>최고 5건까지 조회</em> 가능합니다. 비즈니스 온의 채권관리 서비스를 체험해보세요!!</span>
				</p>
				<div class="infld">
					<dl>
						<dt>체험 계정을 받으실 메일 주소를 입력해주세요.</dt>
						<dd class="short">
							<input type="text" class="txtInput" id="email" name="email" title="메일 주소" /> 
							<a href="#" class="baseBtn loginBtn" onclick="fnTempLoginId();">체험계정받기</a>
						</dd>
						<dd class="long">
							<input type="text" class="txtInput" id="email1" name="email1" title="메일 주소" />
							@ <input type="text" class="txtInput" id="email2" name="email2" title="메일 도메인" /> 
							<select id="select_email" title="메일 도메인 선택" >
								<c:forEach var="emailCodeList" items="${emailCodeList}">
									<option value="${emailCodeList}">
										<c:choose>
											<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
											<c:otherwise>직접입력</c:otherwise>
										</c:choose>
									</option>
								</c:forEach>
							</select>
							<a href="#none" class="baseBtn loginBtn" id="tempLoginIdBtn">체험계정받기</a>
						</dd>
					</dl>
					<ul>
						<li>
							<span class="dot">※</span>임시 계정의 유효시간은 계정을 확인한 시점에서 24시간 입니다.
						</li>
						<li>
							<span class="dot">※</span>계정을 받은 후 24시간 내에 사용하지 않으시면 자동 소멸됩니다.
						</li>
					</ul>
					<p class="bg"><img src="${IMG }/common/bg_login.gif" alt="" /></p>
				</div>
			</div>
		</div>
	</section>
	<!-- //content -->
</div>
<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>