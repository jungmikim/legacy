<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]><html class="no-js ie ie7 lte7 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8 lte8 lte9" lang="ko"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9 lte9" lang="ko"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" lang="ko"> <!--<![endif]-->
<head>
<title>스마트채권 - 채권관리의 모든 것</title>
<link rel="stylesheet" href="${CSS}/common.css" />
<link rel="stylesheet" href="${CSS}/user/member.css" />
<link rel="stylesheet" href="${CERT}/css/tradesign2.css" type="text/css">
<script type="text/javascript" src="${JS}/jquery/jquery-1.9.1.js"></script> 
<script type="text/javascript" src="${JS}/common/js-pack.min.js"></script>
<script type="text/javascript" src="${JS}/common/common.js"></script>
<script type="text/javascript" src="${JS}/front/frontlib.js"></script>
<script type="text/javascript" src="${JS}/front/frontui.js"></script>
<script type="text/javascript" src="${JS}/respond/respond.src.js"></script> 
<script type="text/javascript" src="${JS}/common/certX.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitConfig.js"></script>
<script type="text/javascript" src="${CERT}/js/TSToolkitObject.js"></script>
<script type="text/javascript">
var numEng = /[0-9]|[a-z]|[A-Z]/;
var onlyNum = /[0-9]/;
var successCertFlag = false; <%--인증서 성공확인Flag 변수--%>

$(document).ready(function(){
	<%--인증서 layer popup call--%>
	setSBInfo();
	
	<%--서업자유형  클릭시--%>
	$("input[name=compType]").click(function(){
		var compType = $(this).val();
		if(compType == "C"){
			$(".corpClass").show();
			$("#ownNmDd").attr("class", "half");
		}else if(compType == "I"){
			$(".corpClass").hide();
			$("#ownNmDd").attr("class", "part");
			$("#corpNo").val("");
		}
	});
	
	<%--이메일 선택 중 직접 입력 시 input box를 활성화 시키는 로직--%>
	$('#select_email').live('change',function(e){
		if($.trim($(this).find('option:selected').val()) == ''){
			$(this).prev().attr('readonly', false);
			$("#emailType01").val("");
		} else {
			$(this).prev().attr('readonly', true);
		}
		$(this).prev().val($(this).val());
	});
	
	<%--회원가입 확인--%>
	$("#btnConfirm").click(function(){
		if(!fnValidate()) {
			return false;
		}
		if(confirm("기업정보을 등록하시겠습니까?")){
			if("${joinType}" == "COMP1"){
				$("#userJoinForm").attr({"action":"${HOME}/user/sbUserJoinCompAdminUserForm.do"}).submit();
			}
		}
	});
	
	<%--회원가입 취소--%>
	$("#btnCancel").click(function(){
		history.back();
		return false;
		/* if(confirm("기업정보 등록을 취소하시겠습니까?")){
			location.href = "${HOME}/user/sbUserJoinEachType.do";
		} */
	});
	
	<%--결제조건변경시 input box를 활성화 시키는 로직--%>
	 $('#payTermType').live('change',function(e){
		 var isDay = $(this).children("option:selected").hasClass('esentialSelect');
		 if(isDay){
			 $(".payTermDayDisplay").show();
		 }else{
			 $(".payTermDayDisplay").hide();
		 }
	 });
	
});
	<%--스마트빌 정보 set--%>
	function setSBInfo(){
		$("#compUsrNm").val("${sbInfo.compUsrNm}");
		$("#ownNm").val("${sbInfo.ownNm}");
		$("#bizCond").val("${sbInfo.bizCond}");
		$("#bizType").val("${sbInfo.bizType}");
		if("${sbInfo.postNo}" != ""){
			$("#post_no1").val("${sbInfo.postNo}".substring(0, 3));
			$("#post_no2").val("${sbInfo.postNo}".substring(3, 6));
		}
		$("#cust_addr_1").val("${sbInfo.addr}");
		if("${sbInfo.email}" != ""){
			var emailArray = "${sbInfo.email}".split("@");
			$("#email1").val(emailArray[0]);
			$("#email2").val(emailArray[1]);
		}
		if("${sbInfo.telNo}" != ""){
			var telNoArray = "${sbInfo.telNo}".split("-");
			$("#telNo1").val(telNoArray[0]);
			$("#telNo2").val(telNoArray[1]);
			$("#telNo3").val(telNoArray[2]);
		}
		if("${sbInfo.mbNo}" != ""){
			var mbNoArray = "${sbInfo.mbNo}".split("-");
			$("#mbNo1").val(mbNoArray[0]);
			$("#mbNo2").val(mbNoArray[1]);
			$("#mbNo3").val(mbNoArray[2]);
		}
	}
	
	<%--기업회원 가입 전송 파라미터 검증 함수--%>
	function fnValidate() {
		if( $.trim($("#usrNo").val()).length == 0 ) {
			alert("사업자등록번호를 입력해주세요.");
			return false;
		}
		if( $.trim($("#compUsrNm").val()).length == 0 ) {
			alert("사업자명을 입력해주세요.");
			$("#compUsrNm").focus();
			return false;
		}
		if( $.trim($("#ownNm").val()).length == 0 ) {
			alert("대표자명을 입력해주세요.");
			$("#ownNm").focus();
			return false;
		}
		if($.trim($("#corpNo").val()).length > 0 && !typeCheck('corpNoCheck', $.trim($("#corpNo").val()))){
			alert('법인등록번호 형식에 맞지 않습니다.');
			return false;
		}
		if( $.trim($("#bizType").val()).length == 0 ) {
			alert("업종를 입력해주세요.");
			$("#bizType").focus();
			return false;
		}
		if( $.trim($("#post_no1").val()).length == 0 || $.trim($("#post_no2").val()).length == 0 ) {
			alert("우편번호를 입력해주세요.");
			return false;
		}
		if(!typeCheck('numCheck', $.trim($("#post_no1").val()) + $.trim($("#post_no2").val()))){
			alert("우편번호는 숫자만 입력 가능합니다.");
			return false;
		}
		if( $.trim($("#cust_addr_1").val()).length == 0 ) {
			alert("주소를 입력해주세요.");
			return false;
		}
		if( ($.trim($("#email1").val()).length == 0 || $.trim($("#email2").val()).length == 0) 
				&& ($.trim($("#emailType01").val()).length == 0 )) {
			alert("이메일을 입력해주세요.");
			return false;
		}
		var emailAddr = $.trim($("#emailType01").val());
		if( $.trim($("#email1").val()).length > 0 && $.trim($("#email2").val()).length > 0){
			emailAddr = $.trim($("#email1").val()) + "@" + $.trim($("#email2").val());
		}
		if( !typeCheck('emailCheck', emailAddr) ) {
			alert('이메일을 형식을 확인해 주세요.');
			return false;
		}
		$("#emailType01").val(emailAddr);
		
		if( $.trim($("#telNo2").val()).length < 3 || $.trim($("#telNo3").val()).length < 4 ) {
			alert("전화번호를 입력해주세요.");
			$("#telNo2").focus();
			return false;
		}
		if(!typeCheck('numCheck', $.trim($("#telNo2").val()) + $.trim($("#telNo3").val()))){
			alert("전화번호는 숫자만 입력 가능합니다.");
			return false;
		}
		if( $.trim($("#mbNo2").val()).length < 3 || $.trim($("#mbNo3").val()).length < 4 ) {
			alert("휴대폰번호를 입력해주세요.");
			$("#mbNo2").focus();
			return false;
		}
		if(!typeCheck('numCheck', $.trim($("#mbNo2").val()) + $.trim($("#mbNo3").val()))){
			alert("휴대폰번호는 숫자만 입력 가능합니다.");
			return false;
		}
		if($("#payTermType").val() == 'NONE'){
			alert("결제조건을 선택해주세요.");
			$("#payTermType").focus();
			return false;
		}
		<%-- 결제 조건(일수) 검증 [대소비교]--%>
		var payTermType = $("#payTermType option:selected");
		if(payTermType.hasClass('esentialSelect')){
			var payTermType = $.trim($("#payTermDay").val());
			if (payTermType > 31 || payTermType <= 0) {
				alert("결제조건기준일은  1~31 사이의 값이어야 합니다.");
				return false;
			}
		}
		/*
		if( !successCertFlag ) {
			alert("기업인증서를 등록해주세요.");
			return false;
		}
		*/
		return true;
	}
	
	<%--인증서 검증 함수--%>
	function fnCheckCertInfo() {
		successCertFlag = false;
		var ssn = $("#usrNo").val();
		CheckIDN(ssn);
	}
	
	<%--인증서 검증 후 결과--%>
	function afterSucessCert(certInfo){
		$("#certInfo").val(certInfo);
		alert("인증서가 등록되었습니다.");
	}
	
	<%--우편번호 검색 팝업창--%>
	function open_post(){ 
		open_win('${HOME}/zip/zipSearchPopup.do');
	} 
	
</script>
</head>
<body>
<!--  Top Area Start -->
<jsp:include page="../common/top.jsp" flush="true">
	<jsp:param name="gnb" value="0702"/>
</jsp:include>
<!-- Top Area End -->
<!-- contentArea -->
<div class="contentArea sub07"><!-- menu별 이미지 class sub00 -->
	<!-- content -->
	<section id="content">
		<div class="titbox">
			<h2 class="tit_sub0702">회원가입</h2>
			<p class="under_h2_sub0702">회원가입을 하시면 더욱 다양하고 편리한 채권관리 서비스를 누리실 수 있습니다</p>
		</div>
		
		<c:choose>
			<c:when test="${joinType eq 'COMP1' }">
				<h3 class="norm"><img src="${IMG }/common/h3_member_s01.gif" alt="기업회원가입1 | 개인회원이 아닌 경우" /></h3>
				<div class="stepbox">
					<ul>
						<li class="s1"><p><em>STEP 01. 약관동의</em></p></li>
						<li class="s2 on"><p><em>STEP 02. 정보입력 (다중 선택 가능)</em></p> <i></i></li>
						<li class="s3"><p><em>STEP 03. 기업담당자 등록</em></p> <i></i></li>
						<li class="s4"><p><em>STEP 04. 가입완료</em></p> <i></i></li>
					</ul>
				</div>
			</c:when>
		</c:choose>
		
		<div id="compUserDiv">
			<h3 class="norm"><img src="${IMG}/common/h3_member_s013.gif" alt="기업회원 정보입력" /></h3>
			<form id="userJoinForm" name="userJoinForm" method="post">
				<input type="hidden" id="usrNm" name="usrNm" value="${sbInfo.usrNm }" />
				<input type="hidden" id="loginId" name="loginId" value="${sbInfo.loginId }" />
				<input type="hidden" id="usrEmail" name="usrEmail" value="${sbInfo.email }" />
				<input type="hidden" id="telNo" name="telNo" value="${sbInfo.telNo }" />
				<input type="hidden" id="mbNo" name="mbNo" value="${sbInfo.mbNo }" />
				<div class="infoInput">
					<dl class="comMemInput">
						<dt class="name"><span><label for="compTypeC">사업자유형</label> <em>*</em></span></dt>
						<dd class="part"><span>
							<input type="radio" id="compTypeC" name="compType" value="C" title="법인사업자" class="radioType" checked="checked" /><label for="compTypeC">법인사업자</label>
							<input type="radio" id="compTypeI" name="compType" value="I" title="개인사업자" class="radioType"/><label for="compTypeI">개인사업자</label>
						</span></dd>
						
						<dt class="name"><span><label for="compUsrNm">사업자 명</label> <em>*</em></span></dt>
						<dd class="half"><span>
							<input type="text" id="compUsrNm" name="compUsrNm" class="txtInput" title="사업자명"/>
						</span></dd>
						<dt class="name"><span><label for="usrNo1">사업자 등록번호</label> <em>*</em></span></dt>
						<dd class="half"><span>
							${fn:substring(sbInfo.usrNo, 0, 3) }-${fn:substring(sbInfo.usrNo, 3, 5) }-${fn:substring(sbInfo.usrNo, 5, 10) }
							<input type="hidden" id="usrNo" name="usrNo" value="${sbInfo.usrNo }" />
						</span></dd>
						
						<dt class="name"><span><label for="ownNm">대표자 명 </label><em>*</em></span></dt>
						<dd id="ownNmDd" class="half"><span>
							<input type="text" id="ownNm" name="ownNm" class="txtInput" title="대표자명"/> 
						</span></dd>
						
						<dt class="name corpClass"><span><label for="corpNo">법인등록번호</label> </span></dt>
						<dd class="half corpClass"><span>
							<input type="text" id="corpNo" name="corpNo" class="txtInput" title="법인등록번호"  maxlength="13"/> 
						</span></dd>
						
						<dt class="name"><span><label for="bizCond">업태</label></span></dt>
						<dd class="half"><span>
							<input type="text" id="bizCond" name="bizCond" class="txtInput" title="업태" /> 
						</span></dd>
						<dt class="name"><span><label for="bizType">업종</label> <em>*</em></span></dt>
						<dd class="half"><span>
							<input type="text" id="bizType" name="bizType" class="txtInput" title="업종" />
						</span></dd>
		
						<dt class="address"><span>회사주소<em> *</em></span></dt>
						<dd class="address"><span>
							<input type="text" class="txtInput wsmall" id="post_no1" name="postNo1" title="우편번호 앞자리" maxlength="3"/> -
							<input type="text" class="txtInput wsmall" id="post_no2" name="postNo2" title="우편번호 뒷자리" maxlength="3"/> 
							<a href="#none"><img src="${IMG}/common/btn_s_zipcode02.gif" alt="우편번호" class="imgBtn" onClick="javascript:open_post()"/></a></span>
							<span><input type="text" class="txtInput address2" id="cust_addr_1" name="addr1" title="주소" /></span>
							<span><input type="text" class="txtInput address1" id="cust_addr_2" name="addr2" title="상세주소" /> 
						</span></dd>
						
						<dt class="mail"><span>이메일 <em>*</em></span></dt>
						<dd class="mail"><span>
							<input type="text" id="emailType01" class="txtInput" name="email" title="이메일" />
							<span id="emailType02">
								<input type="text" class="txtInput" id="email1" name="email1" title="이메일 주소" />
								@ <input type="text" class="txtInput" id="email2" name="email2" title="이메일 도메인" /> 
								<select id="select_email" title="이메일 도메인 선택">
									<c:forEach var="emailCodeList" items="${emailCodeList}">
										<option value="${emailCodeList}">
											<c:choose>
												<c:when test="${not empty emailCodeList}">${emailCodeList}</c:when>
												<c:otherwise>직접입력</c:otherwise>
											</c:choose>
										</option>
									</c:forEach>
								</select>
							</span>
						</span></dd>
						
						<dt class="phone"><span><label for="telNo1">전화번호</label> <em>*</em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="telNo1" name="telNo1" title="전화번호 국번">
								<c:forEach var="phoneCodeList" items="${phoneCodeList}">
									<option value="${phoneCodeList}">${phoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="telNo2" name="telNo2" title="전화번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="telNo3" name="telNo3" title="전화번호 뒷자리" maxlength="4"/> 
						</span></dd>
						
						<dt class="phone"><span><label for="mbNo1">휴대폰번호</label> <em>*</em></span></dt>
						<dd class="phone"><span>
							<select class="required" id="mbNo1" name="mbNo1" title="휴대폰번호 국번">
								<c:forEach var="cellPhoneCodeList" items="${cellPhoneCodeList}">
									<option value="${cellPhoneCodeList}">${cellPhoneCodeList}</option>
								</c:forEach>
							</select> -
							<input type="text" class="txtInput wsmall" id="mbNo2" name="mbNo2" title="휴대폰번호 앞자리" maxlength="4"/> -
							<input type="text" class="txtInput wsmall" id="mbNo3" name="mbNo3" title="휴대폰번호 뒷자리" maxlength="4"/> 
						</span></dd>
						
						<dt class="part"><span><label for="payTermType">결제조건</label> <em>*</em></span></dt>
						<dd class="part"><span>
							<select id="payTermType" name="payTermType" class="payTermType" title="결제조건유형">
								<c:forEach var="payTermTypeList" items="${payTermTypeList}">
									<option value="${payTermTypeList.KEY}" <c:if test="${payTermTypeList.DAYESSENTIAL eq 'true'}">class="esentialSelect"</c:if>>${payTermTypeList.VALUE}</option>
								</c:forEach>
							</select> 
							<input type="text" name="payTermDay" id="payTermDay" class="txtInput wssmall numeric payTermDayDisplay" title="결제조건 일수" style="display:none;" maxlength="2" dir="rtl"/><label class="payTermDayDisplay" style="display:none;"> 일</label>
						</span>
						<dt class="part"><span>기업인증서 등록 <em>*</em></span></dt>
						<dd class="part"><span>
							<!--  <input type="text" class="txtInput wno" id="certInfo" name="certInfo" title="기업인증서 등록" readonly="readonly"/>-->
							<a href="#none" onclick="javascript:fnCheckCertInfo();">
								<img src="${IMG }/common/btn_certificate.gif" alt="기업인증서 등록" class="certImgBtn" />
								<input type="hidden" id="certInfo" name="certInfo" />
							</a>
						</span></dd>
					</dl>
				</div>
			</form>
			<div class="btnWrap btnMember">
				<p><em>*</em> 항목은 필수 입력 사항입니다.</p>
				<div class="btnMiddle">
					<a href="#" class="on" id="btnConfirm">확인</a>
					<a href="#" id="btnCancel">뒤로</a>
				</div>
			</div>
		</div>

	</section>
	<!-- //content -->
</div>

<!-- //contentArea -->

<!--  footer Area Start -->
<jsp:include page="../common/footer.jsp" flush="true"></jsp:include>
<!-- footer Area End -->
</body>
</html>